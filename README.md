# BUSINESSBID SYNOPSIS
This is not a documentation. This synopsis has "In-service" or "Maintained as" technical details about this site.

## Contributors
1. [Amal Ajith](mailto:amal@netiapps.com)

## Repository
Version controlled using Git. Private Repo at GitLab.com. Cannot be cloned without access to repo. Contact [Amal Ajith](mailto:pmamalster@gmail.com) for access to repository.

Click [Businessbid.git](git@gitlab.com:amal2100/Businessbid.git) to go to repository.

## Spec sheet
### Server specification
1. `IP : 128.199.248.0`
2. `Server: Apache/2.4.7 (Ubuntu)`
3. `PHP 5.5.9-1ubuntu4.13`

### Maintenance info & logs
1. This site is built using Symfony v 2.5. Most files of this site except `app/cache/*, app/logs/*` are version controlled. See `.gitignore` for the most recent list of files in or not in version control  
2. All `.htaccess` files deleted and reconfigured in `apache2.conf` and `000-default.conf` when migrated to digitalocean to increase site performance.
3. **Please do not create  `.htaccess` files or enable .htaccess using `AllowOverride All`. It's bad practice**.
4. Disabled sitewide .htaccess using `AllowOverride None`
Please find more on .htaccess in [Official apache documentation](http://httpd.apache.org/docs/current/howto/htaccess.html).  
> You should avoid using .htaccess files completely if you have access to httpd main server config file. Using .htaccess files slows down your Apache http server. Any directive that you can include in a .htaccess file is better set in a Directory block, as it will have the same effect with better performance.

4. Changes made to `apache2/sites-available/000-default.conf`
```
<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/web

        <Directory /var/www/html/web>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ app.php [QSA,L]
        </IfModule>
    </Directory>
        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>
```

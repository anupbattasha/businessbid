# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

  Given a version number MAJOR.MINOR.PATCH, increment the:

  1. MAJOR version when you make incompatible API changes,
  2. MINOR version when you add functionality in a backwards-compatible manner, and
  3. PATCH version when you make backwards-compatible bug fixes.

## [1.0.0] / 30-9-2015
1. Added `/web/stagging/*` to  `.gitignore` list.
2. Email used to set up git in server - bbidsgit@gmail.com / January 10, 1990

## [1.0.1] / 1-10-2015
1. Added `/web/resource_centre/wp-content/*` to the `.gitignore` list. (Repository getting bloated by unwanted images).

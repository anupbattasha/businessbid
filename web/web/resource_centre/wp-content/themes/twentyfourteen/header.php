<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">

<head>
	<?php if (have_posts()):while(have_posts()):the_post(); endwhile; endif;?>
	<?php if (is_category()) { ?>
	<?php  get_home_url(); ?>
<meta http-equiv="content-type" content="<?php bloginfo('html_type');?>character=<?php bloginfo('charset');?>">
<meta name="Description" content="<?php the_permalink() ?>"/>
<meta name="Keywords" content="<?php single_post_title(''); ?>"/>
<meta name="robots" content="<?php category_description(); ?>" />

<!-- if page is others -->
<?php } else { ?>
<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<meta property="og:type" content="website" />
<meta property="og:image" content="logo.jpg" /> <?php } ?>



<title><?php wp_title('|', true, 'left'); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assest/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/bootstrap.min.css">

<!--- new css --->
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/new_page/main.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/new_page/normalize.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/style.css">

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assest/css/main.css"> 
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/new_page/responsive.css">

<!-- <link type="text/css" rel="stylesheet" href="<?php //echo home_url('/');?>wp-content/themes/hostmarks/css/style.css"> -->
<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="wp-content/themes/hostmarks/css/jkmegamenu.css">
<script src="wp-content/themes/twentyfourteen/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<!-- <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script src="http://<?php echo $_SERVER[HTTP_HOST]; ?>/js/bootstrap.js"></script>
<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript">stLight.options({publisher: "916c9cfb-edd5-46f1-b024-c9d477cef57a", doNotHash: false, doNotCopy: false, hashAddressBar: false});

</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
  	<script src="{{ asset('js/html5shiv.min.js') }}"></script>
  	<script src="{{ asset('js/respond.min.js') }}"></script>
	<![endif]-->
<script>
	$(document).ready(function(){
		var realpath = window.location.protocol + "//" + window.location.host + "/";
	$('#category').autocomplete({
		      	source: function( request, response ) {
		      		$.ajax({
		      			url : realpath+"ajax-info.php",
		      			dataType: "json",
						data: {
						   name_startsWith: request.term,
						   type: 'category'
						},
						 success: function( data ) {
							 response( $.map( data, function( item ) {
								return {
									label: item,
									value: item
								}
							}));
						}
		      		});
		      	},
		      	autoFocus: true,
		      	minLength: 2
		      });

});

</script>
<script type='text/javascript' >

	function jsfunction(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						//document.getElementById("searchval").innerHTML=xmlhttp.responseText;
						megamenubycatAjax();
						megamenubycatVendorSearchAjax();
						megamenubycatVendorReviewAjax();
						megamenublogAjax();
					}
				  }
				xmlhttp.open("GET",realpath+"ajax-info.php",true);
				xmlhttp.send();



		}

		function megamenublogAjax(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						document.getElementById("megamenu1").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET",realpath+"mega-menublog.php",true);
				xmlhttp.send();



		}

	function megamenubycatAjax(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						document.getElementById("megamenubycat").innerHTML=xmlhttp.responseText;

					}
				  }
				xmlhttp.open("GET",realpath+"mega-menubycat.php",true);
				xmlhttp.send();



		}

		function megamenubycatVendorSearchAjax(){
				var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					//alert(xmlhttp.responseText);
					//console.log(xmlhttp.responseText);
						var mvsa = document.getElementById("megamenuVenSearchAnch");
						for (var i = 0; i < mvsa.childNodes.length; i++) {
							if (mvsa.childNodes[i].className == "dropdown-menu Searchsegment-drop") {
								mvsa.childNodes[i].innerHTML = xmlhttp.responseText;
							  break;
							}
						}
					//document.getElementById("megamenuVenSearchAnch").innerHTML = xmlhttp.responseText;
					}
				}
				//xmlhttp.open("GET",realpath+"mega-menubycat.php?type=vendorSearch",true);
				xmlhttp.open("GET",realpath+"vendorReviewDropdown.php?type=vendorSearch",true);
				xmlhttp.send();



	}
	function megamenubycatVendorReviewAjax(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						var mvsa = document.getElementById("megamenuVenReviewAnch");
						for (var i = 0; i < mvsa.childNodes.length; i++) {
							if (mvsa.childNodes[i].className == "dropdown-menu Reviewsegment-drop") {
								mvsa.childNodes[i].innerHTML = xmlhttp.responseText;
							  break;
							}
						}
						//document.getElementById("megamenuVenReviewUl").innerHTML = xmlhttp.responseText;
					}
				  }
				//xmlhttp.open("GET",realpath+"mega-menubycat.php?type=vendorReview",true);
				xmlhttp.open("GET",realpath+"vendorReviewDropdown.php?type=vendorReview",true);
				xmlhttp.send();



		}
</script>


<script type="text/javascript">
function show_submenu(b, menuid)
{


	document.getElementById(b).style.display="block";
	if(menuid!="megaanchorbycat"){
		document.getElementById(menuid).className = "setbg";
	}
}

function hide_submenu(b, menuid)
{


		setTimeout(dispminus(b,menuid), 200);



			//document.getElementById(b).onmouseover = function() { document.getElementById(b).style.display="block"; }
			//document.getElementById(b).onmouseout = function() { document.getElementById(b).style.display="none"; }
			//document.getElementById(b).style.display="none";
}

function dispminus(subid,menuid){
	if(menuid!="megaanchorbycat"){
				document.getElementById(menuid).className = "unsetbg";
	}
				document.getElementById(subid).style.display="none";


			}


</script>
<script type="text/javascript">
window.fbAsyncInit = function() {
	FB.init({
	appId      : '754756437905943', // replace your app id here
	status     : true,
	cookie     : true,
	xfbml      : true
	});
};
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
	FB.login(function(response){
		if(response.authResponse){
			window.location.href = "//WWW.businessbid.ae/devfbtestlogin/actions.php?action=fblogin";
		}
	}, {scope: 'email,user_likes'});
}
</script>
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62204441-1', 'auto');
  ga('send', 'pageview');

</script>
<meta name="google-site-verification" content="xyB15PaP0la0qMa8xOYBRacAXqGcvL4eoSnzHX4QlZQ" />
</head>

<body <?php body_class(); ?>>
<div class="main_container">
	<div class="top-nav-bar">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 contact-info">
					<div class="col-sm-3">
						<span class="glyphicon glyphicon-earphone fa-phone"></span>
						<span>(04) 42 13 777</span>
					</div>
					<div class="col-sm-3 left-clear">
						<span class="contact-no"><a href="/contactus">Contact Us</a></span>
					</div>


				</div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4 clear-right">
					<div class="right-top-links">

					    <ul class="nav navbar-nav">
					       <li><a class="fb" href="/login" ><img src="/img/my-login.png"></a></li>
					       <li><a class="fb" href="#"><img src="/img/fbsign.png" onclick="FBLogin();"></a> </li>
						<li><span class="glyphicon glyphicon-search search-block-top"></span></li>
					    </ul>

					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- top header Starts -->
<div class="header">
	<div class="container">
    	<div class="row">
            <div class="col-md-5 logo"><a href="/"><img src="/img/logo.png" ></a></div>
            <div class="col-md-7 right_nav">

            	<nav class="top_menu">
                	<ul class="nav navbar-nav">
                    	<li><a href="/node3">How it Works</a></li>
						<li><a href="/review/login">Write a Review</a></li>
						<!-- <li><a href="/web/app.php/post/quote/inline">Post a Job</a></li> -->
						<li>  <a class="fb" href="/are_you_a_vendor"><img src="/img/ruvendor.jpg"></a> </li>
                  </ul>
                </nav>
				<nav class="top_menu_bottom">
				<ul>

				</ul>
				</nav>

            </div>
        </div>
    </div>
</div>
    <!-- top header Ends -->
    <!-- Search block Starts -->
    <div class="search_block">
    	<div class="container">
        	<div class="row">
			<div class="col-md-7 left_nav">
            	<nav class="user_menu_top">
                	<ul class="nav navbar-nav">

                    	<li><a href="/">Home</a></li>
                       <!-- <li id = "megamenuVenSearchAnch" onMouseOver="javascript:show_submenu('megamenuVenSearch', this.id);" onMouseOut="javascript:hide_submenu('megamenuVenSearch', this.id);"><a href="#" id="vendorsearch" >Vendor Search</a></li>

                        <li id = "megamenuVenReviewAnch" onMouseOver="javascript:show_submenu('megamenuVenReview', this.id);" onMouseOut="javascript:hide_submenu('megamenuVenReview', this.id);"><a href="#" id="vendorreview" >Vendor Reviews</a></li>-->

                        <li id = "megamenuVenSearchAnch" class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendor Search<b class="caret"></b></a>
								<ul class="dropdown-menu Searchsegment-drop"  role="menu" aria-labelledby="dropdownMenu"></ul>
						</li>
						<li id = "megamenuVenReviewAnch" class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendor Review<b class="caret"></b></a>
							<ul class="dropdown-menu Reviewsegment-drop"  role="menu" aria-labelledby="dropdownMenu"></ul>
						</li>

                       <li id = "megamenu1Anch"  onMouseOver="javascript:show_submenu('megamenu1', this.id);" onMouseOut="javascript:hide_submenu('megamenu1', this.id);">
                       	<a href="<?php echo home_url('/');?>home-2" id="megaanchor" >Resource Centre</a>
                       		<ul id="megamenu1" class="submenu menu-mega megamenubg dropdown-menu Resourcesegment-drop" onMouseOver="javascript:show_submenu('megamenu1','megamenu1Anch');" onMouseOut="javascript:hide_submenu('megamenu1','megamenu1Anch');" ></ul>
                       </li>

                    </ul>
                </nav>
               <!-- <script type="text/javascript">jkmegamenu.definemenu("megaanchor", "megamenu1", "mouseover"); </script>
                <script type="text/javascript">jkmegamenu.definemenu("megaanchorbycat", "megamenubycat", "mouseover"); </script>
				<script type="text/javascript">jkmegamenu.definemenu("vendorsearch", "megamenubycat", "mouseover"); </script>
				<script type="text/javascript">jkmegamenu.definemenu("vendorreview", "megamenubycat", "mouseover"); </script>-->
            </div>
				<div class="col-md-5 search_block">
				 <form name="search" method="request" action="/search">
            	 	<input class="col-sm-8 search-box" type="text" list="browsers" placeholder="Type the service you require" id='category' name='category' ><datalist id="searchval"></datalist>
<input type="submit" value="Search" class="search-btn col-sm-3" />
                 </form>
				 </div>
            </div>
	    </div>


    </div>
   	<!-- Search block Ends -->


    <!-- mega menu div block starts -->
		<div id="megamenuVenSearch" class="submenu menu-mega megamenubg" onMouseOver="javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');" onMouseOut="javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');" ><div><ul id="megamenuVenSearchUl"></ul></div></div>
		<div id="megamenuVenReview" class="submenu menu-mega megamenubg-1 megamenubg" onMouseOver="javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');" onMouseOut="javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');"><div class="review-menu-left"><ul id="megamenuVenReviewUl"></ul></div><div  class="review-menu-right"><button class="btn btn-success write-button" type="button" >Write a Review</button></div></div>
		<div id="megamenubycat" class="submenu menu-megabycat megamenubg" onMouseOver="javascript:show_submenu('megamenubycat','megaanchorbycat');" onMouseOut="javascript:hide_submenu('megamenubycat','megaanchorbycat');"><div><ul id="megamenubycatUl"></ul></div><div></div></div>


		<script type="text/javascript">

		document.getElementById("megamenuVenSearch").setAttribute("class", "submenu menu-mega megamenubg");
		document.getElementById("megamenuVenReview").setAttribute("class", "submenu menu-mega megamenubg");
		document.getElementById("megamenu1").setAttribute("class", "submenu menu-mega megamenubg dropdown-menu Resourcesegment-drop");
		document.getElementById("megamenubycat").setAttribute("class", "submenu menu-megabycat megamenubg");
		</script>


    <!-- mega menu div block ends -->


</div>
</body>
</html>
<script type="text/javascript">
window.onload = jsfunction();
//window.onload = getcharactercount();
//window.onload = megamenuAjax();
</script>
<script type="text/javascript">
        $(function() {
            var offset = $("#sidebar").offset();
            var topPadding = 10;
            $(document).scroll(function() {
                if (($(window).scrollTop() > offset.top) && ($(window).scrollTop()+$(window).height() < $(document).height()-150)) {
                    $("#sidebar").stop().animate({
                        marginTop: $(document).scrollTop() - offset.top + topPadding
                    });
                } else {
                   /* $("#sidebar").stop().animate({
                        marginTop: 0
                    });*/
                }; 
            });
        });
    </script>

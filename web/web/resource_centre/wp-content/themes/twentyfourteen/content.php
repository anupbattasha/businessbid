
<div id="post-<?php the_ID(); ?>">
  <div class="postDetail">
    <div class="blog_heading"><a href="<?php the_permalink(); ?>">
      <?php the_title('<h3>', '</h3>'); ?>
      </a> </div>
    <div class="post-meta">
      <?php 

						if (!is_page()) {

						?>
      <div class="authorDetail">
        <div class="authorImage"> 
          <!-- displays the user's photo and then thumbnail -->
          <?php userphoto_the_author_photo() ?>
        </div>
        <div class="authorName"> By <span class="nameDark"> <span class="vcard" style="text-transform:capitalize;"> <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>" title="<?php the_author() ?>"  class="url fn n">
          <?php the_author() ?>
          </a> </span> </span> </div>
        <div class="authorLeft">
          <div class="postDate"> <?php echo get_the_date(); ?> </div>
          <div class="postComment">
            <?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?>
          </div>
        </div>
      </div>
      <div class="blogImg">
        <?php the_post_thumbnail( 'full' ); ?>
      </div>
      <?php 

						}

						?>
    </div>
  </div>
  <span class="blogText">
  <?php 

							if (is_archive() || is_search() || is_category() || is_tag() || is_author() || is_home()) {
								
								echo the_excerpt(80);

							?>
  <div class="aftertext"><a class="CountBtn" href="<?php the_permalink(); ?>">Continue reading</a> <span class="blogSocial"> <?php echo do_shortcode('[simple-social-share]') ;?> </span> </div>
  <?php

							}

							else {

								the_content(); 

							}

						?>
  </span> </div>
<?php 

		if (is_single())  {

						?>
<div class="aboutAuthor">
  <div class="blogQueots">
    <h1>Need a quote for <?php the_category(', ') ?>?
  </h1>
    <?php if (in_category('2')) : ?>
    <?php dynamic_sidebar( 'sidebar-5' ); ?>
    <?php elseif (in_category('3')) : ?>
    <?php dynamic_sidebar( 'sidebar-6' ); ?>
    <?php elseif (in_category('4')) : ?>
    <?php dynamic_sidebar( 'sidebar-7' ); ?>
    <?php elseif (in_category('5')) : ?>
    <?php dynamic_sidebar( 'sidebar-8' ); ?>
    <?php elseif (in_category('6')) : ?>
    <?php dynamic_sidebar( 'sidebar-9' ); ?>
    <?php elseif (in_category('7')) : ?>
    <?php dynamic_sidebar( 'sidebar-10' ); ?>
    <?php else : ?>
    <?php endif; ?>
  </div>
</div>
<div class="relatedArticles">
  <?php wp_related_posts()?>
</div>
<?php 

						}

						?>

<?php

/**

 * Template Name: Custom Home Page

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>


<!--Categories Page-->

<div class="categoriesSec">
  <div class="container">
    <div class="categoriesHed">
      <?php echo get_post_meta($post->ID, 'categoryHeading', true); ?>
    </div>
    <div class="accountantService maincatpage">
      <div class="serviceStep">
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_oneImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_oneText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twoImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twoText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_threeImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_threeText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_fourImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_fourText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6"> 
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_fiveImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_fiveText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_sixImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_sixText', true); ?>
          </span> </div>
      </div>
    </div>
  </div>
</div>
</div>

<!--Categories Page-->


<?php

get_footer();


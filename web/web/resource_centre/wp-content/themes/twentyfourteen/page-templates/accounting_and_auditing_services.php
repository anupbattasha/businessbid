<?php
/**
 * Template Name: Accounting & Auditing Services
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<!--slider-->
<div class="bannerDesign accounting_page_md">
<div class="container">
<div class="col-md-6 col-sm-7">
<div class="bannerText accounting_ali_change"><?php echo get_post_meta($post->ID, 'banner_text', true); ?></div>
</div>
<div class="col-md-6 col-sm-5">
<div class="bannerImg"><?php echo get_post_meta($post->ID, 'banner_img', true); ?></div>
</div>
</div>
</div>
<!--slider--> 

<!--Accouting-Section-->
<div class="accountingSec">
<div class="container">
<div class="col-md-4 col-sm-5">
<div class="accountImg"><?php echo get_post_meta($post->ID, 'Accounting_img', true); ?></div>
</div>
<div class="col-md-8 col-sm-7">
<div class="accountText">
<?php echo get_post_meta($post->ID, 'Accounting_text', true); ?>
</div>
</div>
</div>
</div>
<!--Accouting-Section--> 

<!--Step-Section-->
<div class="stepSection">
<div class="container">
<?php echo get_post_meta($post->ID, 'setup_sectionHeading', true); ?>
<div class="stepS">
<div class="col-md-4 ">
<div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'steup_oneImg', true); ?></span>
<?php echo get_post_meta($post->ID, 'steup_oneText', true); ?>
</div>
</div>
<div class="col-md-4">
<div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'steup_twoImg', true); ?></span>
<?php echo get_post_meta($post->ID, 'steup_twoText', true); ?>
</div>
</div>
<div class="col-md-4">
<div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'steup_threeImg', true); ?></span>
<?php echo get_post_meta($post->ID, 'steup_threeText', true); ?>
</div>
</div>
<div class="stepGet"> <?php echo get_post_meta($post->ID, 'setup_sectionLink', true); ?></div>
</div>
</div>
</div>
<!--Step-Section--> 

<!--Accountant Services-->
<div class="accountantService">
<div class="container">
<?php echo get_post_meta($post->ID, 'a&a_service', true); ?>
</div>
</div>
<!--Accountant Services--> 

<!--assuranceSection-->
<div class="assuranceSection">
<div class="container">
<div class="col-md-9 col-sm-7">
<div class="assuranceText">
<?php echo get_post_meta($post->ID, 'Auditing_AssuranceText', true); ?>
</div>
</div>
<div class="col-md-3 col-sm-5"><span class="assurancImg"><?php echo get_post_meta($post->ID, 'Auditing_AssuranceImg', true); ?></span></div>
</div>
</div>
<!--assuranceSection--> 

<!--Book-Section-->
<div class="bookSection">
<div class="container">
<div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'Bookkeeping_servicesImg', true); ?></span></div>
<div class="col-md-8 col-sm-7">
<div class="bookText">
<?php echo get_post_meta($post->ID, 'Bookkeeping_servicesText', true); ?>
</div>
</div>
</div>
</div>
<!--Book-Section--> 

<!--Budget-Section-->
<div class="budGet">
<div class="container">
<div class="col-md-8 col-sm-7">
<div class="budgetText">
<?php echo get_post_meta($post->ID, 'Budgeting_forecastingText', true); ?>
</div>
</div>
<div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero"><?php echo get_post_meta($post->ID, 'Budgeting_forecastingImg', true); ?></span></div>
</div>
</div>
<!--Budget-Section--> 

<!--Payroll-->
<div class="PayRoll">
<div class="container">
<div class="col-md-4 col-sm-5"><span class="payrollImg"><?php echo get_post_meta($post->ID, 'payroll_serviceImg', true); ?></span></div>
<div class="col-md-8 col-sm-7">
<div class="PayText">
<?php echo get_post_meta($post->ID, 'payroll_serviceText', true); ?>
</div>
</div>
</div>
</div>
<!--Payroll--> 

<!--Qoutes-section-->
<div class="QuotesSec">
<div class="container">
<?php echo get_post_meta($post->ID, 'quotes_section_a&a', true); ?>
 </div>
</div>
<!--Qoutes-section-->

<div class="resourceCenter">
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(2);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=2&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php the_excerpt(); ?></p>
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(2);?>" title="<?php echo get_cat_name(2);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(2);?></a></strong></p>
</div>
 
</div>
</div>


<?php
get_footer();

<?php
/**
 * Template Name: Maintenance
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
        <!-- Main Content File here -->
        <div class="man_headder_area">
            <div class="man_headder_inner">
                <div class="main_child_hedder">
                    <?php echo get_post_meta($post->ID, 'maintenance_banner_text', true); ?>
                    <div class="main_hed_btn">
                        <?php echo get_post_meta($post->ID, 'maintenance_banner_link', true); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Maintenance Service Area here -->
        <div class="man_service_area">
            <div class="man_inner_service">
                <div class="man_child_service_img">
                    <?php echo get_post_meta($post->ID, 'maintenance_sevice_img', true); ?>
                </div>
                <div class="man_child_service_txt">
                   <?php echo get_post_meta($post->ID, 'maintenance_sevice_text', true); ?>
                </div>
            </div>
        </div>

        <!-- Maintenance Step Area here -->
        <div class="man_get_quort_area">
            <div class="man_get_quort_area_inner">
                <?php echo get_post_meta($post->ID, 'maintenance_company_steup', true); ?>
                <div class="man_get_court_child_one">
                    <?php echo get_post_meta($post->ID, 'maintenance_company_steup_one', true); ?>
                </div>
                <div class="man_get_court_child_two">
                    <?php echo get_post_meta($post->ID, 'maintenance_company_steup_two', true); ?>
                </div>
                <div class="man_get_court_child_three">
                    <?php echo get_post_meta($post->ID, 'maintenance_company_steup_three', true); ?>
                </div>
                <div class="man_get_quort_btn">
                    <?php echo get_post_meta($post->ID, 'maintenance_company_steup_link', true); ?>
                </div>
            </div>
        </div> 
        
        <!-- Mantenance Printing press service Area -->
        <div class="man_mainpress_area">
            <div class="man_inner_mainpress_area">
                <?php echo get_post_meta($post->ID, 'maintenance_sevice_type_heading', true); ?>
                <div class="man_child_inner_mainpress_area">
                     <?php echo get_post_meta($post->ID, 'handyman', true); ?>
                </div>
                <div class="man_child_inner_mainpress_area">
                      <?php echo get_post_meta($post->ID, 'electrical', true); ?>                 
                </div>
                <div class="man_child_inner_mainpress_area">
                   <?php echo get_post_meta($post->ID, 'plumbing', true); ?>
                </div>
                <div class="man_child_inner_mainpress_area man_icon_boot">
                    <?php echo get_post_meta($post->ID, 'painting', true); ?>
                </div>
                <div class="man_child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'appliance_install', true); ?>
                </div>
            </div>
        </div>        
        
        <!-- Mantenance Handyman service Area --> 
        <div class="man_handyman_service_arae">
            <div class="inner_handyman_area">
                <div class="child_handyman_txt">
                    <?php echo get_post_meta($post->ID, 'handyman_home_service_text', true); ?>
                </div>
                <div class="child_handyman_img">
                    <?php echo get_post_meta($post->ID, 'handyman_home_service_img', true); ?>
                </div>
            </div>
        </div>       
        
        <!-- Mantenance Electrical Area -->         
        <div class="man_electrical_area">
            <div class="inner_man_electrical">
                <div class="child_man_electrical_img">
                    <?php echo get_post_meta($post->ID, 'electrical_service_text', true); ?>
                </div>
                <div class="child_man_electrical_txt">
                    <?php echo get_post_meta($post->ID, 'electrical_service_img', true); ?>
                </div>
            </div>
        </div>
        
        <!-- Mantenance Plumbing service Area --> 
        <div class="man_plumbing_service_arae">
            <div class="inner_plumbing_area">
                <div class="child_plumbing_txt">
                    <?php echo get_post_meta($post->ID, 'plumber_service_text', true); ?>
                </div>
                <div class="child_plumbing_img">
                    <?php echo get_post_meta($post->ID, 'plumber_service_img', true); ?>
                </div>
            </div>
        </div> 
                 
         <!-- Mantenance Painting Area -->         
        <div class="man_painting_area">
            <div class="inner_man_painting">
                <div class="child_man_painting_img">
                    <?php echo get_post_meta($post->ID, 'painitn_service_img', true); ?>
                </div>
                <div class="child_man_painting_txt">
                   <?php echo get_post_meta($post->ID, 'painitn_service_text', true); ?>
                </div>
            </div>
        </div>       
        
        <!-- Mantenance install Area --> 
        <div class="man_install_service_arae">
            <div class="inner_install_area">
                <div class="child_install_txt">
                    <?php echo get_post_meta($post->ID, 'appliances_install_text', true); ?>
                </div>
                <div class="child_install_img">
                   <?php echo get_post_meta($post->ID, 'appliances_install_img', true); ?>
                </div>
            </div>
        </div> 
        
        <!-- Mantenance Dubai printing Area -->               
        <div class="man_dubai_printing_press_area">
            <div class="man_inner_dubai_printing_press_area">
               <div class="man_child_printing_press_text">
                   <?php echo get_post_meta($post->ID, 'If_you_are_maintence_text', true); ?>
                    <div class="man_printing_dubai_btn">
                        <?php echo get_post_meta($post->ID, 'If_you_are_maintence_link', true); ?>
                    </div>                   
               </div>
            </div>
        </div>        
        
		<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(12);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=12&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php the_excerpt(); ?></p>
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(12);?>" title="<?php echo get_cat_name(12);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(12);?></a></strong></p>
</div>
 
</div>
</div>

<?php
get_footer();
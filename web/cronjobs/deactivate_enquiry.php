<?php
require('parameters.php');
try {
    $sqlQuery = "SELECT id,expstatus FROM `bbids_enquiry` WHERE created <= DATE(NOW() - INTERVAL 72 HOUR) AND expstatus IS NULL ORDER BY `id` DESC";
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $enquiryList = $conn->query($sqlQuery)->fetchAll(PDO::FETCH_ASSOC);
    if($enquiryList) {
        foreach ($enquiryList as $enquiry) {
            $enquiryid =  $enquiry['id'];
            $subSqlQuery = "SELECT COUNT(*) as counter FROM bbids_vendorenquiryrel WHERE enquiryid = $enquiryid AND acceptstatus = 1";
            $counter = $conn->query($subSqlQuery)->fetch(PDO::FETCH_NUM);
            $expstatus = ($counter[0] == '0') ? 2 : 1;

            $updateSql = "UPDATE `bbids_enquiry` SET expstatus = :expstatus WHERE id = :id";
            $preparedStatement = $conn->prepare($updateSql);
            $preparedStatement->execute(array(':expstatus' => $expstatus,':id' => $enquiryid));
        }
        echo "All records update successful";
    } else {
        echo "No records to update";
    }
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}
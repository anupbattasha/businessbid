$(document).ready(function(){	
	
	$('.services').blur(function(){
		var service = $('[name="categories_form[subcategory][]"]:checked').length;
		if(service != 0)
		{
			$('#it_services').text("");
			$('#services').css("border","2px solid #89ceeb");
		}
	}); 
	
	$('.work').blur(function(){
		var people = $('[name="categories_form[people]"]:checked').length;
		if(people != 0)
		{
			$('#it_people').text("");
			$('#work').css("border","2px solid #89ceeb");
		}
	});
		
	$('.require').blur(function(){
		var required = $('[name="categories_form[often]"]:checked').length;
		if(required != 0)
		{
			$('#it_required').text("");
			$('#require').css("border","2px solid #89ceeb");
		}
	});
	
	$('#categories_form_contactname').blur(function(){
		var name =  /^[a-zA-Z ]*$/;
		if($('#categories_form_contactname').val() != '')
			{
				$('#it_name').text("");
				$('#name').css("border","2px solid #89ceeb");
			}
			else if(name.test($('#categories_form_contactname').val()))
			{
				$('#it_name').text("");
				$('#name').css("border","2px solid #89ceeb");
			}
		});
	
	$('#categories_form_email').blur(function(){
		
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		if($('#categories_form_email').val() != '')
			{
				$('#it_email').text("");
				$('#email_id').css("border","2px solid #89ceeb");
			}
			else if(email.test($('#categories_form_email').val()))
			{
				$('#it_email').text("");
				$('#email_id').css("border","2px solid #89ceeb");
			}
		
		});
	 $('.it_location').blur(function(){
      var location = $('[name="categories_form[locationtype]"]:checked').length;
         if(location != 0)
		{
			$('#it_loc').html("");
            $('#location').css("border","2px solid #89ceeb");
		}
    });
	
	$('#categories_form_postJob').click(function(){
		var service = $('[name="categories_form[subcategory][]"]:checked').length;
		var people = $('[name="categories_form[people]"]:checked').length;
		var required = $('[name="categories_form[often]"]:checked').length;
		var name =  /^[a-zA-Z ]*$/;
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		var location = $('[name="categories_form[locationtype]"]:checked').length;
		
		if(service == 0)
		{
			$('#it_services').text("Please select any of the services");
			$('#services').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#services").offset().top},
				'slow');
			//$.scrollTo($('#services'), 500);
		}
		else if(people == 0)
		{
			$('#it_people').text("Please select any value");
			$('#work').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#work").offset().top},
				'slow');
			//$.scrollTo($('#work'), 500);
		}
		else if(required == 0)
		{
			$('#it_required').text("Please select any value");
			$('#require').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#require").offset().top},
				'slow');
			//$.scrollTo($('#require'), 500);
		}
		else if($('#name').length && ($('#categories_form_contactname').val() == ''))
		{
			$('#it_name').text("Please enter your full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
			scrollTop: $("#name").offset().top},
			'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#name').length && (!name.test($('#categories_form_contactname').val())))
		{
			$('#it_name').text("Please enter a valid full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
			scrollTop: $("#name").offset().top},
			'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#email_id').length && ($('#categories_form_email').val() == ''))
		{
			$('#it_email').text("Please enter your Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
			scrollTop: $("#email_id").offset().top},
			'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if($('#email_id').length && (!email.test($('#categories_form_email').val())))
		{
			$('#it_email').text("Please enter a valid Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
			scrollTop: $("#email_id").offset().top},
			'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if(location == 0) 
		{
			$('#it_loc').text("Please select any location");
			$('#location').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#location").offset().top},
				'slow');
			//$.scrollTo($('#location'), 500);
			//return false;
		}
		
	});
	
	
});

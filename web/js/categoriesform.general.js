$(document).ready(function(event){
	
    $('#categories_form_locationtype_0,#categories_form_locationtype_1').click(function(){
        var clickedID = $(this).attr('id');
        clickedID = clickedID.slice(-1);
       
        if(clickedID == 1) {
		
            if($("#intCountry,#intCity,#intContact").hasClass("mask")) {
                $("#intCountry,#intCity,#intContact").removeClass("mask");
                $("#domCity,#domArea,#domContact").addClass("mask");
            }         
            
        } else {
            $("#intCountry,#intCity,#intContact").addClass("mask");
            $("#domCity,#domArea,#domContact").removeClass("mask");
        }
    });
    
    $('#categories_form_verification').val(''); 
		
	var s = $("input[type=radio][name='categories_form[locationtype]']:checked").attr('id');
		 
	if(s == 'categories_form_locationtype_0')
	{
		$("#domCity,#domArea,#domContact").removeClass("mask");
		
	}
	else if(s == 'categories_form_locationtype_1')
	{
		$("#intCountry,#intCity,#intContact").removeClass("mask");			 
		
	}

	//UAE onblur functions
	$('#categories_form_location').blur(function(){
		if(($('#categories_form_location').val() != ''))
		{
			$('#acc_area').html("");
			$('#domArea').css("border","2px solid #89ceeb");
		}
		});
		
	$('#categories_form_mobilecode').blur(function(){
		var mob = /^\d{5,15}$/;
		var mobilecode = /^\d{1,4}$/;
		if(($('#categories_form_mobilecode').val() != '') && ($('#categories_form_mobile').val() != ''))
		{
			$('#acc_mobile').text("");
			$('#mob').css("border","2px solid #89ceeb");
		}
		else if((mobilecode.test($('#categories_form_mobilecodeint').val())) && (mob.test($('#categories_form_mobile').val())))
		{
			$('#acc_mobile').text("");
			$('#mob').css("border","2px solid #89ceeb");
		}
		});
	
	//end of UAE onblur functions

	//INTERNATIONAL onblur functions
	$('#categories_form_country').blur(function(){
		var countryname =  /^[a-zA-Z ]*$/;
		if(($('#categories_form_country').val() != '') && (countryname.test($('#categories_form_country').val())))
		{
			$('#acc_intcountry').html("");
			$('#intCountry').css("border", "2px solid #89ceeb");
		}
		});
	$('#categories_form_cityint').blur(function(){
		var countryname =  /^[a-zA-Z ]*$/;
		if(($('#categories_form_cityint').val() != '') && (countryname.test($('#categories_form_cityint').val())))
		{
			$('#acc_intcity').html("");
			$('#intCity').css("border","2px solid #89ceeb");
		}
		});
   
   $('#categories_form_mobilecodeint').blur(function(){
		var countryname =  /^[a-zA-Z ]*$/;
		if(($('#categories_form_mobilecodeint').val() != '') && (mobilecode.test($('#categories_form_mobilecodeint'))))
		{
			$('#acc_intmobile').html("");
			$('#mobile').css("border","2px solid #89ceeb");
		}
		});
		
	$('#categories_form_mobileint').blur(function(){
		var countryname =  /^[a-zA-Z ]*$/;
		if(($('#categories_form_mobileint').val() != '') && (mob.test($('#categories_form_mobileint').val())))
		{
			$('#acc_intmobile').html("");
			$('#mobile').css("border","2px solid #89ceeb");
		}
		});
		
	$('#categories_form_city').blur(function(){
		if(($('#categories_form_city').val() != ''))
		{
			$('#acc_city').html("");
			$('#domCity').css("border","2px solid #89ceeb");
		}
		});
	//end of international onblur function	
	
    
    $('#categories_form_postJob').click(function(event){
		var countryname =  /^[a-zA-Z ]*$/;
		var mobilecode = /^\d{1,4}$/;
		var intmob = /^\d{5,15}$/;
		var mob = /^\d{5,15}$/;
		
		//international 
		if($('#categories_form_locationtype_1').is(':checked') && ($('#categories_form_country').val() == ''))
		{   
			event.preventDefault();
			$('#acc_intcountry').html("Please enter the country name");
			$('#intCountry').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#intCountry").offset().top},
				'slow');
			//$.scrollTo($('#intCountry'), 500);
			//return false;
		}
		else if($('#categories_form_locationtype_1').is(':checked') && (!countryname.test($('#categories_form_country').val())))
		{
			event.preventDefault();
			$('#acc_intcountry').html("Please enter a valid country"); 
			$('#intCountry').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#intCountry").offset().top},
				'slow');
			//$.scrollTo($('#intCountry'), 500);
			//return false;
		}
			//city validation
		else if($('#categories_form_locationtype_1').is(':checked') && ($('#categories_form_cityint').val() == ''))
		{
			event.preventDefault();
			$('#acc_intcity').html("Please enter the city name");
			$('#intCity').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#intCity").offset().top},
				'slow');
			//$.scrollTo($('#intCity'), 500);
			//return false;
		}
		else if($('#categories_form_locationtype_1').is(':checked') && (!countryname.test($('#categories_form_cityint').val())))
		{
			event.preventDefault();
			$('#acc_intcity').html("Please enter a valid city");
			$('#intCity').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#intCity").offset().top},
				'slow');
			//$.scrollTo($('#intCity'), 500);
			//return false;
		}
		//mobile number validation
		else if($('#categories_form_locationtype_1').is(':checked') && ($('#int_mobileno').length && ($('#categories_form_mobilecodeint').val() == '')))
		{
			event.preventDefault();
			$('#acc_intmobile').html("Please enter a valid mobile code");
			$('#int_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#int_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mob'), 500);
			//return false;
		}
		else if($('#categories_form_locationtype_1').is(':checked') && ($('#int_mobileno').length && ($('categories_form_mobileint').val() == '')))
		{
			event.preventDefault();
			$('#acc_intmobile').html("Please enter a valid mobile number");
			$('#int_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#int_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mob'), 500);
			//return false;
		}			
		else if($('#categories_form_locationtype_1').is(':checked') && ($('#int_mobileno').length && (!mobilecode.test($('#categories_form_mobilecodeint').val()))))
		{
			event.preventDefault();
			$('#acc_intmobile').html("Please enter a valid mobile code");
			$('#int_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#int_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mob'), 500);
			//return false;
		}
		else if($('#categories_form_locationtype_1').is(':checked') && ($('#int_mobileno').length && (!intmob.test($('#categories_form_mobileint').val()))))
		{
			event.preventDefault();
			$('#acc_intmobile').html("Please enter a valid mobile number");
			$('#int_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#int_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mob'), 500);
			//return false;
		}
		
		//UAE
		if($('#categories_form_locationtype_0').is(':checked') && ($('#categories_form_city').val() == ''))
		{		
			event.preventDefault();
			$('#acc_city').html("Please select any city");
			$('#domCity').css("border","2px solid red");
			 $('html,body').animate({
				scrollTop: $("#domCity").offset().top},
				'slow');
			//$.scrollTo($('#domCity'), 500);
			//return false;
		}	
		//location validation
		else if($('#categories_form_locationtype_0').is(':checked') && ($('#categories_form_location').val() == ''))
		{
			event.preventDefault();
			$('#acc_area').html("Please enter the location");
			$('#domArea').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#domArea").offset().top},
				'slow');
			//$.scrollTo($('#domArea'), 500);
			//return false;
		}
		//mobile validation
		else if($('#categories_form_locationtype_0').is(':checked') && $('#uae_mobileno').length && ($('#categories_form_mobilecode').val() == ''))
		{
			event.preventDefault();
			$('#acc_mobile').html("Please select a mobile code");
			$('#uae_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#uae_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mobile'), 500);
			//return false;
		}
		else if($('#categories_form_locationtype_0').is(':checked') && $('#uae_mobileno').length && ($('categories_form_mobile').val() == ''))
		{
			event.preventDefault();
			$('#acc_mobile').html("Please enter a valid mobile number");
			$('#uae_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#uae_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mobile'), 500);
			//return false;
		}
		else if($('#categories_form_locationtype_0').is(':checked') && $('#uae_mobileno').length && (!mob.test($('#categories_form_mobile').val())))
		{
			event.preventDefault();
			$('#acc_mobile').html("Please enter a valid mobile number");
			$('#uae_mobileno').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#uae_mobileno").offset().top},
				'slow');
			//$.scrollTo($('#mobile'), 500);
			//return false;
		}
		
			
	});
});

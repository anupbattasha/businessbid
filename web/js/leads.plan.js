$(document).ready(function(){

    $('a').click(function(event){
        if(disabled)
            event.preventDefault();
    });
    $('span.select-package,span.selected-package').click(function () {
        var clickedID = $( this ).attr('id'), i = 1;
        var pack = clickedID.split("_"), cat = pack[0].split('category');
        for(i=1;i<=4;i++) {
            var $catSelector  = $('span#category'+cat[1]+'_'+i);
            $catSelector.removeClass();
            $catSelector.addClass('select-package');
            $catSelector.html('<a>Select Package</a>');
        }
        $( this ).removeClass('select-package');
        $( this ).addClass('selected-package');
        $( this ).html('<a>Package Selected</a>');
    })
    $('#proceedPayment').click(function () {
        var totalCount = 0;
        $('.selected-package').each(function (){
            totalCount +=1;
        })
        if(totalCount == maxCats) {
            window.location = payUrl;
        } else {
            alert('Please select at one package in each category.');
        }
    })
});
function updatePrice(obj){
    var pack = obj.id.split("_"), packid = pack[1], cat = pack[0].split('category'), catKey = cat[1], price = 0, dicountedPrice = 0, totalPrice = 0, grandTotalPrice = 0,$plan = '';
    if(packid==1) {
        totalPrice = price = parseFloat(planObj[catKey].bPrice)*10;
        savePackagePlan(catKey,packid);
        $plan = 'Bronze Package';
    } else if(packid==2) {
        totalPrice = (parseFloat(planObj[catKey].bPrice)*25);
        price = parseFloat(planObj[catKey].sPrice)*25;
        dicountedPrice = totalPrice - price;
        savePackagePlan(catKey,packid);
        $plan = 'Silver Package';
    } else if(packid==3) {
        totalPrice = (parseFloat(planObj[catKey].bPrice)*50);
        price = parseFloat(planObj[catKey].gPrice)*50;
        dicountedPrice = totalPrice - price;
        savePackagePlan(catKey,packid);
        $plan = 'Gold Package';
    } else if(packid==4) {
        totalPrice = (parseFloat(planObj[catKey].bPrice)*100);
        price = parseFloat(planObj[catKey].pPrice)*100;
        dicountedPrice = totalPrice - price;
        savePackagePlan(catKey,packid);
        $plan = 'Platinum Package';
    }


    $('#categoryName_'+catKey).html($plan+' : '+planObj[catKey].category);
    $('#actualPrice_'+catKey).html('AED '+totalPrice);
    $('#discountPrice_'+catKey).html('AED '+dicountedPrice);
    $('#totalPrice_'+catKey).html('AED '+price);
    grandTotal[catKey] = price;
    for(i=0; i < grandTotal.length; i++) {
        if(grandTotal[i] > 0)
            grandTotalPrice += Number(grandTotal[i]);
    }
    grandTotalPrice +=OTC;
    $('#grandTotal').html('<strong>'+format2(grandTotalPrice,'AED')+'</strong>');
}

function format1(n, currency) {
    return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

function savePackagePlan(catKey,packageType) {
    disabled = true;
    $('a').css('opacity','0.4');
    $("#spinner").show();
    $.post(redeemUrl,
    {
        packageType : packageType,
        key : catKey,
        category : planObj[catKey].category,
        purchaseID : purchaseID
    },function(status){disabled = false;$('a').css('opacity','1');$("#spinner").hide();});
}

function format2(n, currency) {
    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}
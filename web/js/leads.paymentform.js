$(document).ready(function(){

    $('[data-toggle="popover"]').popover();
    $('#form_ccExpDate_month,#form_ccExpDate_year').width(60);
    $('#form_ccSubmit').hide();
    $("#form_ccNumber").keyup(function(){
        var ccVal = $(this).val();
        if(ccVal == '') {
            $('small#errorCCNum').html('Enter card number.');
        } else if(isNaN(ccVal)) {
            $('small#errorCCNum').html('Numbers only !');
        } else {
            $('small#errorCCNum').html('');
            var result = getCreditCardType(ccVal);
            if(result == 'unknown'){
                $('small#errorCCNum').html('Only Master and Visa card allowed');
            } else {
                $('small#errorCCNum').html('');
            }

            var result = isValidCreditCard(type, ccnum);
            if(result) {
                $('small#errorCCNum').html('');
            } else {
                $('small#errorCCNum').html('Invalid card number');
            }
        }
    })
    $("#form_ccName").keyup(function(){
        var ccVal = $(this).val();
        if(ccVal == '') {
            $('small#errorCCName').html('Enter card holder\'s name.');
        } else if(!isNaN(ccVal)) {
            $('small#errorCCName').html('Numbers not allowed !');
        } else {
            $('small#errorCCName').html('');
        }
    })
    $("#form_ccCVV").keyup(function(){
        var ccVal = $(this).val();
        if(ccVal == '') {
            $('small#errorCCVV').html('Enter cvv number.');
        } else if(isNaN(ccVal)) {
            $('small#errorCCVV').html('Numbers only!');
        } else if (ccVal.length > 3){
            $('small#errorCCVV').html('Only 3 digits allowed.');
        } else {
            $('small#errorCCVV').html('');
        }
    })
    $("input[name='payType'],div#editAdress").click(function (){
        var payType = $("input[name='payType']:checked").val();
        if(payType == 'check pickup') {
            $( 'div#checkFillForm' ).toggleClass( "camouflage" );
            $( 'div#checkAddShow' ).toggleClass( "camouflage" );
        }
        else if(payType == 'cash pickup') {
            $( 'div#cashFillForm' ).toggleClass( "camouflage" );
            $( 'div#cashAddShow' ).toggleClass( "camouflage" );
        }
    });
    $('.btn-success').click(function () {
        var payType = $("input[name='payType']:checked").val();//$("input:radio[name='payType']").val();
        alert('Your '+payType+' payment is being processed. Please click OK and do not leave this page');
        if(payType == 'cc') {
            $( "#ccForm" ).submit();
        } else if (payType == 'paypal') {
             $( "#payPalForm" ).submit();
        } else if (payType == 'check pickup') {
            $( "#checkForm" ).submit();
        } else if (payType == 'cash pickup') {
            $( "#cashForm" ).submit();
        }
    })
    $('#form_ccExpDate_month').change(function () {
        var ccmonth = $(this).val();
        if(ccmonth == '') {
            $('small#errorCCExp').html('Select expiry month.');
        } else {
            $('small#errorCCExp').html('');
        }
    })
    $('#form_ccExpDate_year').change(function () {
        var ccmonth = $(this).val();
        if(ccmonth == '') {
            $('small#errorCCExp').html('Select expiry year.');
        } else {
            $('small#errorCCExp').html('');
        }
    })

    $("#form_contactnumber").keyup(function(){
        var ccVal = $(this).val();
        if(ccVal == '') {
            $('small#errorConNum').html('Enter Mobile Contact Number.');
        } else if(isNaN(ccVal)) {
            $('small#errorConNum').html('Numbers only!');
        } else {
            $('small#errorConNum').html('');
        }
    })
});
function getCreditCardType(accountNumber)
{

  //start without knowing the credit card type
  var result = "unknown";

  //first check for MasterCard

  if (/^5[1-5]/.test(accountNumber))
  {
    result = "mastercard";
  }

  //then check for Visa
  else if (/^4/.test(accountNumber))
  {
    result = "visa";
  }

  //then check for AmEx
  /*else if (/^3[47]/.test(accountNumber))
  {
    result = "amex";
  }*/

  return result;
}

function isValidCreditCard(type, ccnum)
{
/* Visa: length 16, prefix 4, dashes optional.
Mastercard: length 16, prefix 51-55, dashes optional.
Discover: length 16, prefix 6011, dashes optional.
American Express: length 15, prefix 34 or 37.
Diners: length 14, prefix 30, 36, or 38. */

  var re = new Regex({ "visa": "/^4\d{3}-?\d{4}-?\d{4}-?\d",
                       "mc": "/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/",
                       "disc": "/^6011-?\d{4}-?\d{4}-?\d{4}$/",
                       "amex": "/^3[47]\d{13}$/",
                       "diners": "/^3[068]\d{12}$/"}[type.toLowerCase()])

   if (!re.test(ccnum)) return false;
   // Remove all dashes for the checksum checks to eliminate negative numbers
   ccnum = ccnum.split("-").join("");
   // Checksum ("Mod 10")
   // Add even digits in even length strings or odd digits in odd length strings.
   var checksum = 0;
   for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
      checksum += parseInt(ccnum.charAt(i-1));
   }
   // Analyze odd digits in even length strings or even digits in odd length strings.
   for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
      var digit = parseInt(ccnum.charAt(i-1)) * 2;
      if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
   }
   if ((checksum % 10) == 0) return true; else return false;
}
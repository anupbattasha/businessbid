function reQuote () {
    var i = 2;
    for (i = 2; i < 5; i++) {
        $("div#step"+i).hide();
    };
    $("div#step1").show();
    $('p#enquiryResult').text('Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!');
}
function selVendorsInit(vList) {
    var i = 0;
    jQuery.each(vList,function(index, item) {
        i++;
        $('input:checkbox.vcheck').each(function () {
            if((index == $(this).val()) && (item.status == 1)) $(this).prop('checked', true);
        });
        if(item.status == 1) {

            jQuery('input#form_com'+i).val(item.name);
        }
        else
            jQuery('input#form_com'+i).val('');
        if(i == 5) return false;
    })
    return true;
}
function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}
$(document).ready(function(){
    $("#musicServicesBlock,#entertainersBlock,#modelsBlock,#hostsBlock,#staffBlock").hide();
    $( "#categories_form_event_planed" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth: true,changeYear: true,minDate: 0, maxDate: "+15M +10D"});
    $('#categories_form_subcategory_other').click(function(){
        $('#categories_form_subcategory_2').prop('checked',true);
    });
    $('#categories_form_help_other').click(function(){
        $('#categories_form_help_type_5').prop('checked',true);
    });
    $('#categories_form_theme_other').click(function(){
        $('#categories_form_theme_0').prop('checked',true);
    });
    $('div.proceed').click(function(){
        var clickedStep = $(this).children("a:first").attr('id'), $step = clickedStep.match(/\d+/);
        console.log("step : "+$step)
        if($step) {
            if($step == "1") {
                var service = $('[name="categories_form[subcategory][]"]:checked').length, eventPlaned = $('[name="categories_form[event_planed]"]').val();
                if(service == 0)
                {
                    $('#kind_service').text("Please select any services");
                    $('div.kindofService').css("border","2px solid red");
                    return false;
                }
                else if($('#categories_form_subcategory_2').is(':checked') && ($('#categories_form_subcategory_other').val() == ''))
                {
                    $('#kind_service').text("Please enter other data");
                    $('div.kindofService').css("border","2px solid red");
                    scrollBox($('div.kindofService'));
                    return false;
                }
                $('div.kindofService').removeAttr('style');$('#kind_service').empty();
                if(eventPlaned == '')
                {
                    $('#weed_planed').text("Please enter the event date.");
                    $('div.eventPlaned').css("border","2px solid red");
                    scrollBox($('div.eventPlaned'));
                    return false;
                }
                $('div.eventPlaned').removeAttr('style');$('#weed_planed').empty();
            }  else if ($step == "2"){

                var helpType = $('[name="categories_form[help_type][]"]').length, theme = $('[name="categories_form[theme]"]:checked').length, guests = $('[name="categories_form[guests]"]:checked').length;
                if(helpType == 0)
                {
                    $('#weed_helpType').text("Please select any value");
                    $('div.helpType').css("border","2px solid red");
                    scrollBox($('div.helpType'));
                    return false;
                }
                else if($('#categories_form_help_type_5').is(':checked') && ($('#categories_form_help_other').val() == ''))
                {
                    $('#weed_helpType').text("Please enter other data");
                    $('div.helpType').css("border","2px solid red");
                    scrollBox($('div.helpType'));
                    return false;
                }
                $('div.helpType').removeAttr('style');$('#weed_helpType').empty();
                if(guests == 0)
                {
                    $('#event_guests').text("Please select any value");
                    $('div.guests').css("border","2px solid red");
                    scrollBox($('div.guests'));
                    return false;
                }
                $('div.guests').removeAttr('style');$('div#event_guests').empty();
                if(theme == 0)
                {
                    $('#weed_theme').text("Please select any value");
                    $('div.weedTheme').css("border","2px solid red");
                    scrollBox($('div.weedTheme'));
                    return false;
                }
                else if($('#categories_form_theme_0').is(':checked') && ($('#categories_form_theme_other').val() == ''))
                {
                    $('#weed_theme').text("Please enter other data");
                    $('div.weedTheme').css("border","2px solid red");
                    scrollBox($('div.weedTheme'));
                    return false;
                }
                $('div.weedTheme').removeAttr('style');$('#weed_theme').empty();
                validationFlag = false;
            }
            else if ($step == "3") {
                var name =  /^[a-zA-Z ]*$/, email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, locationtype = $('[name="categories_form[locationtype]"]:checked');

                var countryname =  /^[a-zA-Z ]*$/, mobilecode = /^\d{1,4}$/, intmob = /^\d{5,15}$/, mob = /^\d{7,15}$/;

                if($('#categories_form_contactname').length >0) {
                    if($('#categories_form_contactname').val() == '')
                    {
                        $('#name').text("Please enter your full name");
                        $('div.name').css("border","2px solid red");
                        scrollBox($('div.name'));return false;
                        validationFlag = false;
                        return false;
                    }
                    else if(!name.test($('#categories_form_contactname').val()))
                    {
                        $('#name').text("Please enter a valid full name");
                        $('div.name').css("border","2px solid red");
                        scrollBox($('div.name'));return false;
                        validationFlag = false;
                        return false;
                    }
                    $('div.name').removeAttr('style');$('#name').empty();
                }
                if($('#categories_form_email').length >0) {
                    if($('#categories_form_email').val() == '')
                    {
                        $('#email').text("Please enter your Email-Id");
                        $('div.email').css("border","2px solid red");
                        scrollBox($('div.email'));return false;
                        validationFlag = false;
                        return false;
                    }
                    else if(!email.test($('#categories_form_email').val()))
                    {
                        $('#email').text("Please enter a valid Email-Id");
                        $('div.email').css("border","2px solid red");
                        scrollBox($('div.email'));return false;
                        validationFlag = false;
                        return false;
                    }
                    $('div.email').removeAttr('style');$('#email').empty();
                }
                if(locationtype.length == 0)
                {
                    $('#location').text("Please select any location");
                    $('div.location').css("border","2px solid red");
                    scrollBox($('div.location'));
                    validationFlag = false;
                    return false;
                } else {
                    $('div.location').removeAttr('style');$('#location').empty();
                    if(locationtype.attr('id') == "categories_form_locationtype_0") {
                        if($('#categories_form_city').val() == '')
                        {
                            $('#acc_city').html("Please select any city");
                            $('#domCity').css("border","2px solid red");
                            scrollBox($('#domCity'));return false;
                        }
                        else if($('#categories_form_location').val() == '')
                        {
                            $('div#domCity').removeAttr('style');$('#acc_city').empty();

                            $('#acc_area').html("Please enter the location");
                            $('#domArea').css("border","2px solid red");
                            scrollBox($('#domArea'));return false;
                        }
                        else if($('#uae_mobileno').length && ($('#categories_form_mobilecode').val() == ''))
                        {
                            $('div#domArea').removeAttr('style');$('#acc_area').empty();

                            $('#acc_mobile').html("Please select a mobile code");
                            $('#uae_mobileno').css("border","2px solid red");
                            scrollBox($('#uae_mobileno'));return false;
                        }
                        else if($('#uae_mobileno').length && ($('categories_form_mobile').val() == ''))
                        {

                            $('#acc_mobile').html("Please enter a valid mobile number");
                            $('#uae_mobileno').css("border","2px solid red");
                            scrollBox($('#uae_mobileno'));return false;
                        }
                        else if($('#uae_mobileno').length && (!mob.test($('#categories_form_mobile').val())))
                        {
                            $('#acc_mobile').html("Please enter a valid mobile number");
                            $('#uae_mobileno').css("border","2px solid red");
                            scrollBox($('#uae_mobileno'));return false;
                        }

                        $('div#uae_mobileno').removeAttr('style');$('#acc_mobile').empty();

                    } else {
                        if($('#categories_form_country').val() == '')
                        {
                            $('#acc_intcountry').html("Please enter the country name");
                            $('#intCountry').css("border","2px solid red");
                            scrollBox($('#intCountry'));return false;
                        }
                        else if(!countryname.test($('#categories_form_country').val()))
                        {
                            $('#acc_intcountry').html("Please enter a valid country");
                            $('#intCountry').css("border","2px solid red");
                            scrollBox($('#intCountry'));return false;
                        }
                        else if($('#categories_form_cityint').val() == '')
                        {
                            $('#acc_intcity').html("Please enter the city name");
                            $('#intCity').css("border","2px solid red");
                            scrollBox($('#intCity'));return false;
                        }
                        else if(!countryname.test($('#categories_form_cityint').val()))
                        {
                            $('#acc_intcity').html("Please enter a valid city");
                            $('#intCity').css("border","2px solid red");
                            scrollBox($('#intCity'));return false;
                        }
                        //mobile number validation
                        else if($('#int_mobileno').length && ($('#categories_form_mobilecodeint').val() == ''))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile code");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        else if($('#int_mobileno').length && ($('categories_form_mobileint').val() == ''))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile number");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        else if($('#int_mobileno').length && (!mobilecode.test($('#categories_form_mobilecodeint').val())))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile code");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        else if($('#int_mobileno').length && (!intmob.test($('#categories_form_mobileint').val())))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile number");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        $('div#intCity,div#intCountry,div#int_mobileno').removeAttr('style');$('#acc_intcountry,#acc_intcity,#acc_intmobile').empty();


                    }
                }

                /*if($("#recaptcha_response_field").length) {
                    if($("#recaptcha_response_field").val() == '') {
                        $('div#recaptcha_widget_div').css("border","2px solid red");
                        return false;
                    }
                    $('div#recaptcha_widget_div').removeAttr('style');
                }*/
                validationFlag = true;
            }

            $("div#step"+$step).hide();
            $step++;
            $("div#step"+$step).show();
            scrollBox($("div#step"+$step));
            return true;
        }
    });

    $("div.goback").click(function(){
        var gobackStep = $(this).children("a:first").attr('id'), $step = gobackStep.match(/\d+/);
        $("div#step"+$step).show();
        $step++;
        $("div#step"+$step).hide();
    });

    $('select[name="sorting"]').on('change', function() {
      $('input#filterform_sort_type').val($(this).val());
      $('form[name="filterform"]').submit();
    });

    $('span[id^="contactdetailsdiv"]').hide();
    $('input[id^="contactdetails-"]').click(function(){
            var btid = $(this).attr('id');
            var arr = btid.split('-');


        $('#contactdetails-'+arr[1]).hide();
        $('#contactdetailsdiv'+arr[1]).show();

    });
    $.fn.stars = function() {
        return $(this).each(function() {
            // Get the value
            var val = parseFloat($(this).html());
            // Make sure that the value is in 0 - 5 range, multiply to get width
            var size = Math.max(0, (Math.min(5, val))) * 16;

            // Create stars holder
            var $span = $('<span />').width(size);

            // Replace the numerical value with stars
            $(this).html($span);
        });
    }
    $('span.stars').stars();
});

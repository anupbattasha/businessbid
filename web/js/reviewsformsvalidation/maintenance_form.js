function reQuote () {
    var i = 2;
    for (i = 2; i < 5; i++) {
        $("div#step"+i).hide();
    };
    $("div#step1").show();
    $('p#enquiryResult').text('Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!');
}
function selVendorsInit(vList) {
    var i = 0;
    jQuery.each(vList,function(index, item) {
        i++;
        $('input:checkbox.vcheck').each(function () {
            if((index == $(this).val()) && (item.status == 1)) $(this).prop('checked', true);
        });
        if(item.status == 1) {

            jQuery('input#form_com'+i).val(item.name);
        }
        else
            jQuery('input#form_com'+i).val('');
        if(i == 5) return false;
    })
    return true;
}
function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}
$(document).ready(function(){
    $('#categories_form_handyman_other').click(function(){
        $('#categories_form_handyman_4').prop('checked',true);
        });
    $('#categories_form_appliances_other').click(function(){
        $('#categories_form_appliances_4').prop('checked',true);
    });
    $('#categories_form_painting_other').click(function(){
        $('#categories_form_painting_2').prop('checked',true);
    });
    $('#categories_form_plumbing_other').click(function(){
        $('#categories_form_plumbing_6').prop('checked',true);
    });
    $('#categories_form_electrical_other').click(function(){
        $('#categories_form_electrical_5').prop('checked',true);
    });
    $('#categories_form_joinery_other').click(function(){
        $('#categories_form_joinery_3').prop('checked',true);
    });
    $('#categories_form_flooring_type_other').click(function(){
        $('#categories_form_flooring_type_3').prop('checked',true);
    });

    $('div.proceed').click(function(){
        var clickedStep = $(this).children("a:first").attr('id'), $step = clickedStep.match(/\d+/);
        console.log("step : "+$step)
        if($step) {
            if($step == "1") {
                var service = $('[name="categories_form[subcategory][]"]:checked').length;
                if(service == 0)
                {
                    $('#kind_service').text("Please select any services");
                    $('div.kindofService').css("border","2px solid red");
                    return false;
                }
                $('div.kindofService').removeAttr('style');$('#kind_service').empty();
                $("[name='categories_form[subcategory][]']:checked:checked:enabled").each(function() {
                    var selectedSubCategory = $( this ).val();
                    switch (selectedSubCategory) {
                        case '1006' :
                            $('div#handymanBlock').show();
                            break;
                        case '1007':
                            $('div#plumbingBlock').show();
                            break;
                        case '1008':
                            $('div#appliancesBlock').show();
                            break;
                        case '1009':
                            $('div#electricalBlock').show();
                            break;
                        case '1010':
                            $('div#paintingBlock').show();
                            break;
                        case '1045':
                            $('div#carpentryBlock,div#joineryBlock').show();
                            break;
                        case '1047':
                            $('div#flooringBlock,div#flooringtypeBlock').show();
                            break;
                    }
                });
            }  else if ($step == "2"){
                var propertyType = $('[name="categories_form[property_type][]"]:checked').length, dimensions = $('[name="categories_form[dimensions]"]:checked').length;
                var failedFlag = false;
                $("[name='categories_form[subcategory][]']:checked:checked:enabled").each(function() {
                    var selectedSubCategory = $( this ).val();
                    switch (selectedSubCategory) {
                        case '1006' :
                            var handymanService = $('[name="categories_form[handyman][]"]:checked').length;
                                if(handymanService == 0)
                                {
                                    $('#handyman_service').text("Please select any value");
                                    $('div.handymanService').css("border","2px solid red");
                                    scrollBox($('div.handymanService'));failedFlag = true;
                                    return false;
                                }
                                else if($('#categories_form_handyman_4').is(':checked') && ($('#categories_form_handyman_other').val() == ''))
                                {
                                    $('#handyman_service').text("Please enter other data");
                                    $('div.handymanService').css("border","2px solid red");
                                    scrollBox($('div.handymanService'));failedFlag = true;return false;
                                }
                                $('div.handymanService').removeAttr('style');$('#handyman_service').empty();
                                break;
                        case '1007':
                            var plumbing = $('[name="categories_form[plumbing][]"]:checked').length;
                            if(plumbing == 0)
                            {
                                $('#plumbing_type').text("Please select any value");
                                $('div.plumbingType').css("border","2px solid red");
                                scrollBox($('div.plumbingType'));failedFlag = true;
                                return false;
                            }
                            else if($('#categories_form_plumbing_6').is(':checked') && ($('#categories_form_plumbing_other').val() == ''))
                            {
                                $('#plumbing_type').text("Please enter other data");
                                $('div.plumbingType').css("border","2px solid red");
                                scrollBox($('div.plumbingType'));failedFlag = true;return false;
                            }
                            $('div.plumbingType').removeAttr('style');$('#plumbing_type').empty();
                            break;
                        case '1008':
                            var appliances = $('[name="categories_form[appliances][]"]:checked').length;
                            if(appliances == 0)
                            {
                                $('#appliances_type').text("Please select any value");
                                $('div.appliances').css("border","2px solid red");
                                scrollBox($('div.appliances'));failedFlag = true;
                                return false;
                            }
                            else if($('#categories_form_appliances_4').is(':checked') && ($('#categories_form_appliances_other').val() == ''))
                            {
                                $('#appliances_type').text("Please enter other data");
                                $('div.appliances').css("border","2px solid red");
                                scrollBox($('div.appliances'));failedFlag = true;return false;
                            }
                            $('div.appliances').removeAttr('style');$('#appliances_type').empty();
                            break;
                        case '1009':
                            var electrical = $('[name="categories_form[electrical][]"]:checked').length;
                            if(electrical == 0)
                            {
                                $('#electrical_type').text("Please select any value");
                                $('div.electricalType').css("border","2px solid red");
                                scrollBox($('div.electricalType'));failedFlag = true;
                                return false;
                            }
                            else if($('#categories_form_electrical_5').is(':checked') && ($('#categories_form_electrical_other').val() == ''))
                            {
                                $('#electrical_type').text("Please enter other data");
                                $('div.electricalType').css("border","2px solid red");
                                scrollBox($('div.electricalType'));failedFlag = true;return false;
                            }
                            $('div.electricalType').removeAttr('style');$('#electrical_type').empty();
                            break;
                        case '1010':
                            var painting = $('[name="categories_form[painting][]"]:checked').length;
                            if(painting == 0)
                            {
                                $('#painting_type').text("Please select any value");
                                $('div.paintingType').css("border","2px solid red");
                                scrollBox($('div.paintingType'));failedFlag = true;
                                return false;
                            }
                            else if($('#categories_form_painting_2').is(':checked') && ($('#categories_form_painting_other').val() == ''))
                            {
                                $('#painting_type').text("Please enter other data");
                                $('div.paintingType').css("border","2px solid red");
                                scrollBox($('div.paintingType'));failedFlag = true;return false;
                            }
                            $('div.paintingType').removeAttr('style');$('#painting_type').empty();
                            break;
                        case '1043':
                            var carpentry = $('[name="categories_form[carpentry]"]:checked').length;
                            if(carpentry == 0)
                            {
                                $('#carpentry_type').text("Please select any value");
                                $('div.carpentryType').css("border","2px solid red");
                                scrollBox($('div.carpentryType'));failedFlag = true;
                                return false;
                            }
                            $('div.carpentryType').removeAttr('style');$('#carpentry_type').empty();
                            var joinery = $('[name="categories_form[joinery][]"]:checked').length;
                            if(joinery == 0)
                            {
                                $('#joinery_type').text("Please select any value");
                                $('div.joinery').css("border","2px solid red");
                                scrollBox($('div.joinery'));failedFlag = true;
                                return false;
                            }
                            else if($('#categories_form_joinery_3').is(':checked') && ($('#categories_form_joinery_other').val() == ''))
                            {
                                $('#joinery_type').text("Please enter other data");
                                $('div.joinery').css("border","2px solid red");
                                scrollBox($('div.joinery'));failedFlag = true;return false;
                            }
                            $('div.joinery').removeAttr('style');$('#joinery').empty();
                            break;
                        case '1045':
                            var flooring = $('[name="categories_form[flooring]"]:checked').length;
                            if(flooring == 0)
                            {
                                $('#flooring').text("Please select any value");
                                $('div.flooring').css("border","2px solid red");
                                scrollBox($('div.flooring'));failedFlag = true;
                                return false;
                            }
                            $('div.flooring').removeAttr('style');$('#flooring').empty();
                            var flooring_type = $('[name="categories_form[flooring_type][]"]:checked').length;
                            if(flooring_type == 0)
                            {
                                $('#flooring_type').text("Please select any value");
                                $('div.flooringType').css("border","2px solid red");
                                scrollBox($('div.flooringType'));failedFlag = true;
                                return false;
                            }
                            else if($('#categories_form_flooring_type_3').is(':checked') && ($('#categories_form_flooring_type_other').val() == ''))
                            {
                                $('#flooring_type').text("Please enter other data");
                                $('div.flooringType').css("border","2px solid red");
                                scrollBox($('div.flooringType'));failedFlag = true;return false;
                            }
                            $('div.flooring_type').removeAttr('style');$('#flooring_type').empty();
                            break;
                    }

                });
                if(failedFlag)return false;
                if(propertyType == 0)
                {
                    $('#maintenance_property').text("Please select any value");
                    $('div.property').css("border","2px solid red");
                    scrollBox($('div.property'));
                    return false;
                }
                else if($('#categories_form_property_type_3').is(':checked') && ($('#categories_form_property_type_other').val() == ''))
                {
                    $('#maintenance_property').text("Please enter other data");
                    $('div.property').css("border","2px solid red");
                    scrollBox($('div.property'));failedFlag = true;return false;
                }
                $('div.property').removeAttr('style');$('div#maintenance_property').empty();
                if(dimensions == 0)
                {
                    $('#maintenance_dimension').text("Please select any value");
                    $('div.dimensions').css("border","2px solid red");
                    scrollBox($('div.dimensions'));
                    return false;
                }
                else if($('#categories_form_dimensions_5').is(':checked') && ($('#categories_form_dimensions_other').val() == ''))
                {
                    $('#maintenance_dimension').text("Please enter other data");
                    $('div.dimensions').css("border","2px solid red");
                    scrollBox($('div.dimensions'));failedFlag = true;return false;
                }
                $('div.dimensions').removeAttr('style');$('div#maintenance_dimension').empty();
                validationFlag = false;
            }
            else if ($step == "3") {
                var name =  /^[a-zA-Z ]*$/, email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, locationtype = $('[name="categories_form[locationtype]"]:checked');

                var countryname =  /^[a-zA-Z ]*$/, mobilecode = /^\d{1,4}$/, intmob = /^\d{5,15}$/, mob = /^\d{7,15}$/;

                if($('#categories_form_contactname').length > 0) {
                    if($('#categories_form_contactname').val() == '')
                    {
                        $('#name').text("Please enter your full name");
                        $('div.name').css("border","2px solid red");
                        scrollBox($('div.name'));return false;
                        validationFlag = false;
                        return false;
                    }
                    else if(!name.test($('#categories_form_contactname').val()))
                    {
                        $('#name').text("Please enter a valid full name");
                        $('div.name').css("border","2px solid red");
                        scrollBox($('div.name'));return false;
                        validationFlag = false;
                        return false;
                    }
                    $('div.name').removeAttr('style');$('#name').empty();
                }
                if($('#categories_form_email').length >0) {
                    if($('#categories_form_email').val() == '')
                    {
                        $('#email').text("Please enter your Email-Id");
                        $('div.email').css("border","2px solid red");
                        scrollBox($('div.email'));return false;
                        validationFlag = false;
                        return false;
                    }
                    else if(!email.test($('#categories_form_email').val()))
                    {
                        $('#email').text("Please enter a valid Email-Id");
                        $('div.email').css("border","2px solid red");
                        scrollBox($('div.email'));return false;
                        validationFlag = false;
                        return false;
                    }
                    $('div.email').removeAttr('style');$('#email').empty();
                }
                if(locationtype.length == 0)
                {
                    $('#location').text("Please select any location");
                    $('div.location').css("border","2px solid red");
                    scrollBox($('div.location'));
                    validationFlag = false;
                    return false;
                } else {
                    $('div.location').removeAttr('style');$('#location').empty();
                    if(locationtype.attr('id') == "categories_form_locationtype_0") {
                        if($('#categories_form_city').val() == '')
                        {
                            $('#acc_city').html("Please select any city");
                            $('#domCity').css("border","2px solid red");
                            scrollBox($('#domCity'));return false;
                        }
                        else if($('#categories_form_location').val() == '')
                        {
                            $('div#domCity').removeAttr('style');$('#acc_city').empty();

                            $('#acc_area').html("Please enter the location");
                            $('#domArea').css("border","2px solid red");
                            scrollBox($('#domArea'));return false;
                        }
                        else if($('#uae_mobileno').length && ($('#categories_form_mobilecode').val() == ''))
                        {
                            $('div#domArea').removeAttr('style');$('#acc_area').empty();

                            $('#acc_mobile').html("Please select a mobile code");
                            $('#uae_mobileno').css("border","2px solid red");
                            scrollBox($('#uae_mobileno'));return false;
                        }
                        else if($('#uae_mobileno').length && ($('categories_form_mobile').val() == ''))
                        {

                            $('#acc_mobile').html("Please enter a valid mobile number");
                            $('#uae_mobileno').css("border","2px solid red");
                            scrollBox($('#uae_mobileno'));return false;
                        }
                        else if($('#uae_mobileno').length && (!mob.test($('#categories_form_mobile').val())))
                        {
                            $('#acc_mobile').html("Please enter a valid mobile number");
                            $('#uae_mobileno').css("border","2px solid red");
                            scrollBox($('#uae_mobileno'));return false;
                        }

                        $('div#uae_mobileno').removeAttr('style');$('#acc_mobile').empty();

                    } else {
                        if($('#categories_form_country').val() == '')
                        {
                            $('#acc_intcountry').html("Please enter the country name");
                            $('#intCountry').css("border","2px solid red");
                            scrollBox($('#intCountry'));return false;
                        }
                        else if(!countryname.test($('#categories_form_country').val()))
                        {
                            $('#acc_intcountry').html("Please enter a valid country");
                            $('#intCountry').css("border","2px solid red");
                            scrollBox($('#intCountry'));return false;
                        }
                        else if($('#categories_form_cityint').val() == '')
                        {
                            $('#acc_intcity').html("Please enter the city name");
                            $('#intCity').css("border","2px solid red");
                            scrollBox($('#intCity'));return false;
                        }
                        else if(!countryname.test($('#categories_form_cityint').val()))
                        {
                            $('#acc_intcity').html("Please enter a valid city");
                            $('#intCity').css("border","2px solid red");
                            scrollBox($('#intCity'));return false;
                        }
                        //mobile number validation
                        else if($('#int_mobileno').length && ($('#categories_form_mobilecodeint').val() == ''))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile code");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        else if($('#int_mobileno').length && ($('categories_form_mobileint').val() == ''))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile number");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        else if($('#int_mobileno').length && (!mobilecode.test($('#categories_form_mobilecodeint').val())))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile code");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        else if($('#int_mobileno').length && (!intmob.test($('#categories_form_mobileint').val())))
                        {
                            $('#acc_intmobile').html("Please enter a valid mobile number");
                            $('#int_mobileno').css("border","2px solid red");
                            scrollBox($('#int_mobileno'));return false;
                        }
                        $('div#intCity,div#intCountry,div#int_mobileno').removeAttr('style');$('#acc_intcountry,#acc_intcity,#acc_intmobile').empty();


                    }
                }

                /*if($("#recaptcha_response_field").length) {
                    if($("#recaptcha_response_field").val() == '') {
                        $('div#recaptcha_widget_div').css("border","2px solid red");
                        return false;
                    }
                    $('div#recaptcha_widget_div').removeAttr('style');
                }*/
                validationFlag = true;
            }

            $("div#step"+$step).hide();
            $step++;
            $("div#step"+$step).show();
            scrollBox($("div#step"+$step));
            return true;
        }
    });

    $("div.goback").click(function(){
        var gobackStep = $(this).children("a:first").attr('id'), $step = gobackStep.match(/\d+/);
        $("div#step"+$step).show();
        scrollBox($("div#step"+$step));
        if($step == 1) {
            $('div#handymanBlock,div#plumbingBlock,div#appliancesBlock,div#electricalBlock,div#paintingBlock,div#carpentryBlock,div#joineryBlock,div#flooringBlock,div#flooringtypeBlock').hide();
        }
        $step++;
        $("div#step"+$step).hide();
    });

    $('select[name="sorting"]').on('change', function() {
      $('input#filterform_sort_type').val($(this).val());
      $('form[name="filterform"]').submit();
    });

    $('span[id^="contactdetailsdiv"]').hide();
    $('input[id^="contactdetails-"]').click(function(){
            var btid = $(this).attr('id');
            var arr = btid.split('-');


        $('#contactdetails-'+arr[1]).hide();
        $('#contactdetailsdiv'+arr[1]).show();

    });
    $.fn.stars = function() {
        return $(this).each(function() {
            // Get the value
            var val = parseFloat($(this).html());
            // Make sure that the value is in 0 - 5 range, multiply to get width
            var size = Math.max(0, (Math.min(5, val))) * 16;

            // Create stars holder
            var $span = $('<span />').width(size);

            // Replace the numerical value with stars
            $(this).html($span);
        });
    }
    $('span.stars').stars();

    /**/




});

$(document).ready(function(){
	
	$('.int_design').blur(function(){
        var design = $('[name="categories_form[subcategory][]"]:checked').length;
		if(design != 0)
		{
            $('#int_design').html("");
            $('#services').css("border","2px solid #89ceeb");
        }
    });
    $('.int_property').blur(function(){
        var property = $('[name="categories_form[property_type]"]:checked').length;
		if(property != 0)
		{
            $('#int_property').html("");
            $('#property').css("border","2px solid #89ceeb");
        }
    });
    $('.int_dimension').blur(function(){
       var dimension = $('[name="categories_form[dimensions]"]:checked').length;
		if(dimension != 0)
		{
            $('#int_dimension').html("");
            $('#dimension').css("border","2px solid #89ceeb");
        }
    });
	$('.name').blur(function(){
        if($('.name').val() != '')
		{
			$('#int_name').html("");
            $('#name').css("border","2px solid #89ceeb");
		}
    });

    $('.email_id').blur(function(){
        if($('.email_id').val() != '')
		{
			$('#int_email').html("");
            $('#email_id').css("border","2px solid #89ceeb");
		}
    });
    
      $('.int_location').blur(function(){
      var location = $('[name="categories_form[locationtype]"]:checked').length;
         if(location != 0)
		{
			$('#int_location').html("");
            $('#location').css("border","2px solid #89ceeb");
		}
    });
	
	$('#categories_form_postJob').click(function(){
		var design = $('[name="categories_form[subcategory][]"]:checked').length;
		var property = $('[name="categories_form[property_type]"]:checked').length;
		var dimension = $('[name="categories_form[dimensions]"]:checked').length;
		var name =  /^[a-zA-Z ]*$/;
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		var location = $('[name="categories_form[locationtype]"]:checked').length;
		
		if(design == 0)
		{
			$('#int_design').text("Please select any interior design and fit out services");
			$('#services').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#services").offset().top},
				'slow');
			//$.scrollTo($('#services'), 500);
		}
		else if(property == 0)
		{
			$('#int_property').text("Please select any value");
			$('#property').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#property").offset().top},
				'slow');
			//$.scrollTo($('#property'), 500);
		}
		else if($('#categories_form_property_type_4').is(':checked') && ($('#categories_form_property_type_other').val() == ''))
		{
			$('#int_property').text("Please enter other data");
			$('#property').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#property").offset().top},
				'slow');
			//$.scrollTo($('#property'), 500);
		}
		else if(dimension == 0)
		{
			$('#int_dimension').text("Please select any dimension");
			$('#dimension').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#dimension").offset().top},
				'slow');
			//$.scrollTo($('#dimension'), 500);
		}
		else if($('#name').length && ($('#categories_form_contactname').val() == ''))
		{
			$('#int_name').text("Please enter your full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#name').length && (!name.test($('#categories_form_contactname').val())))
		{
			$('#int_name').text("Please enter a valid full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#email_id').length && ($('#categories_form_email').val() == ''))
		{
			$('#int_email').text("Please enter your Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if($('#email_id').length && (!email.test($('#categories_form_email').val())))
		{
			$('#int_email').text("Please enter a valid Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if(location == 0) 
		{
			$('#int_location').text("Please select any location");
			$('#location').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#location").offset().top},
				'slow');
			//$.scrollTo($('#location'), 500);
		}
	});
	
	
	
	$('#categories_form_property_type_other').click(function(){
		$('#categories_form_property_type_4').prop('checked',true);
		});
});

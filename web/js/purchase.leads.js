function arraySearch(arr,val) {
    for (var i=0; i<arr.length; i++)
        if (arr[i] === val)
            return i;
    return -1;
}
$(document).ready(function(){
    $('table').on('click','tr a.delete-package',function(e){
        e.preventDefault();
        $('table tr#nocat').remove();
        var closestTr = $(this).closest('tr'), rowCount=0, selCatIndex='', closestTrID= '', closestTrIDArray = new Array();
        closestTrID      = closestTr[0].id;
        closestTrID      = closestTrID.toString();
        closestTrIDArray = closestTrID.split(" - ", 2);
        closestTrID      = parseInt(closestTrIDArray[1],10);
        selCatIndex      = selectedCats.indexOf(closestTrID);
        // delete selectedCats[selCatIndex];
        if (selCatIndex > -1) {
            selectedCats.splice(selCatIndex, 1);
        }
        $(this).closest('tr').remove();
        rowCount = $('table tr').length-1;
        if(rowCount == 0){
            $('#selectedCatTable tbody').append('<tr id="nocat"><td colspan="3">No Service Category Selected.</td></tr>');
        }
        $.post(redeemUrl,
        {
            position : closestTrID,
            purchaseID : purchaseID,
            reqType : 'del'
        },
        function(data,status){
          // alert("Data: " + data + "\nStatus: " + status);
          console.log(status);
        });
    });
    $('.btn-success').click(function () {
        var catCount = selectedCats.length;
        if(catCount == 0)
            return alert('Please select at least one category to proceed.');
        else
            window.location = planUrl;
    });
    $('.submit-pack').click(function () {
        var clickedID = $( this ).attr("id"), searchVal = '', position=-1, existFlag = -1;
        if(clickedID == 'autoCompleteAdd')
            searchVal = $( "#parentCat" ).val();
        else
            searchVal = $( "#selectBoxParentCat" ).val();
        searchval = $.trim(searchval);
        if(searchval == '') {
            alert("Please select a service category from the list and add.");
            return false;
        }
        position = jQuery.inArray( searchVal, availableCat );

        $('table tr#nocat').remove();
        var rowCount = $('table tr').length-1;
        if(position != -1) {
            existFlag = jQuery.inArray( position, selectedCats );
            if(existFlag == -1) {
                selectedCats.push(position);
                if(rowCount != 0){
                    $('#selectedCatTable tr:last').after('<tr id="cat - '+position+'"><td>'+searchVal+'</td><td>'+availableNumbers[position]+'</td><td><a class="delete-package">Delete</a></td></tr>');
                } else {
                    $('#selectedCatTable tbody').append('<tr id="cat - '+position+'"><td>'+searchVal+'</td><td>'+availableNumbers[position]+'</td><td><a class="delete-package">Delete</a></td></tr>');
                }
                $.post(redeemUrl,
                {
                    position : position,
                    purchaseID : purchaseID,
                    reqType : 'add'
                },
                function(data,status){
                  console.log(status);
                });
            } else {
                alert("Note : Service Category Already Selected !");
                rowCount = $('table tr').length-1;
                if(rowCount == 0)
                    $('#selectedCatTable tbody').append('<tr id="nocat"><td colspan="3">No Service Category Selected.</td></tr>');
            }
            $( "#parentCat" ).val('');$('#selectBoxParentCat').prop('selectedIndex',0);
        } else {
            alert("Note : Service Category Already Selected !");
            rowCount = $('table tr').length-1;
            if(rowCount == 0)
                $('#selectedCatTable tbody').append('<tr id="nocat"><td colspan="3">No Service Category Selected.</td></tr>');
        }
    });
    $( "#parentCat" ).autocomplete({
        source: availableCat,
        change: function(event,ui)
        {
            if (ui.item==null)
            {
                $( this ).val('');
                $( this ).focus();
            }
        }
    });
});
$(document).ready(function(){
	
	 $('.sservice').blur(function(){
       var service = $('[name="categories_form[subcategory][]"]:checked').length;
        if(service != 0)
        {
            $('#land_service').html("");
            $('#lservice').css("border","2px solid #89ceeb");
        }
    });
    $('.kindofproperty').blur(function(){
       var property = $('[name="categories_form[property_type][]"]:checked').length;
        if(property != 0)
        {
            $('#land_property').html("");
            $('#lproperty').css("border","2px solid #89ceeb");
        }
    });
    $('.lawn_garden').blur(function(){
       var lawn_garden = $('[name="categories_form[service_list][]"]:checked').length;
       var nolawn_garden = $('[name="categories_form[service_none]"]:checked').length;
        if((lawn_garden != 0) || (nolawn_garden != 0))
        {
            $('#land_need').html("");
            $('#lneed').css("border","2px solid #89ceeb");
        }
    });
    $('.land_need').blur(function(){
      var need = $('[name="categories_form[landscape_service][]"]:checked').length;
      var noneed = $('[name="categories_form[landscape_none]"]:checked').length;
        if((need != 0) || (noneed != 0))
        {
            $('#land_kindofprop').html("");
            $('#lfeature').css("border","2px solid #89ceeb");
        }
    });
	$('.land_irrig').blur(function(){
		var irrigation = $('[name="categories_form[irrigation][]"]:checked').length;
		var noirrigation = $('[name="categories_form[irrigation_none]"]:checked').length;
        if((irrigation != 0) && (noirrigation != 0))
        {
            $('#land_irrigation').html("");
            $('#lirrigation').css("border","2px solid #89ceeb");
        }
        else if($('#categories_form_irrigation_4').is(':checked') && ($('#categories_form_irrigation_other').val() != 0))
        {
			$('#land_irrigation').html("");
            $('#lirrigation').css("border","2px solid #89ceeb");
		}
    });
    	$('.dimension').blur(function(){
		var dimension = $('[name="categories_form[dimensions]"]:checked').length;
        if(dimension != 0)
        {
            $('#land_dimension').html("");
            $('#ldimension').css("border","2px solid #89ceeb");
        }
    });
    $('.name').blur(function(){
        if($('.name').val() != '')
		{
			$('#land_name').html("");
            $('#name').css("border","2px solid #89ceeb");
		}
    });

    $('.email_id').blur(function(){
        if($('.email_id').val() != '')
		{
			$('#land_email').html("");
            $('#email_id').css("border","2px solid #89ceeb");
		}
    });
    
     $('.land_location').blur(function(){
      var location = $('[name="categories_form[locationtype]"]:checked').length;
         if(location != 0)
		{
			$('#land_loc').html("");
            $('#location').css("border","2px solid #89ceeb");
		}
    });
    
	$('#categories_form_postJob').click(function(){
		var service = $('[name="categories_form[subcategory][]"]:checked').length;
		/*var kindofservice = $('[name="categories_form[service_type][]"]:checked').length;*/
		var property = $('[name="categories_form[property_type][]"]:checked').length;
		var need = $('[name="categories_form[landscape_service][]"]:checked').length;
		var lawn_garden = $('[name="categories_form[service_list][]"]:checked').length;
		var nolawn_garden = $('[name="categories_form[service_none]"]:checked').length;
		var noneed = $('[name="categories_form[landscape_none]"]:checked').length;
		var kindofprop = $('[name="categories_form[property_type][]"]:checked').length;
		/*var custom = $('[name="categories_form[custom_landscape]"]:checked').length;*/
		var irrigation = $('[name="categories_form[irrigation][]"]:checked').length;
		var noirrigation = $('[name="categories_form[irrigation_none]"]:checked').length;
		var dimension = $('[name="categories_form[dimensions]"]:checked').length;
		var name =  /^[a-zA-Z ]*$/;
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		var location = $('[name="categories_form[locationtype]"]:checked').length;
		

		if(service == 0)
		{
			$('#land_service').text("Please select any services");
			$('#lservice').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lservice").offset().top},
				'slow');
			//$.scrollTo($('#lservice'), 500);
		}
		else if(property == 0)
		{
			$('#land_property').text("Please select any value");
			$('#lproperty').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lproperty").offset().top},
				'slow');
			//$.scrollTo($('#lproperty'), 500);
		}
		else if($('#categories_form_property_type_3').is(':checked') && ($('#categories_form_property_type_other').val() == ''))
		{
			$('#land_property').text("Please enter other data");
			$('#lproperty').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lproperty").offset().top},
				'slow');
			//$.scrollTo($('#lproperty'), 500);
		}
		else if((lawn_garden == 0) && (nolawn_garden == 0))
		{  
			$('#land_need').text("Please select any value");
			$('#lneed').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lneed").offset().top},
				'slow');
			//$.scrollTo($('#lneed'), 500);
		}
		else if($('#categories_form_service_list_7').is(':checked') && ($('#categories_form_service_list_other').val() == ''))
		{
			$('#land_need').text("Please enter other data");
			$('#lneed').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lneed").offset().top},
				'slow');
			//$.scrollTo($('#lneed'), 500);
		}
		else if((need == 0) && (noneed == 0))
		{
			$('#land_kindofprop').text("Please select any value");
			$('#lfeature').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lfeature").offset().top},
				'slow');
			//$.scrollTo($('#lfeature'), 500);
		}
		else if($('#categories_form_service_list_7').is(':checked') && ($('#categories_form_service_list_other').val() == ''))
		{
			$('#lfeature').text("Please enter other data");
			$('#land_kindofprop').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#land_kindofprop").offset().top},
				'slow');
			//$.scrollTo($('#land_kindofprop'), 500);
		}
		else if(kindofprop == 0)
		{
			$('#land_kindofprop').text("Please select custom features");
			$('#lfeature').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lfeature").offset().top},
				'slow');
			//$.scrollTo($('#lfeature'), 500);
		}
		else if($('#categories_form_landscape_service_5').is(':checked') && ($('#categories_form_landscape_other').val() == ''))
		{
			$('#land_kindofprop').text("Please enter other data");
			$('#lfeature').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lfeature").offset().top},
				'slow');
			//$.scrollTo($('#lfeature'), 500);
		}

		/*if(custom == 0)
			$('#land_custom').text("Please select value");
		else
			$('#land_custom').text("");
		if($('#categories_form_custom_landscape_4').is(':checked'))
		{
			if($('#categories_form_custom_other').val() == '')
				$('#land_custom').text("Please enter other data");
			else
				$('#land_custom').text("");
		}*/
		else if((irrigation == 0) && (noirrigation == 0))
		{
			$('#land_irrigation').text("Please select any irrigation services");
			$('#lirrigation').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lirrigation").offset().top},
				'slow');
			//$.scrollTo($('#lirrigation'), 500);
		}
		else if($('#categories_form_irrigation_4').is(':checked') && ($('#categories_form_irrigation_other').val() == ''))
		{
			$('#land_irrigation').text("Please enter other data");
			$('#lirrigation').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#lirrigation").offset().top},
				'slow');
			//$.scrollTo($('#lirrigation'), 500);
		}
		else if(dimension == 0)
		{
			$('#land_dimension').text("Please select any dimension");
			$('#ldimension').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#ldimension").offset().top},
				'slow');
			//$.scrollTo($('#ldimension'), 500);
		}
		else if($('#name').length && ($('#categories_form_contactname').val() == ''))
		{
				$('#land_name').text("Please enter your full name");
				$('#name').css("border","2px solid red");
				$('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
				//$.scrollTo($('#name'), 500);
		}
		else if($('#name').length && !name.test($('#categories_form_contactname').val()))
		{
			$('#land_name').text("Please enter a valid full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#email_id').length && ($('#categories_form_email').val() == ''))
		{
			$('#land_email').text("Please enter your Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if($('#email_id').length && (!email.test($('#categories_form_email').val())))
		{
			$('#land_email').text("Please enter a valid Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if(location == 0) 
		{
			$('#land_loc').text("Please select any location");
			$('#location').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#location").offset().top},
				'slow');
			//$.scrollTo($('#location'), 500);		
		}
		
	});
	$('#categories_form_property_type_other').click(function(){
		$('#categories_form_property_type_3').prop('checked',true);
		});
	$('#categories_form_service_list_other').click(function(){
		$('#categories_form_service_list_7').prop('checked',true);
		$('.lawn_garden_other').prop('checked',false);
	});
	$('#categories_form_landscape_other').click(function(){
		$('#categories_form_landscape_service_5').prop('checked',true);
		$('.land_otherneed').prop('checked',false);
	});
	$('#categories_form_custom_other').click(function(){
		$('#categories_form_custom_landscape_4').prop('checked',true);
		});
	$('#categories_form_irrigation_other').click(function(){
		$('#categories_form_irrigation_4').prop('checked',true);
		$('.land_oirrigation').prop('checked',false);
	});

	$('.lawn_garden').click(function(){
		$('.lawn_garden_other').prop('checked',false);
	});

	$('.lawn_garden_other').click(function(){
		$('.lawn_garden').prop('checked',false);
	});
	$('.land_oirrigation').click(function(){
		$('.land_irrig').prop('checked',false);
	});
	$('.land_irrig').click(function(){
		$('.land_oirrigation').prop('checked',false);
		});
	$('.land_need').click(function(){
		$('.land_otherneed').prop('checked',false);
		});
	$('.land_otherneed').click(function(){
		$('.land_need').prop('checked',false);
		});

});

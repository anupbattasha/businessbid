$(document).ready(function(){
	
	$('.signage').blur(function(){
       var service = $('[name="categories_form[subcategory][]"]:checked').length;
        if(service != 0)
        {
            $('#sign_service').html("");
            $('#signage_service').css("border","2px solid #89ceeb");
        }
    });
    $('.swidth').blur(function(){
       var width = $('[name="categories_form[appox_width]"]:checked').length;
        if(width != 0)
        {
            $('#sign_width').html("");
            $('#width').css("border","2px solid #89ceeb");
        }
    });
	$('.sheight').blur(function(){
      var height = $('[name="categories_form[appox_height]"]:checked').length;
        if(height != 0)
        {
            $('#sign_heigth').html("");
            $('#height').css("border","2px solid #89ceeb");
        }
    });
    $('.sinstall').blur(function(){
      var installation = $('[name="categories_form[installation]"]:checked').length;
        if(installation != 0)
        {
            $('#sign_installation').html("");
            $('#installation').css("border","2px solid #89ceeb");
        }
    });
	$('.name').blur(function(){
        if($('.name').val() != '')
		{
			$('#sign_name').html("");
            $('#name').css("border","2px solid #89ceeb");
		}
    });

    $('.email_id').blur(function(){
        if($('.email_id').val() != '')
		{
			$('#sign_email').html("");
            $('#email_id').css("border","2px solid #89ceeb");
		}
    });
	
	$('.location').blur(function(){
		var location = $('[name="categories_form[locationtype]"]:checked').length;
        if(location != 0)
		{
			$('#sign_loc').html("");
            $('#location').css("border","2px solid #89ceeb");
		}
    });
	
	$('#categories_form_postJob').click(function(e){
		var service = $('[name="categories_form[subcategory][]"]:checked').length;
		var width = $('[name="categories_form[appox_width]"]:checked').length;
		var height = $('[name="categories_form[appox_height]"]:checked').length;
		var installation = $('[name="categories_form[installation]"]:checked').length;
		var name =  /^[a-zA-Z ]*$/;
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		var location = $('[name="categories_form[locationtype]"]:checked').length;
		
		if(service == 0)
		{
			$('#sign_service').text("Please select any services");
			$('#signage_service').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#signage_service").offset().top},
				'slow');
			//$.scrollTo($('#signage_service'), 500);
		} 
		else if(width == 0)
		{
			$('#sign_width').text("Please select the width");
			$('#width').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#width").offset().top},
				'slow');
			//$.scrollTo($('#width'), 500);
		}
		else if(height == 0)
		{	
			$('#sign_heigth').text("Please select the heigth");
			$('#height').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#height").offset().top},
				'slow');
			//$.scrollTo($('#height'), 500);
		}
		else if(installation == 0)
		{
			$('#sign_installation').text("Please select any value");
			$('#installation').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#installation").offset().top},
				'slow');
			//$.scrollTo($('#installation'), 500);
		}
		else if($('#name').length && (($('#categories_form_contactname').val() == '') || (!name.test($('#categories_form_contactname').val()))))
		{
				$('#sign_name').text("Please enter a valid full name");
				$('#name').css("border","2px solid red");
				 $('html,body').animate({
					scrollTop: $("#name").offset().top},
					'slow');
				//$.scrollTo($('#name'), 500);
				//return false;  
			
		}   
	
		else if($('#email_id').length && ($('#categories_form_email').val() == ''))
		{
				$('#sign_email').text("Please enter your Email-Id");
					$('#email_id').css("border","2px solid red");
					 $('html,body').animate({
					scrollTop: $("#email_id").offset().top},
					'slow');
					//$.scrollTo($('#email'), 500); 
					//return false;

		}
		else if($('#email_id').length && (!email.test($('#categories_form_email').val())))
		{
				$('#sign_email').text("Please enter your Email-Id");
					$('#email_id').css("border","2px solid red");
					 $('html,body').animate({
					scrollTop: $("#email_id").offset().top},
					'slow');
					//$.scrollTo($('#email'), 500); 
					//return false;

		} 
		else if(location == 0) 
		{
			$('#sign_loc').text("Please select any location");
			$('#location').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#location").offset().top},
				'slow');
			//$.scrollTo($('#location'), 500);
		} 
		
	});
});

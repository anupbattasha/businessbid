function albumValidate()
{
    var isValid = true;
    var albumName = $('#form_albumname');
    if(albumName.val() == '') {
        albumName.css('border', '3px solid red');
        albumName.focus();
        isValid = false;
    } else {
        albumName.css('border', '3px solid green');
    }
    return isValid;
}
function productValidate() {
    var e = !0;
    return $('input[name="product[]"]').each(function() {
        var i = $(this).val();
        console.log(i), "" == i && (alert("All product fields should be filled."), $(this).focus(),
        e = !1);
    }), e;
}

function certificateValidate() {
    var e = !0;
    return $('input[name="certification[]"]').each(function() {
        var i = $(this).val();
        "" == i ? ($(this).css("border", "3px solid red"), $(this).focus(), e = !1) : $(this).css("border", "3px solid green");
    });
}

function deleteEntity(e, i, t) {
    if ("certification" == e) {
        var r = confirm("Are You sure to delete the Certification");
        if (r) {
            var n = certificationDelUrl.replace("cid", i);
            return n = n.replace("vid", t), window.open(n, "_self"), !0;
        }
        return !1;
    }
    if ("product" == e) {
        var r = confirm("Are You sure to delete the Product");
        if (r) {
            var n = productDelUrl.replace("pid", i);
            return n = n.replace("vid", t), window.open(n, "_self"), !0;
        }
        return !1;
    }
    if ("license" == e) {
        var r = confirm("Are You sure to delete the License");
        if (r) {
            var n = licenseDelUrl.replace("eid", i);
            return n = n.replace("vid", t), window.open(n, "_self"), !0;
        }
        return !1;
    }
    if("album"== e)
    {
        var r = confirm("Are You sure to delete the Album");
        if (r) {
            var n = albumDelUrl.replace("eid", i);
            return n = n.replace("vid", t), window.open(n, "_self"), !0;
        }
        return !1;
    }
    if("category"== e)
    {
        var r = confirm("Are You sure to delete the Service Category");
        if (r) {
            var n = catDelUrl.replace("eid", i);
            return n = n.replace("vid", t), window.open(n, "_self"), !0;
        }
        return !1;
    }
}

$(document).ready(function() {
    var max_fields_subcat      = 5; //maximum input boxes allowed
    var wrapper_subcategory         = $(".subcategories"); //Fields wrapper
    var add_button_subcategory      = $("#addform1"); //Add button ID

    var x_subcat = 1; //initlal text box count
    $(add_button_subcategory).click(function(e){ //on add input button click
        e.preventDefault();
        if(x_subcat < max_fields_subcat){ //max input box allowed
            x_subcat++; //text box increment
            $("#moreproducts1").append('<div class="morecertificates"><input type="text" name="cat[]" placeholder="Enter your category"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });

    $(wrapper_subcategory).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x_subcat--;
    });
    $('#form_albumimages').change(function(e,data){
        var inp = $(this).get(0);
        var uploadErrors = [];
        var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
        for (var i = 0; i < inp.files.length; ++i) {
          var name = inp.files.item(i).name;
          var fileType = inp.files.item(i).type;
          var fileSize = inp.files.item(i).size;
            if(!acceptFileTypes.test(fileType)) {
                uploadErrors.push('File No.'+ (i+1) +' is not an accepted file type');
            }
            if(fileSize > 5000000) {
                uploadErrors.push('Filesize is too big');
            }
          // console.log("here is a file name: " + name);
        }
        if(uploadErrors.length > 0) {
            alert(uploadErrors.join("\n"));
            $(this).val('');
            return false;
        }
        return true;
    });
    var e = 10, i = $(".products"), t = $(".add_products_button"), r = 1;
    $(t).click(function(i) {
        i.preventDefault(), e > r && (r++, $("#moreproducts").append('<div class="file-upload-wrapper"><input type="text" name="product[]"/><a href="#" class="remove_field">Remove</a></div>'));
    }), $(i).on("click", ".remove_field", function(e) {
        e.preventDefault(), $(this).parent("div").remove(), r--;
    });
    var n = 5, c = $(".certificates"), o = $(".add_certificate_button"), a = 1;
    $(o).click(function(e) {
        e.preventDefault(), n > a && (a++, $("#morecertificates").append('<div class="morecertificates"><input type="text" name="certification[]"/><a href="#" class="remove_field">Remove</a></div>'));
    }), $(c).on("click", ".remove_field", function(e) {
        e.preventDefault(), $(this).parent("div").remove(), a--;
    }), $("#datepicker3").datepicker({
        dateFormat: "dd-mm-yy",
        minDate: 0,
        changeMonth: !0,
        changeYear: !0,
        yearRange: "2014:2024"
    }), $("#changeimage").click(function() {
        $("#logo-form").show();
    });
});
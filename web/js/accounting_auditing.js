$(document).ready(function(){
    $('.acc_services').blur(function(){
        var service = $('[name="categories_form[subcategory][]"]:checked').length;
        if(service != 0)
        {
            $('#acc_services').html("");
            $('#services').css("border","2px solid #89ceeb");
        }
    });
    $('.acc_software').blur(function(){
        var software = $('[name="categories_form[software][]"]:checked').length;
        if(software != 0)
        {
            $('#acc_software').html("");
            $('#software').css("border","2px solid #89ceeb");
        }
        else if($('#categories_form_software_4').is(':checked') &&  ($('#categories_form_software_other').val() != ''))
	   {
            $('#acc_software').html("");
            $('#software').css("border","2px solid #89ceeb");
	   }
    });

    $('.acc_type').blur(function(){
        var type = $('[name="categories_form[accounting]"]:checked').length;
        if(type != 0)
		{
			$('#acc_company').html("");
            $('#account').css("border","2px solid #89ceeb");
		}
    });

    $('.acc_need').blur(function(){
        var required = $('[name="categories_form[often]"]:checked').length;
        if(required != 0)
		{
			$('#acc_req').html("");
            $('#need').css("border","2px solid #89ceeb");
		}
    });
    
     $('.acc_revenue').blur(function(){
       var revenue = $('[name="categories_form[turnover]"]:checked').length;
        if(revenue != 0)
		{
			$('#acc_revenue').html("");
            $('#revenue').css("border","2px solid #89ceeb");
		}
    });

    $('.name').blur(function(){
        if($('.name').val() != '')
		{
			$('#acc_name').html("");
            $('#name').css("border","2px solid #89ceeb");
		}
    });

    $('.email_id').blur(function(){
        if($('.email_id').val() != '')
		{
			$('#acc_email').html("");
            $('#email_id').css("border","2px solid #89ceeb");
		}
    });
    $('.acc_location').blur(function(){
      var location = $('[name="categories_form[locationtype]"]:checked').length;
         if(location != 0)
		{
			$('#acc_loc').html("");
            $('#location').css("border","2px solid #89ceeb");
		}
    });
    
    $('#categories_form_software_4').blur(function(){
		if($('#categories_form_software_4').is(':checked') && $('#categories_form_software_other').val() != '')
		{
			 $('#acc_software').html("");
            $('#software').css("border","2px solid #89ceeb");
		}
		});
		
	$('#categories_form_accounting_2').blur(function(){
		if($('#categories_form_accounting_2').is(':checked') && $('#categories_form_accounting_other').val() != '')
		{
			 $('#acc_company').html("");
            $('#account').css("border","2px solid #89ceeb");
		}
		});

	$('#categories_form_postJob').click(function(){
		var service = $('[name="categories_form[subcategory][]"]:checked').length;
		var software = $('[name="categories_form[software][]"]:checked').length;
		var type = $('[name="categories_form[accounting]"]:checked').length;
		var required = $('[name="categories_form[often]"]:checked').length;
		var revenue = $('[name="categories_form[turnover]"]:checked').length;
		var location = $('[name="categories_form[locationtype]"]:checked').length;

		var name =  /^[a-zA-Z ]*$/;
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

		if(service == 0)
		{
			$('#acc_services').text("Please select any of the services");
            $('#services').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#services").offset().top},
				'slow');
			//$.scrollTo($('#services'), 500);
			//return false;
		}
		else if(software == 0)
		{
			$('#acc_software').text("Please select any of the software");
            $('#software').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#software").offset().top},
				'slow');
			//$.scrollTo($('#software'), 500);
			//return false;
		}
		else if($('#categories_form_software_4').is(':checked') && ($('#categories_form_software_other').val() == ''))
		{
			$('#acc_software').text("Please enter the other software");
			$('#software').css("border","2px solid red");
			 $('html,body').animate({
				scrollTop: $("#software").offset().top},
				'slow');
			//$.scrollTo($('#software'), 500);
		}
		else if(type == 0)
		{
			$('#acc_company').text("Please select any type of company");
            $('#account').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#account").offset().top},
				'slow');
			//$.scrollTo($('#account'), 500);
			//return false;
		}
		else if($('#categories_form_accounting_2').is(':checked') && ($('#categories_form_accounting_other').val() == ''))
		{
				$('#acc_company').text("Please enter the other type of company");
				$('#account').css("border","2px solid red");
				 $('html,body').animate({
				scrollTop: $("#account").offset().top},
				'slow');
				//$.scrollTo($('#account'), 500);
		}
		else if(required == 0)
		{
			$('#acc_req').text("Please select any type of service");
            $('#need').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#need").offset().top},
				'slow');
			//$.scrollTo($('#need'), 500);
			//return false;
		}
		else if($('#categories_form_often_2').is(':checked') && ($('#categories_form_often_other').val() == ''))
		{
			$('#acc_req').text("Please enter the other data");
			$('#need').css("border","2px solid red");
			 $('html,body').animate({
				scrollTop: $("#need").offset().top},
				'slow');
			//$.scrollTo($('#need'), 500);
		}
		else if(revenue == 0)
		{
			$('#acc_revenue').text("Please select any revenue");
            $('#revenue').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#revenue").offset().top},
				'slow');
			//$.scrollTo($('#revenue'), 500);
			//return false;
		}
		else if($('#name').length && ($('#categories_form_contactname').val() == ''))
		{
				$('#acc_name').text("Please enter your full name");
                $('#name').css("border","2px solid red");
                 $('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
				//$.scrollTo($('#name'), 500);
				//return false;
		}
		else if($('#name').length && (!name.test($('#categories_form_contactname').val())))
		{
			$('#acc_name').text("Please enter a valid full name");
			$('#name').css("border","2px solid red");
			 $('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
			//$.scrollTo($('#name'), 500);
			//return false;
		}
	
		else if($('#email_id').length && ($('#categories_form_email').val() == ''))
		{
			$('#acc_email').text("Please enter your Email-Id");
                $('#email_id').css("border","2px solid red");
                 $('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
				//$.scrollTo($('#email'), 500);
				//return false;
		}
		else if($('#email_id').length && (!email.test($('#categories_form_email').val())))
		{
			$('#acc_email').text("Please enter a valid Email-Id");
			$('#email_id').css("border","2px solid red");
			 $('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
			//return false;
		}
        
        else if(location == 0)
		{
			$('#acc_loc').html("Please select any location");
            $('#location').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#location").offset().top},
				'slow');
			//$.scrollTo($('#location'), 500);
			//return false;
		}

		
		
	});
	$('#categories_form_software_other').click(function(){
		$('#categories_form_software_4').prop('checked', true);
	});
	$('#categories_form_accounting_other').click(function(){
		$('#categories_form_accounting_2').prop('checked', true);
		$('#categories_form_accounting_1').prop('checked', false);
		$('#categories_form_accounting_0').prop('checked', false);
	});
	$('#categories_form_often_other').click(function(){
		$('#categories_form_often_2').prop('checked', true);
		$('#categories_form_often_1').prop('checked', false);
		$('#categories_form_often_0').prop('checked', false);
	});
});

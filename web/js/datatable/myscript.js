$(document).ready( function () {
				$.datepicker.regional[""].dateFormat = 'yy-mm-dd';
                $.datepicker.regional[""].changeMonth = true;
                $.datepicker.regional[""].changeYear = true;
                $.datepicker.setDefaults($.datepicker.regional['']);
				$('#vendorTicketList').dataTable({
					"bLengthChange": false,
			     	"iDisplayLength": 10,
			     	"language": {
			     		"search": "_INPUT_",
				        "searchPlaceholder": "Search records"
				    },
				    "sZeroRecords": "No Records to dispaly",
					"sPaginationType" : 'full_numbers',
					"sDom": '<"top"l>rt<"bottom"ip><"clear">'	
				}).columnFilter({aoColumns:[
								null,
								{ type:"date-range", sSelector: "#dateFilter" },
								{ sSelector: "#ticketidFilter" },
								{ sSelector: "#subjectFilter" },
								{ type:"select", values : ["Low", "Medium", "High"], sSelector: "#priorityFilter" },
								{ type:"select", values : ["Open", "Close", "Pending"], sSelector: "#statusFilter" }
								]}
							);
				
			} );
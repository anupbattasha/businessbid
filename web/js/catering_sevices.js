$(document).ready(function(){
    $('.catering').blur(function(){
       var service = $('[name="categories_form[subcategory][]"]:checked').length;
        if(service != 0)
        {
            $('#cat_services').html("");
            $('#catering').css("border","2px solid #89ceeb");
        }
    });

    $('#categories_form_event_planed').blur(function(){
		
       if($('#categories_form_event_planed').val() != '')
       {
          $('#cat_event').html(""); 
          $('#event').css("border","2px solid #89ceeb"); 
       }
    }); 
    
     
    $('#ui-datepicker-div').blur(function(){
		var t = $('#categories_form_event_planed').val();
		alert(t);
		});

    $('.live1').blur(function(){
       var live_station = $('[name="categories_form[live_station]"]:checked').length;
       
        if(live_station != 0)
        {
            $('#cat_livestation').html("");
            $('#live').css("border","2px solid #89ceeb");
        }
    });

    $('.meal').blur(function(){
      var meal = $('[name="categories_form[mealtype][]"]:checked').length;
        if(meal != 0)
        {
            $('#cat_meal').html("");
            $('#meal').css("border","2px solid #89ceeb");
        }
    });

    $('.beverages').blur(function(){
     var beverages = $('[name="categories_form[beverages]"]:checked').length;
        if(beverages != 0)
        {
            $('#cat_beverages').html("");
            $('#beverages').css("border","2px solid #89ceeb");
        }
    });

    $('.desert').blur(function(){
     var dessert = $('[name="categories_form[dessert]"]:checked').length;
        if(dessert != 0)
        {
            $('#cat_dessert').html("");
            $('#dessert').css("border","2px solid #89ceeb");
        }
    });

    $('.cat_support').blur(function(){
    	var support = $('[name="categories_form[support_staff][]"]:checked').length;
        var support_other = $('[name="categories_form[support_staff_other]"]:checked').length;
        if((support != 0) || (support_other != 0))
        {
			$('#staff').css("border","2px solid #89ceeb");
            $('#cat_support').html("");
            
        }
    });

     $('.cuisine').blur(function(){
     var cuisine = $('[name="categories_form[cuisine][]"]:checked').length;
        if(cuisine != 0)
        {
            $('#cat_cuisine').html("");
            $('#cuisine').css("border","2px solid #89ceeb");
        }
    });

     $('.guest').blur(function(){
    var guest = $('[name="categories_form[guests]"]:checked').length;
        if(guest != 0)
        {
            $('#cat_guest').html("");
            $('#guest').css("border","2px solid #89ceeb");
        }
    });

    $('#categories_form_contactname').blur(function(){
        if($('#categories_form_contactname').val() != '')
		{
			$('#cat_name').html("");
            $('#name').css("border","2px solid #89ceeb");
		}
    });

    $('#categories_form_email').blur(function(){
        if($('#categories_form_email').val() != '')
		{
			$('#cat_email').html("");
            $('#email_id').css("border","2px solid #89ceeb");
		}
    });
    $('.acc_location').blur(function(){
      var location = $('[name="categories_form[locationtype]"]:checked').length;  
         if(location != 0)
		{
			$('#cat_loc').html("");
            $('#location').css("border","2px solid #89ceeb");
		}
    });

	$('#categories_form_postJob').click(function(){
		var service = $('[name="categories_form[subcategory][]"]:checked').length;
		var dessert = $('[name="categories_form[dessert]"]:checked').length;
		var live_station = $('[name="categories_form[live_station]"]:checked').length;
		var meal = $('[name="categories_form[mealtype][]"]:checked').length;
		var beverages = $('[name="categories_form[beverages]"]:checked').length;
		var support = $('[name="categories_form[support_staff][]"]:checked').length;
		var support_other = $('[name="categories_form[support_staff_other]"]:checked').length;
		var cuisine = $('[name="categories_form[cuisine][]"]:checked').length;
		var guest = $('[name="categories_form[guests]"]:checked').length;
		var name =  /^[a-zA-Z ]*$/;
		var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
		var location = $('[name="categories_form[locationtype]"]:checked').length;

		if(service == 0)
        {
			$('#cat_services').text("Please select any of the services");
            $('#catering').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#catering").offset().top},
				'slow');
			//$.scrollTo($('#catering'), 500);
        }
		else if($('#categories_form_event_planed').val() == '')
        {
			$('#cat_event').text("Please enter the event planned");
             $('#event').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#event").offset().top},
				'slow');
			//$.scrollTo($('#event'), 500);
        }
        else if(($('#categories_form_event_planed').val() != '') && (live_station == 0))
        {
			$('#cat_event').text("");
             $('#event').css("border","2px solid #89ceeb");
             $('#live').css("border","2px solid red");
             $('#cat_livestation').text("Select any live station");
             $('html,body').animate({
				scrollTop: $("#live").offset().top},
				'slow');
			//$.scrollTo($('#event'), 500);
        }
	   
	    else if(live_station == 0)
        {
			$('#cat_livestation').text("Select any live station");
            $('#live').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#live").offset().top},
				'slow');
			//$.scrollTo($('#live'), 500);
        }
        
		else if(meal == 0)
        {
			$('#cat_meal').text("Select any meal");
            $('#meal').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#meal").offset().top},
				'slow');
			//$.scrollTo($('#meal'), 500);
        }
		else if($('#categories_form_mealtype_5').is(':checked') && ($('#categories_form_meal_other').val() == ''))
		{
			$('#cat_meal').text("Please enter some data for other meals");
			$('#meal').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#meal").offset().top},
				'slow');
			//$.scrollTo($('#meal'), 500);
		}
		else if(beverages == 0)
        {
			$('#cat_beverages').text("Please select any beverages");
            $('#beverages').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#beverages").offset().top},
				'slow');
			//$.scrollTo($('#beverages'), 500);
        }
         else if(dessert == 0)
        {
			$('#cat_dessert').text("Select any type");
             $('#dessert').css("border","2px solid red");
             $('html,body').animate({
				scrollTop: $("#dessert").offset().top},
				'slow');
			//$.scrollTo($('#dessert'), 500);
        }
	    else if((support == 0) && (support_other == 0))
        {
			$('#cat_support').text("Please select any support");
            $('#staff').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#staff").offset().top},
				'slow');
			//$.scrollTo($('#staff'), 500);
        }
	    else if(cuisine == 0)
        {
			$('#cat_cuisine').text("Please select any cuisine");
            $('#cuisine').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#cuisine").offset().top},
				'slow');
			//$.scrollTo($('#cuisine'), 500);
        }
		else if($('#categories_form_cuisine_4').is(':checked') && ($('#categories_form_cuisine_other').val() == ''))
		{
			$('#cat_cuisine').text("Please enter other cuisine");
			$('#cuisine').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#cuisine").offset().top},
				'slow');
			//$.scrollTo($('#cuisine'), 500);
		}
		else if(guest == 0)
        {
			$('#cat_guest').text("Please select the number of guest");
            $('#guest').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#guest").offset().top},
				'slow');
			//$.scrollTo($('#guest'), 500);
        }
      
		else if($('#name').length && ($('#categories_form_contactname').val() == ''))
		{
			$('#cat_name').text("Please enter your full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#name').length && (!name.test($('#categories_form_contactname').val())))
		{
			$('#cat_name').text("Please enter a valid full name");
			$('#name').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#name").offset().top},
				'slow');
			//$.scrollTo($('#name'), 500);
		}
		else if($('#email_id').length && ($('#categories_form_email').val() == ''))
		{
			$('#cat_email').text("Please enter your Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
		}
		else if($('#email_id').length && (!email.test($('#categories_form_email').val())))
		{
			$('#cat_email').text("Please enter a valid Email-Id");
			$('#email_id').css("border","2px solid red");
			$('html,body').animate({
				scrollTop: $("#email_id").offset().top},
				'slow');
			//$.scrollTo($('#email'), 500);
		}
		  else if(location == 0)
		{
			$('#cat_loc').text("Please select any location");
            $('#location').css("border","2px solid red");
            $('html,body').animate({
				scrollTop: $("#location").offset().top},
				'slow');
			//$.scrollTo($('#location'), 500);
        }
       
		//if((service == 0) || (dessert == 0) || (live_station == 0) || (meal == 0) || (beverages == 0) || (support == 0) && (support_other == 0) || (cuisine == 0) || (guest == 0))
			//return false;
		
	});
	$('#categories_form_meal_other').click(function(){
		$('#categories_form_mealtype_5').prop('checked', true);
	});
	$('.cat_support').click(function(){
		$('.cat_radsupport').prop('checked', false);
		$('#cat_support').text("");
	});
	$('.cat_radsupport').click(function(){
		$('.cat_support').prop('checked', false);
		$('#cat_support').text("");
	});
	$('#categories_form_cuisine_other').click(function(){
		$('#categories_form_cuisine_4').prop('checked', true);
	});
});

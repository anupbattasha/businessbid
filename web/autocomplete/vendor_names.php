<?php

// sleep( 3 );
// no term passed - just exit early with no response
if (empty($_GET['term'])) exit ;
$q = strtolower($_GET["term"]);
// remove slashes if they were magically added
if (get_magic_quotes_gpc()) $q = stripslashes($q);

$host     = 'localhost';
$dbname   = 'bizbids';
$username = 'bizbids';
$password = 'eY3@gV*094(Wl1$';
    try {
        $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $items = $conn->query("SELECT userid,contactname,bizname FROM bbids_account where profileid = 3 and contactname like '%$q%'")->fetchAll(PDO::FETCH_ASSOC);

        $result = array();
        foreach ($items as $value) {
            if (strpos(strtolower($value['contactname']), $q) !== false) {
                array_push($result, array("id"=>$value['userid'], "label"=>$value['contactname'].'-'.$value['bizname'], "value" => strip_tags($value['contactname'])));
            }
            if (count($result) > 11)
                break;
        }
        echo json_encode($result);
    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
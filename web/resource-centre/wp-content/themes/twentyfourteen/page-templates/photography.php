<?php
/**
 * Template Name: Photography
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('newhead'); ?>
        <!-- Main Content File here -->
        <div id="ac_header" class="photog_222">
            <div class="cleaning_headder_inner">
                <div class="headesr_slide_text">
                <?php echo get_post_meta($post->ID, 'photoghraphy_banner_text', true); ?>
            </div>
        </div>
		</div>
        <!-- Maintenance Service Area here -->
       <div class="main_text_area_left_new">
            <div id="ac_pc">
                <div class="left_img">
                <?php echo get_post_meta($post->ID, 'dubai_photography_img', true); ?>
				</div>
				<div class="left_text">
                <?php echo get_post_meta($post->ID, 'dubai_photography_text', true); ?>
                </div>
            </div>
        </div>

        <!-- Maintenance Step Area here -->
        <div id="common_quort_area">
            <div class="inner_quort" style="width:727px !important;">
            <?php echo get_post_meta($post->ID, 'photographer_3esay_step', true); ?>
        </div>
		</div>
        
        <!-- Mantenance Printing press service Area -->
       <div id="common_circel_area">
            <div class="inner_circel">
                <?php echo get_post_meta($post->ID, 'type_of_service_heading', true); ?>
				<div class="comm_three">
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'wedding', true); ?>
                </div>
                <div class="child_circle">
                      <?php echo get_post_meta($post->ID, 'corporate', true); ?>                 
                </div>
                <div class="child_circle">
                   <?php echo get_post_meta($post->ID, 'family', true); ?>
                </div>
				</div>
				<div class="comm_two">
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'events', true); ?>
                </div>
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'architectural', true); ?>
                </div>
				</div>
            </div>
        </div>        
        
        <!-- Mantenance Handyman service Area --> 
        <div class="main_text_area_right_new">
            <div id="hvac_system">
			<div class="inner_hvac">
                <div class="left_text" style="margin-top:0 !important;">
                   <?php echo get_post_meta($post->ID, 'wedding_photography_text', true); ?>
                </div>
                <div class="right_img">
                    <?php echo get_post_meta($post->ID, 'wedding_photography_img', true); ?>
                </div>
            </div>
        </div>       
        </div>
        <!-- Mantenance Electrical Area -->         
        <div class="main_text_area_left_new">
            <div id="split_pc">
                <div class="left_img">
                    <?php echo get_post_meta($post->ID, 'corporate_photography_img', true); ?>
                </div>
                <div class="left_text" style="margin-top:0 !important;">
                    <?php echo get_post_meta($post->ID, 'corporate_photography_text', true); ?>
                </div>
            </div>
        </div>
        
        <!-- Mantenance Plumbing service Area --> 
        <div class="main_text_area_right_new">
            <div id="hvac_system">
			<div class="inner_hvac">
                <div class="left_text" style="margin-top:0 !important;">
                   <?php echo get_post_meta($post->ID, 'family_photograph_text', true); ?>
                </div>
                <div class="right_img">
                    <?php echo get_post_meta($post->ID, 'family_photograph_img', true); ?>
                </div>
            </div>
        </div>
		</div>
                 
         <!-- Mantenance Painting Area -->         
        <div class="main_text_area_left_new">
            <div id="split_pc">
                <div class="left_img">
                    <?php echo get_post_meta($post->ID, 'events_photography_img', true); ?>
                </div>
                <div class="left_text" style="margin-top:0 !important;">
                   <?php echo get_post_meta($post->ID, 'events_photography_text', true); ?>
                </div>
            </div>
        </div>       
        
        <!-- Mantenance install Area --> 
        <div class="main_text_area_right_new">
            <div id="hvac_system">
			<div class="inner_hvac">
                <div class="left_text" style="margin-top:0 !important;">
                    <?php echo get_post_meta($post->ID, 'architectural_photography_text', true); ?>
                </div>
                <div class="right_img">
                    <?php echo get_post_meta($post->ID, 'architectural_photography_img', true); ?>
                </div>
            </div>
        </div>
</div>		
        
        <!-- Mantenance Dubai printing Area -->               
        <div id="getquort">
            <div class="inner_area">
                <?php echo get_post_meta($post->ID, 'professional_photographer_bottom_text', true); ?>
                
                    <?php echo get_post_meta($post->ID, 'professional_photographer_bottom_link', true); ?>
                
            </div>
        </div>        
		<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(10);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=10&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(10);?>" title="<?php echo get_cat_name(10);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(10);?></a></strong></p>
</div>
 
</div>
</div>

        
<?php
get_footer();
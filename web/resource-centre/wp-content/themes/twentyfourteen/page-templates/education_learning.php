<?php 
/**
template name: education-learning Page
**/

get_header();
?>

<!-- Start of Header Image Area -->
        <div id="edulera_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('need_tutoring') ): ?>
                   <?php the_field('need_tutoring'); ?> 
				<?php endif; ?>  
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="edu_ler_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('tutor_compimg') ): ?>
                   <?php the_field('tutor_compimg'); ?> 
				<?php endif; ?>" alt="EDUCATION & LEARNING">
               </div>
               <div class="left_text">
			   <?php if( get_field('educ_learntxt') ): ?>
                   <?php the_field('educ_learntxt'); ?> 
				<?php endif; ?>
               </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort education">
              <?php if( get_field('hire_edutxt') ): ?>
                   <?php the_field('hire_edutxt'); ?> 
				<?php endif; ?>
               <div class="child_quort">
                   <?php if( get_field('fil_simpletxt') ): ?>
                   <?php the_field('fil_simpletxt'); ?> 
				<?php endif; ?>
               </div>
               <div class="child_quort">
                   <?php if( get_field('rec_quottxt') ): ?>
                   <?php the_field('rec_quottxt'); ?> 
				<?php endif; ?>
               </div>
               <div class="child_quort">
                   <?php if( get_field('best_lrng_provider') ): ?>
                   <?php the_field('best_lrng_provider'); ?> 
				<?php endif; ?>
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
               <?php if( get_field('types_education') ): ?>
                   <?php the_field('types_education'); ?> 
				<?php endif; ?>
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('eduction_tutorimg') ): ?>
                   <?php the_field('eduction_tutorimg'); ?> 
				<?php endif; ?>" alt="Educational Tutoring">
                     <?php if( get_field('eduction_tutortxt') ): ?>
                   <?php the_field('eduction_tutortxt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('musiclesson_img') ): ?>
                   <?php the_field('musiclesson_img'); ?> 
				<?php endif; ?>" alt="Music Lessions">
                       <?php if( get_field('musiclesson_txt') ): ?>
                   <?php the_field('musiclesson_txt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('dance_lessonimg') ): ?>
                   <?php the_field('dance_lessonimg'); ?> 
				<?php endif; ?>" alt="Dance Lessons">
                       <?php if( get_field('dance_lessontxt') ): ?>
                   <?php the_field('dance_lessontxt'); ?> 
				<?php endif; ?>
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('art_paintimg') ): ?>
                   <?php the_field('art_paintimg'); ?> 
				<?php endif; ?>" alt="Art & Painting Classes">
                       <?php if( get_field('art_painttext') ): ?>
                   <?php the_field('art_painttext'); ?> 
				<?php endif; ?>
                   </div> 
                   <div class="child_circle">
                       <img src="<?php if( get_field('langu_corsesimg') ): ?>
                   <?php the_field('langu_corsesimg'); ?> 
				<?php endif; ?>" alt="Language Courses">
				<?php if( get_field('langu_corsestxt') ): ?>
                   <?php the_field('langu_corsestxt'); ?> 
				<?php endif; ?>
                       
                   </div>                   
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="edu_totoring">
                <div class="left_text">
                    <?php if( get_field('educaion_tutoringtxt') ): ?>
                   <?php the_field('educaion_tutoringtxt'); ?> 
				<?php endif; ?>
				</div>
                <div class="right_img">
                    <img src="<?php if( get_field('educaion_tutoringimg') ): ?>
                   <?php the_field('educaion_tutoringimg'); ?> 
				<?php endif; ?>" alt="EDUCATIONAL TUTORING">
                </div>
                <div class="full_width">
				<?php if( get_field('educaion_tutoringdwntxt') ): ?>
                   <?php the_field('educaion_tutoringdwntxt'); ?> 
				<?php endif; ?>
                </div>    
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="music_lessons">
               <div class="left_img">
                   <img src="<?php if( get_field('music_lessonsimg') ): ?>
                   <?php the_field('music_lessonsimg'); ?> 
				<?php endif; ?>" alt="MUSIC LESSONS">
               </div>
               <div class="left_text">
			   <?php if( get_field('music_lessonstxt') ): ?>
                   <?php the_field('music_lessonstxt'); ?> 
				<?php endif; ?>
               </div>    
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="dance_leasson">
                <div class="left_text">
                  <?php if( get_field('dancing_lessontxt') ): ?>
                   <?php the_field('dancing_lessontxt'); ?> 
				<?php endif; ?>
                </div>
                <div class="right_img">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dance_leasson.png" alt="DANCE LESSONS image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="art_printing">
               <div class="left_img">
                   <img src="<?php if( get_field('art_paintleimg') ): ?>
                   <?php the_field('art_paintleimg'); ?> 
				<?php endif; ?>" alt="ART & PAINTING CLASSES Image">
               </div>
               <div class="left_text">
			   <?php if( get_field('art_paintletxt') ): ?>
                   <?php the_field('art_paintletxt'); ?> 
				<?php endif; ?>
               </div>    
               <div class="full_width">
			   <?php if( get_field('art_paintledwntxt') ): ?>
                   <?php the_field('art_paintledwntxt'); ?> 
				<?php endif; ?>
               </div>    
           </div>            
        </div>     
        
        <div class="main_text_area_right_new">
            <div id="language_curce">
                <div class="left_text">
				<?php if( get_field('lang_courtxt') ): ?>
                   <?php the_field('lang_courtxt'); ?> 
				<?php endif; ?>
                    </div>
                <div class="right_img">
                    <img src="<?php if( get_field('lang_courimg') ): ?>
                   <?php the_field('lang_courimg'); ?> 
				<?php endif; ?>" alt="LANGUAGE COURSES image">
                </div>
                <div class="full_width">
				<?php if( get_field('lang_courdwntxt') ): ?>
                   <?php the_field('lang_courdwntxt'); ?> 
				<?php endif; ?>
                </div>
            </div>
        </div> 
        
        <div id="getquort">
            <div class="inner_area">
                <?php if( get_field('quotes_now') ): ?>
                   <?php the_field('quotes_now'); ?> 
				<?php endif; ?>
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(26);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=26&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(26);?>" title="<?php echo get_cat_name(26);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(26);?></a></strong></p>
</div>
 
</div>

</div>


<?php get_footer(); ?>
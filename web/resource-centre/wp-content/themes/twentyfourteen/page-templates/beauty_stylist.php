<?php 
/**
template name: beauty-stylist Page
**/

get_header();
?>
 <!-- Start of Header Image Area -->
        <div id="beauty_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('beauty_stylist') ): ?>
                   <?php the_field('beauty_stylist'); ?> 
				<?php endif; ?>
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="beauty_service">
               <div class="left_img">
                   <img src="<?php if( get_field('beauty_styl_img') ): ?>
								<?php the_field('beauty_styl_img'); ?> 
							<?php endif; ?>" alt="PROFESSIONAL PET CARE SERVICES">
               </div>
               <div class="left_text">
				<?php if( get_field('beauty_styl_txt') ): ?>
                   <?php the_field('beauty_styl_txt'); ?> 
				<?php endif; ?> 
              </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort education">
             <?php if( get_field('event_beauty_stylist') ): ?>
                   <?php the_field('event_beauty_stylist'); ?> 
				<?php endif; ?>  
               <div class="child_quort">
                <?php if( get_field('fill_out_one') ): ?>
                   <?php the_field('fill_out_one'); ?> 
				<?php endif; ?>     
               </div>
               <div class="child_quort">
                 <?php if( get_field('receive_quation') ): ?>
                   <?php the_field('receive_quation'); ?> 
				<?php endif; ?>  
               </div>
               <div class="child_quort">
                <?php if( get_field('hire_beauty_style') ): ?>
                   <?php the_field('hire_beauty_style'); ?> 
				<?php endif; ?>     
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
             <?php if( get_field('event_beauty_styling') ): ?>
                   <?php the_field('event_beauty_styling'); ?> 
				<?php endif; ?>  
               <div class="comm_four">
                   <div class="child_circle">
                       <img src="<?php if( get_field('hair_styling_img') ): ?>
                   <?php the_field('hair_styling_img'); ?> 
				<?php endif; ?>" alt="Hair Styling">
                    <?php if( get_field('hair_styling') ): ?>
                   <?php the_field('hair_styling'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('make-up_services_img') ): ?>
                   <?php the_field('make-up_services_img'); ?> 
				<?php endif; ?>" alt="Make-Up Services">
                    <?php if( get_field('make-up_services') ): ?>
                   <?php the_field('make-up_services'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('henna_services_img') ): ?>
                   <?php the_field('henna_services_img'); ?> 
				<?php endif; ?>" alt="Henna Services">
                    <?php if( get_field('henna_services') ): ?>
                   <?php the_field('henna_services'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('personal_styling_img') ): ?>
                   <?php the_field('personal_styling_img'); ?> 
				<?php endif; ?>" alt="Personal Styling">
                     <?php if( get_field('personal_styling') ): ?>
                   <?php the_field('personal_styling'); ?> 
				<?php endif; ?>  
                   </div>
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="hair_styleing">
                <div class="left_text">
                 <?php if( get_field('hair_styling_text') ): ?>
                   <?php the_field('hair_styling_text'); ?> 
				<?php endif; ?>     
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('hair_styling_imgs') ): ?>
                   <?php the_field('hair_styling_imgs'); ?> 
				<?php endif; ?>" alt="Hair Styling">
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="make_up_service">
               <div class="left_img">
                   <img src="<?php if( get_field('make_up_ser_img') ): ?>
                   <?php the_field('make_up_ser_img'); ?> 
				<?php endif; ?>" alt="Make Up Service">
               </div>
               <div class="left_text">
                 <?php if( get_field('make_up_ser') ): ?>
                   <?php the_field('make_up_ser'); ?> 
				<?php endif; ?>  
				   </div>              
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="henna_service">
                <div class="left_text">
                 <?php if( get_field('henna_servic') ): ?>
                   <?php the_field('henna_servic'); ?> 
				<?php endif; ?>    
				   </div>
                <div class="right_img">
                    <img src="<?php if( get_field('henna_servic_img') ): ?>
                   <?php the_field('henna_servic_img'); ?> 
				<?php endif; ?>" alt="Henna Services image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="per_styling">
               <div class="left_img">
                   <img src="<?php if( get_field('personal_styling_imgs') ): ?>
                   <?php the_field('personal_styling_imgs'); ?> 
				<?php endif; ?>" alt="Personal Styling Image">
               </div>
               <div class="left_text">
                 <?php if( get_field('personal_styling_texts') ): ?>
                   <?php the_field('personal_styling_texts'); ?> 
				<?php endif; ?>  
				   </div>
           </div>            
        </div>                  
        
        <div id="getquort">
            <div class="inner_area">
             <?php if( get_field('get_quotes_nows') ): ?>
                   <?php the_field('get_quotes_nows'); ?> 
				<?php endif; ?>   
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(24);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=24&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(24);?>" title="<?php echo get_cat_name(24);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(24);?></a></strong></p>
</div>
 
</div>

</div>

<?php get_footer(); ?>
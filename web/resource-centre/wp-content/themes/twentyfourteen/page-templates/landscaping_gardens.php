<?php
/**
 * Template Name: Landscaping & Gardens

 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('newhead'); ?>


<!--slider-->
<div class="bannerDesign landscaping_page_md">
  <div class="container">
    <div class="col-md-6 col-sm-7">
      <div class="bannerText landscaping_ali_change">
        <?php echo get_post_meta($post->ID, 'lG_bannerText', true); ?> </div>
    </div>
    <div class="col-md-6 col-sm-5">
      <div class="bannerImg"><?php echo get_post_meta($post->ID, 'lG_bannerImg', true); ?></div>
    </div>
  </div>
</div>
<!--slider--> 

<!--Accouting-Section-->
<div class="accountingSec margonB">
  <div class="container">
    <div class="col-md-4 col-sm-5">
      <div class="accountImg"><?php echo get_post_meta($post->ID, 'Landscape_gardensImg', true); ?></div>
    </div>
    <div class="col-md-8 col-sm-7">
      <div class="accountText catMargin">
        <?php echo get_post_meta($post->ID, 'Landscape_gardensText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Accouting-Section--> 

<!--Step-Section-->
<div id="step_area" class="stepSection">
  <div class="container">
     <?php echo get_post_meta($post->ID, 'lg_stepsHeading', true); ?>
    <div class="stepS">
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'lg_stepsoneImg', true); ?></span>
           <?php echo get_post_meta($post->ID, 'lg_stepsoneText', true); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'lg_stepstwoImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'lg_stepstwoText', true); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'lg_stepsthreeImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'lg_stepsthreeText', true); ?>
        </div>
      </div>
      <div class="stepGet"> <?php echo get_post_meta($post->ID, 'lg_stepsLink', true); ?></div>
    </div>
  </div>
</div>
<!--Step-Section--> 

<!--Accountant Services-->
<div class="accountantService">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'lg_serviceHeading', true); ?>
    <div class="row">
    <div class="serviceStep">
    
      <div class="col-md-4 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'lg_serviceoneImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'lg_serviceoneText', true); ?>
        </span> </div>
        
        <div class="col-md-4 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'lg_servicetwoImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'lg_servicetwoText', true); ?>
        </span> </div>
        
        <div class="col-md-4 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'lg_servicethreeImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'lg_servicethreeText', true); ?>
        </span> </div>
        
        <div class="col-md-4 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'lg_servicefourImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'lg_servicefourText', true); ?>
        </span> </div>
        
        <div class="col-md-4 col-sm-6">
        <div class="serviceIcon"> <?php echo get_post_meta($post->ID, 'lg_servicefiveImg', true); ?></div>
        <span class="serviceHed">
         <?php echo get_post_meta($post->ID, 'lg_servicefiveText', true); ?>
        </span> </div>
        
        <div class="col-md-4 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'lg_servicesixImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'lg_servicesixText', true); ?>
        </span> </div>
        
    </div>
  </div>
  </div>
</div>
<!--Accountant Services--> 

<!--InteriorDbai-->
<div class="assuranceSection">
  <div class="container">
    <div class="col-md-9 col-sm-7">
      <div class="assuranceText">
        <?php echo get_post_meta($post->ID, 'lg_gardensText', true); ?>
      </div>
    </div>
    <div class="col-md-3 col-sm-5"><span class="assurancImg"><?php echo get_post_meta($post->ID, 'lg_gardensImg', true); ?></span></div>
  </div>
</div>
<!--InteriorDbai--> 

<!--assuranceSection--> 

<!--Book-Section-->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'lg_hardscapingImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'lg_hardscapingText', true); ?>
      </div>
    </div>
    
    
    
  </div>
</div>
<!--Book-Section--> 

<!--fitOut-->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'lg_IrrigationSystemsText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg"><?php echo get_post_meta($post->ID, 'lg_IrrigationSystemsImg', true); ?></span></div>
  </div>
</div>
<!--fitOut--> 

 
 
<!----Networking--->
 <div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg artificiaLower"><?php echo get_post_meta($post->ID, 'lg_ArtificialSyntheticImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'lg_ArtificialSyntheticText', true); ?>
      </div>
    </div>
    
    
    
  </div>
</div>
<!----Networking--->


<!---Computer optimization--->

<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'lg_deckingText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero deckingUp"><?php echo get_post_meta($post->ID, 'lg_deckingImg', true); ?></span></div>
  </div>
</div>

<!---Computer optimization--->


<!---Backup--->
 <div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'lg_WaterFeaturesImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'lg_WaterFeaturesText', true); ?>
      </div>
    </div>
    
    
    
  </div>
</div>
<!---Backup--->


<!--Qoutes-section-->
<div class="QuotesSec"> 
  <div class="container">
    <?php echo get_post_meta($post->ID, 'lg_quoteText', true); ?> </div>
</div>
<!--Qoutes-section-->
<div class="resourceCenter">
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(7);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=7&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(7);?>" title="<?php echo get_cat_name(7);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(7);?></a></strong></p>
</div>
</div>
</div>


<?php
get_footer();

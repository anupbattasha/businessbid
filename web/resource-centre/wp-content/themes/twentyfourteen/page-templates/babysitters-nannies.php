
<?php
/**
 * Template Name: babysitters-nannies Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
 <!-- Start of Header Image Area -->
        <div id="babysitters_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('hire_babyseaters') ): ?>
                   <?php the_field('hire_babyseaters'); ?> 
				<?php endif; ?>
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="babysitters_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('babysit_img') ): ?>
                   <?php the_field('babysit_img'); ?> 
				<?php endif; ?>" alt="BABYSITTING">
               </div>
               <div class="left_text">
			   <?php if( get_field('babysit_tex') ): ?>
                   <?php the_field('babysit_tex'); ?> 
				<?php endif; ?>
               </div>
               <div class="full_width">
                <?php if( get_field('babysitt_text') ): ?>
                   <?php the_field('babysitt_text'); ?> 
				<?php endif; ?>   
				   </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort">
             <?php if( get_field('hire__babysitter') ): ?>
                   <?php the_field('hire__babysitter'); ?> 
				<?php endif; ?>    
               <div class="child_quort">
                <?php if( get_field('fill_out') ): ?>
                   <?php the_field('fill_out'); ?> 
				<?php endif; ?>   
               </div>
               <div class="child_quort">
                <?php if( get_field('receive_qut') ): ?>
                   <?php the_field('receive_qut'); ?> 
				<?php endif; ?>      
               </div>
               <div class="child_quort">
                <?php if( get_field('hire_best') ): ?>
                   <?php the_field('hire_best'); ?> 
				<?php endif; ?>         
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
             <?php if( get_field('types_of_babysitting_') ): ?>
                   <?php the_field('types_of_babysitting_'); ?> 
				<?php endif; ?>           
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('child_safety_img') ): ?>
                   <?php the_field('child_safety_img'); ?> 
				<?php endif; ?>" alt="Child Safety">
                    <?php if( get_field('child_safety_text') ): ?>
                   <?php the_field('child_safety_text'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('engagement_img') ): ?>
                   <?php the_field('engagement_img'); ?> 
				<?php endif; ?>" alt="Engagement">
                    <?php if( get_field('engagement_text') ): ?>
                   <?php the_field('engagement_text'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('changing_img') ): ?>
                   <?php the_field('changing_img'); ?> 
				<?php endif; ?>" alt="Changing & Cleaning">
                     <?php if( get_field('changing_text') ): ?>
                   <?php the_field('changing_text'); ?> 
				<?php endif; ?>  
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('meal_preparation_img') ): ?>
                   <?php the_field('meal_preparation_img'); ?> 
				<?php endif; ?>" alt="Meal Preparation">
                    <?php if( get_field('meal_preparation_text') ): ?>
                   <?php the_field('meal_preparation_text'); ?> 
				<?php endif; ?>   
                   </div> 
                   <div class="child_circle">
                       <img src="<?php if( get_field('sleep_time_img') ): ?>
                   <?php the_field('sleep_time_img'); ?> 
				<?php endif; ?>" alt="Sleep Time">
                     <?php if( get_field('sleep_time_text') ): ?>
                   <?php the_field('sleep_time_text'); ?> 
				<?php endif; ?>  
                   </div>                   
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="child_saifet">
                <div class="inner_hvac">
                    <div class="left_text">
                     <?php if( get_field('focus_safety_text') ): ?>
                   <?php the_field('focus_safety_text'); ?> 
				<?php endif; ?>   
						</div>
                    <div class="right_img">
                        <img src="<?php if( get_field('focus_safety_img') ): ?>
                   <?php the_field('focus_safety_img'); ?> 
				<?php endif; ?>" alt="Focus on Safety">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="engage_child">
               <div class="left_img">
                   <img src="<?php if( get_field('engagement_children_img') ): ?>
                   <?php the_field('engagement_children_img'); ?> 
				<?php endif; ?>" alt="Engagement with Children">
               </div>
               <div class="left_text">
                  <?php if( get_field('engagement_children_text') ): ?>
                   <?php the_field('engagement_children_text'); ?> 
				<?php endif; ?> 
				   </div>
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="babysitter_changing">
                <div class="left_text">
                 <?php if( get_field('cleaning_chores_text') ): ?>
                   <?php the_field('cleaning_chores_text'); ?> 
				<?php endif; ?>    
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('cleaning_chores_img') ): ?>
                   <?php the_field('cleaning_chores_img'); ?> 
				<?php endif; ?>" alt="Changing  & Cleaning Chores image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="meal_protection">
               <div class="left_img">
                   <img src="<?php if( get_field('meal_preparat_img') ): ?>
                   <?php the_field('meal_preparat_img'); ?> 
				<?php endif; ?>" alt="Meal Preparation Image">
               </div>
               <div class="left_text">
                 <?php if( get_field('meal_preparat_text') ): ?>
                   <?php the_field('meal_preparat_text'); ?> 
				<?php endif; ?> 
				  </div>
           </div>            
        </div>     
        
        <div class="main_text_area_right_new">
            <div id="putting_child">
                <div class="left_text">
                 <?php if( get_field('child_to_sleep') ): ?>
                   <?php the_field('child_to_sleep'); ?> 
				<?php endif; ?>    
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('child_sleep_img') ): ?>
                   <?php the_field('child_sleep_img'); ?> 
				<?php endif; ?>" alt="Putting your Child to Sleep image">
                </div>
            </div>
        </div> 
        
        <div class="main_text_area_left_new">
           <div id="assist_chores">
               <div class="left_img">
                   <img src="<?php if( get_field('laundry_chores_img') ): ?>
                   <?php the_field('laundry_chores_img'); ?> 
				<?php endif; ?>" alt="Assist in Laundry Chores Image">
               </div>
               <div class="left_text">
                 <?php if( get_field('laundry_chore_text') ): ?>
                   <?php the_field('laundry_chore_text'); ?> 
				<?php endif; ?>  
				   </div>
           </div>            
        </div>            
        
        <div id="getquort">
            <div class="inner_area">
              <?php if( get_field('get_quote_now') ): ?>
                   <?php the_field('get_quote_now'); ?> 
				<?php endif; ?>    
				</div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(23);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=23&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(23);?>" title="<?php echo get_cat_name(23);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(23);?></a></strong></p>
</div>
 
</div>

</div>
<?php get_footer(); ?>
<?php 
/**
template name: moving-storage Page
**/

get_header();
?>

<!-- Start of Header Image Area -->
        <div id="moving_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('looking_storage') ): ?>
                   <?php the_field('looking_storage'); ?> 
				<?php endif; ?> 
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="storage_move">
               <div class="left_img">
                   <img src="<?php if( get_field('movestore_img') ): ?>
                   <?php the_field('movestore_img'); ?> 
				<?php endif; ?>" alt="STORAGE SERVICES">
               </div>
               <div class="left_text">
			   <?php if( get_field('movestore_txt') ): ?>
                   <?php the_field('movestore_txt'); ?> 
				<?php endif; ?>
                </div>   
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort education">
              <?php if( get_field('hire_moving_storage') ): ?>
                   <?php the_field('hire_moving_storage'); ?> 
				<?php endif; ?>
               <div class="child_quort">
                   <?php if( get_field('fillout_simple') ): ?>
                   <?php the_field('fillout_simple'); ?> 
				<?php endif; ?>
               </div>
               <div class="child_quort">
                   <?php if( get_field('receive_quotation') ): ?>
                   <?php the_field('receive_quotation'); ?> 
				<?php endif; ?>
               </div>
               <div class="child_quort">
                   <?php if( get_field('hire_best_moving_storage') ): ?>
                   <?php the_field('hire_best_moving_storage'); ?> 
				<?php endif; ?>
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
                <?php if( get_field('types_moving_storage') ): ?>
                   <?php the_field('types_moving_storage'); ?> 
				<?php endif; ?>
               <div class="comm_four">
                   <div class="child_circle">
                       <img src="<?php if( get_field('domestic_moving_serviceimg') ): ?>
                   <?php the_field('domestic_moving_serviceimg'); ?> 
				<?php endif; ?>" alt="Domestic Moving Services">
                       <?php if( get_field('domestic_moving_servicetxt') ): ?>
                   <?php the_field('domestic_moving_servicetxt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('international_moving_serviceimg') ): ?>
                   <?php the_field('international_moving_serviceimg'); ?> 
				<?php endif; ?>" alt="International Moving Services">
                       <?php if( get_field('international_moving_servicetxt') ): ?>
                   <?php the_field('international_moving_servicetxt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('moving_packingimg') ): ?>
                   <?php the_field('moving_packingimg'); ?> 
				<?php endif; ?>" alt="Moving & Packing ">
                       <?php if( get_field('moving_packingtxt') ): ?>
                   <?php the_field('moving_packingtxt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('storage_servicesimg') ): ?>
                   <?php the_field('storage_servicesimg'); ?> 
				<?php endif; ?>" alt="Storage Services">
                       <?php if( get_field('storage_servicestxt') ): ?>
                   <?php the_field('storage_servicestxt'); ?> 
				<?php endif; ?>
                   </div>
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="domestic_moving">
                <div class="left_text">
                    <?php if( get_field('domestic_movingtext') ): ?>
                   <?php the_field('domestic_movingtext'); ?> 
				<?php endif; ?>
                </div>
                <div class="right_img">
                    <img src="<?php if( get_field('domestic_movingimg') ): ?>
                   <?php the_field('domestic_movingimg'); ?> 
				<?php endif; ?>" alt="Domestic Moving">
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="inter_moving">
               <div class="left_img">
                   <img src="<?php if( get_field('international_movingimg') ): ?>
                   <?php the_field('international_movingimg'); ?> 
				<?php endif; ?>" alt="International Moving">
               </div>
               <div class="left_text">
			   <?php if( get_field('international_movingtxt') ): ?>
                   <?php the_field('international_movingtxt'); ?> 
				<?php endif; ?>
                </div>
                <div class="full_width">
                  <?php if( get_field('international_moving_txt') ): ?>
                   <?php the_field('international_moving_txt'); ?> 
				<?php endif; ?>  
                </div>               
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="mov_pac_ser">
                <div class="left_text">
				<?php if( get_field('moving_packing_servicetxt') ): ?>
                   <?php the_field('moving_packing_servicetxt'); ?> 
				<?php endif; ?>
                </div>
                <div class="right_img">
                    <img src="<?php if( get_field('moving_packing_serviceimg') ): ?>
                   <?php the_field('moving_packing_serviceimg'); ?> 
				<?php endif; ?>" alt="Packing Services image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="storage_ser">
               <div class="left_img">
                   <img src="<?php if( get_field('storage_serviceimg') ): ?>
                   <?php the_field('storage_serviceimg'); ?> 
				<?php endif; ?>" alt="Storage Services Image">
               </div>
               <div class="left_text">
			   <?php if( get_field('storage_servicetxt') ): ?>
                   <?php the_field('storage_servicetxt'); ?> 
				<?php endif; ?>
               </div>
               <div class="full_width">
               <?php if( get_field('storage_service_txt') ): ?>
                   <?php the_field('storage_service_txt'); ?> 
				<?php endif; ?>
			   </div>
           </div>            
        </div>     
        
        <div id="getquort">
            <div class="inner_area">
              <?php if( get_field('free_quotes') ): ?>
                   <?php the_field('free_quotes'); ?> 
				<?php endif; ?>  
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(27);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=27&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(27);?>" title="<?php echo get_cat_name(27);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(27);?></a></strong></p>
</div>
 
</div>

</div>
<?php get_footer(); ?>
<?php
/**
 * Template Name: Cleaning
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('newhead'); ?>
        <!-- Main Content File here -->
        <div id="ac_header" class="ac_hed222">
            <div class="cleaning_headder_inner">
                <div class="headesr_slide_text">
                    <?php echo get_post_meta($post->ID, 'cleaning_banner_text', true); ?>
                    
                        <?php echo get_post_meta($post->ID, 'cleaning_banner_link', true); ?>
                    
                    
                        <?php echo get_post_meta($post->ID, 'cleaning_banner_link_two', true); ?>
                    
                </div>
            </div>
        </div>
        <!-- Maintenance Service Area here -->
        <div class="main_text_area_left_new">
            <div id="ac_pc">
                <div class="left_img">
                   <?php echo get_post_meta($post->ID, 'commercial_cleaning_img', true); ?>
                </div>
                <div class="left_text">
                    <?php echo get_post_meta($post->ID, 'commercial_cleaning_text', true); ?>
                </div>
            </div>
        </div>

        <!-- Maintenance Step Area here -->
        <div id="common_quort_area">
            <div class="inner_quort" > 
                <?php echo get_post_meta($post->ID, 'cleaning_3esay_steup_heading', true); ?>
                <div class="child_quort">
                    <?php echo get_post_meta($post->ID, '3easy_one', true); ?>
                </div>
                <div class="child_quort">
                    <?php echo get_post_meta($post->ID, '3easy_two', true); ?>
                </div>
                <div class="child_quort">
                    <?php echo get_post_meta($post->ID, '3easy_three', true); ?>
                </div>
                <div class="new_get_quort_btn">
                    <?php echo get_post_meta($post->ID, '3easy_link', true); ?>
                </div>
            </div>
        </div> 
        
        <!-- Mantenance Printing press service Area -->
        <div id="common_circel_area">
            <div class="inner_circel">
                <?php echo get_post_meta($post->ID, 'cleaning_service_heading', true); ?>
				<div class="comm_two">
				<div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'residential', true); ?>
                </div>
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'commercial', true); ?>               
                </div>
				</div>
            </div>
        </div>        
        
        <!-- Mantenance Handyman service Area --> 
        <div class="main_text_area_right_new">
            <div id="hvac_system">
			<div class="inner_hvac">
                <div class="left_text" style="margin-top:0 !important;">
                   <?php echo get_post_meta($post->ID, 'residential_text', true); ?>
                </div>
                <div class="right_img">
                    <?php echo get_post_meta($post->ID, 'residential_img', true); ?>
                </div>
            </div>
			</div>
        </div>       
        
        <!-- Mantenance Electrical Area -->         
        <div class="main_text_area_left_new">
            <div id="split_pc">
                <div class="left_img">
                    <?php echo get_post_meta($post->ID, 'commartial_img', true); ?>
                </div>
                <div class="left_text" style="margin-top:0 !important;">
                    <?php echo get_post_meta($post->ID, 'commartial_text', true); ?>
                </div>
            </div>
        </div>
        
        <!-- Mantenance Dubai printing Area -->               
        <div id="getquort">
            <div class="inner_area">
                <?php echo get_post_meta($post->ID, 'If_you_are_cleaning_text', true); ?>
                
                    <?php echo get_post_meta($post->ID, 'If_you_are_cleaning_link', true); ?>
               
            </div>
        </div>        
        <div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(9);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=9&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(9);?>" title="<?php echo get_cat_name(9);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(9);?></a></strong></p>
</div>
 
</div>
</div>

 <?php
get_footer();
<?php
/**
 * Template Name: Catering Services
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('newhead'); ?>


<!--slider-->
<div class="bannerDesign catering_page_md">
  <div class="container">
    <div class="col-md-6 col-sm-7">
      <div class="bannerText catering_ali_change">
        <?php echo get_post_meta($post->ID, 'catering_bannerText', true); ?>
         </div>
    </div>
    <div class="col-md-6 col-sm-5">
      <div class="bannerImg"><?php echo get_post_meta($post->ID, 'catering_bannerImg', true); ?></div>
    </div>
  </div>
</div>
<!--slider--> 

<!--Accouting-Section-->
<div class="accountingSec">
  <div class="container">
    <div class="col-md-4 col-sm-5">
      <div class="accountImg"><?php echo get_post_meta($post->ID, 'chossing_cateringImg', true); ?></div>
    </div>
    <div class="col-md-8 col-sm-7"> 
      <div class="accountText catMargin">
        <?php echo get_post_meta($post->ID, 'chossing_cateringText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Accouting-Section--> 

<!--Step-Section-->
<div class="stepSection">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'setup_cateringHeading', true); ?>
    <div class="stepS">
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'setup_cateringOneImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'setup_cateringOneText', true); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="stepOne">  <?php echo get_post_meta($post->ID, 'setup_cateringtwoImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'setup_cateringtwoText', true); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'setup_cateringthreeImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'setup_cateringthreeText', true); ?>
        </div>
      </div>
      <div class="stepGet"><?php echo get_post_meta($post->ID, 'setup_cateringLink', true); ?></div>
    </div>
  </div>
</div>
<!--Step-Section--> 

<!--Accountant Services-->
<div class="accountantService">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'service_cateringHeading', true); ?>
    <div class="serviceStep">
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringoneImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringoneText', true); ?></div>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringtwoImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringtwoText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringthreeImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringthreeText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringfourImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringfourText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringfiveImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringfiveText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringsixImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringsixText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringsevenImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringsevenText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'service_cateringeightImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'service_cateringeightText', true); ?>
        </span> </div>
    </div>
  </div>
</div>
<!--Accountant Services--> 

<!--catererExperince-->

<div class="catererExperince">
  <div class="container">
  <div class="row">
    <div class="col-md-8 col-sm-7">
      <div class="catererText">
        <?php echo get_post_meta($post->ID, 'catering_experienceText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="catererImg"><?php echo get_post_meta($post->ID, 'catering_experienceImg', true); ?></span></div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'experince_oneImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'experince_oneText', true); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'experince_twoImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'experince_twoText', true); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"> <?php echo get_post_meta($post->ID, 'experince_threeImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'experince_threeText', true); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'experince_fourImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'experince_fourText', true); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'experince_fiveImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'experince_fiveText', true); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<!--catererExperince--> 

<!--assuranceSection--> 

<!--Book-Section-->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'PlanAhead_cateringImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'PlanAhead_cateringText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Book-Section--> 

<!--dicied-->
<div class="catererExperince">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="catererText">
        <?php echo get_post_meta($post->ID, 'Decide_cateringText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="catererImg"><?php echo get_post_meta($post->ID, 'Decide_cateringImg', true); ?></span></div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'type_of_mealImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'type_of_mealText', true); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'type_of_cuisineImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'type_of_cuisineText', true); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1 col-sm-2"><span class="catereIcon"><?php echo get_post_meta($post->ID, 'number_of_coursesImg', true); ?></span></div>
      <div class="col-md-11 col-sm-10">
        <div class="catererList">
          <?php echo get_post_meta($post->ID, 'number_of_coursesText', true); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!--dicied--> 

<!--Payroll-->
<div class="PayRoll">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="payrollImg"><?php echo get_post_meta($post->ID, 'support_coursesImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="PayText">
        <?php echo get_post_meta($post->ID, 'support_coursesText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Payroll--> 

<!--expectingSection-->
<div class="PayRoll experting">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="PayText">
        <?php echo get_post_meta($post->ID, 'people_coursesText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="payrollImg"><?php echo get_post_meta($post->ID, 'people_coursesImg', true); ?></span></div>
  </div>
</div>
<!--expectingSection--> 

<!--Qoutes-section-->
<div class="QuotesSec caterQuotes">
  <div class="container">
  <?php echo get_post_meta($post->ID, 'quote_cateringText', true); ?>
     </div>
</div>
<!--Qoutes-section--> 


<div class="resourceCenter grayBg">
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(3);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=3&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php //the_excerpt(10); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(3);?>" title="<?php echo get_cat_name(3);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(3);?></a></strong></p>
</div>
</div>
</div>

<?php
get_footer();

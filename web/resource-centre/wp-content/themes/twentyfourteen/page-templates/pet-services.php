<?php 
/**
template name: pet-services Page
**/

get_header();
?>

<!-- Start of Header Image Area -->
        <div id="pet_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('free_quotes') ): ?>
                   <?php the_field('free_quotes'); ?> 
				<?php endif; ?> 
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="personal_pet_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('personal_petcareimg') ): ?>
                   <?php the_field('personal_petcareimg'); ?> 
							<?php endif; ?>" alt="PROFESSIONAL PET CARE SERVICES">
               </div>
               <div class="left_text">
					<?php if( get_field('personal_petcaretxt') ): ?>
						  <?php the_field('personal_petcaretxt'); ?> 
					<?php endif; ?>
               </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort education">
					<?php if( get_field('hire_petcare') ): ?>
						  <?php the_field('hire_petcare'); ?> 
					<?php endif; ?>
               <div class="child_quort">
                    <?php if( get_field('fill_out') ): ?>
						  <?php the_field('fill_out'); ?> 
					<?php endif; ?> 
               </div>
               <div class="child_quort">
                    <?php if( get_field('receive_quotation') ): ?>
						  <?php the_field('receive_quotation'); ?> 
					<?php endif; ?>
				   
               </div>
               <div class="child_quort">
                    <?php if( get_field('best_careprovider') ): ?>
						  <?php the_field('best_careprovider'); ?> 
					<?php endif; ?>
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
					<?php if( get_field('types_petservices') ): ?>
						  <?php the_field('types_petservices'); ?> 
					<?php endif; ?>
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('grooming_img') ): ?>
									   <?php the_field('grooming_img'); ?> 
								 <?php endif; ?>" alt="Grooming">
								 
								 <?php if( get_field('grooming_txt') ): ?>
									   <?php the_field('grooming_txt'); ?> 
								 <?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('pet_sittimgimg') ): ?>
									   <?php the_field('pet_sittimgimg'); ?> 
								 <?php endif; ?>" alt="Pet Sitting">
								 
								 <?php if( get_field('pet_sittimgtxt') ): ?>
									   <?php the_field('pet_sittimgtxt'); ?> 
								 <?php endif; ?>
                       
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('dog_trainingimg') ): ?>
									   <?php the_field('dog_trainingimg'); ?> 
								 <?php endif; ?>" alt="Dog Training">
								  
								 <?php if( get_field('dog_trainingtxt') ): ?>
									   <?php the_field('dog_trainingtxt'); ?> 
								 <?php endif; ?> 
                       
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('dogwalkers_img') ): ?>
									   <?php the_field('dogwalkers_img'); ?> 
								 <?php endif; ?>" alt="Dog Walkers">
								 
								 <?php if( get_field('dogwalkers_txt') ): ?>
									   <?php the_field('dogwalkers_txt'); ?> 
								 <?php endif; ?>
                       
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('kennel_servicesimg') ): ?>
									   <?php the_field('kennel_servicesimg'); ?> 
								 <?php endif; ?>" alt="Kennel Services">
                       
								 <?php if( get_field('kennel_servicestxt') ): ?>
									   <?php the_field('kennel_servicestxt'); ?> 
								 <?php endif; ?>	
								
                   </div>
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="gromming_image">
                <div class="left_text">
                
					<?php if( get_field('new_groomingtxt') ): ?>
								<?php the_field('new_groomingtxt'); ?> 
				    <?php endif; ?>
				
				</div>	
				 
                <div class="right_img">
                    <img src="<?php if( get_field('new_groomingimg') ): ?>
								<?php the_field('new_groomingimg'); ?> 
							  <?php endif; ?>" alt="Grooming">
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="per_setting">
               <div class="left_img">
                   <img src="<?php if( get_field('pet_setting_img') ): ?>
								<?php the_field('pet_setting_img'); ?> 
							  <?php endif; ?>" alt="Pet sitting">
               </div>
               <div class="left_text">
                <?php if( get_field('pet_setting_text') ): ?>
								<?php the_field('pet_setting_text'); ?> 
							  <?php endif; ?>   
				   </div>
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="dog_traning">
                <div class="left_text">
                 <?php if( get_field('dog_tranning_text') ): ?>
								<?php the_field('dog_tranning_text'); ?> 
							  <?php endif; ?>   
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('dog_tranning_img') ): ?>
								<?php the_field('dog_tranning_img'); ?> 
							  <?php endif; ?>" alt="Dog Training image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="dog_walker">
               <div class="left_img">
                   <img src="<?php if( get_field('dog_waller_img') ): ?>
								<?php the_field('dog_waller_img'); ?> 
							  <?php endif; ?>" alt="Dog Walkers Image">
               </div>
               <div class="left_text">
                 <?php if( get_field('dog_walker_text') ): ?>
								<?php the_field('dog_walker_text'); ?> 
							  <?php endif; ?>  
				   </div>
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="kennal_serve">
                <div class="left_text">
				<?php if( get_field('kennel_servicetxt') ): ?>
						<?php the_field('kennel_servicetxt'); ?> 
				<?php endif; ?>
                </div>
                <div class="right_img">
                    <img src="<?php if( get_field('kennel_serviceimg') ): ?>
						<?php the_field('kennel_serviceimg'); ?> 
						<?php endif; ?>" alt="Kennel Service image">
                </div>
            </div>
        </div>                      
        
        <div id="getquort">
            <div class="inner_area">
               <?php if( get_field('get_quotes') ): ?>
						<?php the_field('get_quotes'); ?> 
			<?php endif; ?>
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(22);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=22&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(22);?>" title="<?php echo get_cat_name(22);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(22);?></a></strong></p>
</div>
 
</div>

</div>
		
<?php get_footer(); ?>
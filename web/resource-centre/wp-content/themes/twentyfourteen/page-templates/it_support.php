<?php
/**
 * Template Name: IT Support
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('newhead'); ?>


<!--slider-->
<div class="bannerDesign it_support_page_md">
  <div class="container">
    <div class="col-md-6 col-sm-7">
      <div class="bannerText it_support_ali_change">
        <?php echo get_post_meta($post->ID, 'ITsupport_bannerText', true); ?>
         </div>
    </div>
    <div class="col-md-6 col-sm-5">
      <div class="bannerImg bottomZero"><?php echo get_post_meta($post->ID, 'ITsupport_bannerImg', true); ?></div>
    </div>
  </div>
</div>
<!--slider--> 

<!--Accouting-Section-->
<div class="accountingSec">
  <div class="container">
    <div class="col-md-4 col-sm-5">
      <div class="accountImg"><?php echo get_post_meta($post->ID, 'IT_support_servicesImg', true); ?></div>
    </div>
    <div class="col-md-8 col-sm-7">
      <div class="accountText catMargin">
        <?php echo get_post_meta($post->ID, 'IT_support_servicesText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Accouting-Section--> 

<!--Step-Section-->
<div class="stepSection">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'ItSupport_stepsHeading', true); ?>
    <div class="stepS">
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'ItSupport_stepsoneImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'ItSupport_stepsoneText', true); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'ItSupport_stepstwoImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'ItSupport_stepstwoText', true); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'ItSupport_stepsthreeImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'ItSupport_stepsthreeText', true); ?>
        </div>
      </div>
      <div class="stepGet">  <?php echo get_post_meta($post->ID, 'ItSupport_stepsLink', true); ?></div>
    </div>
  </div>
</div>
<!--Step-Section--> 

<!--Accountant Services-->
<div class="accountantService">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'ItSupport_serviceHeading', true); ?>
    <div class="serviceStep serviceStep_02">
      <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'ItSupport_serviceoneImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'ItSupport_serviceoneText', true); ?>
        </span> </div>
      <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'ItSupport_servicetwoImg', true); ?></div>
        <span class="serviceHed">
       <?php echo get_post_meta($post->ID, 'ItSupport_servicetwoText', true); ?>
        </span> </div>
      <div class="child_circle custom">
        <div class="serviceIcon"> <?php echo get_post_meta($post->ID, 'ItSupport_servicethreeImg', true); ?></div>
        <span class="serviceHed">
         <?php echo get_post_meta($post->ID, 'ItSupport_servicethreeText', true); ?>
        </span> </div>
      <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'ItSupport_servicefourImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'ItSupport_servicefourText', true); ?>
        </span> </div>
      
         
        <!--<div class="supportContainer">-->
        <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'ItSupport_servicefiveImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'ItSupport_servicefiveText', true); ?>
        </span> </div>
        
        <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'ItSupport_servicesixImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'ItSupport_servicesixText', true); ?>
        </span> </div>
        <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'security_surveillance_img', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'security_surveillance_txt', true); ?>
        </span> </div>
        <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'access_control', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'access_control_txt', true); ?>
        </span> </div>
        <div class="child_circle custom">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'ItSupport_servicesevenImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'ItSupport_servicesevenText', true); ?>
        </span> </div>
        
        
        
        
        
        
        <!--</div>-->
        
    </div>
  </div>
</div>
<!--Accountant Services--> 

<!--InteriorDbai-->
<div class="assuranceSection">
  <div class="container">
    <div class="col-md-9 col-sm-7">
      <div class="assuranceText">
        <?php echo get_post_meta($post->ID, 'IT_maintenance_contractText', true); ?>
      </div>
    </div>
    <div class="col-md-3 col-sm-5"><span class="assurancImg"><?php echo get_post_meta($post->ID, 'IT_maintenance_contractImg', true); ?></span></div>
  </div>
</div>
<!--InteriorDbai--> 

<!--assuranceSection--> 

<!--Book-Section-->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg hardwareLower"><?php echo get_post_meta($post->ID, 'IT_hardwareSoftwareImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText"> 
        <?php echo get_post_meta($post->ID, 'IT_hardwareSoftwareText', true); ?>
      </div>
    </div>
    
    
    
  </div>
</div>
<!--Book-Section--> 

<!--fitOut-->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'IT_computerSecurityText', true); ?>

      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero securityUp"><?php echo get_post_meta($post->ID, 'IT_computerSecurityImg', true); ?></span></div>
  </div>
</div>
<!--fitOut--> 

 
 
<!----Networking--->
 <div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'IT_networkingImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'IT_networkingText', true); ?>

      </div>
    </div>
    
    
    
  </div>
</div>
<!----Networking--->


<!---Computer optimization--->

<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'Computer_optimizationText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero"><?php echo get_post_meta($post->ID, 'Computer_optimizationImg', true); ?></span></div>
  </div>
</div>

<!---Computer optimization--->


<!---Backup--->
 <div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'It_storageServicesImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'It_storageServicesText', true); ?>
     </div>
    </div>
    
    
    
  </div>
</div>
<!---Backup--->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText"><?php echo get_post_meta($post->ID, 'IT_security_servilance_txt', true); ?> 
	  </div>
	  </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero"> <?php echo get_post_meta($post->ID, 'IT_security_servilance_img', true); ?>
         </span></div>
  </div>
</div>
<div class="bookSection">
  <div class="container">
    <div class="col-md-3 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'it_access_con_img', true); ?></span></div>
    <div class="col-md-9 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'it_access_con_txt', true); ?>     </div>
    </div>
    
    
    
  </div>
</div>
<!---Pabx--->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'It_PabxText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero"><?php echo get_post_meta($post->ID, 'It_PabxImg', true); ?></span></div>
  </div>
</div>
<!---Pabx--->

<!--Qoutes-section-->
<div class="QuotesSec pabx"> 
  <div class="container"><?php echo get_post_meta($post->ID, 'IT_quoteText', true); ?></div>
</div>

<!--Qoutes-section-->
<div class="resourceCenter grayBg">
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(5);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=5&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(5);?>" title="<?php echo get_cat_name(5);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(5);?></a></span></strong></p>
</div>
</div>
</div>

<?php
get_footer();

<?php
/**

 * Template Name: Pest Control

 *

 * This is the most generic template file in a WordPress theme

 * and one of the two required files for a theme (the other being style.css).

 * It is used to display a page when nothing more specific matches a query.

 * For example, it puts together the home page when no home.php file exists.

 *

 * @link http://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */



get_header('newhead'); ?>

<!-- Start of Header Image Area -->


<div id="pest_header" class="header_main_slider_image">
	<div class="header_slider">
		<div class="headesr_slide_text"> <?php echo get_post_meta($post->ID, 'Pest_banner_text', true); ?> </div>
	</div>
</div>
<!-- End of Header Image Area --> 

<!-- Start of left Image Area -->
<div class="main_text_area_left">
	<div id="pest_control_services" class="main_left_image">
		<div class="mail_left_img"> <?php echo get_post_meta($post->ID, 'pest_control_Img', true); ?> </div>
		<div class="mail_left_text"> <?php echo get_post_meta($post->ID, 'pest_control_text', true); ?> </div>
	</div>
</div>
<!-- End of left Image Area --> 

<!-- Start Step Area here -->
<div id="pest_quort" class="new_get_quort_area">
	<div class="new_get_quort_area_inner"> <?php echo get_post_meta($post->ID, 'pest_step_heading', true); ?>
		<div class="new_get_court_child_one"> <?php echo get_post_meta($post->ID, 'pest_step_one', true); ?> </div>
		<div class="new_get_court_child_two"> <?php echo get_post_meta($post->ID, 'pest_step_two', true); ?> </div>
		<div class="new_get_court_child_three"> <?php echo get_post_meta($post->ID, 'pest_step_three', true); ?> </div>
		<div class="new_fix_fix_quort"></div>
		<div class="new_get_quort_btn"> <?php echo get_post_meta($post->ID, 'pest_step_btn', true); ?></div>
	</div>
</div>
<!-- End of Step Area here --> 

<!-- Start of Types Area here -->
<div id="pest_newpress" class="newpress_area">
<div class="inner_newpress_area"> <?php echo get_post_meta($post->ID, 'pest_service_heading', true); ?>
	<div class="child_inner_newpress_area"> <?php echo get_post_meta($post->ID, 'bed_bugs', true); ?></div>
		<div class="child_inner_newpress_area"> <?php echo get_post_meta($post->ID, 'cockroaches', true); ?> </div>
		<div class="child_inner_newpress_area"> <?php echo get_post_meta($post->ID, 'rats', true); ?> </div>
		<div class="child_inner_newpress_area"> <?php echo get_post_meta($post->ID, 'ants', true); ?> </div>
	</div>
</div>
<!-- End of Types Area here --> 

<!-- Start of Right Image Area -->
<div id="control_pest" class="main_text_area_right">
	<div class="main_right_image">
		<div class="mail_right_text"> <?php echo get_post_meta($post->ID, 'choose_pest_control_leftContent', true); ?> </div>
		<div class="mail_right_img"> <?php echo get_post_meta($post->ID, 'choose_pest_control_rightImg', true); ?> </div>
		<div class="mail_right_text custom"> <?php echo get_post_meta($post->ID, 'choose_pest_control_leftContent1', true); ?> </div>
	</div>
</div>
<!-- End of Right Image Area --> 

<!-- Start of left Image Area -->
<div id="pest_hours" class="main_text_area_left">
	<div class="main_left_image">
		<div class="mail_left_img"> <?php echo get_post_meta($post->ID, 'Preparing_your_house_leftImg', true); ?> </div>
		<div class="mail_left_text"> <?php echo get_post_meta($post->ID, 'Preparing_your_house_rightContent', true); ?> </div>
	</div>
</div>
<!-- End of left Image Area --> 

<!-- Start of Right Image Area -->
<div id="prevention_pest" class="main_text_area_right">
	<div class="main_right_image">
		<div class="mail_right_text"> <?php echo get_post_meta($post->ID, 'prevention_pest_infestations_leftContent', true); ?> </div>
		<div class="mail_right_img"> <?php echo get_post_meta($post->ID, 'prevention_pest_infestations_rightImg', true); ?></div>
	</div>
</div>
<!-- End of Right Image Area --> 

<!-- Start of Get Quort Area -->
<div id="white" class="get_quort_area">
	<div class="inner_quort_area"> <?php echo get_post_meta($post->ID, 'Pest_qourt', true); ?> </div>
</div>
<!-- End of Get Quort Area -->

<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring a pest control company in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=18&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(18);?>" title="<?php echo get_cat_name(18);?>">CLICK HERE TO VIEW MORE ARTICLES ON PEST CONTROL SERVICES</a></strong></p>
</div>
 
</div>

<?php get_footer(); ?>

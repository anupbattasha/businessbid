<?php

/**

 * Template Name: Custom Home Page

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header('newhead'); ?>
<script>
  jQuery(document).ready(function(){
  jQuery('.six_btn a').click(function(){
  var cate=jQuery('#category').val();
  window.location.href = cate;


  });
  });
 /* $('.go-btn').click(function() {
    window.location = $('#my-dropdown').val();
});*/

  </script>
      <div id="service_cat_area" class="six_area">
            <div class="six_child_area">
                <div class="inner_six_area">
                    <h2>SELECT A SERVICE CATEGORY</h2>
                    <div class="six_tabil">
  <select id="category" name="categories" class="ui-autocomplete-input" autocomplete="off">
    <option value="">Select a category</option>
    <option value="http://www.businessbid.ae/resource-centre/accounting-audit-firms/">Accounting &amp; Auditing</option>
	<option value="http://www.businessbid.ae/resource-centre/catering-companies/">Catering Services</option>
	<option value="http://www.businessbid.ae/resource-centre/interior-design-companies/">Interior Design &amp; Fit Out</option>
	<option value="http://www.businessbid.ae/resource-centre/it-support-companies/">IT Support</option>
	<option value="http://www.businessbid.ae/resource-centre/signage-graphics-companies/">Signage and Signboards</option>
	<option value="http://www.businessbid.ae/resource-centre/landscaping-gardening-companies/">Landscaping &amp; Garden</option>
	<option value="http://www.businessbid.ae/resource-centre/maintenance-handyman-companies/">Maintenance</option>
	<option value="http://www.businessbid.ae/resource-centre/printing-press/">Business Cards &amp; Printing</option>
	<option value="http://www.businessbid.ae/resource-centre/cleaning-services/">Commercial Cleaning</option>
	<option value="http://www.businessbid.ae/resource-centre/photographers/">Photography &amp; Film</option>
	<option value="http://www.businessbid.ae/resource-centre/pest-control-companies/">Pest Control</option>
	<option value="http://www.businessbid.ae/resource-centre/florists/">Florists</option>
	<option value="http://www.businessbid.ae/resource-centre/wedding-planners/">Wedding Planners</option>
	<option value="http://www.businessbid.ae/resource-centre/audio-visual-companies/">Audio &amp; Visual</option>
	<option value="http://www.businessbid.ae/resource-centre/event-management-companies/">Event Management</option>
	<option value="http://www.businessbid.ae/resource-centre/exhibition-stand-builders/">Exhibition Stands &amp; Stages</option>
    <option value="http://www.businessbid.ae/resource-centre/gifts-and-promotional-items/">Gifts &amp; Promotional Item</option>
    <!--<option value="996">Residential Cleaning</option>-->
  </select>
                    </div>
                    <div class="six_btn"><a href="javascript:void(0)">START FREE QUOTES</a></div>
                </div>
            </div>
        </div>

<!--Categories Page-->

<div class="categoriesSec">
  <div class="container">
    <div class="categoriesHed">
      <?php echo get_post_meta($post->ID, 'categoryHeading', true); ?>
    </div>
    <div class="accountantService maincatpage">
      <div class="serviceStep">
        <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_oneImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_oneText', true); ?>
          </span> </div>
        <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twoImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twoText', true); ?>
          </span> </div>
        <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_threeImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_threeText', true); ?>
          </span> </div>
        <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_fourImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_fourText', true); ?>
          </span> </div>
        <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_fiveImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_fiveText', true); ?>
          </span> </div>
        <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_sixImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_sixText', true); ?>
          </span> </div>



          <!--New added-->
          <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_sevenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_sevenText', true); ?>
          </span> </div>

          <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_eightImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_eightText', true); ?>
          </span> </div>

          <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_nineImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_nineText', true); ?>
          </span> </div>

          <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_tenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_tenText', true); ?>
          </span> </div>

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_elevenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_elevenText', true); ?>
          </span> </div>

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_tweleveImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_tweleveText', true); ?>
          </span> </div>

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_thirteenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_thirteenText', true); ?>
          </span> </div>

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_forteenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_forteenText', true); ?>
          </span> </div>

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_fifteenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_fifteenText', true); ?>
          </span> </div>

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_sixteenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_sixteenText', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_eighteenimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_eighteen', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_nineteenimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_nineteenText', true); ?>
          </span> </div>
		  
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentyimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentyText', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentyoneimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentyoneText', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentytwoimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentytwoText', true); ?>
          </span> </div>	

		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentythreeimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentythreeText', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentyfourimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentyfourText', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentyfiveimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentyfiveText', true); ?>
          </span> </div>
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentysiximg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentysixText', true); ?>
          </span> </div>
		  
		  
		  <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'category_twentysevenimg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'category_twentysevenText', true); ?>
          </span> </div>
			
            <div class="col-md-3 col-sm-6 cust">
          <div class="serviceIcon new_icon_01"><a class="cat28-img" title="Swimming & Sport Lessons" href="http://www.businessbid.ae/resource-centre/gifts-and-promotional-items/">&nbsp;</a></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'gift_txt', true); ?>
          </span> </div>
      </div>
    </div>
  </div>
</div>
</div>

<!--Categories Page-->


<?php

get_footer();


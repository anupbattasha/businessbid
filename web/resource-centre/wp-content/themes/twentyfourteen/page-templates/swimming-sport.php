<?php 
/**
template name: swimming-sport Page
**/

get_header();
?>

<!-- Start of Header Image Area -->
        <div id="sports_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                 <?php if( get_field('looking_swiming') ): ?>
                   <?php the_field('looking_swiming'); ?> 
				<?php endif; ?>    
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="sports_swi_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('swimming_and_sport_img') ): ?>
                   <?php the_field('swimming_and_sport_img'); ?> 
				<?php endif; ?>" alt="SWIMMING & SPORTS IMAGE">
               </div>
               <div class="left_text">
                 <?php if( get_field('swimming_sport_text') ): ?>
                   <?php the_field('swimming_sport_text'); ?> 
				<?php endif; ?>
				 </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort">
             <?php if( get_field('hire_sport') ): ?>
                   <?php the_field('hire_sport'); ?> 
				<?php endif; ?> 
               <div class="child_quort">
                <?php if( get_field('hire_sport_one') ): ?>
                   <?php the_field('hire_sport_one'); ?> 
				<?php endif; ?>    
               </div>
               <div class="child_quort">
                <?php if( get_field('receive_quato') ): ?>
                   <?php the_field('receive_quato'); ?> 
				<?php endif; ?>   
               </div>
               <div class="child_quort">
                <?php if( get_field('hire_best_instructor') ): ?>
                   <?php the_field('hire_best_instructor'); ?> 
				<?php endif; ?>   
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
              <?php if( get_field('typing_swimming_sport') ): ?>
                   <?php the_field('typing_swimming_sport'); ?> 
				<?php endif; ?>    
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('swimming_lession_img') ): ?>
                   <?php the_field('swimming_lession_img'); ?> 
				<?php endif; ?>" alt="Swimming Lessons">
                    <?php if( get_field('swimming_lession_text') ): ?>
                   <?php the_field('swimming_lession_text'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('cricket_lession_img') ): ?>
                   <?php the_field('cricket_lession_img'); ?> 
				<?php endif; ?>" alt="Cricket Lessons">
                    <?php if( get_field('cricket_lession') ): ?>
                   <?php the_field('cricket_lession'); ?> 
				<?php endif; ?>  
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('football_coaching_img') ): ?>
                   <?php the_field('football_coaching_img'); ?> 
				<?php endif; ?>" alt="Football Coaching">
                    <?php if( get_field('football_coaching') ): ?>
                   <?php the_field('football_coaching'); ?> 
				<?php endif; ?>   
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('tenis_lession_img') ): ?>
                   <?php the_field('tenis_lession_img'); ?> 
				<?php endif; ?>" alt="Tennis Lessons">
                    <?php if( get_field('tenis_lession') ): ?>
                   <?php the_field('tenis_lession'); ?> 
				<?php endif; ?>   
                   </div> 
                   <div class="child_circle">
                       <img src="<?php if( get_field('basketball_lession_img') ): ?>
                   <?php the_field('basketball_lession_img'); ?> 
				<?php endif; ?>" alt="Basketball Lessons">
                     <?php if( get_field('basketball_lession') ): ?>
                   <?php the_field('basketball_lession'); ?> 
				<?php endif; ?>  
                   </div>                   
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="swimming_lessons">
                <div class="inner_hvac">
                    <div class="left_text">
                    <?php if( get_field('swimming_lessons_text') ): ?>
                   <?php the_field('swimming_lessons_text'); ?> 
				<?php endif; ?>    
                    </div>
                    <div class="right_img">
                        <img src="<?php if( get_field('swimming_lesson_img') ): ?>
                   <?php the_field('swimming_lesson_img'); ?> 
				<?php endif; ?>" alt="Swimming Lessons">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="cricket_lesson">
               <div class="left_img">
                   <img src="<?php if( get_field('cricket_lesson_image') ): ?>
                   <?php the_field('cricket_lesson_image'); ?> 
				<?php endif; ?>" alt="Cricket Lessons">
               </div>
               <div class="left_text">
                 <?php if( get_field('cricket_lesson_text') ): ?>
                   <?php the_field('cricket_lesson_text'); ?> 
				<?php endif; ?> 
				</div>
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="football_service">
                <div class="left_text">
                  <?php if( get_field('foot_ball_coaching') ): ?>
                   <?php the_field('foot_ball_coaching'); ?> 
				<?php endif; ?>  
				   </div>
                <div class="right_img">
                    <img src="<?php if( get_field('football_coaching_image') ): ?>
                   <?php the_field('football_coaching_image'); ?> 
				<?php endif; ?>" alt="Football Coaching image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="tennis_lesson">
               <div class="left_img">
                   <img src="<?php if( get_field('tenis_lesson_image') ): ?>
                   <?php the_field('tenis_lesson_image'); ?> 
				<?php endif; ?>" alt="Tennis Lessons">
               </div>
               <div class="left_text">
                 <?php if( get_field('tenis_lesson_txt') ): ?>
                   <?php the_field('tenis_lesson_txt'); ?> 
				<?php endif; ?>  
				   </div>
           </div>            
        </div>     
        
        <div class="main_text_area_right_new">
            <div id="basketball_less">
                <div class="left_text">
                  <?php if( get_field('basketball_lesson_text') ): ?>
                   <?php the_field('basketball_lesson_text'); ?> 
				<?php endif; ?>  
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('basketball_lesson_image') ): ?>
                   <?php the_field('basketball_lesson_image'); ?> 
				<?php endif; ?>" alt="Basketball Lessons">
                </div>
            </div>
        </div> 
        
        <div id="getquort">
            <div class="inner_area">
             <?php if( get_field('get_quotes_now') ): ?>
                   <?php the_field('get_quotes_now'); ?> 
				<?php endif; ?>  
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(29);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=29&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(29);?>" title="<?php echo get_cat_name(29);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(29);?></a></strong></p>
</div>
 
</div>

</div>


<?php get_footer();?>
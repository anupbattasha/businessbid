<?php 
/**
template name: personal-training Page
**/

get_header();
?>

<!-- Start of Header Image Area -->
        <div id="presonal_trainer">
           <div class="header_slider">
               <div class="headesr_slide_text">
                <?php if( get_field('personal_trainer') ): ?>
                   <?php the_field('personal_trainer'); ?> 
				<?php endif; ?> 
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="personal_treaine_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('personal_trainingimg') ): ?>
								<?php the_field('personal_trainingimg'); ?> 
							<?php endif; ?>" alt="PERSONAL TRAINING">
               </div>
               <div class="left_text">
					<?php if( get_field('personal_trainingtxt') ): ?>
						<?php the_field('personal_trainingtxt'); ?> 
					<?php endif; ?>
			   </div>
               <div class="full_width">
					<?php if( get_field('personal_trainingdwntxt') ): ?>
						<?php the_field('personal_trainingdwntxt'); ?> 
					<?php endif; ?>
			   
			   </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort education">
					<?php if( get_field('hire_personal_trainer') ): ?>
						<?php the_field('hire_personal_trainer'); ?> 
					<?php endif; ?>
               <div class="child_quort">
					<?php if( get_field('fill_simpleform') ): ?>
						<?php the_field('fill_simpleform'); ?> 
					<?php endif; ?>
               </div>
               <div class="child_quort">
					<?php if( get_field('receive_quotation') ): ?>
						<?php the_field('receive_quotation'); ?> 
					<?php endif; ?>
               </div>
               <div class="child_quort">
					<?php if( get_field('hire_bestpersonal_trainer') ): ?>
						<?php the_field('hire_bestpersonal_trainer'); ?> 
					<?php endif; ?>
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
					<?php if( get_field('types_personal_training') ): ?>
						<?php the_field('types_personal_training'); ?> 
					<?php endif; ?>
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('focusimg') ): ?>
									<?php the_field('focusimg'); ?> 
								 <?php endif; ?>" alt="1:1 Focus">
								 <?php if( get_field('focustxt') ): ?>
									<?php the_field('focustxt'); ?> 
								 <?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('body_assesmentimg') ): ?>
									<?php the_field('body_assesmentimg'); ?> 
								 <?php endif; ?>" alt="Body Assessment">
								 <?php if( get_field('body_assesmenttxt') ): ?>
									<?php the_field('body_assesmenttxt'); ?> 
								 <?php endif; ?>
                       
                   </div>
				   
                   <div class="child_circle">
                       <img src="<?php if( get_field('personalized_programimg') ): ?>
									<?php the_field('personalized_programimg'); ?> 
								 <?php endif; ?>" alt="Personalized Programs">
								 <?php if( get_field('personalized_programimg') ): ?>
									<?php the_field('personalized_programimg'); ?> 
								 <?php endif; ?>
                       
                   </div>
				   
               </div>
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('exercise_and_fitnessimg') ): ?>
									<?php the_field('exercise_and_fitnessimg'); ?> 
								 <?php endif; ?>" alt="Exercise & Fitness Routine">
								 <?php if( get_field('exercise_and_fitnessimg') ): ?>
									<?php the_field('exercise_and_fitnessimg'); ?> 
								 <?php endif; ?>
                       
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('nutrition_managementimg') ): ?>
									<?php the_field('nutrition_managementimg'); ?> 
								 <?php endif; ?>" alt="Nutrition Management">
								 <?php if( get_field('nutrition_managementtxt') ): ?>
									<?php the_field('nutrition_managementtxt'); ?> 
								 <?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('rehabitation_injuryimg') ): ?>
									<?php the_field('rehabitation_injuryimg'); ?> 
								 <?php endif; ?>" alt="Rehabilitation & Injury Management">
								 <?php if( get_field('rehabitation_injurytxt') ): ?>
									<?php the_field('rehabitation_injurytxt'); ?> 
								 <?php endif; ?>
                   </div>
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="one_one_focus">
                <div class="left_text">
					<?php if( get_field('new_focustxt') ): ?>
									<?php the_field('new_focustxt'); ?> 
					<?php endif; ?>
				</div>    
                <div class="right_img">
                    <img src="<?php if( get_field('new_focusimg') ): ?>
									<?php the_field('new_focusimg'); ?> 
							  <?php endif; ?>" alt="1:1 Focus">
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="body_assessment">
               <div class="left_img">
                   <img src="<?php if( get_field('newbody_assesmentimg') ): ?>
									<?php the_field('newbody_assesmentimg'); ?> 
							  <?php endif; ?>" alt="Body Assessment">
               </div>
               <div class="left_text">
					<?php if( get_field('newbody_assesmenttxt') ): ?>
									<?php the_field('newbody_assesmenttxt'); ?> 
				    <?php endif; ?>
			   
			   </div>               
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="personalized_pero">
                <div class="left_text">
					<?php if( get_field('newpersonalized_program') ): ?>
									<?php the_field('newpersonalized_program'); ?> 
				    <?php endif; ?>	
				
                </div>
                <div class="right_img">
                    <img src="<?php if( get_field('newpersonalized_programimg') ): ?>
									<?php the_field('newpersonalized_programimg'); ?> 
				    <?php endif; ?>" alt="Personalized Programs image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="exercise_fit_rut">
               <div class="left_img">
                   <img src="<?php if( get_field('exercise_and_fitnessroutineimg') ): ?>
									<?php the_field('exercise_and_fitnessroutineimg'); ?> 
				    <?php endif; ?>" alt="Exercise & Fitness Routine Image">
               </div>
               <div class="left_text">
						<?php if( get_field('exercise_and_fitnessroutinetxt') ): ?>
									<?php the_field('exercise_and_fitnessroutinetxt'); ?> 
						<?php endif; ?>
               </div>
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="nutrition_manage">
                <div class="left_text">
					<?php if( get_field('newnutrition_managementtxt') ): ?>
									<?php the_field('newnutrition_managementtxt'); ?> 
					<?php endif; ?>
				</div>
                <div class="right_img">
                    <img src="<?php if( get_field('newnutrition_managementimg') ): ?>
									<?php the_field('newnutrition_managementimg'); ?> 
					<?php endif; ?>" alt="Nutrition Management image">
                </div>
            </div>
        </div>  
        
        <div class="main_text_area_left_new">
           <div id="rehabi_injury">
               <div class="left_img">
                   <img src="<?php if( get_field('newrehabitation_injuryimg') ): ?>
									<?php the_field('newrehabitation_injuryimg'); ?> 
					<?php endif; ?>" alt="Rehabilitation & Injury Management Image">
               </div>
               <div class="left_text">
					<?php if( get_field('newrehabitation_injurytxt') ): ?>
									<?php the_field('newrehabitation_injurytxt'); ?> 
					<?php endif; ?>
               </div>
           </div>            
        </div>                     
        
        <div id="getquort">
            <div class="inner_area">
					<?php if( get_field('get_quotes') ): ?>
									<?php the_field('get_quotes'); ?> 
					<?php endif; ?>
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(28);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=28&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(28);?>" title="<?php echo get_cat_name(28);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(28);?></a></strong></p>
</div>
 
</div>

</div>

<?php get_footer(); ?>
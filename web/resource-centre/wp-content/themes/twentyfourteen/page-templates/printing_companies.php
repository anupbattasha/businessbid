<?php
/**
 * Template Name: Printing Compaines
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('newhead'); ?>

        <!-- Main Content File here -->
        <div id="ac_header" class="printing222">
            <div class="cleaning_headder_inner">
                <div class="headesr_slide_text">
                   <?php echo get_post_meta($post->ID, 'printing_banner_text', true); ?>
                </div>               
           </div>
        </div>
        <div class="main_text_area_left_new">
            <div id="ac_pc">
                <div class="left_img">
                   <?php echo get_post_meta($post->ID, 'business_card_printing_img', true); ?>
                </div>
                <div class="left_text">
                    <?php echo get_post_meta($post->ID, 'business_card_printing_text', true); ?>
                </div>
            </div>
        </div>
        <div id="common_quort_area">
            <div class="inner_quort" style="width:727px !important;">
                <?php echo get_post_meta($post->ID, 'printing_easySteup_heading', true); ?>
                <div class="child_quort">
                   <?php echo get_post_meta($post->ID, 'printing_easySteup_one', true); ?>
                </div>
                <div class="child_quort">
                   <?php echo get_post_meta($post->ID, 'printing_easySteup_two', true); ?>
                </div>
                <div class="child_quort">
                    <?php echo get_post_meta($post->ID, 'printing_easySteup_three', true); ?>
                </div>
                <div class="new_get_quort_btn">
                   <?php echo get_post_meta($post->ID, 'printing_easySteup_link', true); ?>
                </div>
            </div>
        </div>
        <div id="common_circel_area">
            <div class="inner_circel">
               <?php echo get_post_meta($post->ID, 'printing_press_type_heading', true); ?>
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'business_card', true); ?>
                </div>
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'business_stationary', true); ?>                  
                </div>
                <div class="child_circle">
                   <?php echo get_post_meta($post->ID, 'event_promotion', true); ?>
                </div>
                <div class="child_circle">
                    <?php echo get_post_meta($post->ID, 'corporate_marketing', true); ?>
                </div>
            </div>
        </div>
        <div class="main_text_area_right_new">
            <div id="hvac_system">
			<div class="inner_hvac">
                <div class="left_text" style="margin-top:0 !important;">
                    <?php echo get_post_meta($post->ID, 'business_cards_text', true); ?>
                </div>
                <div class="right_img">
                   <?php echo get_post_meta($post->ID, 'business_cards_img', true); ?>
                </div>
            </div>
        </div>
		</div>
        <div class="main_text_area_left_new">
            <div id="split_pc">
                <div class="left_img">
                    <?php echo get_post_meta($post->ID, 'business_stationery_img', true); ?>
                </div>
                <div class="left_text" style="margin-top:0 !important;">
                    <?php echo get_post_meta($post->ID, 'business_stationery_text', true); ?>
                </div>
            </div>
        </div>
        <div class="main_text_area_right_new">
            <div id="hvac_system">
			<div class="inner_hvac">
                <div class="left_text" style="margin-top:0 !important;">
                   <?php echo get_post_meta($post->ID, 'event_promotion_text', true); ?>
                </div>
                <div class="right_img" >
                   <?php echo get_post_meta($post->ID, 'event_promotion_img', true); ?>
                </div>
            </div>
        </div>
		</div>
        <div class="main_text_area_left_new">
            <div id="split_pc">
                <?php echo get_post_meta($post->ID, 'corporate_marketing_text', true); ?>
            
			</div>
        </div>
        <div id="getquort">
            <div class="inner_area">
               <?php echo get_post_meta($post->ID, 'If_you_are_printing_press', true); ?>
                
                    <?php echo get_post_meta($post->ID, 'If_you_are_printing_press_link', true); ?>
                </div>
            </div>
                
		<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(11);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=11&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-sm-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(11);?>" title="<?php echo get_cat_name(11);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(11);?></a></strong></p>
</div>
 
</div>
</div>

 <?php
get_footer();

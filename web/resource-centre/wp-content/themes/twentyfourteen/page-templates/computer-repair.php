<?php 
/**
template name: computer-repair Page
**/

get_header();
?>


<!-- Start of Header Image Area -->
        <div id="pc_repair_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('computer_repair_company') ): ?>
                   <?php the_field('computer_repair_company'); ?> 
				<?php endif; ?>  
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="repair_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('computer_service_img') ): ?>
                   <?php the_field('computer_service_img'); ?> 
				<?php endif; ?> " alt="COMPUTER REPAIR SERVICES YOU CAN COUNT ON">
               </div>
               <div class="left_text">
			   <?php if( get_field('computer_service_text') ): ?>
                   <?php the_field('computer_service_text'); ?> 
				<?php endif; ?>
			   </div>
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort">
              <?php if( get_field('hire_comp_repair') ): ?>
                   <?php the_field('hire_comp_repair'); ?> 
				<?php endif; ?>
               <div class="child_quort">
                   <?php if( get_field('fill_sample') ): ?>
                   <?php the_field('fill_sample'); ?> 
				<?php endif; ?>
               </div>
               <div class="child_quort">
                  <?php if( get_field('receive_quotation') ): ?>
                   <?php the_field('receive_quotation'); ?> 
				<?php endif; ?> 
               </div>
               <div class="child_quort">
                   <?php if( get_field('hire_best_computer_repair') ): ?>
                   <?php the_field('hire_best_computer_repair'); ?> 
				<?php endif; ?> 
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
               <?php if( get_field('types_computer_repair_service') ): ?>
                   <?php the_field('types_computer_repair_service'); ?> 
				<?php endif; ?>
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('repair_fix_img') ): ?>
                   <?php the_field('repair_fix_img'); ?> 
				<?php endif; ?>" alt="Repair & Fix">
                      <?php if( get_field('repair_fix_txt') ): ?>
                   <?php the_field('repair_fix_txt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('system_upgrade_img') ): ?>
                   <?php the_field('system_upgrade_img'); ?> 
				<?php endif; ?>" alt="System Upgrades">
                       <?php if( get_field('system_upgrade_txt') ): ?>
                   <?php the_field('system_upgrade_txt'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('virus_protection_img') ): ?>
                   <?php the_field('virus_protection_img'); ?> 
				<?php endif; ?>" alt="Virus Protection">
                     <?php if( get_field('virus_protection_txt') ): ?>
                   <?php the_field('virus_protection_txt'); ?> 
				<?php endif; ?>
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('settingup_computer') ): ?>
                   <?php the_field('settingup_computer'); ?> 
				<?php endif; ?>" alt="Setting up your computer">
                       <?php if( get_field('settingup_computertxt') ): ?>
                   <?php the_field('settingup_computertxt'); ?> 
				<?php endif;?>
                   </div> 
                   <div class="child_circle">
                       <img src="<?php if( get_field('data_recoveryimg') ): ?>
                   <?php the_field('data_recoveryimg'); ?> 
				<?php endif;?>" alt="Data Recovery">
				<?php if( get_field('data_recoverytxt') ): ?>
                   <?php the_field('data_recoverytxt'); ?> 
				<?php endif;?>
                       
                   </div>                   
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="repair_and_fix">
                <div class="inner_hvac">
                    <div class="left_text">
					<?php if( get_field('repair_fixtxt') ): ?>
                   <?php the_field('repair_fixtxt'); ?> 
				<?php endif;?>
                        </div>
                    <div class="right_img">
                        <img src="<?php if( get_field('repair_fiximg') ): ?>
                   <?php the_field('repair_fiximg'); ?> 
				<?php endif;?>" alt="Repair & Fix">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="data_updratadion">
               <div class="left_img">
                   <img src="<?php if( get_field('systemup_img') ): ?>
                   <?php the_field('systemup_img'); ?> 
				<?php endif;?>" alt="System upgrades">
               </div>
               <div class="left_text">
			   <?php if( get_field('systemup_txt') ): ?>
                   <?php the_field('systemup_txt'); ?> 
				<?php endif;?>
               </div>    
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="virus_protection">
                <div class="left_text">
				<?php if( get_field('virs_protxt') ): ?>
                   <?php the_field('virs_protxt'); ?> 
				<?php endif;?>
                    </div>
                <div class="right_img">
                    <img src="<?php if( get_field('virs_protimg') ): ?>
                   <?php the_field('virs_protimg'); ?> 
				<?php endif;?>" alt="Virus protection image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="setup_cp">
               <div class="left_img">
                   <img src="<?php if( get_field('settup_comimg') ): ?>
                   <?php the_field('settup_comimg'); ?> 
				<?php endif;?>" alt="Setting up your computer Image">
               </div>
               <div class="left_text">
			   <?php if( get_field('settup_comtxt') ): ?>
                   <?php the_field('settup_comtxt'); ?> 
				<?php endif;?>
               </div>
           </div>            
        </div>     
        
        <div class="main_text_area_right_new">
            <div id="data_takeover">
                <div class="left_text">
				<?php if( get_field('datarec_txt') ): ?>
                   <?php the_field('datarec_txt'); ?> 
				<?php endif;?>
                </div>
                <div class="right_img">
                    <img src="<?php if( get_field('datarec_img') ): ?>
                   <?php the_field('datarec_img'); ?> 
				<?php endif;?>" alt="Data Recovery image">
                </div>
            </div>
        </div>            
        
        <div id="getquort">
            <div class="inner_area">
                <?php if( get_field('get_quotes') ): ?>
                   <?php the_field('get_quotes'); ?> 
				<?php endif;?>
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(25);?> Companies in UAE</p>
</div>
<div class="row">
<?php
$catquery = new WP_Query( 'cat=25&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>
</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(25);?>" title="<?php echo get_cat_name(25);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(25);?></a></strong></p>
</div>
 
</div>

</div>


<?php get_footer(); ?>
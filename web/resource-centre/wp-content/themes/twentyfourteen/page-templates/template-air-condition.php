 <?php 
/**
template name: air conditioning Page
**/

get_header('newhead');
?>
<!-- Start of Header Image Area -->
        <div id="ac_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
				<?php if( get_field('editor_text') ): ?>
                   <?php the_field('editor_text'); ?> 
				<?php endif; ?>				   
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="ac_pc">
               <div class="left_img">
                   <img src="<?php the_field('old_aircontion'); ?>" alt="AIR CONDITIONING SERVICES THAT YOU CAN COUNT ON">
               </div>
               <div class="left_text">
               <?php if( get_field('left_text') ): ?>
                   <?php the_field('left_text'); ?> 
				<?php endif; ?>	
			   
			   </div>
               <div class="full_width">
               <?php if( get_field('full_width') ): ?>
                   <?php the_field('full_width'); ?> 
				<?php endif; ?>	
			   
			   </div>    
           </div>            
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort">
              <?php if( get_field('hire_con') ): ?>
                   <?php the_field('hire_con'); ?> 
				<?php endif; ?>	
               <div class="child_quort">
                   <?php if( get_field('fill_out') ): ?>
                   <?php the_field('fill_out'); ?> 
				<?php endif; ?>	
               </div>
               <div class="child_quort">
                   <?php if( get_field('receive_quotation') ): ?>
                   <?php the_field('receive_quotation'); ?> 
				<?php endif; ?>	
               </div>
               <div class="child_quort">
                   <?php if( get_field('hire_best') ): ?>
                   <?php the_field('hire_best'); ?> 
				<?php endif; ?>	
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
               <?php if( get_field('air_cooling_services') ): ?>
                   <?php the_field('air_cooling_services'); ?> 
				<?php endif; ?>
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('hha_vc') ): ?>
                   <?php the_field('hha_vc'); ?> 
				<?php endif; ?>" alt="HVAC">
                    <?php if( get_field('havc_text') ): ?>
                   <?php the_field('havc_text'); ?> 
				<?php endif; ?>   
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('spl_sys') ): ?>
                   <?php the_field('spl_sys'); ?> 
				<?php endif; ?>" alt="Split System Air Conditioning">
                       <?php if( get_field('system_air_conditioning') ): ?>
                   <?php the_field('system_air_conditioning'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('port_aircon') ): ?>
                   <?php the_field('port_aircon'); ?> 
				<?php endif; ?>" alt="Portable Air Conditioning ">
                    <?php if( get_field('portable_air_conditioning') ): ?>
                   <?php the_field('portable_air_conditioning'); ?> 
				<?php endif; ?>   
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('air_cool') ): ?>
                   <?php the_field('air_cool'); ?> 
				<?php endif; ?>" alt="Air Coolers ">
                    <?php if( get_field('aiir_coller') ): ?>
                   <?php the_field('aiir_coller'); ?> 
				<?php endif; ?>   
                   </div> 
                   <div class="child_circle">
                       <img src="<?php if( get_field('mist_cool') ): ?>
                   <?php the_field('mist_cool'); ?> 
				<?php endif; ?>" alt="Mist Cooling ">
                     <?php if( get_field('mist_coolingg') ): ?>
                   <?php the_field('mist_coolingg'); ?> 
				<?php endif; ?>  
                   </div>                   
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="hvac_system">
                <div class="inner_hvac">
                    <div class="left_text">
					<?php if( get_field('hvac_sys') ): ?>
                   <?php the_field('hvac_sys'); ?> 
				<?php endif; ?>
                    </div>
                    <div class="right_img">
					
                        <img src="<?php if( get_field('hvac') ): ?>
                   <?php the_field('hvac'); ?> 
				<?php endif; ?>" alt="HVAC system">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="split_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('split_system') ): ?>
                   <?php the_field('split_system'); ?> 
				<?php endif; ?>" alt="Split System">
               </div>
               <div class="left_text">
			   <?php if( get_field('splisys_aircon') ): ?>
                   <?php the_field('splisys_aircon'); ?> 
				<?php endif; ?>
			   </div>   
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="portable_area">
                <div class="left_text">
                <?php if( get_field('port_aircond') ): ?>
                   <?php the_field('port_aircond'); ?> 
				<?php endif; ?>  
				</div>
                <div class="right_img">
                    <img src="<?php if( get_field('por_air') ): ?>
                   <?php the_field('por_air'); ?> 
				<?php endif; ?>" alt="Portable Air Conditioner image">
                </div>
            </div>
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="air_coolers_area">
               <div class="left_img">
                   <img src="<?php if( get_field('airs_coller') ): ?>
                   <?php the_field('airs_coller'); ?> 
				<?php endif; ?>" alt="Air Coolers Image">
               </div>
               <div class="left_text">
                 <?php if( get_field('air_coolers') ): ?>
                   <?php the_field('air_coolers'); ?> 
				<?php endif; ?>   
			 </div>
           </div>            
        </div>     
        
        <div class="main_text_area_right_new">
            <div id="mist_area">
                <div class="left_text">
                  <?php if( get_field('mist_cooling') ): ?>
                   <?php the_field('mist_cooling'); ?> 
				<?php endif; ?>  
					
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('mist_colling') ): ?>
                   <?php the_field('mist_colling'); ?> 
				<?php endif; ?>" alt="Mist Cooling image">
                </div>
            </div>
        </div> 
        
        <div id="getquort">
            <div class="inner_area">
                <?php if( get_field('get_quote') ): ?>
                   <?php the_field('get_quote'); ?> 
				<?php endif; ?>
            </div>
        </div>
		        <div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(21);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=21&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(21);?>" title="<?php echo get_cat_name(21);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(21);?></a></strong></p>
</div>
 
</div>

</div>
<?php get_footer(); ?>
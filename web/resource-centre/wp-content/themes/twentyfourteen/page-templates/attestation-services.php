<?php 
/**
template name: attestation-services Page
**/

get_header();
?>
 <!-- Start of Header Image Area -->
        <div id="attest_header">
           <div class="header_slider">
               <div class="headesr_slide_text">
                   <?php if( get_field('start_freequote') ): ?>
                   <?php the_field('start_freequote'); ?> 
				<?php endif; ?>  
               </div>
           </div>            
        </div>
       <!-- End of Header Image Area -->
       
       <!-- Start of left Image Area -->
        <div class="main_text_area_left_new">
           <div id="attast_pc">
               <div class="left_img">
                   <img src="<?php if( get_field('comp_img') ): ?>
                   <?php the_field('comp_img'); ?> 
				<?php endif; ?> " alt="ATTESTATION SERVICES">
               </div>
               <div class="left_text">
               <?php if( get_field('attestatin_ser') ): ?>
                   <?php the_field('attestatin_ser'); ?> 
				<?php endif; ?>
			   </div>
        </div>
       <!-- End of left Image Area --> 
             
       <!-- Quort Area -->
       <div id="common_quort_area">
           <div class="inner_quort attastation">
              <?php if( get_field('hire_attes') ): ?>
                   <?php the_field('hire_attes'); ?> 
				<?php endif; ?>
               <div class="child_quort">
                <?php if( get_field('fill_out') ): ?>
                   <?php the_field('fill_out'); ?> 
				<?php endif; ?>   
               </div>
               <div class="child_quort">
                 <?php if( get_field('receive_two') ): ?>
                   <?php the_field('receive_two'); ?> 
				<?php endif; ?>  
               </div>
               <div class="child_quort">
                 <?php if( get_field('hire_best') ): ?>
                   <?php the_field('hire_best'); ?> 
				<?php endif; ?>    
               </div>
           </div>
       </div>   
       
       <!-- Circel Area -->  
       <div id="common_circel_area">
           <div class="inner_circel">
             <?php if( get_field('type_attention') ): ?>
                   <?php the_field('type_attention'); ?> 
				<?php endif; ?>      
               <div class="comm_three">
                   <div class="child_circle">
                       <img src="<?php if( get_field('marriage_certificates') ): ?>
                   <?php the_field('marriage_certificates'); ?> 
				<?php endif; ?>" alt="Marriage Certificates">
                       <?php if( get_field('mallage_certi') ): ?>
                   <?php the_field('mallage_certi'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('birth_cert') ): ?>
                   <?php the_field('birth_cert'); ?> 
				<?php endif; ?>" alt="Birth Certificates">
                       <?php if( get_field('birth_certificates') ): ?>
                   <?php the_field('birth_certificates'); ?> 
				<?php endif; ?>
                   </div>
                   <div class="child_circle">
                       <img src="<?php if( get_field('child_wife') ): ?>
                   <?php the_field('child_wife'); ?> 
				<?php endif; ?>" alt="Child & Wife Sponsorship">
                       <?php if( get_field('children_wife') ): ?>
                   <?php the_field('children_wife'); ?> 
				<?php endif; ?>
                   </div>
               </div>
               <div class="comm_two">
                   <div class="child_circle">
                       <img src="<?php if( get_field('education_de') ): ?>
                   <?php the_field('education_de'); ?> 
				<?php endif; ?>" alt="Education Degrees">
                     <?php if( get_field('edu_degree') ): ?>
                   <?php the_field('edu_degree'); ?> 
				<?php endif; ?>  
                   </div> 
                   <div class="child_circle">
                       <img src="<?php if( get_field('trans_services') ): ?>
                   <?php the_field('trans_services'); ?> 
				<?php endif; ?>" alt="Translation Services">
                       <?php if( get_field('traslation_ser') ): ?>
                   <?php the_field('traslation_ser'); ?> 
				<?php endif; ?>
                   </div>                   
               </div>
           </div>
       </div>        
        
        <div class="main_text_area_right_new">
            <div id="marriage_cer">
                <div class="inner_hvac">
                    <div class="left_text">
                       <?php if( get_field('certificate_attestation') ): ?>
                   <?php the_field('certificate_attestation'); ?> 
				<?php endif; ?> 
				</div>
                    <div class="right_img">
                        <img src="<?php if( get_field('cer_attestation') ): ?>
                   <?php the_field('cer_attestation'); ?> 
				<?php endif; ?> " alt="Marriage Certificate">
                    </div>
                    <div class="full_width">
                     <?php if( get_field('after_marrge') ): ?>
                   <?php the_field('after_marrge'); ?> 
				<?php endif; ?>  
			</div>                    
                </div>
            </div>
        </div>
        
        <div class="main_text_area_left_new">
           <div id="birth_certificate">
               <div class="left_img">
                   <img src="<?php if( get_field('birth_certi') ): ?>
                   <?php the_field('birth_certi'); ?> 
				<?php endif; ?>" alt="Birth Certificate">
               </div>
               <div class="left_text">
                <?php if( get_field('birth_dubai') ): ?>
                   <?php the_field('birth_dubai'); ?> 
				<?php endif; ?>   
				   </div>
               <div class="full_width">
                 <?php if( get_field('born_dubaii') ): ?>
                   <?php the_field('born_dubaii'); ?> 
				<?php endif; ?>      
					</div>
           </div>            
        </div>
        
        <div class="main_text_area_right_new">
            <div id="child_sponsorship">
                <div class="left_text">
                 <?php if( get_field('spouse_sponsorship') ): ?>
                   <?php the_field('spouse_sponsorship'); ?> 
				<?php endif; ?>   
					</div>
            <div class="right_img">
                <img src="<?php if( get_field('spo_sponsor') ): ?>
                   <?php the_field('spo_sponsor'); ?> 
				<?php endif; ?>" alt="Child and Spouse Sponsorship image">
            </div>            
        </div>        
        
        <div class="main_text_area_left_new">
           <div id="education_attestation">
               <div class="left_img">
                   <img src="<?php if( get_field('attestation_image') ): ?>
                   <?php the_field('attestation_image'); ?> 
				<?php endif; ?>" alt="Education Degrees Attestation Image">
               </div>
               <div class="left_text">
                 <?php if( get_field('eduction_page') ): ?>
                   <?php the_field('eduction_page'); ?> 
				<?php endif; ?>  
				   </div>
           </div>            
        </div>     
        
        <div class="main_text_area_right_new">
            <div id="translation_services">
                <div class="left_text">
                  <?php if( get_field('services_in_dubai') ): ?>
                   <?php the_field('services_in_dubai'); ?> 
				<?php endif; ?>  
					</div>
                <div class="right_img">
                    <img src="<?php if( get_field('translation_services_image') ): ?>
                   <?php the_field('translation_services_image'); ?> 
				<?php endif; ?>" alt="Translation Services image">
                </div>
            </div>
        </div> 
        
        <div id="getquort">
            <div class="inner_area">
                <?php if( get_field('get_quotes_now') ): ?>
                   <?php the_field('get_quotes_now'); ?> 
				<?php endif; ?> 
            </div>
        </div>
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(30);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=30&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(30);?>" title="<?php echo get_cat_name(30);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(30);?></a></strong></p>
</div>
 
</div>

</div>

<?php get_footer(); ?>
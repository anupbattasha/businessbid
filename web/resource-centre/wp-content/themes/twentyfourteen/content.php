<div id="post-<?php the_ID(); ?>">
  <div class="postDetail">
    <div class="blog_heading">
    
    
  <a href="<?php the_permalink(); ?>"><?php the_title('<p>', '</p>'); ?></a>   
      
<?php /*?><?php
if (is_single($post) ) {?>
     <a href="<?php the_permalink(); ?>">
      <?php the_title('<h1>', '</h1>'); ?>
      </a>
  <?php } else {?>
	  
<a href="<?php the_permalink(); ?>">
      <?php the_title('<h2>', '</h2>'); ?>
      </a>
						<? }?><?php */?>

      
      
      
      
       </div>
    <div class="post-meta">
      <?php

						if (!is_page()) {

						?>
      <div class="authorDetail">
        <div class="authorImage">
          <!-- displays the user's photo and then thumbnail -->
          <?php userphoto_the_author_photo() ?>
        </div>
        <div class="authorName"> By <span class="nameDark"> <span class="vcard" style="text-transform:capitalize;"> <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>" title="<?php the_author() ?>"  class="url fn n">
          <?php the_author() ?>
          </a> </span> </span> </div>
        <div class="authorLeft">
          <div class="postDate"> <?php echo get_the_date(); ?> </div>
          <div class="postComment">
            <?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?>
          </div>
        </div>
      </div>
      <div class="blogImg">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a>
      </div>
      <?php

						}

						?>
    </div>
  </div>
  <span class="blogText ssss">
  <?php

							if (is_archive() || is_search() || is_category() || is_tag() || is_author() || is_home()) {

								//echo the_excerpt(80);

							?>
                            
                            
                            
  <div class="aftertext"><a class="CountBtn" href="<?php the_permalink(); ?>">Continue reading</a> <span class="blogSocial"> <?php echo do_shortcode('[simple-social-share]') ;?> </span> </div>
  <?php

							}
							
							


							else {

								the_content();

							}

						?>
  </span> </div>
<?php

		if (is_single())  {

						?>
<div class="aboutAuthor">
  <div class="blogQueots">
    <div class="needquote">Need a quote for <?php the_category(', ') ?>?
  </div>
    <?php if (in_category('2')) : ?>
    <?php dynamic_sidebar( 'sidebar-5' ); ?>
    <?php elseif (in_category('3')) : ?>
    <?php dynamic_sidebar( 'sidebar-6' ); ?>
    <?php elseif (in_category('4')) : ?>
    <?php dynamic_sidebar( 'sidebar-7' ); ?>
    <?php elseif (in_category('5')) : ?>
    <?php dynamic_sidebar( 'sidebar-8' ); ?>
    <?php elseif (in_category('6')) : ?>
    <?php dynamic_sidebar( 'sidebar-9' ); ?>
    <?php elseif (in_category('7')) : ?>
    <?php dynamic_sidebar( 'sidebar-10' ); ?>


    <?php elseif (in_category('9')) : ?>
    <?php dynamic_sidebar( 'sidebar-12' ); ?>

    <?php elseif (in_category('10')) : ?>
    <?php dynamic_sidebar( 'sidebar-13' ); ?>

    <?php elseif (in_category('11')) : ?>
    <?php dynamic_sidebar( 'sidebar-14' ); ?>
    
     <?php elseif (in_category('12')) : ?>
    <?php dynamic_sidebar( 'sidebar-11' ); ?>
    
    <?php else : ?>
    <?php endif; ?>
  </div>
</div>

<div class="relatedArticles">
  <?php //wp_related_posts()?>
</div>
<?php

}?>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<?php if ( is_category() || is_author()) { ?>
<meta name="robots" content="noindex,nofollow" />
<?php } else { ?>
<meta name="robots" content="index,follow" />
<?php } if (have_posts()):while(have_posts()):the_post(); endwhile; endif;
if (is_category()) {  get_home_url(); ?>
<meta http-equiv="content-type" content="<?php bloginfo('html_type');?>character=<?php bloginfo('charset');?>">


<meta name="robots" content="<?php category_description(); ?>" />

<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<meta property="og:type" content="website" />
<meta property="og:image" content="logo.jpg" /> <?php } ?>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<title><?php wp_title('|', true, 'left'); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--new css-->
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/new_page/main.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/new_page/normalize.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/style.css">
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assest/css/main.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assest/css/new_page/responsive.css">

<!-- <link type="text/css" rel="stylesheet" href="<?php //echo home_url('/');?>wp-content/themes/hostmarks/css/style.css"> -->
<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
<!--<link type="text/css" rel="stylesheet" href="wp-content/themes/hostmarks/css/jkmegamenu.css">-->
<!--<script src="wp-content/themes/twentyfourteen/js/jquery-1.10.2.min.js"></script>-->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<!-- <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script src="http://<?php echo $_SERVER[HTTP_HOST]; ?>/js/bootstrap.js"></script>
<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript">stLight.options({publisher: "916c9cfb-edd5-46f1-b024-c9d477cef57a", doNotHash: false, doNotCopy: false, hashAddressBar: false});

</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
  	<script src="{{ asset('js/html5shiv.min.js') }}"></script>
  	<script src="{{ asset('js/respond.min.js') }}"></script>
	<![endif]-->
<script>
	$(document).ready(function(){
		var realpath = window.location.protocol + "//" + window.location.host + "/";
	$('#category').autocomplete({
		      	source: function( request, response ) {
		      		$.ajax({
		      			url : realpath+"ajax-info.php",
		      			dataType: "json",
						data: {
						   name_startsWith: request.term,
						   type: 'category'
						},
						 success: function( data ) {
							 response( $.map( data, function( item ) {
								return {
									label: item,
									value: item
								}
							}));
						}
		      		});
		      	},
		      	autoFocus: true,
		      	minLength: 2
		      });

});

</script>
<script type='text/javascript' >

	function jsfunction(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						//document.getElementById("searchval").innerHTML=xmlhttp.responseText;
						megamenubycatAjax();
						megamenubycatVendorSearchAjax();
						megamenubycatVendorReviewAjax();
						megamenublogAjax();
					}
				  }
				xmlhttp.open("GET",realpath+"ajax-info.php",true);
				xmlhttp.send();



		}

		function megamenublogAjax(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						document.getElementById("megamenu1").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET",realpath+"mega-menublog.php",true);
				xmlhttp.send();



		}

	function megamenubycatAjax(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						document.getElementById("megamenubycat").innerHTML=xmlhttp.responseText;

					}
				  }
				xmlhttp.open("GET",realpath+"mega-menubycat.php",true);
				xmlhttp.send();



		}

		function megamenubycatVendorSearchAjax(){
				var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					//alert(xmlhttp.responseText);
					//console.log(xmlhttp.responseText);
						var mvsa = document.getElementById("megamenuVenSearchAnch");
						for (var i = 0; i < mvsa.childNodes.length; i++) {
							if (mvsa.childNodes[i].className == "dropdown-menu Searchsegment-drop") {
								mvsa.childNodes[i].innerHTML = xmlhttp.responseText;
							  break;
							}
						}
					//document.getElementById("megamenuVenSearchAnch").innerHTML = xmlhttp.responseText;
					}
				}
				//xmlhttp.open("GET",realpath+"mega-menubycat.php?type=vendorSearch",true);
				xmlhttp.open("GET",realpath+"vendorReviewDropdown.php?type=vendorSearch",true);
				xmlhttp.send();



	}
	function megamenubycatVendorReviewAjax(){


			var realpath = window.location.protocol + "//" + window.location.host + "/";
				var xmlhttp;
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
							//alert(xmlhttp.responseText);
						var mvsa = document.getElementById("megamenuVenReviewAnch");
						for (var i = 0; i < mvsa.childNodes.length; i++) {
							if (mvsa.childNodes[i].className == "dropdown-menu Reviewsegment-drop") {
								mvsa.childNodes[i].innerHTML = xmlhttp.responseText;
							  break;
							}
						}
						//document.getElementById("megamenuVenReviewUl").innerHTML = xmlhttp.responseText;
					}
				  }
				//xmlhttp.open("GET",realpath+"mega-menubycat.php?type=vendorReview",true);
				xmlhttp.open("GET",realpath+"vendorReviewDropdown.php?type=vendorReview",true);
				xmlhttp.send();



		}
</script>


<script type="text/javascript">
function show_submenu(b, menuid)
{


	document.getElementById(b).style.display="block";
	if(menuid!="megaanchorbycat"){
		document.getElementById(menuid).className = "setbg";
	}
}

function hide_submenu(b, menuid)
{


		setTimeout(dispminus(b,menuid), 200);



			//document.getElementById(b).onmouseover = function() { document.getElementById(b).style.display="block"; }
			//document.getElementById(b).onmouseout = function() { document.getElementById(b).style.display="none"; }
			//document.getElementById(b).style.display="none";
}

function dispminus(subid,menuid){
	if(menuid!="megaanchorbycat"){
				document.getElementById(menuid).className = "unsetbg";
	}
				document.getElementById(subid).style.display="none";


			}


</script>
<script type="text/javascript">
window.fbAsyncInit = function() {
	FB.init({
	appId      : '754756437905943', // replace your app id here
	status     : true,
	cookie     : true,
	xfbml      : true
	});
};
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
	FB.login(function(response){
		if(response.authResponse){
			window.location.href = "//WWW.businessbid.ae/devfbtestlogin/actions.php?action=fblogin";
		}
	}, {scope: 'email,user_likes'});
}
</script>
<script type="text/javascript">
$(document).ready(function(){
$( ".six_tabil #category" ).keydown(function() {
$("#ui-id-1").remove();
});
$('#newsletterwidget-3').removeClass('widget');
});
</script>
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59054368-1', 'auto');
  ga('send', 'pageview');

</script>
<meta name="google-site-verification" content="xyB15PaP0la0qMa8xOYBRacAXqGcvL4eoSnzHX4QlZQ" />
</head>

  <script>

  jQuery(document).ready(function(){
  var url=jQuery(location).attr('href');
  if(url == 'http://www.businessbid.ae/resource-centre/home-2/'){
    window.location.assign("http://www.businessbid.ae/resource-centre/home/");
    }
  });

</script>
<script type="text/javascript">
window.onload = jsfunction();
//window.onload = getcharactercount();
//window.onload = megamenuAjax();
</script>
<script type="text/javascript">
        $(function() {
            var offset = $("#sidebar").offset();
            var topPadding = 10;
            $(document).scroll(function() {
                if (($(window).scrollTop() > offset.top) && ($(window).scrollTop()+$(window).height() < $(document).height()-150)) {
                    $("#sidebar").stop().animate({
                        marginTop: $(document).scrollTop() - offset.top + topPadding
                    });
                } else {
                   /* $("#sidebar").stop().animate({
                        marginTop: 0
                    });*/
                };
            });
        });
    </script>
<body <?php body_class(); ?>>

<!-- Add your site or application content here -->
<div class="main_header_area">
	<div class="inner_headder_area">
		<div class="main_logo_area"><a href="http://www.businessbid.ae"><img src="<?php echo get_template_directory_uri();?>/assest/img/logo.png" alt="Business Bid Logo"></a></div>
		<div class="facebook_login_area">
			<div class="phone_top_area">
				<h3>(04) 42 13 777</h3>
			</div>
			<div class="fb_login_top_area">
				<ul>
					<li><a href="/login"><img src="<?php echo get_template_directory_uri();?>/assest/img/sign_in_btn.png" alt="Sign In"></a></li>
					<li><img src="<?php echo get_template_directory_uri();?>/assest/img/fb_sign_in.png" alt="Facebook Sign In" onClick="FBLogin();"></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="mainmenu_area">
	<div class="inner_main_menu_area">
		<div class="original_menu">
			<ul id="nav">
				<li><a href="http://www.businessbid.ae">Home</a></li>
				<li><a href="/vendor/search/home">Find Services Professionals</a>
					<ul>
						<li><a class="commercial" href="/post/quote/bysearch/inline/47">Commercial Cleaning Quotes</a></li>
						<li><a class="photography" href="/post/quote/bysearch/inline/114">Photography & Film Quotes</a></li>
						<li><a class="landscaping" href="/post/quote/bysearch/inline/93">Landscaping & Garden Quotes</a></li>
						<li><a class="control" href="/post/quote/bysearch/inline/112">Pest Control Quotes</a></li>
						<li><a class="maintenance" href="/post/quote/bysearch/inline/994">Maintenance Quotes</a></li>
						<li><a class="offset  dubble" href="/post/quote/bysearch/inline/79">Business Cards & Printing Quotes</a></li>
						<li><a class="wedding" href="/post/quote/bysearch/inline/141">Wedding Planners Quotes</a></li>
						<li><a class="bouquet" href="/post/quote/bysearch/inline/1018">Florists Quotes</a></li>
						<li><a class="catering" href="/post/quote/bysearch/inline/45">Catering Services Quotes</a></li>
						<li><a class="auditing" href="/post/quote/bysearch/inline/14">Accounting & Auditing Quotes</a></li>
						<li><a class="interior" href="/post/quote/bysearch/inline/87">Interior Design & Fit Out Quotes</a></li>
						<li><a class="give_gift" href="/post/quote/bysearch/inline/18">Gifts & Promotional Item Quotes</a></li>
						<li><a class="support" href="/post/quote/bysearch/inline/89">IT Support Quotes</a></li>
						<li><a class="all-category" href="/vendor/search/home">See All Categories</a></li>
					</ul>
				</li>
				<li><a href="/node4">Search reviews</a>
					<ul>
						<li><a class="commercial" href="/reviews/list/14">Commercial Cleaning Reviews</a></li>
						<li><a class="photography" href="/reviews/list/14">Photography & Film Reviews</a></li>
						<li><a class="landscaping" href="/reviews/list/14">Landscaping & Garden Reviews</a></li>
						<li><a class="control" href="/reviews/list/14">Pest Control Reviews</a></li>
						<li><a class="maintenance" href="/reviews/list/14">Maintenance Reviews</a></li>
						<li><a class="offset dubble" href="/reviews/list/14">Business Cards & Printing Reviews</a></li>
						<li><a class="wedding" href="/reviews/list/14">Wedding Planners Reviews</a></li>
						<li><a class="bouquet" href="/reviews/list/14">Florists Reviews</a></li>
						<li><a class="catering" href="/reviews/list/14">Catering Services Reviews</a></li>
						<li><a class="auditing" href="/reviews/list/14">Accounting & Auditing Reviews</a></li>
						<li><a class="interior" href="/reviews/list/14">Interior Design & Fit Out Reviews</a></li>
						<li><a class="give_gift" href="/reviews/list/14">Gifts & Promotional Item Reviews</a></li>
						<li><a class="support" href="/reviews/list/14">IT Support Reviews</a></li>
						<li><a class="all-reviews" href="/node4">See All Reviews</a></li>
					</ul>
				</li>
				<li><a href="http://www.businessbid.ae/resource-centre/home/">resource centre</a>
					<ul>
						<li><a class="commercial" href="http://www.businessbid.ae/resource-centre/cleaning-services/">Commercial Cleaning Articles</a></li>
						<li><a class="photography" href="http://www.businessbid.ae/resource-centre/photographers/">Photography & Film Articles</a></li>
						<li><a class="landscaping" href="http://www.businessbid.ae/resource-centre/landscaping-gardening-companies/">Landscaping & Garden Articles</a></li>
						<li><a class="control" href="http://www.businessbid.ae/resource-centre/pest-control-companies/">Pest Control Articles</a></li>
						<li><a class="maintenance" href="http://www.businessbid.ae/resource-centre/maintenance-handyman-companies">Maintenance Articles</a></li>
						<li><a class="offset dubble" href="http://www.businessbid.ae/resource-centre/printing-press">Business Cards & Printing Articles</a></li>
						<li><a class="wedding" href="http://www.businessbid.ae/resource-centre/wedding-planners">Wedding Planners Articles</a></li>
						<li><a class="bouquet" href="http://www.businessbid.ae/resource-centre/florists">Florists Articles</a></li>
						<li><a class="catering" href="http://www.businessbid.ae/resource-centre/catering-companies">Catering Services Articles</a></li>
						<li><a class="auditing" href="http://www.businessbid.ae/resource-centre/accounting-audit-firms">Accounting & Auditing Articles</a></li>
						<li><a class="interior" href="http://www.businessbid.ae/resource-centre/interior-design-companies">Interior Design & Fit Out Articles</a></li>
						<li><a class="give_gift" href="http://www.businessbid.ae/resource-centre/gifts-and-promotional-items">Gifts & Promotional Item Articles</a></li>
						<li><a class="support" href="http://www.businessbid.ae/resource-centre/IT-support-companies">IT Support Articles</a></li>
						<!--<li><a class="acrepair" href="http://www.businessbid.ae/resource-centre/ac-repair/">AC REPAIR</a></li>
						<li><a class="attestation" href="http://www.businessbid.ae/resource-centre/attestation-services/">ATTESTATION SERVICES</a></li>
						<li><a class="babysitting" href="http://www.businessbid.ae/resource-centre/babysitting-nannies/">BABYSITTING NANNIES</a></li>
						<li><a class="computerrepair" href="http://www.businessbid.ae/resource-centre/computer-repair/">COMPUTER REPAIR</a></li>
						<li><a class="edulearning" href="http://www.businessbid.ae/resource-centre/education-and-learning/">EDUCATION $ LEARNING</a></li>
						<li><a class="movestorecompany" href="http://www.businessbid.ae/resource-centre/moving-storage-company/">MOVING STORAGE COMPANY</a></li>
						<li><a class="persontrainer" href="http://www.businessbid.ae/resource-centre/personal-trainer/">PERSONAL TRAINER</a></li>
						<li><a class="petservices" href="http://www.businessbid.ae/resource-centre/pet-services/">PET SERVICES</a></li>
						<li><a class="eventmakeup" href="http://www.businessbid.ae/resource-centre/event-make-up-styling/">EVENT MAKEUP STYLING</a></li>
						<li><a class="swimmingsport" href="http://www.businessbid.ae/resource-centre/swimming-sports-coaching/">SWIMMING SPORT</a></li>-->
						<li><a class="all-articles" href="http://www.businessbid.ae/resource-centre/home">See All Articles</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="register_menu">
			<div class="register_menu_btn"><a href="/are-you-a-vendor">Register Your Business</a></div>
		</div>
	</div>
</div>
<div class="adjust"></div>
<!-- End of Header Area -->

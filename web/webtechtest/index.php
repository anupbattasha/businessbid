<?php

include ("createpaymenttoken.php");
$token = $paymentToken->getId();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Evisasworld" />
        <meta name="keywords" content="Evisasworld" />
        <title>Checkout Demo</title>
    </head>
    <body>

        <script src="https://sandbox.checkout.com/cdn/js/checkout.js"></script>

            <form class="payment-form">
                <script>
                  var publickey = 'pk_test_519b2513-80ad-4f1f-8524-1d583f5ccbbd'; //PublicKey provided by Checkout

                  Checkout.render({
                      debugMode: true,
                      namespace: 'CheckoutIntegration',
                      publicKey: publickey,
                      paymentToken: "<?php echo $token;?>",
                      customerEmail: 'test@gmail.com',
                      customerName: 'Testing',
                      paymentMode: 'card',
                      value: 100,
                      currency: 'EUR',
                      forceMobileRedirect: false,
                      payButtonSelector: '#payButtonId',
                      widgetContainerSelector: '.payment-form',
                      useCurrencyCode: false,
                      

                       styling: {
                            widgetColor: 'rgba(204, 204, 204, 0.16)',
                            themeColor: '',
                            buttonColor: '',
                            buttonLabelColor: '',
                            logoUrl: '',
                            formButtonColor: '',
                            formButtonLabelColor: '',
                            iconColor: '',
                            overlayShade: 'dark',
                            overlayOpacity: 0.8,
                            buttonLabel: 'Visa Checkout'
                        },

                 
                      cardCharged: function (event) {
                          document.getElementById('cko-cc-paymenToken').value = event.data.paymentToken;
                          document.getElementById('payment-tokenform').submit();
                      }

                  });
                </script>

            </form>
            <form name="payment-tokenform" method="post" id="payment-tokenform" action="verify.php">
                <input type="hidden" id ="cko-cc-paymenToken" name ="cko-cc-paymenToken" />
            </form>

        <button type="button" id="payButtonId">Paynow</button>

    </body>
</html>
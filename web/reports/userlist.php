<?php
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
    $aColumns = array("u.created", "a.profileid", "u.email", "u.id", "a.smsphone", "u.status", "a.contactname", "u.logincount", "u.lastaccess");

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "bbids_user.id";

    /* DB table to use */
    $sTable = "bbids_user, bbids_account";

    /* Join condition */
    $sJoin = "bbids_user.id = bbids_account.userid";

    /* Database connection information */
    $gaSql['user']       = "bizbids";
    $gaSql['password']   = "eY3@gV*094(Wl1$";
    $gaSql['db']         = "bizbids";
    $gaSql['server']     = "localhost";


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */

    /*
     * MySQL connection
     */
    $gaSql['link'] =  mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
        die( 'Could not open connection to server' );

    mysql_select_db( $gaSql['db'], $gaSql['link'] ) or
        die( 'Could not select database '. $gaSql['db'] );


    /*
     * Paging
     */
    $sLimit = "";
    if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
    {
        $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
    }


    /*
     * Ordering
     */
    $sOrder = "";
    if ( isset( $_GET['iSortCol_0'] ) )
    {
        $sOrder = "ORDER BY  ";
        for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
        {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
                $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".($_GET['sSortDir_'.$i]==='asc' ? 'desc' : 'asc') .", ";
            }
        }

        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" )
        {
            $sOrder = "";
        }
    }


    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
    $sWhere = "";
    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    {
        $sWhere = "AND (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
    }

    /* Individual column filtering */
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
        if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        {
            if(false) //if ( $sWhere == "" )
            {
                $sWhere = "WHERE ";
            }
            else
            {
                $sWhere .= " AND ";
            }
            $columnFilterValue = mysql_real_escape_string($_GET['sSearch_' . $i]);
            $rangeSeparator = "~";
            $columnFilterRangeMatches = explode($rangeSeparator, $columnFilterValue);
             if($aColumns[$i] == 'u.created'){
                if (empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
                        $sWhere .= " 0 = 0 ";
                else if (!empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
                        $sWhere .=" DATE( ".$aColumns[$i] .")". " BETWEEN '" . $columnFilterRangeMatches[0] . "' and '" . $columnFilterRangeMatches[1] . "' ";
                else if (empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
                        $sWhere .= $aColumns[$i] . " <= '" . $columnFilterRangeMatches[1] . "' ";
                else if (!empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
                        $sWhere .= $aColumns[$i] . " >= '" . $columnFilterRangeMatches[0] . "' ";
            }
            else if($aColumns[$i] == 'u.logincount') {
                    $sWhere .= ($columnFilterValue == 'Zero') ? $aColumns[$i]." IS NULL " : $aColumns[$i]." IS NOT NULL ";
            }
            else
                $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
        }
    }

    // Add the join
    /*if ( $sWhere == "" ) {
        $sWhere = "WHERE $sJoin";
    }
    else {
        $sWhere = " AND $sJoin";
    }*/

    /*
     * Select list
     */
    $sSelect = "";
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
        $sSelect .= $aColumns[$i] .' as `'.$aColumns[$i].'`, ';
    }
    $sSelect = substr_replace( $sSelect, "", -2 );


    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = "SELECT SQL_CALC_FOUND_ROWS $sSelect FROM bbids_user u INNER JOIN bbids_account a ON  u.id = a.userid WHERE a.profileid !=1 $sWhere $sOrder $sLimit";
     // echo $sQuery; exit;
    $rResult = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());

    /* Data set length after filtering */
    $sQuery = "
        SELECT FOUND_ROWS()
    ";
    $rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
    $aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
    $iFilteredTotal = $aResultFilterTotal[0];

    /* Total data set length */
    $sQuery = "
        SELECT COUNT(".$sIndexColumn.")
        FROM   $sTable
        WHERE  $sJoin
    ";
    // echo $sQuery;exit;
    $rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
    $aResultTotal = mysql_fetch_array($rResultTotal);
    $iTotal = $aResultTotal[0];


    /*
     * Output
     */
    $output = array(
        "sEcho"                => intval($_GET['sEcho']),
        "iTotalRecords"        => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData"               => array()
    );

    while ( $aRow = mysql_fetch_array( $rResult ) )
    {
        $row = array();
        for ( $i=0 ; $i<count($aColumns) ; $i++ ) {

            if ( $aColumns[$i] == "u.status" )
            {
                /* Special output formatting for 'version' column */
                $row[] = ($aRow[ $aColumns[$i] ]=="1" || $aRow[ $aColumns[$i] ]=="4") ? 'Active' : 'Inactive';
            }
            else if ( $aColumns[$i] == "a.profileid" ) {

                $row[] = ($aRow[ $aColumns[$i] ]=="2") ? 'Customer' : 'Vendor';
            }
            else if ( $aColumns[$i] == "u.email" ) {

                $email = $aRow[ $aColumns[$i] ];
                $row[] = $aRow[ $aColumns[$i] ];
            }
            else if ( $aColumns[$i] == "u.id" ) {
                $uid = $aRow[ $aColumns[$i] ];
                $row[] = '<a href="/admin/user/edit/'.$aRow[ $aColumns[$i] ].'" target="_blank">Edit</a> | <a href="#" onclick="deletefunc(\''.$aRow[ $aColumns[$i] ].'\', \''.$email.'\')" >Delete</a>';
            }
            else if ( $aColumns[$i] == "a.contactname" ) {

                $row[] = '<a href="/admin/user/view/'.$uid.'" target="_blank">'.$aRow[ $aColumns[$i] ].'</a>';
            }
            else if ( $aColumns[$i] == "u.logincount" ) {
                $row[] = ($aRow[ $aColumns[$i] ]== NULL) ? 0 : $aRow[ $aColumns[$i] ];
            }
            else // if ( $aColumns[$i] != ' ' )
            {
                /* General output */
                $row[] = $aRow[ $aColumns[$i] ];
            }
        }
        $output['aaData'][] = $row;
    }

    echo json_encode( $output );

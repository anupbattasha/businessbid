<?php

require_once  ("MacAddress.php");

$mac = new MacAddress ('00:FF:AB:CC:DD:FF');

echo $mac->output ('.',4)."<br/>";                   //  00FF.ABCC.DDFF

echo $mac->output ('.',4,'lower')."<br/>";           //  00ff.abcc.ddff
echo $mac->output ('-',2,'upper')."<br/>";           //  00-FF-AB-CC-DD-FF
echo $mac->output (':',2,'lower')."<br/>";           //  00:ff:ab:cc:dd:ff

$mac2 =   new MacAddress ($mac->GetMacAddr());      // mac of eth0 of my server
echo $mac2->output (':',2,'lower')."<br/>";            //  00:50:56:ac:52:7b
echo $mac2->output ('.',4,'lower')."<br/>";            //  0050.56ac.527b
echo $mac2->output ('.',4,'upper')."<br/>";            //  0050.56AC.527B


?>
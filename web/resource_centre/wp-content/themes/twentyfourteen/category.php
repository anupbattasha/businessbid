<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="lightblue">
<div class="container">
<div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?></div>
</div>
</div>

<div class="blogSection">
  <h2 class="categoryHeading"><span style="font-size:26px;">RESOURCE CENTRE</span><br />
<?php single_cat_title( '', true ); ?> </h2>
  <div class="container">
    <div class="col-md-8 col-sm-8">
     <?php if (have_posts()) : ?>

			
			<?php
					// Start the Loop.
					while (have_posts()) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					// */

the_title();

					get_template_part( 'content', get_post_format());

					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
				get_template_part( 'content', 'none' );

				endif;
			?>
 </div>
    
    <div class="col-md-4 col-sm-4">
    <?php get_sidebar(); 
	get_sidebar( 'content' );
	?>
    </div>
  </div>
</div>



			
		

<?php
//get_sidebar( 'content' );

get_footer();

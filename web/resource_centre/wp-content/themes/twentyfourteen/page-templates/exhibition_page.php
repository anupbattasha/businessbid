<?php
/**

 * Template Name: Exhibition

 *

 * This is the most generic template file in a WordPress theme

 * and one of the two required files for a theme (the other being style.css).

 * It is used to display a page when nothing more specific matches a query.

 * For example, it puts together the home page when no home.php file exists.

 *

 * @link http://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */



get_header(); ?>

<!-- Start of Header Image Area -->

<div id="exhibition_header" class="header_main_slider_image">
	<div class="header_slider">
		<div class="headesr_slide_text"><?php echo get_post_meta($post->ID, 'exhibition_banner_text', true); ?></div>
	</div>
</div>
<!-- End of Header Image Area --> 

<!-- Start of left Image Area -->
<div class="main_text_area_left">
	<div id="companies_in_dubai" class="main_left_image">
		<div class="mail_left_img"><?php echo get_post_meta($post->ID, 'exhibition_leftImg', true); ?> </div>
		<div class="mail_left_text"> <?php echo get_post_meta($post->ID, 'exhibition_rightContent', true); ?> </div>
	</div>
</div>
<!-- End of left Image Area --> 

<!-- Start Step Area here -->
<div id="exhi_quort" class="new_get_quort_area">
	<div class="new_get_quort_area_inner"> <?php echo get_post_meta($post->ID, 'exhibition_step_heading', true); ?>
		<div class="new_get_court_child_one"> <?php echo get_post_meta($post->ID, 'exhibition_step_one', true); ?> </div>
		<div class="new_get_court_child_two"> <?php echo get_post_meta($post->ID, 'exhibition_step_two', true); ?> </div>
		<div class="new_get_court_child_three"> <?php echo get_post_meta($post->ID, 'exhibition_step_three', true); ?> </div>
		<div class="new_get_quort_btn"> <?php echo get_post_meta($post->ID, 'exhibition_step_btn', true); ?></div>
	</div>
</div>
<!-- End of Step Area here --> 

<!-- Start of Types Area here -->
<div id="exhibition_newpress" class="newpress_area">
	<div class="inner_newpress_area"> <?php echo get_post_meta($post->ID, 'exhibition_service_heading', true); ?>
		<div class="child_inner_newpress_area"> <?php echo get_post_meta($post->ID, 'exhibition_stand_building', true); ?> </div>
		<div class="child_inner_newpress_area"> <?php echo get_post_meta($post->ID, 'kiosk_building', true); ?> </div>
	</div>
</div>
<!-- End of Types Area here --> 

<!-- Start of Right Image Area -->
<div id="stand_building" class="main_text_area_right">
	<div class="main_right_image">
		<div class="mail_right_text"> <?php echo get_post_meta($post->ID, 'exhibition_stand_building_leftContent', true); ?> </div>
		<div class="mail_right_img"> <?php echo get_post_meta($post->ID, 'exhibition_stand_building_rightImg', true); ?> </div>
	</div>
</div>
<!-- End of Right Image Area --> 

<!-- Start of left Image Area -->
<div id="kiosk_building" class="main_text_area_left">
	<div class="main_left_image">
		<div class="mail_left_img"> <?php echo get_post_meta($post->ID, 'kiosk_building_leftImg', true); ?> </div>
		<div class="mail_left_text"> <?php echo get_post_meta($post->ID, 'kiosk_building_rightContent', true); ?> </div>
	</div>
</div>
<!-- End of left Image Area --> 

<!-- Start of Right Image Area -->
<div id="stand_companis_exi" class="main_text_area_right">
	<div class="main_right_image">
		<div class="mail_right_text"> <?php echo get_post_meta($post->ID, 'exhibition_what_do_you_LeftContent', true); ?> </div>
		<div class="mail_right_img"> <?php echo get_post_meta($post->ID, 'exhibition_what_do_you_rightImg', true); ?> </div>
	</div>
</div>
<!-- End of Right Image Area --> 

<!-- Start of left Image Area -->
<div class="main_text_area_left">
	<div id="stand_lock" class="main_left_image">
		<div class="mail_left_img">  <?php echo get_post_meta($post->ID, 'exhibition_stand_look_leftImg', true); ?></div>
		<div class="mail_left_text"> <?php echo get_post_meta($post->ID, 'exhibition_stand_look_rightContent', true); ?> </div>
	</div>
</div>
<!-- End of left Image Area --> 

<!-- Start of Get Quort Area -->
<div class="get_quort_area">
	<div class="inner_quort_area"> <?php echo get_post_meta($post->ID, 'exhibition_quort_text', true); ?> </div>
</div>
<!-- End of Get Quort Area -->

<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right Exhibition Stand Contractor in the UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=15&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(15);?>" title="<?php echo get_cat_name(15);?>">CLICK HERE TO VIEW MORE ARTICLES ON EXHIBITION STANDS</a></strong></p>
</div>
 
</div>

<?php get_footer(); ?>

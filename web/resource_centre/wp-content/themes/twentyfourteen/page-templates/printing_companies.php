<?php
/**
 * Template Name: Printing Compaines
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

        <!-- Main Content File here -->
        <div class="header_area_main">
           <div class="inner_header_area">
               <div class="header_opacity"></div>  
                <div class="child_header_area">
                   <?php echo get_post_meta($post->ID, 'printing_banner_text', true); ?>
                </div>               
           </div>
        </div>
        <div class="printing_press_area">
            <div class="inner_printing_press_area">
                <div class="printing_press_area_img">
                   <?php echo get_post_meta($post->ID, 'business_card_printing_img', true); ?>
                </div>
                <div class="printing_press_area_txt">
                    <?php echo get_post_meta($post->ID, 'business_card_printing_text', true); ?>
                </div>
            </div>
        </div>
        <div class="get_quort_area">
            <div class="get_quort_area_inner">
                <?php echo get_post_meta($post->ID, 'printing_easySteup_heading', true); ?>
                <div class="get_court_child_one">
                   <?php echo get_post_meta($post->ID, 'printing_easySteup_one', true); ?>
                </div>
                <div class="get_court_child_two">
                   <?php echo get_post_meta($post->ID, 'printing_easySteup_two', true); ?>
                </div>
                <div class="get_court_child_three">
                    <?php echo get_post_meta($post->ID, 'printing_easySteup_three', true); ?>
                </div>
                <div class="get_quort_btn">
                   <?php echo get_post_meta($post->ID, 'printing_easySteup_link', true); ?>
                </div>
            </div>
        </div>
        <div class="mainpress_area">
            <div class="inner_mainpress_area">
               <?php echo get_post_meta($post->ID, 'printing_press_type_heading', true); ?>
                <div class="child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'business_card', true); ?>
                </div>
                <div class="child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'business_stationary', true); ?>                  
                </div>
                <div class="child_inner_mainpress_area">
                   <?php echo get_post_meta($post->ID, 'event_promotion', true); ?>
                </div>
                <div class="child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'corporate_marketing', true); ?>
                </div>
            </div>
        </div>
        <div class="business_card_area">
            <div class="inner_business_card_area">
                <div class="inner_business_card_area_txt">
                    <?php echo get_post_meta($post->ID, 'business_cards_text', true); ?>
                </div>
                <div class="inner_business_card_area_img">
                   <?php echo get_post_meta($post->ID, 'business_cards_img', true); ?>
                </div>
            </div>
        </div>
        <div class="stationary_area">
            <div class="stationary_area_inner">
                <div class="stationary_area_inner_img">
                    <?php echo get_post_meta($post->ID, 'business_stationery_img', true); ?>
                </div>
                <div class="stationary_area_inner_text">
                    <?php echo get_post_meta($post->ID, 'business_stationery_text', true); ?>
                </div>
            </div>
        </div>
        <div class="promot_event_area">
            <div class="inner_promot_event_area">
                <div class="promot_event_area_text">
                   <?php echo get_post_meta($post->ID, 'event_promotion_text', true); ?>
                </div>
                <div class="promot_event_area_img">
                   <?php echo get_post_meta($post->ID, 'event_promotion_img', true); ?>
                </div>
            </div>
        </div>
        <div class="corporate_area">
            <div class="corporate_area_inner">
                <?php echo get_post_meta($post->ID, 'corporate_marketing_text', true); ?>
            </div>
        </div>
        <div class="dubai_printing_press_area">
            <div class="inner_dubai_printing_press_area">
               <?php echo get_post_meta($post->ID, 'If_you_are_printing_press', true); ?>
                <div class="printing_dubai_btn">
                    <?php echo get_post_meta($post->ID, 'If_you_are_printing_press_link', true); ?>
                </div>
            </div>
        </div>        
		<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(11);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=11&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(11);?>" title="<?php echo get_cat_name(11);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(11);?></a></strong></p>
</div>
 
</div>
</div>

 <?php
get_footer();

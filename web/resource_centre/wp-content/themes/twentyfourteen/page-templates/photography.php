<?php
/**
 * Template Name: Photography
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
        <!-- Main Content File here -->
        <div class="photo_headder_area">
            <div class="man_headder_inner">
                <?php echo get_post_meta($post->ID, 'photoghraphy_banner_text', true); ?>
            </div>
        </div>
        <!-- Maintenance Service Area here -->
        <div class="man_service_area">
            <div class="man_inner_service">
                <?php echo get_post_meta($post->ID, 'dubai_photography_img', true); ?>
                <?php echo get_post_meta($post->ID, 'dubai_photography_text', true); ?>
                
            </div>
        </div>

        <!-- Maintenance Step Area here -->
        <div class="man_get_quort_area">
            <?php echo get_post_meta($post->ID, 'photographer_3esay_step', true); ?>
        </div> 
        
        <!-- Mantenance Printing press service Area -->
        <div class="man_mainpress_area">
            <div class="man_inner_mainpress_area">
                <?php echo get_post_meta($post->ID, 'type_of_service_heading', true); ?>
                <div class="man_child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'wedding', true); ?>
                </div>
                <div class="man_child_inner_mainpress_area">
                      <?php echo get_post_meta($post->ID, 'corporate', true); ?>                 
                </div>
                <div class="man_child_inner_mainpress_area">
                   <?php echo get_post_meta($post->ID, 'family', true); ?>
                </div>
                <div class="man_child_inner_mainpress_area photo_icon_boot">
                    <?php echo get_post_meta($post->ID, 'events', true); ?>
                </div>
                <div class="man_child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'architectural', true); ?>
                </div>
            </div>
        </div>        
        
        <!-- Mantenance Handyman service Area --> 
        <div class="man_handyman_service_arae">
            <div class="inner_handyman_area">
                <div class="child_handyman_txt">
                   <?php echo get_post_meta($post->ID, 'wedding_photography_text', true); ?>
                </div>
                <div class="child_handyman_img">
                    <?php echo get_post_meta($post->ID, 'wedding_photography_img', true); ?>
                </div>
            </div>
        </div>       
        
        <!-- Mantenance Electrical Area -->         
        <div class="man_electrical_area">
            <div class="inner_man_electrical">
                <div class="child_man_electrical_img child_man_corporate_img">
                    <?php echo get_post_meta($post->ID, 'corporate_photography_img', true); ?>
                </div>
                <div class="child_man_electrical_txt">
                    <?php echo get_post_meta($post->ID, 'corporate_photography_text', true); ?>
                </div>
            </div>
        </div>
        
        <!-- Mantenance Plumbing service Area --> 
        <div class="man_plumbing_service_arae">
            <div class="inner_plumbing_area">
                <div class="child_plumbing_txt">
                   <?php echo get_post_meta($post->ID, 'family_photograph_text', true); ?>
                </div>
                <div class="child_plumbing_img">
                    <?php echo get_post_meta($post->ID, 'family_photograph_img', true); ?>
                </div>
            </div>
        </div> 
                 
         <!-- Mantenance Painting Area -->         
        <div class="man_painting_area">
            <div class="inner_man_painting">
                <div class="child_man_painting_img">
                    <?php echo get_post_meta($post->ID, 'events_photography_img', true); ?>
                </div>
                <div class="child_man_painting_txt">
                   <?php echo get_post_meta($post->ID, 'events_photography_text', true); ?>
                </div>
            </div>
        </div>       
        
        <!-- Mantenance install Area --> 
        <div class="man_install_service_arae">
            <div class="inner_install_area">
                <div class="child_install_txt">
                    <?php echo get_post_meta($post->ID, 'architectural_photography_text', true); ?>
                </div>
                <div class="child_install_img">
                    <?php echo get_post_meta($post->ID, 'architectural_photography_img', true); ?>
                </div>
            </div>
        </div> 
        
        <!-- Mantenance Dubai printing Area -->               
        <div class="man_dubai_printing_press_area">
            <div class="man_inner_dubai_printing_press_area">
                <?php echo get_post_meta($post->ID, 'professional_photographer_bottom_text', true); ?>
                <div class="man_printing_dubai_btn">
                    <?php echo get_post_meta($post->ID, 'professional_photographer_bottom_link', true); ?>
                </div>
            </div>
        </div>        
		<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(10);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=10&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(10);?>" title="<?php echo get_cat_name(10);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(10);?></a></strong></p>
</div>
 
</div>
</div>

        
<?php
get_footer();
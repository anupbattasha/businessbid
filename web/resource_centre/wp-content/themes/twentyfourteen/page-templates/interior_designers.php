<?php
/**
 * Template Name: Interior Designers
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>



<!--slider-->
<div class="bannerDesign interior_page_md">
  <div class="container">
    <div class="col-md-6 col-sm-7">
      <div class="bannerText interior_ali_change"><?php echo get_post_meta($post->ID, 'interior_bannerText', true); ?> </div>
    </div>
    <div class="col-md-6 col-sm-5">
      <div class="bannerImg"><?php echo get_post_meta($post->ID, 'interior_bannerImg', true); ?> </div>
    </div>
  </div>
</div>
<!--slider--> 

<!--Accouting-Section-->
<div class="accountingSec">
  <div class="container">
    <div class="col-md-4 col-sm-5">
      <div class="accountImg"><?php echo get_post_meta($post->ID, 'interior_designersImg', true); ?> </div>
    </div>
    <div class="col-md-8 col-sm-7">
      <div class="accountText catMargin">
       <?php echo get_post_meta($post->ID, 'interior_designersText', true); ?> 
      </div>
    </div>
  </div>
</div>
<!--Accouting-Section--> 

<!--Step-Section-->
<div class="stepSection">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'interior_setupHeading', true); ?> 
    <div class="stepS">
      <div class="col-md-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'interior_setuponeImg', true); ?> </span>
          <?php echo get_post_meta($post->ID, 'interior_setuponeText', true); ?> 
        </div>
      </div>
      <div class="col-md-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'interior_setuptwoImg', true); ?> </span>
          <?php echo get_post_meta($post->ID, 'interior_setuptwoText', true); ?> 
        </div>
      </div>
      <div class="col-md-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'interior_setupthreeImg', true); ?> </span>
          <?php echo get_post_meta($post->ID, 'interior_setupthreeText', true); ?> 
        </div>
      </div>
      <div class="stepGet"><?php echo get_post_meta($post->ID, 'interior_setupLink', true); ?> </div>
    </div>
  </div>
</div>
<!--Step-Section--> 

<!--Accountant Services-->
<div class="accountantService">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'interior_serviceHeading', true); ?>
    <div class="serviceStep">
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'interior_serviceoneImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'interior_serviceoneText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'interior_servicetwoImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'interior_servicetwoText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6"> 
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'interior_servicethreeImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'interior_servicethreeText', true); ?>
        </span> </div>
      <div class="col-md-3 col-sm-6">
        <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'interior_servicefourImg', true); ?></div>
        <span class="serviceHed">
        <?php echo get_post_meta($post->ID, 'interior_servicefourText', true); ?>
        </span> </div>
    </div>
  </div>
</div>
<!--Accountant Services--> 

<!--InteriorDbai-->
<div class="assuranceSection">
  <div class="container">
    <div class="col-md-9 col-sm-7">
      <div class="assuranceText">
        <?php echo get_post_meta($post->ID, 'choosing_interiorText', true); ?>
      </div>
    </div>
    <div class="col-md-3 col-sm-5"><span class="assurancImg"><?php echo get_post_meta($post->ID, 'choosing_interiorImg', true); ?></span></div>
  </div>
</div>
<!--InteriorDbai--> 

<!--assuranceSection--> 

<!--Book-Section-->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg InteriorBottom"><?php echo get_post_meta($post->ID, 'interior_designImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'interior_designText', true); ?>
      </div>
    </div>
    
    <div class="interioDesign col-md-12">
    <?php echo get_post_meta($post->ID, 'interior_designList', true); ?>
    </div>
    
  </div>
</div>
<!--Book-Section--> 

<!--fitOut-->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'fitOutText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg"><?php echo get_post_meta($post->ID, 'fitOutImg', true); ?></span></div>
  </div>
</div>
<!--fitOut--> 

<!--Payroll-->
<div class="PayRoll">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="payrollImg"><?php echo get_post_meta($post->ID, 'kitchen_designerImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="PayText kitchen">
        
<?php echo get_post_meta($post->ID, 'kitchen_designerText', true); ?>

      </div>
    </div>
  </div>
</div>
<!--Payroll-->  

<!--Qoutes-section-->
<div class="QuotesSec">
  <div class="container">
  <?php echo get_post_meta($post->ID, 'quotes_interiorText', true); ?>
     </div>
</div>
<div class="resourceCenter">
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(4);?> in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=4&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(4);?>" title="<?php echo get_cat_name(4);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(4);?></a></span></strong></p>
</div>
</div>
</div>
<?php
get_footer();

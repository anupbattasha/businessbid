<?php
/**
 * Template Name: Signage & Signboards
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


<!--slider-->
<div class="bannerDesign">
  <div class="container">
    <div class="col-md-6 col-sm-7">
      <div class="bannerText">
         <?php echo get_post_meta($post->ID, 'Signboard_bannerText', true); ?></div>
    </div>
    <div class="col-md-6 col-sm-5">
      <div class="bannerImg signBordBanner"><?php echo get_post_meta($post->ID, 'Signboard_bannerImg', true); ?></div>
    </div>
  </div>
</div>
<!--slider--> 

<!--Accouting-Section-->
<div class="accountingSec margonB">
  <div class="container">
    <div class="col-md-4 col-sm-5">
      <div class="accountImg"><?php echo get_post_meta($post->ID, 'Signage_signImg', true); ?></div>
    </div>
    <div class="col-md-8 col-sm-7">
      <div class="accountText catMargin">
        <?php echo get_post_meta($post->ID, 'Signage_signText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Accouting-Section--> 

<!--Step-Section-->
<div class="stepSection">
  <div class="container">
     <?php echo get_post_meta($post->ID, 'Signboard_stepsHeading', true); ?>
    <div class="stepS">
      <div class="col-md-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'Signboard_stepsoneImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'Signboard_stepsoneText', true); ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'Signboard_stepstwoImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'Signboard_stepstwoText', true); ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="stepOne"> <span class="StepIcon"><?php echo get_post_meta($post->ID, 'Signboard_stepsthreeImg', true); ?></span>
          <?php echo get_post_meta($post->ID, 'Signboard_stepsthreeText', true); ?>
        </div>
      </div>
      <div class="stepGet"><?php echo get_post_meta($post->ID, 'Signboard_stepsLink', true); ?></div>
    </div>
  </div>
</div>
<!--Step-Section--> 

<!--Accountant Services-->
<div class="accountantService">
  <div class="container">
    <?php echo get_post_meta($post->ID, 'Signboard_serviceHeading', true); ?>
    <div class="row">
      <div class="serviceStep">
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_serviceoneImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_serviceoneText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicetwoImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicetwoText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicethreeImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicethreeText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicefourImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicefourText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicefiveImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicefiveText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicesixImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicesixText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicesevenImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicesevenText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_serviceeightImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_serviceeightText', true); ?>
          </span> </div>
        <div class="col-md-4 col-sm-6">
          <div class="serviceIcon"><?php echo get_post_meta($post->ID, 'Signboard_servicenineImg', true); ?></div>
          <span class="serviceHed">
          <?php echo get_post_meta($post->ID, 'Signboard_servicenineText', true); ?>
          </span> </div>
      </div>
    </div>
  </div>
</div>
<!--Accountant Services--> 

<!--InteriorDbai-->
<div class="assuranceSection">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="assuranceText">
        <?php echo get_post_meta($post->ID, 'Outdoor_signageText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="assurancImg"><?php echo get_post_meta($post->ID, 'Outdoor_signageImg', true); ?></span></div>
  </div>
</div>
<!--InteriorDbai--> 

<!--assuranceSection--> 

<!--Book-Section-->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg"><?php echo get_post_meta($post->ID, 'Indoor_signageImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'Indoor_signageText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--Book-Section--> 

<!--fitOut-->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'Roll_upBannersText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero rollLower"><?php echo get_post_meta($post->ID, 'Roll_upBannersImg', true); ?></span></div>
  </div>
</div>
<!--fitOut--> 

<!----Networking--->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg rollLower"><?php echo get_post_meta($post->ID, 'Window_graphicsImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'Window_graphicsText', true); ?>
      </div>
    </div>
  </div>
</div>
<!----Networking---> 

<!---Computer optimization--->

<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText paddingZero">
        <?php echo get_post_meta($post->ID, 'Wall_graphicsText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg graphicsUp"><?php echo get_post_meta($post->ID, 'Wall_graphicsImg', true); ?></span></div>
  </div>
</div>

<!---Computer optimization---> 

<!---Backup--->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg floorDown"><?php echo get_post_meta($post->ID, 'Floor_graphicsImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'Floor_graphicsText', true); ?>
      </div>
    </div>
  </div>
</div>
<!---Backup---> 

<!--cutom sticker-->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText paddingZero">
        <?php echo get_post_meta($post->ID, 'Custom_stickersText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg securityUp"><?php echo get_post_meta($post->ID, 'Custom_stickersImg', true); ?></span></div>
  </div>
</div>
<!--cutom sticker--> 

<!--digital-->
<div class="bookSection">
  <div class="container">
    <div class="col-md-4 col-sm-5"><span class="bookImg securityUp"><?php echo get_post_meta($post->ID, 'Digital_signageImg', true); ?></span></div>
    <div class="col-md-8 col-sm-7">
      <div class="bookText">
        <?php echo get_post_meta($post->ID, 'Digital_signageText', true); ?>
      </div>
    </div>
  </div>
</div>
<!--digital--> 

<!--Flage&banner-->
<div class="budGet">
  <div class="container">
    <div class="col-md-8 col-sm-7">
      <div class="budgetText">
        <?php echo get_post_meta($post->ID, 'Flags_bannerText', true); ?>
      </div>
    </div>
    <div class="col-md-4 col-sm-5"><span class="budgetImg paddingZero flageDown"><?php echo get_post_meta($post->ID, 'Flags_bannerImg', true); ?></span></div>
  </div>
</div>
<!--Flage&banner-->
<div class="QuotesSec caterQuotes"> 
  <div class="container">
    <?php echo get_post_meta($post->ID, 'signage_quoteText', true); ?> </div>
</div>



<div class="resourceCenter grayBg">
<div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(6);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=6&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(6);?>" title="<?php echo get_cat_name(6);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(6);?></a></strong></p>
</div>
</div>
</div>


<?php
get_footer();

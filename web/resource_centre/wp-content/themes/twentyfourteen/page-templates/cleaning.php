<?php
/**
 * Template Name: Cleaning
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
        <!-- Main Content File here -->
        <div class="cleaning_headder_area">
            <div class="cleaning_headder_inner">
                <div class="cleaning_child_hedder">
                    <?php echo get_post_meta($post->ID, 'cleaning_banner_text', true); ?>
                    <div class="clearing_hed_btn">
                        <?php echo get_post_meta($post->ID, 'cleaning_banner_link', true); ?>
                    </div>
                    <div class="clearing_hed_btn">
                        <?php echo get_post_meta($post->ID, 'cleaning_banner_link_two', true); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Maintenance Service Area here -->
        <div class="man_service_area">
            <div class="man_inner_service">
                <div class="man_child_service_img">
                   <?php echo get_post_meta($post->ID, 'commercial_cleaning_img', true); ?>
                </div>
                <div class="man_child_service_txt">
                    <?php echo get_post_meta($post->ID, 'commercial_cleaning_text', true); ?>
                </div>
            </div>
        </div>

        <!-- Maintenance Step Area here -->
        <div class="man_get_quort_area">
            <div class="man_get_quort_area_inner">
                <?php echo get_post_meta($post->ID, 'cleaning_3esay_steup_heading', true); ?>
                <div class="man_get_court_child_one">
                    <?php echo get_post_meta($post->ID, '3easy_one', true); ?>
                </div>
                <div class="man_get_court_child_two">
                    <?php echo get_post_meta($post->ID, '3easy_two', true); ?>
                </div>
                <div class="man_get_court_child_three">
                    <?php echo get_post_meta($post->ID, '3easy_three', true); ?>
                </div>
                <div class="man_get_quort_btn">
                    <?php echo get_post_meta($post->ID, '3easy_link', true); ?>
                </div>
            </div>
        </div> 
        
        <!-- Mantenance Printing press service Area -->
        <div class="cleaning_mainpress_area">
            <div class="cleaning_inner_mainpress_area">
                <?php echo get_post_meta($post->ID, 'cleaning_service_heading', true); ?>
                <div class="cleaning_child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'residential', true); ?>
                </div>
                <div class="cleaning_child_inner_mainpress_area">
                    <?php echo get_post_meta($post->ID, 'commercial', true); ?>               
                </div>
            </div>
        </div>        
        
        <!-- Mantenance Handyman service Area --> 
        <div class="man_handyman_service_arae">
            <div class="inner_handyman_area">
                <div class="child_handyman_txt">
                   <?php echo get_post_meta($post->ID, 'residential_text', true); ?>
                </div>
                <div class="child_handyman_img">
                    <?php echo get_post_meta($post->ID, 'residential_img', true); ?>
                </div>
            </div>
        </div>       
        
        <!-- Mantenance Electrical Area -->         
        <div class="man_electrical_area">
            <div class="inner_man_electrical">
                <div class="child_man_electrical_img">
                    <?php echo get_post_meta($post->ID, 'commartial_img', true); ?>
                </div>
                <div class="child_man_electrical_txt">
                    <?php echo get_post_meta($post->ID, 'commartial_text', true); ?>
                </div>
            </div>
        </div>
        
        <!-- Mantenance Dubai printing Area -->               
        <div class="cleaning_dubai_printing_press_area">
            <div class="man_inner_dubai_printing_press_area">
                <?php echo get_post_meta($post->ID, 'If_you_are_cleaning_text', true); ?>
                <div class="man_printing_dubai_btn">
                    <?php echo get_post_meta($post->ID, 'If_you_are_cleaning_link', true); ?>
                </div>
            </div>
        </div>        
        <div class="container">
<div class="resouceHed">
<h2>RESOURCE CENTRE</h2>
<p>Tips and advice on hiring the right <?php echo get_cat_name(9);?> Companies in UAE</p>
</div>
<div class="row">


<?php
$catquery = new WP_Query( 'cat=9&posts_per_page=4' );
while($catquery->have_posts()) : $catquery->the_post();
?>
<div class="col-md-6">
<span class="recImg"><?php the_post_thumbnail(large); ?></span>
<div class="recText">

<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

<p><?php// the_excerpt(); ?></p><br /><br />
<a class="recBtn" href="<?php the_permalink(); ?>">Continue reading</a>

<?php echo do_shortcode('[simple-social-share]') ;?>

</div>
</div>
<?php endwhile; ?>


</div>

<div class="tagLine">
<p><strong><a href="<?php echo get_category_link(9);?>" title="<?php echo get_cat_name(9);?>">CLICK HERE TO VIEW MORE Articles ON <?php echo get_cat_name(9);?></a></strong></p>
</div>
 
</div>
</div>

 <?php
get_footer();
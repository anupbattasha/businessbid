<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

	<!--Footer-->
	<footer class="footer_wrapper">
	<div class="container">
		<div class="row">
			<div class="customer-benefits-block">
				 <div class="region region-footer-links-one footer-menu">
					<section id="block-menu-menu-footer-menu-one" class="block block-menu clearfix customer-benefits">
						<h2 class="block-title">Customer Benefits</h2>
						<ul class="menu nav">
							<li class="first leaf"><a href="#" title="">Convenience</a>
								<span>Multiple quotations with a single, simple form</span>
							</li>
							<li class="leaf active"><a href="#" title="" class="active">Competitive Pricing</a>
								<span>Compare quotes before picking your favourite.</span>
							</li>
							<li class="leaf active"><a href="#" title="" class="active">Speed</a>
								<span>Quick responses to all your job requests.</span>
							</li>
							<li class="leaf active"><a href="#" title="" class="active">Reputation</a>
								<span>Check reviews and ratings for the inside scoop.</span>
							</li>
							<li class="leaf active"><a href="#" title="" class="active">Free to Use</a>
								<span>No usage fees. Ever!</span>
							</li>
							<li class="last leaf active"><a href="#" title="" class="active">Amazing Support</a>
								<span>Phone. Live Chat. Facebook. Twitter.</span>
							</li>
						</ul>
					</section> <!-- /.block -->
				</div>
			</div>
		<div class="footer-right-menu-block">
			<div class="footer-right-menu-block-top">
			<div class="customer-benefits-block-first">
				 <div class="region region-footer-links-two footer-menu">
					<section id="block-menu-menu-footer-menu-two" class="block block-menu clearfix">
						<h2 class="block-title">About</h2>
						<ul class="menu nav">
							<li class="first leaf"><a href="/aboutbusinessbid#aboutus" title="">About Us</a></li>
							<li class="leaf active"><a href="/aboutbusinessbid#meetteam" title="" class="active">Meet the Team</a></li>
							<li class="leaf active"><a href="/aboutbusinessbid#career" title="" class="active">Career Oprtunities</a></li>
							<li class="last leaf active"><a href="/aboutbusinessbid#valuedpartner" title="" class="active">Valued Partners</a></li>
							<li class="leaf active"><a href="/contactus" class="active">Contact Us</a></li>

						</ul>
					</section> <!-- /.block -->
				</div>
			</div>
			<div class=" col-sm-3">
				<div class="region region-footer-links-three footer-menu">
					<section id="block-menu-menu-for-services-professionals" class="block block-menu clearfix ">
						<h2 class="block-title">Vendor Resources</h2>
						<ul class="menu nav">
							<li class="leaf"><a href="/login#vendor" title="">Login</a></li>
							<!--<li class="first leaf active"><a href="/node1" title="" class="active">Grow Your Business</a></li>-->
							<li class="leaf"><a href="#" title="">How it Works</a></li>
							<li class="first leaf"><a href="/vendor/register" title="">Become a Vendor</a></li>
							<!--<li class="leaf active"><a href="/node2#venFAQ" title="" class="active">FAQ's & Support</a></li>-->
						</ul>
					</section>
				<!-- /.block -->
				</div>
			</div>
			<div class=" col-sm-3">
				<div class="region region-footer-links-four footer-menu">
					<section id="block-menu-menu-for-services-professionals" class="block block-menu clearfix">
						<h2 class="block-title">Stay Connected</h2>
							<ul class="menu nav">
							<li class="leaf"><a href="https://www.facebook.com/businessbid?ref=hl" class="facebook" target="_blank">Like us on Facebook</a></li>
							<li class="first leaf active"><a href="https://twitter.com/BusinessBid" title="" class="twitter" target="_blank">Follow us on Twitter</a></li>
							<li class="leaf"><a href="#" title="" class="gplus" target="_blank">Follow Us on Google+</a></li>
							</ul>
					</section>
					<!-- /.block -->
				</div>
			</div>

			<div class="col-xs-12 customer-benefits-block-last footer_nav_liks">
				<div class="region region-footer-links-five">
					<section id="block-block-8" class="block block-block clearfix">
						<h2 class="block-title">Contact Us</h2>
						<address class="addr-map">Suite 503, Level 5, Indigo Icon Tower, Cluster F, Jumeirah Lakes Tower - Dubai, UAE</address>
						<span class="work-day-time">Sunday-Thursday</span>
						<span class="work-day-time">9 am - 6 pm</span>

						<span class="mobile">(04) 42 13 777</span>
						<span class="addr">BusinessBid DMCC, <br>PO Box- 393507, Dubai, UAE</span>

					</section> <!-- /.block -->
				</div>

			</div>
			</div>
			<div class="footer-right-menu-block-top">
			<div class="customer-benefits-block-first">
				 <div class="region region-footer-links-two footer-menu">
					<section id="block-menu-menu-footer-menu-two" class="block block-menu clearfix">
						<h2 class="block-title">Client Services</h2>
						<ul class="menu nav">
							<!-- <li class="first leaf"><a href="/web/app.php/post/quote/inline" title="">Post a Job</a></li> -->
							<li class="leaf"><a href="/node4" title="">Search Reviews</a></li>
							<li class="last leaf"><a href="/review/login" title="">Write a Review</a></li>
						</ul>
					</section> <!-- /.block -->
				</div>
			</div>
			<div class=" col-sm-3">
				<div class="region region-footer-links-three footer-menu">
					<section id="block-menu-menu-client-resources" class="block block-menu clearfix">
						<h2 class="block-title">Client Resources</h2>
						<ul class="menu nav">
							<li class="leaf"><a href="/#consumer" title="">Login</a></li>
							<li class="leaf"><a href="/node3">How it Works</a></li>
							<!--<li class="first leaf"><a href="{{ path('b_bids_b_bids_consumer_register') }}" title="">Become a Consumer</a></li> -->
							<li class="last leaf active"><a href="/resource_centre/home-2" title="" class="active">Resource Center</a></li>
							<li class="leaf active"><a href="/node6" title="" class="active">FAQ's & Support</a></li>
						</ul>
					</section> <!-- /.block -->
				</div>
			</div>
			<div class=" col-sm-3">
				<div class="region region-footer-links-four footer-menu">
					<section id="block-menu-menu-for-services-professionals" class="block block-menu clearfix payment-accept">
						<h2 class="block-title">Accepted Payment</h2>
						<ul class="menu nav col-sm-8">
							<li class="leaf"><a href="#" title=""><img src="<?php bloginfo('template_url'); ?>/images/visa-icon.jpg " alt="visa" /></a></li>
							<li class="leaf"><a href="#" title=""><img src="<?php bloginfo('template_url'); ?>/images/mastercard_icon.png" alt="Mastercard"/></a></li>
							<!--<li class="leaf"><a href="#" title=""><img src="<?php// bloginfo('template_url'); ?>/images/cashu-icon.jpg " /></a></li>
							<li class="leaf"><a href="#" title=""><img src="<?php //bloginfo('template_url'); ?>/images/paypal-icon.jpg" /></a></li>-->
							<li class="leaf"><a href="#" title=""><img src="<?php bloginfo('template_url'); ?>/images/securecode-icon.jpg" alt="SecureCode" /></a></li>
						</ul>
					</section>
					<!-- /.block -->
				</div>
			</div>

			<div class="col-xs-12 customer-benefits-block-last footer_nav_liks">
				<div class="region region-footer-links-five">
					<section id="block-block-8" class="block block-block clearfix subscribe_news">
						<h2 class="block-title">Subscribe to Newsletter</h2>
						<div class="subscribe-text">Sign up with your e-mail to get tips, resources and amazing deals</div>
						<?php echo do_shortcode('[do_widget id=newsletterwidget-2]');
						/*
						if ( is_search() ){?>
						<h2 class="block-title">Subscribe to Newsletter</h2>
						<div class="subscribe-text">Sign up with your e-mail to get tips, resources and amazing deals</div>
						<div class="mail-box">
						<input type="email" name="email" value="" placeholder="Enter your email here">
						<input class="submit" type="submit" value="Subscribe" /></div>
						<?} ?>
						<?
						if ( is_404() ) {?>
							<h2 class="block-title">Subscribe to Newsletter</h2>
						<div class="subscribe-text">Sign up with your e-mail to get tips, resources
and amazing deals</div> <div class="mail-box"> <input type="email" name="email" value="" placeholder="Enter your email here"> <input class="submit" type="submit" value="Subscribe" /></div>
						<?
						}
						?>
						*/
						?>
					</section> <!-- /.block -->
				</div>

			</div>
			</div>

			<div class="footer-right-menu-block-top">
				<div class="region region-footer-quick-links">
					<section id="block-menu-menu-footer-quick-links" class="block block-menu clearfix">
					  <ul class="menu nav">
					  		<li class="first leaf"><a href="http://www.businessbid.ae/feedback" title="">Feedback</a></li>
							<li class="leaf"><a href="http://www.businessbid.ae/aboutbusinessbid#career" title="">Careers</a></li>
							<li class="leaf"><a data-toggle="modal" data-target="#privacy-policy">Privacy policy</a></li>
							<li class="leaf"><a href="http://www.businessbid.ae/sitemap" title="">Site Map</a></li>
							<li class="last leaf"><a data-toggle="modal" data-target="#terms-conditions">Terms of Use</a></li>
					  </ul>
					</section> <!-- /.block -->
  				</div>
               	</div>


			</div>
		</div>
		</div>
	</div>
	<a href="#" class="live"></a>
</footer>
<div class="modal fade" id="terms-conditions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Terms &amp; Conditions</h4>
      </div>
	<iframe src="/htm-files/terms-conditions.html" width="100%" height="500" frameborder="0">
	</iframe>
    </div>
  </div>
</div>
<div class="modal fade" id="privacy-policy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
      </div>
	<iframe src="/htm-files/privacy-policy.html" width="100%" height="500" frameborder="0">
	</iframe>

    </div>
  </div>
</div>
</body>
</html>
<?php wp_footer(); ?>
</body>
</html>

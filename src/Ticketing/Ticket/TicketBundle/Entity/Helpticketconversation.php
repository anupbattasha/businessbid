<?php

namespace Ticketing\Ticket\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Helpticketconversation {
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $ticketId;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $ticketResponse;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ticketId
     *
     * @param integer $ticketId
     * @return Helpticketconversation
     */
    public function setTicketId($ticketId)
    {
        $this->ticketId = $ticketId;

        return $this;
    }

    /**
     * Get ticketId
     *
     * @return integer 
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Helpticketconversation
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set ticketResponse
     *
     * @param string $ticketResponse
     * @return Helpticketconversation
     */
    public function setTicketResponse($ticketResponse)
    {
        $this->ticketResponse = $ticketResponse;

        return $this;
    }

    /**
     * Get ticketResponse
     *
     * @return string 
     */
    public function getTicketResponse()
    {
        return $this->ticketResponse;
    }
    /**
     * @var \DateTime
     */
    private $ticketDatetime;


    /**
     * Set ticketDatetime
     *
     * @param \DateTime $ticketDatetime
     * @return Helpticketconversation
     */
    public function setTicketDatetime($ticketDatetime)
    {
        $this->ticketDatetime = $ticketDatetime;

        return $this;
    }

    /**
     * Get ticketDatetime
     *
     * @return \DateTime 
     */
    public function getTicketDatetime()
    {
        return $this->ticketDatetime;
    }
}

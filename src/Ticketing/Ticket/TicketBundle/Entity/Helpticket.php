<?php

namespace Ticketing\Ticket\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Helpticket {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorId;

    /**
     * @var integer
     */
    private $ticketTypeId;

    /**
     * @var string
     */
    private $ticketNo;

    /**
     * @var string
     */
    private $ticketSubject;

    /**
     * @var string
     */
    private $ticketQuestion;

    /**
     * @var string
     */
    private $ticketPriority;

    /**
     * @var string
     */
    private $ticketUploadFile;

    /**
     * @var string
     */
    private $ticketStatus;

    /**
     * @var \DateTime
     */
    private $ticketDatetime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorId
     *
     * @param integer $vendorId
     * @return Helpticket
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;

        return $this;
    }

    /**
     * Get vendorId
     *
     * @return integer 
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * Set ticketTypeId
     *
     * @param integer $ticketTypeId
     * @return Helpticket
     */
    public function setTicketTypeId($ticketTypeId)
    {
        $this->ticketTypeId = $ticketTypeId;

        return $this;
    }

    /**
     * Get ticketTypeId
     *
     * @return integer 
     */
    public function getTicketTypeId()
    {
        return $this->ticketTypeId;
    }

    /**
     * Set ticketNo
     *
     * @param string $ticketNo
     * @return Helpticket
     */
    public function setTicketNo($ticketNo)
    {
        $this->ticketNo = $ticketNo;

        return $this;
    }

    /**
     * Get ticketNo
     *
     * @return string 
     */
    public function getTicketNo()
    {
        return $this->ticketNo;
    }

    /**
     * Set ticketSubject
     *
     * @param string $ticketSubject
     * @return Helpticket
     */
    public function setTicketSubject($ticketSubject)
    {
        $this->ticketSubject = $ticketSubject;

        return $this;
    }

    /**
     * Get ticketSubject
     *
     * @return string 
     */
    public function getTicketSubject()
    {
        return $this->ticketSubject;
    }

    /**
     * Set ticketQuestion
     *
     * @param string $ticketQuestion
     * @return Helpticket
     */
    public function setTicketQuestion($ticketQuestion)
    {
        $this->ticketQuestion = $ticketQuestion;

        return $this;
    }

    /**
     * Get ticketQuestion
     *
     * @return string 
     */
    public function getTicketQuestion()
    {
        return $this->ticketQuestion;
    }

    /**
     * Set ticketPriority
     *
     * @param string $ticketPriority
     * @return Helpticket
     */
    public function setTicketPriority($ticketPriority)
    {
        $this->ticketPriority = $ticketPriority;

        return $this;
    }

    /**
     * Get ticketPriority
     *
     * @return string 
     */
    public function getTicketPriority()
    {
        return $this->ticketPriority;
    }

    /**
     * Set ticketUploadFile
     *
     * @param string $ticketUploadFile
     * @return Helpticket
     */
    public function setTicketUploadFile($ticketUploadFile)
    {
        $this->ticketUploadFile = $ticketUploadFile;

        return $this;
    }

    /**
     * Get ticketUploadFile
     *
     * @return string 
     */
    public function getTicketUploadFile()
    {
        return $this->ticketUploadFile;
    }

    /**
     * Set ticketStatus
     *
     * @param string $ticketStatus
     * @return Helpticket
     */
    public function setTicketStatus($ticketStatus)
    {
        $this->ticketStatus = $ticketStatus;

        return $this;
    }

    /**
     * Get ticketStatus
     *
     * @return string 
     */
    public function getTicketStatus()
    {
        return $this->ticketStatus;
    }

    /**
     * Set ticketDatetime
     *
     * @param \DateTime $ticketDatetime
     * @return Helpticket
     */
    public function setTicketDatetime($ticketDatetime)
    {
        $this->ticketDatetime = $ticketDatetime;

        return $this;
    }

    /**
     * Get ticketDatetime
     *
     * @return \DateTime 
     */
    public function getTicketDatetime()
    {
        return $this->ticketDatetime;
    }
    /**
     * @var integer
     */
    private $ticketUserType;


    /**
     * Set ticketUserType
     *
     * @param integer $ticketUserType
     * @return Helpticket
     */
    public function setTicketUserType($ticketUserType)
    {
        $this->ticketUserType = $ticketUserType;

        return $this;
    }

    /**
     * Get ticketUserType
     *
     * @return integer 
     */
    public function getTicketUserType()
    {
        return $this->ticketUserType;
    }
}

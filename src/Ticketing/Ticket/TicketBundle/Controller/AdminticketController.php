<?php

namespace Ticketing\Ticket\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use Ticket\TicketBundle\Entity\Helpticket;
use Ticketing\Ticket\TicketBundle\Entity\Helpticketconversation;
use BBids\BBidsHomeBundle\Entity\Account;

class AdminticketController extends Controller
{
    public function adminTicketHomeAction(Request $request)
    {
        $session = $this->container->get('session');
        if($session->has('uid')){
            if($session->get('pid')!=1){
                $session->getFlashBag()->add('error','Your are not authorised to access this area');

                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
            }
        }

        $uid = $session->get('uid');

        // $vendorTicketsListArray     = $this->getDoctrine()->getRepository('TicketingTicketTicketBundle:Helpticket')->findBy(array('ticketUserType'=>1), array('ticketDatetime' => 'DESC'));
        // $customerTicketsListArray   = $this->getDoctrine()->getRepository('TicketingTicketTicketBundle:Helpticket')->findBy(array('ticketUserType'=>2), array('ticketDatetime' => 'DESC'));
        return $this->render('TicketingTicketTicketBundle:Admin:vendor_ticket_list.html.twig',array('vendorTicketsList'=>$vendorTicketsListArray,'customerTicketsList'=>$customerTicketsListArray));
    }

    public function adminListTicketsAction($usertype)
    {
        $session = $this->container->get('session');
        if($session->has('uid')){
            if($session->get('pid')!=1){
                $session->getFlashBag()->add('error','Your are not authorised to access this area');

                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
            }
        }

        $uid = $session->get('uid');
        if($usertype == 'Vendor') {
            $ticketsListArray = $this->getDoctrine()->getRepository('TicketingTicketTicketBundle:Helpticket')->findBy(array('ticketUserType'=>1), array('ticketDatetime' => 'DESC'));
            return $this->render('TicketingTicketTicketBundle:Admin:vendor_ticket_list.html.twig',array('ticketsList'=>$ticketsListArray));
        }
        else {
            $ticketsListArray = $this->getDoctrine()->getRepository('TicketingTicketTicketBundle:Helpticket')->findBy(array('ticketUserType'=>2), array('ticketDatetime' => 'DESC'));
            return $this->render('TicketingTicketTicketBundle:Admin:customer_ticket_list.html.twig',array('ticketsList'=>$ticketsListArray));
        }

    }

    public function adminViewTicketAction(Request $request,$id)
    {
        $session = $this->container->get('session');
        if($session->has('uid')){
            if($session->get('pid')!=1){
                $session->getFlashBag()->add('error','Your are not authorised to access this area');

                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
            }
        }

        $uid = $session->get('uid');
        $ticketDetails = $this->getDoctrine()
                            ->getRepository('TicketingTicketTicketBundle:Helpticket')
                            ->findOneById($id);
        $status         = $ticketDetails->getTicketStatus();
        $userId         = $ticketDetails->getVendorId();
        $ticketUserType = $ticketDetails->getTicketUserType();
        $subject        = $ticketDetails->getTicketSubject();
        $userAccount    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userId));
        $userArray      = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->find($userId);
        if(empty($userArray)) {
            $session->getFlashBag()->add('error','User is deleted from the system.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_ticket_adminlist',array('usertype'=>'Vendor')));
        }
        $email          = $userArray->getEmail();
        if($ticketDetails)
        {
            $ticketConversation = $this->getDoctrine()
                                ->getRepository('TicketingTicketTicketBundle:Helpticketconversation')
                                ->findBy(array('ticketId'=>$id));

            $request            = $this->container->get('request');
            $createReplyForm    = $this->createFormBuilder()
                                ->add('reply', 'textarea', array('required'=>TRUE,'trim'=>TRUE,'label'=>'Reply','attr'=>array('class'=>'form-control','placeholder'=>'Enter your comments')))
                                ->add('status', 'choice', array('choices'=>array('1'=>'Open', '2'=>'Close', '3'=>'Pending'), 'multiple'=>False, 'expanded'=>false, 'data'=>$status))
                                ->add('ticketno', 'hidden', array('data'=>$id))
                                ->add('submit', 'submit',array('attr'=>array('class'=>'btn btn-sm btn-warning')))
                                ->getForm();
            $createReplyForm->handleRequest($request);
            if($request->isMethod('POST')) {
                if($createReplyForm->isValid()){
                    $postdata       = $createReplyForm->getData();
                    $reply          = htmlentities($postdata['reply']);
                    $ticketstatus   = (int)$postdata['status'];
                    $ticketno       = $postdata['ticketno'];
                    if($reply == "") {

                        $session->getFlashBag()->add('error','Please enter reply');
                        return $this->render('TicketingTicketTicketBundle:Admin:ticketview.html.twig',array('form'=>$createReplyForm->createView(),'ticketDetails'=>$ticketDetails,'ticketConversation'=>$ticketConversation,'ticketid'=>$id,'userAccount'=>$userAccount));
                    }
                    else {

                        $created = new \Datetime();

                        $em = $this->getDoctrine()->getManager();

                        $adminTicketReply = new Helpticketconversation();

                        $adminTicketReply->setTicketId($ticketno);
                        $adminTicketReply->setUserId(0);
                        $adminTicketReply->setTicketResponse($reply);
                        $adminTicketReply->setTicketDatetime($created);

                        $em->persist($adminTicketReply);

                        $em->flush();

                        /* Update ticket status*/
                        $em = $this->getDoctrine()->getManager();
                        $ticketDetails = $em->getRepository('TicketingTicketTicketBundle:Helpticket')->find($id);

                        $ticketDetails->setTicketStatus($ticketstatus);
                        $em->flush();


                        $this->sendEmail($email,$subject,$reply,$userId);

                        $session->getFlashBag()->add('success','Reply successful');
                        return $this->redirect($request->headers->get('referer'));
                    }
                }
            }
            return $this->render('TicketingTicketTicketBundle:Admin:ticketview.html.twig',array('form'=>$createReplyForm->createView(),'ticketDetails'=>$ticketDetails,'ticketConversation'=>$ticketConversation,'ticketid'=>$id,'userAccount'=>$userAccount,'ticketUserType'=>$ticketUserType));
        }
        else
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));

    }

    private function sendEmail($email,$subject,$body,$uid)
    {
        $url  = 'https://api.sendgrid.com/';
        $user = 'businessbid';
        $pass = '!3zxih1x0';
        /*$subject = 'Welcome to the BusinessBid Network';

        $body=*/
        $em = $this->getDoctrine()->getManager();
        $useremail = new Usertoemail();

        $useremail->setFromuid(11); //11 admin
        $useremail->setTouid($uid);
        $useremail->setFromemail('support@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj($subject);
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);

        $em->persist($useremail);
        $em->flush();

        $message = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $email,
            'subject'   => $subject,
            'html'      => $body,
            'text'      => $body,
            'from'      => 'support@businessbid.ae',
         );


        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $sess = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($sess, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        // Tell curl not to return headers, but do return the response
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($sess);
        curl_close($sess);
    }
}

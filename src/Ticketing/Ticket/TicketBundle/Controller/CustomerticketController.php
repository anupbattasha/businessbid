<?php

namespace Ticketing\Ticket\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use Ticketing\Ticket\TicketBundle\Entity\Helpticket;
use Ticketing\Ticket\TicketBundle\Entity\Helpticketconversation;
use BBids\BBidsHomeBundle\Entity\Freetrail;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;

class CustomerticketController extends Controller
{
    public function customerTicketHomeAction(Request $request)
    {
        $session = $this->container->get('session');
        if($session->has('uid'))
		{

			$uid = $session->get('uid');

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
					->select('count(l.id)')
					->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
					->add('where','l.vendorid = :uid')
					->andWhere('l.status = 1')
					->setParameter('uid', $uid);
			$useridexists = $query->getQuery()->getSingleScalarResult();

			$request 			= $this->container->get('request');
			$createTicketForm 	= $this->createFormBuilder()
								->add('subject', 'text', array('required'=>TRUE,'trim'=>TRUE,'label'=>'Subject','attr'=>array('placeholder'=>'Enter Subject')))
								->add('issuetype', 'choice', array('required'=>TRUE, 'label'=>'Related Issue','choices'=>array('1'=>'Account Suspension', '2'=>'Identity Verification', '3'=>'Reviews & Ratings', '4'=>'System Issue', '5'=>'Vendor Dispute', '6'=>'Email Address Change', '0'=>'Other Issue'), 'multiple'=>FALSE, 'empty_data'=>NULL, 'empty_value'=>'Choose Issue Type', 'expanded'=>FALSE))
								->add('question', 'textarea', array('required'=>TRUE,'trim'=>TRUE,'label'=>'Your Question','attr'=>array('placeholder'=>'Enter Your Question')))
								->add('priority', 'choice', array('required'=>TRUE, 'label'=>'Severity','choices'=>array('1'=>'Low', '2'=>'Medium', '3'=>'High'), 'multiple'=>FALSE, 'empty_data'=>NULL, 'empty_value'=>'Choose Severity', 'expanded'=>FALSE))
								->add('upload', 'file', array('required'=>FALSE,'label'=>'Attach File (optional)'))
								->add('submit', 'submit')
								->getForm();
			$createTicketForm->handleRequest($request);
			if($request->isMethod('POST')) {
				if($createTicketForm->isValid()){
					$postdata 	= $createTicketForm->getData();
					$subject 	= $postdata['subject'];
					$issuetype 	= $postdata['issuetype'];
					$question 	= $postdata['question'];
					$priority 	= $postdata['priority'];

					if($subject == ""){
						$session = $this->container->get('session');
						$session->getFlashBag()->add('error','Please enter subject');
						return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
					}
					else if ($issuetype == "") {
						$session = $this->container->get('session');
						$session->getFlashBag()->add('error','Please select related issue');
						return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
					}
					else if ($question == "") {
						$session = $this->container->get('session');
						$session->getFlashBag()->add('error','Please enter the enter question');
						return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
					}
					else if ($priority == "") {
						$session = $this->container->get('session');
						$session->getFlashBag()->add('error','Please select the priority');
						return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
					}
					else
					{
						$ticketsSeries = 0000;
						$em = $this->getDoctrine()->getManager();

						$query = $em->createQuery('SELECT MAX(h.ticketNo) FROM TicketingTicketTicketBundle:Helpticket h WHERE h.ticketUserType = :type')->setParameter('type', '2');
						$ticketsSeries = $query->getSingleScalarResult();

						$ticketNo = (int)$ticketsSeries+1;
						$ticketNo = str_pad($ticketNo, 4, "0", STR_PAD_LEFT);

						$fileName = '';
						// echo '<pre/>';print_r($_FILES);exit;
						$allowed = array('png', 'jpg', 'gif','jpeg','pdf','doc','docx');
						if(isset($_FILES['form']) && $_FILES['form']['error']['upload'] == 0){

							$extension = pathinfo($_FILES['form']['name']['upload'], PATHINFO_EXTENSION);

							if(!in_array(strtolower($extension), $allowed)){

								$session = $this->container->get('session');
								$session->getFlashBag()->add('error','File type not allowed ');
								return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
							}

							$upload_dir= '/var/www/html/web/support/uploads/';
							$six_digit_random_number = mt_rand(100000, 999999);
							$fileName = $six_digit_random_number.'.'.$extension;
							if(move_uploaded_file($_FILES['form']['tmp_name']['upload'], $upload_dir.$fileName)) {

							}
							else {
								$session = $this->container->get('session');
								$session->getFlashBag()->add('error','Opps unable to upload the file ');
								return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
							}
						}


						// echo $extension; exit;
						$created = new \Datetime();

						$em = $this->getDoctrine()->getManager();

						$vendorTicket = new Helpticket();

						$vendorTicket->setVendorId($uid);
						$vendorTicket->setTicketTypeId($issuetype);
						$vendorTicket->setTicketNo($ticketNo);
						$vendorTicket->setTicketSubject($subject);
						$vendorTicket->setTicketQuestion($question);
						$vendorTicket->setTicketPriority($priority);
						$vendorTicket->setTicketUploadFile($fileName);
						$vendorTicket->setTicketStatus(1);
						$vendorTicket->setTicketDatetime($created);
						$vendorTicket->setTicketUserType(2);

						$em->persist($vendorTicket);

						$em->flush();

						$session = $this->container->get('session');
						$session->getFlashBag()->add('success','Create ticket successful');

						$email 		=  $session->get('email');
						$subject 	= 'BusinessBid Network Support Ticket';
						$body 		= '<p><span>Thank you for your submitting a support ticket.</span><br /><span>&nbsp;</span><br /><span>Your ticket reference is #'.$ticketNo.'</span><br /><span>&nbsp;</span><br /><span>Our support teams will attend to your query and respond to you within 2 business days.</span><br /><span>&nbsp;</span><br /><span>If the matter is urgent please contact the customer support line on 1800 555 323.&nbsp;</span></p>';
						$this->sendEmail($email,$subject,$body,$uid);

						return $this->redirect($request->headers->get('referer'));
					}
				}
			}
			else
				return $this->render('TicketingTicketTicketBundle:Customer:index.html.twig',array('ticketform'=>$createTicketForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists));
		}
        else
        {
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
        }
    }

    function customerTicketListAction()
    {
    	$session = $this->container->get('session');
        if($session->has('uid'))
		{

			$uid = $session->get('uid');

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
					->select('count(l.id)')
					->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
					->add('where','l.vendorid = :uid')
					->andWhere('l.status = 1')
					->setParameter('uid', $uid);
			$useridexists = $query->getQuery()->getSingleScalarResult();

			$ticketListArray = $this->getDoctrine()->getRepository('TicketingTicketTicketBundle:Helpticket')->findBy(array('vendorId'=>$uid), array('ticketDatetime' => 'DESC'));


			return $this->render('TicketingTicketTicketBundle:Customer:ticketlist.html.twig',array('useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists,'listData'=>$ticketListArray));
		}
        else
        {
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
        }
    }

    function customerViewTicketAction(Request $request,$id)
    {
    	/*echo $id; exit;*/
    	$session = $this->container->get('session');
        if($session->has('uid'))
		{

			$uid = $session->get('uid');

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
					->select('count(l.id)')
					->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
					->add('where','l.vendorid = :uid')
					->andWhere('l.status = 1')
					->setParameter('uid', $uid);
			$useridexists = $query->getQuery()->getSingleScalarResult();

			$ticketDetails = $this->getDoctrine()
							->getRepository('TicketingTicketTicketBundle:Helpticket')
							->findOneById($id);
			$subject = 	$ticketDetails->getTicketSubject();
			if($ticketDetails)
			{
				$ticketConversation = $this->getDoctrine()
									->getRepository('TicketingTicketTicketBundle:Helpticketconversation')
									->findBy(array('ticketId'=>$id));

				$request 			= $this->container->get('request');
				$createReplyForm 	= $this->createFormBuilder()
									->add('reply', 'textarea', array('required'=>TRUE,'trim'=>TRUE,'label'=>'Reply','attr'=>array('class'=>'form-control','placeholder'=>'Reply with your comments')))
									->add('ticketno', 'hidden', array('data'=>$id))
									->add('submit', 'submit',array('attr'=>array('class'=>'btn btn-sm btn-warning')))
									->getForm();
				$createReplyForm->handleRequest($request);
				if($request->isMethod('POST')) {
					if($createReplyForm->isValid()){
						$postdata 	= $createReplyForm->getData();
						$reply 		= htmlentities($postdata['reply']);
						$ticketno 	= $postdata['ticketno'];
						if($reply == ""){
							$session->getFlashBag()->add('error','Please enter reply');
							return $this->render('TicketingTicketTicketBundle:Customer:ticketview.html.twig',array('form'=>$createReplyForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists,'ticketDetails'=>$ticketDetails,'ticketConversation'=>$ticketConversation,'ticketid'=>$id));
						}
						else {

							$created = new \Datetime();

							$em = $this->getDoctrine()->getManager();

							$vendorTicketReply = new Helpticketconversation();

							$vendorTicketReply->setTicketId($ticketno);
							$vendorTicketReply->setUserId($uid);
							$vendorTicketReply->setTicketResponse($reply);
							$vendorTicketReply->setTicketDatetime($created);

							$em->persist($vendorTicketReply);

							$em->flush();

							$email 		=  $session->get('email');
							$this->sendEmail($email,$subject,$reply,$uid);

							$session->getFlashBag()->add('success','Reply successful');
							return $this->redirect($request->headers->get('referer'));
						}
					}
				}
				return $this->render('TicketingTicketTicketBundle:Customer:ticketview.html.twig',array('form'=>$createReplyForm->createView(),'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists,'ticketDetails'=>$ticketDetails,'ticketConversation'=>$ticketConversation,'ticketid'=>$id));
			}
			else
				return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
        else
        {
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
        }
    }

    private function sendEmail($email,$subject,$body,$uid)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'businessbid';
        $pass = '!3zxih1x0';
        /*$subject = 'Welcome to the BusinessBid Network';

        $body=*/
        $em = $this->getDoctrine()->getManager();
        $useremail = new Usertoemail();

		$useremail->setFromuid($uid);
		$useremail->setTouid(11);//11 means admin
		$useremail->setFromemail('support@businessbid.ae');
		$useremail->setToemail($email);
		$useremail->setEmailsubj($subject);
		$useremail->setCreated(new \Datetime());
		$useremail->setEmailmessage($body);
		$useremail->setEmailtype(1);
		$useremail->setStatus(1);

		$em->persist($useremail);
		$em->flush();

        $message = array(
	        'api_user'  => $user,
	        'api_key'   => $pass,
	        'to'        => $email,
	        'subject'   => $subject,
	        'html'      => $body,
	        'text'      => $body,
	        'from'      => 'support@businessbid.ae',
         );


        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $sess = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($sess, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        // Tell curl not to return headers, but do return the response
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($sess);
        curl_close($sess);
    }
}

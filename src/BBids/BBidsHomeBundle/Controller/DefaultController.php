<?php

namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BBids\BBidsHomeBundle\Entity\Usertoemail;

class DefaultController extends Controller
{
	public function postemailAction()
	{
		$em = $this->getDoctrine()->getManager();

		$useremail = new Usertoemail();

                                        $useremail->setFromuid(1);
                                        $useremail->setTouid(0);
                                        $useremail->setFromemail('info@businessbid.ae');
                                        $useremail->setToemail('anup@netiapps.com');
                                        $useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:Home:feedbackemail.html.twig'));
                                        $useremail->setEmailtype(1);
                                        $useremail->setStatus(1);

                                        $em->persist($useremail);
                                        $em->flush();
		exit;
	}

    public function indexAction($name)
    {
        return $this->render('BBidsBBidsHomeBundle:Default:index.html.twig', array('name' => $name));
    }
	
    
}

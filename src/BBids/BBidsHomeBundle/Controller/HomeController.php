<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BBids\BBidsHomeBundle\Entity\Enquiry;
use BBids\BBidsHomeBundle\Entity\Categories;
use Symfony\Component\HttpFoundation\Response;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use Symfony\Component\Validator\Mapping\ClassMetaData;
use Symfony\Component\Validator\Constraints as Assert;
use BBids\BBidsHomeBundle\Entity\Account;
use BBids\BBidsHomeBundle\Entity\Activity;
use BBids\BBidsHomeBundle\Entity\Ratings;
use BBids\BBidsHomeBundle\Entity\Freetrail;
use BBids\BBidsHomeBundle\Entity\Contactus;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorcityrel;
use BBids\BBidsHomeBundle\Entity\Vendorsubcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Sessioncategoriesrel;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use BBids\BBidsHomeBundle\Entity\Vendoralbumrel;
use BBids\BBidsHomeBundle\Entity\Albumphotorel;
use BBids\BBidsHomeBundle\Entity\Sessionquotes;
use BBids\BBidsHomeBundle\Entity\Vendorservices;
use BBids\BBidsHomeBundle\Form\DirectVendorEnquiryForm;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use BBids\BBidsHomeBundle\Entity\Vendorlogorel;

class HomeController extends Controller
{
	public function getemailmessagesAction(Request $request)
	{

		$session = $this->container->get('session');

		if($session->has('uid')){

		$sessionid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
                               ->select('count(f.userid)')
                               ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                               ->add('where','f.userid = :uid')
                               ->setParameter('uid', $sessionid);

        $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

        $query = $em->createQueryBuilder()
                         ->select('count(l.id)')
                         ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                         ->add('where','l.vendorid = :uid')
                         ->andWhere('l.status = 1')
                         ->setParameter('uid', $sessionid);
        $useridexists = $query->getQuery()->getSingleScalarResult();

        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid OR fromuid = $sessionid ORDER BY id DESC");
        $statement->execute();
        $emailArray = $statement->fetchAll();

		$queryenquiry = $em->createQueryBuilder()
                        ->select(array('a.userid,v.vendorid,v.enquiryid,c.category,e.subj, e.created, e.id, e.authorid,u.email, e.city,v.acceptstatus'))
                        ->from('BBidsBBidsHomeBundle:Enquiry','e')
                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
                        ->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'v.vendorid=u.id')
                        ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id=a.userid')
                        ->leftJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
                        ->add('where','v.vendorid = :uid')
                        ->andWhere('e.status = 1')
                        ->setParameter('uid', $sessionid)
                        ->add('orderBy', 'e.created DESC')
                        ->setMaxResults(10);

		$enquiriesvendor = $queryenquiry->getQuery()->getResult();

		$searchform = $this->createFormBuilder()
				->add('from', 'text', array('attr'=>array('placeholder'=>'mm/dd/yyyy', 'id'=>'datepicker1'), 'required'=>false))
				->add('to', 'text', array('attr'=>array('placeholder'=>'mm/dd/yyyy', 'id'=>'datepicker2'), 'required'=>false))
				->add('email', 'hidden', array('attr'=>array('placeholder'=>'Email id'), 'required'=>false))
				->add('search', 'submit')
				->getForm();

        $qb = $em->createQueryBuilder();
        $q = $qb->update('BBidsBBidsHomeBundle:Usertoemail', 'e')
                ->set('e.readStatus', 1)
                ->where('e.touid = :uid or e.fromuid = :fid')
                ->setParameter('fid', $sessionid)
                ->setParameter('uid', $sessionid)
                ->getQuery();
        $p = $q->execute();

        $emailCount = $this->getMessageCount();
		$searchform->handleRequest($request);

		if($request->isMethod('POST')){
			$fromdate = $searchform['from']->getData();
			if($fromdate != ""){
				$fromArray = explode('/', $fromdate);
				$fromdate = $fromArray[2]."-".$fromArray[0]."-".$fromArray[1];
			}

			$todate = $searchform['to']->getData();
			if($todate != ""){
				$toArray = explode('/', $todate);
				$todate = $toArray[2]."-".$toArray[0]."-".$toArray[1];
			}
			$email = $searchform['email']->getData();

			if($fromdate == "" && $todate ==""){
				$session->getFlashBag()->add('error', 'Please fill one of the search fields to get a result!');

				return $this->render('BBidsBBidsHomeBundle:Home:mymessages.html.twig', array('form'=>$searchform->createView(), 'emails'=>$emailArray, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists, 'enquiriesvendorcount'=>$enquiriesvendor,'emailCount'=>$emailCount));

			}

			$em = $this->getDoctrine()->getManager();

			$connection = $em->getConnection();

			if($email != "" && $fromdate == "" && $todate == ""){
				$results = $this->getDoctrine()->getRepository()->findBy(array('fromemail'=>$email, 'touid'=>$sessionid));
			}
			else if($email != "" && $fromdate != "" && $todate == "") {
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND fromemail like '$email' AND date(created) >= '$fromdate'");
                                $statement->execute();
                                $results = $statement->fetchAll();
			}
			else if($email != "" && $fromdate == "" && $todate != ""){
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND fromemail like '$email' AND date(created) <= '$todate'");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}
			else if($email == "" && $fromdate != "" && $todate == ""){
				$statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND date(created) >= '$fromdate'");
				//$statement->bindValue('fromdate', $fromdate);
				$statement->execute();
				$results = $statement->fetchAll();


			}
			else if($email == "" && $fromdate != "" && $todate != ""){
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND date(created) between '$fromdate' AND '$todate' ");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}
			else if($email == "" && $fromdate == "" && $todate != "" ){
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND date(created) <= '$todate'");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}
			else if($email != "" && $fromdate != "" && $todate != ""){
				$statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND fromemail like '$email' AND date(created) between '$fromdate' AND '$todate' ");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}

			return $this->render('BBidsBBidsHomeBundle:Home:mymessages.html.twig', array('form'=>$searchform->createView(), 'emails'=>$results, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists, 'enquiriesvendorcount'=>$enquiriesvendor,'emailCount'=>$emailCount));

		}

		/*$emailArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Usertoemail')->findBy(array('touid'=>$sessionid));*/

		return $this->render('BBidsBBidsHomeBundle:Home:mymessages.html.twig', array('form'=>$searchform->createView(), 'emails'=>$emailArray, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists, 'enquiriesvendorcount'=>$enquiriesvendor,'emailCount'=>$emailCount));

		}
		else {
			$session->getFlashBag()->add('error', 'It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');

			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function conreviewsAction($offset)
	{
		$session = $this->container->get('session');

		if($session->has('uid')){
			$em = $this->getDoctrine()->getManager();

			$sessionid = $session->get('uid');

			$query = $em->createQueryBuilder()
				->select('r.review, r.created, r.rating, r.businessknow, r.businessrecomond, r.ratework, r.rateservice, r.ratemoney, r.ratebusiness, r.servicesummary, r.serviceexperience, a.contactname, e.subj')
				->from('BBidsBBidsHomeBundle:Reviews', 'r')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = r.vendorid')
				->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'e.id = r.enquiryid')
				->where('r.authorid = :authorid')
				->setParameter('authorid', $sessionid);

			$reviews = $query->getQuery()->getArrayResult();

			$query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $sessionid);
                        $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


							$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $sessionid);

                         $useridexists = $query->getQuery()->getSingleScalarResult();

			return $this->render('BBidsBBidsHomeBundle:Home:conreviews.html.twig', array('reviews'=>$reviews, 'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads));
		}
		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function enviewAction($enquiryid)
	{
		$session = $this->container->get('session');

		if($session->has('uid')){

		 	$em =  $this->getDoctrine()->getManager();
			$uid = $session->get('uid');
			$em = $this->getDoctrine()->getmanager();
                        $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                        $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


			$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);

               		 $useridexists = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('e.subj,e.id, e.description, e.city, e.created, e.updated, e.location, c.category, a.contactname, a.address, a.smsphone, a.homephone')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'c.id = e.category')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = e.authorid')
				->where('e.id = :enquiryid')
				->setParameter('enquiryid', $enquiryid);

			$enquiry = $query->getQuery()->getArrayResult();

			return $this->render('BBidsBBidsHomeBundle:Home:enview.html.twig', array('enquiry'=>$enquiry,'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads));

		}
		else {
			$session->getFlashBag()->add('error', 'It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae.');

			return $this->redirect('b_bids_b_bids_home_homepage');
		}
	}

	public function myreviewsAction($offset)
	{
		$session = $this->container->get('session');

		$sessionid = $session->get('uid');
		$uid = $session->get('uid');
		$em = $this->getDoctrine()->getmanager();
			$query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);

                $useridexists = $query->getQuery()->getSingleScalarResult();

		if($session->has('uid')){
			$start = (($offset * 4) - 4);

			$from = $start + 1;

	                $to = $start + 4;



			$query = $em->createQueryBuilder()
				->select('r.id as revid,r.review, r.author,e.subj,e.description, r.created, r.rating, r.businessknow, r.businessrecomond, r.ratework, r.rateservice, r.ratemoney, r.ratebusiness, r.servicesummary, r.serviceexperience,e.id,e.description,e.location,c.category,e.created as encreated ')
				->from('BBidsBBidsHomeBundle:Reviews', 'r')
				->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'r.enquiryid = e.id')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
				->where('r.vendorid = :vendorid')
				->setFirstResult($start)
				->setMaxResults(4)
				->setParameter('vendorid', $sessionid);

			$reviews = $query->getQuery()->getArrayResult();



			$query = $em->createQueryBuilder()
				->select('count(r.id )')
				->from('BBidsBBidsHomeBundle:Reviews', 'r')
				->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'r.enquiryid = e.id')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
				->where('r.vendorid = :vendorid')
				->setParameter('vendorid', $sessionid);

			$count = $query->getQuery()->getSingleScalarResult();

			 if($to > $count)
			 {

              $to =$count;

			 }
            $emailCount = $this->getMessageCount();
			return $this->render('BBidsBBidsHomeBundle:Home:myreviews.html.twig', array('reviews'=>$reviews, 'from'=>$from, 'to'=>$to, 'count'=>$count,'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));
		}
		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function setsessioncategoryold($catid)
	{

		$catArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('category'=>$catid));

		foreach($catArray as $c){
			$cid = $c->getId();
		}

		$session = $this->container->get('session');

		$sessionid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$sessioncatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessioncategoriesrel')->findBy(array('userid'=>$sessionid));

		if(!empty($sessioncatArray)){
			foreach($sessioncatArray as $sc){
				$sid = $sc->getId();

				$scat = $em->getRepository('BBidsBBidsHomeBundle:Sessioncategoriesrel')->find($sid);

				$em->remove($scat);
				$em->flush();
			}
		}

		$sessioncat = new Sessioncategoriesrel();

		$sessioncat->setUserid($sessionid);
		$sessioncat->setCategoryid($cid);

		$em->persist($sessioncat);
		$em->flush();

		return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_choose_lead_pack'));
	}
	public function setsessioncategoryAction($catid)
	{

		$catArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('category'=>$catid));

		foreach($catArray as $c){
			$cid = $c->getId();
		}

		$session = $this->container->get('session');

		$sessionid = $session->get('uid');
		$purchaseID = rand ( 10000 , 99999 );
		$_SESSION['leads'][$purchaseID]['selIDs'][0] = $cid;
		//echo 'jdgjfghjhdg'.$catid.'<<>>'.$_SESSION['leads'][$purchaseID]['selIDs'][0].'<<>>'.$purchaseID;exit;

		return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_lead_plan',array('purchaseID'=>$purchaseID)));
	}

	public function freetrialorderAction()
	{
		$session = $this->container->get('session');

		if($session->has('uid')){

			$sessionid = $session->get('uid');


			$uid = $session->get('uid');

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
				->select('f.orderno, f.status, f.created, f.updated, f.leads, f.duration, c.category')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'c.id = f.category')
				->where('f.userid = :vendorid')
				->setParameter('vendorid', $sessionid);

			$trialArray = $query->getQuery()->getArrayResult();
			$query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                                 //->setParameter('uid', 333);
                        //~ echo $myquery = $query->getQuery()->getSQL();
                 	$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
 			$query = $em->createQueryBuilder()
                                ->select('count(l.userid)')
                                ->from('BBidsBBidsHomeBundle:Usertotryleadtrel', 'l')
                                ->add('where','l.userid = :uid')
                                ->setParameter('uid', $uid);

                $useridexists = $query->getQuery()->getSingleScalarResult();


			return $this->render('BBidsBBidsHomeBundle:Home:freetrialorder.html.twig', array('order'=>$trialArray,'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads));
		} else {
			$session->getFlashBag()->add('error', 'It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');

			return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
		}
	}

	public function node6Action()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:node6.html.twig');
	}

	public function aboutbusinessbidAction()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig');
	}

	public function dashboardAction()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:dashboard.html.twig');
	}

	public function albumviewAction($albumid)
	{
		$albumArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendoralbumrel')->findBy(array('albumid'=>$albumid));

		$photoArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Albumphotorel')->findBy(array('albumid'=>$albumid));

		return $this->render('BBidsBBidsHomeBundle:Home:viewgallery.html.twig', array('albuminfo'=>$albumArray, 'photos'=>$photoArray));
	}

    function reviewsListByCategoryAction($catid,Request $request) {
        if(empty($catid)) {
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
        }
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;

        $page =  (isset($_GET['page'])) ? $_GET['page']: 1;
        $limit =  (isset($_GET['limit'])) ? $_GET['limit']: 10;

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        $category = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$catid));
        $categoryname = $category->getCategory();
        // echo '<pre/>';print_r($category);exit;

        $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));
        $offset = 1;
        $start = (($offset * 10) - 10);

        $session = $this->container->get('session');
        $session->start();
        $quoteID  = $session->getId();

        $sessionQuotes    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findBy(array('quoteid'=>$quoteID,'categoryid'=>$catid));

        $selVendorList  = array();
        foreach ($sessionQuotes as $value) {
            $selVendorList[$value->getVendorid()]['name'] = $value->getVname();
            $selVendorList[$value->getVendorid()]['status'] = $value->getStatus();
            $selVendorList[$value->getVendorid()]['id'] = $value->getVendorid();
        }

        $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$catid));
        $subcategory = array();

        foreach($subcategoryList as $sublist){
            $subid = $sublist->getId();
            $subcat = $sublist->getCategory();
            $subcategory[$subid] = $subcat;
        }

        $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
        $cityArray = array();

        foreach($cities as $c){
            $city = $c->getCity();
            $cityArray[$city] = $city;
        }

        $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$catid));
        if($formOptions) {
            $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
            $categoryForm = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$catid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>'','loginFlag'=>$loginFlag,'reqType'=>'popup'));
            $twigFileName = $formOptions->getTwigName();
        } else {
            $session->getFlashBag()->add('error',"Coming Soon - We are currently working on this service. We won't be too long and call us on 04 42 13 777 if you need assistance");
            return $this->render('::404error.html.twig');
        }
        // echo '<pre/>';print_r(json_encode($formOptions));exit;
        $multiVendorsform = $this->createFormBuilder()
            ->add('com1', 'text', array('required'=>false, 'label'=>'Company 1', 'attr'=>array('placeholder'=>'Select company', 'class'=>'form-control')))
            ->add('com2', 'text', array('required'=>false, 'label'=>'Company 2', 'attr'=>array('placeholder'=>'Select company', 'class'=>'form-control')))
            ->add('com3', 'text', array('required'=>false, 'label'=>'Company 3', 'attr'=>array('placeholder'=>'Select company', 'class'=>'form-control')))
            ->add('com4', 'text', array('required'=>false, 'label'=>'Company 4', 'attr'=>array('placeholder'=>'Select company', 'class'=>'form-control')))
            ->add('com5', 'text', array('required'=>false, 'label'=>'Company 5', 'attr'=>array('placeholder'=>'Select company', 'class'=>'form-control')))->getForm();
        $filterForm = $this->get('form.factory')->createNamedBuilder('filterform', 'form',  null, array())
            ->add('sort_type', 'hidden', array('data'=>''))
            ->add('offset', 'hidden', array('data'=>1))->getForm();
        $filterForm->handleRequest($request);


        if($request->isMethod('POST')) {
            // echo '<pre/>';print_r($filterForm->getData());exit;
            $sortType = $filterForm['sort_type']->getData();
            if(empty($sortType)) {
                $offset = $filterForm['offset']->getData();
                $sort = 'r.rating DESC';
            } else {
                switch ($sortType) {
                    case 'atoz':
                        $sort = 'a.bizname ASC';
                        break;
                    case 'ztoa':
                        $sort = 'a.bizname DESC';
                        break;
                    default:
                        $sort = 'r.rating DESC';
                        break;
                }
            }
            $start = (($offset * 10) - 10);

            $query = $em->createQueryBuilder()
                    ->select('a.bizname, a.userid, a.logopath, r.rating, l.fileName') //, count(re.id)
                    ->from('BBidsBBidsHomeBundle:Account', 'a')
                    ->innerJoin('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'c', 'with', 'a.userid=c.vendorid')
                    ->leftJoin('BBidsBBidsHomeBundle:Ratings', 'r', 'with', 'a.userid=r.vendorid')
                    ->leftJoin('BBidsBBidsHomeBundle:Vendorlogorel', 'l', 'with', 'a.userid=l.vendorid')
                    ->add('where','c.categoryid = :categoryid')
                    ->andWhere('a.test_account is null')
                    ->add('groupBy', 'a.bizname')
                    ->add('orderBy',$sort)
                    ->setParameter('categoryid', $catid)
                    ->setFirstResult($start)
                    ->setMaxResults($limit);
            $reviewList = $query->getQuery()->getResult();
        } else {
            if($page != 1) {
                $start = $page * 10 - 10;
            }
            $query = $em->createQueryBuilder()
                    ->select('a.bizname, a.userid, a.logopath, r.rating, l.fileName') //, count(re.id)
                    ->from('BBidsBBidsHomeBundle:Account', 'a')
                    ->innerJoin('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'c', 'with', 'a.userid=c.vendorid')
                    ->leftJoin('BBidsBBidsHomeBundle:Ratings', 'r', 'with', 'a.userid=r.vendorid')
                    ->leftJoin('BBidsBBidsHomeBundle:Vendorlogorel', 'l', 'with', 'a.userid=l.vendorid')
                    ->add('where','c.categoryid = :categoryid')
                    ->andWhere('a.test_account is null')
                    ->add('groupBy', 'a.bizname')
                    ->setParameter('categoryid', $catid)
                    ->setFirstResult($start)
                    ->setMaxResults($limit);
            $reviewList = $query->getQuery()->getResult();
        }

        // Fetch the count of vendor reviews
        $reviewQuery = $connection->prepare("SELECT vendorid,COUNT(*) as reviewcount FROM bbids_reviews GROUP BY vendorid");
        $reviewQuery->execute();
        $reviewCountArray = $reviewQuery->fetchAll();
        $rcGroup = array();
        foreach ($reviewCountArray as $value) {
            $rcGroup[$value['vendorid']][] = $value['reviewcount'];
        }
        // echo '<pre/>';print_r($reviewList);exit;

        // Fetch the service city by vendor
        $cityQuery = $connection->prepare("SELECT vc.vendorid,c.city FROM bbids_vendorcityrel vc INNER JOIN bbids_city c ON vc.city = c.id WHERE vendorid IN (SELECT vendorid FROM bbids_vendorcategoriesrel WHERE categoryid = $catid)");
        $cityQuery->execute();
        $vendorCityArray = $cityQuery->fetchAll();

        $vcGroup = array();
        foreach ($vendorCityArray as $value) {
            $vcGroup[$value['vendorid']][] = $value['city'];
        }

        // Fetch the services provided by vendor
        $serviceQuery = $connection->prepare("SELECT vs.vendorid,vs.category FROM bbids_vendorservices vs WHERE vendorid IN (SELECT vendorid FROM bbids_vendorcategoriesrel WHERE categoryid = $catid)");
        $serviceQuery->execute();
        $vendorServiceArray = $serviceQuery->fetchAll();

        $vsGroup = array();
        foreach ($vendorServiceArray as $value) {
            $vsGroup[$value['vendorid']][] = $value['category'];
        }

        /*$paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);
        $paginator->setUseOutputWalkers(false);
        $totalRows = count($paginator);*/
        $repository    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel');
        $totalRows = $repository->createQueryBuilder('u')
            ->select('count(u.id)')
            ->leftJoin('BBidsBBidsHomeBundle:Ratings', 'r', 'with', 'u.vendorid=r.vendorid')
            ->add('where','u.categoryid = :categoryid')
            ->setParameter('categoryid', $catid)
            ->getQuery()
            ->getSingleScalarResult();
        $pagination = $this->createLinks( 3, 10, $page, $totalRows);

        return $this->render('BBidsBBidsHomeBundle:Home:reviews_list.html.twig',array('categories'=>$categoriesArray,'reviewList'=>$reviewList,'vcGroup'=>$vcGroup,'vsGroup'=>$vsGroup,'rcGroup'=>$rcGroup,'categoryname'=>$categoryname,'catid'=>$catid,'vList'=>$selVendorList,'pagination'=>$pagination,'twigFileName'=>$twigFileName, 'filterForm'=>$filterForm->createView(), 'quoteForm'=>$multiVendorsform->createView(), 'form'=>$categoryForm->createView()));
    }

    function vendorProfileAction($userid,$catid) {
        $em = $this->getDoctrine()->getManager();
        $profile = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));
        if(empty($profile)) return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));

        $products = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorproductsrel')->findBy(array('vendorid'=>$userid));

        $logoObject = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorlogorel')->findOneBy(array('vendorid'=>$userid));

        $query = $em->createQueryBuilder()
                ->select('c.city')
                ->from('BBidsBBidsHomeBundle:Vendorcityrel', 'vc')
                ->innerJoin('BBidsBBidsHomeBundle:City', 'c', 'WITH', 'vc.city = c.id')
                ->where('vc.vendorid = :vendorid')
                ->setParameter('vendorid', $userid);

        $cities = $query->getQuery()->getArrayResult();

        $certifications = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcertificationrel')->findBy(array('vendorid'=>$userid));
        if(isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            $type = $_GET['type'];
            if($sort == 'date')
                $sortArray = array('created'=>$type);
            else
                $sortArray = array('rating'=>$type);
            $reviews = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Reviews')->findBy(array('vendorid'=>$userid),$sortArray);
        }
        else
            $reviews = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Reviews')->findBy(array('vendorid'=>$userid));

        $noOfReviews = count($reviews);

        $albums = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendoralbumrel')->findBy(array('vendorid'=>$userid));
        $photoArray = array();
        foreach ($albums as $album) {
            $photoArray[] = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Albumphotorel')->findBy(array('albumid'=>$album->getAlbumid()));
        }

        // echo '<pre/>';print_r($logoObject);exit;
        $query = $em->createQueryBuilder()
            ->select('c.category, c.id')
            ->from('BBidsBBidsHomeBundle:Categories','c')
            ->innerJoin('BBidsBBidsHomeBundle:Vendorcategoriesrel','vc', 'with', 'c.id=vc.categoryid')
            ->add('where', 'vc.vendorid = :userid')
            ->setParameter('userid',$userid);

        $categoryArray = $query->getQuery()->getArrayResult();

        $services = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorservices')->findBy(array('vendorid'=>$userid,'status'=>1));

        $expLicense = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorlicenserel')->findOneBy(array('vendorid'=>$userid));

        $finalRating = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Ratings')->findOneBy(array('vendorid'=>$userid));
        $form = $this->createForm(new DirectVendorEnquiryForm($this->getDoctrine()->getManager()),array('vendorid'=>$userid));

        $session = $this->container->get('session');
        $enqid = 0;
        $war = 0;
        if(!$session->has('uid')){
            $war = 2;
        }
        else{
            $sessionuid = $session->get('uid');

            if($userid==$sessionuid){
                $war = 3;
            }
            else{
                $query = $em->createQueryBuilder()
                    ->select('e.id, e.authorid, ve.enquiryid, ve.vendorid, ve.acceptstatus')
                    ->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                    ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 've', 'with', 'e.id=ve.enquiryid')
                    ->where('ve.acceptstatus=1 and e.authorid = :authorid and ve.vendorid = :vendorid')
                    ->setParameter('authorid', $sessionuid)
                    ->setParameter('vendorid', $userid);

                $enquiries = $query->getQuery()->getArrayResult();


                $count = count($enquiries);

                $enqid = 0;
                for($i = 0; $i<$count; $i++){
                    $enquiryid = $enquiries[$i]['id'];

                    $vendorrelArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Reviews')->findBy(array('enquiryid'=>$enquiryid, 'authorid'=>$sessionuid, 'vendorid'=>$userid));

                    if(empty($vendorrelArray)){
                        $enqid = $enquiryid;
                        $war = 1;
                    }
                }
            }
        }

        $renderTwig = $this->render('BBidsBBidsHomeBundle:Home:vendor_profile.html.twig',array('catid'=>$catid, 'profile'=>$profile, 'categories'=>$categoryArray, 'services'=>$services, 'albums'=>$albums, 'products'=>$products, 'cities'=>$cities, 'certifications'=>$certifications, 'reviews'=>$reviews, 'expLicense'=>$expLicense, 'war'=>$war, 'vendorid'=>$userid, 'finalRating'=>$finalRating, 'noOfReviews'=>$noOfReviews, 'photos'=>$photoArray, 'logoObject'=>$logoObject,'enqForm'=>$form->createView()));

        return $renderTwig;
    }

    function createLinks( $links, $limit, $page, $total) {
        if ( $limit == 'all' ) {
            return '';
        }

        $last       = ceil( $total / $limit );

        $start      = ( ( $page - $links ) > 0 ) ? $page - $links : 1;
        $end        = ( ( $page + $links ) < $last ) ? $page + $links : $last;

        $html       = '<ul>';

        $class      = ( $page == 1 ) ? "disabled" : "";
		if($page==1){
			$html       .= '<li class="disabled"><a href="#">Prev</a></li>';
		}
		else{
        $html       .= '<li class="' . $class . '"><a href="?limit=' . $limit . '&page=' . ( $page - 1 ) . '">Prev</a></li>';
        }
        if ( $start > 1 ) {
            $html   .= '<li><a href="?limit=' . $limit . '&page=1">1</a></li>';
            $html   .= '<li><a href="">.....</a></li>';
        }

        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $page == $i ) ? "actve" : "";
            $html   .= '<li><a class="' . $class . '" href="?limit=' . $limit . '&page=' . $i . '">' . $i . '</a></li>';
        }

        if ( $end < $last ) {
            $html   .= '<li><a href="">.....</a></li>';
            $html   .= '<li><a href="?limit=' . $limit . '&page=' . $last . '">' . $last . '</a></li>';
        }

        $class      = ( $page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="?limit=' . $limit . '&page=' . ( $page + 1 ) . '">Next</a></li>';

        $html       .= '</ul>';

        return $html;
    }

    function storeReviewVendorsAction(Request $request) //Ajax function to update the categories
    {
        // echo '<pre/>';print_r($_POST);exit;
        $em = $this->getDoctrine()->getManager();
        if($request->isMethod('POST')){
            $postData = $this->get('request')->request->all();
            $vendorid = $postData['boxValue'];
            $reqType  = $postData['reqType'];
            $categoryid  = $postData['catid'];

            $session = $this->container->get('session');
            $session->start();
            $quoteID  = $session->getId();
            // echo $quoteID;exit;
            $account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
            $bizname = $account->getBizname();
            if($reqType == 'add') {
                $sessionQuotes    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findOneBy(array('vendorid'=>$vendorid,'quoteid'=>$quoteID));
                if(!empty($sessionQuotes)) {

                    $sessionQuotes->setStatus(1);

                    $em->persist($sessionQuotes);
                    $em->flush();
                } else {
                    $requestQuotes = new Sessionquotes();
                    $requestQuotes->setQuoteid($quoteID);
                    $requestQuotes->setVendorid($vendorid);
                    $requestQuotes->setCategoryid($categoryid);
                    $requestQuotes->setVname($bizname);
                    $requestQuotes->setStatus(1);
                    $requestQuotes->setCreated(new \Datetime());

                    $em->persist($requestQuotes);
                    $em->flush();
                }
                return new response($bizname);
            } else {
                $sessionQuotes    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findOneBy(array('vendorid'=>$vendorid,'quoteid'=>$quoteID));
                if(!empty($sessionQuotes)) {
                    $sessionQuotes->setStatus(0);
                    $em->persist($sessionQuotes);
                    $em->flush();
                }
                return new response($bizname);
            }

        } else {
            return new response('Only authentic data is accepted.');
        }
    }

	public function reviewformpostAction($offset,$cid,$city,Request $request)
	{

        $params = $this->getRequest()->request->all();
        if(isset($params['form']['category'])){
			$categoryid = $params['form']['category'];
			if(isset($params['form']['city']))
				$city = $params['form']['city'];
			else
				$city = $city;
        }
        else{
            $categoryid = $cid;
            $city       = $city;
        }

        $categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));

		foreach($categoryArray as $c){
			$category = $c->getCategory();
		}

		$ratings = new Ratings();

		$form = $this->createFormBuilder($ratings)
			->add('filter','choice', array('choices'=>array('1'=>'Ratings - Highest Rated ', '2'=>'Ratings - Most Reviewed', '3'=>'Alphabetical - A-Z', '4'=>'Alphabetical - Z-A'), 'mapped'=>false))
			->getForm();

        $start = (($offset * 10) - 10);

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
			->select('a.bizname, a.userid, a.description, a.address, a.smsphone, r.rating, count(re.id)')
			->from('BBidsBBidsHomeBundle:Account', 'a')
			->innerJoin('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'c', 'with', 'a.userid=c.vendorid')
			->innerJoin('BBidsBBidsHomeBundle:Ratings', 'r', 'with', 'a.userid=r.vendorid')
			->innerJoin('BBidsBBidsHomeBundle:Reviews', 're', 'with' ,'a.userid=re.vendorid')
			->add('where','c.categoryid = :categoryid')
			->andWhere('re.modstatus = 1')
			->add('groupBy', 'a.bizname')
			->setParameter('categoryid', $categoryid)
            ->setFirstResult($start)
            ->setMaxResults(10);

		$vendors = $query->getQuery()->getResult();

        $from = $start+1;
        $to   = $start + 10;

        $countQuery = $em->createQueryBuilder()
            ->select('count(re.id)')
            ->from('BBidsBBidsHomeBundle:Account', 'a')
            ->innerJoin('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'c', 'with', 'a.userid=c.vendorid')
            ->innerJoin('BBidsBBidsHomeBundle:Ratings', 'r', 'with', 'a.userid=r.vendorid')
            ->innerJoin('BBidsBBidsHomeBundle:Reviews', 're', 'with' ,'a.userid=re.vendorid')
            ->add('where','c.categoryid = :categoryid')
            ->andWhere('re.modstatus = 1')
            ->add('groupBy', 'a.bizname')
            ->setParameter('categoryid', $categoryid);

        $countResult = $countQuery->getQuery()->getResult();
        $count = count($countResult);

        if($to > $count)
            $to = $count;


	//	print_r($vendors);

		return $this->render('BBidsBBidsHomeBundle:Home:node5.html.twig', array('category'=>$category, 'city'=>$city, 'form'=>$form->createView(), 'vendors'=>$vendors,'categoryid'=>$categoryid, 'from'=>$from, 'to'=>$to, 'count'=>$count));
	}



    public function node4Action(Request $request)
    {
        $request  = Request::createFromGlobals();
        $category = $request->query->get('category');
        $category = urldecode($category);
        // echo $category;exit;
        $cid = 14;
        if(isset($category)){
            $categoryArray1 = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('category'=>$category,'status'=>1));
            // echo '<pre/>';print_r($categoryArray1);exit;
            foreach($categoryArray1 as $c1)
            {
                $cid = $c1->getId();
            }
        }
        if($category != 'all'){

            $cat = new Categories();

            $categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

            $catArray = array();

            foreach($categoryArray as $c){
                $catid = $c->getId();
                $catname = $c->getCategory();
                $catArray[$catid] = $catname;
                if($category == $catname) $cid = $catid;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            // echo $catid;exit;
            $formreview = $this->createFormBuilder($cat)
                ->setAction($this->generateUrl('b_bids_b_bids_reviewform_post',array('catid'=>$cid)))

                ->add('category','choice', array('choices'=>$catArray, 'data'=> $cid, 'empty_value'=>'Select a Category', 'empty_data'=>null, 'label'=>'Select a category '))
                //->add('city','choice', array('choices'=>$cityArray, 'empty_value'=>'Select a City', 'empty_data'=>null, 'label'=>'Select a city', 'mapped'=>false))
                ->add('submit', 'submit', array('attr'=>array('class'=>'brouse_btn')))
                ->getForm();


            $formjob = $this->createFormBuilder($cat)
                ->setAction($this->generateUrl('b_bids_b_bids_jobform_post'))
                ->add('category','choice', array('choices'=>$catArray, 'data'=> $catid, 'empty_value'=>'Select a Category', 'empty_data'=>null, 'label'=>'Select a category '))
                ->add('city', 'choice', array('choices'=>$cityArray, 'empty_value'=>'select a city', 'empty_data'=>null, 'mapped'=>false))
                ->add('submitjob', 'submit', array('attr'=>array('class'=>'btn btn-success btn-block')))
                ->getForm();


            $segmentsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>111111, 'parentid'=>111111,'status'=>1));
            $segArray = array();

            foreach($segmentsArray as $s){
                $catArray = array();

                $segname = $s->getCategory();
                $segid = $s->getId();

                array_push($segArray, $segname);

                $cArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>$segid,'status'=>1));

                foreach($cArray as $c){
                    $cname = $c->getCategory();

                    array_push($catArray, $cname);
                }

                $segArray[$segname] = $catArray;
            }

            $em = $this->getDoctrine()->getManager();

            $query = $em->createQueryBuilder()
                ->select('count(c.id)')
                ->from('BBidsBBidsHomeBundle:Categories', 'c')
                ->where('c.parentid=0')
                ->andWhere('c.status=1');

            $catCount = $query->getQuery()->getSingleScalarResult();

            return $this->render('BBidsBBidsHomeBundle:Home:node4.html.twig', array('formreview'=>$formreview->createView(), 'formjob'=>$formjob->createView(), 'segments'=>$segArray, 'catcount'=>$catCount, 'categoryArray'=>$categoryArray, 'chosenCategory'=>$category));
        }
        else{

        }
    }

	public function node3Action()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:node3.html.twig',array('title'=>'How BusinessBid Works for Customers'));
	}

	public function acceptenquiryAction($enquiryid, $category)
	{
		$session = $this->container->get('session');

		$uid = $session->get('uid');

		$enquiryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Enquiry')->findOneBy(array('id'=>$enquiryid));
		$location     = $enquiryArray->getLocation();
		$enquiryauth  = $enquiryArray->getAuthorid();
        $expStatus = $enquiryArray->getExpstatus();

        if($expStatus == 2 OR $expStatus == 1) {
            $session->getFlashBag()->add('error', "You are unable to accept this Job Request $enquiryid as 3 vendors have already accepted this job. Please be quick on your next job response.");
            return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
        }
		$enquiryauthArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$enquiryauth));
        if(empty($enquiryauthArray)) {
            $session->getFlashBag()->add('error', "You are unable to accept this Job Request $enquiryid as the user is deleted from the system.");
            return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
        }
		$authname      	  = $enquiryauthArray->getContactname();
		$authmobile       = $enquiryauthArray->getSmsphone();

		$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid));
		$name      	  = $accountArray->getContactname();

        $userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$uid));
        $email     = $userArray->getEmail();
        // echo $email;exit;

		$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('category'=>$category,'status'=>1));

		foreach($categoryArray as $c){
			$categoryid = $c->getId();
		}

		$enquiryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('enquiryid'=>$enquiryid, 'vendorid'=>$uid));
		foreach($enquiryArray as $enquiry){
			$vdid = $enquiry->getId();
		}


		$em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
                ->select('count(ve.id)')
                ->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 've')
                ->add('where', 've.enquiryid = :enquiryid')
                ->andWhere('ve.acceptstatus =1')
                ->setParameter('enquiryid', $enquiryid);

        $enquiryacceptcount = $query->getQuery()->getSingleScalarResult();


		$vendorcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$uid, 'categoryid'=>$categoryid,'status'=>1,  'flag'=>2));
        $subcount = 1; // Note Logic Changed... Only one lead deducted for one enquiry
		if(!empty($vendorcatArray))
		{

			foreach($vendorcatArray as $vendorcat){
				$id = $vendorcat->getId();
				$leadpack = $vendorcat->getLeadpack();

				if($leadpack >= $subcount){
					$nowpack = $leadpack - $subcount;

					if($enquiryacceptcount < 3)
					{

				        $em = $this->getDoctrine()->getManager();

		                $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);
						$lead->setLeadpack($nowpack);
		                $lead->setDeductedlead(0);
		                $lead->setDeductedfreelead($subcount);
						$em->flush();

						$vendorflag = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);
						$vflag = $vendorflag->getFlag();

						$vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vdid);

	                	$vendor->setAcceptstatus(1);
	                	$vendor->setFlag($vflag);

            			$em->flush();

						//send email
                        $subject = 'BusinessBid Job Acceptance Confirmation';
                        $body    = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig', array('enquiryid'=>$enquiryid, 'category'=>$category,'subcount'=>$subcount,'name'=>$name,'location'=>$location,'authname'=>$authname,'authmobile'=>$authmobile));

                        $this->sendEmail($email,$subject,$body,$uid);
					}

					else {
				        $session->getFlashBag()->add('error', '<p>You are unable to receive this Job Request as 3 vendors have already accepted this job.</p><p>Please be quick on your next job response </p>');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
					}
                    if($nowpack == 0){
                        $em = $this->getDoctrine()->getManager();

                        $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);

                        $lead->setStatus(0);
                        $lead->setDeductedlead(0);
                        $lead->setDeductedfreelead(0);

                        $em->flush();
                    }
					return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
				}
				else {
					//echo 'dsfdfs';exit;
					$vcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$uid, 'categoryid'=>$categoryid, 'flag'=>1));

	                if(!empty($vcatArray)) {
                        foreach($vcatArray as $vendorcat) {
                            $nid = $vendorcat->getId();
        			        $nleadpack = $vendorcat->getLeadpack();

							$newlead = $leadpack + $nleadpack;

							if($newlead >= $subcount){

								//$nowpack = $nleadpack - $leadpack;
								$nowpack = $newlead - $subcount;
								$deductedbuy=$subcount-$leadpack;
								if($enquiryacceptcount < 3)
								{

			                        $em = $this->getDoctrine()->getManager();

                                    $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);

        			                $lead->setLeadpack(0);
									$lead->setStatus(0);
									//$lead->setDeductedlead($deductedbuy);
									//$lead->setDeductedfreelead($leadpack);

                                    $em->flush();

								    $em = $this->getDoctrine()->getManager();

                                    $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);

                                    $lead->setLeadpack($nowpack);
                                    $lead->setStatus(1);
                                    $lead->setDeductedlead($deductedbuy);
                                    $lead->setDeductedfreelead($leadpack);

                                    $em->flush();


                                    $vendorflag = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);
									$vflag = $vendorflag->getFlag();


                                    $vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vdid);
                                    $vendor->setAcceptstatus(1);
                                    $vendor->setFlag(1);

                                    $em->flush();

                                    // Send email
                                    $subject = 'BusinessBid Job Acceptance confirmation';

                                    $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig', array('enquiryid'=>$enquiryid, 'category'=>$category,'subcount'=>$subcount,'name'=>$name,'location'=>$location,'authname'=>$authname,'authmobile'=>$authmobile));

                                    $this->sendEmail($email,$subject,$body,$uid);
                                }
                                else {
									$session->getFlashBag()->add('error', '<p>You are unable to receive this Job Request as 3 vendors have already accepted this job.</p><p>Please be quick on your next job response </p>');
                                        return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
                                }

                            }
                            else {
								$session->getFlashBag()->add('error', 'You do not have sufficient lead credit to accept this enquiry. Please purchase more leads');
								return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
							}
					   }

					   return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
				    }
                    else {
						$session->getFlashBag()->add('error', 'You do not have sufficient lead credit to accept this enquiry. Please purchase more leads');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
					}
				}
			}
		}
		else {
            $vendorcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$uid, 'categoryid'=>$categoryid, 'flag'=>1));

            if(!empty($vendorcatArray)){
                foreach($vendorcatArray as $vendorcat) {
                    $nid       = $vendorcat->getId();
                    $nleadpack = $vendorcat->getLeadpack();
			        if($nleadpack >= $subcount) {
                 		$nowpack = $nleadpack - $subcount;
                 		$deductedbuy =$nleadpack -$nowpack;

				        $em = $this->getDoctrine()->getManager();
						if($enquiryacceptcount < 3)
						{
                            $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);
                            $lead->setLeadpack($nowpack);
                            $lead->setDeductedlead($deductedbuy);
                            $lead->setDeductedfreelead(0);
                            $em->flush();

                            $vendorflag = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);
                            $vflag = $vendorflag->getFlag();
                            //echo 'third';echo $vflag;exit;

                            $vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vdid);

                            $vendor->setAcceptstatus(1);
                            $vendor->setFlag($vflag);

                            $em->flush();

                            $subject = 'BusinessBid Job Acceptance Confirmation';

                            $body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig', array('enquiryid'=>$enquiryid, 'category'=>$category,'subcount'=>$subcount,'name'=>$name,'location'=>$location,'authname'=>$authname,'authmobile'=>$authmobile));

                            $this->sendEmail($email,$subject,$body,$uid);

						}
                        else {
                            $session->getFlashBag()->add('error', '<p>You are unable to receive this Job Request as 3 vendors have already accepted this job.</p><p>Please be quick on your next job response </p>');
                            return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
				        }

                        if($nowpack == 0){
                            $em = $this->getDoctrine()->getManager();

                            $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);

                            $lead->setStatus(0);

                            $em->flush();
                        }

				}
				else {
					$session->getFlashBag()->add('error', 'You do not have sufficient lead credit to accept this enquiry. Please purchase more leads');
                    return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
				}
			}
		}
		else {
			$session->getFlashBag()->add('error', 'You do not have sufficient lead credit to accept this enquiry. Please purchase more leads');
                                        return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
		}
		//$msg = '.$subcount leads were credited from your account';
		$session->getFlashBag()->add('success', 'You have successfully accepted the customer lead. Please initiate contact with customer.');

		return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_enquiries'));
	}
	}

	public function indexoldAction(Request $request)
	{

		$session = $this->container->get('session');

		$em = $this->getDoctrine()->getManager();
		$connection = $em->getConnection();
		$qb = $em->createQueryBuilder();
    		$qb->select('count(enquiry.id)');
    		$qb->from('BBidsBBidsHomeBundle:Enquiry','enquiry');
    		$count = $qb->getQuery()->getSingleScalarResult();


    		$statement = $connection->prepare("SELECT count(id) FROM bbids_enquiry WHERE date(created)=CURDATE()");
				$count2 = $statement->execute();
				//$counte = $statement->fetchALL();
			//echo $count2;exit;


		$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

		$catArray = array();

		foreach($categoryArray as $c){
			$catid = $c->getId();
			$catname = $c->getCategory();
			$catArray[$catid] = $catname;
		}

		$cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

		$cityArray = array();

		foreach($cities as $c){
			$city = $c->getCity();
			$cityArray[$city] = htmlspecialchars($city);
		}

		$categories = new Categories();


		$form = $this->createFormBuilder($categories)
            ->setMethod('POST')
			->add('category', 'choice', array('label'=>'What job do you need done', 'expanded'=>FALSE, 'attr' => array('class'=>'form-control'), 'choices'=>$catArray, 'empty_value'=>'Select Services Required', 'empty_data'=>null))
			->add('city', 'choice', array('label'=>'where do you need the job done', 'expanded'=>FALSE, 'attr' => array('class'=>'form-control'), 'choices'=>$cityArray, 'empty_value'=>'Select City', 'empty_data'=>null, 'mapped'=>FALSE ))
			->add('submit', 'submit', array('label'=>'Get Quotes Now', 'attr' => array('class'=>'btn btn-success pull-right ban-get-quote')))
			->getForm();



		$query = $em->createQueryBuilder()
			->select('a.category, a.created, a.type, ac.contactname, a.city, a.message')
			->from('BBidsBBidsHomeBundle:Activity', 'a')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'ac', 'WITH', 'a.authorid=ac.userid')
			->add('where', 'a.created >= CURRENT_DATE()')
			->add('orderBy', 'a.id DESC');


		$activities = $query->getQuery()->getResult();

		$count=count($activities);

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				$category = $form['category']->getData();
                $category = urlencode($category);
				$city = $form['city']->getData();
				return $this->redirect("/app.php/search?category=$category");
			}
			return $this->render('BBidsBBidsHomeBundle:Home:index.html.twig', array('enquiries'=>$count, 'activities'=>$activities, 'form'=>$form->createView(),'title'=>'BusinessBid Main Home Page'));
		}
		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('e.subj, r.created, r.review, r.id')
			->from('BBidsBBidsHomeBundle:Reviews', 'r')
			->innerJoin('BBidsBBidsHomeBundle:Enquiry','e', 'with','e.id = r.enquiryid')
			->where('r.modstatus = 1')
			->add('orderBy','r.id DESC')
			->setMaxResults(1);

			$review = $query->getQuery()->getResult();


	 	return $this->render('BBidsBBidsHomeBundle:Home:index.html.twig', array('enquiries'=>$count, 'review'=>$review, 'title'=>'BusinessBid Main Home Page','activities'=>$activities, 'form'=>$form->createView()));
	}

    function indexAction(Request $request) {
		
				if($_SERVER['HTTP_HOST']=='businessbid.ae' || $_SERVER['REQUEST_URI']=='/index.php'):
					header("HTTP/1.1 301 Moved Permanently");
					header('Location: http://www.businessbid.ae');
					exit;
				endif;
        // $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1),array('menuOrder'=> 'ASC'),14);
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('BBidsBBidsHomeBundle:Categories', 'c')
                        ->add('where','c.parentid = 0')
                        ->andWhere('c.status = 1')
                        ->andWhere('c.menuOrder IS NOT NULL')
                        ->add('orderBy', 'c.menuOrder ASC')
                        ->setMaxResults(14);
        $categoriesArray = $query->getQuery()->getResult();

        $categoryList = $em->createQueryBuilder()
                        ->select('c')
                        ->from('BBidsBBidsHomeBundle:Categories', 'c')
                        ->add('where','c.parentid = 0')
                        ->andWhere('c.status = 1')
                        ->add('orderBy', 'c.category ASC');
        $categoryListArray = $categoryList->getQuery()->getResult();

        return $this->render('BBidsBBidsHomeBundle:Home:index.html.twig', array('categories'=>$categoriesArray,'categoriesList'=>$categoryListArray));
    }

	public function searchAction($vendorid)
	{

		$request = Request::createFromGlobals();

		$category =  $request->query->get('category');

		$city='';
		if(isset($_REQUEST['category']))
		{
			$category =  $request->query->get('category');
		}
		else if(isset($_REQUEST['city']))
		{
			$city =  $request->query->get('city');
		}
		$category= urldecode($category);

		if($vendorid != 1){
			$session = $this->container->get('session');

			$session->set('maptovendorid',$vendorid);
		}


		if(isset($category) && is_numeric($category))
		{
			$categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$category,'status'=>1));

			foreach($categoryRow as $category){
				$categoryid = $category->getId();
				$category = $category->getCategory();
			}

			if(!isset($categoryid)){
				$categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Keyword')->findBy(array('keyword'=>$category));
				foreach($categoryRow as $category){
					 $categoryid = $category->getParentid();
				}
			 }
			 else if(!isset($category)){
				$categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Keyword')->findBy(array('keyword'=>$category));
				foreach($categoryRow as $category){
					 $categoryid = $category->getParentid();
				}
			 }

		}
		else
		{
			$categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('category'=>trim($category),'status'=>1));
			// echo "<pre/>";print_r($categoryRow);exit;
            foreach($categoryRow as $category){
				$categoryid = ($category->getParentid() == 0) ? $category->getId() : $category->getParentid();
			}

			if(!isset($categoryid)){
				$categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Keyword')->findBy(array('keyword'=>$category));
				foreach($categoryRow as $category){
					 $categoryid = $category->getParentid();
				}
			 }
		}


	     if(!isset($categoryid)){
			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));

		}
		$session = $this->container->get('session');

		if($session->has('uid')){
			return $this->redirect($this->generateUrl('b_bids_b_bids_post_by_search', array('categoryid'=>$categoryid)));
		}
		else{
			return $this->redirect($this->generateUrl('b_bids_b_bids_post_by_search_inline', array('categoryid'=>$categoryid)));
		}
	}

	public function searchResourceAction($vendorid)
	{

		$request = Request::createFromGlobals();

		$category =  $request->query->get('category');

		$city='';
		if(isset($_REQUEST['city']))
		{
			$city =  $request->query->get('city');
		}
		$category= urldecode($category);
		//echo $category;exit;
		 $cry2=str_replace("–","-",$category);
		  //$cry2=str_replace("/","-",$category);


		if($vendorid != 1){
			$session = $this->container->get('session');

			$session->set('maptovendorid',$vendorid);
		}

		 $categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('category'=>trim($cry2),'status'=>1));
                foreach($categoryRow as $category){
                      $categoryid = $category->getId();
                }




             if(!isset($categoryid)){
			$categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Keyword')->findBy(array('keyword'=>$category));
			foreach($categoryRow as $category){
				 $categoryid = $category->getParentid();
			}

	     }

	     if(!isset($categoryid)){
			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));

		}
		$session = $this->container->get('session');

		if($session->has('uid')){
			return $this->redirect($this->generateUrl('b_bids_b_bids_post_by_search', array('categoryid'=>$categoryid)));
		}
		else{
			return $this->redirect($this->generateUrl('b_bids_b_bids_post_by_search_inline', array('categoryid'=>$categoryid)));
		}
	}

    public function postbysearchinlineAction(Request $request, $categoryid)
    {
        $session = $this->container->get('session');
        $request = Request::createFromGlobals();
        $selectedCity = $parentid = '';

        if(isset($_REQUEST['city']))
        {
            $selectedCity =  $request->query->get('city');
        }
        $title = 'Businessbids';

        $enquiry = new Enquiry();

        $categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));
        foreach($categoryRow as $category){
            $parentid = $category->getParentid();
        }

        if($parentid == 0){
            $parentid = $categoryid;
        }

        if(empty($parentid)) {
            $session->getFlashBag()->add('error',"Coming Soon - We are currently working on this service. We won't be too long and call us on 04 42 13 777 if you need assistance");
            return $this->render('::404error.html.twig');
        }

        $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$parentid));

        foreach($categorynamelist as $category){
        $categoryname = $category->getCategory();
        }

        if(empty($categorynamelist))
        {
            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));
            foreach($categorynamelist as $category){
            $categoryname = $category->getCategory();
            }

        }

        $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$parentid));

                $subcategory = array();

                foreach($subcategoryList as $sublist){
                        $subid = $sublist->getId();
                        $subcat = $sublist->getCategory();
                        $subcategory[$subid] = $subcat;
                }

        $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

                $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }

        $session->start();
        $quoteID  = $session->getId();

        $sessionQuotes    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findBy(array('quoteid'=>$quoteID,'categoryid'=>$categoryid));

        $selVendorList  = array();
        foreach ($sessionQuotes as $value) {
            $selVendorList[$value->getVendorid()]['name'] = $value->getVname();
            $selVendorList[$value->getVendorid()]['status'] = $value->getStatus();
            $selVendorList[$value->getVendorid()]['id'] = $value->getVendorid();
        }

        $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));
        // echo '<pre/>';print_r($formOptions);
        if(!empty($formOptions)) {
            $categoryname = str_replace(" and ", " & ", $categoryname);
            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>FALSE,'reqType'=>'inline'));
                $twigFileName =$formOptions->getTwigName();
            }
        } else {
            $session->getFlashBag()->add('error',"Coming Soon - We are currently working on this service. We won't be too long and call us on 04 42 13 777 if you need assistance");
            return $this->render('::404error.html.twig');
        }
        $form->handleRequest($request);

        if($request->isMethod('POST')) {
            if($form->isValid()) {

                $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();

                $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));

                if(!empty($account)) {
                    $session->getFlashbag()->add('error','Mobile Number already exists. Please try a different mobile number');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }

                $location       = $form['location']->getData();
                $mobileother    = $form['mobileother']->getData();
                $homephoneother = $form['homephoneother']->getData();
                $locationother  = $form['locationother']->getData();
                $subcategoryids = $form['subcategory']->getData();



                $subcount = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }
                $subj        = $form['subj']->getData();
                $description = $form['description']->getData();
                $city        = $form['city']->getData();
                $hire        = $form['hire']->getData();

                if($subj == "") {
                    $session->getFlashBag()->add('error', 'Subject field cannot be blank');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }

                if($description == "") {
                    $session->getFlashBag()->add('error', 'Description field cannot be blank');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request Timeline  field cannot be blank');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }

                $email = $form['email']->getData();

                if($email == "") {
                    $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }

                $userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBY(array('email'=>$email));

                foreach($userArray as $u) {
                    $id = $u->getId();
                    $userstatus = $u->getStatus();
                }

                /* if(isset($id)){
                    return $this->redirect($this->generateUrl('b_bids_b_bids_user_authenticate', array('email'=>$email)));
                } */

                $smscode     = $form['mobilecode']->getData();
                $smsphone    = $form['mobile']->getData();
                $home        = $form['homecode']->getData();
                $phone       = $form['homephone']->getData();
                $contactname = $form['contactname']->getData();
                if(!empty($home)) {
                    $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                } else {
                    $homephone = "";
                }
                $session = $this->container->get('session');

                if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                    $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                }

                if($smsphone) {
                    if(strlen($smsphone) != 7) {
                        $session->getFlashBag()->add('error', 'Mobile Number should be exactly 7 digits');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                    }
                }
                if($smsphone) {
                    if(!is_numeric($smsphone)){
                        $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                    }
                }
                if($home) {
                    if(!empty($phone) && !empty($home) &&(!is_numeric($home) || !is_numeric($phone))) {
                        $session->getFlashBag()->add('error', 'Home Phone should contain only numbers');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                    }
                }
                if($phone) {
                    if(strlen($phone) != 7) {
                        $session->getFlashBag()->add('error', 'Phone Number should be exactly 7 digits');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
                    }
                }

                if(isset($id)) {
                    $userid = $id;
                    $mobilecode = rand(100000,999999);

                    $em = $this->getDoctrine()->getManager();

                    $user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($id);

                    $user->setMobilecode($mobilecode);

                    $em->flush();
                } else {
                    $user =  new User();

                    $created = new \DateTime();
                    $userhash = $this->generateHash(16);
                    $mobilecode = rand(100000,999999);

                    $user->setEmail($email);
                    $user->setCreated($created);
                    $user->setUpdated($created);
                    $user->setUserhash($userhash);
                    $user->setStatus(0);
                    $user->setMobilecode($mobilecode);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();

                    $userid = $user->getId();
                    if($form['mobilecode']->getData() == 000) {
                        $smsphone2 = $mobileother;
                    } else {
                      $smsphone2 = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                    }

                    if($home==00) {
                        $homephone=$homephoneother;
                    }
                    $account = new Account();

                    $account->setProfileid(2);
                    $account->setUserid($userid);
                    $account->setSmsphone($smsphone2);
                    $account->setHomephone($homephone);
                    $account->setContactname($contactname);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($account);
                    $em->flush();
                }

                $categoryid     = $form['category']->getData();
                $subcategoryids = $form['subcategory']->getData();
                $subj           = $form['subj']->getData();
                $description    = $form['description']->getData();
                $city           = $form['city']->getData();
                if($city=='other') {
                    $location = $locationother;
                }
                $created = new \DateTime();

                $enquiry = new Enquiry();
                $enquiry->setSubj($subj);
                $enquiry->setDescription($description);
                $enquiry->setCity($city);
                $enquiry->setCategory($categoryid);
                $enquiry->setAuthorid($userid);
                $enquiry->setCreated($created);
                $enquiry->setUpdated($created);
                $enquiry->setStatus(0);
                $enquiry->setLocation($location);

                $em = $this->getDoctrine()->getManager();
                $em->persist($enquiry);
                $em->flush();

                $enquiryid = $enquiry->getId();

                $session = $this->container->get('session');


                $eSubCategoryString = '';

                $count = count($subcategoryids);

                for($i=0; $i<$count; $i++) {
                    $subCatID = $subcategoryids[$i];
                    $enquirysubrel = new EnquirySubcategoryRel();
                    $enquirysubrel->setEnquiryid($enquiryid);
                    $enquirysubrel->setSubcategoryid($subcategoryids[$i]);

                    $eSubCategoryString .= $subcategory[$subCatID].", ";

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($enquirysubrel);
                    $em->flush();
                }

                if($count > 1) {
                    $multipleCatText = 'Job with multiple leads credited.';
                    if(strlen($eSubCategoryString) > 50){
                        $eSubCategoryString = substr($eSubCategoryString,0,50).'...';
                    }
                } else {
                    $multipleCatText = ' ';
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                if($session->has('maptovendorid')) {
                    $vendorid    = $session->get('maptovendorid');
                    $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$$vendorid));

                    foreach($vendorArray as $vendor){
                        $status = $vendor->getStatus();
                        if($status == 1) {
                            $vendorenquiryrel = new Vendorenquiryrel();

                            $vendorenquiryrel->setVendorid($vendorid);
                            $vendorenquiryrel->setEnquiryid($enquiryid);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($vendorenquiryrel);
                            $em->flush();

                            // Send sms to all vendors
                             $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));

                            // Send sms to mobile number provided.
                            $smsphone      = $accountArray->getSmsphone();
                            $smsphoneArray = explode("-",$smsphone);
                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                            $vendorSMSText = "Job $enquiryid in $city for $eSubCategoryString - $description. $multipleCatText Reply 'YES Job Number' for client details.";
                            $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                            /*End of sms to vendors*/
                        }
                    }
                } else {
                    $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('categoryid'=>$categoryid));
                    $enqcount = 0;
                    foreach($vendorArray as $vendor) {
                        $enqcount++;
                        $vendorid = $vendor->getVendorid();
                        $status = $vendor->getStatus();
                        $vendorleadpack = $vendor->getLeadpack();

                    //################# city mapping removed ###############################//
                        //$cityArray = array();

                        //$vendorquery = $em->createQueryBuilder()
                                    //->select('c.city')
                                    //->from('BBidsBBidsHomeBundle:City', 'c')
                                    //->innerJoin('BBidsBBidsHomeBundle:Vendorcityrel', 'vc', 'with', 'c.id = vc.city')
                                    //->where('vc.vendorid = :vendorid')
                                    //->setParameter('vendorid', $vendorid);

                        //$vendorcityArray = $vendorquery->getQuery()->getArrayResult();

                        //$vcount = count($vendorcityArray);

                        //for($i=0; $i<$vcount; $i++){
                            //array_push($cityArray, $vendorcityArray[$i]['city']);
                        //}

                        //if($status == 1 && $vendorleadpack !=0 & in_array($city, $cityAArray)) {

                    //################# city mapping removed ###############################//

                        if($status == 1 && $vendorleadpack !=0 ) {
                           $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                            if(empty($vendenqArray)){
                                 //if($enqcount == 1){
                                $vendorenquiryrel = new Vendorenquiryrel();

                                $vendorenquiryrel->setVendorid($vendorid);
                                $vendorenquiryrel->setEnquiryid($enquiryid);

                                $em = $this->getDoctrine()->getManager();
                                $em->persist($vendorenquiryrel);
                                $em->flush();

                                // Send sms to all vendors
                                 $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));

                                // Send sms to mobile number provided.
                                $smsphone      = $accountArray->getSmsphone();
                                $smsphoneArray = explode("-",$smsphone);
                                $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                                $vendorSMSText = "Job $enquiryid in $city for $eSubCategoryString - $description. $multipleCatText Reply 'YES Job Number' for client details.";
                                $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                                /*End of sms to vendors*/
                            }
                        }
                    }
                }

                /*Send sms customer for confirmation */

                $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));
                $customerName = 'User';
                foreach($userDetails as $n) {
                    $smsphone = $n->getSmsphone();
                    $customerName = $n->getContactname();
                }

                $smsphoneArray = explode("-",$smsphone);
                $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString & vendors will contact you shortly.";
                $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');
                /*End*/

                $categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));
                foreach($categoryArray as $c){
                    $cat = $c->getCategory();
                }

                $activity = new Activity();

                $activity->setMessage('New Enquiry');
                $activity->setCity($city);
                $activity->setCategory($cat);
                $activity->setAuthorid($userid);
                $activity->setCreated($created);
                $activity->setType(1);

                $em = $this->getDoctrine()->getManager();
                $em->persist($activity);
                $em->flush();

                //echo $userstatus;
                //exit;

                if(isset($id)) {
                    if($userstatus == 1) {
                        return $this->redirect($this->generateUrl('b_bids_b_bids_user_authenticate', array('email'=>$email, 'enquiryid'=>$enquiryid)));
                    }
                    if($userstatus == 0 || $userstatus == 2) {
                        return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verify', array('smscode'=>$mobilecode, 'userid'=>$userid, 'path'=>'inline','enquiryid'=>$enquiryid)));
                    }
                    if($userstatus == 3) {
                        $session->getFlashBag()->add('error', 'Your account has been blocked. Please contact the system administrator for further details.');
                        return $this->render('::error.html.twig');
                    }

                    if($userstatus == 0 || $userstatus == 2){
                        return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verify', array('smscode'=>$mobilecode, 'userid'=>$userid, 'path'=>'inline','enquiryid'=>$enquiryid)));
                    }
                } elseif($smscode==000) {
                    //echo 'international';exit;
                    $path = 'inline';
                    $userArray  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid));

                    foreach($userArray as $uses){
                        $smscode = $uses->getMobilecode();
                        $email = $uses->getEmail();
                        $status = $uses->getStatus();
                    }
                    $user = new User();

                    $userhash = $this->generateHash(16);
                    $em = $this->getDoctrine()->getManager();
                    $user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($userid);

                    $user->setMobilecode('');
                    $user->setStatus(4);
                    $user->setUserhash($userhash);
                    $em->flush();

                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));

                    foreach($accountArray as $a){
                        $profileid = $a->getProfileid();
                    }

                    if($path == 'inline'){
                        $em = $this->getDoctrine()->getManager();
                        $enquiry = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);

                        $enquiry->setStatus(1);

                        $em->flush();
                    }

                    $username = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));
                    //print_r($username);exit;
                    foreach($username as $n){
                            $name = $n->getContactname();
                    }

                    $em = $this->getDoctrine()->getManager();

                    $useremail = new Usertoemail();

                    $useremail->setFromuid(11);
                    $useremail->setTouid($userid);
                    $useremail->setFromemail('infosupport@businessbid.ae');
                    $useremail->setToemail($email);
                    $useremail->setEmailsubj('Welcome to the BusinessBid Network');
                    $useremail->setCreated(new \Datetime());
                    $useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activationemail.html.twig', array('userid'=>$userid, 'userhash'=>$userhash,'status'=>$status,'name'=>$name)));
                    $useremail->setEmailtype(1);
                    $useremail->setStatus(1);
                    $useremail->setReadStatus(0);

                    $em->persist($useremail);
                    $em->flush();

                    $url = 'https://api.sendgrid.com/';
                    $user = 'businessbid';
                    $pass = '!3zxih1x0';
                    $subject = 'Welcome to the BusinessBid Network';

                    $body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activationemail.html.twig', array('userid'=>$userid, 'userhash'=>$userhash,'status'=>$status,'name'=>$name));

                    $message = array(
                                'api_user'  => $user,
                                'api_key'   => $pass,
                                'to'        => $email,
                                'subject'   => $subject,
                                'html'      => $body,
                                'text'      => $body,
                                'from'      => 'support@businessbid.ae',
                            );


                    $request =  $url.'api/mail.send.json';

                    // Generate curl request
                    $sess = curl_init($request);
                    // Tell curl to use HTTP POST
                    curl_setopt ($sess, CURLOPT_POST, true);
                    // Tell curl that this is the body of the POST
                    curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
                    // Tell curl not to return headers, but do return the response
                    curl_setopt($sess, CURLOPT_HEADER,false);
                    curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

                    // obtain response
                    $response = curl_exec($sess);
                    curl_close($sess);

                    return $this->redirect($this->generateUrl('b_bids_b_bids_international_user__verified'));

                } else {
                    return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verify', array('smscode'=>$mobilecode, 'userid'=>$userid, 'path'=>'inline','enquiryid'=>$enquiryid)));
                }
            }
            else{
                return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title));
            }
        }


        /*return $this->render('BBidsBBidsHomeBundle:Home:postbysearchinline.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity));*/
        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>$selectedCity,'title'=>$title,'twigFileName'=>$twigFileName,'catid'=>$categoryid,'vList'=>$selVendorList));
    }

    public function postbysearchAction(Request $request, $categoryid)
    {
        $session = $this->container->get('session');
        if($session->has('uid')) {
            $enquiry    = new Enquiry();
            $categories = new Categories();

            $categoryRow = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));
            foreach($categoryRow as $category){
                $parentid = $category->getParentid();
            }

            if($parentid == 0){
                $parentid = $categoryid;
            }

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$parentid));

            foreach($categorynamelist as $category){
                $categoryname = $category->getCategory();
            }


            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$parentid));

            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid               = $sublist->getId();
                $subcat              = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }

            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }

            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));

            if(!empty($formOptions)) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>'','loginFlag'=>TRUE,'reqType'=>'inline'));
                $twigFileName = $formOptions->getTwigName();
            } else {
                $session->getFlashBag()->add('error',"Coming Soon - We are currently working on this service. We won't be too long and call us on 04 42 13 777 if you need assistance");
                return $this->render('::404error.html.twig');
            }

            $title = 'Businessbids';

            $session->start();
            $quoteID  = $session->getId();

            $sessionQuotes    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findBy(array('quoteid'=>$quoteID,'categoryid'=>$categoryid));

            $selVendorList  = array();
            foreach ($sessionQuotes as $value) {
                $selVendorList[$value->getVendorid()]['name'] = $value->getVname();
                $selVendorList[$value->getVendorid()]['status'] = $value->getStatus();
                $selVendorList[$value->getVendorid()]['id'] = $value->getVendorid();
            }

            $form->handleRequest($request);

            if($request->isMethod('POST')) {
                if($form->isValid()) {

                    $location = $form['location']->getData();
                    if($location == "") {
                        $session = $this->container->get('session');
                        $session->getFlashbag()->add('error', 'Location field cannot be left blank');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearch.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $subj = $form['subj']->getData();
                    if($subj == "") {
                        $session = $this->container->get('session');
                        $session->getFlashBag()->add('error', 'Subject field cannot be blank');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearch.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $subcategoryids  = $form['subcategory']->getData();
                    $subcount = count($subcategoryids);
                    if($subcount == 0) {
                        $session = $this->container->get('session');
                        $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                        return $this->render('BBidsBBidsHomeBundle:Home:postbysearch.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $session = $this->container->get('session');

                    $categoryid = $form['category']->getData();
                    //$subcategoryids  = $form['subcategory']->getData();
                    $subj = $form['subj']->getData();
                    $description = $form['description']->getData();
                    $city = $form['city']->getData();

                    $uid = $session->get('uid');
                    $created = new \DateTime();

                    $enquiry = new Enquiry();
                    $enquiry->setSubj($subj);
                    $enquiry->setDescription($description);
                    $enquiry->setCity($city);
                    $enquiry->setCategory($categoryid);
                    $enquiry->setAuthorid($uid);
                    $enquiry->setCreated($created);
                    $enquiry->setUpdated($created);
                    $enquiry->setStatus(1);
                    $enquiry->setLocation($location);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($enquiry);
                    $em->flush();

                    $enquiryid = $enquiry->getId();



                    $count = count($subcategoryids);
                    $session = $this->container->get('session');

                    $eSubCategoryString = '';
                    for($i=0; $i<$count; $i++){
                        $subCatID = $subcategoryids[$i];
                        $enquirysubrel = new EnquirySubcategoryRel();
                        $enquirysubrel->setEnquiryid($enquiryid);
                        $enquirysubrel->setSubcategoryid($subcategoryids[$i]);

                        $eSubCategoryString .= $subcategory[$subCatID].", ";

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($enquirysubrel);
                        $em->flush();
                    }

                     if($count > 1) {
                        $multipleCatText = '';
                        if(strlen($eSubCategoryString) > 50){
                            $eSubCategoryString = substr($eSubCategoryString,0,50).'...';
                        }
                    } else {
                        $multipleCatText    = 'Job with multiple leads credited.';
                        $subCatID           = $subcategoryids[0];
                        $eSubCategoryString = $subcategory[$subCatID];
                    }

                    if($session->has('maptovendorid')) {
                        $vendorid = $session->get('maptovendorid');
                        $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid));
                            //print_r($vendorArray);

                        $addedVendorArray = array();
                        foreach($vendorArray as $vendor) {
                            $status = $vendor->getStatus();
                            $flag = $vendor->getFlag();
                            $leads = $vendor->getLeadpack();
                            if($status == 1) {
                                $addedVendorArray[] = $vendorid;
                                $vendorenquiryrel = new Vendorenquiryrel();

                                $vendorenquiryrel->setVendorid($vendorid);
                                $vendorenquiryrel->setEnquiryid($enquiryid);

                                $em = $this->getDoctrine()->getManager();
                                $em->persist($vendorenquiryrel);
                                $em->flush();

                                // Send sms to all vendors
                                $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));

                                // Send sms to mobile number provided.
                                $smsphone      = $accountArray->getSmsphone();
                                $smsphoneArray = explode("-",$smsphone);
                                $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                                $vendorSMSText = "Job $enquiryid in $city for $eSubCategoryString - $description. $multipleCatText Reply 'YES Job Number' for client details.";
                                $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                                /*End of sms to vendors*/
                            }
                        }
                    }
                    else {
                        $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('categoryid'=>$categoryid));
                        foreach($vendorArray as $vendor) {

                            $vendorid       = $vendor->getVendorid();
                            $status         = $vendor->getStatus();
                            $vendorleadpack = $vendor->getLeadpack();

                            $newcityArray = array();

                            //################# city mapping removed ###############################//

                            //$vendorquery = $em->createQueryBuilder()
                                    //->select('c.city')
                                    //->from('BBidsBBidsHomeBundle:City', 'c')
                                    //->innerJoin('BBidsBBidsHomeBundle:Vendorcityrel', 'vc', 'with', 'c.id = vc.city')
                                    //->where('vc.vendorid = :vendorid')
                                    //->setParameter('vendorid', $vendorid);

                            //$vendorcityArray = $vendorquery->getQuery()->getArrayResult();

                            //$vcount = count($vendorcityArray);

                            //for($i=0; $i<$vcount; $i++){
                                //array_push($newcityArray, $vendorcityArray[$i]['city']);
                            //}

                            //if($status == 1 && $vendorleadpack !=0 && in_array($city, $newcityArray)) {

                            //################# city mapping removed ###############################//

                            if($status == 1 && $vendorleadpack !=0 ) {

                                $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                                if(empty($vendenqArray)) {
                                    $vendorenquiryrel = new Vendorenquiryrel();
                                    $vendorenquiryrel->setVendorid($vendorid);
                                    $vendorenquiryrel->setEnquiryid($enquiryid);

                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($vendorenquiryrel);
                                    $em->flush();

                                    // Send sms to all vendors
                                     $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));

                                    // Send sms to mobile number provided.
                                    $smsphone      = $accountArray->getSmsphone();
                                    $smsphoneArray = explode("-",$smsphone);
                                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                                    $vendorSMSText = "Job $enquiryid in $city for $eSubCategoryString - $description. $multipleCatText Reply 'YES Job Number' for client details.";
                                    $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                                    /*End of sms to vendors*/
                                }
                            }
                        }
                    }




                    /*Send sms confirmation to customer*/

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
                    $customerName = 'User';
                    foreach($userDetails as $n) {
                        $smsphone = $n->getSmsphone();
                        $customerName = $n->getContactname();
                    }

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString & vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');
                    /*End*/

                    $categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));
                    foreach($categoryArray as $c){
                        $cat = $c->getCategory();
                    }

                    $activity = new Activity();

                    $activity->setMessage('New Enquiry');
                    $activity->setCity($city);
                    $activity->setCategory($cat);
                    $activity->setAuthorid($uid);
                    $activity->setCreated($created);
                    $activity->setType(1);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($activity);
                    $em->flush();
                    /*return $this->redirect($this->generateUrl('b_bids_b_bids_enquiry_processing'));*/
                    return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
                }
                else{
                    return $this->render('BBidsBBidsHomeBundle:Home:postbysearch.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname));
                }
            }

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname,'selectedCity'=>'','title'=>$title,'twigFileName'=>$twigFileName,'catid'=>$categoryid,'vList'=>$selVendorList));
        } else {
            $session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
        }
    }

    protected function sendSMSToPhone($phoneNumber, $text, $type='') {
        //$phoneNumber = substr($phoneNumber, 1);
        $subphone = substr($phoneNumber, 1);
        $smsphone = "971".$subphone;

		 if(strpos($text, 'for client details'))
			$senderid = 3579;
		else
			$senderid = 'BusinessBid';
        $smsJson = '{
                    "authentication": {
                        "username": "agefilms",
                        "password": "12345Uae"
                    },
                    "messages": [
                        {
                            "sender": '.$senderid.',
                            "text": "'.$text.'",';
        if($type) {
            $smsJson .= '"type":"longSMS",';
        }
        $smsJson .= '
                            "recipients": [
                                {
                                    "gsm": "'.$smsphone.'"
                                }
                            ]
                        }
                    ]
                }';


        $encodeSMS = base64_encode(json_encode($smsJson));
        $ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: api.infobip.com',
            'Accept: */*',
            'Content-Length: ' . strlen($smsJson))
        );

        $result = curl_exec($ch);
        // $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // echo "status : ".$http_status;
         // echo 'response: '.$result;
        $response        = new Response($result);
        $encodedResponse = base64_encode(json_encode($response));

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $sentSMS = $connection->prepare("INSERT INTO `bizbids`.`bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, $phoneNumber, '$encodeSMS', '$encodedResponse', NOW())");
        $sentSMS->execute();

        return $response;
    }

	public function postjobAction(Request $request)
	{
		$session = $this->container->get('session');


		if($session->has('uid')){
			return $this->redirect($this->generateUrl('b_bids_b_bids_post_quote'));
		}
		else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_post_quote_inline'));
		}
	}

	 public function generateHash($length)
        {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randomString = '';

                for($i=0; $i< $length; $i++) {
                        $randomString .= $characters[rand(0, strlen($characters) - 1)];
                }

                return $randomString;
        }



	public function postquoteAction(Request $request)
	{
		$enquiry = new Enquiry();

		$categories = new Categories();

		$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

		foreach($categoriesArray as $category){
			$categoryname = $category->getCategory();
			$categoryid = $category->getId();
			$categorylist[$categoryid] = $categoryname;
		}

		$subcategoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>0,'status'=>1));

		foreach($subcategoriesArray as $subcategory){
			$subcategoryid = $subcategory->getId();
			$subcategoryname = $subcategory->getCategory();
			$subcategories[$subcategoryid] = $subcategoryname;
		}


		$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

        $cityList = array();

        foreach($cityArray as $city){
                $cityname = $city->getCity();
                $cityList[$cityname] = $cityname;
        }

		$subcategory = ["Select a category"];
		$form = $this->createFormBuilder($enquiry)
				->add('category','choice', array('choices'=>$categorylist, 'empty_value'=>'Select a Category', 'empty_data'=>null, 'label'=>'Select a category '))
				// ->add('subcategory','choice', array('choices'=>$subcategories, 'multiple'=>TRUE,  'label'=>'Select a subcategory', 'empty_value'=>'Choose subcategories', 'empty_data'=>null,  'mapped'=>FALSE, 'required'=>TRUE))
				->add('subcategory','choice', array('choices'=>$subcategories, 'multiple'=>TRUE, 'expanded'=>TRUE,  'label'=>'Select a subcategory ', 'empty_value'=>'Choose subcategories', 'empty_data'=>null,  'mapped'=>FALSE, 'required'=>TRUE))
				->add('subj','text', array('label'=>'Enter Subject : '))
				->add('description','textarea', array('label'=>'Describe your enquiry','attr'=>array('maxlength'=>240)))
				->add('hire','choice', array('choices'=> array('1'=>'Urgent', '2'=>'1-3 days', '3'=>'4-7 days','4'=>'7-14 days','5'=>'Flexible Dates'), 'mapped'=>FALSE, 'label'=>'When are you looking to hire *'))
				->add('city','choice', array('label'=>'Please choose your city', 'choices'=>$cityList, 'empty_value'=>'Select city', 'empty_data'=>null))
				->add('location', 'text', array('label'=>'Specify a Location '))
				->add('Post Job','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
				->getForm();
		$form->handleRequest($request);

		if($request->isMethod('POST')){

			$location = $form['location']->getData();
			if($location == "") {
				$session = $this->container->get('session');
				$session->getFlashbag()->add('error', 'LOcation field cannot be left blank');
				return $this->render('BBidsBBidsHomeBundle:Home:enquiry.html.twig', array('form'=>$form->createView()));
			}

			$subj = $form['subj']->getData();
			if($subj == "") {
				$session = $this->container->get('session');
				$session->getFlashBag()->add('error', 'Subject field cannot be blank');
				return $this->render('BBidsBBidsHomeBundle:Home:enquiry.html.twig', array('form'=>$form->createView(), 'category'=>$categoryname));
			}

			$session = $this->container->get('session');

			$categoryid     = $form['category']->getData();
			$subcategoryids = $form['subcategory']->getData();
			$subj           = $form['subj']->getData();
			$description    = $form['description']->getData();
			$city           = $form['city']->getData();

			$uid     = $session->get('uid');


			$customerEmailidArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$uid));
			$customerEmailid      = $customerEmailidArray->getEmail();

			$created = new \DateTime();

			$enquiry = new Enquiry();
			$enquiry->setSubj($subj);
			$enquiry->setDescription($description);
			$enquiry->setCity($city);
			$enquiry->setCategory($categoryid);
			$enquiry->setAuthorid($uid);
			$enquiry->setCreated($created);
			$enquiry->setUpdated($created);
			$enquiry->setStatus(1);
			$enquiry->setLocation($location);

			$em = $this->getDoctrine()->getManager();
			$em->persist($enquiry);
			$em->flush();

			$enquiryid = $enquiry->getId();

			$vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('categoryid'=>$categoryid));

            $count = count($subcategoryids);
            $eSubCategoryString = '';
            for($i=0; $i<$count; $i++) {
                $subCatID = $subcategoryids[$i];

                $enquirysubrel = new EnquirySubcategoryRel();
                $enquirysubrel->setEnquiryid($enquiryid);
                $enquirysubrel->setSubcategoryid($subcategoryids[$i]);

                $eSubCategoryString .= $subcategories[$subCatID].", ";

                $em = $this->getDoctrine()->getManager();
                $em->persist($enquirysubrel);
                $em->flush();
            }

         //   print_r($subcategoryids);
           if($count > 1) {
                $multipleCatText = 'Job with multiple leads credited.';
                if(strlen($eSubCategoryString) > 50){
                    $eSubCategoryString = substr($eSubCategoryString,0,50).'...';
                }
            } else {
                $multipleCatText = '';
                $subCatID = $subcategoryids[0];
                $eSubCategoryString = $subcategories[$subCatID];
            }

			foreach($vendorArray as $vendor) {

				$vendorid       = $vendor->getVendorid();
				$status         = $vendor->getStatus();
				$vendorleadpack = $vendor->getLeadpack();

			//################# city mapping removed ###############################//

				//$cityArray      = array();

				//$vendorquery = $em->createQueryBuilder()
								//->select('c.city')
								//->from('BBidsBBidsHomeBundle:City', 'c')
								//->innerJoin('BBidsBBidsHomeBundle:Vendorcityrel', 'vc', 'with', 'c.id = vc.city')
								//->where('vc.vendorid = :vendorid')
								//->setParameter('vendorid', $vendorid);

				//$vendorcityArray = $vendorquery->getQuery()->getArrayResult();

				//$vcount = count($vendorcityArray);

				//for($i=0; $i<$vcount; $i++){
					//array_push($cityArray, $vendorcityArray[$i]['city']);
				//}

				//if($status == 1 && $vendorleadpack !=0 && in_array($city, $cityArray)){

			//################# city mapping removed ###############################//
				if($status == 1 && $vendorleadpack !=0){

					$vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
					if(empty($vendenqArray)){


					//if($enqcount == 1){
						$vendorenquiryrel = new Vendorenquiryrel();

						$vendorenquiryrel->setVendorid($vendorid);
						$vendorenquiryrel->setEnquiryid($enquiryid);

						$em = $this->getDoctrine()->getManager();
						$em->persist($vendorenquiryrel);
						$em->flush();

                        // Send sms to all vendors
                         $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));

                        // Send sms to mobile number provided.
                        $smsphone      = $accountArray->getSmsphone();
                        $smsphoneArray = explode("-",$smsphone);
                        $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                        $vendorSMSText = "Job $enquiryid in $city for $eSubCategoryString - $description. $multipleCatText Reply 'YES Job Number' for client details.";
                        $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                        /*End of sms to vendors*/
					}
				}
			}


			$enquiryid = $enquiry->getId();


            /*Send sms customer for confirmation */

            $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
            $customerName = 'User';
            foreach($userDetails as $n) {
                $smsphone     = $n->getSmsphone();
                $customerName = $n->getContactname();
            }

            $smsphoneArray = explode("-",$smsphone);
            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

            $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString & vendors will contact you shortly.";
            $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');
            /*End*/

			$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));
			foreach($categoryArray as $c) {
				$cat = $c->getCategory();
			}

			$activity = new Activity();

			$activity->setMessage('New Enquiry');
			$activity->setCity($city);
			$activity->setCategory($cat);
			$activity->setAuthorid($uid);
			$activity->setCreated($created);
			$activity->setType(1);

			$em = $this->getDoctrine()->getManager();
			$em->persist($activity);
			$em->flush();

			$url     = 'https://api.sendgrid.com/';
			$user    = 'businessbid';
			$pass    = '!3zxih1x0';
			$subject = 'Welcome to the BusinessBid Network';

			$body=$this->renderView('BBidsBBidsHomeBundle:Home:enquiryprocessemail.html.twig', array('name'=>$customerName,'jobrequestno'=>$enquiryid));

			$message = array(
						'api_user'  => $user,
						'api_key'   => $pass,
						'to'        => $customerEmailid,
						'subject'   => $subject,
						'html'      => $body,
						'text'      => $body,
						'from'      => 'support@businessbid.ae',
					       );


			$request =  $url.'api/mail.send.json';


			$sess = curl_init($request);
					curl_setopt ($sess, CURLOPT_POST, true);
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					curl_setopt($sess, CURLOPT_HEADER,false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					$response = curl_exec($sess);
					curl_close($sess);

			/*return $this->redirect($this->generateUrl('b_bids_b_bids_enquiry_processing'));*/
			return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
		}
		return $this->render('BBidsBBidsHomeBundle:Home:enquiry.html.twig', array('form'=>$form->createView()));
	}

	public function chooseSubcategoriesByCategoryAction()
	{
		echo $html = $html . sprintf("<option value=\"%d\">%s</option>",1,'hi');
   		return new Response($html);
	}

	public function enquiryprocessAction()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig');
	}

	public function vendorcategoriesfreetailAction(Request $request)
	{


		$session = $this->container->get('session');

		$uid = $session->get('uid');

		if($session->has('uid')){
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
				->select('count(l.userid)')
				->from('BBidsBBidsHomeBundle:Usertotryleadtrel', 'l')
				->add('where','l.userid = :uid')
				->setParameter('uid', $uid);

		$useridexists = $query->getQuery()->getSingleScalarResult();
		$categories = new Categories();
		 $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0));

		$categoryList = array();

		foreach($categoriesArray as $category){
			$categoryid = $category->getId();
			$categoryname = $category->getCategory();
			$categoryList[$categoryid] = $categoryname;
		}

		$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

		$cityList = array();

		foreach($cityArray as $city){
			$cityid = $city->getId();
			$cityname = $city->getCity();
			$cityList[$cityid] = $cityname;

		}



		$status=0;
		$leads=0;
		$created = new \DateTime();



		 $query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
			   ->setParameter('uid', $uid);

		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
		 $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);

					$useridexists = $query->getQuery()->getSingleScalarResult();

			$squery  = $em->createQueryBuilder()
					->select('c.category, c.id')
					->from('BBidsBBidsHomeBundle:Categories', 'c')
					->where('c.parentid != 0');

			$subcategoryArray = $squery->getQuery()->getArrayResult();

			$scount = count($subcategoryArray);

			for($i = 0; $i < $scount; $i++){
				$subid = $subcategoryArray[$i]['id'];
				$sub = $subcategoryArray[$i]['category'];

				$subcategoryList[$subid] = $sub;
			}

		$form = $this->createFormBuilder()
				->add('category','choice', array('label'=>'Please choose your category ', 'choices'=>$categoryList, 'multiple'=>FALSE, 'expanded'=>TRUE))
				->add('subcategory','choice', array('label'=>'Please choose your category ', 'choices'=>$subcategoryList, 'mapped'=>FALSE, 'multiple'=>FALSE, 'expanded'=>TRUE, 'required'=>FALSE))

				->add('city','choice', array('label'=>'Please choose your city', 'choices'=>$cityList, 'multiple'=>TRUE, 'expanded'=>TRUE, 'mapped'=>FALSE))
				->add('free','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center', 'value'=>'Submit')))
				->getForm();


		$form->handleRequest($request);

		if($request->isMethod('POST')){

			$cityArray = $form['city']->getData();


			if(empty($cityArray))
			{
				$session->getFlashBag()->add('error','Please select the aleast one city');
				return $this->redirect($this->generateUrl('b_bids_b_bids_free_trial'));
			}


				$em = $this->getDoctrine()->getManager();

				$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
								->setParameter('uid', $uid);

					$useridexists = $query->getQuery()->getSingleScalarResult();


					if($useridexists == 0){

						$category = $form['category']->getData();

						$vendorcategoryrel = new Vendorcategoriesrel();

						$vendorcategoryrel->setVendorid($uid);
						$vendorcategoryrel->setCategoryid($category);
						$vendorcategoryrel->setStatus(1);
						$vendorcategoryrel->setFlag(2);

						$em = $this->getDoctrine()->getManager();
						$em->persist($vendorcategoryrel);
						$em->flush();

						$cityArray = $form['city']->getData();

						$ccount = count($cityArray);

						if($ccount == 0){
							$session->getFlashBag()->add('error', 'Please choose your city of service');

							return $this->render('BBidsBBidsHomeBundle:User:freetrail.html.twig',array('useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'form'=>$form->createView()));
						}

						for($j=0; $j < $ccount; $j++){
								$citi = $cityArray[$j];

								$vendorcity = new Vendorcityrel();

								$vendorcity->setVendorid($uid);
								$vendorcity->setCity($citi);

								$em->persist($vendorcity);
								$em->flush();

						}
						$subArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$category));

						$query = $em->createQueryBuilder()
							->select('o.id')
							->from('BBidsBBidsHomeBundle:Freetrail','o')
							->add('orderBy', 'o.id DESC')
							->setFirstResult(0)
							->setMaxResults(1);
					$orderArray = $query->getQuery()->getResult();

					if(!empty($orderArray)){

						$ordernum = $orderArray[0]['id'];

					}
					else {
						$ordernum = 1;
					}

					$ordermax = $ordernum + 1;

					$ordernumber = "FTO". $ordermax;
						$freetrail = new Freetrail();

						$freetrail->setorderno($ordernumber);
						$freetrail->setuserid($uid);
						$freetrail->setcategory($category);
						$freetrail->setstatus($status);
						$freetrail->setcreated($created);
						//$freetrail->setupdated($updated);
						$freetrail->setleads($leads);
						$em->persist($freetrail);
						$em->flush();


						foreach($subArray as $s){
									$subcat = $s->getId();
                                     $vendor = new Vendorsubcategoriesrel();

                                        $vendor->setVendorid($uid);
                                        $vendor->setSubcategoryid($subcat);
                                        $vendor->setStatus(1);

                                        $em = $this->getDoctrine()->getManager();
                                        $em->persist($vendor);
                                        $em->flush();
                         }

                    $query = $em->createQueryBuilder()
							->select('f.id,f.userid,f.orderno,f.category, u.email,a.contactname,f.created,u.id,c.category')
							->from('BBidsBBidsHomeBundle:Freetrail', 'f')
							->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'u.id = f.userid')
							->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'a.userid = f.userid')
							->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'c.id = f.category')
							->add('where','f.userid = :uid')
							->setParameter('uid', $uid);


					$usersemail = $query->getQuery()->getArrayResult();

        $userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBY(array('id'=>$uid));

                                foreach($userArray as $u){

                                        $email = $u->getEmail();
					$eid = $u->getId();
                                }

					if($email != ''){
    					$em = $this->getDoctrine()->getManager();

    					$useremail = new Usertoemail();

    					$useremail->setFromuid(11);
    					$useremail->setTouid($eid);
    					$useremail->setFromemail('support@businessbid.ae');
    					$useremail->setToemail($email);
    					$useremail->setEmailsubj('Free Trail');
    					$useremail->setCreated(new \Datetime());
    					$useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:Home:freetrailemailvendor.html.twig',array('users'=>$usersemail)));
    					$useremail->setEmailtype(1);
    					$useremail->setStatus(1);
                        $useremail->setReadStatus(0);

    					$em->persist($useremail);
    					$em->flush();

    					 $useremail = new Usertoemail();

                        $useremail->setFromuid(11);
                        $useremail->setTouid(11);
                        $useremail->setFromemail('support@businessbid.ae');
                        $useremail->setToemail('infosupport@netiapps.com');
                        $useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:Home:freetrailemailvendor.html.twig',array('users'=>$usersemail)));
                        $useremail->setEmailtype(1);
    					$useremail->setEmailsubj('Free Trail');
                        $useremail->setCreated(new \Datetime());

                        $useremail->setStatus(1);
                        $useremail->setReadStatus(0);

                        $em->persist($useremail);
                        $em->flush();

    					 $url = 'https://api.sendgrid.com/';
                        $user = 'businessbid';
                        $pass = '!3zxih1x0';
    					$subject = 'Free Trail';
                        $body=$this->renderView('BBidsBBidsHomeBundle:Home:freetrailemailvendor.html.twig',array('users'=>$usersemail));

                        $message = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'to'        => $email,
                        'subject'   => $subject,
                        'html'      => $body,
                        'text'      => $body,
                        'from'      => 'support@businessbid.ae',
                         );


                        $request =  $url.'api/mail.send.json';

                        // Generate curl request
                        $sess = curl_init($request);
                        // Tell curl to use HTTP POST
                        curl_setopt ($sess, CURLOPT_POST, true);
                        // Tell curl that this is the body of the POST
                        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
                        // Tell curl not to return headers, but do return the response
                        curl_setopt($sess, CURLOPT_HEADER, false);
                        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

                        // obtain response
                        $response = curl_exec($sess);
                        curl_close($sess);
			  		}
                        //print_r($usersemail);exit;
                    $url = 'https://api.sendgrid.com/';
					$user = 'businessbid';
					$pass = '!3zxih1x0';
					$email = 'infosupport@businessbid.ae';
					$subject = 'Request for Freetrail';
					$body=$this->renderView('BBidsBBidsHomeBundle:Home:freetrailemail.html.twig', array('users'=>$usersemail));

					$message = array(
					'api_user'  => $user,
					'api_key'   => $pass,
					'to'        => $email,
					'subject'   => $subject,
					'html'      => $body,
					'text'      => $body,
					'from'      => 'support@businessbid.ae',
					 );

					$request =  $url.'api/mail.send.json';

					// Generate curl request
					$sess = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($sess, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					// Tell curl not to return headers, but do return the response
					curl_setopt($sess, CURLOPT_HEADER, false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					// obtain response
					$response = curl_exec($sess);
					curl_close($sess);



                         $session->getFlashBag()->add('success', 'You have successfully subscribed to Freetrail. Leads will be credited to your account within 24 hours.');
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_freetrail_success'));










	}

	}

	return $this->render('BBidsBBidsHomeBundle:User:freetrail.html.twig',array('useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'form'=>$form->createView()));
	}

	else {
		$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
		return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
	}
}

	public function successAction()
        {

                $session = $this->container->get('session');

                $uid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();


 			$query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


	  return $this->render('BBidsBBidsHomeBundle:Home:success.html.twig',array('useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads));

}

	public function faqAction()
        {
                return $this->render('BBidsBBidsHomeBundle:Home:vendorfaq.html.twig');
        }




	public function supportAction()
        {

                $session = $this->container->get('session');

                $uid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();


                        $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


          return $this->render('BBidsBBidsHomeBundle:Home:support.html.twig',array('useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads));

	}


	public function supportviewAction()
        {

                $session = $this->container->get('session');

                $uid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();


                        $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


          return $this->render('BBidsBBidsHomeBundle:Home:supportview.html.twig',array('useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads));

        }


        public function supportconsumerAction()
        {

                $session = $this->container->get('session');

                $uid = $session->get('uid');

          return $this->render('BBidsBBidsHomeBundle:Home:supportconsumer.html.twig');

		}

		public function supportviewconsumerAction()
        {

                $session = $this->container->get('session');

                $uid = $session->get('uid');




          return $this->render('BBidsBBidsHomeBundle:Home:supportviewconsumer.html.twig');

        }

        public function consumerratingsAction($offset)
	{
		$session = $this->container->get('session');

		if($session->has('uid')){
			$em = $this->getDoctrine()->getManager();

			$sessionid = $session->get('uid');

			$start = (($offset * 4) - 4);
                        $from = $start +1 ;

                        $to = $start + 4;
		      //echo $offset;exit;

			 $query = $em->createQueryBuilder()
				->select('r.id as revid, r.review, r.created, r.rating, r.businessknow, r.businessrecomond, r.ratework, r.rateservice, r.ratemoney, r.ratebusiness, r.servicesummary, r.serviceexperience,e.subj,r.enquiryid,c.category,a.bizname,e.id,e.description,e.location,c.category,e.created')
				->from('BBidsBBidsHomeBundle:Reviews', 'r')
				->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'e.id = r.enquiryid')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'e.category = c.id')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'r.vendorid = a.userid')
				->where('r.authorid = :authorid')
                ->setParameter('authorid', $sessionid)
                ->setFirstResult($start)
                ->setMaxResults(4);


			$reviews = $query->getQuery()->getArrayResult();
           //print_r($reviews);
			//exit;
			$reviewsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Reviews')->findBy(array('authorid'=>$sessionid));
            $count = count($reviewsArray);
			//echo $count;exit;
             if($to>$count)
                   $to = $count;
            $emailCount = $this->getMessageCount();

			return $this->render('BBidsBBidsHomeBundle:Home:consumerratings.html.twig', array('reviews'=>$reviews, 'from'=>$from, 'to'=>$to, 'count'=>$count, 'emailCount'=>$emailCount));
		}
		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}
	public function mappedvendortocustAction($eid)

        {
		$session = $this->container->get('session');
		$sessionid = $session->get('uid');

                if($session->has('uid'))
                {
			$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
                 ->select('e.vendorid,u.id,e.acceptstatus,a.contactname,a.bizname,a.address,a.description,u.email,a.smsphone')
					->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
					->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'e.vendorid = a.userid')
					->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'with', 'e.vendorid = u.id')
					->where('e.enquiryid = :eid')
					->andWhere('e.acceptstatus = 1')
                     ->setParameter('eid', $eid);


                        $mappedenquiry = $query->getQuery()->getArrayResult();
                      //~

                        //~ if($mappedenquiry){
                        //~ foreach($mappedenquiry as $m){
							//~
							 //~ $vid=$m['vendorid'];
							//~
						//~
					//~
                      // $reviewsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Reviews')->findBy(array('authorid'=>$sessionid,'vendorid'=>$vid));
							//~
						//~ $query = $em->createQueryBuilder()
							//~ ->select('r.id')
							//~ ->from('BBidsBBidsHomeBundle:Reviews', 'r')
							//~ ->add('where','r.vendorid = :vid')
							//~ ->andWhere('r.enquiryid = :eid')
							//~ ->setParameter('vid', $vid)
							//~ ->setParameter('eid', $eid);
							//~
                     //~ $reviewsArray = $query->getQuery()->getArrayResult();
				//print_r($reviewsArray);exit;
					//~ $counreview=count($reviewsArray);
						//~
						//~
						//~ }
						//~
					//~ }


			//~ return $this->render('BBidsBBidsHomeBundle:Home:mappedvendortocust.html.twig', array('mappedenquiry'=>$mappedenquiry,'eid'=>$eid,'authorid'=>$sessionid,'counreview'=>$counreview));
			// echo "</pre>";print_r($mappedenquiry);exit;
            return $this->render('BBidsBBidsHomeBundle:Home:mappedvendortocust.html.twig', array('mappedenquiry'=>$mappedenquiry,'eid'=>$eid,'authorid'=>$sessionid));


			}

	}

    public function vieworderoldAction($id)
    {
        $session = $this->container->get('session');
        if($session->has('uid')){
        $em = $this->getDoctrine()->getManager();

        $order = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Order')->findBy(array('id'=>$id));

        $query = $em->createQueryBuilder()
            ->select('c.category, l.leadpack ')
            ->from('BBidsBBidsHomeBundle:Categories', 'c')
            ->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' ,'c.id = l.categoryid')
            ->where('l.ordernumber = :id')
            ->setParameter('id', $id);

        $categories = $query->getQuery()->getResult();
        $query = $em->createQueryBuilder()
            ->select('a.contactname ')
            ->from('BBidsBBidsHomeBundle:Order', 'o')
            ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' ,'o.author =a.userid')
            ->where('o.id = :id')
            ->setParameter('id', $id);

        $ordername = $query->getQuery()->getSingleResult();


         $uid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();


                        $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

        //$categories = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Orderproductsrel')->findBy(array('ordernumber'=>$id));

        return $this->render('BBidsBBidsHomeBundle:Home:order.html.twig', array('order'=>$order, 'categories'=>$categories,'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'ordername'=>$ordername,'ordernumber'=>$id));
        }

        else {
            $session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
        }

    }

	public function vieworderAction($id)
	{
		$session = $this->container->get('session');
		if($session->has('uid')) {
            $em = $this->getDoctrine()->getManager();

        $order = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Order')->findBy(array('id'=>$id));

        $query = $em->createQueryBuilder()
            ->select('c.category, l.leadpack ')
            ->from('BBidsBBidsHomeBundle:Categories', 'c')
            ->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' ,'c.id = l.categoryid')
            ->where('l.ordernumber = :id')
            ->setParameter('id', $id);

        $categories = $query->getQuery()->getResult();
        $query = $em->createQueryBuilder()
            ->select('a.contactname ')
            ->from('BBidsBBidsHomeBundle:Order', 'o')
            ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' ,'o.author =a.userid')
            ->where('o.id = :id')
            ->setParameter('id', $id);

        $ordername = $query->getQuery()->getSingleResult();
        $uid = $session->get('uid');
        $vendorAccount = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid,'profileid'=>3));
        // echo "<pre/>";print_r($order);exit;

        $bizname  = $vendorAccount->getBizname();
        $address  = $vendorAccount->getAddress();
        $smsphone = $vendorAccount->getSmsphone();
        $email    = $this->get('session')->get('email');

        foreach ($order as $value) {
            $orderNumber = $value->getOrdernumber();
            $payType     = $value->getPayoption();
            $payDate     = $value->getCreated();
            break;
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                    ->select('count(l.id)')
                    ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                    ->add('where','l.vendorid = :uid')
                    ->andWhere('l.status = 1')
                    ->setParameter('uid', $uid);
        $useridexists = $query->getQuery()->getSingleScalarResult();


        $query = $em->createQueryBuilder()
                    ->select('count(f.userid)')
                    ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                    ->add('where','f.userid = :uid')
                    ->setParameter('uid', $uid);
        $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

        //$categories = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Orderproductsrel')->findBy(array('ordernumber'=>$id));

        return $this->render('BBidsBBidsHomeBundle:Home:order.html.twig', array('order'=>$order, 'categories'=>$categories,'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'ordername'=>$ordername,'bizname'=>$bizname,'address'=>$address,'smsphone'=>$smsphone,'email'=>$email,'ordernumber'=>$orderNumber,'paytype'=>$payType,'paydate'=>$payDate));
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

    function printOrderAction($id)
    {
        $session = $this->container->get('session');
        if($session->has('uid')) {
            // echo "<pre/>$id";exit;
            $em = $this->getDoctrine()->getManager();
            $orderID = substr($id,2);
            $order = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Order')->findBy(array('id'=>$orderID));
            // echo "<pre/>";print_r($order);exit;
            $query = $em->createQueryBuilder()
                ->select('c.category, l.leadpack ')
                ->from('BBidsBBidsHomeBundle:Categories', 'c')
                ->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' ,'c.id = l.categoryid')
                ->where('l.ordernumber = :id')
                ->setParameter('id', $orderID);

            $categories = $query->getQuery()->getResult();
            $query = $em->createQueryBuilder()
                ->select('a.contactname ')
                ->from('BBidsBBidsHomeBundle:Order', 'o')
                ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' ,'o.author =a.userid')
                ->where('o.id = :id')
                ->setParameter('id', $orderID);

            $ordername = $query->getQuery()->getSingleResult();
            $uid = $session->get('uid');
            $vendorAccount = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid,'profileid'=>3));

            $bizname  = $vendorAccount->getBizname();
            $address  = $vendorAccount->getAddress();
            $smsphone = $vendorAccount->getSmsphone();
            $email    = $this->get('session')->get('email');

            foreach ($order as $value) {
                $orderNumber = $value->getOrdernumber();
                $payType     = $value->getPayoption();
                $payDate     = $value->getCreated();
                break;
            }

            $html = $this->renderView('BBidsBBidsHomeBundle:Mailtemplates:invoice.html.twig',array('order'=>$order, 'categories'=>$categories,'ordername'=>$ordername,'bizname'=>$bizname,'address'=>$address,'smsphone'=>$smsphone,'email'=>$email,'ordernumber'=>$orderNumber,'paytype'=>$payType,'paydate'=>$payDate));
            // echo $html;exit;

            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="'.$orderNumber.'.pdf"'
                )
            );
        }

        else {
            return new Response('Unable to process your request.');
        }
    }

	public function reviewloginAction(Request $request, $returnurl)
	{
		$session = $this->container->get('session');
		$retpath = explode('+',$returnurl);

		if($session->has('uid'))
		{
			return $this->redirect($this->generateUrl('b_bids_b_bids_ratings'));
		}

		$user = new User();

		$form = $this->createFormBuilder($user)
			->add('email','email', array('label'=>'Enter your email id', 'attr'=>array('size'=>30, 'placeholder'=>'Email Id')))
			->add('password','password', array('label'=>'Enter your password', 'attr'=>array('size'=>18, 'placeholder'=>'Password')))
			->add('Login','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				$email = $form['email']->getData();
				$password = md5($form['password']->getData());

				$user = new User();

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email, 'password'=>$password));
				foreach($userArray as $user){
					$uid = $user->getId();
				}

				if(isset($uid)){

					$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

					foreach($accountArray as $account){
						$pid = $account->getProfileid();
						$name = $account->getContactname();

					}

					$session = $this->container->get('session');

					$session->set('uid',$uid);
					$session->set('email',$email);
					$session->set('pid', $pid);
					$session->set('name', $name);
					if($returnurl != 1){
						if($retpath[0] == 'b_bids_b_bids_vendor'){
							$vendorid = $retpath[1];
							return $this->redirect('/vendor/#rating', array('userid'=>$vendorid));
						}
					}
					else if($pid == 2){
						return $this->redirect($this->generateUrl('b_bids_b_bids_ratings'));
					}
					else{
						return $this->redirect($this->generateUrl('b_bids_b_bids_home'));
					}
				}
				else {
					$session = $this->container->get('session');

					$session->getFlashbag()->add('error','Email and password did not match');

					return $this->render('BBidsBBidsHomeBundle:User:loginreview.html.twig',array('form'=>$form->createView(),'title'=>'BusinessBid Write A Review Login Page'));
				}

			}
			else{
				return $this->render('BBidsBBidsHomeBundle:User:loginreview.html.twig', array('form'=>$form->createView(),'title'=>'BusinessBid Write A Review Login Page'));
			}
		}

		return $this->render('BBidsBBidsHomeBundle:User:loginreview.html.twig', array('form'=>$form->createView(),'title'=>'BusinessBid Write A Review Login Page'));
	}


	public function enquirydetailsAction($eid){


		$session = $this->container->get('session');
		$sessionid = $session->get('uid');

                if($session->has('uid'))
                {
					$em = $this->getDoctrine()->getManager();



			$query = $em->createQueryBuilder()
                      ->select('e.id,e.subj,e.description,e.location,c.category,e.created')
                      ->from('BBidsBBidsHomeBundle:Enquiry','e')
					  ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
                      ->where('e.id = :eid')
					  ->andWhere('e.status = 1')
					  ->setParameter('eid', $eid);


         $mappedenquiry = $query->getQuery()->getArrayResult();

         return $this->render('BBidsBBidsHomeBundle:Home:mappedenqyirytocust.html.twig', array('mappedenquiry'=>$mappedenquiry));


	}



	}


	public function reviewdetailsAction($rid){


		$session = $this->container->get('session');
		$sessionid = $session->get('uid');

                if($session->has('uid'))
                {
					$em = $this->getDoctrine()->getManager();

                      $query = $em->createQueryBuilder()
							->select('r.id, r.review, r.created, r.rating, r.businessknow, r.businessrecomond, r.ratework, r.rateservice, r.ratemoney, r.ratebusiness, r.servicesummary, r.serviceexperience,e.subj,r.enquiryid,c.category,e.id')
							->from('BBidsBBidsHomeBundle:Reviews', 'r')
							->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'e.id = r.enquiryid')
							->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'e.category = c.id')
							->where('r.id = :rid')
							->andWhere('e.status = 1')
							->setParameter('rid', $rid);


					$mappedreview = $query->getQuery()->getArrayResult();
					return $this->render('BBidsBBidsHomeBundle:Home:mappedreviewtocust.html.twig', array('mappedreview'=>$mappedreview));

			 }


			 else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}


	}


	public function enquirydetailsvendorAction($eid){


		$session = $this->container->get('session');
		$sessionid = $session->get('uid');

                if($session->has('uid'))
                {


			$uid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();


                        $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

                 $query = $em->createQueryBuilder()
                                ->select('count(e.id)')
                                ->from('BBidsBBidsHomeBundle:Enquiry', 'e');

        $totalenquirycount = $query->getQuery()->getSingleScalarResult();

        $query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                                ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
                                ->where('v.vendorid = :uid')
                                ->andWhere('v.acceptstatus=1')
                                ->setParameter('uid', $uid);

			$totalusercountforapproval = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(o.id)')
				->from('BBidsBBidsHomeBundle:Order','o')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
                                ->where('o.author = :author')
                                ->setParameter('author', $uid);

			$totalordercount = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('o.ordernumber, o.status, o.created, o.amount, o.author,o.id')
				->from('BBidsBBidsHomeBundle:Order', 'o')
				->add('where','o.author = :author')
			    //->andWhere('o.status = 0')
				->add('orderBy', 'o.created DESC')
				->setMaxResults(10)
				->setParameter('author', $uid);

		$enquiries = $query->getQuery()->getResult();

			$query = $em->createQueryBuilder()
                      ->select('e.id,e.subj,e.description,e.location,c.category,e.created')
                      ->from('BBidsBBidsHomeBundle:Enquiry','e')
					  ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
                      ->where('e.id = :eid')
					  ->andWhere('e.status = 1')
					  ->setParameter('eid', $eid);


         $mappedenquiry = $query->getQuery()->getArrayResult();

         return $this->render('BBidsBBidsHomeBundle:Home:mappedenqyiryvendortocust.html.twig', array('useridexists'=>$useridexists , 'totalordercount'=>$totalordercount,'enquiries'=>$enquiries,'useridexistsfreeleads'=>$useridexistsfreeleads,'totalenquirycount'=>$totalenquirycount,'mappedenquiry'=>$mappedenquiry, 'totalusercountforapproval'=>$totalusercountforapproval));


	}

	else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function reviewdetailsvendorAction($rid){


		$session = $this->container->get('session');
		$sessionid = $session->get('uid');

                if($session->has('uid'))
                {


			$uid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
                                ->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();


                        $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

                 $query = $em->createQueryBuilder()
                                ->select('count(e.id)')
                                ->from('BBidsBBidsHomeBundle:Enquiry', 'e');

        $totalenquirycount = $query->getQuery()->getSingleScalarResult();

        $query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                                ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
                                ->where('v.vendorid = :uid')
                                ->andWhere('v.acceptstatus=1')
                                ->setParameter('uid', $uid);

			$totalusercountforapproval = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(o.id)')
				->from('BBidsBBidsHomeBundle:Order','o')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
                                ->where('o.author = :author')
                                ->setParameter('author', $uid);

			$totalordercount = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('o.ordernumber, o.status, o.created, o.amount, o.author,o.id')
				->from('BBidsBBidsHomeBundle:Order', 'o')
				->add('where','o.author = :author')
			    //->andWhere('o.status = 0')
				->add('orderBy', 'o.created DESC')
				->setMaxResults(10)
				->setParameter('author', $uid);

		$enquiries = $query->getQuery()->getResult();

			$query = $em->createQueryBuilder()
							->select('r.id, r.review, r.created, r.rating, r.businessknow, r.businessrecomond, r.ratework, r.rateservice, r.ratemoney, r.ratebusiness, r.servicesummary, r.serviceexperience,e.subj,r.enquiryid,c.category,e.id')
							->from('BBidsBBidsHomeBundle:Reviews', 'r')
							->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'e.id = r.enquiryid')
							->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'e.category = c.id')
							->where('r.id = :rid')
							->andWhere('e.status = 1')
							->setParameter('rid', $rid);


					$mappedreview = $query->getQuery()->getArrayResult();


         $mappedreview = $query->getQuery()->getArrayResult();

         return $this->render('BBidsBBidsHomeBundle:Home:mappedreviewvendortocust.html.twig', array('useridexists'=>$useridexists , 'totalordercount'=>$totalordercount,'enquiries'=>$enquiries,'useridexistsfreeleads'=>$useridexistsfreeleads,'totalenquirycount'=>$totalenquirycount,'mappedreview'=>$mappedreview, 'totalusercountforapproval'=>$totalusercountforapproval));


	}
	else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}


	}

	public function sitemapAction()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:sitemap.html.twig');
	}

	public function contactusAction(Request $request)
	{
		$session = $this->container->get('session');
		$contactus = new Contactus();
		$form = $this->createFormBuilder()
					->add('name', 'text', array('label'=>'Name','attr'=>array('placeholder'=>'Provide your name')))
					->add('mobile', 'text', array('label'=>'Mobile Number','attr'=>array('placeholder'=>'Enter mobile number')))
					->add('email', 'email', array('label'=>'Email ID','attr'=>array('placeholder'=>'Enter your email')))
					->add('usertype','choice', array('label'=>'User Type','choices'=>array('2'=>'Customer', '3'=>'Vendor', '1'=>'Other'),'multiple'=>FALSE, 'expanded'=>TRUE))
					->add('message', 'textarea', array('label'=>'Message','attr'=>array('placeholder'=>'Provide as much detail as you can','maxlength'=>1000)))
					->add('verification', 'text', array('label'=>'Image Text','attr'=>array('placeholder'=>'Enter the text shown in image')))
                    ->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
					->getForm();

		$form->handleRequest($request);

		if($request->getMethod() == "POST") {
	        if ($form->isValid()) {
	        	$verification = $form['verification']->getData();
	        	if (empty($_SESSION['captcha']) || trim(strtolower($verification)) != $_SESSION['captcha']) {
					$session = $this->container->get('session');
					$session->getFlashbag()->add('error','Please enter the correct verification code' );
					return $this->render('BBidsBBidsHomeBundle:Home:contactus.html.twig', array('form'=>$form->createView()));
                }
                else
                	unset($_SESSION['captcha']);

	        	$name = $form['name']->getData();
	        	$mobile = $form['mobile']->getData();
				$email = $form['email']->getData();
				$usertype = $form['usertype']->getData();
				$message = $form['message']->getData();
				$contact = new Contactus();

				$contact->setName($name);
				$contact->setMobile($mobile);
				$contact->setEmail($email);
				$contact->setUsertype($usertype);
				$contact->setMessage($message);
				$contact->setCreated(new \DateTime());
				$em = $this->getDoctrine()->getManager();
				$em->flush();
				if($email)
				{

                    $url     = 'https://api.sendgrid.com/';
                    $user    = 'businessbid';
                    $pass    = '!3zxih1x0';
                    $subject = 'BusinessBid Contactus';
    				$body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Admin:contactustoadminemail.html.twig',array('name'=>$name,'email'=>$email,'mobile'=>$mobile,'usertype'=>$usertype,'message'=>$message));

    				$message = array(
    				'api_user'  => $user,
    				'api_key'   => $pass,
    				'to'        => 'support@businessbid.ae',
    				'subject'   => $subject,
    				'html'      => $body,
    				'text'      => $body,
    				'from'      => 'support@businessbid.ae',
    				 );


    				$request =  $url.'api/mail.send.json';


    				$sess = curl_init($request);

    				curl_setopt ($sess, CURLOPT_POST, true);
    				curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
    				curl_setopt($sess, CURLOPT_HEADER, false);
    				curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
    				$response = curl_exec($sess);
    				curl_close($sess);
    			}

                $url     = 'https://api.sendgrid.com/';
                $user    = 'businessbid';
                $pass    = '!3zxih1x0';
                $subject = 'BusinessBid Contactus';
				$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Others:contactusemail.html.twig',array('name'=>$name));

				$message = array(
				'api_user'  => $user,
				'api_key'   => $pass,
				'to'        => $email,
				'subject'   => $subject,
				'html'      => $body,
				'text'      => $body,
				'from'      => 'support@businessbid.ae',
				 );


				$request =  $url.'api/mail.send.json';

				$sess = curl_init($request);

				curl_setopt ($sess, CURLOPT_POST, true);
				curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
				curl_setopt($sess, CURLOPT_HEADER, false);
				curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($sess);
				curl_close($sess);
				$session->getFlashBag()->add('success', 'Thank you – your message has been submitted successfully!');

				return $this->redirect($this->generateUrl('b_bids_b_bids_contactus'));
	        }
		}
		return $this->render('BBidsBBidsHomeBundle:Home:contactus.html.twig', array('form'=>$form->createView(),'title'=>'BusinessBid Contact Us Form Page'));
	}

	public function globalSearchAction(Request $request)
	{
		$keyword = isset($_GET['keyword']) ? $request->query->get('keyword') : '';
		return $this->render('BBidsBBidsHomeBundle:Home:global_search.html.twig',array('keyword'=>$keyword));
	}

	public function getcustomeremailmessagesAction(Request $request)
	{

		$session = $this->container->get('session');

		if($session->has('uid')){

		$sessionid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

        $connection = $em->getConnection();
        $statement  = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid OR fromuid = $sessionid ORDER BY id DESC");
        $statement->execute();
        $emailArray = $statement->fetchAll();

		$searchform = $this->createFormBuilder()
				->add('from', 'text', array('attr'=>array('placeholder'=>'mm/dd/yyyy', 'id'=>'datepicker1'), 'required'=>false))
				->add('to', 'text', array('attr'=>array('placeholder'=>'mm/dd/yyyy', 'id'=>'datepicker2'), 'required'=>false))
				->add('email', 'hidden', array('attr'=>array('placeholder'=>'Email id'), 'required'=>false))
				->add('search', 'submit')
				->getForm();

        $qb = $em->createQueryBuilder();
        $q = $qb->update('BBidsBBidsHomeBundle:Usertoemail', 'e')
                ->set('e.readStatus', 1)
                ->where('e.touid = :uid or e.fromuid = :fid')
                ->setParameter('fid', $sessionid)
                ->setParameter('uid', $sessionid)
                ->getQuery();
        $p = $q->execute();
        $emailCount = $this->getMessageCount();

		$searchform->handleRequest($request);
		if($request->isMethod('POST')){
			$fromdate = $searchform['from']->getData();
			if($fromdate != ""){
				$fromArray = explode('/', $fromdate);
				$fromdate = $fromArray[2]."-".$fromArray[0]."-".$fromArray[1];
			}

			$todate = $searchform['to']->getData();
			if($todate != ""){
				$toArray = explode('/', $todate);
				$todate = $toArray[2]."-".$toArray[0]."-".$toArray[1];
			}
			$email = $searchform['email']->getData();

			if($fromdate == "" && $todate ==""){
				$session->getFlashBag()->add('error', 'Please fill one of the search fields to get a result!');

				return $this->render('BBidsBBidsHomeBundle:Home:mymessages.html.twig', array('form'=>$searchform->createView(), 'emails'=>$emailArray, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists, 'enquiriesvendorcount'=>$enquiriesvendor,'emailCount'=>$emailCount));

			}

			$em = $this->getDoctrine()->getManager();

			$connection = $em->getConnection();

			if($email != "" && $fromdate == "" && $todate == ""){
				$results = $this->getDoctrine()->getRepository()->findBy(array('fromemail'=>$email, 'touid'=>$sessionid));
			}
			else if($email != "" && $fromdate != "" && $todate == "") {
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND fromemail like '$email' AND date(created) >= '$fromdate'");
                                $statement->execute();
                                $results = $statement->fetchAll();
			}
			else if($email != "" && $fromdate == "" && $todate != ""){
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND fromemail like '$email' AND date(created) <= '$todate'");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}
			else if($email == "" && $fromdate != "" && $todate == ""){
				$statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND date(created) >= '$fromdate'");
				//$statement->bindValue('fromdate', $fromdate);
				$statement->execute();
				$results = $statement->fetchAll();


			}
			else if($email == "" && $fromdate != "" && $todate != ""){
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND date(created) between '$fromdate' AND '$todate' ");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}
			else if($email == "" && $fromdate == "" && $todate != "" ){
				 $statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND date(created) <= '$todate'");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}
			else if($email != "" && $fromdate != "" && $todate != ""){
				$statement = $connection->prepare("SELECT * FROM bbids_usertoemail WHERE touid = $sessionid AND fromemail like '$email' AND date(created) between '$fromdate' AND '$todate' ");
                                //$statement->bindValue('fromdate', $fromdate);
                                $statement->execute();
                                $results = $statement->fetchAll();

			}

			return $this->render('BBidsBBidsHomeBundle:Home:customermessages.html.twig', array('form'=>$searchform->createView(), 'emails'=>$results, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'useridexists'=>$useridexists, 'enquiriesvendorcount'=>$enquiriesvendor,'emailCount'=>$emailCount));

		}

		/*$emailArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Usertoemail')->findBy(array('touid'=>$sessionid));*/

		return $this->render('BBidsBBidsHomeBundle:Home:customermessages.html.twig', array('form'=>$searchform->createView(), 'emails'=>$emailArray,'emailCount'=>$emailCount));

		}
		else {
			$session->getFlashBag()->add('error', 'Session expired!! Please login to continue');

			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

    private function getMessageCount()
    {
        $session = $this->container->get('session');
        $sessionid = $session->get('uid');

        $em = $this->getDoctrine()->getManager();

        $query  = $em->createQueryBuilder()
            ->select('count(e.id)')
            ->from('BBidsBBidsHomeBundle:Usertoemail', 'e')
            ->where('e.touid = :uid or e.fromuid = :fid')
            ->andWhere('e.readStatus = :status')
            ->setParameter('status', 0)
            ->setParameter('fid', $sessionid)
            ->setParameter('uid', $sessionid);

        return $emailcount = $query->getQuery()->getSingleScalarResult();
    }

    private function sendEmail($email,$subject,$body,$uid,$cc = '')
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'businessbid';
        $pass = '!3zxih1x0';
        /*$subject = 'Welcome to the BusinessBid Network';

        $body=*/
        $em = $this->getDoctrine()->getManager();
        $useremail = new Usertoemail();

        $useremail->setFromuid($uid);
        $useremail->setTouid(11);//11 means admin
        $useremail->setFromemail('support@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj($subject);
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $message = array(
            'api_user' => $user,
            'api_key'  => $pass,
            'to'       => $email,
            'subject'  => $subject,
            'html'     => $body,
            'text'     => $body,
            'from'     => 'support@businessbid.ae',
         );

        if($cc != '')
            $message['cc'] = 'support@businessbid.ae';
        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $sess = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($sess, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        // Tell curl not to return headers, but do return the response
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($sess);
        curl_close($sess);
        // echo "$response : $response";
    }

    public function areyouaVendorAction(Request $request)
	{
		return $this->render('BBidsBBidsHomeBundle:Home:are_youa_vendor.html.twig');
	}

	public function bbidsWorkAction(Request $request)
	{
		return $this->render('BBidsBBidsHomeBundle:Home:bbids_work.html.twig');
	}

	public function getHiredAction(Request $request)
	{
		return $this->render('BBidsBBidsHomeBundle:Home:get_hired.html.twig');
	}
	public function whyJoinBbidsAction(Request $request)
	{
		return $this->render('BBidsBBidsHomeBundle:Home:why_join_bbids.html.twig');
	}
	public function template5Action()
	{
		return $this->render('BBidsBBidsHomeBundle:Home:template5.html.twig');
	}

    function vendorSearchHomeAction()
    {
        $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));
        return $this->render('BBidsBBidsHomeBundle:Home:vendor_search_home.html.twig',array('categories'=>$categoriesArray));
    }

    function vendorDirectEnqAction(Request $request)
    {
        $data = $request->request->all();
        if ($request->getMethod() == 'POST') {
                $contactname = $data['direct_enq_form']['contactname'];
                $custEmail   = $data['direct_enq_form']['email'];
                $mobile      = $data['direct_enq_form']['mobile'];
                $description = $data['direct_enq_form']['description'];
                $vendorid    = $data['direct_enq_form']['usertype'];

                $subject = 'Businessbids Customer Enquiry';

                $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:direct_enquiry_email.html.twig', array('contactname'=>$contactname, 'email'=>$custEmail,'mobile'=>$mobile,'description'=>$description));

                $userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                $email     = $userArray->getEmail();
                $this->sendEmail($email,$subject,$body,$vendorid,'support@businessbid.ae');
                return new response('Success');

        } else {
            return new response('Error');
        }

    }

    function jobPostSucessfulAction(Request $request)
    {
        $eid = '';
        $categoryid = '';
        if($request->isMethod('POST')) {
            $session = $this->container->get('session');
            $em = $this->getDoctrine()->getManager();

            $postParams = $this->get('request')->request->all();
            //echo '<pre/>';print_r($postParams);exit;
            $eid = $postParams['jobno'];
            $enquiryObject   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($eid);
            if(is_null($enquiryObject) || empty($enquiryObject)) {
                $session->getFlashBag()->add('error','Unable to find the job you are requesting.');
                $categoryid = null;
            } else
                $categoryid = $enquiryObject->getCategory();
        }

        return $this->render('BBidsBBidsHomeBundle:Home:job_post_successfull.html.twig',array('eid'=>$eid,'catid'=>$categoryid));
    }
}
/*End of Class*/

<?php
// src/BBids/BBidsHomeBundle/Controller/MenuController.php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    public function fetchMenuAction($typeOfMenu = '')
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('BBidsBBidsHomeBundle:Categories', 'c')
                        ->add('where','c.parentid = 0')
                        ->andWhere('c.status = 1')
                        ->andWhere('c.menuOrder IS NOT NULL')
                        ->add('orderBy', 'c.menuOrder ASC')
                        ->setMaxResults(14);
        $categoriesArray = $query->getQuery()->getResult();

        $session = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        return $this->render('BBidsBBidsHomeBundle:Home:menu.html.twig', array('categories'=>$categoriesArray,'typeOfMenu'=>$typeOfMenu,'loginFlag'=>$loginFlag));
    }
} //End of class
<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetaData;
use BBids\BBidsHomeBundle\Entity\Smsreplies;
use BBids\BBidsHomeBundle\Entity\Enquiry;
use BBids\BBidsHomeBundle\Entity\Categories;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;

class SmsController extends Controller
{
	function smsRepliesAction(Request $request)
	{
		/* "The SMS Replies via Post or Get Method <pre/>";
		print_r($request); */
        $replies = print_r($_REQUEST,TRUE);
		$em = $this->getDoctrine()->getManager();
		$systemResponse = "Empty";

		$vendorNumber = $_REQUEST['sender'];
		$text         = strtoupper($_REQUEST['text']);

		/*$vendorNumber      = '971525936286';
		$text              = 'BID 208';*/

		$recievedTextArray = explode(" ",$text);
		if($recievedTextArray[0] == 'BID') {
			$enquiryid = (int)$recievedTextArray[1];
			$enquiry   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);

			if(is_null($enquiry)) {
				//Send error sms to vendor
				$systemResponse = $smsText = "Unable to find the job you are requesting.";
				$this->sendSMSToPhone($vendorNumber, $smsText);
			} else {
				//Get Vendor ID from db
				$vendorSubNumb = substr($vendorNumber,5,12);

                $vendorcode = substr($vendorNumber,3,2);
                $vendorphone = '0'.$vendorcode.'-'.trim($vendorSubNumb);

				$smsphoneArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('profileid'=>3, 'smsphone'=>$vendorphone));

				//print_r($smsphoneArray);
				if(!empty($smsphoneArray)){
                    if(count($smsphoneArray) > 1){
                        $smsText = "Job $enquiryid unable to accept as your number is already in use by other vendor - Please Contact BusinessBid Support";
                        $this->sendSMSToPhone($vendorNumber, $smsText);
                        // exit('planned exit');
                        return new response($smsText);
                    }
					foreach($smsphoneArray as $s){
                        $vendorid = $s->getUserid();
                        $smsphone = $s->getSmsphone();
                        $vendorname = $s->getContactname();
					}

                    $vendorDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                    $vendorEmail = $vendorDetails->getEmail();

					// echo $email; echo $enquiryid; exit;
                    $category        = $enquiry->getCategory();
                    $enquiryLocation = $enquiry->getLocation();
                    $expStatus       = $enquiry->getExpstatus();

                    if($expStatus == 2 OR $expStatus == 1) {
                        $smsText = "Job $enquiryid is no longer available - Regards BusinessBid";
                        $this->sendSMSToPhone($vendorNumber, $smsText);
                        // exit('planned exit');
                        return new response($smsText);
                    }

					// Manually Category mapping by admin
                    $vendorcategoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findBy(array('categoryID'=>$category, 'vendorID'=>$vendorid, 'status'=>1));

					if(!empty($vendorcategoryArray)) {
						// Send SMS to vendor telling to purchase leads as this is kind of free trial
                        $smsText = "You do not have any leads to accept job requests. Please contact BusinessBid on 044213777 or login http://www.businessbid.ae/login to top up your lead balance.";
                        $this->sendSMSToPhone($vendorNumber, $smsText);
                        $systemResponse = 'You do not have sufficient lead credit to accept this enquiry. Please purcace more leads';
                        return new response($systemResponse);
					}


					$acceptanceStatusQuery = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid,'enquiryid'=>$enquiryid));
					//print_r($acceptanceStatusQuery); exit;
					if(!empty($acceptanceStatusQuery)){
						foreach($acceptanceStatusQuery as $asq){

							$acceptanceStatus = $asq->getAcceptstatus();
							$vdid = $asq->getId();
						}
					}



					if($acceptanceStatus == '' or $acceptanceStatus == NULL) {
						$query = $em->createQueryBuilder()
                                ->select('count(ve.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 've')
                                ->add('where', 've.enquiryid = :enquiryid')
                                ->andWhere('ve.acceptstatus =1')
                                ->setParameter('enquiryid', $enquiryid);
						$enquiryacceptcount = $query->getQuery()->getSingleScalarResult();

						//echo $enquiryacceptcount; exit;
						if($enquiryacceptcount  >= 3){
							// Send SMS to vendor
                            $smsText = "Sorry Job Number $enquiryid  for $category in $enquiryLocation is no longer available. No leads deducted. Please respond earlier next time.";
                            $this->sendSMSToPhone($vendorNumber, $smsText);
                            $systemResponse = 'You cannot Accept this enquiry,Already 3 vendors have accepted this enquiry ';
                            return new response($systemResponse);
						}
						else {

							$acount = $enquiryacceptcount + 1;

                            $query = $em->createQueryBuilder()
                                    ->select('count(e.id)')
                                    ->from('BBidsBBidsHomeBundle:EnquirySubcategoryRel', 'e')
                                    ->add('where', 'e.enquiryid = :enquiryid')
                                    ->setParameter('enquiryid', $enquiryid);

                            $subcount = $query->getQuery()->getSingleScalarResult();

							$vendorcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid, 'categoryid'=>$category, 'status'=>1));


							if(!empty($vendorcatArray)) {
                                // Freee trial leads purchase
                                foreach($vendorcatArray as $vendorcat) {
                                    $id       = $vendorcat->getId();
	                                $leadpack = $vendorcat->getLeadpack();

        	                        if($leadpack >= 1) { // check for free leads av more than request for job
            	                        $nowpack = $leadpack - 1;

									// Modified Logic - One single checkpoint for acceptance count - Anup
                                                        /*                if($enquiryacceptcount < 3) // Check for 3X vendors
                                                                        { */
                                        $em = $this->getDoctrine()->getManager();

                                        $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);
                                        $lead->setLeadpack($nowpack);
                                        $lead->setDeductedlead(1);
                                        $lead->setDeductedfreelead(0);
                                        $em->flush();

                                        $vendorflag = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);
                                        $vflag = $vendorflag->getFlag();

                                        $vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vdid);

                                        $vendor->setAcceptstatus(1);
										$vendor->setFlag($vflag);
										$em->flush();

										if($nowpack == 0) {
                                    	        $em = $this->getDoctrine()->getManager();

                                            	$lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);

                                                $lead->setStatus(0);
                                                $lead->setDeductedlead(0);
                                                $lead->setDeductedfreelead(0);

                                                $em->flush();
                                        }

										$enquiryquery = $em->createQueryBuilder()
													->select('a.contactname, u.email, e.city, e.location, a.smsphone, a.homephone')
													->from('BBidsBBidsHomeBundle:Account', 'a')
													->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'a.userid = e.authorid')
													->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'with', 'u.id = e.authorid')
													->add('where', 'e.id = :enquiryid')
													->setParameter('enquiryid', $enquiryid);

										$enquiryArray = $enquiryquery->getQuery()->getArrayResult();

										//print_r($enquiryArray); echo "frbeahj";
										$contactname = $enquiryArray[0]['contactname'];
										$useremail = $enquiryArray[0]['email'];
										$encity = $enquiryArray[0]['city'];
										$enloc = $enquiryArray[0]['location'];
										$ensmsphone = $enquiryArray[0]['smsphone'];
										$enhomephone = $enquiryArray[0]['homephone'];


                    $smsText = "Accepted BID ".$enquiryid." ".$acount.'/3\r\n';
                    $smsText .= 'Contactname: '.$contactname.'\r\n';
                    $smsText .= 'Email: '.$useremail.'\r\n';
                    $smsText .= 'City: '.$encity.'\r\n';
                    $smsText .= 'Location: '.$enloc.'\r\n';
                    $smsText .= 'Phone: '.$ensmsphone.'\r\n';

                    $this->sendSMSToPhone($vendorNumber, $smsText);
                    $url = 'https://api.sendgrid.com/';
                    $user = 'businessbid';
                    $pass = '!3zxih1x0';
                    $subject = 'Job request accepted';
                    $body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig', array('enquiryid'=>$enquiryid, 'category'=>$category,'subcount'=>$subcount,'name'=>$vendorname,'location'=>$enloc,'authname'=>$contactname,'authmobile'=>$ensmsphone));
                    $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $vendorEmail,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                     );


                    $request =  $url.'api/mail.send.json';

                    $sess = curl_init($request);
                    curl_setopt ($sess, CURLOPT_POST, true);
                    curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
                    curl_setopt($sess, CURLOPT_HEADER, false);
                    curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

                    // obtain response
                    $response = curl_exec($sess);
                    curl_close($sess);
	                $systemResponse = 'You have successfully accepted the enquiry.'.$subcount.' leads were credited from your account. From your free leads.';
                    // exit($systemResponse);
                    return new response($systemResponse);
				}
				else {
										$vcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid, 'categoryid'=>$category, 'flag'=>2));

                	                                                        if(!empty($vcatArray)) {

                        	                                                        foreach($vcatArray as $vendorcat) {
                                	                                                        $nid       = $vendorcat->getId();
                                        	                                                $nleadpack = $vendorcat->getLeadpack();
                                                	                                        $newlead   = $leadpack + $nleadpack;

                                                        	                                if($newlead >= 1) {  // check for Purchase leads + free leads av more to accept the job
                                                                                                //$nowpack = $nleadpack - $leadpack;
                                                                	                                $nowpack = $newlead - 1;
                                                                        	                        $deductedbuy=$subcount-$leadpack;
                                                                                /*                if($enquiryacceptcount < 3) { // Check for 3X vendors */
                                                                                                        $em = $this->getDoctrine()->getManager();
                                                                                                        $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);

                                                                                                        $lead->setLeadpack(0);
                                                                                                        $lead->setStatus(0);
                                                                                                        //$lead->setDeductedlead($deductedbuy);
                                                                                                        //$lead->setDeductedfreelead($leadpack);

                                                                                                        $em->flush();

                                                                                                        $em = $this->getDoctrine()->getManager();

                                                                                                        $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);

                                                                                                        $lead->setLeadpack($nowpack);
                                                                                                        $lead->setStatus(1);
                                                                                                        $lead->setDeductedlead($deductedbuy);
                                                                                                        $lead->setDeductedfreelead($leadpack);

                                                                                                        $em->flush();

                                                                                                        $vendorflag = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);
                                                                                                        $vflag = $vendorflag->getFlag();


                                                                                                        $vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vdid);
                                                                                                        $vendor->setAcceptstatus(1);
                                                                                                        $vendor->setFlag(1);

                                                                                                        $em->flush();

																										$enquiryquery = $em->createQueryBuilder()
													->select('a.contactname, u.email, e.city, e.location, a.smsphone, a.homephone')
													->from('BBidsBBidsHomeBundle:Account', 'a')
													->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'a.userid = e.authorid')
													->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'with', 'u.id = e.authorid')
													->add('where','e.id = :enquiryid')
													->setParameter('enquiryid', $enquiryid);

										$enquiryArray = $enquiryquery->getQuery()->getArrayResult();


                                        $contactname = $enquiryArray[0]['contactname'];
                                        $useremail   = $enquiryArray[0]['email'];
                                        $encity      = $enquiryArray[0]['city'];
                                        $enloc       = $enquiryArray[0]['location'];
                                        $ensmsphone  = $enquiryArray[0]['smsphone'];
                                        $enhomephone = $enquiryArray[0]['homephone'];


                                        $smsText = "Accepted BID ".$enquiryid." ".$acount.'/3\r\n';
                                        $smsText .= 'Contactname: '.$contactname.'\r\n';
                                        $smsText .= 'Email: '.$useremail.'\r\n';
                                        $smsText .= 'City: '.$encity.'\r\n';
                                        $smsText .= 'Location: '.$enloc.'\r\n';
                                        $smsText .= 'Phone: '.$ensmsphone.'\r\n';


													 $oldsmsText = "Accepted Lead $subcount for $enquiryid in $enquiryLocation Contact (Insert Client Name) on (insert client mobile number)";
        	                                                                                        $this->sendSMSToPhone($vendorNumber, $smsText);
                    $url = 'https://api.sendgrid.com/';
                    $user = 'businessbid';
                    //$pass = 'n9(Sg;iz{0{Qf>x';
					$pass = '!3zxih1x0';					
                    $subject = 'Job request accepted';
                    $body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig', array('enquiryid'=>$enquiryid, 'category'=>$category,'subcount'=>$subcount,'name'=>$vendorname,'location'=>$enloc,'authname'=>$contactname,'authmobile'=>$ensmsphone));
                    $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                     );


                    $request =  $url.'api/mail.send.json';

                    // Generate curl request
                    $sess = curl_init($request);
                    // Tell curl to use HTTP POST
                    curl_setopt ($sess, CURLOPT_POST, true);
                    // Tell curl that this is the body of the POST
                    curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
                    // Tell curl not to return headers, but do return the response
                    curl_setopt($sess, CURLOPT_HEADER, false);
                    curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

                    // obtain response
                    $response = curl_exec($sess);
                    curl_close($sess);
                                $systemResponse = 'You have successfully accepted the enquiry.'.$subcount.' leads were credited from your account. From your free leads + purchase leads.';
                            // exit($systemResponse);
                            return new response($systemResponse);
												}
												else {
                                                	// Send SMS to vendor
                                                    $smsText = "You do not have any leads to accept new job requests. Please contact BusinessBid on 044213777 or login http://www.businessbid.ae/login to top up your lead balance.";
                                                    $this->sendSMSToPhone($vendorNumber, $smsText);
                                                    $systemResponse = 'You do not have sufficient lead credit to accept this enquiry. Please purcace more leads';

												}
											}
										 }
										else {
                                            	// Send SMS to vendor
                                                $smsText = "You do not have any leads to accept new job requests. Please contact BusinessBid on 044213777 or login http://www.businessbid.ae/login to top up your lead balance.";
                                                $this->sendSMSToPhone($vendorNumber, $smsText);
                                                $systemResponse = 'You do not have sufficient lead credit in purchase leads or in free trial leads to accept this enquiry. Please purcace more leads';
                                            // exit($systemResponse);
                                            return new response($systemResponse);
                                        }
									}
								}
							}
							else {
                            	// if empty of vendorcatArray and it means vendor does not have any free trial leads for job category
                                $vendorcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid, 'categoryid'=>$category, 'flag'=>1));

                                if(!empty($vendorcatArray)) {
                                        foreach($vendorcatArray as $vendorcat) {
	                                        $nid = $vendorcat->getId();
        	                                $nleadpack = $vendorcat->getLeadpack();
                    	                        if($nleadpack >= 1) {
                                                $nowpack = $nleadpack - 1;
                                                $deductedbuy =$nleadpack -$nowpack;

                                                $em = $this->getDoctrine()->getManager();
                                                /* if($enquiryacceptcount < 3) { */

                                                $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);
                                                $lead->setLeadpack($nowpack);
                                                $lead->setDeductedlead($deductedbuy);
                                                $lead->setDeductedfreelead(0);
                                                $em->flush();

                                                $vendorflag = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);
                                                $vflag = $vendorflag->getFlag();
                                                //echo 'third';echo $vflag;exit;

                                                $vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vdid);

                                                $vendor->setAcceptstatus(1);
                                                $vendor->setFlag($vflag);

											$em->flush();

											if($nowpack == 0) {
                                                $em = $this->getDoctrine()->getManager();

                                                $lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($nid);

                                                $lead->setStatus(0);

                                                $em->flush();
                                            }

											$enquiryquery = $em->createQueryBuilder()
													->select('a.contactname, u.email, e.city, e.location, a.smsphone, a.homephone')
													->from('BBidsBBidsHomeBundle:Account', 'a')
													->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'a.userid = e.authorid')
													->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'with', 'u.id = e.authorid')
													->where('e.id = :enquiryid')
													->setParameter('enquiryid', $enquiryid);

    										$enquiryArray = $enquiryquery->getQuery()->getArrayResult();

                                            $contactname = $enquiryArray[0]['contactname'];
                                            $useremail   = $enquiryArray[0]['email'];
                                            $encity      = $enquiryArray[0]['city'];
                                            $enloc       = $enquiryArray[0]['location'];
                                            $ensmsphone  = $enquiryArray[0]['smsphone'];
                                            $enhomephone = $enquiryArray[0]['homephone'];

                                            $smsText = "Accepted BID ".$enquiryid." ".$acount.'/3\r\n';
                                            $smsText .= 'Contactname: '.$contactname.'\r\n';
                                            $smsText .= 'Email: '.$useremail.'\r\n';
                                            $smsText .= 'City: '.$encity.'\r\n';
                                            $smsText .= 'Location: '.$enloc.'\r\n';
                                            $smsText .= 'Phone: '.$ensmsphone.'\r\n';



                                        	$this->sendSMSToPhone($vendorNumber, $smsText);
                                            $url = 'https://api.sendgrid.com/';
                                            $user = 'businessbid';
                                            $pass = '!3zxih1x0';
                                            $subject = 'Job request accepted';
                                            $body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig', array('enquiryid'=>$enquiryid, 'category'=>$category,'subcount'=>$subcount,'name'=>$vendorname,'location'=>$enloc,'authname'=>$contactname,'authmobile'=>$ensmsphone));
                                            $message = array(
                                            'api_user'  => $user,
                                            'api_key'   => $pass,
                                            'to'        => $email,
                                            'subject'   => $subject,
                                            'html'      => $body,
                                            'text'      => $body,
                                            'from'      => 'support@businessbid.ae',
                                             );


                                            $request =  $url.'api/mail.send.json';

                                            // Generate curl request
                                            $sess = curl_init($request);
                                            // Tell curl to use HTTP POST
                                            curl_setopt ($sess, CURLOPT_POST, true);
                                            // Tell curl that this is the body of the POST
                                            curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
                                            // Tell curl not to return headers, but do return the response
                                            curl_setopt($sess, CURLOPT_HEADER, false);
                                            curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

                                            // obtain response
                                            $response = curl_exec($sess);
                                            curl_close($sess);
                                            $systemResponse = 'You have successfully accepted the enquiry.'.$subcount.' leads were credited from your account. From your purchase leads.';
                                            // exit($systemResponse);
                                            return new response($systemResponse);
										}
										else {
                                            // Send SMS to vendor
                                        	$smsText = "You do not have any leads to accept new job requests. Please contact BusinessBid on 044213777 or login http://www.businessbid.ae/login to top up your lead balance.";
                                            $this->sendSMSToPhone($vendorNumber, $smsText);
                                            $systemResponse = 'You do not have sufficient lead credit to accept this enquiry. Please purcace more leads';
                                            // exit($systemResponse);
                                            return new response($systemResponse);
    	                                }
									}
								}
								else {
                                	// Send SMS to vendor
                                    $smsText = "You do not have any leads to accept new job requests. Please contact BusinessBid on 044213777 or login http://www.businessbid.ae/login to top up your lead balance.";
                                    $this->sendSMSToPhone($vendorNumber, $smsText);
                                    $systemResponse = 'You do not have sufficient lead credit to accept this enquiry. Please purcace more leads';
                                    // exit($systemResponse);
                                    return new response($systemResponse);
	                            }
							}
						}
					}
					else {
						$smsText = "You have already accepted job $enquiryid";
                        $this->sendSMSToPhone($vendorNumber, $smsText);
                        $systemResponse = "You have already accepted job $enquiryid";
					}

				}
				else {
					// Send SMS to vendor
                    $smsText = "Number not found in db and your number is not registered with us";
                    $this->sendSMSToPhone($vendorNumber, $smsText);
                    $systemResponse = 'Number not found in db and Tell the customer number is not registered with us';
                    // exit($systemResponse);
                    return new response($systemResponse);
				}
			}
		}
		else {
			// Send error sms to vendor
            $smsText = "Unable to process your request.";
            $this->sendSMSToPhone($vendorNumber, $smsText);
            $systemResponse = 'Number not found in db and Tell the customer number is not registered with us';

        }

		$smsreplies =  new Smsreplies();

		$smsreplies->setParentid(1);
		$smsreplies->setReply($replies);
		$smsreplies->setSystemresponse($systemResponse);

		$em->persist($smsreplies);
		$em->flush();
        return new response("SMS Sent Successful");
	}

	protected function sendSMSToPhone($phoneNumber, $text, $type='') {
		/* $subphone = substr($phoneNumber, 1);
        $smsphone = "9".$subphone; */
        // $phoneNumber = substr($phoneNumber, 1);
        $smsJson = '{
                    "authentication": {
                        "username": "agefilms",
                        "password": "12345Uae"
                    },
                    "messages": [
                        {
                            "sender": "3579",
                            "text": "'.$text.'",';
        if(true) {
            $smsJson .= '"type":"longSMS",';
        }
        $smsJson .= '
                            "recipients": [
                                {
                                    "gsm": "'.$phoneNumber.'"
                                }
                            ]
                        }
                    ]
                }';


        $encodeSMS = base64_encode(json_encode($smsJson));
        $ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: api.infobip.com',
            'Accept: */*',
            'Content-Length: ' . strlen($smsJson))
        );

        $result = curl_exec($ch);
        // $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // echo "status : ".$http_status;
        // echo 'response: '.$result;
        $response        = new response($result);
        $encodedResponse = base64_encode(json_encode($response));

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
       $sentSMS = $connection->prepare("INSERT INTO `bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, $phoneNumber, '$encodeSMS', '$encodedResponse', NOW())");
        $sentSMS->execute();
        return $response;
    }

	function sendJobReqSmsToVendorsAction()
	{
		// return true;
		$smsJson = '{
					"authentication": {
						"username": "agefilms",
						"password": "12345Uae"
					},
					"messages": [
						{
							"sender": "2240",
							"text": "Lead 3636 in Sharjah for Catering Services - Hotel Staff Catering -\'Looking for Educational Institute Catering\'. Respond YES BB36 for client details",
							"type": "longSMS",
							"recipients": [
								{
									"gsm": "'.$phoneNumber.'"
								}
							]
						}
					]
				}';
		$encodeSMS = base64_encode(json_encode($smsJson));
		$ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json',
			'Host: api.infobip.com',
			'Accept: */*',
		    'Content-Length: ' . strlen($smsJson))
		);

		$result = curl_exec($ch);
		// $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		// echo "status : ".$http_status;
		// echo 'response: '.$result;
		$response 	= new response($result);
		$encodedResponse = base64_encode(json_encode($response));

		$em = $this->getDoctrine()->getManager();
		$connection = $em->getConnection();
		$sentSMS      = $connection->prepare("INSERT INTO `bizbids`.`bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, '1', '$encodeSMS', '$encodedResponse', NOW())");
		$sentSMS->execute();

		return $response;
	}
}/*End of Class*/

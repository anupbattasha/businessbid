<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Type;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\Account;
use BBids\BBidsHomeBundle\Entity\Categories;
use BBids\BBidsHomeBundle\Entity\City;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorsubcategoriesrel;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\Sessioncategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorcityrel;
use BBids\BBidsHomeBundle\Entity\Vendorcertificationrel;
use BBids\BBidsHomeBundle\Entity\Vendorproductsrel;
use BBids\BBidsHomeBundle\Entity\Vendororderproductsrel;
use BBids\BBidsHomeBundle\Entity\Order;
use BBids\BBidsHomeBundle\Entity\Orderproductsrel;
use BBids\BBidsHomeBundle\Entity\Vendoralbumrel;
use BBids\BBidsHomeBundle\Entity\Albumphotorel;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use BBids\BBidsHomeBundle\Entity\Reviews;
use BBids\BBidsHomeBundle\Entity\Ratings;
use BBids\BBidsHomeBundle\Entity\Usertotryleadtrel;
use Ob\HighchartsBundle\Highcharts\Highchart;
use BBids\BBidsHomeBundle\Entity\Feedback;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use BBids\BBidsHomeBundle\Entity\Vendorlicenserel;
use BBids\BBidsHomeBundle\Entity\Purchaseleads;
use BBids\BBidsHomeBundle\Entity\Paymentgatewayinfo;
use BBids\BBidsHomeBundle\Entity\Checkpaymenthistory;
use Symfony\Component\Security\Core\Util\SecureRandom;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use BBids\BBidsHomeBundle\Entity\Categoriesextrafieldsrel;
use BBids\BBidsHomeBundle\Entity\Vendorcategorymapping;
use BBids\BBidsHomeBundle\Entity\Cashpaymenthistory;
use BBids\BBidsHomeBundle\Entity\Vendorservices;
use BBids\BBidsHomeBundle\Entity\Registrationfeecatrel;
use BBids\BBidsHomeBundle\Entity\Token;
use BBids\BBidsHomeBundle\Entity\Vendorlogorel;

class UserController extends Controller
{
	public function getleadcreditAction($userid)
	{
		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('c.category, lc.leadcredited, lc.created, lc.leadcount, lc.newleadcount')
			->from('BBidsBBidsHomeBundle:Categories', 'c')
			->innerJoin('BBidsBBidsHomeBundle:Leadcredit', 'lc', 'with', 'lc.categoryid = c.id')
			->where('lc.userid = :userid')
			->setParameter('userid', $userid);

		$credits = $query->getQuery()->getArrayResult();

		return $this->render('BBidsBBidsHomeBundle:User:showleadscredit.html.twig', array('credits'=>$credits));
	}

	private function getMessageCount()
	{
		$session = $this->container->get('session');

		$sessionid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$query  = $em->createQueryBuilder()
            ->select('count(e.id)')
            ->from('BBidsBBidsHomeBundle:Usertoemail', 'e')
            ->where('e.touid = :uid or e.fromuid = :fid')
            ->andWhere('e.readStatus = :status')
            ->setParameter('status', 0)
            ->setParameter('fid', $sessionid)
            ->setParameter('uid', $sessionid);

		return $emailcount = $query->getQuery()->getSingleScalarResult();


	}

	public function feedbackAction(Request $request)
        {
		$em = $this->getDoctrine()->getManager();
		$session = $this->container->get('session');

		if($session->has('uid')) $sessionemail = $session->get('email'); else $sessionemail = "";
        $form = $this->createFormBuilder()
				->add('name', 'text', array('label'=>'Name','attr'=>array('placeholder'=>'Provide your name')))
				->add('email', 'email', array('label'=>'Email ID','attr'=>array('placeholder'=>'Enter your email', 'value'=>$sessionemail)))
				->add('subject', 'text', array('label'=>'Subject','attr'=>array('placeholder'=>'Provide subject')))
				->add('verification', 'text', array('label'=>'Image Text','attr'=>array('placeholder'=>'Enter the text shown in image')))
                ->add('feedback', 'textarea', array('label'=>'Feedback','attr'=>array('placeholder'=>'Provide as much detail as you would like regarding your enquiry','maxlength'=>1000)))
                ->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
				->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				// $session = $this->get('session')->all();echo '<pre/>';print_r($session);exit;
				$feedback     = $form['feedback']->getData();
				$summary      = $form['subject']->getData();
				$email        = $form['email']->getData();
				$verification = $form['verification']->getData();
				$fname = $form['name']->getData();

				if (empty($_SESSION['captcha']) || trim(strtolower($verification)) != $_SESSION['captcha']) {
					$session = $this->container->get('session');
					$session->getFlashbag()->add('error','Please enter the correct verification code' );
					return $this->render('BBidsBBidsHomeBundle:Home:feedback.html.twig', array('form'=>$form->createView()));
                }
                else
                	unset($_SESSION['captcha']);

				$feed = new Feedback();

				$feed->setFeedback($feedback);
				$feed->setSummary($summary);
				$feed->setEmail($email);
				$feed->setCreated(new \DateTime());
				$feed->setUpdated(new \DateTime());
				$feed->setStatus(1);

				$em->persist($feed);
				$em->flush();

				$useremail = new Usertoemail();

                $useremail->setFromuid(11);
                $useremail->setTouid(1);
                $useremail->setFromemail('support@businessbid.ae');
                $useremail->setToemail($email);
                $useremail->setEmailsubj('Feedback Auto Response BusinessBid');
                $useremail->setCreated(new \Datetime());
                $useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Others:feedbackemail.html.twig'));
                 $useremail->setEmailtype(1);
                $useremail->setStatus(1);
        		$useremail->setReadStatus(0);

                $em->persist($useremail);
                $em->flush();
				if($email)
				{
					$url     = 'https://api.sendgrid.com/';
					$user    = 'businessbid';
					$pass    = '!3zxih1x0';
					$subject = 'Feedback Auto Response BusinessBid';
					$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Admin:feedbacktoadminemail.html.twig',array('email'=>$email,'name'=>$fname,'summary'=>$summary,'feedback'=>$feedback));

					$message = array(
					'api_user'  => $user,
					'api_key'   => $pass,
					'to'        => 'support@businessbid.ae',
					'subject'   => $subject,
					'html'      => $body,
					'text'      => $body,
					'from'      => 'infosupport@businessbid.ae',
					 );


					$request =  $url.'api/mail.send.json';

					// Generate curl request
					$sess = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($sess, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					// Tell curl not to return headers, but do return the response
					curl_setopt($sess, CURLOPT_HEADER, false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					// obtain response
					$response = curl_exec($sess);
					curl_close($sess);
				}

				$url = 'https://api.sendgrid.com/';
				$user = 'businessbid';
				$pass = '!3zxih1x0';
				$subject = 'Feedback Auto Response BusinessBid';
				$body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Others:feedbackemail.html.twig');

				$message = array(
				'api_user'  => $user,
				'api_key'   => $pass,
				'to'        => $email,
				'subject'   => $subject,
				'html'      => $body,
				'text'      => $body,
				'from'      => 'support@businessbid.ae',
				 );


				$request =  $url.'api/mail.send.json';

				// Generate curl request
				$sess = curl_init($request);
				// Tell curl to use HTTP POST
				curl_setopt ($sess, CURLOPT_POST, true);
				// Tell curl that this is the body of the POST
				curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
				// Tell curl not to return headers, but do return the response
				curl_setopt($sess, CURLOPT_HEADER, false);
				curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

				// obtain response
				$response = curl_exec($sess);
				curl_close($sess);

				//print_r($response);exit;
				$session->getFlashBag()->add('success', 'Thank you – your feedback has been submitted successfully!');

				return $this->redirect($this->generateUrl('b_bids_b_bids_feedback'));

			}
		}


		return $this->render('BBidsBBidsHomeBundle:Home:feedback.html.twig', array('form'=>$form->createView()));
	}

	public function trynoworderAction($categoryArray)
	{
		$session = $this->container->get('session');

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('o.id')
			->from('TreeTreeBundle:Order','o')
			->add('orderBy', 'o.id DESC')
			->setFirstResult(0)
			->setMaxResults(1);
		$orderArray = $query->getQuery()->getResult();

		$ordernum = $orderArray[0]['id'];

		$ordermax = $ordernum + 1;

		$ordernumber = "BB". $ordermax;

		$amount = 0;

		$payoption = "TRY IT OPTION";

		$transactionid = "";

		$bankname = "";

		$transactionstatus = 1;

		$author = $session->get('uid');

		$order = new Order();

		$order->setOrdernumber($ordernumber);
		$order->setCreated(new \DateTime());
		$order->setUpdated(new \DateTime());
		$order->setAmount($amount);
		$order->setPayoption($payoption);
		$order->setTransactionstatus(1);
		$order->setAuthor($author);
		$order->setStatus(1);

		$em->persist($order);
		$em->flush();

		$orderid = $order->getId();

		$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessioncategoriesrel')->findBy(array('userid'=>$session->get('uid')));
		foreach($categoryArray as $cat){
			$catid = $cat->getCategoryid();

			$subcategoryArray = $this->getDoctrine()->getRepository('TreeTreeBundle:Categories')->findBy(array('parentid'=>$catid));

			foreach($subcategoryArray as $sub){
				$subid = $sub->getId();

					$vendorsub = $em->getRepository('TreeTreeBundle:Vendorsubcategoriesrel')->findBy(array('vendorid'=>$session->get('uid'), 'subcategoryid'=>$subid));
					$vendor->setStatus(1);

					$em->flush();

					$vendorcat = new Vendorcategoriesrel();

					$vendorcat->setVendorid($session->get('uid'));
					$vendorcat->setCategoryid($catid);
					$vendorcat->setLeadpack(0);
					$vendorcat->setStatus(1);

					$em->persist($vendorcat);
					$em->flush();

					$vendorcatrel = $em->getRepository('TreeTreeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$session->get('uid'), 'subcategoryid'=>$catid));

					$vendorcatrel->setLeadpack(0);
					$vendorcatrel->setStatus(1);

					$em->persist($vendorcatrel);
					$em->flush();

					$oproducts = new Orderproductsrel();

					$products->setOrdernumber($ordernumber);					$products->setCategoryid($catid);
					$products->setLeadpack(0);

					$em->persist($products);
					$em->flush();




			}
		}

		$try = new Usertotryleadtrel();

		$try->setUserid($session->get('uid'));

		$em->persist($try);

		$em->flush();

		$ordertry = new Ordertotryleadsrel();

		$ordertry->setOrderid($orderid);
		$ordertry->setOrdernumber($ordernumber);
		$ordertry->setAuthorid($session->get('uid'));
		$ordertry->setStatus(1);

		$em->persist($ordertry);
		$em->flush();

		$session->getFlashBag()->add('success', 'You have successfully added completed the try it no action. Leads will be credited to your account within 24 hours.');
		return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_home'));

	}

	public function consumerreviewsAction($offset)
	{
	    $session = $this->container->get('session');

	    	$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$start = $offset;

		$reviews =  $em->createQueryBuilder()
			->select('a.bizname, a.contactname, r.review, r.created, r.status, r.rating')
			->from('BBidsBBidsHomeBundle:Reviews', 'r')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = r.vendorid')
			->where('r.authorid = :authorid')
			->add('orderBy', 'r.created DESC')
			->setFirstResult($start)

			 ->setParameter('authorid', $uid);
			//->setParameter('authorid',261);

		$from = $start + 1;

		$to = $start + 10;

		$query = $em->createQueryBuilder()
			->select('count(r.id)')
			->from('BBidsBBidsHomeBundle:Reviews', 'r')
			->where('r.authorid = :authorid')
			->setParameter('authorid', $uid);

		$count = $query->getQuery()->getSingleResult();

		if($to<$count)
			$to = $count;

		return $this->render('BBidsBBidsHomeBundle:User:consumerreviews.html.twig', array('reviews'=>$reviews));
	}

	public function vendorreviewsAction($offset)
	{
		$session = $this->container->get('session');
                 $uid = $session->get('uid');
		$vendorid= $session->get('uid');

		$start = (( $offset * 10) - 10);

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                           ->setParameter('uid', $uid);
                                 //->setParameter('uid', 333);
                        // echo $myquery = $query->getQuery()->getSQL();
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);

                $useridexists = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
			->select('r.review, r.author, r.created, r.rating, e.subj, e.description')
			->from('BBidsBBidsHomeBundle:Reviews', 'r')
			->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'e.id=r.enquiryid')
			->where('r.vendorid = :vendorid')
			->add('orderBy', 'r.created DESC')
			->setFirstResult($start)

			->setMaxResults(10)
			->setParameter('vendorid', $vendorid);

		$reviews = $query->getQuery()->getResult();

		$from = $start + 1;

		$to = $start + 10;

		$query = $em->createQueryBuilder()
			->select('count(r.id)')
			->from('BBidsBBidsHomeBundle:Reviews','r')
			->where('r.vendorid = :vendorid')
			->setParameter('vendorid', $vendorid);

		$count = $query->getQuery()->getSingleScalarResult();

		if($to < $count)
			$count = $to;

		$emailCount = $this->getMessageCount();

		return $this->render('BBidsBBidsHomeBundle:User:vendorreviews.html.twig', array('reviews'=>$reviews, 'from'=>$from, 'to'=>$to, 'count'=>$count,'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));
	}

	public function vendorordersAction($offset)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){

		$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$start = (($offset * 4) - 4);

	 	$query = $em->createQueryBuilder()
				->select('o.id, o.status,o.ordernumber,o.created, o.amount, o.payoption, a.contactname, a.bizname')
				->from('BBidsBBidsHomeBundle:Order', 'o')
				// ->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' ,'substring(o.ordernumber,3) = l.ordernumber')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
				->where('o.author = :author')
				->add('orderBy', 'o.created DESC')
				->setFirstResult($start)
				->setMaxResults(4)
				->setParameter('author', $uid);

		$orders = $query->getQuery()->getResult();
		$from   = $start + 1;
		$to     = $start + 4;

		$query = $em->createQueryBuilder()
				->select('count(o.id)')
				->from('BBidsBBidsHomeBundle:Order', 'o')
				//->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' ,'substring(o.ordernumber,3) = l.ordernumber')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
				->where('o.author = :author')
				->setParameter('author',$uid);

                $count = $query->getQuery()->getSingleScalarResult();

        if($to>$count)
            $to=$count;
		$query = $em->createQueryBuilder()
				->select('count(l.id)')
				->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
				->add('where','l.vendorid = :uid')
				->andWhere('l.status = 1')
				->setParameter('uid', $uid);
		$useridexists = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		//if($offset == 1){
		$fquery = $em->createQueryBuilder()
				->select('f.orderno, f.status, f.created, f.leads, f.duration, f.updated, c.category')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'c.id = f.category')
				->where('f.userid = :userid')
				->setParameter('userid', $uid);

		$freepack = $fquery->getQuery()->getArrayResult();
		//}
		//else { $freepack == 0; }

		$emailCount = $this->getMessageCount();

		return $this->render('BBidsBBidsHomeBundle:User:vendororders.html.twig', array('orders'=>$orders, 'from'=>$from, 'freepack'=>$freepack,  'to'=>$to, 'count'=>$count,'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function consumerviewAction($id,$eid)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){
		$user = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$id));
		$enquiry = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Enquiry')->findBy(array('id'=>$eid));

		$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$id));
		$session = $this->container->get('session');

		$uid = $session->get('uid');
		$em = $this->getDoctrine()->getManager();


		$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
		 $useridexists = $query->getQuery()->getSingleScalarResult();

		 $query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);

		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		return $this->render('BBidsBBidsHomeBundle:User:consumerview.html.twig', array('user'=>$user, 'account'=>$account,'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'enquiry'=>$enquiry));
			}


		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function changepasswordAction(Request $request)
	{
		$user = new User();

		$form = $this->createFormBuilder($user)
			->add('oldpassword', 'password', array('mapped'=>false))
			->add('newpassword', 'repeated', array('type'=>'password', 'invalid_message'=>'The password fields mus match', 'mapped'=>false, 'required'=>true, 'first_options'=>array('label'=>'Password'), 'second_options'=> array('label'=>'Repeat Password')))
			->add('submit', 'submit')
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				$session = $this->container->get('session');

				$uid = $session->get('uid');

				$oldpassword = md5($form['oldpassword']->getData());

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid, 'password'=>$oldpassword));
				foreach($userArray as $u){
					$email = $u->getEmail();
				}

				if(!isset($email)){
					$session = $this->container->get('session');

					$session->getFlashBag()->add('error', 'Your current password did not match');

					return $this->render('BBidsBBidsHomeBundle:User:changepassword.html.twig', array('form'=>$form->createView()));
				}
				else {
					$newpassword = md5($form['newpassword']->getData());

					$em = $this->getDoctrine()->getManager();

					$u = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

					$u->setPassword($newpassword);

					$em->flush();

					$date = new \Datetime();

					$subject = 'Business Bids: Account password changed';
					$body    = $this->renderView('BBidsBBidsHomeBundle:Mailtemplates:changepassword.html.twig', array('date'=>$date));

					// Send email to user
					$this->sendEmail($email,$subject,$body,$uid);

                    $session = $this->container->get('session');

                    $session->getFlashBag()->add('success', 'Password has been changed successfully');

                    return $this->render('BBidsBBidsHomeBundle:User:changepassword.html.twig', array('form'=>$form->createView()));

				}
			}
		}

		return $this->render('BBidsBBidsHomeBundle:User:changepassword.html.twig', array('form'=>$form->createView()));
	}

	public function postreviewAction($authorid, $vendorid, $enquiryid,Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
							->select('count(r.id)')
							->from('BBidsBBidsHomeBundle:Reviews', 'r')
							->add('where','r.vendorid = :vid')
							->andWhere('r.enquiryid = :eid')
							->setParameter('vid', $vendorid)
							->setParameter('eid', $enquiryid);

		$reviewscount = $query->getQuery()->getSingleScalarResult();


		if($reviewscount==0)
		{
			$reviews = new Reviews();

			$form = $this->createFormBuilder($reviews)
			->add('businessknow', 'choice', array('label'=>'How do you know this business?', 'choices'=>array('I hired them'=>'I hired them', 'I spoke with them but did not hire'=>'I spoke with them but did not hire'), 'mapped'=>false, 'expanded'=>false, 'multiple'=>false, 'empty_value'=>'Select a value', 'empty_data'=>null))
			->add('businessrecomond','choice', array('label'=>'Would you recommend this business?', 'choices'=>array('No'=>'No', 'Yes'=>'Yes'), 'mapped'=>false, 'expanded'=>false, 'multiple'=>false))
			->add('ratework', 'hidden', array('mapped'=>false))
			->add('rateservice','hidden', array('mapped'=>false))
			->add('ratemoney', 'hidden', array('mapped'=>false))
			->add('ratebusiness', 'hidden', array('mapped'=>false))
			->add('servicesummary', 'textarea', array('label'=>'What else would you like others to know about your service?', 'mapped'=>false))
			//->add('showname', 'choice', array('label'=>'May we show your name with this review?', 'choices'=>array('1'=>'Show my first name and last initial on the review','2'=>'Please make my review anonymous'), 'mapped'=>false, 'expanded'=>false, 'multiple'=>false))
			->add('notchoose', 'choice', array('label'=>'Why didn’t you hire this company? (can choose multiple boxes)', 'choices'=>array('Although they were good, I hired another business'=>'Although they were good, I hired another business', 'I hired a business outside of Business Bid'=>'I hired a business outside of Business Bid', 'I had to cancel the project'=>'I had to cancel the project', 'They showed poor customer service'=>'They showed poor customer service','They were too expensive'=>'They were too expensive', "They didn't contact me"=>'They didn’t contact me', 'They missed or were late for a scheduled appointment'=>'They missed or were late for a scheduled appointment','Something else (please include in comment below)'=>'Something else (please include in comment below)'), 'mapped'=>false, 'expanded'=>true, 'multiple'=>true))
			->add('serviceexperience', 'textarea', array('mapped'=>false))
			->add('submit','submit',array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){

			$businessknow      = $form['businessknow']->getData();
			$businessrecomond  = $form['businessrecomond']->getData();
			$ratework          = $form['ratework']->getData();
			$rateservice       = $form['rateservice']->getData();
			$ratemoney         = $form['ratemoney']->getData();
			$ratebusiness      = $form['ratebusiness']->getData();
			$servicesummary    = $form['servicesummary']->getData();
			$notchoose         = $form['notchoose']->getData();
			$serviceexperience = $form['serviceexperience']->getData();

			$rating = round((($ratework + $rateservice + $ratemoney + $ratebusiness) / 4), 1);

			$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$authorid));

			foreach($accountArray as $a){
				$name = $a->getContactname();
			}

			$custname = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$authorid));

			foreach($custname as $c){
				$email = $c->getEmail();
			}
			$created = new \Datetime();

			$author = $name;

			$em = $this->getDoctrine()->getManager();

			$reviews = new Reviews();

			$reviews->setVendorid($vendorid);
			$reviews->setServiceexperience($serviceexperience);
			$reviews->setReview($serviceexperience);
			$reviews->setAuthor($author);
			$reviews->setCreated($created);
			$reviews->setModstatus(0);
			$reviews->setStatus(0);
			$reviews->setRating($rating);
			$reviews->setEnquiryid($enquiryid);
			$reviews->setAuthorid($authorid);
			$reviews->setBusinessknow($businessknow);
			$reviews->setBusinessrecomond($businessrecomond);
			$reviews->setServicesummary($servicesummary);
			$reviews->setShowname('noval');
			$reviews->setServiceexperience($serviceexperience);

			$em->persist($reviews);
			$em->flush();


			$ratingArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Ratings')->findBy(array('vendorid'=>$vendorid));

			foreach($ratingArray as $r){
				$vrating = $r->getRating();
				$id = $r->getId();
			}

			if(isset($id)){

				if($vrating!=0)
					$updaterating = round((($rating + $vrating)/2),2);
				else
					$updaterating = $rating;

				$em = $this->getDoctrine()->getManager();

				$ratings = $em->getRepository('BBidsBBidsHomeBundle:Ratings')->find($id);

				$ratings->setRating($updaterating);

				$em->flush();
			}
			else {
				$em = $this->getDoctrine()->getManager();

				$ratings = new Ratings();

				$ratings->setVendorid($vendorid);
				$ratings->setRating($rating);

				$em->persist($ratings);
				$em->flush();
			}

			$adminEmail = 'support@businessbid.ae';
			$subject    = 'Review and Ratings Updates';
			$body       = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Admin:custvendorreviewemail.html.twig');

			$this->sendEmail($adminEmail,$subject,$body,11); //Notification email to admin about customer has written a review on a vendor.

			if($email)
			{
				$email   = $email;
				$subject = 'Review and Ratings Updates';
				$body    = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:customerreviewemail.html.twig', array('name'=>$name));

				$this->sendEmail($email,$subject,$body,$authorid);
			}

			//Also send a notify the vendor
			/*$vendorDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
			$vendorEmail = $vendorDetails->getEmail();
			if($vendorEmail)
			{
				$vendorAccount = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
				$vendorName = $vendorAccount->getContactname();

				$subject = 'Review and Ratings Updates';
				$body    = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_review_notifyemail.html.twig', array('name'=>$vendorName));

				$this->sendEmail($vendorEmail,$subject,$body,$vendorid);
			}*/

			$session = $this->container->get('session');


			if($session->has('uid') && $session->get('pid')==1) {
				$session->getFlashBag()->add('success', 'Review Post successful. Please publish the review from publish section.');
				return $this->redirect($this->generateUrl('bizbids_vendor_reviews_upload'));
			}
			else{
				$session->getFlashBag()->add('success', '<p>Thank you for writing a review- we really appreciate it!</p><p>Your review has been submitted successfully and will be published after moderator approval.</p>');
				return $this->redirect($this->generateUrl('b_bids_b_bids_mapped_vendor',array('eid'=>$enquiryid)));
			}
		}

		return $this->render('BBidsBBidsHomeBundle:User:postreview.html.twig', array('form'=>$form->createView()));
		}
		else
		{
			$session = $this->container->get('session');
			$session->getFlashBag()->add('error', 'Oops! It appears that you have already written a review for this job');
			if($session->has('uid') && $session->get('pid')==1)
				return $this->redirect($this->generateUrl('bizbids_vendor_reviews_upload'));
			else
				return $this->redirect($this->generateUrl('b_bids_b_bids_mapped_vendor',array('eid'=>$enquiryid)));


		}
	}

	public function savealbumAction(Request $request)
	{
		$session = $this->container->get('session');

		$params = $this->getRequest()->request->all();
		// echo '<pre/>';print_r($params);exit;
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['form']['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		$albumname = (isset($params['form']['albumname'])) ? $params['form']['albumname'] : '';

		$albumDescription = (isset($params['form']['description'])) ? $params['form']['description'] : '';

		$em = $this->getDoctrine()->getManager();

		$va = new Vendoralbumrel();

		$exists = 1;

		while($exists = 1){
			$albumid = $this->generateRandom(10);

			$albumArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendoralbumrel')->findBy(array('albumid'=>$albumid));

			foreach($albumArray as $album){
				$id = $album->getId();
			}

			if(!isset($id)){
				$exists = 0;
				break;
			}
		}
		// echo $albumid;
		// echo '<pre/>';print_r($_FILES);exit;
		$loopLength = 0;
		if(isset($_FILES['form']['name']['albumimages'])){
			$loopLength = count($_FILES['form']['name']['albumimages']);
		}
		$updated = new \Datetime();

		$va->setVendorid($vendorid);
		$va->setAlbumid($albumid);
		$va->setAlbumname($albumname);
		$va->setPhotocount($loopLength);
		$va->setDefaultphotopath('nothing');
		$va->setSummary($albumDescription);
		$va->setUpdated($updated);

		$em->persist($va);
		$em->flush();

		$fs = new Filesystem();

		$fs->mkdir('/var/www/html/web/gallery/'.$albumid);

		$fs->chmod('/var/www/html/web/gallery/'.$albumid.'/', 0777, 0000, true);

		$em = $this->getDoctrine()->getManager();

		// $albumimages = $params['form']['albumimages'];
		$allowed = array('png', 'jpg', 'gif','jpeg');
		$upload_dir = '/var/www/html/web/gallery/'.$albumid.'/';

		if(isset($_FILES['form']['name']['albumimages'])){
			for ($i=0; $i < $loopLength; $i++) {
				$loop = $i+1;
				if($_FILES['form']['name']['albumimages'][$i]){
					$extension = pathinfo($_FILES['form']['name']['albumimages'][$i], PATHINFO_EXTENSION);

					if(!in_array(strtolower($extension), $allowed)) {
						$session->getFlashbag()->add('error','File type not allowed');
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));exit;
					}

					$fileName = $albumid.$loop;
					move_uploaded_file($_FILES['form']['tmp_name']['albumimages'][$i], $upload_dir.$fileName);
				}

				$albumPhotoRel = new Albumphotorel();

				$albumPhotoRel->setAlbumid($albumid);
				$albumPhotoRel->setPhotoid($albumid.$i);
				$albumPhotoRel->setPhototag($extension);

				$em->persist($albumPhotoRel);

			} /** for loop ends here **/
		}
		$em->flush();

		$session->getFlashbag()->add('success','Album addition successful');
		if($session->has('uid') && $session->get('pid')==1){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
		}
	}

	function removeAlbumAction($id,$vid)
	{
		$session = $this->container->get('session');
		if($session->has('uid')) {
			$em = $this->getDoctrine()->getManager();

	        $vendorAlbum = $em->getRepository('BBidsBBidsHomeBundle:Vendoralbumrel')->find($id);

	        $albumid = $vendorAlbum->getAlbumid();

	        $em->remove($vendorAlbum);
	        $em->flush();

	        $photoArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Albumphotorel')->findBy(array('albumid'=>$albumid));

	        foreach ($photoArray as $photo) {
	        	$em->remove($photo);
	        }
	        $em->flush();

	        shell_exec("rm -rf /var/www/html/web/gallery/$albumid/");

	        $session->getFlashbag()->add('message','Album delete successful');
	        if($session->get('pid')==1){
				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));
			} else {
				$vendorid = $session->get('uid');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
			}
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function saveproductAction(Request $request)
	{

		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		// echo '<pre/>';print_r($params);exit;
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		/*echo '<pre/>';
		print_r($params);
		exit;*/
		$products = $params['product'];

		$em = $this->getDoctrine()->getManager();
		foreach ($products as $product) {
			if(!empty($product)) {
				$vp = new Vendorproductsrel();

				$vp->setVendorid($vendorid);
				$vp->setProduct($product);
				$em->persist($vp);
			}
		}

		$em->flush();

		$session->getFlashbag()->add('success','Products addition successful');
		if($session->has('uid') && $session->get('pid')==1){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
		}
	}

	public function savecertificationAction(Request $request)
	{
		$session = $this->container->get('session');
		$request = $this->getRequest()->request->all();

		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $request['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		$certificates = $request['certification'];

		$em = $this->getDoctrine()->getManager();
		if(!empty($certificates)) {
			foreach ($certificates as $certificate) {
				$vc = new VendorCertificationrel();
				$vc->setVendorid($vendorid);
				$vc->setCertification($certificate);
				$em->persist($vc);
			}
		}
		/*$allowed = array('png', 'jpg', 'gif','jpeg');
		$upload_dir = "/var/www/html/web/img/certificates/";

		if(isset($_FILES['certificationfile'])){
			$loopLength = count($_FILES['certificationfile']['name']);
			for ($i=0; $i < $loopLength; $i++) {
				if($_FILES['certificationfile']['name'][$i]){
					$extension = pathinfo($_FILES['certificationfile']['name'][$i], PATHINFO_EXTENSION);

					if(!in_array(strtolower($extension), $allowed)) {
						$session->getFlashbag()->add('error','File type not allowed');
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));exit;
					}
					$six_digit_random_number = mt_rand(100000, 999999);
					$fileName = $six_digit_random_number.'.'.$extension;
					move_uploaded_file($_FILES['certificationfile']['tmp_name'][$i], $upload_dir.$fileName);
				}
				if(!empty($certificates[$i])) {
					$vc = new VendorCertificationrel();

					$vc->setVendorid($vendorid);
					$vc->setCertification($certificates[$i]);
					$vc->setFile($fileName);
					$em->persist($vc);
				}
			} // for loop ends here
		}*/
		$em->flush();
		$session->getFlashbag()->add('success','Certificate successfully added');
		if($session->has('uid') && $session->get('pid')==1){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
		}
	}

	public function savecityAction(Request $request)
	{
		$session = $this->container->get('session');
		$em= $this->getDoctrine()->getManager();
		$params = $this->getRequest()->request->all();
		// echo '<pre/>';print_r($params);exit;
		$city = $params['form']['city'];

		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['form']['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		$vendorcityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcityrel')->findBy(array('vendorid'=>$vendorid));

		foreach($vendorcityArray as $vc){
			$id = $vc->getId();
			$cityrel = $em->getRepository('BBidsBBidsHomeBundle:Vendorcityrel')->find($id);
			$em->remove($cityrel);
		}
		$em->flush();

		 $count = count($city);

		for($i=0; $i<$count; $i++) {
			$vcrel = new Vendorcityrel();
			$vcrel->setVendorid($vendorid);
			$vcrel->setCity($city[$i]);
			$em->persist($vcrel);
		}
		$em->flush();

		$session->getFlashbag()->add('success','City Information successfully updated');

		if($session->has('uid') && $session->get('pid')==1){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
		}
	}

	public function saveaccountAction(Request $request)
	{
		$session = $this->container->get('session');
		$params  = $this->getRequest()->request->all();

		$address     = isset($params['form']['address']) ? $params['form']['address']: '';
		$smscode     = $params['form']['mobilecode'];
		$smsphn      = $params['form']['smsphone'];
		$homecode    = $params['form']['homecode'];
		$homephn     = $params['form']['homephone'];
		$bizname     = $params['form']['bizname'];
		$contactname = $params['form']['contactname'];
		$fax         = $params['form']['fax'];
		$description = $params['form']['description'];

		$smsphone  = $smscode.'-'.$smsphn;
		$homephone = $homecode.'-'.$homephn;

		$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

		foreach($userArray as $u){
			$id = $u->getId();
			$sms = $u->getSmsphone();
		}


		// if($sms != $smsphone){
			// $session = $this->container->get('session');
			// $session->getFlashBag()->add('message','SMS verification code has been sent to you mobile number. Your account will be put under inactive state untill you verify your mobile number');
			// // sms verificATION CODE GOES HEREW
			// //DEACTIVATE USER ACCOUNT
		// }

		if($contactname == "") {
			$session->getFlashbag()->add('error','Contact name field should not be left blank' );
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
		}

       	if(!is_numeric($smsphn)) {
			$session->getFlashBag()->add('error','Mobile number field cannot contain strings');
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
		}

		if(strlen($smsphn) != 7) {
			$session->getFlashBag()->add('error','Mobile number field should be of 7 digits');
            return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
		}

		if($homephn != "" || $homecode != "") {
			if(!is_numeric($homephn) || !is_numeric($homecode)) {
				$session->getFlashbag()->add('error','Business number should not contain strings');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
			}
		}

		if($homephn != "" || $homecode != "") {
			if(strlen($homephn) != 7) {
				$session->getFlashBag()->add('error','Business number should be of 7 digits');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
			}
		}
		if($fax != "" ) {
			if(!is_numeric($fax)) {
				$session->getFlashbag()->add('error','Fax number should not contain strings');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
			}
		}

		$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->find($id);

		$account->setAddress($address);
		$account->setSmsphone($smsphone);
		$account->setHomephone($homephone);
		$account->setBizname($bizname);
		$account->setContactname($contactname);
		$account->setFax($fax);
		$account->setdescription($description);

		$em->flush();

		$updated = new  \Datetime();

		$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

		$user->setUpdated($updated);

		$em->flush();
		return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$uid)));
	}

	public function vendorupdateAction(Request $request, $vendorid)
	{

		$session = $this->container->get('session');

		if($session->has('uid')){

		$uid = $session->get('uid');
		$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$vendorid));

		$vendoralbumArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendoralbumrel')->findBy(array('vendorid'=>$vendorid));

		$vendorcityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcityrel')->findBy(array('vendorid'=>$vendorid));

        $logoObject = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorlogorel')->findOneBy(array('vendorid'=>$vendorid));

		$vendorcertificationArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcertificationrel')->findBy(array('vendorid'=>$vendorid));
		$vendorproductsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorproductsrel')->findBy(array('vendorid'=>$vendorid));

		$cityList 	= $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
		$expLicense = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorlicenserel')->findBy(array('vendorid'=>$vendorid));

		$cityArray = array();

		$em = $this->getDoctrine()->getManager();

		$catquery = $em->createQueryBuilder()
			->select('c.category')
			->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'vc')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with',  'vc.categoryid = c.id')
			->where('vc.vendorid = :vendorid')
			->setParameter('vendorid', $uid);

		$maincategory = $catquery->getQuery()->getArrayResult();

		foreach($cityList as $c){
			$city   = $c->getCity();
			$cityid = $c->getId();
			if(!empty($city))
				$cityArray[$cityid] = $city;
		}

		foreach($accountArray as $a){
			$address     = $a->getAddress();
			$smsphone    = $a->getSmsphone();
			$homephone   = $a->getHomephone();
			$bizname     = $a->getBizname();
			$contactname = $a->getContactname();
			$fax         = $a->getFax();
			$description = $a->getDescription();

		}

	 //echo $smsphone;exit;
		if(!empty($smsphone)) {
			$sms=explode('-',$smsphone);
			$smscode=$sms[0];
			$smsphone=$sms[1];
		}
		if(!empty($homephone)) {
			$home=explode('-',$homephone);
			$homecode=$home[0];
			$homephone=$home[1];
		}
		$cities2 = array();
		foreach($vendorcityArray as $vc){
			$city2 = $vc->getCity();
		 	$cities2[$city2] = $city2;
		}

		$account = new Account();

		$profileform = $this->createFormBuilder($account)
			->setAction($this->generateUrl('b_bids_b_bids_vendor_save_account'))
			->add('address','textarea', array('data'=>$address,'required'=>false))
			->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'data'=>$smscode,'label'=>'Mobile Number','empty_value'=>'Area Code', 'mapped'=>FALSE))
			->add('smsphone','text', array('data'=>$smsphone))
			->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06'),'data'=>$homecode,'label'=>'Business Number','empty_value'=>'Area Code', 'mapped'=>FALSE))
			->add('homephone', 'text', array('data'=>$homephone, 'required'=>false))
			->add('bizname', 'text', array('label'=>'Business Name','data'=>$bizname))
			->add('contactname', 'text', array('label'=>'Contact Name' ,'data'=>$contactname))
			->add('fax', 'text', array('data'=>$fax, 'required'=>false,'label'=>'Fax Number'))
			->add('description', 'textarea', array('data'=>$description,'required'=>false ,'attr'=>array('placeholder'=>'Enter your description')))
			->add('save', 'submit')
			->getForm();

		$albumform = $this->createFormBuilder()
			->setAction($this->generateUrl('b_bids_b_bids_vendor_save_album'))
			->add('albumname','text',array('label'=>'Album Name','mapped'=>false))
			->add('description', 'textarea', array('required'=>false ,'attr'=>array('placeholder'=>'Enter album description')))
			->add('albumimages','file',array('label'=>'','mapped'=>false,'attr'=>array("multiple" => "multiple")))
			->add('save', 'submit')
			->getForm();

		$cityform = $this->createFormBuilder($account)
			->setAction($this->generateUrl('b_bids_b_bids_vendor_save_city'))
			->add('city','choice', array('choices'=>$cityArray, 'multiple'=>true, 'expanded'=>true, 'data'=>$cities2,'mapped'=>false, 'required'=>false))
			->add('save', 'submit')
			->getForm();

		$logoform = $this->createFormBuilder($account)
			//->setAction($this->generateUrl('b_bids_b_bids_vendor_save_logo'))
			->add('image', 'file', array('mapped'=>false, 'required'=>false))
			->add('Upload','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$certificationform = $this->createFormBuilder($account)
			->setAction($this->generateUrl('b_bids_b_bids_vendor_save_certification'))
			->add('certification', 'text', array('mapped'=>false, 'required'=>false))
			->add('save', 'submit')
			->getForm();

		$productform = $this->createFormBuilder($account)
			->setAction($this->generateUrl('b_bids_b_bids_vendor_save_product'))
			->add('product','text', array('mapped'=>false, 'required'=>false))
			->add('save', 'submit')
			->getForm();

		/*if($expLicense) {
			$expirydate = $expLicense
		}*/
		$licenseform = $this->createFormBuilder($account)
			->setAction($this->generateUrl('b_bids_b_bids_vendorlicense'))
			->add('name', 'text', array('label' =>'License Name','mapped'=>false))
			->add('expirydate','text', array('label' =>'Expiry Date','mapped'=>false))
			->add('licensefile','file', array('label' =>'License File','mapped'=>false))
			->add('save', 'submit')
			->getForm();


		$logoform->handleRequest($request);

		$showcat = $em->createQueryBuilder()
			->select('c.category,c.id')
			->from('BBidsBBidsHomeBundle:Vendorservices', 'c')
			->where('c.vendorid = :vendorid')
			->setParameter('vendorid', $vendorid);

		$dispcat = $showcat->getQuery()->getArrayResult();

		if($request->isMethod('POST')) {
			//$newfilename = "hifilename";
			$file = $logoform['image']->getData();
			// print_r($_FILES["form"]);exit;
			$filename = $_FILES["form"]['tmp_name']['image'];
			$imageTypeArray = array(0=>'UNKNOWN',1=>'GIF',2=>'JPEG',3=>'PNG',4=>'SWF',5=>'PSD',6=>'BMP',7=>'TIFF_II',8=>'TIFF_MM',9=>'JPC',10=>'JP2',11=>'JPX',12=>'JB2',13=>'SWC',14=>'IFF',15=>'WBMP',16=>'XBM',17=>'ICO',18=>'COUNT');
    		list($width, $height, $type, $attr) = getimagesize($filename);
			$profileimage = $vendorid.'-logo.'.strtolower($imageTypeArray[$type]);//$_FILES["form"]["name"]['image'];
			$dir = "/var/www/html/web/logo/";
			$logoform['image']->getData()->move($dir, $profileimage);

			$em = $this->getDoctrine()->getManager();
			/*$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$vendorid));

			foreach($userArray as $u){
				$id = $u->getId();
			}*/
			/*$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
			$account->setLogopath($profileimage);
            $em->flush();*/
            $vendorLogoObject = $em->getRepository('BBidsBBidsHomeBundle:Vendorlogorel')->findOneBy(array('vendorid'=>$vendorid));
            if(is_null($vendorLogoObject))
			{
				$newVendorlogo = new Vendorlogorel();
				$newVendorlogo->setVendorid($vendorid);
				$newVendorlogo->setFileName($profileimage);

				$em->persist($newVendorlogo);
				$em->flush();
			} else {
				$vendorLogoObject->setFileName($profileimage);

				$em->persist($vendorLogoObject);
				$em->flush();
			}
			$session->getFlashbag()->add('success','Logo update successful');
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
		}

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $vendorid);

                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $vendorid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		return $this->render('BBidsBBidsHomeBundle:User:vendorupdate.html.twig', array('expLicense'=>$expLicense,'bizname'=>$bizname, 'useridexists'=>$useridexists,'maincat'=>$maincategory, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'vendorid'=>$vendorid, 'bizname'=>$bizname,  'logoform'=>$logoform->createView(),'profileform'=>$profileform->createView(), 'albumform'=>$albumform->createView(), 'cityform'=>$cityform->createView(), 'certificationform'=>$certificationform->createView(), 'productform'=>$productform->createView(), 'licenseform'=>$licenseform->createView(), 'albums'=>$vendoralbumArray, 'certifications'=>$vendorcertificationArray, 'products'=>$vendorproductsArray, 'accountArray'=>$accountArray,'displaycat'=>$dispcat,'logoObject'=>$logoObject));
		}

		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function forgotpasswordautoAction($email)
	{
		$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email));

		foreach($userArray as $u){
			$userid = $u->getId();
		}

		$session = $this->container->get('session');
		$uid     = $session->get('uid');
		$username = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));

		foreach($username as $n){
			$uname = $n->getContactname();
		}

		$userhash = $this->generateHash(16);

		$em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($userid);
        $user->setUserhash($userhash);
        $em->flush();

		$subject = 'Password Reset E-mail';
		$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Others:forgotpasswordemail.html.twig', array('userid'=>$userid, 'userhash'=>$userhash, 'uname'=>$uname));

		$this->sendEmail($email,$subject,$body,$userid);

		$session->getFlashBag()->add('message','An email has been sent to you with forgot password details');

		return $this->render('::basesuccess.html.twig');
	}

	public function authenticateAction(Request $request, $email, $enquiryid)
	{
		$user = new User();

		$form = $this->createFormBuilder($user)
			->add('password', 'password', array('label'=>'Enter your password'))
			->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){

				$password = md5($form['password']->getData());
				$user     = new User();

                $userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email, 'password'=>$password));
                foreach($userArray as $user){
                        $uid = $user->getId();
                }

                if(isset($uid)) {

                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

                    foreach($accountArray as $account){
						$pid  = $account->getProfileid();
						$name = $account->getContactname();
            		}

					$em = $this->getDoctrine()->getManager();
					$enquiry = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);
					$enquiry->setStatus(1);
					$em->flush();

                    $session = $this->container->get('session');

                    $session->set('uid',$uid);
                    $session->set('email',$email);
                    $session->set('pid', $pid);
                    $session->set('name', $name);

                    /*return $this->redirect($this->generateUrl('b_bids_b_bids_enquiry_processing'));*/
                    return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
               	}
				else {
	                $session = $this->container->get('session');
	                $session->getFlashbag()->add('error','Email and password did not match');
	                return $this->render('BBidsBBidsHomeBundle:User:authenticate.html.twig',array('form'=>$form->createView(), 'useremail'=>$email));
                }

			}
			else {
				return $this->render('BBidsBBidsHomeBundle:User:authenticate.html.twig', array('form'=>$form->createView(), 'useremail'=>$email));
			}
		}

		return $this->render('BBidsBBidsHomeBundle:User:authenticate.html.twig', array('form'=>$form->createView(), 'useremail'=>$email));
	}

	public function forgotpasswordAction(Request $request)
	{
		$user = new User();

		$form = $this->createFormBuilder($user)
			->add('email', 'email', array('label'=>'Enter your email'))
			->add('submit','submit' , array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')) {
            if($form->isValid()) {
				$email = $form['email']->getData();

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email));

				foreach($userArray as $u){
					$id = $u->getId();
				}

				if(isset($id)){
					$user = new User();

                    $userhash = $this->generateHash(16);

                    $em = $this->getDoctrine()->getManager();
                    $user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($id);

                    $user->setMobilecode('');
                    $user->setStatus(1);

                    $user->setUserhash($userhash);
                    $em->flush();

                	$username = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$id));
					//print_r($username);exit;
	                foreach($username as $n){
                        $name = $n->getContactname();
	                }

					$subject = 'BusinessBid Password Reset Request';
					$body = $this->renderView('BBidsBBidsHomeBundle:Home:forgotpassword.html.twig', array('userid'=>$id, 'userhash'=>$userhash,'name'=>$name));

					$this->sendEmail($email,$subject,$body,$id);

					$session = $this->container->get('session');

					$session->getFlashBag()->add('success', 'Account activation link has been sent to your email. Please check your email id for furhter instructions');
					return $this->render('::basesuccess.html.twig');
				} else {
					$session = $this->container->get('session');

                    $session->getFlashBag()->add('error', 'Sorry!! You have entered an incorrect email. This email does not exist within our database. Please enter a valid email.');
                   	return $this->render('BBidsBBidsHomeBundle:User:forgotpassword.html.twig', array('form'=>$form->createView()));
				}
            }
			else
				return $this->render('BBidsBBidsHomeBundle:User:forgotpassword.html.twig', array('form'=>$form->createView()));
            }

		return $this->render('BBidsBBidsHomeBundle:User:forgotpassword.html.twig', array('form'=>$form->createView()));
	}

	public function purchaseLeadsAction()
	{
		$session = $this->container->get('session');

		$uid = $session->get('uid');
		if($session->has('uid')){
			$em = $this->getDoctrine()->getManager();
			$query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);
			 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
			 $query = $em->createQueryBuilder()
	                                ->select('count(l.id)')
	                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
	                                ->add('where','l.vendorid = :uid')
	                                ->andWhere('l.status = 1')
								->setParameter('uid', $uid);
		 	$useridexists = $query->getQuery()->getSingleScalarResult();

			$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

			$catArray = $catPrice = array();
			$categoryOptions = $_SESSION['leads'] = $_SESSION['leads']['avCat'] = ''; //Initialization for next plan page
			$purchaseID = rand ( 10000 , 99999 );

			foreach($categoryArray as $c){
				$_SESSION['leads']['avCat'][] = $c->getId();
				$catname = $c->getCategory();
				$price = $c->getPrice();
				$catArray[] = $catname;
				$catPrice[] = (int)$price;
				$categoryOptions .= '<option value="'.$catname.'">'.$catname.'</option>';
			}
			$emailCount = $this->getMessageCount();
			return $this->render('BBidsBBidsHomeBundle:User:purchase_leads.html.twig',array('useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists,'catArray'=>$catArray,'categoryOptions'=>$categoryOptions,'catPrice'=>$catPrice,'purchaseID'=>$purchaseID,'emailCount'=>$emailCount));
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	function storePurchaseLeadsAction(Request $request) //Ajax function to update the categories
	{

		$session    = $this->container->get('session');
		$uid        = $session->get('uid');
		if($session->has('uid')){
			if($request->isMethod('POST')){
				$lead       = $this->get('request')->request->all();
				$avCatKey   = $lead['position'];
				$reqType    = $lead['reqType'];
				$purchaseID = $lead['purchaseID'];

				if($reqType == 'add') {
					$selectedCat = $_SESSION['leads']['avCat'][$avCatKey];
					$_SESSION['leads'][$purchaseID]['selIDs'][] = $selectedCat;
				//echo '<pre/>';print_r($_SESSION['leads'][$purchaseID]);exit;
					return new response('Added successfully');
				} else {
					$selectedCat = $_SESSION['leads']['avCat'][$avCatKey];
					if (($key = array_search($selectedCat, $_SESSION['leads'][$purchaseID]['selIDs'])) !== false) unset($_SESSION['leads'][$purchaseID]['selIDs'][$key]);
					return new response('Removed successfully');
				}
			}
		} else {
			return new response('It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
		}
	}

	public function leadsPlanAction($purchaseID)
	{
		//echo $purchaseID;exit;
		$session = $this->container->get('session');

		$uid     = $session->get('uid');
		$selectedCats = array();

		if($session->has('uid'))
		{
			$selectedCats = $_SESSION['leads'][$purchaseID]['selIDs'];

			if(empty($selectedCats))
			{
				$session->getFlashBag()->add('error','Unable to process your request. Please try again after some time.');
            	return $this->redirect($this->generateUrl('b_bids_b_bids_vendororders'));
			}
			$_SESSION['leads'][$purchaseID]['plan'] = ''; //Initialization for next pymt page

			$em = $this->getDoctrine()->getManager();

			$connection = $em->getConnection();
			$catQuery = $connection->prepare("SELECT c.id,c.category,FLOOR(c.price) as bPrice FROM bbids_categories c WHERE c.id IN (".implode(',',$selectedCats).")");
            $catQuery->execute();
            $selectedCatsArray = $catQuery->fetchAll();

            // Delete the table records if already exist for purchase id
            $query = $connection->prepare('DELETE FROM bbids_purchaseleads WHERE purchaseid = '.$purchaseID);
			$query->execute();

            foreach($selectedCatsArray as $key => $value) {
            	$catPrice = 0;
            	$catPrice = $value['bPrice'];

				$silverPrice    = (($catPrice)-((5*$catPrice)/100));
				$goldPrice      = (($catPrice)-((10*$catPrice)/100));
				$platiniumPrice = (($catPrice)-((15*$catPrice)/100));

				$selectedCatsArray[$key]['sPrice'] = $silverPrice;
				$selectedCatsArray[$key]['gPrice'] = $goldPrice;
				$selectedCatsArray[$key]['pPrice'] = $platiniumPrice;

				$allPackagePrice = $catPrice.' - '.$silverPrice.' - '.$goldPrice.' - '.$platiniumPrice;

				$purchaseLeads = new Purchaseleads();
				$created       = new \DateTime();
				$purchaseLeads->setPurchaseid($purchaseID);
				$purchaseLeads->setUserid($uid);
				$purchaseLeads->setCategoryid($value['id']);
				$purchaseLeads->setCategory($value['category']);
				$purchaseLeads->setPrice($allPackagePrice);
				$purchaseLeads->setCreated($created);
				$purchaseLeads->setStatus(1);

				$em->persist($purchaseLeads);
				$em->flush();
            }

            $totalSelectedCats = count($selectedCatsArray);

            // echo '<pre/>';print_r($_SESSION);exit;

			$account    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid));

            $otcStatus  = $account->getOtcStatus();
            $registerFee = 600; $firstCategoryID = '';
            $firstCategoryID = $_SESSION['leads'][$purchaseID]['selIDs'][0];
            if($otcStatus == 0 || $otcStatus == '') {
				$getRegisterFee = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Registrationfeecatrel')->findOneBy(array('categoryid'=>$firstCategoryID,'status'=>1));
				$registerFee = (empty($getRegisterFee)) ? 600: $getRegisterFee->getPrice();
			} else $registerFee = 600;

			$em = $this->getDoctrine()->getManager();
			$query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
			$query = $em->createQueryBuilder()
                    ->select('count(l.id)')
                    ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                    ->add('where','l.vendorid = :uid')
                    ->andWhere('l.status = 1')
					->setParameter('uid', $uid);
			$useridexists = $query->getQuery()->getSingleScalarResult();


			return $this->render('BBidsBBidsHomeBundle:User:leads_plan.html.twig',array('useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists,'selectedCatsArray'=>$selectedCatsArray,'totalSelectedCats'=>$totalSelectedCats,'purchaseID'=>$purchaseID,'otcStatus'=>$otcStatus,'registerFee'=>$registerFee));
		}
		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	function storeLeadsPlanAction(Request $request)
	{
		$session    = $this->container->get('session');
		$uid        = $session->get('uid');
		$em = $this->getDoctrine()->getManager();
		if($session->has('uid')){
			if($request->isMethod('POST')) {
				$packageInfo = $this->get('request')->request->all();
				$avCat       = $packageInfo['category'];
				$catKey      = $packageInfo['key'];
				$packageType = $packageInfo['packageType'];
				$purchaseID  = $packageInfo['purchaseID'];
				// echo '<pre/>';print_r($packageInfo);exit;
				$_SESSION['leads'][$purchaseID]['plan'][$catKey]['type'] = $packageType;
				$_SESSION['leads'][$purchaseID]['plan'][$catKey]['name'] = $avCat;

				$plan = 'Bronze';
				if($packageType==1) {
			        $plan = 'Bronze';
			    } else if($packageType==2) {
			        $plan = 'Silver';
			    } else if($packageType==3) {
			        $plan = 'Gold';
			    } else if($packageType==4) {
			        $plan = 'Platinum';
			    }

				$purchaseLeadsUpdate = $em->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findOneBy(array('purchaseid'=>$purchaseID,'category'=>$avCat,'status'=>1));
				if($purchaseLeadsUpdate) {
					$purchaseLeadsUpdate->setPlan($plan);

					$em->persist($purchaseLeadsUpdate);
					$em->flush();
					return new response('Update successfull');
				}
				else {
					return new response('Update unsuccessful');
				}
			}
		} else {
			return new response('It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
		}
	}

	function leadsPaymentFormAction(Request $request,$purchaseID)
	{
		$session = $this->container->get('session');
		$uid     = $session->get('uid');
		//echo $purchaseID; die;
		if($session->has('uid')){
			$em = $this->getDoctrine()->getManager();
			$getPurchasingLeads = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findBy(array('purchaseid'=>$purchaseID,'status'=>1));

			if(empty($getPurchasingLeads))
			{
				$session->getFlashBag()->add('error','Unable to process your request. Please try again after some time.');
            	return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_home'));
			}
			// echo '<pre/>';print_r($getPurchasingLeads);exit;

			$leadsPerPack = array('Bronze'=>10, 'Silver'=>25, 'Gold'=>50, 'Platinum'=>100);
			$leadsPack    = array('Bronze'=>0, 'Silver'=>1, 'Gold'=>2, 'Platinum'=>3);

			$account    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid));

			$otcStatus      = $account->getOtcStatus();
			$vendorAddress  = $account->getAddress();
			$vendorCompName = $account->getBizname();
			$vendorcontactname = $account->getcontactname();

			$userArray    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$uid));
			$userEmail      = $userArray->getEmail();

			$query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
			$query = $em->createQueryBuilder()
                    ->select('count(l.id)')
                    ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                    ->add('where','l.vendorid = :uid')
                    ->andWhere('l.status = 1')
					->setParameter('uid', $uid);
			$useridexists = $query->getQuery()->getSingleScalarResult();

			$form = $this->createFormBuilder()
			    ->add('paytype','hidden', array('data'=>'cc'))
				->add('ccNumber','text',array('label'=>'Card Number','attr' => array('placeholder' => 'Enter Card Number', "autocomplete" => "off",'min' => 13, 'max' => 19)))
				->add('ccName','text',array('label'=>'Card Holder\'s Name','attr' => array('placeholder' => 'Enter Card Holder\'s Name', "autocomplete" => "off")))
				->add('ccExpDate', 'date', array(
				 'label' => 'Expiry Date',
				 'widget' => 'choice',
				 'empty_value' => array('year' => 'Year', 'month' => 'Month', 'day' => 'Day'),
				 'format' => 'dd-MM-yyyy',
				 'input' => 'string',
				 'data' => date('Y-m-d'),
				 'years' => range(date('Y'), date('Y') + 10)
				))
				->add('ccCVV','text',array('label'=>'Card Verification Code','attr' => array('placeholder' => 'Enter CVV Number', "autocomplete" => "off", 'min' => 2, 'max' => 3)))
		        ->getForm();
            // $form->handleRequest($request);
	        $cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
			$cityList = array();
			foreach($cityArray as $city){
				$cityid = $city->getId();
				$cityname = $city->getCity();
				$cityList[$cityname] = $cityname;
			}
		    $checkForm = $this->createFormBuilder()
		    		 	->add('paytype','hidden', array('data'=>'check'))
		    		 	->add('address','text', array('label'=>'Address(First Line)','required'=>TRUE,'data'=>$vendorAddress,'attr' => array('placeholder' => 'Enter pick up address(First Line).', "autocomplete" => "off")))
		    		 	->add('addresssecond','text', array('label'=>'Address(Second Line)','attr' => array('placeholder' => 'Enter pick up address(Second Line).', "autocomplete" => "off")))
		    		 	->add('city','choice', array('required'=>TRUE,'label'=>'City ','required'=>false,'choices'=>$cityList, 'empty_value'=>'Choose a city', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
						->add('contactnumber','text',array('required'=>TRUE,'label'=>'Contact Number','attr' => array('placeholder' => 'Enter contact number', "autocomplete" => "off",'size'=>10)))
						->add('contactperson','text',array('required'=>TRUE,'label'=>'Contact Person','attr' => array('placeholder' => 'Enter person name', "autocomplete" => "off")))
						->add('bankname','text',array('required'=>TRUE,'label'=>'Bank Name','attr' => array('placeholder' => 'Enter bank name', "autocomplete" => "off")))
						->add('checknumber','text',array('required'=>TRUE,'label'=>'Check Number','attr' => array('placeholder' => 'Enter check number', "autocomplete" => "off")))
						->add('Submit','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			    		->getForm();
		    $cashForm = $this->createFormBuilder()
		    		 	->add('paytype','hidden', array('data'=>'cash'))
		    		 	->add('address','text', array('label'=>'Address(First Line)','required'=>TRUE,'data'=>$vendorAddress,'attr' => array('placeholder' => 'Enter pick up address(First Line).', "autocomplete" => "off")))
		    		 	->add('addresssecond','text', array('label'=>'Address(Second Line)','attr' => array('placeholder' => 'Enter pick up address(Second Line).', "autocomplete" => "off")))
		    		 	->add('city','choice', array('required'=>TRUE,'label'=>'City ','required'=>false,'choices'=>$cityList, 'empty_value'=>'Choose a city', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
						->add('contactnumber','text',array('required'=>TRUE,'label'=>'Contact Number','attr' => array('placeholder' => 'Enter contact number', "autocomplete" => "off",'size'=>10)))
						->add('contactperson','text',array('required'=>TRUE,'label'=>'Contact Person','attr' => array('placeholder' => 'Enter person name', "autocomplete" => "off")))
						->add('Submit','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			    		->getForm();

			$paypalForm = $this->createFormBuilder()
		    		 	->add('paytype','hidden', array('data'=>'paypal'))
		    		 	->getForm();
		    $firstCategoryID = $getPurchasingLeads[0]->getCategoryid();
		    if($otcStatus == 0 || $otcStatus == '') {
				$getRegisterFee = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Registrationfeecatrel')->findOneBy(array('categoryid'=>$firstCategoryID,'status'=>1));
	    		$registerFee = (empty($getRegisterFee)) ? 0: $getRegisterFee->getPrice();
			} else $registerFee = 0;
			mysql_connect("localhost","bizbids","eY3@gV*094(Wl1$");
mysql_select_db("bizbids");


			// $token=$this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Token')->findOneBy(array('id'=>$uid));
			// print_r($token);
				$grandTotal = 0;
					foreach($getPurchasingLeads as $pLeads)
					{
						$priceArray = explode("-",$pLeads->getPrice());
						$plan       = $pLeads->getPlan();
						$selPlan    = $leadsPack[$plan];
						if($plan != 'Bronze') {
							$grandTotal += $priceArray[$selPlan] * $leadsPerPack[$plan];
						} else {
							$grandTotal += $priceArray[0] * $leadsPerPack[$plan];
						}
					}
		              $total1=$grandTotal + $registerFee;
					 if($uid!=""){
		$s=mysql_query("select * from bbids_token where id ='{$uid}'");
		if(mysql_num_rows($s)>0){
			
			mysql_query("update   bbids_token set amount = '{$total1}' where id='{$uid}'");
	
	}else{
		//echo "jndjn";
		mysql_query("INSERT INTO `bbids_token` (`id`, `amount`) VALUES ('$uid', '$total1')");
	}
	}
					$s=mysql_query("select * from bbids_token where id ='{$uid}'");
					$tok=mysql_fetch_array($s);
					$token=$tok['token'];
					//$total1=$tok['amount'];					
					  //setcookie('totalnew',$total1);
					setcookie('email',$userEmail);
					$uname     = $session->get('name');
				 setcookie('username',$uname);
				 setcookie('orderid',$purchaseID);
				 setcookie('uid',$uid);
					//print_r($_SESSION);
					//echo "<br>";
	        //$customername=$_SESSION['_sf2_attributes']['name'];
			//print_r($getPurchasingLeads);
			
		    //$_SESSION['total']= number_format($grandTotal+$registerFee, 2, '.', '')*100;
			//$_SESSION['finaltotal']= number_format($grandTotal+$registerFee, 2, '.', '');
			//$total1=$grandTotal+$registerFee;
			//echo "<pre>";
			//print_r($_SESSION);
		    $rendTwig = $this->render('BBidsBBidsHomeBundle:User:leads_paymentform.html.twig',array('useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists,'purchasingLeads'=>$getPurchasingLeads,'leadsPerPack' => $leadsPerPack,'token'=>$token,'leadsPack'=>$leadsPack,'otcStatus' => $otcStatus,'purchaseID'=>$purchaseID,'usermail'=>$userEmail,'vendorAddress'=>$vendorAddress, 'vendorCompName'=>$vendorCompName,'custome'=>$uname ,'registerFee'=>$registerFee,'form' => $form->createView(),'checkForm'=>$checkForm->createView(),'paypalForm'=>$paypalForm->createView(),'cashForm'=>$cashForm->createView()));
	        $form->handleRequest($request);
         	if($request->isMethod('POST')) {
				//if($form->isValid()) {
					$data = $request->request->all();
					$grandTotal = 0; $firstCategoryID = '';
					$firstCategoryID = $getPurchasingLeads[0]->getCategoryid();
					foreach($getPurchasingLeads as $pLeads)
					{
						$priceArray = explode("-",$pLeads->getPrice());
						$plan       = $pLeads->getPlan();
						$selPlan    = $leadsPack[$plan];
						if($plan != 'Bronze') {
							$grandTotal += $priceArray[$selPlan] * $leadsPerPack[$plan];
						} else {
							$grandTotal += $priceArray[0] * $leadsPerPack[$plan];
						}
					}

					if($otcStatus == 0 || $otcStatus == '') {
						$getRegisterFee = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Registrationfeecatrel')->findOneBy(array('categoryid'=>$firstCategoryID,'status'=>1));
						$grandTotal += (empty($getRegisterFee)) ? 600: $getRegisterFee->getPrice();
					}

					$formData = $data['form'];
					// echo '<pre/>';print_r($formData);exit;
					$paytype  = $formData['paytype'];
					if($paytype == 'check') {
						$address       = $formData['address'];
						$addresssecond = $formData['addresssecond'];
						$city          = $formData['city'];
						$contactnumber = $formData['contactnumber'];
						$contactperson = $formData['contactperson'];
						$bankname      = $formData['bankname'];
						$checknumber   = $formData['checknumber'];
						if($address == "") {
							$session->getFlashbag()->add('error','Please Enter Pick Up Address' );
							return $rendTwig;
						} else if($city == '') {
							$session->getFlashbag()->add('error','Please Enter City' );
							return $rendTwig;
						} else if($contactperson == '') {
							$session->getFlashbag()->add('error','Please Contact Person Name' );
							return $rendTwig;
						} else if($bankname == '') {
							$session->getFlashbag()->add('error','Please Enter Bank Name' );
							return $rendTwig;
						} else if($checknumber == '') {
							$session->getFlashbag()->add('error','Please Enter Check Number' );
							return $rendTwig;
						} else if($contactnumber == '') {
							$session->getFlashbag()->add('error','Please Enter Contact Number.' );
							return $rendTwig;
						}
						$checkOrderNumber = $this->leadCheckPayments($formData, $purchaseID, $grandTotal,$vendorCompName);
						if($checkOrderNumber) {

							$subject = 'Businessbid Check Pickup Request Successful';
							$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:paymentsuccessemail.html.twig', array('name'=>$vendorcontactname,'OrderNumber'=>$checkOrderNumber));
							$this->sendEmail($userEmail,$subject,$body,$uid);

							return $this->redirect($this->generateUrl('b_bids_b_bids_pymt_success',array('ordernumber'=>$checkOrderNumber)));
						}
						exit('User exit');
					}else if($paytype == 'cash') {
                        $address       = $formData['address'];
                        $addresssecond = $formData['addresssecond'];
                        $city          = $formData['city'];
                        $contactnumber = $formData['contactnumber'];
                        $contactperson = $formData['contactperson'];
                        if($address == "") {
                            $session->getFlashbag()->add('error','Please Enter Pick Up Address' );
                            return $rendTwig;
                        } else if($city == '') {
                            $session->getFlashbag()->add('error','Please Enter City' );
                            return $rendTwig;
                        } else if($contactperson == '') {
                            $session->getFlashbag()->add('error','Please Contact Person Name' );
                            return $rendTwig;
                        } else if($contactnumber == '') {
                            $session->getFlashbag()->add('error','Please Enter Contact Number.' );
                            return $rendTwig;
                        }
                        $cashOrderNumber = $this->leadCashPayments($formData, $purchaseID, $grandTotal,$vendorCompName);
                        if($cashOrderNumber) {

                            $subject = 'Businessbid Cash Pickup Request Successful';
                            $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:paymentsuccessemail.html.twig', array('name'=>$vendorcontactname,'OrderNumber'=>$cashOrderNumber));

                            $this->sendEmail($userEmail,$subject,$body,$uid);

                            return $this->redirect($this->generateUrl('b_bids_b_bids_pymt_success',array('ordernumber'=>$cashOrderNumber)));
                        }
                        exit('User exit');
					} else if ($paytype == 'paypal') {
						return $this->redirect($this->generateUrl('b_bids_b_bids_pgateway_paypal',array('purchaseID'=>$purchaseID,'actiontype'=>'process')));
					} else if ($paytype == 'cc') {
						$ccNumber  = $formData['ccNumber'];
						$ccName    = $formData['ccName'];
						$ccExpDate = $formData['ccExpDate'];
						$ccCVV     = $formData['ccCVV'];
						if($ccNumber == "") {
							$session->getFlashbag()->add('error','Please Enter Card Number' );
							return $rendTwig;
						} else if(!is_numeric($ccNumber)) {
							$session->getFlashbag()->add('error','Please Enter Card Number Numbers Only' );
							return $rendTwig;
						} else if($ccName == '') {
							$session->getFlashbag()->add('error','Please Enter Card Holder Name' );
							return $rendTwig;
						} else if($ccExpDate == '') {
							$session->getFlashbag()->add('error','Please Choose Card Expiry Date' );
							return $rendTwig;
						} else if($ccCVV == '') {
							$session->getFlashbag()->add('error','Please Enter Card CVV Number' );
							return $rendTwig;
						} else if(strlen($ccCVV) != 3) {
							$session->getFlashbag()->add('error','Please Enter 3 Digits Card CVV Number' );
							return $rendTwig;
						}


						//$paymentFlag = $this->paymentGatewayCall($formData, $purchaseID, $grandTotal,'Testss');
						$paymentFlag = $this->paymentGatewayCall($formData, $purchaseID, $grandTotal,'Testsss');

						if($paymentFlag)
						{
							$query = $em->createQueryBuilder()
										->select('o.ordernumber')
										->from('BBidsBBidsHomeBundle:Order', 'o')
										->where('o.transactionid = :transId')
										->setParameter('transId', $paymentFlag);


							$orderNumber = $query->getQuery()->getResult();

							$orderNumbertrans=$orderNumber[0]['ordernumber'];


							$subject = 'BusinessBid Card Payment Successful';
							$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:paymentsuccessemail.html.twig', array('name'=>$vendorcontactname,'OrderNumber'=>$orderNumbertrans));

							$this->sendEmail($userEmail,$subject,$body,$uid);

							return $this->redirect($this->generateUrl('b_bids_b_bids_pymt_success',array('ordernumber'=>$orderNumbertrans)));

							//$session->getFlashBag()->add('success','Purchase Leads successfully');
	                        //return $this->redirect($this->generateUrl('b_bids_b_bids_vendororders'));

	                        exit;
						} else {
							$session->getFlashBag()->add('error','Purchase Leads failed. Please try again later');
	                        return $this->redirect($this->generateUrl('b_bids_b_bids_vendororders'));
						}
					}
				//}
			}
			return $rendTwig;
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function paypalPaymentgatewayAction($purchaseID,$actiontype)
	{
		$session   = $this->container->get('session');
		$uid       = $session->get('uid');
		$client_ip = $_SERVER['REMOTE_ADDR'];
		$em        = $this->getDoctrine()->getManager();

        if($session->has('uid'))
        {
			$uid = $session->get('uid');
            $query = $em->createQueryBuilder()
                    ->select('count(l.id)')
                    ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                    ->add('where','l.vendorid = :uid')
                    ->andWhere('l.status = 1')
                    ->setParameter('uid', $uid);
            $useridexists = $query->getQuery()->getSingleScalarResult();


            $query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);
            $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

            switch ($actiontype) {
            	case 'process':
	            	$getPurchasingLeads = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findBy(array('purchaseid'=>$purchaseID,'status'=>1));

	            	if(empty($getPurchasingLeads))
					{
						$session->getFlashBag()->add('error','Unable to find your request to process for paypal. Please try again after some time.');
		            	return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_home'));
					}

					$leadsPerPack = array('Bronze'=>10, 'Silver'=>25, 'Gold'=>50, 'Platinum'=>100);
					$leadsPack    = array('Bronze'=>0, 'Silver'=>1, 'Gold'=>2, 'Platinum'=>3);

					$grandTotal = 0;
					foreach($getPurchasingLeads as $pLeads)
					{
						$priceArray = explode("-",$pLeads->getPrice());
						$plan       = $pLeads->getPlan();
						$selPlan    = $leadsPack[$plan];
						if($plan != 'Bronze') {
							$grandTotal += $priceArray[$selPlan] * $leadsPerPack[$plan];
						} else {
							$grandTotal += $priceArray[0] * $leadsPerPack[$plan];
						}
					}

					$pgInfo  = new Paymentgatewayinfo();
					$created = new \DateTime();

					$pgInfo->setCreated($created);
					$pgInfo->setUpdated($created);
					$pgInfo->setPayAmount($grandTotal);

					$pgInfo->setUserId($uid);
					$pgInfo->setUserIP($client_ip);
					$pgInfo->setPurchaseID($purchaseID);
					$pgInfo->setPayStatus('In progress');

					$em->persist($pgInfo);
					$em->flush();
					$account    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid));

					// $formAction    = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
					$formAction    = 'https://www.paypal.com/cgi-bin/webscr';
					$cancel_return = 'http://'.$_SERVER['HTTP_HOST'].$this->generateUrl('b_bids_b_bids_pgateway_paypal', array('purchaseID' => $purchaseID,'actiontype'=>'cancel'));
					$return        = 'http://'.$_SERVER['HTTP_HOST'].$this->generateUrl('b_bids_b_bids_pgateway_paypal', array('purchaseID' => $purchaseID,'actiontype'=>'success'));
					$notify_url    = 'http://'.$_SERVER['HTTP_HOST'].$this->generateUrl('b_bids_b_bids_pgateway_paypal', array('purchaseID' => $purchaseID,'actiontype'=>'ipn'));
					$convertValue = $this->currencyConverter('AED','USD'); // echo 'AED to USD'.$convertValue;exit;
					$grandTotal = $grandTotal * $convertValue;
					return $this->render('BBidsBBidsHomeBundle:User:paypal_payform.html.twig',array('useridexists'=>$useridexists, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'formAction'=>$formAction, 'return'=>$return, 'notify_url'=>$notify_url, 'cancel_return'=>$cancel_return, 'purchaseID'=>$purchaseID, 'quantity'=>1, 'grandTotal'=>$grandTotal));
        			break;
        		case 'success':
					/*echo 'Purchase Id'.$purchaseID;
					echo '<pre/>';print_r($_REQUEST);
					echo '<pre/>';print_r($_POST);exit;*/
					$log_array     = print_r($_POST, TRUE);
					$trasaction_id = $purchaseID;
					$connection    = $em->getConnection();
					$catQuery      = $connection->prepare("INSERT INTO `bbids_paypal_log` (`txn_id`, `log`, `posted_date`) VALUES ('$trasaction_id', '$log_array', NOW())");
		            $catQuery->execute();

		            $resultArray = array();
		            $resultArray['tranid'] = $purchaseID;
		            $resultArray['result'] = 'Successful';


					$leadsPerPack = array('Bronze'=>10, 'Silver'=>25, 'Gold'=>50, 'Platinum'=>100);
					$leadsPack    = array('Bronze'=>0, 'Silver'=>1, 'Gold'=>2, 'Platinum'=>3);

		            $getPurchasingLeads = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findBy(array('purchaseid'=>$purchaseID,'status'=>1));
					if(empty($getPurchasingLeads))
					{
						$session->getFlashBag()->add('error','Unable to find your ordernumber to process your request. Please try again after some time.');
		            	return $this->redirect($this->generateUrl('b_bids_b_bids_vendororders'));
					}

					$grandTotal = 0;
					foreach($getPurchasingLeads as $pLeads)
					{
						$priceArray = explode("-",$pLeads->getPrice());
						$plan       = $pLeads->getPlan();
						$selPlan    = $leadsPack[$plan];
						if($plan != 'Bronze') {
							$grandTotal += $priceArray[$selPlan] * $leadsPerPack[$plan];
						} else {
							$grandTotal += $priceArray[0] * $leadsPerPack[$plan];
						}
					}

					$userArray    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$uid));
					$vendoremail     = $userArray->getEmail();

					$account    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid));
					$otcStatus     = $account->getOtcStatus();
					$vendorcontactname     = $account->getContactname();
					if($otcStatus == 0)
						$grandTotal += 600;

					$paypalOrderNumber = '';
		            $paypalOrderNumber = $this->createOrder($resultArray, $purchaseID, $grandTotal,'Paypal');

		            $getPurchasingLeads = $em->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findOneBy(array('purchaseid'=>$purchaseID));
		            if($getPurchasingLeads) {
    		            $getPurchasingLeads->setStatus(0);

    		            $em->persist($getPurchasingLeads);
    					$em->flush();
		            }

					if($paypalOrderNumber) {

						$subject = 'BusinessBid Paypal Payment Successful';
						$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:paymentsuccessemail.html.twig', array('name'=>$vendorcontactname,'OrderNumber'=>'BB'.$paypalOrderNumber));

						$this->sendEmail($vendoremail,$subject,$body,$uid);

						return $this->redirect($this->generateUrl('b_bids_b_bids_pymt_success',array('ordernumber'=>'BB'.$paypalOrderNumber)));
					} else {
						$session->getFlashBag()->add('error','Unable to find your order to process your request.');
            			return $this->redirect($this->generateUrl('b_bids_b_bids_vendororders'));
					}
        			break;
        		case 'cancel':
        			// echo 'cancel<pre/>';print_r($_REQUEST);exit;
        		 	$getPurchasingLeads = $em->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findOneBy(array('purchaseid'=>$purchaseID));
		            if($getPurchasingLeads) {
    		            $getPurchasingLeads->setStatus(0);

    		            $em->persist($getPurchasingLeads);
    					$em->flush();
		            }
		            $session->getFlashBag()->add('error','Your cancelled the Purchase Leads request.');
                    return $this->redirect($this->generateUrl('b_bids_b_bids_vendororders'));
        			break;
        		case 'ipn':
        			$log_array		= print_r($_POST, TRUE);
					if(isset($_POST["txn_id"]))
						$trasaction_id  = $_POST["txn_id"];
					else
						$trasaction_id = 123;
					$connection = $em->getConnection();
					$catQuery = $connection->prepare("INSERT INTO `bbids_paypal_log` (`txn_id`, `log`, `posted_date`) VALUES ('$trasaction_id', '$log_array', NOW())");
		            $catQuery->execute();
		            echo 'ipn<pre/>';print_r($_REQUEST);exit;
        			break;
            	default:
            		$session->getFlashBag()->add('error','Unable to process your request.');
            		return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
            		break;
            }
 		}  else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	protected function currencyConverter($from_code,$to_code)
	{
		$url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $from_code . $to_code .'=X';
		$handle = @fopen($url, 'r');
	   	if ($handle)
	  	{
			$result = fgets($handle, 4096);
			fclose($handle);
	  	}
	  	$allData = explode(',',$result); /* Get all the contents to an array */

	  	//print'<pre/>';print_r($result);exit;
	  	return $dollarValue = $allData[1];
	}

	function leadCheckPayments($formData, $purchaseID, $grandTotal, $vendorCompName)
	{
		$session = $this->container->get('session');
		$uid     = $session->get('uid');

		//Send email about check pickup to admin
		$body = $this->render('BBidsBBidsHomeBundle:Emailtemplates/Admin:cheque_pickup.html.twig',array('formData'=>$formData,'vendorCompName'=>$vendorCompName));
		$this->sendEmail('infosupport@businessbid.ae','Check Pickup',$body,$uid);

		//Order is set to bbids_order and bbids_orderproductsrel
		$getPurchasingLeads = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findBy(array('purchaseid'=>$purchaseID,'status'=>1));
		// echo '<pre/>';print_r($getPurchasingLeads);exit;



		$em = $this->getDoctrine()->getManager();

		$connection = $em->getConnection();

		$maxOrderSql = $connection->prepare("SELECT MAX(id) AS maxid FROM bbids_order");
        $maxOrderSql->execute();
		$maxOrderSql = $maxOrderSql->fetchAll();
		$maxid       = $maxOrderSql[0]['maxid'];
        // echo '<pre/>';print_r($maxOrderSql);exit;

		$nowmaxid    = $maxid + 1;
		$ordernumber = "BB".$nowmaxid;
		$created     = new \DateTime();

		$newOrder = new Order();

		$newOrder->setOrdernumber($ordernumber);
		$newOrder->setCreated($created);
		$newOrder->setUpdated($created);
		$newOrder->setAmount($grandTotal);
		$newOrder->setPayoption('Cheque Payment');
		$newOrder->setTransactionid($purchaseID);
		$newOrder->setBankname($formData['bankname']);
		$newOrder->setTransactionstatus('Pending');
		$newOrder->setAuthor($uid);
		$newOrder->setStatus(3);

		$em->persist($newOrder);
		$em->flush();

		$orderID = $newOrder->getId();

        $leadsPerPack = array('Bronze'=>10, 'Silver'=>25, 'Gold'=>50, 'Platinum'=>100);
		$leadsPack    = array('Bronze'=>0, 'Silver'=>1, 'Gold'=>2, 'Platinum'=>3);
		foreach($getPurchasingLeads as $pLeads)
		{
			$catid = $pLeads->getCategoryid();
			$plan  = $pLeads->getPlan();
			$noOfLead = $leadsPerPack[$plan];

			//step 4  insert into bbids_orderproductsrel
			$newOrderproductsrel = new Orderproductsrel();
			$newOrderproductsrel->setOrdernumber($orderID);
			$newOrderproductsrel->setCategoryid($catid);
			$newOrderproductsrel->setLeadpack($noOfLead);
			$em->persist($newOrderproductsrel);
			$em->flush();
		}

		$newCheckpaymenthistory = new Checkpaymenthistory();
		$newCheckpaymenthistory->setOrdernumber($ordernumber);
		$newCheckpaymenthistory->setCreated($created);
		$newCheckpaymenthistory->setUpdated($created);
		$newCheckpaymenthistory->setAmount($grandTotal);
		$newCheckpaymenthistory->setAddress($formData['address']);
		$newCheckpaymenthistory->setCity($formData['city']);
		$newCheckpaymenthistory->setContactPerson($formData['contactperson']);
		$newCheckpaymenthistory->setContactNumber($formData['contactnumber']);
		$newCheckpaymenthistory->setBankname($formData['bankname']);
		$newCheckpaymenthistory->setCheckNumber($formData['checknumber']);
		$newCheckpaymenthistory->setAuthor($uid);
		$newCheckpaymenthistory->setStatus(3);

		$em->persist($newCheckpaymenthistory);
		$em->flush();

		$getPurchasingLeads = $em->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findOneBy(array('purchaseid'=>$purchaseID));
        if($getPurchasingLeads) {
	        $getPurchasingLeads->setStatus(0);

	        $em->persist($getPurchasingLeads);
	        $em->flush();
    	}

		return $ordernumber;
		unset($_SESSION['leads']);
	}

    function leadCashPayments($formData, $purchaseID, $grandTotal, $vendorCompName)
    {
        $session = $this->container->get('session');
        $uid     = $session->get('uid');

        //Send email about check pickup to admin
        $body = $this->render('BBidsBBidsHomeBundle:Emailtemplates/Admin:cheque_pickup.html.twig',array('formData'=>$formData,'vendorCompName'=>$vendorCompName));
        $this->sendEmail('infosupport@businessbid.ae','Cash Pickup',$body,$uid);

        //Order is set to bbids_order and bbids_orderproductsrel
        $getPurchasingLeads = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findBy(array('purchaseid'=>$purchaseID,'status'=>1));
        // echo '<pre/>';print_r($getPurchasingLeads);exit;



        $em = $this->getDoctrine()->getManager();

        $connection = $em->getConnection();

        $maxOrderSql = $connection->prepare("SELECT MAX(id) AS maxid FROM bbids_order");
        $maxOrderSql->execute();
        $maxOrderSql = $maxOrderSql->fetchAll();
        $maxid       = $maxOrderSql[0]['maxid'];
        // echo '<pre/>';print_r($maxOrderSql);exit;

        $nowmaxid    = $maxid + 1;
        $ordernumber = "BB".$nowmaxid;
        $created     = new \DateTime();

        $newOrder = new Order();

        $newOrder->setOrdernumber($ordernumber);
        $newOrder->setCreated($created);
        $newOrder->setUpdated($created);
        $newOrder->setAmount($grandTotal);
        $newOrder->setPayoption('Cash Payment');
        $newOrder->setTransactionid($purchaseID);
        $newOrder->setBankname('Nil');
        $newOrder->setTransactionstatus('Pending');
        $newOrder->setAuthor($uid);
        $newOrder->setStatus(3);

        $em->persist($newOrder);
        $em->flush();

        $orderID = $newOrder->getId();

        $leadsPerPack = array('Bronze'=>10, 'Silver'=>25, 'Gold'=>50, 'Platinum'=>100);
        $leadsPack    = array('Bronze'=>0, 'Silver'=>1, 'Gold'=>2, 'Platinum'=>3);
        foreach($getPurchasingLeads as $pLeads)
        {
            $catid = $pLeads->getCategoryid();
            $plan  = $pLeads->getPlan();
            $noOfLead = $leadsPerPack[$plan];

            //step 4  insert into bbids_orderproductsrel
            $newOrderproductsrel = new Orderproductsrel();
            $newOrderproductsrel->setOrdernumber($orderID);
            $newOrderproductsrel->setCategoryid($catid);
            $newOrderproductsrel->setLeadpack($noOfLead);
            $em->persist($newOrderproductsrel);
            $em->flush();
        }

        $newCashpaymenthistory = new Cashpaymenthistory();
        $newCashpaymenthistory->setOrdernumber($ordernumber);
        $newCashpaymenthistory->setCreated($created);
        $newCashpaymenthistory->setUpdated($created);
        $newCashpaymenthistory->setAmount($grandTotal);
        $newCashpaymenthistory->setAddress($formData['address']);
        $newCashpaymenthistory->setCity($formData['city']);
        $newCashpaymenthistory->setContactPerson($formData['contactperson']);
        $newCashpaymenthistory->setContactNumber($formData['contactnumber']);
        $newCashpaymenthistory->setAuthor($uid);
        $newCashpaymenthistory->setStatus(3);

        $em->persist($newCashpaymenthistory);
        $em->flush();

        $getPurchasingLeads = $em->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findOneBy(array('purchaseid'=>$purchaseID));
        if($getPurchasingLeads) {
            $getPurchasingLeads->setStatus(0);

            $em->persist($getPurchasingLeads);
            $em->flush();
        }

        return $ordernumber;
        unset($_SESSION['leads']);
    }

	function leadsPaymentConfirmationAction($ordernumber)
	{
		$session = $this->container->get('session');
		$uid     = $session->get('uid');
		if($session->has('uid')){
			$em = $this->getDoctrine()->getManager();
			$query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
			$query = $em->createQueryBuilder()
	                ->select('count(l.id)')
	                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
	                ->add('where','l.vendorid = :uid')
	                ->andWhere('l.status = 1')
					->setParameter('uid', $uid);
			$useridexists = $query->getQuery()->getSingleScalarResult();
			return $this->render('BBidsBBidsHomeBundle:User:leads_paymentconfirmation.html.twig',array('useridexistsfreeleads'=>$useridexistsfreeleads,'useridexists'=>$useridexists,'ordernumber'=>$ordernumber));
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	function paymentGatewayCall($formData, $purchaseID, $grandTotal, $type)
	{
		//echo $grandTotal.'<pre/>';print_r($formData);exit;
		$session = $this->container->get('session');
		$uid     = $session->get('uid');
		if($type == 'Test')
		{

			$merchantid  = 'CKODUBAPITEST';
			$password    = 'Password1!';
			$trackid     = rand ( 10000 , 99999 );
			$cc_name     = 'CASH CUSTOMER';
			$cc_type     = 'CC';
			$cc_brand    = 'VC';
			$cc_no       = '4543474002249996';
			$expmonth    = 06;
			$expyear     = 2017;
			$cvv2        = 956;
			$bill_amount = 10.00;
			$bill_currencycode = 'USD';
			$gatewayurl = "https://api.checkout.com/Process/gateway.aspx";
		}
		else {


			$merchantid  = 'BUSINESSBID';
			$password    = 'Bidae2';
			$trackid     = $purchaseID;
			$cc_name     = $formData['ccName'];
			$cc_type     = 'CC';
			$cc_brand    = 'MC';
			$cc_no       = $formData['ccNumber'];
			/*$expiryArray = explode("-",$formData['ccExpDate']);
			$expmonth    = $expiryArray[1];
			$expyear     = $expiryArray[0];*/
			$expmonth    = $formData['ccExpDate']['month'];
			$expmonth    = sprintf("%02s", $expmonth);
			$expyear    = $formData['ccExpDate']['year'];
			$cvv2        = $formData['ccCVV'];
			$bill_amount = $grandTotal;
			$bill_currencycode = 'AED';
			// $gatewayurl = "https://api.checkout.com/hpayment-TokenService/CreateToken.ashx";
			$gatewayurl = "https://api.checkout.com/Process/gateway.aspx";
		}
		$client_ip   = $_SERVER['REMOTE_ADDR'];


		// $gatewayurl = "http://uat.egatepay.com/process/gateway.aspx";*/

		$post_string = '<?xml version="1.0" encoding="ISO-8859-1"?>';
		$post_string .= '<request>';
		$post_string .= '<account_identifier></account_identifier>';
		$post_string .= '<merchantid>'.$merchantid.'</merchantid>';
		$post_string .= '<password>'.$password.'</password>';
		$post_string .= '<action>1</action>';
		$post_string .= '<trackid>'.$trackid.'</trackid>';
		$post_string .= '<bill_currencycode>'.$bill_currencycode.'</bill_currencycode>';
		$post_string .= '<bill_cardholder>'.$cc_name.'</bill_cardholder>';
		$post_string .= '<bill_cc_type>'.$cc_type.'</bill_cc_type>';
		$post_string .= '<bill_cc_brand>'.$cc_brand.'</bill_cc_brand>';
		$post_string .= '<bill_cc>'.$cc_no.'</bill_cc>';
		$post_string .= '<bill_expmonth>'.$expmonth.'</bill_expmonth>';
		$post_string .= '<bill_expyear>'.$expyear.'</bill_expyear>';
		$post_string .= '<bill_cvv2>'.$cvv2.'</bill_cvv2>';
		$post_string .= '<bill_address>SMALLSYS INC</bill_address>';
		$post_string .= '<bill_address2>795 E DRAGRAM</bill_address2>';
		$post_string .= '<bill_postal>85705</bill_postal>';
		$post_string .= '<bill_city>DRAGRAM</bill_city>';
		$post_string .= '<bill_state>TUCSON</bill_state>';
		$post_string .= '<bill_email>irfan@netiapps.com</bill_email>';
		$post_string .= '<bill_country>USA</bill_country>';
		$post_string .= '<bill_amount>'.$bill_amount.'</bill_amount>';
		$post_string .= '<bill_phone>44-12312331312</bill_phone>';
		$post_string .= '<bill_fax>44-12312331312</bill_fax>';
		$post_string .= '<bill_customerip>'.$client_ip.'</bill_customerip>';
		$post_string .= '<bill_merchantip>115.124.120.36</bill_merchantip>';
		$post_string .= '<ship_address>SMALLSYS INC</ship_address>';
		$post_string .= '<ship_email>irfan@netiapps.com</ship_email>';
		$post_string .= '<ship_postal>85705</ship_postal>';
		$post_string .= '<ship_address2>795 E DRAGRAM</ship_address2>';
		$post_string .= '<ship_type>FEDEX</ship_type>';
		$post_string .= '<ship_city>DRAGRAM</ship_city>';
		$post_string .= '<ship_state>TUCSON</ship_state>';
		$post_string .= '<ship_phone>44-12312331312</ship_phone>';
		$post_string .= '<ship_country>USA</ship_country>';
		$post_string .= '<ship_fax>44-12312331312</ship_fax>';
		$post_string .= '<udf1></udf1>';
		$post_string .= '<udf2></udf2>';
		$post_string .= '<udf3></udf3>';
		$post_string .= '<udf4></udf4>';
		$post_string .= '<udf5></udf5>';
		$post_string .= '<merchantcustomerid></merchantcustomerid>';
		$post_string .= '<product_desc></product_desc>';
		$post_string .= '<product_quantity></product_quantity>';
		$post_string .= '<product_unitcost></product_unitcost>';
		$post_string .= '</request>';

		$header  = "POST HTTP/1.0 \r\n";
		$header .= "Content-type: text/xml \r\n";
		$header .= "Content-length: ".strlen($post_string)." \r\n";
		$header .= "Content-transfer-encoding: text \r\n";
		$header .= "Connection: close \r\n\r\n";
		$header .= $post_string;

		$myFile = "req_response.xml";

		// echo "<pre>".$post_string."</pre>";
		$fh = fopen($myFile, 'a+') or die("can't open file");
		fwrite($fh, $post_string);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_URL, $gatewayurl);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		$data = curl_exec($ch);


		fwrite($fh, $data);
		fclose($fh);

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
	 	echo $data;*/
	 	$resultArray = json_decode(json_encode((array)simplexml_load_string($data)),1);
	 	// echo '<pre/>';print_r($resultArray);exit;
	 	$em = $this->getDoctrine()->getManager();
		if(curl_errno($ch))
		    print curl_error($ch);
		else {
			curl_close($ch);
			if($resultArray['@attributes']['type'] == 'error') {
				$pgInfo  = new Paymentgatewayinfo();
				$created = new \DateTime();

				$pgInfo->setCreated($created);
				$pgInfo->setUpdated($created);
				$pgInfo->setPayAmount($grandTotal);
				if(isset($resultArray['error_code_tag']))
					$pgInfo->setPayError($resultArray['error_code_tag']);
				if(isset($resultArray['error_text']))
					$pgInfo->setPayErrorText($resultArray['error_text']);
				$pgInfo->setUserId($uid);
				$pgInfo->setUserIP($client_ip);
				$pgInfo->setPurchaseID($purchaseID);
				$pgInfo->setPayStatus('Failed');

				$em->persist($pgInfo);
				$em->flush();
				return false;
			} else if(($resultArray['@attributes']['type'] == 'valid') AND ($resultArray['result'] == 'Successful')) {
				$pgInfo  = new Paymentgatewayinfo();
				$created = new \DateTime();

				$pgInfo->setCreated($created);
				$pgInfo->setUpdated($created);
				$pgInfo->setPayAmount($grandTotal);
				$pgInfo->setPayResult($resultArray['result']);
				$pgInfo->setPayRescode($resultArray['responsecode']);
				$pgInfo->setPayTranid($resultArray['tranid']);
				$pgInfo->setPurchaseID($resultArray['trackid']);
				$pgInfo->setPayAuthcode($resultArray['authcode']);
				$pgInfo->setUserId($uid);
				$pgInfo->setUserIP($client_ip);
				$pgInfo->setPayStatus('Successful');

				$em->persist($pgInfo);
				$em->flush();

				$this->createOrder($resultArray, $purchaseID, $grandTotal, 'Credit Card'); //Generate order for vendor leads purchase
					$transactionid=$resultArray['tranid'];
				return $transactionid;
			} else if(($resultArray['@attributes']['type'] == 'valid') AND ($resultArray['result'] == 'Not Successful')) {
				$pgInfo  = new Paymentgatewayinfo();
				$created = new \DateTime();

				$pgResult       = $resultArray['result'];
				$pgResponsecode = $resultArray['responsecode'];
				$pgTranid       = $resultArray['tranid'];
				$pgTrackid      = $resultArray['trackid'];
				$pgAuthcode     = '';

				$pgInfo->setCreated($created);
				$pgInfo->setUpdated($created);
				$pgInfo->setPayAmount($grandTotal);
				$pgInfo->setPayResult($pgResult);
				$pgInfo->setPayRescode($pgResponsecode);
				$pgInfo->setPayTranid($pgTranid);
				$pgInfo->setPurchaseID($pgTrackid);
				$pgInfo->setPayAuthcode($pgAuthcode);
				$pgInfo->setUserId($uid);
				$pgInfo->setUserIP($client_ip);
				$pgInfo->setPayStatus('Failed');

				$em->persist($pgInfo);
				$em->flush();
				return false;
			}
		}
	}

	protected function createOrder($resultArray, $purchaseID, $grandTotal, $orderType='')
	{

		$session = $this->container->get('session');
		$uid     = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$connection = $em->getConnection();



		$maxOrderSql = $connection->prepare("SELECT MAX(id) AS maxid FROM bbids_order");
        $maxOrderSql->execute();
        $maxOrderSql = $maxOrderSql->fetchAll();
        $maxid = $maxOrderSql[0]['maxid'];
        // echo '<pre/>';print_r($maxOrderSql);exit;

		$nowmaxid    = $maxid + 1;
		$ordernumber = "BB".$nowmaxid;
		$created     = new \DateTime();

		$newOrder = new Order();

		$newOrder->setOrdernumber($ordernumber);
		$newOrder->setCreated($created);
		$newOrder->setUpdated($created);
		$newOrder->setAmount($grandTotal);
		$newOrder->setPayoption($orderType);
		$newOrder->setTransactionid($resultArray['tranid']);
		$newOrder->setBankname('Nil');
		$newOrder->setTransactionstatus($resultArray['result']);
		$newOrder->setAuthor($uid);
		$newOrder->setStatus(1);

		$em->persist($newOrder);
		$em->flush();

		$orderID = $newOrder->getId();

		/* One Charge 600 AED */
		$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$uid));
        $account->setOtcStatus(1);
        $em->persist($account);
		$em->flush();

        $getPurchasingLeads = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Purchaseleads')->findBy(array('purchaseid'=>$purchaseID,'status'=>1));
        $leadsPerPack = array('Bronze'=>10, 'Silver'=>25, 'Gold'=>50, 'Platinum'=>100);
		$leadsPack    = array('Bronze'=>0, 'Silver'=>1, 'Gold'=>2, 'Platinum'=>3);
		foreach($getPurchasingLeads as $pLeads)
		{
			$catid = $pLeads->getCategoryid();
			$plan  = $pLeads->getPlan();
			$noOfLead = $leadsPerPack[$plan];

			// Disable the manual category mapping
			$manualVendCatMapping = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findOneBy(array('vendorID'=>$uid));
			if(!empty($manualVendCatMapping)) {

				$manualVendCatMapping->setStatus(0);

				$em->persist($manualVendCatMapping);
				$em->flush();
			}

			//Step 1 insert into SUbCategories
			$subCategoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$catid));
			foreach ($subCategoryArray as $subCategory) {
				$subCatid = $subCategory->getId();
				$subCategoryArray = $em->getRepository('BBidsBBidsHomeBundle:Vendorsubcategoriesrel')->findOneBy(array('subcategoryid'=>$subCatid, 'vendorid'=>$uid));

				if(is_null($subCategoryArray))
				{
					$newVendorSubCat = new Vendorsubcategoriesrel();
					$newVendorSubCat->setVendorid($uid);
					$newVendorSubCat->setSubcategoryid($subCatid);
					$newVendorSubCat->setStatus(1);

					$em->persist($newVendorSubCat);
					$em->flush();
				} else {
					$subCategoryArray->setStatus(1);

					$em->persist($subCategoryArray);
					$em->flush();
				}
			}

			//Step 2 insert into Vendorcategoriesrel
			$vendorCatIDExist = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$uid,'categoryid'=>$catid,'flag'=>1));
			// echo '<pre/>';print_r($vendorCatIDExist);exit;
			if(empty($vendorCatIDExist))
			{
				$newVendorCatrel = new Vendorcategoriesrel();
				$newVendorCatrel->setVendorid($uid);
				$newVendorCatrel->setCategoryid($catid);
				$newVendorCatrel->setLeadpack($noOfLead);
				$newVendorCatrel->setStatus(1);
				$newVendorCatrel->setFlag(1);

				$em->persist($newVendorCatrel);
				$em->flush();
			} else {
				foreach ($vendorCatIDExist as $catRelVaule) {
					$lastPackValue = $catRelVaule->getLeadpack();
					$newPackValue = $lastPackValue + $noOfLead;
					$catRelVaule->setLeadpack($newPackValue);

					$em->persist($catRelVaule);
					$em->flush();
				}
			}

			//Step 3 insert into bbids_vendororderproductsrel
			$vendorOrderProductRel = $em->getRepository('BBidsBBidsHomeBundle:Vendororderproductsrel')->findBy(array('vendorid'=>$uid,'categoryid'=>$catid));
			// echo '<pre/>';print_r($vendorOrderProductRel);exit;
			if(empty($vendorOrderProductRel))
			{
				$newVendorOrderProductRel = new Vendororderproductsrel();
				$newVendorOrderProductRel->setVendorid($uid);
				$newVendorOrderProductRel->setCategoryid($catid);
				$newVendorOrderProductRel->setLeadpack($noOfLead);

				$em->persist($newVendorOrderProductRel);
				$em->flush();
			} else {
				foreach ($vendorOrderProductRel as $vendorCatOrdersValue) {
					$lastOrderPackValue = $vendorCatOrdersValue->getLeadpack();
					$newOrderPackValue  = $lastOrderPackValue + $noOfLead;
					$vendorCatOrdersValue->setLeadpack($newOrderPackValue);

					$em->persist($vendorCatOrdersValue);
					$em->flush();
				}
			}

			//step 4  insert into bbids_orderproductsrel
			$newOrderproductsrel = new Orderproductsrel();
			$newOrderproductsrel->setOrdernumber($orderID);
			$newOrderproductsrel->setCategoryid($catid);
			$newOrderproductsrel->setLeadpack($noOfLead);
			$em->persist($newOrderproductsrel);
			$em->flush();
		}

		return $orderID;
		unset($_SESSION['leads']);
	}

	protected function cardType($number)
	{
	    $number=preg_replace('/[^\d]/','',$number);
	    if (preg_match('/^3[47][0-9]{13}$/',$number))
	    {
	        return 'American Express';
	    }
	    elseif (preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',$number))
	    {
	        return 'Diners Club';
	    }
	    elseif (preg_match('/^6(?:011|5[0-9][0-9])[0-9]{12}$/',$number))
	    {
	        return 'Discover';
	    }
	    elseif (preg_match('/^(?:2131|1800|35\d{3})\d{11}$/',$number))
	    {
	        return 'JCB';
	    }
	    elseif (preg_match('/^5[1-5][0-9]{14}$/',$number))
	    {
	        return 'MC';//MasterCard
	    }
	    elseif (preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/',$number))
	    {
	        return 'VC';//Visa
	    }
	    else
	    {
	        return 'Unknown';
	    }
	}

	public function buyleadsAction()
	{
		$session = $this->container->get('session');

		$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('c.category, v.leadpack, v.status,f.leads,c.id')
			->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')
			->innerJoin('BBidsBBidsHomeBundle:Freetrail', 'f', 'with', 'v.vendorid = f.userid')
			->where('v.vendorid = :vendorid')
			->setParameter('vendorid', $uid);
			//->setParameter('vendorid',261);

		$leads = $query->getQuery()->getResult();

		$query = $em->createQueryBuilder()
                    ->select('count(l.id)')
                    ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                    ->add('where','l.vendorid = :uid')
                    ->andWhere('l.status = 1')
					->setParameter('uid', $uid);
		$useridexists = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
		return $this->render('BBidsBBidsHomeBundle:User:myleads.html.twig', array('leads'=>$leads,'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads));
	}

	public function leadhistoryAction()
    {
		$session = $this->container->get('session');
		if($session->has('uid')){
			$uid = $session->get('uid');

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
                        //->select('c.category,v.categoryid, v.leadpack,v.deductedlead,v.deductedfreelead, v.status,v.flag,v.vendorid')
                        ->select('c.category,v.categoryid, v.leadpack,v.deductedlead,v.deductedfreelead, v.status,v.flag,v.vendorid,o.leadpack as pur')
                        ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')
						 ->innerJoin('BBidsBBidsHomeBundle:Vendororderproductsrel', 'o', 'with', 'v.categoryid = o.categoryid')
						->where('v.vendorid = :vendorid')
						 ->andWhere('o.vendorid = :uid')
                        ->setParameter('vendorid', $uid)
                        ->setParameter('uid', $uid);
						


                $leads = $query->getQuery()->getResult();

                $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);
			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
			$emailCount = $this->getMessageCount();

			return $this->render('BBidsBBidsHomeBundle:User:leadhistory.html.twig',array('leads'=>$leads,
                        'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));

		}

		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

        public function leadsviewAction()
        {
              $session = $this->container->get('session');

                $uid = $session->get('uid');

                $em = $this->getDoctrine()->getManager();
	         $query = $em->createQueryBuilder()
                        ->select('c.category, v.leadpack, v.status,v.flag')
                        ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')

                        ->where('v.vendorid = :vendorid')

                        ->setParameter('vendorid', $uid);
                        //->setParameter('vendorid',261);

                $leads = $query->getQuery()->getResult();

                 $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);
                                 //->setParameter('uid', 333);
                        // echo $myquery = $query->getQuery()->getSQL();
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

                return $this->render('BBidsBBidsHomeBundle:User:leadsview.html.twig',array('leads'=>$leads,
'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads));

        }

	public function leadsviewhistoryAction($uid, $catid, Request $request)
        {
              $session = $this->container->get('session');
             $sessionid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
				 $query = $em->createQueryBuilder()
						->select('e.id,e.subj,e.category,e.created,e.updated,a.contactname')
						->from('BBidsBBidsHomeBundle:Enquiry', 'e')
						->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'l', 'with', 'e.id = l.enquiryid')
						->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = e.authorid')
						->where('l.vendorid= :vendorid')
						->andWhere('e.category = :category')
						->andWhere('l.flag= 1')
						//->andWhere('vc.leadpack != 0')
						->andWhere('l.acceptstatus = 1')
						->setParameter('category',$catid)
						->setParameter('vendorid',$sessionid);
			$leadsviews = $query->getQuery()->getResult();

			$leadshistory=count($leadsviews);

           $leadshistory=count($leadsviews);
			if($leadshistory > 0){
                       // for($i = 0; $i<$leadshistory; $i++){
			       // $enqid=$leadsviews[$i]['id'];
				// $query = $em->createQueryBuilder()
                                // ->select('count(s.id)')
                                // ->from('BBidsBBidsHomeBundle:EnquirySubcategoryRel', 's')
                                // ->add('where', 's.enquiryid = :enquiryid')
                                // ->setParameter('enquiryid', $enqid);
	  // $subcount = $query->getQuery()->getSingleScalarResult();

				// }
				$subcount=array();
				foreach($leadsviews as $l){
					$enqid=$l['id'];

					$query = $em->createQueryBuilder()
                                ->select('count(s.id)')
                                ->from('BBidsBBidsHomeBundle:EnquirySubcategoryRel', 's')
                                ->add('where', 's.enquiryid = :enquiryid')
                                ->setParameter('enquiryid', $enqid);

	 $subcount[] = $query->getQuery()->getSingleScalarResult();

					}
			} else { $subcount = 0; }
                 $query = $em->createQueryBuilder()
                        ->select('c.category, v.leadpack, v.status,v.flag,v.deductedlead,v.deductedfreelead')
                        ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')

                        ->where('v.vendorid = :vendorid')

                        ->setParameter('vendorid', $uid);
                        //->setParameter('vendorid',261);

                $leads = $query->getQuery()->getResult();

                  $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);
                                 //->setParameter('uid', 333);
                        // echo $myquery = $query->getQuery()->getSQL();
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		$catArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$catid));
		foreach($catArray as $c){
			$cat = $c->getCategory();
		}

                return $this->render('BBidsBBidsHomeBundle:User:leadsviewhistory.html.twig',array('leads'=>$leads, 'userid'=>$sessionid, 'cat'=>$cat,'leadsviews'=>$leadsviews,
'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'subcount'=>$subcount));
}





	public function myenquiriesAction()
	{
		$session = $this->container->get('session');

                $uid = $session->get('uid');

                $em = $this->getDoctrine()->getManager();

                $query = $em->createQueryBuilder()
                        ->select('c.category, v.leadpack, v.status,f.leads')
                        ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')
                        ->innerJoin('BBidsBBidsHomeBundle:Freetrail', 'f', 'with', 'v.vendorid = f.userid')
                        ->where('v.vendorid = :vendorid')
                        ->setParameter('vendorid', $uid);
                        //->setParameter('vendorid',261);

                $leads = $query->getQuery()->getResult();

               $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);
                                 //->setParameter('uid', 333);
                        // echo $myquery = $query->getQuery()->getSQL();
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();


		$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('e.id, e.subj, e.description, e.city, e.created, c.category')
			->from('BBidsBBidsHomeBundle:Enquiry' ,'e')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
			->add('where', 'e.authorid = :authorid')
			->setParameter('authorid', $uid);

		$enquiries = $query->getQuery()->getResult();


		//$enquiries = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Enquiry')->findBy(array('authorid'=>$uid));

		return $this->render('BBidsBBidsHomeBundle:User:consumerenquiries.html.twig', array('enquiries'=>$enquiries));
	}

	public function vendorenquiriesAction($offset, Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){
		 $uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();
                  $query = $em->createQueryBuilder()
                                ->select('count(e.id)')
                                ->from('BBidsBBidsHomeBundle:Enquiry', 'e');

        $totalenquirycount = $query->getQuery()->getSingleScalarResult();
        $start = (($offset * 4) - 4);
                        $from = $start +1 ;

                        $to = $start + 4;



		 $query = $em->createQueryBuilder()
                     ->select(array('e.subj,e.id, e.description, e.city, c.category, e.created, e.id, v.acceptstatus, e.authorid'))
					 ->from('BBidsBBidsHomeBundle:Enquiry','e')
			         ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
			         ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
			         //->innerJoin('BBidsBBidsHomeBundle:City', 'ci', 'WITH', 'e.city like ci.city')
			         ->add('where','v.vendorid = :uid')
			         ->andWhere('e.status = 1')
			         ->add('orderBy', 'e.created DESC')
			         ->setFirstResult($start)
			         ->setMaxResults(4)
					->setParameter('uid', $uid);


		$enquiries = $query->getQuery()->getResult();
		$query = $em->createQueryBuilder()
                                ->select('count(e.id)')
                                ->from('BBidsBBidsHomeBundle:Enquiry','e')
								 ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
								 ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
								 ->add('where','v.vendorid = :uid')
								 ->andWhere('e.status = 1')
                                ->setParameter('uid', $uid);
		$count = $query->getQuery()->getSingleScalarResult();

		 $encount=count($enquiries);


		 $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
		$useridexists = $query->getQuery()->getSingleScalarResult();
		$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		 $categories = new Categories();
		 $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0));

		$categoryList = array();

		foreach($categoriesArray as $category){
			$categoryid = $category->getId();
			$categoryname = $category->getCategory();
			$categoryList[$categoryid] = $categoryname;
		}

		$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();


		$cityList = array();
		foreach($cityArray as $city){
			$cityid = $city->getId();
			$cityname = $city->getCity();
			$cityList[$cityname] = $cityname;
		}
		$emailCount = $this->getMessageCount();

		$form = $this->createFormBuilder()
				->add('category','choice', array('label'=>'Category ','required'=>false,'choices'=>$categoryList, 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
				->add('city','choice', array('label'=>'City ','required'=>false,'choices'=>$cityList, 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
				 ->add('fromdate','text',array('mapped'=>false,'label'=>'From Date'))
				 ->add('todate','text',array('mapped'=>false ,'required'=>false,'label'=>'To Date'))
                 ->add('filter', 'submit')
    	         ->getForm();
            $form->handleRequest($request);
         if($request->isMethod('POST')){

				if($form->isValid()){

					$fromdate = $form['fromdate']->getData();
					$todate = $form['todate']->getData();
					$category = $form['category']->getData();
					$city = $form['city']->getData();

					//echo $fromdate;exit;
					$connection = $em->getConnection();
					if($category == "" && $city==""){

					$query = $connection->prepare("SELECT * FROM bbids_enquiry e JOIN bbids_vendorenquiryrel v ON e.id=v.enquiryid JOIN bbids_categories c ON e.category=c.id JOIN bbids_city ci ON e.city =ci.city WHERE v.vendorid = $uid AND e.status= 1 AND date(e.created) between '$fromdate' AND '$todate' ");

                                $query->execute();
                                $enquiries = $query->fetchAll();



					}

					else if($category != "" && $city==''){


					$query = $connection->prepare("SELECT * FROM bbids_enquiry e JOIN bbids_vendorenquiryrel v ON e.id=v.enquiryid JOIN bbids_categories c ON e.category=c.id JOIN bbids_city ci ON e.city=ci.city WHERE v.vendorid = $uid AND e.category =$category AND  e.status= 1 AND date(e.created) between '$fromdate' AND '$todate' ");

                                $query->execute();
                                $enquiries = $query->fetchAll();




					}

					else if($category == "" && $city !=''){

					$query = $connection->prepare("SELECT * FROM bbids_enquiry e JOIN bbids_vendorenquiryrel v ON e.id=v.enquiryid JOIN bbids_categories c ON e.category=c.id JOIN bbids_city ci ON e.city=ci.city WHERE v.vendorid = $uid AND e.city LIKE '$city' AND e.status= 1 AND date(e.created) between '$fromdate' AND '$todate' ");

                                $query->execute();
                                $enquiries = $query->fetchAll();



					}

					else
					{
						$query = $connection->prepare("SELECT * FROM bbids_enquiry e JOIN bbids_vendorenquiryrel v ON e.id=v.enquiryid JOIN bbids_categories c ON e.category=c.id JOIN bbids_city ci ON e.city=ci.city WHERE v.vendorid = $uid AND e.category =$category AND e.city LIKE '$city' AND e.status= 1 AND date(e.created) between '$fromdate' AND '$todate' ");

                                $query->execute();
                                $enquiries = $query->fetchAll();
					}
				}

				$count ='';
				$to    ='';
				$from  ='';

				return $this->render('BBidsBBidsHomeBundle:User:vendorenquiries.html.twig', array('enquiries'=>$enquiries,'useridexists'=>$useridexists, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'from'=>$from, 'to'=>$to, 'count'=>$count, 'form'=>$form->createView(),'emailCount'=>$emailCount));
			}
			// echo $to;exit;
			if($to>$count)
				$to = $count;


		return $this->render('BBidsBBidsHomeBundle:User:vendorenquiries.html.twig', array('enquiries'=>$enquiries,'useridexists'=>$useridexists, 'useridexistsfreeleads'=>$useridexistsfreeleads, 'from'=>$from, 'to'=>$to, 'count'=>$count, 'form'=>$form->createView(),'emailCount'=>$emailCount));
		}
		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function choosepackAction()
	{
		$session = $this->container->get('session');
		if($session->has('uid')){
			$uid = $session->get('uid');
			$em = $this->getDoctrine()->getManager();

			$account 	= $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
			$otcStatus 	= $account[0]->getOtcStatus();

			$query = $em->createQueryBuilder()
                        ->select('count(l.id)')
                        ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                        ->add('where','l.vendorid = :uid')
                        ->andWhere('l.status = 1')
					->setParameter('uid', $uid);

	        $useridexists = $query->getQuery()->getSingleScalarResult();
	        $query = $em->createQueryBuilder()
	                        ->select('count(f.userid)')
	                        ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
	                        ->add('where','f.userid = :uid')
	                        ->setParameter('uid', $uid);

			$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

			$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessioncategoriesrel')->findBy(array('userid'=>$uid));

			$categoryidArray = array();

			$categorynameArray = array();

			$leadCount = count($categoryArray);

			foreach($categoryArray as $category) {
				array_push($categoryidArray, $category->getCategoryid());
				$catid = $category->getCategoryid();

				$categorylist= $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$catid));

				foreach($categorylist as $categoryname){
					$categorynameArray[$catid] = $categoryname->getCategory();
				}
			}
			return $this->render('BBidsBBidsHomeBundle:User:chooseleadpack.html.twig', array('count'=>$leadCount, 'categoryArray'=>$categorynameArray, 'categoryidArray'=>$categoryidArray, 'uid'=>$uid,'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'otcStatus'=>$otcStatus));
		}
		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function consumerregisterAction(Request $request)
	{
		$session = $this->container->get('session');
		$user = new User();

		$form = $this->createFormBuilder($user)
			->add('contactname','text', array('label'=>'Enter your full name', 'mapped'=>FALSE,'attr'=>array('autocomplete' => 'off')))
			->add('email','email',array('label'=>'Enter Your email', 'attr'=>array('size'=>40,'autocomplete' => 'off')))
			->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Enter Mobile Number ','empty_value'=>'Area Code', 'mapped'=>FALSE))
			->add('smsphone','text',array('mapped'=>FALSE, 'invalid_message'=>'Mobile Number field cannot contain strings','attr'=>array('size'=>7,'autocomplete' => 'off')))
			->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'required'=>FALSE,'label'=>'Enter your phone number ','empty_value'=>'Area Code', 'mapped'=>FALSE))
			->add('homephone','text', array('mapped'=>FALSE,'required'=>FALSE,'attr'=>array('size'=>7, 'placeholder'=>'Phone number','autocomplete' => 'off')))
			->add('password','repeated', array(
						'mapped'=>FALSE,
						'type'=>'password',
						'invalid_message'=>'The password fields did not match',
						'options'=>array('attr'=>array('class'=>'password-failed')),
						'first_options' => array('label'=>'Password'),
						'second_options'=> array('label'=>'Confirm Password'),
					))
			->add('Register','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST'))
		{
			if($form->isValid())
			{
				/*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);


                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
                }

				$contactname = $form['contactname']->getData();

				if($contactname == "") {
                    $session->getFlashbag()->add('error','Contact name field should not be left blank' );
                	return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
				}
				$smsphone = $form['smsphone']->getData();
				if(!is_numeric($smsphone)){
	                $session->getFlashbag()->add('error','Mobile number should not contain strings');
	                return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
				}
				if(strlen($smsphone) != 7) {
                    $session->getFlashbag()->add('error','Mobile number field should be of 7 digits');
                    return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
				}

				$smsphone = $form['mobilecode']->getData()."-".$form['smsphone']->getData();

				$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));

				if(!empty($account)) {
					$session->getFlashbag()->add('error','Mobile Number already exists. Please try a different mobile number');
					return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
				}


				$homecode  = $form['homecode']->getData();
				$homephone = $form['homephone']->getData();
				if($homecode) {
					if(strlen($homephone) != 7) {
						$session->getFlashbag()->add('error','Phone number field should be of 7 digits');
						return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
					}
				}
				if($homephone) {
					if(!is_numeric($homephone) || !is_numeric($homecode)) {
                    	$session->getFlashbag()->add('error','Home phone should not contain strings');
                    	return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
					}
				}
				$email = $form['email']->getData();

				$userArray  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email));

                foreach($userArray as $uses){
                    $uid = $uses->getId();
                }

				if(isset($uid)) {
					$session->getFlashbag()->add('error','Email id already exists. Please try a different email');
					return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
                } else {
					$mobile = $form['mobilecode']->getData();
					$sms    = $form['smsphone']->getData();
					$home   = $form['homecode']->getData();
					$name   = $form['contactname']->getData();

					$password = $form['password']->getData();
					$smsphone = $form['mobilecode']->getData()."-".$form['smsphone']->getData();

					if(!empty($home)){
						$homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
					}
					else{
						$homephone = "";
					}

					$user = new User();

					$created = new \DateTime();
					$userhash = $this->generateHash(16);
					$mobilecode = rand(100000,999999);

					$user->setPassword(md5($password));

					$user->setEmail($email);
					$user->setCreated($created);
					$user->setUpdated($created);
					$user->setUserhash($userhash);
					$user->setStatus(1);
					$user->setMobilecode($mobilecode);

					$em = $this->getDoctrine()->getManager();
					$em->persist($user);
					$em->flush();

					$userid = $user->getId();

					$account = new Account();

					$account->setProfileid(2);
					$account->setUserid($userid);
					$account->setSmsphone($smsphone);
					$account->setHomephone($homephone);
					$account->setContactname($name);

					$em = $this->getDoctrine()->getManager();
					$em->persist($account);
					$em->flush();
					/*//$smsphoneArray = explode("-",$smsphone);
					$smsphone      = $smsphone;
					$text = "Thank you for using BusinessBid. Please enter this code $mobilecode on the verification field online to complete activation process.";
					$response = $this->sendSMSToPhone($smsphone, $text);

					return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verify', array('smscode'=>$mobilecode, 'userid'=>$userid, 'path'=>'login','enquiryid'=>0)));*/
					$this->sendWelcomeEmail($name,$userid,$email,$password);

					$session->getFlashBag()->add('success','Your successfully registered to BusinessBid Network. Please login with the same details.');

					$session->set('uid',$userid);
					$session->set('email',$email);
					$session->set('pid', 2);
					$session->set('name', $name);
            		return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_home'));
				}
			}
			else
			{
				return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
			}
		}
		return $this->render('BBidsBBidsHomeBundle:Home:consumerregister.html.twig', array('error'=>'','form'=>$form->createView()));
	}

	public function accountAction(Request $request)
	{
		$session = $this->container->get('session');

		if($session->has('uid')){

			$uid = $session->get('uid');

			$user = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));

			$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

					//print_r($account);exit;

				$em = $this->getDoctrine()->getManager();
				$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);

		$useridexists = $query->getQuery()->getSingleScalarResult();
			$query = $em->createQueryBuilder()
					->select('count(f.userid)')
					->from('BBidsBBidsHomeBundle:Freetrail', 'f')
					->add('where','f.userid = :uid')
					->setParameter('uid', $uid);

		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
		$certArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcertificationrel')->findBy(array('vendorid'=>$uid));

		$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcityrel')->findBy(array('vendorid'=>$uid));

		$productsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorproductsrel')->findBy(array('vendorid'=>$uid));

		$subquery = $em->createQueryBuilder()
			->select('c.category, vc.status')
			->from('BBidsBBidsHomeBundle:Categories', 'c')
			->innerJoin('BBidsBBidsHomeBundle:Vendorsubcategoriesrel', 'vc', 'with',  'vc.subcategoryid = c.id')
			->where('vc.vendorid = :vendorid')
			->setParameter('vendorid', $uid);

		$subcat = $subquery->getQuery()->getArrayResult();

		$catquery = $em->createQueryBuilder()
			->select('c.category')
			->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'vc')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with',  'vc.categoryid = c.id')
			->where('vc.vendorid = :vendorid')
			->setParameter('vendorid', $uid);

		$maincategory = $catquery->getQuery()->getArrayResult();
		$emailCount = $this->getMessageCount();

		$form = $this->createFormBuilder($user)
                ->add('subcategory0','text',array('attr'=>array('placeholder'=>'Enter the subcategory')))
                ->add('setdata','hidden',array('attr'=>array('id'=>'setid','value'=>0)))
                ->add('vendorid','hidden',array('attr'=>array('id'=>'vendorid','value'=>$uid)))
                ->add('Save','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                ->getForm();

        $form->handleRequest($request);


		$showcat = $em->createQueryBuilder()
			->select('c.category,c.id')
			->from('BBidsBBidsHomeBundle:Vendorservices', 'c')
			->where('c.vendorid = :vendorid')
			->setParameter('vendorid', $uid);

		$dispcat = $showcat->getQuery()->getArrayResult();

		if($request->isMethod('POST')) {
			if($form->isValid()) {

			}
		}

		return $this->render('BBidsBBidsHomeBundle:User:account.html.twig', array('certArray'=>$certArray, 'cityArray'=>$cityArray, 'productsArray'=>$productsArray, 'subcat'=>$subcat,'maincat'=>$maincategory, 'useridexists'=>$useridexists ,'useridexistsfreeleads'=>$useridexistsfreeleads,'user'=>$user, 'account'=>$account,'emailCount'=>$emailCount,'form'=>$form->createView(),'displaycat'=>$dispcat,'vendorid'=>$uid));
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
            return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}
	protected function sendSMSToPhone($phoneNumber, $text, $type='') {
		$phoneNumber = substr($phoneNumber, 1);

		if(strpos($text, 'for the customer information'))
            $senderid = 3579;
        else
            $senderid = 'Businessbids';

		$smsJson = '{
					"authentication": {
						"username": "agefilms",
						"password": "12345Uae"
					},
					"messages": [
						{
							"sender": "'.$senderid.'",
							"text": "'.$text.'",';
		if($type) {
			$smsJson .= '"type":"longSMS",';
		}
		$smsJson .=	'
							"recipients": [
								{
									"gsm": "971'.$phoneNumber.'"
								}
							]
						}
					]
				}';
		$encodeSMS = base64_encode(json_encode($smsJson));
		$ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json',
			'Host: api.infobip.com',
			'Accept: */*',
		    'Content-Length: ' . strlen($smsJson))
		);

		$result = curl_exec($ch);
		// $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		// echo "status : ".$http_status;
		// echo 'response: '.$result;
		$response        = new Response($result);
		$encodedResponse = base64_encode(json_encode($response));

		$em = $this->getDoctrine()->getManager();
		$connection = $em->getConnection();
		$sentSMS = $connection->prepare("INSERT INTO `bizbids`.`bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, $phoneNumber, '$encodeSMS', '$encodedResponse', NOW())");
		$sentSMS->execute();

		return $response;
	}

	public function smsverifyAction(Request $request, $smscode, $userid, $path, $enquiryid)
	{
		$user = new User();

		$form = $this->createFormBuilder($user)
                ->add('mobilecode','number',array('label'=>'Please enter the SMS code you have received to verify your mobile number', 'attr'=>array('size'=>7)))
                ->add('userid','hidden', array('mapped'=>FALSE, 'data'=>$userid))
                ->add('Verify','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                ->getForm();
        $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

		// Send sms to mobile number provided.
		$smsphone      = $accountArray->getSmsphone();
		$smsphoneArray = explode("-",$smsphone);
		$smsphone      = $smsphoneArray[0].$smsphoneArray[1];

		//Code commented by Anup. This sends sms twice. once while loading the form, and second when the form is submitted.
		/*
        $text = "Thank you for using BusinessBid. Please enter this code $smscode on the verification field online to complete activation process.";
        $response = $this->sendSMSToPhone($smsphone, $text); */

		$form->handleRequest($request);
		$em = $this->getDoctrine()->getManager();
		if($request->isMethod('POST')) {
			if($form->isValid()) {
				$mobilecode = $form['mobilecode']->getData();
				$userid     = $form['userid']->getData();

				$userArray  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid));

				foreach($userArray as $uses) {
					$smscode = $uses->getMobilecode();
					$email   = $uses->getEmail();
					$status  = $uses->getStatus();
				}
				if($smscode == $mobilecode){
					$user = new User();
					$userhash = $this->generateHash(16);

					$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($userid);

					$user->setMobilecode('');
					$user->setStatus(2);
					$user->setUserhash($userhash);
					$em->flush();

					$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));

					foreach($accountArray as $a){
						$profileid = $a->getProfileid();
						$name      = $a->getContactname();
					}
					if($path == 'inline') {
						$enquiry = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);
						$enquiry->setStatus(1);
						$em->flush();
					}

					$body = '';
					if($profileid == 2) {
						$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activationemail.html.twig', array('userid'=>$userid, 'userhash'=>$userhash,'status'=>$status,'name'=>$name));
					}
					else {
						$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:activationemailvendor.html.twig', array('userid'=>$userid, 'userhash'=>$userhash,'status'=>$status,'name'=>$name));
					}


					$useremail = new Usertoemail();

					$useremail->setFromuid(11);
					$useremail->setTouid($userid);
					$useremail->setFromemail('infosupport@businessbid.ae');
					$useremail->setToemail($email);
					$useremail->setEmailsubj('Welcome to the BusinessBid Network');
					$useremail->setCreated(new \Datetime());
					$useremail->setEmailmessage($body);

					$useremail->setEmailtype(1);
					$useremail->setStatus(1);
        			$useremail->setReadStatus(0);

					$em->persist($useremail);
					$em->flush();

					$url     = 'https://api.sendgrid.com/';
					$user    = 'businessbid';
					$pass    = '!3zxih1x0';
					$subject = 'Welcome to the BusinessBid Network';

					$message = array(
						'api_user'  => $user,
						'api_key'   => $pass,
						'to'        => $email,
						'subject'   => $subject,
						'html'      => $body,
						'text'      => $body,
						'from'      => 'infosupport@businessbid.ae',
					 );


					$request =  $url.'api/mail.send.json';

					// Generate curl request
					$sess = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($sess, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					// Tell curl not to return headers, but do return the response
					curl_setopt($sess, CURLOPT_HEADER,false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					// obtain response
					$response = curl_exec($sess);
					curl_close($sess);

					return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verified'));
				} else{
					$session = $this->container->get('session');
					$session->getFlashbag()->add('error','The code did not match at our end.');
					return $this->render('BBidsBBidsHomeBundle:Home:smsverify.html.twig', array('form'=>$form->createView(), 'smscode'=>$smscode));
				}
			}
			else{
			 	return $this->render('BBidsBBidsHomeBundle:Home:smsverify.html.twig', array('form'=>$form->createView(), 'smscode'=>$smscode));
			}

		}
		return $this->render('BBidsBBidsHomeBundle:Home:smsverify.html.twig', array('form'=>$form->createView(), 'smscode'=>$smscode));
	}

	public function emailactivateAction($email, $userid)
	{
		$userhash = $this->generateHash(16);

				$em = $this->getDoctrine()->getManager();

					$useremail = new Usertoemail();

					$useremail->setFromuid(11);
					$useremail->setTouid($userid);
					$useremail->setFromemail('infosupport@businessbid.ae');
					$useremail->setToemail($email);
					$useremail->setEmailsubj('Welcome to the BusinessBid Network');
					$useremail->setCreated(new \Datetime());
					$useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activationemail.html.twig', array('userid'=>$userid, 'userhash'=>$userhash)));
					$useremail->setEmailtype(1);
					$useremail->setStatus(1);
        			$useremail->setReadStatus(0);

					$em->persist($useremail);
					$em->flush();


					$url = 'https://api.sendgrid.com/';
					$user = 'businessbid';
					$pass = '!3zxih1x0';
					$subject = 'Welcome to the BusinessBid Network';
					$body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activationemail.html.twig', array('userid'=>$userid, 'userhash'=>$userhash));

					$message = array(
					'api_user'  => $user,
					'api_key'   => $pass,
					'to'        => $email,
					'subject'   => $subject,
					'html'      => $body,
					'text'      => $body,
					'from'      => 'support@businessbid.ae',
					 );


					$request =  $url.'api/mail.send.json';

					// Generate curl request
					$sess = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($sess, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					// Tell curl not to return headers, but do return the response
					curl_setopt($sess, CURLOPT_HEADER, false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					// obtain response
					$response = curl_exec($sess);
					curl_close($sess);




                return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verified'));

	}
	public function smsverifiedAction()
	{
		 $session = $this->container->get('session');

                 $session->getFlashbag()->add('success','Your mobile number has been verified. Please check your email for further details to complete the registration process');

	         return $this->render('::basesuccess.html.twig');

	}
	public function internationaluserverifiedAction()
	{
		 $session = $this->container->get('session');

                 $session->getFlashbag()->add('success','Please check your email for further explanation.');

	         return $this->render('::basesuccess.html.twig');

	}

	public function generateRandom($length)
	{
		$characters = '0123456789';
                $randomString = '';

                for($i=0; $i< $length; $i++) {
                        $randomString .= $characters[rand(0, strlen($characters) - 1)];
                }

                return $randomString;
	}

	public function generateHash($length)
    {
		$characters   = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';

        for($i=0; $i< $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

	public function emailverifyAction(Request $request, $userid, $userhash)
	{

		$session = $this->container->get('session');
		$user = new User();
		$activateAuth = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid, 'userhash'=>$userhash));

		if(!empty($activateAuth)) {
			foreach($activateAuth as $user) {
				$uid   = $user->getId();
				$email = $user->getEmail();
			}
			$session->getFlashbag()->add('success','Please create a password to activate your account');
		}


		$em    = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
				->select(' a.status')
				->from('BBidsBBidsHomeBundle:User', 'a')
				->add('where','a.id = :uid')
				->setParameter('uid', $userid);

		$statusaccount = $query->getQuery()->getResult();
                //print_r($useraccount);exit;

     	$useraccount = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid));
		foreach($useraccount as $uses){
			$email  = $uses->getEmail();
			$status = $uses->getStatus();
		}

		//print_r($status);exit;

		if(isset($uid)) {
			$form = $this->createFormBuilder($user)
                    ->add('password','repeated', array(
						'type'=>'password',
						'invalid_message'=>'The password fields did not match',
						'options'=>array('attr'=>array('class'=>'password-failed')),
						'first_options' => array('label'=>'Password'),
						'second_options'=> array('label'=>'Confirm password'),
					))
			        ->add('Activate','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                    ->getForm();

			$form->handleRequest($request);

			if($request->isMethod('POST')) {
				if($form->isValid()) {
					$password = md5($form['password']->getData());
					$em = $this->getDoctrine()->getManager();

					$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));

					foreach($accountArray as $account){
						$pid = $account->getProfileid();
						$name = $account->getContactname();
					}

                   	$userstatus = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid));
                   	foreach($userstatus as $s){
						$statusaccount = $s->getStatus();
					}

                   $username = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$userid));

					foreach($username as $n) {
                    	$name = $n->getContactname();
                    	$smsphone = $n->getSmsphone();
					}

					$smsphoneArray = explode("-",$smsphone);
					$smsphone      = $smsphoneArray[0].$smsphoneArray[1];

					$em = $this->getDoctrine()->getManager();

					$type = (isset($_GET['type'])) ? $_GET['type'] : '';

					$subject = ($type == 'fp') ? 'BusinessBid Password Reset Confirmation' : 'Welcome to the BusinessBid Network';
					if($pid==2) {
						$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activateemailstatuscustom.html.twig', array('useraccount'=>$statusaccount,'name'=>$name,'type'=>$type));
					}
					else {
						$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:activateemailstatus.html.twig', array('useraccount'=>$statusaccount,'name'=>$name,'type'=>$type));
					}

					$useremail = new Usertoemail();

					$useremail->setFromuid(11);
					$useremail->setTouid($uid);
					$useremail->setFromemail('infosupport@businessbid.ae');
					$useremail->setToemail($email);
					$useremail->setEmailsubj($subject);
					$useremail->setCreated(new \Datetime());
					$useremail->setEmailmessage($body);
					$useremail->setEmailtype(1);
					$useremail->setStatus(1);
        			$useremail->setReadStatus(0);

					$em->persist($useremail);
					$em->flush();

					$url     = 'https://api.sendgrid.com/';
					$user    = 'businessbid';
					$pass    = '!3zxih1x0';

					$message = array(
								'api_user'  => $user,
								'api_key'   => $pass,
								'to'        => $email,
								'subject'   => $subject,
								'html'      => $body,
								'text'      => $body,
								'from'      => 'infosupport@businessbid.ae',
						 	);


					$request =  $url.'api/mail.send.json';

					// Generate curl request
					$sess = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($sess, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					// Tell curl not to return headers, but do return the response
					curl_setopt($sess, CURLOPT_HEADER, false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					// obtain response
					$response = curl_exec($sess);
					curl_close($sess);


				  	$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

					$user->setPassword($password);
					$user->setUserhash('');
					$user->setStatus(1);

					$em->flush();

					$session = $this->container->get('session');

					$session->set('uid',$uid);
					$session->set('pid',$pid);
					$session->set('email',$email);
					$session->set('name', $name);

					$enquiryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Enquiry')->findBy(array('authorid'=>$userid));

					$subArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>0));

					$subList = array();
					foreach($subArray as $sub){
						$subid = $sub->getId();
						$subname = $sub->getCategory();
						$subList[$subid] = $subname;
					}

					if(false)//if(!empty($enquiryArray))
					{
						$em = $this->getDoctrine()->getManager();
						foreach($enquiryArray as $e){
							$city        = $e->getCity();
							$hire        = $e->getHirePriority();
							$description = $e->getDescription();
							$enquiryid   = $id = $e->getId();

							$enquiry   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($id);
							$enquiry->setStatus(1);

							$em->flush();

							$enquirySubcategory  = $em->getRepository('BBidsBBidsHomeBundle:EnquirySubcategoryRel')->findBy(array('enquiryid'=>$enquiryid));
							$eSubCategoryString = '';
							foreach ($enquirySubcategory as $eSubCategory) {
								$eSubCategoryID = $eSubCategory->getSubcategoryid();
								$eSubCategoryString .= $subList[$eSubCategoryID].", ";
							}
							if(strlen($eSubCategoryString) > 50){
								$eSubCategoryString = substr($eSubCategoryString,0,50).'...';
							}
							$smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString & vendors will contact you shortly.";
							$response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');

							// Send sms to all vendors after email verification for JR
							$hireArray = array('No Priority', 'Urgent', '1-3 days', '4-7 days', '7-14 days', 'Just Planning','Flexible Dates');

				            $formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));
				            $extraInfoSMS = $emailBody = '';
				            $extraInfoSMS .= "Job ref no : ".$enquiryid.'\r\n';
				            $extraInfoSMS .= $eSubCategoryString.'\r\n';
				            if($formExtraFields) {
				                foreach ($formExtraFields as $field) {
				                    $extraInfoSMS .= $field->getKeyName().' : '.$field->getKeyValue().'\r\n';
				                }
				                $emailBody = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig',array('formExtraFields'=>$formExtraFields,'jobNo'=>$enquiryid, 'city'=>$city,'hire'=>$hire, 'description'=>$description, 'eSubCategoryString'=>$eSubCategoryString));
				            }
				            $extraInfoSMS .= "City : ".$city.'\r\n';
				            $extraInfoSMS .= "Priority : ".$hireArray[$hire].'\r\n';
				            if($description)
				                $extraInfoSMS .= "Info : ".$description.'\r\n';

							$enqMapedVendList  = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('enquiryid'=>$enquiryid));
							foreach ($enqMapedVendList as $enqMapedVend) {
								$vendorID = $enqMapedVend->getVendorid();
								$vendAccArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorID));
								if($vendAccArray) {
		                            // Send sms to mobile number provided.
		                            $vendorName    = $vendAccArray->getContactname();
		                            $smsphone      = $vendAccArray->getSmsphone();
		                            $smsphoneArray = explode("-",$smsphone);
		                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

		                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
		                            $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
		                            /*End of sms to vendors*/

		                            // Send email notification to all vendors
		                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorID));
		                            if($vendorUser) {
		                                $vendorEmail = $vendorUser->getEmail();
		                                $this->sendVendorNofifyEmail($vendorID,$vendorEmail,$enquiryid,$emailBody,$vendorName);
		                            }
		                        }
							} /*End of automated mapped vendor list*/

							// Manual mapped vendors sms and email
							$manualVendCatMapping = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findBy(array('categoryID'=>$categoryid,'status'=>1));

							foreach ($manualVendCatMapping as $manualMappedVend) {
								$vendorID = $manualMappedVend->getVendorID();
								$vendAccArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorID));
								if($vendAccArray) {
		                            // Send sms to mobile number provided.
		                            $vendorName    = $vendAccArray->getContactname();
		                            $smsphone      = $vendAccArray->getSmsphone();
		                            $smsphoneArray = explode("-",$smsphone);
		                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

		                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
		                            $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
		                            /*End of sms to vendors*/

		                            // Send email notification to all vendors
		                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorID));
		                            if($vendorUser) {
		                                $vendorEmail = $vendorUser->getEmail();
		                                $this->sendVendorNofifyEmail($vendorID,$vendorEmail,$enquiryid,$emailBody,$vendorName);
		                            }
		                        }
							} /*End of manuall mapped vendor list*/

						}
					}

					if($pid == 2){
						if(false)//if(!empty($enquiryArray))
						{
							/*return $this->redirect($this->generateUrl('b_bids_b_bids_enquiry_processing'));*///Returning to enquiry progress if enquiry exist
							$this->sendEnquiryConfirmationEmail($uid,$email,$enquiryid);
							return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
						}
						else
							return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
					}
					if($pid == 3) {
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_home'));
					}

				} else {
					return $this->render('BBidsBBidsHomeBundle:Home:accountactivate.html.twig', array('form'=>$form->createView(),'useraccount'=>$statusaccount,'status'=>$status));
				}
			}

			return $this->render('BBidsBBidsHomeBundle:Home:accountactivate.html.twig', array('form'=>$form->createView(),'useraccount'=>$statusaccount,'status'=>$status));
		} else
		{

			$session->getFlashBag()->add('error','The link has expired. Please click on forgot password link if you have forgotten your password.');
			return $this->render('::error.html.twig');
		}
	}

	public function emailverifiedAction()
    {
		$session = $this->container->get('session');

		$session->getFlashbag()->add('success','Your account is now verified. Please check your email for further explanation.');

		return $this->render('::basesuccess.html.twig');
    }

	public function vendorregisterAction(Request $request)
	{
		$session = $this->container->get('session');
		$user = new User();

                $form = $this->createFormBuilder($user)
                        ->add('email','email',array('label'=>'Enter Your Email', 'attr'=>array('size'=>40)))
                        ->add('smscode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Enter Mobile Number ','empty_value'=>'Area Code', 'mapped'=>FALSE))
                        ->add('smsphone','text',array('mapped'=>FALSE, 'invalid_message'=>'Mobile Number field cannot contain strings','attr'=>array('size'=>7)))
				        ->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'label'=>'Enter Your Phone Number ','empty_value'=>'Area Code', 'mapped'=>FALSE))
						->add('homephone','text', array('mapped'=>FALSE, 'attr'=>array('size'=>7, 'placeholder'=>'Phone number')))
						->add('address','textarea', array('label'=>'Enter your address', 'mapped'=>FALSE))
						->add('bizname','text', array('label'=>'Enter Your Business/Company Name', 'mapped'=>FALSE))
						->add('contactname','text', array('label'=>'Enter Your Full Name', 'mapped'=>FALSE ))
						->add('faxcode','text',array('label'=>'Enter Your Fax Phone', 'mapped'=>FALSE, 'required'=>FALSE, 'attr'=>array('size'=>5, 'placeholder'=>'Area code')))
                        ->add('faxphone','text', array('mapped'=>FALSE, 'required'=>FALSE, 'attr'=>array('size'=>12, 'placeholder'=>'Phone number')))
                        /*->add('verification', 'text', array('required'=>TRUE,'mapped'=>FALSE,'label'=>'','attr'=>array('placeholder'=>'Enter the text shown in image')))*/
                        ->add('Register','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                        ->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){

				/*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

				if ($captchaFlag != 'Valid') {

                    $session = $this->container->get('session');
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('error'=>'','form'=>$form->createView()));
                }

				// print_r($request);exit;
				$smscode = $form['smscode']->getData();
				$smsphone = $form['smsphone']->getData();
				//echo $smscode.'<<>>'.$smsphone;
				$email = $form['email']->getData();
				$contactname = $form['contactname']->getData();
				if($contactname == ""){

					$session->getFlashbag()->add('error','Contact name field should not be left blank' );
					return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('error'=>'','form'=>$form->createView()));
				}

                $smsphone = $form['smsphone']->getData();
				if(!is_numeric($smsphone)){

					$session->getFlashBag()->add('error','Mobile number field cannot contain strings');
					return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('form'=>$form->createView()));
				}
				if(strlen($smsphone) != 7){

					$session->getFlashBag()->add('error','Mobile number field should be of 7 digits');
					return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('form'=>$form->createView()));
				}

				$smsphone = $form['smscode']->getData()."-".$form['smsphone']->getData();

                $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone,'profileid'=>3));

                if(!empty($account)) {
                    $session->getFlashbag()->add('error','Mobile Number already exists. Please try a different mobile number');
                    return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('form'=>$form->createView()));
                }

				$homecode = $form['homecode']->getData();
                $homephone = $form['homephone']->getData();

             	if($homephone != "" || $homecode != "") {
                 	if(strlen($homephone) != 7) {

						$session->getFlashBag()->add('error','Home Phone number field should be of 7 digits');
						return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('form'=>$form->createView()));
					}
				}

				if($homephone != "" || $homecode != "") {
					if(!is_numeric($homephone) || !is_numeric($homecode)) {

						$session->getFlashbag()->add('error','Home phone should not contain strings');
						return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('error'=>'','form'=>$form->createView()));
					}
				}

				$faxcode  = $form['faxcode']->getData();
				$faxphone = $form['faxphone']->getData();

				if($faxphone != "" || $faxcode != ""){
                	if(!is_numeric($faxphone) || !is_numeric($faxcode)){

						$session->getFlashbag()->add('error','Fax number should not contain strings');
						return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('error'=>'','form'=>$form->createView()));
                    }
				}


				$userArray  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email));

				foreach($userArray as $uses){
					$uid = $uses->getId();
				}
                // print_r($userArray);exit;
                if($userArray)
                {
                	$userArray2  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
					foreach($userArray2 as $p)
					{
						$aid = $p->getId();
						$profileid = $p->getProfileid();
					}
				}
				//echo 'hcghjgv';exit;
                if(isset($uid)&& $profileid ==3) {

                    $session->getFlashbag()->add('error','Email id already exists. Please try a different email');
                    return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('error'=>'','form'=>$form->createView()));
                }

                if(!empty($form['homecode']->getData)){
                	$homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                } else {
					$homephone = "";
        		}

				$address     = $form['address']->getData();
				$bizname     = $form['bizname']->getData();
				$contactname = $form['contactname']->getData();
				$session     = $this->container->get('session');

				if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
					$session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
					return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('error'=>'','form'=>$form->createView()));
				}
				if(!empty($form['faxcode']->getData)){
					$faxphone = $form['faxcode']->getData()."-".$form['faxphone']->getData();
				} else{
					$faxphone = "";
				}

                if(isset($profileid)) {
					if($profileid !=3) {
						$smscode = $form['smscode']->getData();
						$smsphone = $form['smsphone']->getData();
						$smsphone2 = $smscode."-".$smsphone;
						$homecode = $form['homecode']->getData();
						$homephone=$form['homephone']->getData();
						$homephone2=$homecode."-".$homephone;
						$faxphone2 = $form['faxcode']->getData()."-".$form['faxphone']->getData();

						$session = $this->container->get('session');

						$session->set('puid',$uid);
						$session->set('ppid',$profileid);
						$session->set('pemail',$email);
						$session->set('pbizname',$bizname);
						$session->set('pname', $contactname);
						$session->set('phomephone', $homephone2);
						$session->set('psmsphone', $smsphone2);
						$session->set('paddress', $address);
						$session->set('pfaxphone', $faxphone);

						// echo $session->get('bizname');exit;
						return $this->redirect($this->generateUrl('b_bids_b_bids_user_custtovendorupgration_check',array('userid'=>$uid)));
					//$this->custovendorupgrationcheck($uid);exit;
				}
			}
			else {

				$user = new User();

				$created = new \DateTime();
				$userhash = $this->generateHash(16);
				$mobilecode = rand(100000,999999);

				$user->setEmail($email);
				$user->setCreated($created);
				$user->setUpdated($created);
				$user->setUserhash($userhash);
				$user->setStatus(0);
				$user->setMobilecode($mobilecode);

				$em = $this->getDoctrine()->getManager();
				$em->persist($user);
				$em->flush();

				$userid = $user->getId();

				$rating = new Ratings();

				$rating->setVendorid($userid);
				$rating->setRating('0.000');

				$em->persist($rating);

				$created = new \DateTime();
				/*$reviews = new Reviews();

				$reviews->setVendorid($userid);
				$reviews->setServiceexperience('noval');
				$reviews->setReview('noval');
				$reviews->setAuthor('noval');
				$reviews->setCreated($created);
				$reviews->setModstatus(0);
				$reviews->setStatus(0);
				$reviews->setRating('0.0000');
				$reviews->setEnquiryid(001);
				$reviews->setAuthorid(000);
				$reviews->setBusinessknow('noval');
				$reviews->setBusinessrecomond('noval');
				$reviews->setServicesummary('noval');
				$reviews->setShowname('noval');
				//             $reviews->setNotchoose($notchoose);
				$reviews->setServiceexperience('noval');

				$em->persist($reviews);
				$em->flush();*/

				$smscode   = $form['smscode']->getData();
				$smsphone  = $form['smsphone']->getData();
				$smsphone2 = $smscode."-".$smsphone;
				$homecode  = $form['homecode']->getData();
				$homephone = $form['homephone']->getData();

				$homephone2 = $homecode."-".$homephone;
				//echo $homephone2.'<<>>'.$homecode.'<<>>'.$homephone;exit;



				$account = new Account();

				$account->setProfileid(3);
				$account->setUserid($userid);
				$account->setSmsphone($smsphone2);
				$account->setHomephone($homephone2);
				$account->setAddress($address);
				$account->setBizname($bizname);
				$account->setContactname($contactname);
				$account->setFax($faxphone);

				$em = $this->getDoctrine()->getManager();
				$em->persist($account);
				$em->flush();

				$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll(); // mapp to all cities

				foreach($cityArray as $ci){
					$city = $ci->getId();

					$vcity = new Vendorcityrel();

					$vcity->setVendorid($userid);
					$vcity->setCity($city);

					$em->persist($vcity);
					$em->flush();
				}

				$smsphone      = $smscode.$smsphone;
				$text = "Thank you for using BusinessBid. Please enter this code $mobilecode on the verification field online to complete activation process.";
				$response = $this->sendSMSToPhone($smsphone, $text);

				 return $this->redirect($this->generateUrl('b_bids_b_bids_user_sms_verify', array('smscode'=>$mobilecode, 'userid'=>$userid, 'path'=>'login','enquiryid'=>0)));
			}

			}
			else {
				return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('form'=>$form->createView()));
			}
		}
		return $this->render('BBidsBBidsHomeBundle:Home:vendorregister.html.twig', array('form'=>$form->createView()));
	}

	public function vendorcategoriesAction(Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){
		$uid = $session->get('uid');
		$categories = new Categories();

		$segmentArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>111111));

		$segmentList = array();
		foreach($segmentArray as $segment){
			$segmentid = $segment->getId();
			$segmentname = $segment->getCategory();
			$segmentList[$segmentid] = $segmentname;
		}

		$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0));

		$categoryList = array();

		foreach($categoriesArray as $category){
			$categoryid = $category->getId();
			$categoryname = $category->getCategory();
			$categoryList[$categoryid] = $categoryname;
		}

		$subArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>0));

		$subList = array();

		foreach($subArray as $sub){
			$subid = $sub->getId();
			$subname = $sub->getCategory();
			$subList[$subid] = $subname;
		}


		$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

		$cityList = array();

		foreach($cityArray as $city){
			$cityid = $city->getId();
			$cityname = $city->getCity();
			$cityList[$cityid] = $cityname;

		}


		$form = $this->createFormBuilder($categories)
			->add('segment','choice', array('label'=>'Please choose your segment ', 'choices'=>$segmentList, 'multiple'=>TRUE, 'expanded'=>TRUE, 'mapped'=>FALSE, 'required'=>FALSE))
			->add('category','choice', array('label'=>'Please choose your category ', 'choices'=>$categoryList, 'multiple'=>TRUE, 'expanded'=>TRUE, 'required'=>FALSE))
			->add('subcategory','choice', array('label'=>'Please choose your segment ', 'choices'=>$subList, 'multiple'=>TRUE, 'expanded'=>TRUE, 'mapped'=>FALSE, 'required'=>FALSE))
			->add('city','choice', array('label'=>'Please choose your city', 'choices'=>$cityList, 'multiple'=>TRUE, 'expanded'=>TRUE, 'mapped'=>FALSE))
			->add('BuyLeads','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center', 'value'=>'Buy Leads')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){

				$segmentcount = count($form['segment']->getData());
				$categorycount = count($form['category']->getData());
                                $subcategorycount = count($form['subcategory']->getData());
                                $citycount = count($form['city']->getData());

				$em = $this->getDoctrine()->getManager();
				$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
		$useridexists = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);

		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
				if($segmentcount == 0 || $categorycount == 0 || $citycount == 0){


					$session = $this->container->get('session');



                                        $session->getFlashbag()->add('error','Selecting of one item each from Segment, Category, Subcategory, & City');

                                        return $this->render('BBidsBBidsHomeBundle:User:selectcategories.html.twig',array('form'=>$form->createView(),'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads));
				}

				$session  = $this->container->get('session');

				$uid = $session->get('uid');

				$categoryArray = $form['category']->getData();

				$sessionArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessioncategoriesrel')->findBy(array('userid'=>$uid));

				foreach($sessionArray as $sessiondata){
                                                $sid = $sessiondata->getId();

                                                $em = $this->getDoctrine()->getManager();
                                                $sessionrow = $em->getRepository('BBidsBBidsHomeBundle:Sessioncategoriesrel')->find($sid);

                                                $em->remove($sessionrow);
                                                $em->flush();
                                        }


				for($i = 0; $i < $categorycount; $i++){
					$sessioncategory = new Sessioncategoriesrel();

					$sessioncategory->setUserid($uid);
					$sessioncategory->setCategoryid($categoryArray[$i]);

					$em = $this->getDoctrine()->getManager();
					$em->persist($sessioncategory);
					$em->flush();
				}

				$subcategoryArray = $form['subcategory']->getData();

                                for($i = 0; $i < $subcategorycount; $i++){
                                        $vendor = new Vendorsubcategoriesrel();

                                        $vendor->setVendorid($uid);
                                        $vendor->setSubcategoryid($subcategoryArray[$i]);
                                        $vendor->setStatus(0);

                                        $em = $this->getDoctrine()->getManager();
                                        $em->persist($vendor);
                                        $em->flush();
                                }


                  $cityArray = $form['city']->getData();

						$ccount = count($cityArray);

						if($ccount == 0){
							$session->getFlashBag()->add('error', 'Please choose your city of service');

							return $this->render('BBidsBBidsHomeBundle:User:selectcategories.html.twig',array('form'=>$form->createView(), 'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads));
						}

						for($j=0; $j < $ccount; $j++){
								$citi = $cityArray[$j];
						$newcitycheck=array();
						$newcitycheck = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcityrel')->findBy(array('vendorid'=>$uid,'city'=>$citi));

								if(empty($newcitycheck))
								{
								$vendorcity = new Vendorcityrel();

								$vendorcity->setVendorid($uid);
								$vendorcity->setCity($citi);

								$em->persist($vendorcity);
								$em->flush();

								}

						}


				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_choose_lead_pack'));
		}
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
		$useridexists = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
				 //->setParameter('uid', 333);
			// echo $myquery = $query->getQuery()->getSQL();
		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
		return $this->render('BBidsBBidsHomeBundle:User:selectcategories.html.twig',array('form'=>$form->createView(), 'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads));
		}

		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function logoutAction()
	{
		$session = $this->container->get('session');
		$this->get('session')->clear();
		// $session->invalidate();

		return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
	}

	public function loginAction(Request $request, $returnurl)
	{
		$session = $this->container->get('session');
		$retpath = explode('+',$returnurl);

		$em = $this->getDoctrine()->getManager();

		if($session->has('uid'))
		{
			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

		$user = new User();

		$form = $this->createFormBuilder($user)
			->add('email','email', array('label'=>'Enter your email id', 'attr'=>array('size'=>30, 'placeholder'=>'Email ID')))
			->add('password','password', array('label'=>'Enter your password', 'attr'=>array('size'=>18, 'placeholder'=>'Password')))
			->add('Login','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				$email = $form['email']->getData();
				$password = md5($form['password']->getData());

				$user = new User();

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email, 'password'=>$password));
				foreach($userArray as $user){
					$uid = $user->getId();
					$logincount = $user->getLogincount();
				}

				if(isset($uid)){

					foreach($userArray as $user){
						$lastAccess = new \DateTime();
						$logincount += 1;
						$user->setLastaccess($lastAccess);
						$user->setLogincount($logincount);

						$em->persist($user);
						$em->flush();
					}

					$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

					foreach($accountArray as $account){
						$pid = $account->getProfileid();
						$name = $account->getContactname();

					}

					$session = $this->container->get('session');

					$session->set('uid',$uid);
					$session->set('email',$email);
					$session->set('pid', $pid);
					$session->set('name', $name);
					if($returnurl != 1){
						if($retpath[0] == 'b_bids_b_bids_vendor'){
							$vendorid = $retpath[1];
							return $this->redirect('/vendor/#rating', array('userid'=>$vendorid));
						}
					}
					else if($pid == 2){
						return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_home'));
					}
					else{
						return $this->redirect($this->generateUrl('b_bids_b_bids_home'));
					}
				}
				else {
					$session = $this->container->get('session');

					$session->getFlashbag()->add('error','Please check your login credentials and try again or email support@businessbid.ae');

					return $this->render('BBidsBBidsHomeBundle:User:login.html.twig',array('form'=>$form->createView(),'title'=>'BusinessBid Account Login Page'));
				}

			}
			else{
				return $this->render('BBidsBBidsHomeBundle:User:login.html.twig', array('form'=>$form->createView(),'title'=>'BusinessBid Account Login Page'));
			}
		}

		return $this->render('BBidsBBidsHomeBundle:User:login.html.twig', array('form'=>$form->createView(),'title'=>'BusinessBid Account Login Page'));
	}


	public function facebookloginAction($userid,$email,$fname,$pid)
	{
		 $session = $this->container->get('session');

		// echo $userid.'<<>>'.$email.'<<>>'.$fname.'<<>>'.$pid;exit;
		if($session->has('uid'))
		{
			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

		else
		{
			$session->set('pid',$pid);
			$session->set('uid',$userid);
			$session->set('email',$email);
			$session->set('name', $fname);

			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));

		}

	}

	public function homeAction()
	{
		$session = $this->container->get('session');

		$pid = $session->get('pid');

		if($pid == 2){
			return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_home'));
		} else if($pid == 3){
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_home'));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function vendorhomeAction()
	{

		$session = $this->container->get('session');
		if($session->has('uid')){
		$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();
					$year = date('Y');



			$query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                                ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
                                ->add('where','v.vendorid = :uid')
                                ->setParameter('uid', $uid);

			$totalenquirycount = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(o.id)')
				->from('BBidsBBidsHomeBundle:Order','o')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
                                ->where('o.author = :author')
                                ->setParameter('author', $uid);

			$totalordercount = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                                ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
                                ->where('v.vendorid = :uid')
                                ->andWhere('v.acceptstatus=1')
                                ->setParameter('uid', $uid);

			$totalusercountforapproval = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
		 $useridexists = $query->getQuery()->getSingleScalarResult();

		 $query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);

		 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
				->select('o.ordernumber, o.status, o.created, o.amount, o.author,o.id')
				->from('BBidsBBidsHomeBundle:Order', 'o')
				->add('where','o.author = :author')
			    ->add('orderBy', 'o.created DESC')
				->setMaxResults(10)
				->setParameter('author', $uid);

		$enquiries = $query->getQuery()->getResult();

		$queryorder = $em->createQueryBuilder()
				->select('c.category')
				->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'v.categoryid=c.id')
				->add('where','v.vendorid = :uid')
				->setParameter('uid', $uid);



		$orders = $queryorder->getQuery()->getResult();

		$queryenquiry = $em->createQueryBuilder()
						 ->select(array('a.userid,v.vendorid,v.enquiryid,c.category,e.subj, e.created, e.id, e.authorid,u.email, e.city,v.acceptstatus'))
						 ->from('BBidsBBidsHomeBundle:Enquiry','e')
						 ->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'v', 'WITH', 'e.id=v.enquiryid')
						 ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
						 ->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'v.vendorid=u.id')
                         ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id=a.userid')
						->add('where','v.vendorid = :uid')
						 ->andWhere('e.status = 1')
						 ->setParameter('uid', $uid)
						 ->add('orderBy', 'e.created DESC')
						 ->setMaxResults(10);
			$enquiriesvendor = $queryenquiry->getQuery()->getResult();

		 $queryenquirycount = $em->createQueryBuilder()
						 ->select('r.rating')
						 ->from('BBidsBBidsHomeBundle:Reviews','r')
						->add('where', 'r.vendorid = :vendorid')
						->setParameter('vendorid', $uid)
						->add('orderBy', 'r.created DESC');


		$enquiriesvendorcount2 = $queryenquirycount->getQuery()->getResult();
		if($enquiriesvendorcount2){
			$query = $em->createQueryBuilder()
						 ->select('count(r.id)')
						 ->from('BBidsBBidsHomeBundle:Reviews','r')
						->add('where', 'r.vendorid = :vendorid')
						->setParameter('vendorid', $uid)
						->add('orderBy', 'r.created DESC');


		$reviewcount = $query->getQuery()->getSingleScalarResult();

		$summaryTotals = Array();$n=0;
		 foreach($enquiriesvendorcount2 as $ratingvalue)
		 {

			  $n=($n+$ratingvalue['rating']);


		 }

		 $enquiriesvendorcount=$n/$reviewcount;
		}
		else{

			$enquiriesvendorcount=0;
		}

		$em = $this->getDoctrine()->getEntityManager();
			$sql="SELECT count(v.id) as rc FROM bbids_vendorenquiryrel v JOIN bbids_enquiry e ON e.id=v.enquiryid WHERE v.vendorid=$uid GROUP BY DATE(created) ORDER BY rc ASC ";
			$connection = $em->getConnection();
			$statement = $connection->prepare($sql);
			$statement->execute();
			$results = $statement->fetchAll();
			$res=array_map('current',$results);
			$c = count($res);
			for ($n=0;$n<$c;$n++) {
			  $res[$n] = (int) $res[$n];
			}

		//echo $res;exit;

		$series = array(array('name'=> "Enquiries per Month",'data'=>$res));
				//print_r($series);exit;
			$ob = new HighChart();
			$ob->chart->renderTo('linechart');
			$ob->title->text('Enquiries per Month');
			$ob->xAxis->title(array('text'=> "Days"));
			$ob->yAxis->title(array('text'=>'Number of enquiries'));
			$ob->series($series);

			$emailCount = $this->getMessageCount();
			return $this->render('BBidsBBidsHomeBundle:Home:vendor.html.twig',array('totalenquirycount'=>$totalenquirycount, 'totalordercount'=>$totalordercount, 'totalusercountforapproval'=>$totalusercountforapproval,'enquiries'=>$enquiries, 'enquiriesvendor'=>$enquiriesvendor, 'orders'=>$orders,'enquiriesvendorcount'=>$enquiriesvendorcount,'chart'=>$ob,'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));
		}

		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function consumerhomeAction($offset)
	{
        $session = $this->container->get('session');
        if($session->has('uid')){
			$uid   = $session->get('uid');
			$start = (($offset * 4) - 4);
			//$start = $offset;

			$from         = $start +1;
			$to           = $start + 4;
			$em           = $this->getDoctrine()->getManager();

			$queryenquiry = $em->createQueryBuilder()
                         ->select('e.id,e.subj,e.description,e.location,c.category,e.created')
                         ->from('BBidsBBidsHomeBundle:Enquiry','e')
						 ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
                        //->innerJoin('BBidsBBidsHomeBundle:City', 'a', 'WITH', 'e.city=a.id')
                         ->add('where','e.authorid = :uid')
                         ->andWhere('e.status = 1')
                         ->setParameter('uid', $uid)
                         ->add('orderBy', 'e.created DESC')
                         ->setFirstResult($start)
                         ->setMaxResults(4);
            $enquiries = $queryenquiry->getQuery()->getResult();

			$enqcount=count($enquiries);
			if($enqcount !=0) {
				for($i=0;$i<$enqcount;$i++) {
					$eid=$enquiries[$i]['id'];
				}
				$query = $em->createQueryBuilder()
	                	->select('e.id')
						->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
						->where('e.enquiryid = :eid')
						->andWhere('e.acceptstatus = 1')
	                    ->setParameter('eid', $eid);

				$mappedenquiry     = $query->getQuery()->getArrayResult();
				$countmappedvendor = count($mappedenquiry);
            } else {
				$countmappedvendor=0;
			}
		 	$query = $em->createQueryBuilder()
                        ->select('count(e.id)')
                        ->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                        ->where('e.authorid = :author')
                        ->setParameter('author',$uid);

            $enquiriescount = $query->getQuery()->getSingleScalarResult();

		  	$count = $enquiriescount;
		   	//echo $count;exit;
			if($to>$count) {
				$to = $count;
			}
			$emailCount = $this->getMessageCount();
			return $this->render('BBidsBBidsHomeBundle:Home:consumer.html.twig', array('enquiries'=>$enquiries,'from'=>$from, 'to'=>$to, 'count'=>$count ,'countmappedvendor'=>$countmappedvendor,'emailCount'=>$emailCount));
		}
		return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
	}

	public function old_consumerenquiriesAction($offset)
	{
		$session = $this->container->get('session');

		$uid = $session->get('uid');

		$start = (($offset * 4) - 4);
		$from  = $start +1;
		$to    = $start + 4;

		 $em = $this->getDoctrine()->getManager();
         $queryenquiry = $em->createQueryBuilder()
                         ->select('e.id,e.subj,e.description,e.location,c.category,e.created')
                         ->from('BBidsBBidsHomeBundle:Enquiry','e')
						 ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
						 ->add('where','e.authorid = :uid')
                         ->andWhere('e.status = 1')
                         ->add('orderBy', 'e.created DESC')
                         ->setParameter('uid', $uid);
                   $enquiries = $queryenquiry->getQuery()->getResult();
                   $enqcount=count($enquiries);

				if($enqcount !=0)
                {
				for($i=0;$i<$enqcount;$i++)
				{
					$eid=$enquiries[$i]['id'];
				}

				$query = $em->createQueryBuilder()
                 ->select('e.id')
					->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
					->where('e.enquiryid = :eid')
					->andWhere('e.acceptstatus = 1')
                     ->setParameter('eid', $eid);


                        $mappedenquiry = $query->getQuery()->getArrayResult();
                       $countmappedvendor = count($mappedenquiry);
                }
                else
                {
					$countmappedvendor=0;
				}
				$query = $em->createQueryBuilder()
                        ->select('count(e.id)')
                        ->from('BBidsBBidsHomeBundle:Enquiry', 'e')
                        ->where('e.authorid = :author')
                        ->setParameter('author',$uid);

                $enquiriescount = $query->getQuery()->getSingleScalarResult();

				$count = $enquiriescount;

				if($to>$count)
				{
					$to = $count;
				}



		return $this->render('BBidsBBidsHomeBundle:User:enquiriesconsumer.html.twig',array('enquiries'=>$enquiries,'from'=>$from, 'to'=>$to, 'count'=>$count ,'countmappedvendor'=>$countmappedvendor));
	}
	public function consumerenquiriesAction($offset)
	{
		$session = $this->container->get('session');

		$uid = $session->get('uid');

		if($session->has('uid')) {

		 $em = $this->getDoctrine()->getManager();
         $queryenquiry = $em->createQueryBuilder()
                         ->select('e.id,e.subj,e.description,e.location,c.category,e.created')
                         ->from('BBidsBBidsHomeBundle:Enquiry','e')
						 ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
						 ->add('where','e.authorid = :uid')
                         ->andWhere('e.status = 1')
                         ->add('orderBy', 'e.created DESC')
                         ->setParameter('uid', $uid);
                   $enquiries = $queryenquiry->getQuery()->getResult();

                //echo '<pre/>';   print_r($enquiries);exit;
                   $enqcount=count($enquiries);

				if($enqcount !=0)
                {
				for($i=0;$i<$enqcount;$i++)
				{
					$eid=$enquiries[$i]['id'];
				}

				$query = $em->createQueryBuilder()
                 ->select('e.id')
					->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
					->where('e.enquiryid = :eid')
					->andWhere('e.acceptstatus = 1')
                     ->setParameter('eid', $eid);


                        $mappedenquiry = $query->getQuery()->getArrayResult();
                       $countmappedvendor = count($mappedenquiry);
                }
                else
                {
					$countmappedvendor=0;
				}
			$query = $em->createQueryBuilder()
					->select('count(e.id)')
					->from('BBidsBBidsHomeBundle:Enquiry', 'e')
					->where('e.authorid = :author')
					->setParameter('author',$uid);

			$enquiriescount = $query->getQuery()->getSingleScalarResult();

			$count = $enquiriescount;
			$emailCount = $this->getMessageCount();
			return $this->render('BBidsBBidsHomeBundle:User:enquiriesconsumer.html.twig',array('enquiries'=>$enquiries,'countmappedvendor'=>$countmappedvendor,'emailCount'=>$emailCount));
		}
		return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
	}


	public function vendorviewAction($id)
	{
		$user = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$id));

		$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$id));
		$session = $this->container->get('session');

		$uid = $session->get('uid');
		$em = $this->getDoctrine()->getManager();


		return $this->render('BBidsBBidsHomeBundle:User:vendorview.html.twig', array('user'=>$user, 'account'=>$account));

	}


	public function consumeraccountAction()
	{
		$session = $this->container->get('session');

		if($session->has('uid')){

			$uid = $session->get('uid');

			$user = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));

			$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
				// print_r($account);exit;
		$em = $this->getDoctrine()->getManager();
		$certArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcertificationrel')->findBy(array('vendorid'=>$uid));


		$emailCount = $this->getMessageCount();

			return $this->render('BBidsBBidsHomeBundle:User:consumeraccount.html.twig', array('certArray'=>$certArray,'user'=>$user, 'account'=>$account, 'emailCount'=>$emailCount));
		}

		return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));


	}

	public function consumerupdateAction(Request $request, $consumerid)
	{
		$session = $this->container->get('session');

		if($session->has('uid')){
		$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$consumerid));



		foreach($accountArray as $a)
		{
			$address = $a->getAddress();
			$smsphone = $a->getSmsphone();
			$homephone = $a->getHomephone();
			$contactname = $a->getContactname();

		}

		$sms=explode('-',$smsphone);
		$homecode=explode('-',$homephone);

		// print_r($mobilecode);exit;
		$account = new Account();

		$logoform = $this->createFormBuilder($account)
			->add('image', 'file', array('mapped'=>false, 'required'=>false))
			->add('Upload','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();



		$logoform->handleRequest($request);

		if($request->isMethod('POST')){

			print_r($_FILES);
			//$newfilename = "hifilename";

			$file = $logoform['image']->getData();

			//$extension = $file->guessExtension();

			//$newfilename .= ".".$extension;

			echo $dir = "logo/";
//			$file->move($dir, $newfilename);
//			$file->move($dir, $file->getClientOriginalName());
			move_uploaded_file($_FILES["form"]['tmp_name']["image"],"logo/" . $_FILES["form"]["name"]['image']);
			///exit;
		}



		return $this->render('BBidsBBidsHomeBundle:User:consumerupdate.html.twig', array('contactname'=>$contactname, 'consumerid'=>$consumerid,'logoform'=>$logoform->createView(),'profileform'=>$profileform->createView(),  'accountArray'=>$accountArray));
		}

		else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function consumersaveaccountAction(Request $request)
	{
		$session = $this->container->get('session');
		$params = $this->getRequest()->request->all();

		$address = $params['form']['address'];
		$mobilecode = $params['form']['mobilecode'];
		$smsphone = $params['form']['smsphone'];
		$homecode = $params['form']['homecode'];
		$homephone = $params['form']['homephone'];
		$contactname = $params['form']['contactname'];

		$smsphonecode=$mobilecode.'-'.$smsphone;
		$homephonecode=$homecode.'-'.$homephone;


		$uid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();

		$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

		foreach($userArray as $u){
			$id = $u->getId();
			$sms = $u->getSmsphone();
		}


		// if($sms != $smsphone){
			// $session = $this->container->get('session');
			// $session->getFlashBag()->add('message','SMS verification code has been sent to you mobile number. Your account will be put under inactive state untill you verify your mobile number');
			// // sms verificATION CODE GOES HEREW
			// //DEACTIVATE USER ACCOUNT
		// }


		if($contactname == ""){
                                        $session = $this->container->get('session');

                                        $session->getFlashbag()->add('error','Contact name field should not be left blank' );

                                       return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_update', array('consumerid'=>$uid)));
                                }
         if(!is_numeric($smsphone)){
					$session = $this->container->get('session');

        				$session->getFlashBag()->add('error','Mobile number field cannot contain strings');

					   return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_update', array('consumerid'=>$uid)));
                                }
       if(strlen($smsphone) != 7){
					$session = $this->container->get('session');

                                        $session->getFlashBag()->add('error','Mobile number field should be of 7 digits');

					   return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_update', array('consumerid'=>$uid)));
                                }
        if($homephone != "" || $homecode != ""){

	                                if(!is_numeric($homephone) || !is_numeric($homecode)){
        	                                $session = $this->container->get('session');

        	                                $session->getFlashbag()->add('error','Home Phon should not contain strings');

                	                      return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_update', array('consumerid'=>$uid)));
                                }
				}
        if($homephone != "" || $homecode != ""){

	                                if(strlen($homephone) != 7){
									$session = $this->container->get('session');

                                        $session->getFlashBag()->add('error','Home Phone should be of 7 digits');


                	                      return $this->redirect($this->generateUrl('b_bids_b_bids_consumer_update', array('consumerid'=>$uid)));
                                }
				}
		$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->find($id);


		$account->setAddress($address);
		$account->setSmsphone($smsphonecode);
		$account->setHomephone($homephonecode);
		$account->setContactname($contactname);


		$em->flush();

		$updated = new  \Datetime();

		$em= $this->getDoctrine()->getmanager();

		$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

		$user->setUpdated($updated);

		$em->flush();
		return $this->redirect($this->generateUrl('b_bids_b_bids_consumeraccount'));
	}



	public function freeleadsviewhistoryAction($uid, $catid, Request $request)
        {
              $session = $this->container->get('session');
             if($session->has('uid')){
              $sessionid = $session->get('uid');


                $em = $this->getDoctrine()->getManager();
				  $query = $em->createQueryBuilder()
						->select('e.id,e.subj,e.category,e.created,e.updated,a.contactname')
						->from('BBidsBBidsHomeBundle:Enquiry', 'e')
						->innerJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 'l', 'with', 'e.id = l.enquiryid')
						->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = e.authorid')
						->where('l.vendorid= :vendorid')
						->andWhere('e.category = :category')
						->andWhere('l.flag= 2')
						//->andWhere('vc.leadpack != 0')
						->andWhere('l.acceptstatus = 1')
						->setParameter('category',$catid)
						->setParameter('vendorid',$sessionid);

				$leadsviews = $query->getQuery()->getResult();
				//print_r($leadsviews);exit;
				 $leadshistory=count($leadsviews);
				//exit;
				if($leadshistory > 0){
                       for($i = 0; $i<$leadshistory; $i++){
			       $enqid=$leadsviews[$i]['id'];
					$query = $em->createQueryBuilder()
                                ->select('count(s.id)')
                                ->from('BBidsBBidsHomeBundle:EnquirySubcategoryRel', 's')
                                ->add('where', 's.enquiryid = :enquiryid')
                                ->setParameter('enquiryid', $enqid);

				$subcount[] = $query->getQuery()->getSingleScalarResult();
				}
			} else { $subcount = 0; }
                 $query = $em->createQueryBuilder()
                        ->select('c.category, v.leadpack,v.deductedlead,v.deductedfreelead, v.status,v.flag')
                        ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')

                        ->where('v.vendorid = :vendorid')

                        ->setParameter('vendorid', $uid);
                        //->setParameter('vendorid',261);

                $leads = $query->getQuery()->getResult();

                  $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);
                                 //->setParameter('uid', 333);
                        // echo $myquery = $query->getQuery()->getSQL();
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		$catArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$catid));
		foreach($catArray as $c){
			$cat = $c->getCategory();
		}

        return $this->render('BBidsBBidsHomeBundle:User:leadsviewhistory.html.twig',array('leads'=>$leads, 'cat'=>$cat,'leadsviews'=>$leadsviews,
'useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'subcount'=>$subcount));
	}
	else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}

	}

	public function updatevendorlogoAction(Request $request,$userid)
	{


		$session = $this->container->get('session');

		$vendorid = $session->get('uid');

		$request = $this->getRequest()->request->all();
			// echo '<pre/>';print_r($_FILES['form']['tmp_name']);exit;
		$file= $_FILES['form']['name']['image'];
		$dir = "/var/www/html/web/logo";

		return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));

	}


	public function custovendorupgrationAction()
	{
		$session = $this->container->get('session');
		$session->getFlashbag()->add('success','You have registered as a vendor successfully. Please check your email for further explanation.');
		return $this->render('::basesuccess.html.twig');
	}

	public function custovendorupgrationcheckAction(Request $request,$userid)
	{
		$session = $this->container->get('session');
		 $passwordform = $this->createFormBuilder()
				->add('password','password', array('label'=>'Enter Password','mapped'=>false))
				->add('Submit', 'submit')
				->getForm();


		$passwordform->handleRequest($request);

		if($request->isMethod('POST')){

		$pass = md5($passwordform['password']->getData());
		//echo $pass.'<br/>';//exit;

		$user = new User();

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid, 'password'=>$pass));
				// print_r($userArray);exit;
				foreach($userArray as $user){
					$puid = $user->getId();
				}

				if(isset($puid)){

					$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$puid));

					foreach($accountArray as $account){
						$aid = $account->getId();
						$apid = $account->getProfileid();
						$aname = $account->getContactname();

					}
				$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->find($aid);
								$session = $this->container->get('session');

								$email = $session->get('pemail');
								$bizname = $session->get('pbizname');
								$contactname = $session->get('pname');
								$homephone2 = $session->get('phomephone');
								$smsphone2 = $session->get('psmsphone');
								$address = $session->get('paddress');
								$faxphone2 = $session->get('pfaxphone');

								$account->setUserid($puid);
								$account->setProfileid(3);
                                $account->setSmsphone($smsphone2);
                                $account->setHomephone($homephone2);
								$account->setAddress($address);
								$account->setBizname($bizname);
								$account->setContactname($contactname);
								$account->setFax($faxphone2);
								$em = $this->getDoctrine()->getManager();
								$em->flush();

				$em = $this->getDoctrine()->getManager();

				$useremail = new Usertoemail();

				$useremail->setFromuid(11);
				$useremail->setTouid($puid);
				$useremail->setFromemail('infosupport@businessbid.ae');
				$useremail->setToemail($email);
				$useremail->setEmailsubj('Welcome to the BusinessBid Network');
				$useremail->setCreated(new \Datetime());
				$useremail->setEmailmessage($this->renderView('BBidsBBidsHomeBundle:User:custtovendorupgradation.html.twig', array('name'=>$contactname)));
				$useremail->setEmailtype(1);
				$useremail->setStatus(1);
        		$useremail->setReadStatus(0);

				$em->persist($useremail);
				$em->flush();


				$url = 'https://api.sendgrid.com/';
				$user = 'businessbid';
				$pass = '!3zxih1x0';
				$subject = 'BusinessBid Network Vendor To Customer Upgrade Successful';

				$body = $this->renderView('BBidsBBidsHomeBundle:User:custtovendorupgradation.html.twig', array('name'=>$contactname));

				$message = array(
					'api_user'  => $user,
					'api_key'   => $pass,
					'to'        => $email,
					'subject'   => $subject,
					'html'      => $body,
					'text'      => $body,
					'from'      => 'support@businessbid.ae',
					 );


					$request =  $url.'api/mail.send.json';

					// Generate curl request
					$sess = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($sess, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
					// Tell curl not to return headers, but do return the response
					curl_setopt($sess, CURLOPT_HEADER,false);
					curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

					// obtain response
					$response = curl_exec($sess);
					curl_close($sess);


							$session->getFlashbag()->add('success','Your account has been successfully activated as vendor');

					return $this->redirect($this->generateUrl('b_bids_b_bids_login'));

				}

			else {


					$session->getFlashbag()->add('error','password did not match');

					 return $this->render('BBidsBBidsHomeBundle:Home:passwordcheck.html.twig',array('form'=>$passwordform->createView()));;
				}
		}
		$session->getFlashbag()->add('message','Our systems indicate that you are already a BusinessBid customer. We are delighted to have you on-board as a vendor. Please enter your password to get access to your Vendor Profile');
		return $this->render('BBidsBBidsHomeBundle:Home:passwordcheck.html.twig',array('form'=>$passwordform->createView()));
	}

	function vendorLicenseAction(Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid')) {
			$request = $this->getRequest()->request->all();
			if($session->get('pid') == 1) {
				$vendorid = $request['form']['vendor'];
			} else {
				$vendorid = $session->get('uid');
			}

			$expirydate  = $request['form']['expirydate'];
			// $licenseName = $request['form']['name'];

			$em = $this->getDoctrine()->getManager();

			// $allowed = array('png', 'jpg', 'gif','jpeg');
			$allowed = array('pdf','docx','doc');
			$upload_dir = "/var/www/html/web/img/licenses/";
			$em = $this->getDoctrine()->getManager();

			if(isset($_FILES['form']['name']['licensefile']) && $_FILES['form']['error']['licensefile'] == 0) {
				$extension = pathinfo($_FILES['form']['name']['licensefile'], PATHINFO_EXTENSION);
				if(!in_array(strtolower($extension), $allowed)){
					$session->getFlashbag()->add('error','File type not allowed');
					if($session->has('uid') && $session->get('pid')==1){
						return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
					} else {
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
					}
				}
				$six_digit_random_number = mt_rand(100000, 999999);
				$fileName = $six_digit_random_number.'.'.$extension;
				if(move_uploaded_file($_FILES['form']['tmp_name']['licensefile'], $upload_dir.$fileName)) {
					if(!empty($expirydate)) {
						$vl = new Vendorlicenserel();
						$vl->setVendorid($vendorid);
						$vl->setLicenseName('Business License');
						$vl->setExpdate(date_create($expirydate));
						$vl->setFile($fileName);
						$em->persist($vl);
					}
				}
				else {
					return new Response('{"status":"error"}');exit;
				}
			}

			$em->flush();
			$session->getFlashbag()->add('success','Business license update successful');
			if($session->has('uid') && $session->get('pid')==1){
				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
			} else {
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
			}
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function removelisenceAction($id,$vid)
	{
		$session = $this->container->get('session');
		if($session->has('uid')) {
			$em = $this->getDoctrine()->getManager();

	        $license = $em->getRepository('BBidsBBidsHomeBundle:Vendorlicenserel')->find($id);

	        $em->remove($license);
	        $em->flush();

	        $session->getFlashbag()->add('message','License delete successful');
	        if($session->get('pid')==1){
				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));
			} else {
				$vendorid = $session->get('uid');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
			}
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function removeproductsAction($id,$vid='')
	{
		$session = $this->container->get('session');
		if($session->has('uid')) {
			$em = $this->getDoctrine()->getManager();

	        $product = $em->getRepository('BBidsBBidsHomeBundle:Vendorproductsrel')->find($id);

	        $em->remove($product);
	        $em->flush();

	        $session->getFlashbag()->add('message','Product delete successful');
	        if($session->get('pid') == 1) {
				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));
			} else {
				$vendorid = $session->get('uid');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
			}
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function removecertificationAction($id,$vid = '')
	{
		$session = $this->container->get('session');
		if($session->has('uid')) {
			$em = $this->getDoctrine()->getManager();

			$certification = $em->getRepository('BBidsBBidsHomeBundle:Vendorcertificationrel')->find($id);

			$em->remove($certification);
			$em->flush();

			$session = $this->container->get('session');

			$session->getFlashbag()->add('message','Certificate delete successful');
	        if($session->get('pid') == 1) {
				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));
			} else {
				$vendorid = $session->get('uid');
				return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
			}
		} else {
			$session->getFlashBag()->add('error','It appears that your session has timed out. Please check your login credentials and try again or email support@businessbid.ae');
        	return $this->redirect($this->generateUrl('b_bids_b_bids_home_homepage'));
		}
	}

	public function conchangepasswordAction(Request $request)
	{
		$session = $this->container->get('session');
		 $userid = $session->get('uid');
		 $passwordform = $this->createFormBuilder()
				->add('password','password', array('label'=>'Enter Old Password','mapped'=>false))
				->add('npassword','password', array('label'=>'Enter New  Password','mapped'=>false))
				->add('cpassword','password', array('label'=>'Confirm  Password','mapped'=>false))
				->add('Submit', 'submit')
				->getForm();


		$passwordform->handleRequest($request);
		$emailCount = $this->getMessageCount();
		if($request->isMethod('POST'))
		{
			$pass = md5($passwordform['password']->getData());
			$npass = md5($passwordform['npassword']->getData());
			$cpass = md5($passwordform['cpassword']->getData());

			$user = new User();

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid, 'password'=>$pass));
				// print_r($userArray);exit;
				foreach($userArray as $user)
				{
					$email = $user->getEmail();
				}

				if(empty($email))
				{
					$session->getFlashbag()->add('error','Old password did not match with exisiting password');

					return $this->render('BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig',array('passwordform'=>$passwordform->createView(), 'emailCount'=>$emailCount));

				}

				if($npass!=$cpass)
				{
					$session->getFlashbag()->add('error','Confirm password doesnot match with New password');

					return $this->render('BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig',array('passwordform'=>$passwordform->createView(), 'emailCount'=>$emailCount));

				}

				else{


				 	$usera = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->find($userid);
					$email = $usera->getEmail();
					$usera->setPassword($cpass);
					$em = $this->getDoctrine()->getManager();
					$em->flush();
					$session->getFlashbag()->add('success','You have updated your account successfully');

					$body = '<html><p style="font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; "> Our records indicate that you have recently changed your password. </p><p style="font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; "> If you believe that it was not you, please report this suspicious activity to BusinessBid on &nbsp;support@businessbid.ae &nbsp; or call us on 04 42 13 777 </p><p style="font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left">Regards<br>BusinessBid Team</p></html>';
					$subject = 'BusinessBid Account Password Change Successful';
					$this->sendEmail($email,$subject,$body,$userid);
					return $this->render('BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig',array('passwordform'=>$passwordform->createView(), 'emailCount'=>$emailCount));

				}



		}

		return $this->render('BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig',array('passwordform'=>$passwordform->createView(), 'emailCount'=>$emailCount));

	}

	public function conupdateemailAction()
	{

		$session = $this->container->get('session');
		$emailCount = $this->getMessageCount();
		return $this->render('BBidsBBidsHomeBundle:User:conupdateemail.html.twig',array('emailCount'=>$emailCount));

	}


	public function condeleteAccountAction()
	{

		$session = $this->container->get('session');
		$emailCount = $this->getMessageCount();
		return $this->render('BBidsBBidsHomeBundle:User:deleteaccountcon.html.twig',array('emailCount'=>$emailCount));

	}

	public function venchangepasswordAction(Request $request)
	{
		$session = $this->container->get('session');
		$userid = $session->get('uid');

		$em = $this->getDoctrine()->getManager();
		 $query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $userid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $userid);
                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

        $emailCount = $this->getMessageCount();
		 $passwordform = $this->createFormBuilder()
				->add('password','password', array('label'=>'Enter Old Password','mapped'=>false))
				->add('npassword','password', array('label'=>'Enter New  Password','mapped'=>false))
				->add('cpassword','password', array('label'=>'Confirm  Password','mapped'=>false))
				->add('Submit', 'submit')
				->getForm();


		$passwordform->handleRequest($request);

		if($request->isMethod('POST'))
		{
			$pass = md5($passwordform['password']->getData());
			$npass = md5($passwordform['npassword']->getData());
			$cpass = md5($passwordform['cpassword']->getData());


			$user = new User();

				$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$userid, 'password'=>$pass));
				// print_r($userArray);exit;
				foreach($userArray as $user)
				{
					$email = $user->getEmail();
				}

				if(empty($email))
				{
					$session->getFlashbag()->add('error','Old password did not match with exisiting password');

					return $this->render('BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig',array('useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'passwordform'=>$passwordform->createView(),'emailCount'=>$emailCount));

				}

				if($npass!=$cpass)
				{
					$session->getFlashbag()->add('error','Confirm password doesnot match with New password');

					return $this->render('BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig',array('useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'passwordform'=>$passwordform->createView(),'emailCount'=>$emailCount));

				}

				else{


					 	$usera = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->find($userid);
						$email = $usera->getEmail();
						$usera->setPassword($cpass);
						$em = $this->getDoctrine()->getManager();
						$em->flush();
						$session->getFlashbag()->add('success','You have updated your account successfully');
						$body = '<html><p style="font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%"> Our records indicate that you have recently changed your password. </p><p style="font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%"> If you believe that it was not you, please report this suspicious activity to BusinessBid on &nbsp;support@businessbid.ae &nbsp; or call us on 04 42 13 777 </p><p style="font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left">Regards<br>BusinessBid Team</p></html>';
						$subject = 'BusinessBid Account Password Change Successful';
					$this->sendEmail($email,$subject,$body,$userid);
					return $this->render('BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig',array('useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'passwordform'=>$passwordform->createView(),'emailCount'=>$emailCount));

				}
		}
		return $this->render('BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig',array('useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'passwordform'=>$passwordform->createView(),'emailCount'=>$emailCount));

	}

	public function venupdateemailAction()
	{

		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
		$uid = $session->get('uid');
		$query = $em->createQueryBuilder()
                                ->select('count(l.id)')
                                ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
                                ->add('where','l.vendorid = :uid')
                                ->andWhere('l.status = 1')
							->setParameter('uid', $uid);
                $useridexists = $query->getQuery()->getSingleScalarResult();

                $query = $em->createQueryBuilder()
                                ->select('count(f.userid)')
                                ->from('BBidsBBidsHomeBundle:Freetrail', 'f')
                                ->add('where','f.userid = :uid')
                                ->setParameter('uid', $uid);

                 $useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();
		$emailCount = $this->getMessageCount();
		return $this->render('BBidsBBidsHomeBundle:User:venupdateemail.html.twig',array('useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));

	}


	public function vendeleteAccountAction()
	{

		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
		$uid = $session->get('uid');
		$query = $em->createQueryBuilder()
				->select('count(l.id)')
				->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'l')
				->add('where','l.vendorid = :uid')
				->andWhere('l.status = 1')
				->setParameter('uid', $uid);
		$useridexists = $query->getQuery()->getSingleScalarResult();

		$query = $em->createQueryBuilder()
				->select('count(f.userid)')
				->from('BBidsBBidsHomeBundle:Freetrail', 'f')
				->add('where','f.userid = :uid')
				->setParameter('uid', $uid);
		$useridexistsfreeleads = $query->getQuery()->getSingleScalarResult();

		$emailCount = $this->getMessageCount();
		return $this->render('BBidsBBidsHomeBundle:User:deleteaccountven.html.twig',array('useridexists'=>$useridexists,'useridexistsfreeleads'=>$useridexistsfreeleads,'emailCount'=>$emailCount));

	}

	function checkLeadAvailableAction(Request $request)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
		$returnArray = array();
		if($session->has('uid') && $session->get('pid')==3){
			$returnArray['status'] = 'Successful';

			$vendorid = $session->get('uid');

			$params = $this->getRequest()->request->all();
			$enquiryid = $params['enqid'];
			$enquiry   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);
			if(is_null($enquiry)) {
				$returnArray['status'] = 'Enquiry does not exist';
				return new response(json_encode($returnArray));
			}

			$category = $enquiry->getCategory();

			$vendorcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid, 'categoryid'=>$category, 'flag'=>1));

			// echo '<pre/>';print_r($vendorcatArray);exit;

			if($vendorcatArray){
				foreach($vendorcatArray as $vendorcat) {
					$nleadpack = $vendorcat->getLeadpack();
				}
				$returnArray['leadCount'] = 0;
				if($nleadpack >= 1)
					$returnArray['leadCount'] = $nleadpack;
				return new response(json_encode($returnArray));

			} else {
				$returnArray['status'] = 'You do not have sufficient lead credit to accept this enquiry. Please purchase more leads';
				return new response(json_encode($returnArray));
			}
		} else {
			$returnArray['status'] = 'Session Time Out';
			return new response(json_encode($returnArray));
		}
	}

	function getExtraFormFieldsAction($id) {
		$formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$id));
		if($formExtraFields)
			$returnResponse = $this->renderView('BBidsBBidsHomeBundle:User:form_extra_fields.html.twig',array('formExtraFields'=>$formExtraFields));
		else
			$returnResponse = '<strong>No Information Available</strong>';
		return new response($returnResponse);
	}



    protected function reCaptchaValidation($challengeField,$responseField)
    {
        require_once('/var/www/html/web/gocaptcha/recaptchalib.php');
        $privatekey = '6LcCrQMTAAAAAIz-hCCncUjsw9SpGtvF0CZDimeh';
        $resp = recaptcha_check_answer ($privatekey,
                        $_SERVER["REMOTE_ADDR"],
                        $challengeField,
                        $responseField);

        if ($resp->is_valid) {
            return 'Valid';
        } else {
            # set the error code so that we can display it
           $error = $resp->error;
           return $error;
        }
    }

    protected function sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$body,$vendorName)
    {

        $body = str_replace("Dear Vendor,", "Dear $vendorName,", $body);
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($vendorid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($vendorEmail);
        $useremail->setEmailsubj('JOB REQUEST '.$enquiryid);
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'JOB REQUEST '.$enquiryid;


        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $vendorEmail,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendEnquiryConfirmationEmail($userid,$email,$enquiryid)
    {
        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:enquiry_confirmation_email.html.twig',array('enquiryid'=>$enquiryid));
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('BusinessBid Network Enquiry Successfully Placed');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'BusinessBid Network Enquiry Successfully Placed';


        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }



    protected function sendWelcomeEmail($name,$userid,$email,$password)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:activationemail.html.twig', array('userid'=>$userid,'name'=>$name,'email'=>$email,'password'=>$password));

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('Welcome to the BusinessBid Network');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'Welcome to the BusinessBid Network';

        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

	private function sendEmail($email,$subject,$body,$uid)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'businessbid';
        $pass = '!3zxih1x0';
        /*$subject = 'Welcome to the BusinessBid Network';

        $body=*/
        $em = $this->getDoctrine()->getManager();
        $useremail = new Usertoemail();

		$useremail->setFromuid($uid);
		$useremail->setTouid(11);//11 means admin
		$useremail->setFromemail('support@businessbid.ae');
		$useremail->setToemail($email);
		$useremail->setEmailsubj($subject);
		$useremail->setCreated(new \Datetime());
		$useremail->setEmailmessage($body);
		$useremail->setEmailtype(1);
		$useremail->setStatus(1);
        $useremail->setReadStatus(0);

		$em->persist($useremail);
		$em->flush();

        $message = array(
	        'api_user'  => $user,
	        'api_key'   => $pass,
	        'to'        => $email,
	        'subject'   => $subject,
	        'html'      => $body,
	        'text'      => $body,
	        'from'      => 'support@businessbid.ae',
         );


        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $sess = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($sess, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        // Tell curl not to return headers, but do return the response
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($sess);
        curl_close($sess);
    }

    public function categorysuccessAction(Request $request)
	{
		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		// echo '<pre/>';print_r($params);exit;
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		/*echo '<pre/>';
		print_r($params);
		exit;*/
		$catcount = count($params['cat']);
		$vendorid = $params['vendorid'];
		$em = $this->getDoctrine()->getManager();
		if($catcount != 0)
		{
			for($h=0;$h<$catcount;$h++)
			{
				if(!empty($params['cat'][$h]))
				{
					$vp = new Vendorservices();

					$vp->setVendorid($vendorid);
					$vp->setCategory($params['cat'][$h]);
					$vp->setStatus(1);
					$em->persist($vp);
					$error_bag = 1;
				}
				else
				{
					$error_bag = 0;

				}
			}
			if($error_bag == 0)
				$session->getFlashbag()->add('error','Category cannot be blank');
			else if($error_bag == 1)
				$session->getFlashbag()->add('success','Category addedd successfully');
		}

		$em->flush();


		return $this->redirect($this->generateUrl('b_bids_b_bids_account'));

	}

	  public function portcategorysuccessAction(Request $request)
	{
		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		// echo '<pre/>';print_r($params);exit;
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['vendorid'];
		} else {
			$vendorid = $session->get('uid');
		}

		/*echo '<pre/>';
		print_r($params);
		exit;*/
		$catcount = count($params['cat']);
		$vendorid = $params['vendorid'];
		$em = $this->getDoctrine()->getManager();
		if($catcount != 0)
		{
			for($h=0;$h<$catcount;$h++)
			{
				if(!empty($params['cat'][$h]))
				{
					$vp = new Vendorservices();

					$vp->setVendorid($vendorid);
					$vp->setCategory($params['cat'][$h]);
					$vp->setStatus(1);
					$em->persist($vp);
					$error_bag = 1;
				}
				else
				{
					$error_bag = 0;

				}
			}
			if($error_bag == 0)
				$session->getFlashbag()->add('error','Category cannot be blank');
			else if($error_bag == 1)
				$session->getFlashbag()->add('success','Category updated successfully');
		}

		$em->flush();
		if($session->has('uid') && $session->get('pid')==1){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update', array('vendorid'=>$vendorid)));
		}
		//$response = $this->vendorupdateAction('test',$vendorid);
		//if ($response) return $response;

	}

	public function categorydeleteAction(Request $request,$id)
	{
		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		$em = $this->getDoctrine()->getManager();

		$vendordata = $em->getRepository('BBidsBBidsHomeBundle:Vendorservices')->find($id);

		$em->remove($vendordata);
		$em->flush();
		$session->getFlashbag()->add('success','Category deleted successfully');
		return $this->redirect($this->generateUrl('b_bids_b_bids_account'));

	}

	public function portcategorydeleteAction(Request $request,$id,$vid)
	{
		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $vid;
		} else {
			$vendorid = $session->get('uid');
		}
		$em = $this->getDoctrine()->getManager();

		$vendordata = $em->getRepository('BBidsBBidsHomeBundle:Vendorservices')->find($id);

		$em->remove($vendordata);
		$em->flush();

		$session->getFlashbag()->add('success','Category deleted successfully');

		$response = $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update',array('vendorid' => $vendorid)));
		if($session->get('pid')==1){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));
		} else {
			$vendorid = $session->get('uid');
			return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update',array('vendorid' => $vendorid)));
		}
	}

	public function categoryeditAction(Request $request)
	{
		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}

		$category = $_REQUEST['edited'];
		$id = $_REQUEST['idval'];
		$em = $this->getDoctrine()->getManager();

		$vendordata = $em->getRepository('BBidsBBidsHomeBundle:Vendorservices')->find($id);
		if(!empty($vendordata))
		{
			if(!empty($category))
			{
				$vendordata->setVendorid($vendorid);
				$vendordata->setCategory($category);
				$vendordata->setStatus(1);
				$em->persist($vendordata);
				$session->getFlashbag()->add('success','Category updated successfully');
			}
			else
			{
				$session->getFlashbag()->add('error','Category cannot be blank');
			}
		}

		$em->flush();

		return $this->redirect($this->generateUrl('b_bids_b_bids_account'));
	}

	public function portcategoryeditAction(Request $request)
	{
		$session= $this->container->get('session');
		$params = $this->getRequest()->request->all();
		if($session->has('uid') && $session->get('pid')==1){
			$vendorid = $params['vendor'];
		} else {
			$vendorid = $session->get('uid');
		}
		$category = $_REQUEST['edited'];
		$id = $_REQUEST['idval'];
		$em = $this->getDoctrine()->getManager();

		$vendordata = $em->getRepository('BBidsBBidsHomeBundle:Vendorservices')->find($id);
		if(!empty($vendordata))
		{
			if(!empty($category))
			{
				$vendordata->setVendorid($vendorid);
				$vendordata->setCategory($category);
				$vendordata->setStatus(1);
				$em->persist($vendordata);
				$session->getFlashbag()->add('success','Category updated successfully');
			}
			else
			{
				$session->getFlashbag()->add('error','Category cannot be blank');
			}
		}

		$em->flush();

		$response = $this->redirect($this->generateUrl('b_bids_b_bids_vendor_update',array('vendorid' => $vendorid)));
		 return $response;
	}

}

<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Mapping\ClassMetaData;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use BBids\BBidsHomeBundle\Entity\Enquiry;
use BBids\BBidsHomeBundle\Entity\Categories;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use BBids\BBidsHomeBundle\Entity\Account;
use BBids\BBidsHomeBundle\Entity\Activity;
use BBids\BBidsHomeBundle\Entity\Ratings;
use BBids\BBidsHomeBundle\Entity\Freetrail;
use BBids\BBidsHomeBundle\Entity\Contactus;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorcityrel;
use BBids\BBidsHomeBundle\Entity\Vendorsubcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Sessioncategoriesrel;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use BBids\BBidsHomeBundle\Entity\Categoriesextrafieldsrel;
use BBids\BBidsHomeBundle\Entity\Vendorcategorymapping;
use BBids\BBidsHomeBundle\Entity\Sessionquotes;

/*
 *
 */

class CategoriesController extends Controller
{

    function signageSignboardsAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();

            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->handleRequest($request);
            //Check form valid

            if($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                /*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                /*Extra fields for Sinage services*/
                $extraFieldsarray = array();

                $appox_width = $form['appox_width']->getData();
                if($appox_width == '') {
                    $session->getFlashBag()->add('error', 'Select approximate width');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                } else {
                    $extraFieldsarray['Width'] = $appox_width;
                }

                $appox_height = $form['appox_height']->getData();
                if($appox_height == '') {
                    $session->getFlashBag()->add('error', 'Select approximate height');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                } else {
                    $extraFieldsarray['Height'] = $appox_height;
                }

                $installation = $form['installation']->getData();
                if($installation == '') {
                    $session->getFlashBag()->add('error', 'Do you require installation');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                } else {
                    $extraFieldsarray['Require installation'] = $installation;
                }
                /*End of validation of extra fields*/

                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $session->getFlashBag()->add('error', 'Description is more than 120 characters. Please reduce the additional information and submit form again.');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $hire        = $form['hire']->getData();

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request timeline  field cannot be blank');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $session->getFlashBag()->add('error','You are looged as admin. Please log out to post a job');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);
                return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
            }/*Form validation ends*/

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

        }
        $session->getFlashBag()->add('error','Failed to fullfil your request. If issue persist then email to support@businessbid.ae');
        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
    }

    function landscapeGardensAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();

            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->handleRequest($request);
            //Check form valid

            if($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                /*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                /*Extra fields for LANDSCAPING & GARDENING*/
                $extraFieldsarray = array();

                $propertyType = $form['property_type']->getData();
                $propertyTypeCount = count($propertyType);
                if($propertyTypeCount == 0) {
                    $session->getFlashBag()->add('error', 'Select atleast one property type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                } else {
                    $propertyTypeOther = $form['property_type_other']->getData();
                    $extraFieldsarray['Type of property'] = implode(', ', $propertyType).', '.$propertyTypeOther;
                }

                $serviceNone = $form['service_none']->getData();
                if($serviceNone == ''){
                    $serviceList = $form['service_list']->getData();
                    $serviceListCount = count($serviceList);
                    $serviceListOther = $form['service_list_other']->getData();
                    if($serviceListCount == 0) {
                        $session->getFlashBag()->add('error', 'Select atleast one lawn & garden services');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Type of services'] = implode(', ', $serviceList).', '.$serviceListOther;
                    }
                } else if ($serviceNone == 'none') {
                    $extraFieldsarray['Type of services'] = $serviceNone;
                } else {
                    $session->getFlashBag()->add('error', 'Select atleast one lawn & garden services');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $landscapeNone = $form['landscape_none']->getData();
                if($landscapeNone == ''){
                    $landscape = $form['landscape_service']->getData();
                    $landscapeCount = count($landscape);
                    $landscapeOther = $form['landscape_other']->getData();
                    if($landscapeCount == 0) {
                        $session->getFlashBag()->add('error', 'Select atleast one lawn & garden services');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Custom features'] = implode(', ', $landscape).', '.$landscapeOther;
                    }
                } else if ($landscapeNone == 'none') {
                    $extraFieldsarray['Custom features'] = $landscapeNone;
                } else {
                    $session->getFlashBag()->add('error', 'Select atleast one custom features');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $irrigationNone = $form['irrigation_none']->getData();
                if($irrigationNone == ''){
                    $irrigation = $form['irrigation']->getData();
                    $irrigationCount = count($irrigation);
                    $irrigationOther = $form['irrigation_other']->getData();
                    if($irrigationCount == 0) {
                        $session->getFlashBag()->add('error', 'Select atleast one lawn & garden services');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Irrigation services'] = implode(', ', $irrigation).', '.$irrigationOther;
                    }
                } else if ($irrigationNone == 'none') {
                    $extraFieldsarray['Irrigation services'] = $irrigationNone;
                } else {
                    $session->getFlashBag()->add('error', 'Select atleast one irrigation services');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $dimensions = $form['dimensions']->getData();
                if($dimensions == '') {
                    $session->getFlashBag()->add('error', 'Select atleast one property dimensions');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $extraFieldsarray['Dimensions of property'] = $dimensions;

                /*End of validation of extra fields*/

                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $session->getFlashBag()->add('error', 'Description is more than 120 characters. Please reduce the additional information and submit form again.');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $hire        = $form['hire']->getData();

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request timeline  field cannot be blank');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $session->getFlashBag()->add('error','You are looged as admin. Please log out to post a job');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);
                return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
            }/*Form validation ends*/

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

        }
        $session->getFlashBag()->add('error','Failed to fullfil your request. If issue persist then email to support@businessbid.ae');
        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
    }

    function itSupportAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();

            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->handleRequest($request);
            //Check form valid

            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                /*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                /*Extra fields for It Support*/
                $extraFieldsarray = array();

                $people = $form['people']->getData();
                if($people == '') {
                    $session->getFlashBag()->add('error', 'Select property type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                } else {
                    $extraFieldsarray['No of people'] = $people;
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $session->getFlashBag()->add('error', 'Select type of service');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $extraFieldsarray['Frequency of service'] = $often;

                /*End of validation of extra fields*/

                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $session->getFlashBag()->add('error', 'Description is more than 120 characters. Please reduce the additional information and submit form again.');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $hire        = $form['hire']->getData();

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request timeline  field cannot be blank');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $session->getFlashBag()->add('error','You are looged as admin. Please log out to post a job');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);
                return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
            }/*Form validation ends*/

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

        }
        $session->getFlashBag()->add('error','Failed to fullfil your request. If issue persist then email to support@businessbid.ae');
        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
    }

    function interiorDesignersFitoutAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();

            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->handleRequest($request);
            //Check form valid

            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                /*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                /*Extra fields for Interior Designers Fit out*/
                $extraFieldsarray = array();

                $propertyType = $form['property_type']->getData();
                if($propertyType == '') {
                    $session->getFlashBag()->add('error', 'Select property type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                } elseif($propertyType == 'Other') {
                    $propertyTypeOther = $form['property_type_other']->getData();
                    if($propertyTypeOther == '') {
                        $session->getFlashBag()->add('error', 'Enter Other Property type');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Type of property'] = $propertyTypeOther;
                    }
                } else {
                    $extraFieldsarray['Type of property'] = $propertyType;
                }

                $dimensions = $form['dimensions']->getData();
                if($dimensions == '') {
                    $session->getFlashBag()->add('error', 'Select atleast one property dimensions');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $extraFieldsarray['Dimensions of property'] = $dimensions;

                /*End of validation of extra fields*/

                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $session->getFlashBag()->add('error', 'Description is more than 120 characters. Please reduce the additional information and submit form again.');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $hire        = $form['hire']->getData();

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request timeline  field cannot be blank');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $session->getFlashBag()->add('error','You are looged as admin. Please log out to post a job');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);
                return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));

            }/*Form validation ends*/

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

        }
        $session->getFlashBag()->add('error','Failed to fullfil your request. If issue persist then email to support@businessbid.ae');
        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
    }

    function cateringServicesAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();

            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag));

                $twigFileName =$formOptions->getTwigName();
            }
            $form->handleRequest($request);
            //Check form valid

            if($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                /*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                /*Extra fields for Catering services*/
                $extraFieldsarray = array();

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $session->getFlashBag()->add('error', 'Enter event planned type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $liveStation = $form['live_station']->getData();
                if($liveStation == '') {
                    $session->getFlashBag()->add('error', 'Select Yes Or No for Live Station');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $mealType = $form['mealtype']->getData();
                $mealTypeCount = count($mealType);
                if($mealTypeCount == 0) {
                    $mealTypeOther = $form['meal_other']->getData();
                    if($mealTypeOther == '') {
                        $session->getFlashBag()->add('error', 'Select atleast one meal type');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Type of meal'] = $mealTypeOther;
                    }
                } else {
                    $extraFieldsarray['Type of meal'] = implode(', ', $mealType);
                }

                $beverages = $form['beverages']->getData();
                if($beverages == '') {
                    $session->getFlashBag()->add('error', 'Select Yes Or No for Beverages');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $dessert = $form['dessert']->getData();
                if($dessert == '') {
                    $session->getFlashBag()->add('error', 'Select Yes Or No for Dessert');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $supportStaff = $form['support_staff']->getData();
                $supportStaffcount = count($supportStaff);
                if($supportStaffcount == 0) {
                    $supportStaffOther = $form['support_staff_other']->getData();
                    if($supportStaffOther == '') {
                        $session->getFlashBag()->add('error', 'Select atleast one Software type');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Type of support staff'] = $supportStaffOther;
                    }
                } else {
                    $extraFieldsarray['Type of support staff'] = implode(', ', $supportStaff);
                }

                $guests = $form['guests']->getData();
                if($guests == '') {
                    $session->getFlashBag()->add('error', 'Select guest you are expecting');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $cuisine = $form['cuisine']->getData();
                $cuisinecount = count($cuisine);
                if($cuisinecount == 0) {
                    $cuisineOther = $form['cuisine_other']->getData();
                    if($cuisineOther == '') {
                        $session->getFlashBag()->add('error', 'Select type of cuisine');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    } else {
                        $extraFieldsarray['Type of cuisine'] = $supportStaffOther;
                    }
                } else {
                    $extraFieldsarray['Type of cuisine'] = implode(', ', $cuisine);
                }


                $extraFieldsarray['Event planned'] = $eventPlaned;
                $extraFieldsarray['Live station']  = $liveStation;
                $extraFieldsarray['Beverages']     = $beverages;
                $extraFieldsarray['No of guests']  = $guests;
                /*End of validation of extra fields*/

                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $session->getFlashBag()->add('error', 'Description is more than 120 characters. Please reduce the additional information and submit form again.');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $hire        = $form['hire']->getData();

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request timeline  field cannot be blank');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $session->getFlashBag()->add('error','You are looged as admin. Please log out to post a job');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);
                return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
            }/*Form validation ends*/

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

        }
        $session->getFlashBag()->add('error','Failed to fullfil your request. If issue persist then email to support@businessbid.ae');
        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
    }

    function accountingAuditingAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();

            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->handleRequest($request);
            //Check form valid

            if($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                /*Captcha Validation*/
                $captchaFlag  = $this->reCaptchaValidation($_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

                if ($captchaFlag != 'Valid') {
                    $session->getFlashbag()->add('error',$captchaFlag );
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0){
                    $session = $this->container->get('session');
                    $session->getFlashBag()->add('error', 'Please select atleast one subcategory');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                /*Extra fields for accounting*/
                $extraFieldsarray = array();

                $software = $form['software']->getData();
                $softwarecount = count($software);
                if($softwarecount == 0) {
                    $session->getFlashBag()->add('error', 'Select atleast one Software type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $accounting = $form['accounting']->getData();
                if($accounting == '') {
                    $session->getFlashBag()->add('error', 'Select atleast one Accounting type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $session->getFlashBag()->add('error', 'Select atleast one Frequency type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                $turnover = $form['turnover']->getData();
                if($turnover == '') {
                    $session->getFlashBag()->add('error', 'Select atleast Annual Revenue type');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $otherSoftware = $form['software_other']->getData();
                $extraFieldsarray['Type of free zones'] = implode(', ', $software).' ,'.$otherSoftware;
                $accountingOther = $form['accounting_other']->getData();
                if($accountingOther == 'Other') {
                    $extraFieldsarray['Type of company'] = $accountingOther;
                } else {
                    $extraFieldsarray['Type of company']    = $accounting;
                }

                $oftenOther = $form['often_other']->getData();
                if($oftenOther == 'Other') {
                    $extraFieldsarray['Need of service']    = $oftenOther;
                } else {
                    $extraFieldsarray['Need of service']    = $often;
                }

                $extraFieldsarray['Annual revenue']     = $turnover;
                /*End of validation of extra fields*/

                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $session->getFlashBag()->add('error', 'Description is more than 120 characters. Please reduce the additional information and submit form again.');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }
                $hire        = $form['hire']->getData();

                if($hire == "") {
                    $session->getFlashBag()->add('error', 'Request timeline  field cannot be blank');
                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $session->getFlashBag()->add('error','You are looged as admin. Please log out to post a job');
                        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $session->getFlashBag()->add('error', 'Email  field cannot be blank');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $session->getFlashBag()->add('error', 'Contact name should contain only alphabets and spaces');
                        return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $session->getFlashBag()->add('error', 'Mobile phone field should contain only numbers.');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                                    return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $session->getFlashBag()->add('error', 'Mobile number is already in use by other user. Please use same email id or login to use this number');
                            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $response = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);
                return $this->render('BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig',array('jobNo' => $enquiryid));
            }/*Form validation ends*/

            return $this->render("BBidsBBidsHomeBundle:Categoriesform:$twigFileName.html.twig", array('form'=>$form->createView(), 'category'=>$categoryname));

        }
        $session->getFlashBag()->add('error','Failed to fullfil your request. If issue persist then email to support@businessbid.ae');
        return $this->redirect($this->generateUrl('b_bids_b_bids_login'));
    }

    protected function createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype)
    {
        $em = $this->getDoctrine()->getManager();

        $user       = new User();
        $created    = new \DateTime();
        // $userhash   = $this->generateHash(16);

        $user->setPassword(md5($mobilecode));

        $user->setEmail($email);
        $user->setCreated($created);
        $user->setUpdated($created);
        // $user->setUserhash($userhash);
        if($locationtype == 'UAE')
            $user->setStatus(1);
        else
            $user->setStatus(4);
        $user->setMobilecode($mobilecode);

        $em->persist($user);
        $em->flush();

        $userid = $user->getId();

        $account = new Account();

        $account->setProfileid(2);
        $account->setUserid($userid);
        $account->setSmsphone($smsphone);
        $account->setHomephone($homephone);
        $account->setContactname($contactname);

        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        return $userid;
    }

    protected function createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();

        $created = new \DateTime();
        $enquiry = new Enquiry();
        $enquiry->setSubj('No Subject');
        $enquiry->setDescription($description);
        $enquiry->setCity($city);
        $enquiry->setCategory($categoryid);
        $enquiry->setAuthorid($userid);
        $enquiry->setCreated($created);
        $enquiry->setUpdated($created);

        $enquiry->setStatus(1);

        $enquiry->setLocation($location);
        $enquiry->setHirePriority($hire);

        $em = $this->getDoctrine()->getManager();
        $em->persist($enquiry);
        $em->flush();

        $enquiryid = $enquiry->getId();

        return $enquiryid;
    }

    protected function createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount)
    {
        $em = $this->getDoctrine()->getManager();
        $eSubCategoryString = '';
        for($i=0; $i<$subcount; $i++) {
            $subCatID = $subcategoryids[$i];
            $enquirysubrel = new EnquirySubcategoryRel();
            $enquirysubrel->setEnquiryid($enquiryid);
            $enquirysubrel->setSubcategoryid($subcategoryids[$i]);

            $eSubCategoryString .= $subcategory[$subCatID].", ";

            $em = $this->getDoctrine()->getManager();
            $em->persist($enquirysubrel);
            $em->flush();
        }
        return $eSubCategoryString;
    }

    protected function mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $hireArray = array('No Priority', 'Urgent', '1-3 days', '4-7 days', '7-14 days', 'Just Planning','Flexible Dates');

        $formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));
        $extraInfoSMS = $emailBody = '';
        $extraInfoSMS .= "Job ref no : ".$enquiryid.'\r\n';
        $extraInfoSMS .= $eSubCategoryString.'\r\n';
        if($formExtraFields) {
            foreach ($formExtraFields as $field) {
                $extraInfoSMS .= $field->getKeyName().' : '.$field->getKeyValue().'\r\n';
            }
            $emailBody = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig',array('formExtraFields'=>$formExtraFields,'jobNo'=>$enquiryid, 'city'=>$city,'hire'=>$hire, 'description'=>$description, 'eSubCategoryString'=>$eSubCategoryString));
        }
        $extraInfoSMS .= "City : ".$city.'\r\n';
        $extraInfoSMS .= "Priority : ".$hireArray[$hire].'\r\n';
        if($description)
            $extraInfoSMS .= "Info : ".$description.'\r\n';

        if($session->has('maptovendorid')) {
            $vendorid    = $session->get('maptovendorid');
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid));

            foreach($vendorArray as $vendor){
                $status = $vendor->getStatus();
                if($status == 1) {
                    $vendorenquiryrel = new Vendorenquiryrel();

                    $vendorenquiryrel->setVendorid($vendorid);
                    $vendorenquiryrel->setEnquiryid($enquiryid);

                    $em->persist($vendorenquiryrel);
                    $em->flush();

                    // Send sms to all vendors
                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                    if($accountArray) {
                        // Send sms to mobile number provided.
                        $vendorName    = $accountArray->getContactname();
                        $smsphone      = $accountArray->getSmsphone();
                        $smsphoneArray = explode("-",$smsphone);
                        $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                        $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                        $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');/*SMS function is blocked*/
                        /*End of sms to vendors*/
                        // Send email notification to all vendors
                        $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                        if($vendorUser) {
                            $vendorEmail = $vendorUser->getEmail();
                            $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                        }
                    }
                }
            }
        } else {
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('categoryid'=>$categoryid));
            $enqcount = 0;
            foreach($vendorArray as $vendor) {
                $enqcount++;
                $vendorid       = $vendor->getVendorid();
                $status         = $vendor->getStatus();
                $vendorleadpack = $vendor->getLeadpack();

                $manualVendCatMapping = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findBy(array('categoryID'=>$categoryid,'vendorID'=>$vendorid,'status'=>1));
                if(empty($manualVendCatMapping)) {
                    if($status == 1 && $vendorleadpack !=0 ) {
                       $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                        if(empty($vendenqArray)) {
                             //if($enqcount == 1){
                            $vendorenquiryrel = new Vendorenquiryrel();

                            $vendorenquiryrel->setVendorid($vendorid);
                            $vendorenquiryrel->setEnquiryid($enquiryid);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($vendorenquiryrel);
                            $em->flush();

                            // Send sms to all vendors
                            $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                            if($accountArray) {
                                // Send sms to mobile number provided.
                                $vendorName    = $accountArray->getContactname();
                                $smsphone      = $accountArray->getSmsphone();
                                $smsphoneArray = explode("-",$smsphone);
                                $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                                $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                                $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                                /*End of sms to vendors*/

                                // Send email notification to all vendors
                                $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                                if($vendorUser) {
                                   $vendorEmail = $vendorUser->getEmail();
                                   $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                                }
                            }
                        }
                    }
                } else {
                    // This means that vendor is manually mapped by admin
                    $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                    if(empty($vendenqArray)) {

                        // Send sms to all manual mapped vendors
                        $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                        if($accountArray) {
                            // Send sms to mobile number provided.
                            $vendorName    = $accountArray->getContactname();
                            $smsphone      = $accountArray->getSmsphone();
                            $smsphoneArray = explode("-",$smsphone);
                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                            $response = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                            /*End of sms to vendors*/

                            // Send email notification to all manual mapped vendors
                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                            if($vendorUser) {
                               $vendorEmail = $vendorUser->getEmail();
                               $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                            }
                        }
                    }
                }

            }
        }
    }

    protected function setNewActivity($city,$categoryname,$userid)
    {
        $activity = new Activity();
        $created  = new \DateTime();

        $activity->setMessage('New Enquiry');
        $activity->setCity($city);
        $activity->setCategory($categoryname);
        $activity->setAuthorid($userid);
        $activity->setCreated($created);
        $activity->setType(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($activity);
        $em->flush();
    }

    protected function sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$body,$vendorName)
    {

        $body = str_replace("Dear Vendor,", "Dear $vendorName", $body);
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($vendorid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($vendorEmail);
        $useremail->setEmailsubj('JOB REQUEST '.$enquiryid);
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'JOB REQUEST '.$enquiryid;


        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $vendorEmail,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendEnquiryConfirmationEmail($userid,$email,$enquiryid)
    {
        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:enquiry_confirmation_email.html.twig',array('enquiryid'=>$enquiryid));
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('BusinessBid Network Enquiry Successfully Placed');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'BusinessBid Network Enquiry Successfully Placed';


        $json_string = array(
          'to' => array(
            $email,'rachelle@businessbid.ae','greeth@businessbid.ae','management@businessbid.ae'
          ),
          'category' => 'test_category'
        );
        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendWelcomeEmail($name,$userid,$email,$password)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:inline_activationemail.html.twig', array('userid'=>$userid,'name'=>$name,'email'=>$email,'password'=>$password));

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('Welcome to the BusinessBid Network');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'Welcome to the BusinessBid Network';

        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function generateHash($length)
    {
        $characters   = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for($i=0; $i< $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    protected function sendSMSToPhone($phoneNumber, $text, $type='')
    {
        return '';
        //$phoneNumber = substr($phoneNumber, 1);
        $subphone = substr($phoneNumber, 1);
        $smsphone = "971".$subphone;

         if(strpos($text, 'for the customer information'))
            $senderid = 3579;
        else
            $senderid = 'Businessbids';

        $smsJson = '{
                    "authentication": {
                        "username": "agefilms",
                        "password": "12345Uae"
                    },
                    "messages": [
                        {
                            "sender": "'.$senderid.'",
                            "text": "'.$text.'",';
        if($type) {
            $smsJson .= '"type":"longSMS",';
        }
        $smsJson .= '
                            "recipients": [
                                {
                                    "gsm": "'.$smsphone.'"
                                }
                            ]
                        }
                    ]
                }';


        $encodeSMS = base64_encode(json_encode($smsJson));
        $ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: api.infobip.com',
            'Accept: */*',
            'Content-Length: ' . strlen($smsJson))
        );

        $result = curl_exec($ch);
        // $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // echo "status : ".$http_status;
         // echo 'response: '.$result;
        $response        = new Response($result);
        $encodedResponse = base64_encode(json_encode($response));

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $sentSMS = $connection->prepare("INSERT INTO `bizbids`.`bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, $phoneNumber, '$encodeSMS', '$encodedResponse', NOW())");
        $sentSMS->execute();

        return $response;
    }

    protected function reCaptchaValidation($challengeField,$responseField)
    {
        require_once('/var/www/html/web/gocaptcha/recaptchalib.php');
        $privatekey = '6LcCrQMTAAAAAIz-hCCncUjsw9SpGtvF0CZDimeh';
        $resp = recaptcha_check_answer ($privatekey,
                        $_SERVER["REMOTE_ADDR"],
                        $challengeField,
                        $responseField);

        if ($resp->is_valid) {
            return 'Valid';
        } else {
            # set the error code so that we can display it
           $error = $resp->error;
           return $error;
        }
    }
}/*End of class*/
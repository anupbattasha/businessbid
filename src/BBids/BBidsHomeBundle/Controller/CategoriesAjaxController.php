<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Mapping\ClassMetaData;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use BBids\BBidsHomeBundle\Entity\Enquiry;
use BBids\BBidsHomeBundle\Entity\Categories;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use BBids\BBidsHomeBundle\Entity\Account;
use BBids\BBidsHomeBundle\Entity\Activity;
use BBids\BBidsHomeBundle\Entity\Ratings;
use BBids\BBidsHomeBundle\Entity\Freetrail;
use BBids\BBidsHomeBundle\Entity\Contactus;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorcityrel;
use BBids\BBidsHomeBundle\Entity\Vendorsubcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Sessioncategoriesrel;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use BBids\BBidsHomeBundle\Entity\Categoriesextrafieldsrel;
use BBids\BBidsHomeBundle\Entity\Vendorcategorymapping;
use BBids\BBidsHomeBundle\Entity\Sessionquotes;

/*
 *
 */

class CategoriesAjaxController extends Controller
{

    function movingStorageAction(Request $request)
    {

    }

    function educationLearningAction(Request $request)
    {

    }

    function conciergeErrandsAction(Request $request)
    {

    }

    function computerPhoneRepairAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcateries']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Computer & Phone Repairs*/
                $extraFieldsarray = array();
                $response['step'] = 2;
                $key = array_search('Other', $subcategoryids);
                $subcategoryOther = $form['subcategory_other']->getData();
                if($key && $subcategoryOther == '') {
                    $response['error'] = "Enter other floral type";
                    return new JsonResponse( $response );
                }

                $services = $form['services']->getData();
                $servicesCount = count($services);
                if($servicesCount == 0) {
                    $response['error'] = "Select atleast one services type";
                    return new JsonResponse( $response );
                } else {
                    $servicesOther = $form['services_other']->getData();
                    $arrString = implode(", ",$services);
                    $arrString = (strpos($arrString,"Other"))? ', '.$servicesOther : $arrString;
                    $extraFieldsarray['Services'] = $arrString;
                }

                $duration = $form['duration']->getData();
                if($duration == '') {
                    $response['error'] = "Select duration";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Duration'] = $duration;

                $onsite = $form['onsite']->getData();
                if($onsite == '') {
                    $response['error'] = "Select onsite";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Onsite'] = $onsite;
                }

                $delivery = $form['delivery']->getData();
                if($delivery == '') {
                    $response['error'] = "Select delivery";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Delivery'] = $delivery;
                }
                /*Extra fields for Computer & Phone Repairs */

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                $eSubCategoryString = (strpos($eSubCategoryString,"Other")) ? (str_replace('Other', $subcategoryOther, $eSubCategoryString)) : $eSubCategoryString;

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));

    }

    function beautyStylingAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = array();
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one service";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Computer & Phone Repairs*/
                $extraFieldsarray = array();
                $response['step'] = 2;
                $key = array_search('Other', $subcategoryids);
                $subcategoryOther = $form['subcategory_other']->getData();
                if($key && $subcategoryOther == '') {
                    $response['error'] = "Enter other service type";
                    return new JsonResponse( $response );
                }

                $event = $form['event']->getData();
                if($event == '') {
                    $response['error'] = "Select no of event";
                    return new JsonResponse( $response );
                } else {
                    $eventOther = $form['event_other']->getData();
                    $extraFieldsarray['Event'] = (!empty($eventOther))? $eventOther : $event;
                }

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Date'] = $eventPlaned;


                $onsite = $form['onsite']->getData();
                if($onsite == '') {
                    $response['error'] = "Select onsite option";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Onsite'] = $onsite;
                }

                $service = $form['service']->getData();
                if($service == '') {
                    $response['error'] = "Select Service for";
                    return new JsonResponse( $response );
                } else {
                    $serviceOther = $form['service_other']->getData();
                    $extraFieldsarray['Service for'] = (!empty($serviceOther))? $serviceOther : $service;
                }

                $hours = $form['hours']->getData();
                if($hours == '') {
                    $response['error'] = "Select no of hours";
                    return new JsonResponse( $response );
                } else {
                    $hoursOther = $form['hours_other']->getData();
                    $extraFieldsarray['Hours'] = (!empty($hoursOther))? $hoursOther : $hours;
                }
                /*Extra fields for Computer & Phone Repairs */

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                $eSubCategoryString = (strpos($eSubCategoryString,"Other")) ? (str_replace('Other', $subcategoryOther, $eSubCategoryString)) : $eSubCategoryString;

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));

    }

    function babysitterNannyAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = array();
                $subcategoryids[0] = $form['subcateries']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one service";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Computer & Phone Repairs*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }

                $hours = $form['hours']->getData();
                if($hours == '') {
                    $response['error'] = "Select no of hours";
                    return new JsonResponse( $response );
                } else {
                    $hoursOther = $form['hours_other']->getData();
                    $extraFieldsarray['Hours'] = (!empty($hoursOther))? $hoursOther : $hours;
                }

                $children = $form['children']->getData();
                if($children == '') {
                    $response['error'] = "Select no of children";
                    return new JsonResponse( $response );
                } else {
                    $childrenOther = $form['children_other']->getData();
                    $extraFieldsarray['Children'] = (!empty($childrenOther))? $childrenOther : $children;
                }

                $age = $form['age']->getData();
                $ageCount = count($age);
                if($ageCount == 0) {
                    $response['error'] = "Select atleast one age range";
                    return new JsonResponse( $response );
                } else {
                    $arrString = implode(", ",$age);
                    $extraFieldsarray['Age'] = $arrString;
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select frequency";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Frequency'] = $often;
                }
                /*Extra fields for Computer & Phone Repairs */

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));

    }

    function attestationTranslationAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcateries']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Computer & Phone Repairs*/
                $extraFieldsarray = array();
                $response['step'] = 2;
                $key = array_search('Other', $subcategoryids);
                $subcategoryOther = $form['subcategory_other']->getData();
                if($key && $subcategoryOther == '') {
                    $response['error'] = "Enter other floral type";
                    return new JsonResponse( $response );
                }

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Service Date'] = $eventPlaned;

                $documents = $form['documents']->getData();
                $documentsCount = count($documents);
                if($documentsCount == 0) {
                    $response['error'] = "Select atleast one Documents type";
                    return new JsonResponse( $response );
                } else {
                    $documentsOther = $form['documents_other']->getData();
                    $arrString = implode(", ",$documents);
                    $arrString = (strpos($arrString,"Other"))? (str_replace('Other', $documentsOther, $arrString)) : $arrString;
                    $extraFieldsarray['Documents'] = $arrString;
                }

                $international = $form['international']->getData();
                if($international == '') {
                    $response['error'] = "Select duration";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['International'] = $international;

                $collection = $form['collection']->getData();
                if($collection == '') {
                    $response['error'] = "Select collection";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Collection'] = $collection;
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select frequency";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Frequency'] = $often;
                }
                /*Extra fields for Computer & Phone Repairs */

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                $eSubCategoryString = (strpos($eSubCategoryString,"Other")) ? (str_replace('Other', $subcategoryOther, $eSubCategoryString)) : $eSubCategoryString;

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function airConditioningCoolingAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Computer & Phone Repairs*/
                $extraFieldsarray = array();
                $response['step'] = 2;
                $key = array_search('Other', $subcategoryids);
                $subcategoryOther = $form['subcategory_other']->getData();
                if($key && $subcategoryOther == '') {
                    $response['error'] = "Enter other services";
                    return new JsonResponse( $response );
                }

                $product = $form['product']->getData();
                $productCount = count($product);
                if($productCount == 0) {
                    $response['error'] = "Select atleast one product type";
                    return new JsonResponse( $response );
                } else {
                    $productOther = $form['product_other']->getData();
                    $arrString = implode(", ",$product);
                    $arrString = (strpos($arrString,"Other"))? (str_replace('Other', $productOther, $arrString)) : $arrString;
                    $extraFieldsarray['Product'] = $arrString;
                }

                $service = $form['service']->getData();
                $serviceCount = count($service);
                if($serviceCount == 0) {
                    $response['error'] = "Select atleast one service type";
                    return new JsonResponse( $response );
                } else {
                    $serviceOther = $form['service_other']->getData();
                    $arrString = implode(", ",$service);
                    $arrString = (strpos($arrString,"Other"))? (str_replace('Other', $serviceOther, $arrString)) : $arrString;
                    $extraFieldsarray['Service'] = $arrString;
                }

                $age = $form['age']->getData();
                if($age == '') {
                    $response['error'] = "Select atleast one age type";
                    return new JsonResponse( $response );
                } else {
                    $ageOther = $form['age_other']->getData();
                    $extraFieldsarray['Unit Age'] = (!empty($ageOther))? $ageOther : $age;
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select frequency";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Frequency'] = $often;
                }
                /*Extra fields for Computer & Phone Repairs */

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                $eSubCategoryString = (strpos($eSubCategoryString,"Other")) ? (str_replace('Other', $subcategoryOther, $eSubCategoryString)) : $eSubCategoryString;

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));

    }

    function photographyVideoAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Photography & video*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }

                $eventService = $form['event_service']->getData();
                $eventServiceCount = count($eventService);
                if($eventServiceCount == 0) {
                    $response['error'] = "Select atleast one event service";
                    return new JsonResponse( $response );
                } else {
                    $eventServiceOther = $form['event_service_other']->getData();
                    $extraFieldsarray['Event'] = implode(', ', $eventService);
                    if($eventServiceOther !='') $extraFieldsarray['Event'] .= ', '.$eventServiceOther;
                }

                $serviceType = $form['service_type']->getData();
                $serviceTypeCount = count($serviceType);
                if($serviceTypeCount == 0) {
                    $response['error'] = "Select atleast one service type";
                    return new JsonResponse( $response );
                } else {
                    $serviceTypeOther = $form['service_type_other']->getData();
                    $extraFieldsarray['Service Location'] = implode(', ', $serviceType);
                    if($serviceTypeOther !='') $extraFieldsarray['Service Location'] .= ', '.$serviceTypeOther;
                }

                $usage = $form['usage']->getData();
                $usageCount = count($usage);
                if($usageCount == 0) {
                    $response['error'] = "Select atleast one usage type";
                    return new JsonResponse( $response );
                } else {
                    $usageOther = $form['usage_other']->getData();
                    $extraFieldsarray['Purpose'] = implode(', ', $usage);
                    if($usageOther !='') $extraFieldsarray['Purpose'] .= ', '.$usageOther;
                }

                $format = $form['format']->getData();
                $formatCount = count($format);
                if($formatCount == 0) {
                    $response['error'] = "Select atleast one format";
                    return new JsonResponse( $response );
                } else {
                    $formatOther = $form['format_other']->getData();
                    $extraFieldsarray['Format'] = implode(', ', $format);
                    if($formatOther !='') $extraFieldsarray['Format'] .= ', '.$formatOther;
                }
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function commercialCleaningAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Photography & video*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $service = $form['service']->getData();
                if($service == '') {
                    $response['error'] = "Select atleast one service";
                    return new JsonResponse( $response );
                } else {
                    $serviceOther = $form['service_other']->getData();
                    $extraFieldsarray['Service'] = $service;
                    if($serviceOther !='') $extraFieldsarray['Service'] .= ', '.$serviceOther;
                }

                /*$offenCleaned = $form['offen_cleaned']->getData();
                if($offenCleaned == '') {
                    $response['error'] = "Select atleast one often cleaning";
                    return new JsonResponse( $response );
                } else {
                    $offenCleanedOther = $form['offen_cleaned_other']->getData();
                    $extraFieldsarray['Often Cleaned'] = $offenCleaned;
                    if($offenCleanedOther !='') $extraFieldsarray['Often Cleaned'] .= ', '.$offenCleanedOther;
                }*/

                $supplies = $form['supplies']->getData();
                if($supplies == '') {
                    $response['error'] = "Select atleast one usage type";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Supplies'] = $supplies;
                }

                $occupied = $form['occupied']->getData();
                if($supplies == '') {
                    $response['error'] = "Select occupancy type";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Occupied'] = $occupied;
                }

                $spaces = $form['spaces']->getData();
                $spacesCount = count($spaces);
                if($spacesCount == 0) {
                    $response['error'] = "Select atleast one space type";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Space type'] = implode(', ', $spaces);
                }

                $specific = $form['specific']->getData();
                $specificCount = count($specific);
                if($specific == 0) {
                    $response['error'] = "Select atleast one specific cleaning";
                    return new JsonResponse( $response );
                } else {
                    $specificOther = $form['specific_other']->getData();
                    $extraFieldsarray['specific'] = implode(', ', $specific);
                    if($specificOther !='') $extraFieldsarray['Often Cleaned'] .= ', '.$specificOther;
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select atleast one often need";
                    return new JsonResponse( $response );
                } else {
                    $offenOther = $form['often_other']->getData();
                    $extraFieldsarray['Often'] = $often;
                    if($offenOther !='') $extraFieldsarray['Often'] .= ', '.$offenOther;
                }

                $dimension = $form['dimension']->getData();
                if($dimension == '') {
                    $response['error'] = "Select atleast one dimension range";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Dimension'] = $dimension;
                }
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function itSupportAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Photography & video*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $people = $form['people']->getData();
                if($people == '') {
                    $response['error'] = "Select atleast one kind of people";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['People'] = $people;
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select atleast one often need";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Often'] = $often;
                }

                $businessType      = $form['business_type']->getData();
                $businessTypeOther = $form['business_other']->getData();
                if($businessType == '') {
                    $response['error'] = "Select atleast one property type";
                    return new JsonResponse( $response );
                } else if($businessType == 'Other'){
                    if($businessTypeOther == '') {
                        $response['error'] = "Select atleast one property type";
                        return new JsonResponse( $response );
                    }else {
                        $extraFieldsarray['Type of Business'] = $businessTypeOther;
                    }
                }
                else
                    $extraFieldsarray['Type of Business'] = $businessType;
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function offsetPrintingAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Photography & video*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select atleast one offen need";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Frequency'] = $often;

                $printSize = $form['print_size']->getData();
                if($printSize == '') {
                    $response['error'] = "Select atleast one print size";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Print volume'] = $printSize;

                $assistance = $form['assistance']->getData();
                if($assistance == '') {
                    $response['error'] = "Select atleast one print size";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Design assistance'] = $assistance;
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function accountingAuditingAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Photography & video*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $software = $form['software']->getData();
                $softwarecount = count($software);
                if($softwarecount == 0) {
                    $response['error'] = "Select atleast one Software type";
                    return new JsonResponse( $response );
                }

                $accounting = $form['accounting']->getData();
                if($accounting == '') {
                    $response['error'] = "Select atleast one Accounting type";
                    return new JsonResponse( $response );
                }

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select atleast one Frequency type";
                    return new JsonResponse( $response );
                }

                $turnover = $form['turnover']->getData();
                if($turnover == '') {
                    $response['error'] = "Select atleast Annual Revenue type";
                    return new JsonResponse( $response );
                }

                $otherSoftware = $form['software_other']->getData();
                $extraFieldsarray['Software Used'] = implode(', ', $software).' ,'.$otherSoftware;
                $accountingOther = $form['accounting_other']->getData();
                if($accountingOther == 'Other') {
                    $extraFieldsarray['Type of company'] = $accountingOther;
                } else {
                    $extraFieldsarray['Type of company'] = $accounting;
                }

                $oftenOther = $form['often_other']->getData();
                if($oftenOther == 'Other') {
                    $extraFieldsarray['Need of service'] = $oftenOther;
                } else {
                    $extraFieldsarray['Need of service'] = $often;
                }

                $extraFieldsarray['Annual revenue'] = $turnover;
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                //echo '<pre/>';print_r($smsphoneExist);exit;
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function cateringServicesAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Photography & video*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }

                $liveStation = $form['live_station']->getData();
                if($liveStation == '') {
                    $response['error'] = "Select Yes Or No for Live Station";
                    return new JsonResponse( $response );
                }

                $mealType = $form['mealtype']->getData();
                $mealTypeCount = count($mealType);
                if($mealTypeCount == 0) {
                    $mealTypeOther = $form['meal_other']->getData();
                    if($mealTypeOther == '') {
                        $response['error'] = "Select atleast one meal type";
                        return new JsonResponse( $response );
                    } else {
                        $extraFieldsarray['Type of meal'] = $mealTypeOther;
                    }
                } else {
                    $extraFieldsarray['Type of meal'] = implode(', ', $mealType);
                }

                $beverages = $form['beverages']->getData();
                if($beverages == '') {
                    $response['error'] = "Select Yes Or No for Beverages";
                    return new JsonResponse( $response );
                }

                $dessert = $form['dessert']->getData();
                if($dessert == '') {
                    $response['error'] = "Select Yes Or No for Dessert";
                    return new JsonResponse( $response );
                }

                $supportStaff = $form['support_staff']->getData();
                $supportStaffcount = count($supportStaff);
                if($supportStaffcount == 0) {
                    $supportStaffOther = $form['support_staff_other']->getData();
                    if($supportStaffOther == '') {
                        $response['error'] = "Select atleast one Software type";
                        return new JsonResponse( $response );
                    } else {
                        $extraFieldsarray['Type of support staff'] = $supportStaffOther;
                    }
                } else {
                    $extraFieldsarray['Type of support staff'] = implode(', ', $supportStaff);
                }

                $guests = $form['guests']->getData();
                if($guests == '') {
                    $response['error'] = "Select guest you are expecting";
                    return new JsonResponse( $response );
                }

                $cuisine = $form['cuisine']->getData();
                $cuisinecount = count($cuisine);
                if($cuisinecount == 0) {
                    $cuisineOther = $form['cuisine_other']->getData();
                    if($cuisineOther == '') {
                        $response['error'] = "Select type of cuisine";
                        return new JsonResponse( $response );
                    } else {
                        $extraFieldsarray['Type of cuisine'] = $supportStaffOther;
                    }
                } else {
                    $extraFieldsarray['Type of cuisine'] = implode(', ', $cuisine);
                }


                $extraFieldsarray['Event planned'] = $eventPlaned;
                $extraFieldsarray['Live station']  = $liveStation;
                $extraFieldsarray['Beverages']     = $beverages;
                $extraFieldsarray['No of guests']  = $guests;
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function interiorDesignersFitoutAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Interior Design & Fit Out*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $propertyType      = $form['property_type']->getData();
                $propertyTypeOther = $form['property_type_other']->getData();
                if($propertyType == '') {
                    $response['error'] = "Select atleast one property type";
                    return new JsonResponse( $response );
                } else if($propertyType == 'Other'){
                    if($propertyTypeOther == '') {
                        $response['error'] = "Select atleast one property type";
                        return new JsonResponse( $response );
                    }else {
                        $extraFieldsarray['Type of property'] = $propertyTypeOther;
                    }
                }
                else
                    $extraFieldsarray['Type of property'] = $propertyType;

                $dimensions = $form['dimensions']->getData();
                if($dimensions == '') {
                    $response['error'] = "Select atleast one property dimensions";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Dimensions of property'] = $dimensions;
                /*Extra fields for Interior Design & Fit Out*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function pestControlAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for landscape & gardens*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $propertyType = $form['property_type']->getData();
                // $propertyTypeCount = count($propertyType);
                if($propertyType == '') {
                    $response['error'] = "Select atleast one property type";
                    return new JsonResponse( $response );
                } else {
                    $propertyTypeOther = $form['property_type_other']->getData();
                    $extraFieldsarray['Type of property'] = ($propertyType == 'Other')?$propertyTypeOther: $propertyType;
                }

                $dimensions = $form['dimensions']->getData();
                if($dimensions == '') {
                    $response['error'] = "Select atleast one property dimensions";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Dimensions of property'] = $dimensions;
                /*Extra fields for landscape & gardens*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function landscapeGardensAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for landscape & gardens*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $propertyType = $form['property_type']->getData();
                // $propertyTypeCount = count($propertyType);
                if($propertyType == '') {
                    $response['error'] = "Select atleast one property type";
                    return new JsonResponse( $response );
                } else {
                    $propertyTypeOther = $form['property_type_other']->getData();
                    $extraFieldsarray['Type of property'] = ($propertyType == 'Other')?$propertyTypeOther : $propertyType;
                }

                foreach ($subcategoryids as $subcategoryid) {
                    switch ($subcategoryid) {
                        case '555' :
                            $serviceList = $form['service_list']->getData();
                            $serviceListCount = count($serviceList);
                            $serviceListOther = $form['service_list_other']->getData();
                            if($serviceListCount == 0) {
                                $response['error'] = "Select atleast one lawn & garden services";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Type of services'] = implode(', ', $serviceList).', '.$serviceListOther;
                            }
                        break;
                        case '556':
                            $landscape = $form['landscape_service']->getData();
                            $landscapeCount = count($landscape);
                            $landscapeOther = $form['landscape_other']->getData();
                            if($landscapeCount == 0) {
                                $response['error'] = "Select atleast one lawn & garden services";
                                    return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Custom features'] = implode(', ', $landscape).', '.$landscapeOther;
                            }
                        break;
                        case '557':
                            $irrigation = $form['irrigation']->getData();
                            $irrigationCount = count($irrigation);
                            $irrigationOther = $form['irrigation_other']->getData();
                            if($irrigationCount == 0) {
                                $response['error'] = "Select atleast one lawn & garden services";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Irrigation services'] = implode(', ', $irrigation).', '.$irrigationOther;
                            }
                        break;
                        case '1044':
                            $swimming = $form['swimming']->getData();
                            $swimmingCount = count($swimming);
                            $swimmingOther = $form['swimming_other']->getData();
                            if($swimmingCount == 0) {
                                $response['error'] = "Select atleast one swimming services";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Swimming services'] = implode(', ', $swimming).', '.$swimmingOther;
                            }
                        break;
                    }
                }

                $dimensions = $form['dimensions']->getData();
                if($dimensions == '') {
                    $response['error'] = "Select atleast one property dimensions";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Dimensions of property'] = $dimensions;
                /*Extra fields for landscape & gardens*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function signageSignboardsAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Signage & signboards*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $appox_width = $form['appox_width']->getData();
                if($appox_width == '') {
                    $response['error'] = "Select approximate width";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Width'] = $appox_width;
                }

                $appox_height = $form['appox_height']->getData();
                if($appox_height == '') {
                    $response['error'] = "Select approximate height";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Height'] = $appox_height;
                }

                $installation = $form['installation']->getData();
                if($installation == '') {
                    $response['error'] = "Do you require installation";
                    return new JsonResponse( $response );
                } else {
                    $extraFieldsarray['Require installation'] = $installation;
                }
                /*Extra fields for Signage & signboards*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function maintenanceAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Maintenance*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                foreach ($subcategoryids as $subcategoryid) {
                    switch ($subcategoryid) {
                        case '1006' :
                            $handyman      = $form['handyman']->getData();
                            $handymanCount = count($handyman);
                            $handymanOther = $form['handyman_other']->getData();
                            if($handymanCount == 0) {
                                $response['error'] = "Select atleast one handyman services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Handyman service'] = implode(", ",$handyman);
                                if($handymanOther != '') {
                                    $extraFieldsarray['Handyman service'] .= ", ".$handymanOther;
                                }
                            }
                            break;
                        case '1007':
                            $plumbing      = $form['plumbing']->getData();
                            $plumbingCount = count($plumbing);
                            $plumbingOther = $form['plumbing_other']->getData();
                            if($plumbingCount == 0) {
                                $response['error'] = "Select atleast one plumbing services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Plumbing service'] = implode(", ",$plumbing);
                                if($plumbingOther != '') {
                                    $extraFieldsarray['Plumbing service'] .= ", ".$plumbingOther;
                                }
                            }
                            break;
                        case '1008':
                            $appliances      = $form['appliances']->getData();
                            $appliancesCount = count($appliances);
                            $appliancesOther = $form['appliances_other']->getData();
                            if($appliancesCount == 0) {
                                $response['error'] = "Select atleast one appliances services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Appliances service'] = implode(", ",$appliances);
                                if($appliancesOther != '') {
                                    $extraFieldsarray['Appliances service'] .= ", ".$appliancesOther;
                                }
                            }
                            break;
                        case '1009':
                            $electrical      = $form['electrical']->getData();
                            $electricalCount = count($electrical);
                            $electricalOther = $form['electrical_other']->getData();
                            if($electricalCount == 0) {
                                $response['error'] = "Select atleast one electrical services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Electrical service'] = implode(", ",$electrical);
                                if($electricalOther != '') {
                                    $extraFieldsarray['Electrical service'] .= ", ".$electricalOther;
                                }
                            }
                            break;
                        case '1010':
                            $painting      = $form['painting']->getData();
                            $paintingCount = count($painting);
                            $paintingOther = $form['painting_other']->getData();
                            if($paintingCount == 0) {
                                $response['error'] = "Select atleast one painting services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Painting service'] = implode(", ",$painting);
                                if($paintingOther != '') {
                                    $extraFieldsarray['Painting service'] .= ", ".$paintingOther;
                                }
                            }
                            break;
                        case '1045':
                            $carpentry      = $form['carpentry']->getData();
                            if($carpentry == '') {
                                $response['error'] = "Select atleast one carpentry services";
                                return new JsonResponse( $response );
                            }
                            $extraFieldsarray['Carpentry Type'] = $carpentry;
                            $joinery      = $form['joinery']->getData();
                            $joineryCount = count($joinery);
                            $joineryOther = $form['joinery_other']->getData();
                            if($joineryCount == 0) {
                                $response['error'] = "Select atleast one joinery services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Joinery service'] = implode(", ",$joinery);
                                if($joineryOther != '') {
                                    $extraFieldsarray['Joinery service'] .= ", ".$joineryOther;
                                }
                            }
                            break;
                        case '1047':
                            $flooring      = $form['flooring']->getData();
                            if($flooring == '') {
                                $response['error'] = "Select atleast one flooring services";
                                return new JsonResponse( $response );
                            }
                            $extraFieldsarray['Flooring Type'] = $flooring;
                            $flooringType      = $form['flooring_type']->getData();
                            $flooringTypeCount = count($flooringType);
                            $flooringTypeOther = $form['flooring_type_other']->getData();
                            if($flooringTypeCount == 0) {
                                $response['error'] = "Select atleast one flooringType services";
                                return new JsonResponse( $response );
                            }
                            else {
                                $extraFieldsarray['Flooring service'] = implode(", ",$flooringType);
                                if($flooringTypeOther != '') {
                                    $extraFieldsarray['Flooring service'] .= ", ".$flooringTypeOther;
                                }
                            }
                            break;
                    }
                }

                $propertyType = $form['property_type']->getData();
                $propertyCount = count($propertyType);
                if($propertyCount == 0) {
                    $response['error'] = "Select atleast one property type";
                    return new JsonResponse( $response );
                } else {
                    $propertyTypeOther = $form['property_type_other']->getData();
                    $arrString = implode(", ",$musicServices);
                    $arrString = (strpos($arrString, 'Other'))? (str_replace('Other', $propertyTypeOther, $arrString)) : $arrString;
                    $extraFieldsarray['Property type'] = $arrString;
                }

                $dimensions = $form['dimensions']->getData();
                $dimensionsOther = $form['dimensions_other']->getData();
                if($dimensions == '') {
                    $response['error'] = "Select atleast one property dimension";
                    return new JsonResponse( $response );
                } else if($dimensions == 'Other'){
                    if($dimensionsOther == '') {
                        $response['error'] = "Please enter other property dimensions";
                        return new JsonResponse( $response );
                    }else {
                        $extraFieldsarray['Dimensions'] = $dimensions;
                    }
                }
                else
                    $extraFieldsarray['Dimensions'] = $dimensionsOther;
                /*Extra fields for Photography & video*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);


                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function floristsAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcateries = $form['subcateries']->getData();
                $subcategoryids[0] = $subcateries;
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Florist*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $subcategoryOther = $form['subcategory_other']->getData();
                if($subcateries == 'Other' && $subcategoryOther == '') {
                    $response['error'] = "Enter other floral type";
                    return new JsonResponse( $response );
                } else {
                    if($subcategoryOther != '')
                        $extraFieldsarray['Floral type'] = $subcategoryOther;
                }

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Event Date'] = $eventPlaned;

                $arrangementType = $form['arrangement_type']->getData();
                $arrangementTypeCount = count($arrangementType);
                if($arrangementTypeCount == 0) {
                    $response['error'] = "Select atleast one arrangement type";
                    return new JsonResponse( $response );
                } else {
                    $arrangementTypeOther = $form['arrangement_other']->getData();
                    $arrString = implode(", ",$arrangementType);
                    $arrString = (strpos($arrString,"Other"))? ', '.$arrangementTypeOther : $arrString;
                    $extraFieldsarray['Placements'] = $arrString;
                }

                $arrangementSize = $form['arrangement_size']->getData();
                $arrangementSizeCount = count($arrangementSize);
                if($arrangementSizeCount == 0) {
                    $response['error'] = "Select atleast one arrangement size";
                    return new JsonResponse( $response );
                } else {
                    $arrangementSizeOther = $form['arrangement_size_other']->getData();
                    $arrString = implode(", ",$arrangementSize);
                    $arrString = (strpos($arrString,"Other"))? ', '.$arrangementSizeOther : $arrString;
                    $extraFieldsarray['No. of Orders'] = $arrString;
                }
                /*Extra fields for Florist */

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function giftsPromotionalAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Gifts & Promotional Items*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $eventType = $form['event_services']->getData();
                if($eventType == '') {
                    $response['error'] = "Select atleast one event type";
                    return new JsonResponse( $response );
                } else {
                    $eventTypeOther = $form['event_other']->getData();
                    $extraFieldsarray['Event'] = ($eventType == 'Other')?$eventTypeOther : $eventType;
                }

                $subcategoryOther = $form['subcategory_other']->getData();
                if($subcategoryOther != '')
                    $extraFieldsarray['Other promotional'] = $subcategoryOther;

                $design = $form['design']->getData();
                if($design == '') {
                    $response['error'] = "Select atleast one choice";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Design Input'] = $design;

                $pieces = $form['pieces']->getData();
                if($pieces == '') {
                    $response['error'] = "Select atleast one choice";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['No of Items'] = $pieces;

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Select atleast one choice";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Event Date'] = $eventPlaned;

                $often = $form['often']->getData();
                if($often == '') {
                    $response['error'] = "Select atleast one choice";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Frequency'] = $often;

                $delivery = $form['delivery']->getData();
                if($delivery == '') {
                    $response['error'] = "Select atleast one delivery type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Delivery Needed'] = $eventPlaned;
                /*Extra fields for Gifts & Promotional Items*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function weedingPlannersAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Wedding Planners*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $subcategoryOther = $form['subcategory_other']->getData();
                if($subcategoryOther != '')
                    $extraFieldsarray['Other service'] = $subcategoryOther;

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Event Date'] = $eventPlaned;

                $helpType = $form['help_type']->getData();
                $helpTypeCount = count($helpType);
                if($helpTypeCount == 0) {
                    $response['error'] = "Select atleast one help type";
                    return new JsonResponse( $response );
                } else {
                    $helpTypeOther = $form['help_other']->getData();
                    $arrString = implode(", ",$helpType);
                    $arrString = (strpos($arrString,"Other"))? ', '.$helpTypeOther : $arrString;
                    $extraFieldsarray['Event'] = $arrString;
                }

                /*$helpType = $form['help_type']->getData();
                if($helpType == '') {
                    $response['error'] = "Select atleast one help type";
                    return new JsonResponse( $response );
                } else {
                    $helpTypeOther = $form['help_other']->getData();
                    $extraFieldsarray['Services Needed'] = ($helpType == 'Other')?$helpTypeOther : $helpType;
                }*/

                $guests = $form['guests']->getData();
                if($guests == '') {
                    $response['error'] = "Select no of guest attending";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['No. of Guests'] = $guests;

                $theme = $form['theme']->getData();
                if($theme == '') {
                    $response['error'] = "Select atleast one event type";
                    return new JsonResponse( $response );
                } else {
                    $themeOther = $form['theme_other']->getData();
                    $extraFieldsarray['Event Theme'] = ($theme == 'Other')?$themeOther : $theme;
                }
                /*Extra fields for Wedding Planners*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function exhibitionsAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Exhibition- Stands, Kiosks, Stages*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $eventType = $form['event_services']->getData();
                // $propertyTypeCount = count($propertyType);
                if($eventType == '') {
                    $response['error'] = "Select atleast one event type";
                    return new JsonResponse( $response );
                } else {
                    $eventTypeOther = $form['event_other']->getData();
                    $extraFieldsarray['Event'] = ($eventType == 'Other')?$eventTypeOther : $eventType;
                }

                $located = $form['located']->getData();
                if($located == '') {
                    $response['error'] = "Select atleast one location type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Location'] = $located;

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Select atleast one planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Event Date'] = $eventPlaned;

                $duration = $form['duration']->getData();
                if($duration == '') {
                    $response['error'] = "Select atleast one duration type";
                    return new JsonResponse( $response );
                }

                $assistance = $form['assistance']->getData();
                if($assistance == '') {
                    $response['error'] = "Select atleast one assistance type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Design Input'] = $assistance;

                $dimension = $form['dimension']->getData();
                if($dimension == '') {
                    $response['error'] = "Select atleast one dimension type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Dimension'] = $dimension;
                /*Extra fields for Exhibition- Stands, Kiosks, Stages*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function eventManagmentAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Event Management*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $key = array_search(1075, $subcategoryids);
                $subcategoryOther = $form['subcategory_other']->getData();
                if($key && $subcategoryOther == '') {
                    $response['error'] = "Please fill the other text";
                    return new JsonResponse( $response );
                }

                $eventType = $form['event_services']->getData();
                // $propertyTypeCount = count($propertyType);
                if($eventType == '') {
                    $response['error'] = "Select atleast one event type";
                    return new JsonResponse( $response );
                } else {
                    $eventTypeOther = $form['event_other']->getData();
                    $extraFieldsarray['Event'] = ($eventType == 'Other')?$eventTypeOther : $eventType;
                }

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Event Date'] = $eventPlaned;

                $duration = $form['duration']->getData();
                if($duration == '') {
                    $response['error'] = "Select atleast one duration type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Duration'] = $duration;

                foreach ($subcategoryids as $subcategoryid) {
                    switch ($subcategoryid) {
                        case '1014' :
                            $musicServices = $form['music_services']->getData();
                            $musicServicesCount = count($musicServices);
                            if($musicServicesCount == 0) {
                                $response['error'] = "Select atleast one music service";
                                return new JsonResponse( $response );
                            } else {
                                $musicOther = $form['music_services_other']->getData();
                                $arrString = implode(", ",$musicServices);
                                $arrString = (strpos($arrString,"Other"))? (str_replace('Other', $musicOther, $arrString)) : $arrString;
                                $extraFieldsarray['Music Services'] = $arrString;
                            }
                        break;
                        case '1015':
                            $entertainers = $form['entertainers']->getData();
                            $entertainersCount = count($entertainers);
                            if($entertainersCount == 0) {
                                $response['error'] = "Select atleast one entertainers";
                                return new JsonResponse( $response );
                            } else {
                                $entertainersOther = $form['entertainers_other']->getData();
                                $arrString = implode(", ",$entertainers);
                                $arrString = (strpos($arrString,"Other"))? (str_replace('Other', $entertainersOther, $arrString)) : $arrString;
                                $extraFieldsarray['Entertainers'] = $arrString;
                            }
                        break;
                        case '1017':
                            $models = $form['models']->getData();
                            $modelsNone = $form['models_none']->getData();
                            if($models == '' && $modelsNone='') {
                                $response['error'] = "Select atleast one models choice";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['No. of Models'] = ($models == '')?$modelsNone : $models;
                            }
                        break;
                        case '1018':
                            $hosts = $form['hosts']->getData();
                            $hostsNone = $form['hosts_none']->getData();
                            if($hosts == '' && $hostsNone='') {
                                $response['error'] = "Select atleast one models choice";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['No. of Hosts'] = ($hosts == '')?$hostsNone : $hosts;
                            }
                        break;
                        case '1019' :
                            $staff = $form['staff']->getData();
                            if($staff == '') {
                                $response['error'] = "Select atleast one staff choice";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['No. of Waiting Staff'] = $staff;
                            }
                        break;
                    }
                }

                $guests = $form['guests']->getData();
                if($guests == '') {
                    $response['error'] = "Select atleast one guests count";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['No. of Guests'] = $guests;
                /*Extra fields for Event Management*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                $eSubCategoryString = (strpos($eSubCategoryString, 'Other')) ? (str_replace('Other', $subcategoryOther, $eSubCategoryString)) : $eSubCategoryString;

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    function soundLightingAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {

            $data = $request->request->all();
            // echo '<pre/>';print_r($data);exit;
            $categoryid   = $data['categories_form']['category'];
            $selectedCity = $data['categories_form']['city'];
            $reqType = $data['categories_form']['reqtype'];

            $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
            $categoryname = $categorynamelist->getCategory();
            $subcategoryList = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
            $subcategory = array();

            foreach($subcategoryList as $sublist){
                $subid = $sublist->getId();
                $subcat = $sublist->getCategory();
                $subcategory[$subid] = $subcat;
            }
            $cities = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
            $cityArray = array();

            foreach($cities as $c){
                $city = $c->getCity();
                $cityArray[$city] = $city;
            }
            $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));


            if($formOptions) {
                $formType = 'BBids\BBidsHomeBundle\Form'."\\".$formOptions->getFormObjName();
                $form = $this->createForm(new $formType($this->getDoctrine()->getManager()),array('action' => $this->generateUrl($formOptions->getFormAjaxAction()),'categoryid'=>$categoryid,'subcategory'=>$subcategory,'cityArray'=>$cityArray,'selectedCity'=>$selectedCity,'loginFlag'=>$loginFlag,'reqType'=>$reqType));
                $twigFileName =$formOptions->getTwigName();
            }
            $form->bind( $request );

            if ( $form->isValid( ) ) {
                $em = $this->getDoctrine()->getManager();
                $response['step'] = 1;
                $subcategoryids = $form['subcategory']->getData();
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for Sound & Lighting Service Category*/
                $extraFieldsarray = array();
                $response['step'] = 2;

                $eventType = $form['event_services']->getData();
                // $propertyTypeCount = count($propertyType);
                if($eventType == '') {
                    $response['error'] = "Select atleast one event type";
                    return new JsonResponse( $response );
                } else {
                    $eventTypeOther = $form['event_other']->getData();
                    $extraFieldsarray['Event'] = ($eventType == 'Other')?$eventTypeOther : $eventType;
                }

                $eventPlaned = $form['event_planed']->getData();
                if($eventPlaned == '') {
                    $response['error'] = "Enter event planned type";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['Event Date'] = $eventPlaned;

                foreach ($subcategoryids as $subcategoryid) {
                    switch ($subcategoryid) {
                        case '1029' :
                            $musicServices = $form['sound_services']->getData();
                            $musicServicesCount = count($musicServices);
                            if($musicServicesCount == 0) {
                                $response['error'] = "Select atleast one music service";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Sound Services'] = implode(', ', $musicServices);
                            }
                        break;
                        case '1030':
                            $lightingServices = $form['lighting_services']->getData();
                            $lightingServicesCount = count($lightingServices);
                            if($lightingServicesCount == 0) {
                                $response['error'] = "Select atleast one entertainers";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Lighting Services'] = implode(', ', $lightingServices);
                            }
                        break;
                        case '1031':
                            $videoServices = $form['video_services']->getData();
                            $videoServicesCount = count($videoServices);
                            if($videoServicesCount == 0) {
                                $response['error'] = "Select atleast one entertainers";
                                return new JsonResponse( $response );
                            } else {
                                $extraFieldsarray['Video Services'] = implode(', ', $videoServices);
                            }
                        break;
                    }
                }

                $guests = $form['guests']->getData();
                if($guests == '') {
                    $response['error'] = "Select atleast one guests count";
                    return new JsonResponse( $response );
                }
                $extraFieldsarray['No. of Guests'] = $guests;
                /*Extra fields for Sound & Lighting Service Category*/

                $response['step'] = 3;
                $description = $form['description']->getData();
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire']->getData();
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();
                    } else {
                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email']->getData();

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname']->getData();

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile']->getData();

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype']->getData();
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']->getData()."-".$form['mobile']->getData();
                        $home     = $form['homecode']->getData();
                        if(!empty($home))
                            $homephone = $form['homecode']->getData()."-".$form['homephone']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode']->getData();
                        $phone    = $form['homephone']->getData();

                        $location = $form['location']->getData();
                        $city     = $form['city']->getData();

                    } else {
                        $smsphone = $form['mobilecodeint']->getData()."-".$form['mobileint']->getData();
                        $home     = $form['homecodeint']->getData();
                        if(!empty($home))
                            $homephone = $form['homecodeint']->getData()."-".$form['homephoneint']->getData();
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint']->getData();
                        $phone    = $form['homephoneint']->getData();

                        $location = $form['country']->getData();
                        $city     = $form['cityint']->getData();
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category']->getData();

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);


                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype']->getData();
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;

            } else {
                $response['errors']  = $this->getErrorMessages($form);
                $response['success'] = false;
                $response['cause']   = 'whatever';
            }
            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));
    }

    protected function createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype)
    {
        $em = $this->getDoctrine()->getManager();

        $user       = new User();
        $created    = new \DateTime();
        // $userhash   = $this->generateHash(16);

        $user->setPassword(md5($mobilecode));

        $user->setEmail($email);
        $user->setCreated($created);
        $user->setUpdated($created);
        // $user->setUserhash($userhash);
        if($locationtype == 'UAE')
            $user->setStatus(1);
        else
            $user->setStatus(4);
        $user->setMobilecode($mobilecode);

        $em->persist($user);
        $em->flush();

        $userid = $user->getId();

        $account = new Account();

        $account->setProfileid(2);
        $account->setUserid($userid);
        $account->setSmsphone($smsphone);
        $account->setHomephone($homephone);
        $account->setContactname($contactname);

        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        return $userid;
    }

    protected function createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();

        $created = new \DateTime();
        $enquiry = new Enquiry();
        $enquiry->setSubj('No Subject');
        $enquiry->setDescription($description);
        $enquiry->setCity($city);
        $enquiry->setCategory($categoryid);
        $enquiry->setAuthorid($userid);
        $enquiry->setCreated($created);
        $enquiry->setUpdated($created);

        $enquiry->setStatus(1);

        $enquiry->setLocation($location);
        $enquiry->setHirePriority($hire);

        $em = $this->getDoctrine()->getManager();
        $em->persist($enquiry);
        $em->flush();

        $enquiryid = $enquiry->getId();

        return $enquiryid;
    }

    protected function createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount)
    {
        $em = $this->getDoctrine()->getManager();
        $eSubCategoryString = '';
        for($i=0; $i<$subcount; $i++) {
            $subCatID = $subcategoryids[$i];
            $enquirysubrel = new EnquirySubcategoryRel();
            $enquirysubrel->setEnquiryid($enquiryid);
            $enquirysubrel->setSubcategoryid($subcategoryids[$i]);

            $eSubCategoryString .= $subcategory[$subCatID].", ";

            $em = $this->getDoctrine()->getManager();
            $em->persist($enquirysubrel);
            $em->flush();
        }
        return $eSubCategoryString;
    }

    protected function mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        $session->start();
        $quoteID  = $session->getId();

        $hireArray = array('No Priority', 'Urgent', '1-3 days', '4-7 days', '7-14 days', 'Just Planning','Flexible Dates');

        $formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));
        $extraInfoSMS = $emailBody = '';
        $extraInfoSMS .= "Job No: ".$enquiryid.'\r\n';
        $extraInfoSMS .= $eSubCategoryString.'\r\n';
        if($formExtraFields) {
            foreach ($formExtraFields as $field) {
                $extraInfoSMS .= $field->getKeyName().': '.$field->getKeyValue().'\r\n';
            }
            $emailBody = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig',array('formExtraFields'=>$formExtraFields,'jobNo'=>$enquiryid, 'city'=>$city,'hire'=>$hire, 'description'=>$description, 'eSubCategoryString'=>$eSubCategoryString));
        }
        $extraInfoSMS .= "City: ".$city.'\r\n';
        $extraInfoSMS .= "Priority: ".$hireArray[$hire].'\r\n';
        if($description)
            $extraInfoSMS .= "Info: ".$description.'\r\n';

        if($session->has('maptovendorid')) {
            $vendorid    = $session->get('maptovendorid');
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid,'flag'=>1));

            foreach($vendorArray as $vendor){
                $status = $vendor->getStatus();
                if($status == 1) {
                    $vendorenquiryrel = new Vendorenquiryrel();

                    $vendorenquiryrel->setVendorid($vendorid);
                    $vendorenquiryrel->setEnquiryid($enquiryid);

                    $em->persist($vendorenquiryrel);
                    $em->flush();

                    // Send sms to all vendors
                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                    if($accountArray) {
                        // Send sms to mobile number provided.
                        $vendorName    = $accountArray->getContactname();
                        $smsphone      = $accountArray->getSmsphone();
                        $smsphoneArray = explode("-",$smsphone);
                        $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                        $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                        $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');/*SMS function is blocked*/
                        /*End of sms to vendors*/
                        // Send email notification to all vendors
                        $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                        if($vendorUser) {
                            $vendorEmail = $vendorUser->getEmail();
                            $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                        }
                    }
                }
            }
        } else {
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findBy(array('categoryid'=>$categoryid,'quoteid'=>$quoteID,'status'=>1));
            $enqcount = 0;
            if(empty($vendorArray)) {

            } else {
                foreach($vendorArray as $vendor) {
                    $enqcount++;
                    $vendorid       = $vendor->getVendorid();

                    $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                    if(empty($vendenqArray)) {

                        $vendorenquiryrel = new Vendorenquiryrel();

                        $vendorenquiryrel->setVendorid($vendorid);
                        $vendorenquiryrel->setEnquiryid($enquiryid);

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($vendorenquiryrel);
                        $em->flush();

                        // Send sms to all manual mapped vendors
                        $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                        if($accountArray) {
                            // Send sms to mobile number provided.
                            $vendorName    = $accountArray->getContactname();
                            $smsphone      = $accountArray->getSmsphone();
                            $smsphoneArray = explode("-",$smsphone);
                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                            $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                            /*End of sms to vendors*/

                            // Send email notification to all manual mapped vendors
                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                            if($vendorUser) {
                               $vendorEmail = $vendorUser->getEmail();
                               $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                            }
                        }
                    }
                }
            }
        }
    }

    protected function mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $hireArray = array('No Priority', 'Urgent', '1-3 days', '4-7 days', '7-14 days', 'Just Planning','Flexible Dates');

        $formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));
        $extraInfoSMS = $emailBody = '';
        $extraInfoSMS .= "Job No : ".$enquiryid.'\r\n';
        $extraInfoSMS .= $eSubCategoryString.'\r\n';
        if($formExtraFields) {
            foreach ($formExtraFields as $field) {
                $extraInfoSMS .= $field->getKeyName().' : '.$field->getKeyValue().'\r\n';
            }
            $emailBody = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig',array('formExtraFields'=>$formExtraFields,'jobNo'=>$enquiryid, 'city'=>$city,'hire'=>$hire, 'description'=>$description, 'eSubCategoryString'=>$eSubCategoryString));
        }
        $extraInfoSMS .= "City : ".$city.'\r\n';
        $extraInfoSMS .= "Priority : ".$hireArray[$hire].'\r\n';
        if($description)
            $extraInfoSMS .= "Info : ".$description.'\r\n';

        if($session->has('maptovendorid')) {
            $vendorid    = $session->get('maptovendorid');
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid,'flag'=>1));

            foreach($vendorArray as $vendor){
                $status = $vendor->getStatus();
                if($status == 1) {
                    $vendorenquiryrel = new Vendorenquiryrel();

                    $vendorenquiryrel->setVendorid($vendorid);
                    $vendorenquiryrel->setEnquiryid($enquiryid);

                    $em->persist($vendorenquiryrel);
                    $em->flush();

                    // Send sms to all vendors
                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                    if($accountArray) {
                        // Send sms to mobile number provided.
                        $vendorName    = $accountArray->getContactname();
                        $smsphone      = $accountArray->getSmsphone();
                        $smsphoneArray = explode("-",$smsphone);
                        $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                        $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                        $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');/*SMS function is blocked*/
                        /*End of sms to vendors*/
                        // Send email notification to all vendors
                        $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                        if($vendorUser) {
                            $vendorEmail = $vendorUser->getEmail();
                            $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                        }
                    }
                }
            }
        } else {
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('categoryid'=>$categoryid,'flag'=>1));
            $enqcount = 0;
            foreach($vendorArray as $vendor) {
                $enqcount++;
                $vendorid       = $vendor->getVendorid();
                $status         = $vendor->getStatus();
                $vendorleadpack = $vendor->getLeadpack();

                $manualVendCatMapping = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findBy(array('categoryID'=>$categoryid,'vendorID'=>$vendorid,'status'=>1));
                if(empty($manualVendCatMapping)) {
                    // if($status == 1 && $vendorleadpack !=0 ) { logic changed
                    if($status == 1) {
                       $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                        if(empty($vendenqArray)) {
                             //if($enqcount == 1){
                            $vendorenquiryrel = new Vendorenquiryrel();

                            $vendorenquiryrel->setVendorid($vendorid);
                            $vendorenquiryrel->setEnquiryid($enquiryid);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($vendorenquiryrel);
                            $em->flush();

                            // Send sms to all vendors
                            $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                            if($accountArray) {
                                // Send sms to mobile number provided.
                                $vendorName    = $accountArray->getContactname();
                                $smsphone      = $accountArray->getSmsphone();
                                $smsphoneArray = explode("-",$smsphone);
                                $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                                $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                                $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                                /*End of sms to vendors*/

                                // Send email notification to all vendors
                                $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                                if($vendorUser) {
                                   $vendorEmail = $vendorUser->getEmail();
                                   $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                                }
                            }
                        }
                    }
                } else {
                    // This means that vendor is manually mapped by admin
                    $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                    if(empty($vendenqArray)) {

                        // Send sms to all manual mapped vendors
                        $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                        if($accountArray) {
                            // Send sms to mobile number provided.
                            $vendorName    = $accountArray->getContactname();
                            $smsphone      = $accountArray->getSmsphone();
                            $smsphoneArray = explode("-",$smsphone);
                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                            $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                            /*End of sms to vendors*/

                            // Send email notification to all manual mapped vendors
                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                            if($vendorUser) {
                               $vendorEmail = $vendorUser->getEmail();
                               $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                            }
                        }
                    }
                }

            }
        }
    }

    protected function setNewActivity($city,$categoryname,$userid)
    {
        $activity = new Activity();
        $created  = new \DateTime();

        $activity->setMessage('New Enquiry');
        $activity->setCity($city);
        $activity->setCategory($categoryname);
        $activity->setAuthorid($userid);
        $activity->setCreated($created);
        $activity->setType(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($activity);
        $em->flush();
    }

    protected function sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$body,$vendorName)
    {

        $body = str_replace("Dear Vendor,", "Dear $vendorName", $body);
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($vendorid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($vendorEmail);
        $useremail->setEmailsubj('JOB REQUEST '.$enquiryid);
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'JOB REQUEST '.$enquiryid;


        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $vendorEmail,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendEnquiryConfirmationEmail($userid,$email,$enquiryid)
    {
        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:enquiry_confirmation_email.html.twig',array('enquiryid'=>$enquiryid));
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('BusinessBid Network Enquiry Successfully Placed');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'BusinessBid Network Enquiry Successfully Placed';


        $json_string = array(
          'to' => array(
            $email,'rachelle@businessbid.ae','greeth@businessbid.ae','management@businessbid.ae'
          ),
          'category' => 'test_category'
        );
        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendWelcomeEmail($name,$userid,$email,$password)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:inline_activationemail.html.twig', array('userid'=>$userid,'name'=>$name,'email'=>$email,'password'=>$password));

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('Welcome to the BusinessBid Network');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'Welcome to the BusinessBid Network';

        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function generateHash($length)
    {
        $characters   = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for($i=0; $i< $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    protected function sendSMSToPhone($phoneNumber, $text, $type='')
    {

        //$phoneNumber = substr($phoneNumber, 1);
        $subphone = substr($phoneNumber, 1);
        $smsphone = "971".$subphone;

         if(strpos($text, 'for the customer information'))
            $senderid = 3579;
        else
            $senderid = 'Businessbids';

        $smsJson = '{
                    "authentication": {
                        "username": "agefilms",
                        "password": "12345Uae"
                    },
                    "messages": [
                        {
                            "sender": "'.$senderid.'",
                            "text": "'.$text.'",';
        if($type) {
            $smsJson .= '"type":"longSMS",';
        }
        $smsJson .= '
                            "recipients": [
                                {
                                    "gsm": "'.$smsphone.'"
                                }
                            ]
                        }
                    ]
                }';


        $encodeSMS = base64_encode(json_encode($smsJson));
        $ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: api.infobip.com',
            'Accept: */*',
            'Content-Length: ' . strlen($smsJson))
        );

        $result = curl_exec($ch);
        // $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // echo "status : ".$http_status;
         // echo 'response: '.$result;
        $response        = new Response($result);
        $encodedResponse = base64_encode(json_encode($response));

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $sentSMS = $connection->prepare("INSERT INTO `bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, $phoneNumber, '$encodeSMS', '$encodedResponse', NOW())");
        $sentSMS->execute();

        return $response;
    }

    protected function reCaptchaValidation($challengeField,$responseField)
    {
        require_once('/var/www/html/web/gocaptcha/recaptchalib.php');
        $privatekey = '6LcCrQMTAAAAAIz-hCCncUjsw9SpGtvF0CZDimeh';
        $resp = recaptcha_check_answer ($privatekey,
                        $_SERVER["REMOTE_ADDR"],
                        $challengeField,
                        $responseField);

        if ($resp->is_valid) {
            return 'Valid';
        } else {
            # set the error code so that we can display it
           $error = $resp->error;
           return $error;
        }
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}/*End of class*/

<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\City;
use BBids\BBidsHomeBundle\Entity\Categories;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\Account;
use BBids\BBidsHomeBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use BBids\BBidsHomeBundle\Entity\Keyword;
use BBids\BBidsHomeBundle\Entity\Orderproductsrel;
use BBids\BBidsHomeBundle\Entity\Enquiry;
use Ob\HighchartsBundle\Highcharts\Highchart;
use BBids\BBidsHomeBundle\Entity\Reviews;
use BBids\BBidsHomeBundle\Entity\Activity;
use BBids\BBidsHomeBundle\Entity\Order;
use BBids\BBidsHomeBundle\Entity\Freetrail;
use BBids\BBidsHomeBundle\Entity\Leadcredit;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorsubcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendororderproductsrel;
use BBids\BBidsHomeBundle\Entity\Checkpaymenthistory;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use BBids\BBidsHomeBundle\Entity\Vendorcategorymapping;
use BBids\BBidsHomeBundle\Entity\Cashpaymenthistory;
use BBids\BBidsHomeBundle\Entity\Registrationfeecatrel;
use BBids\BBidsHomeBundle\Form\UploadReviewsForm;
use BBids\BBidsHomeBundle\Entity\Vendorlogorel;
use BBids\BBidsHomeBundle\Entity\Leadtransactionhistory;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use BBids\BBidsHomeBundle\Entity\FormElements;
use BBids\BBidsHomeBundle\Entity\FormElementTypes;
use BBids\BBidsHomeBundle\Entity\ElementListValues;
use Doctrine\ORM\EntityRepository;

class AdminController extends Controller
{
	function manualLeadDeductionAction(Request $request) {
		$session = $this->container->get('session');

		$em = $this->getDoctrine()->getManager();
		if($session->has('uid') && $session->get('pid')==1) {
			$typeofChoice = array('deduct' => 'Deduct', 'return' => 'Return');
			$form = $this->createFormBuilder()
                        ->add('vendorname', 'text', array('label'=>'Vendor Name', 'attr'=>array('placeholder'=>'Start typing vendor name','maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')))
						->add('vid', 'hidden')
                        ->add('jobno', 'text', array('label'=>'Job No.', 'attr'=>array('placeholder'=>'Enter jobno','maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')))
                        ->add('type', 'choice', array('choices'=>$typeofChoice, 'multiple'=>false, 'expanded'=>false, 'empty_data'=>null, 'empty_value'=>'Choose return / deductions'))
                        ->add('submit', 'submit')
                        ->getForm();
            $form->handleRequest($request);
	        $response =  $this->render('BBidsBBidsHomeBundle:Admin:manual_lead_dedcutions.html.twig', array('form'=>$form->createView()));
			if($request->isMethod('POST'))
			{
				if($form->isValid())
				{
					$vendorname = $form['vendorname']->getData();
					$vendorid   = $form['vid']->getData();
					$jobno = $form['jobno']->getData();
					$type  = $form['type']->getData();

					$enquiryid = (int)$jobno;

					if($type == 'deduct')
					{
						$enquiryObject   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);
						// echo "enquiryObject <pre/>";print_r($enquiryObject);exit;
						if(is_null($enquiryObject)) {
							$session->getFlashBag()->add('error','Unable to find the job you are requesting.');
							return $response;
						} else {
							$categoryid = $enquiryObject->getCategory();
							$vendorcategoriesrelObject = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findOneBy(array('vendorid'=>$vendorid, 'categoryid'=>$categoryid, 'flag'=>1, 'status'=>1));
							if(!empty($vendorcategoriesrelObject)) {

								$currentLeads = $vendorcategoriesrelObject->getLeadpack();
								if($currentLeads == 0) {
									$session->getFlashBag()->add('error','Vendor "'.$vendorname.'" is not having enough leads to accept job.');
									return $response;
								}

								// Check for mapping
								$query = $em->createQueryBuilder()
				                	->select('e')
									->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
									->where('e.enquiryid = :eid')
									->andWhere('e.vendorid = :vendorid')
				                    ->setParameter('eid', $enquiryid)
				                    ->setParameter('vendorid', $vendorid)
				                    ->setMaxResults(1);
		                    	$vendorEnqRel = $query->getQuery()->getResult();
		                    	// echo "Vendorenquiryrel object <pre/>";print_r($vendorEnqRel);exit;
		                    	if(empty($vendorEnqRel)) {
		                    		$vendorenquiryrel = new Vendorenquiryrel();

		                            $vendorenquiryrel->setVendorid($vendorid);
		                            $vendorenquiryrel->setEnquiryid($enquiryid);
		                            $vendorenquiryrel->setAcceptstatus(1);
		                            $vendorenquiryrel->setFlag(1);

		                            $em->persist($vendorenquiryrel);
		                            $em->flush();
		                    	} else {
		                    		$vEnqID = $vendorEnqRel[0]->getId();
		                    		$vendorEnqRelrepo = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vEnqID);
		                    		$em->flush();
									$vendorEnqRelrepo->setAcceptstatus(1);
									$vendorEnqRelrepo->setFlag(1);
									$em->flush();
		                    	}

		                    	// Deduct Leads
		                    	$newPackValue = $currentLeads - 1;
								$vendorcategoriesrelObject->setLeadpack($newPackValue);

								$em->persist($vendorcategoriesrelObject);
								$em->flush();

								$leadTransaction = new Leadtransactionhistory();

								$created = new \DateTime();

	                            $leadTransaction->setOrderNumber($enquiryid);
	                            $leadTransaction->setVendorid($vendorid);
	                            $leadTransaction->setType('Deduction');
	                            $leadTransaction->setCreated($created);
	                            $leadTransaction->setCategoryid($categoryid);
	                            $leadTransaction->setLeadcount($currentLeads);
	                            $leadTransaction->setNewleadcount($newPackValue);

	                            $em->persist($leadTransaction);
	                            $em->flush();

		                    	$session->getFlashBag()->add('success','Vendor '.$vendorname.' accept job no:'.$enquiryid.' successfully accepted.');
		                    	return $this->redirect($this->generateUrl('bizbids_lead_dedcution_return_upload'));
							} else {
								$session->getFlashBag()->add('error','Vendor "'.$vendorname.'" have not purchased leads or vendor status is Inactive.');
								return $response;
							}
						}/*Enough leads else ends here*/
					/* "Deduct " ends here */
					} else {
						$query = $em->createQueryBuilder()
			                	->select('e')
								->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
								->where('e.enquiryid = :eid')
								->andWhere('e.vendorid = :vendorid')
			                    ->setParameter('eid', $enquiryid)
			                    ->setParameter('vendorid', $vendorid)
			                    ->setMaxResults(1);
                    	$vendorEnqRel = $query->getQuery()->getResult();
                    	// echo "Vendorenquiryrel object <pre/>";print_r($vendorEnqRel);exit;
                    	if(empty($vendorEnqRel)) {
                    		$session->getFlashBag()->add('error','Vendor "'.$vendorname.'" is not mapped to Jobno :'.$jobno);
							return $response;
                    	} else {
                    		$enquiryObject   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);
                    		if(is_null($enquiryObject)) {
								$session->getFlashBag()->add('error','Unable to find the job you are requesting.');
								return $response;
							} else {
								// Check for mapping
								$query = $em->createQueryBuilder()
				                	->select('e')
									->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
									->where('e.enquiryid = :eid')
									->andWhere('e.vendorid = :vendorid')
				                    ->setParameter('eid', $enquiryid)
				                    ->setParameter('vendorid', $vendorid)
				                    ->setMaxResults(1);
		                    	$vendorEnqRel = $query->getQuery()->getResult();
		                    	// echo "Vendorenquiryrel object <pre/>";print_r($vendorEnqRel);exit;
		                    	if($vendorEnqRel) {
		                    		$vEnqID = $vendorEnqRel[0]->getId();
		                    		$acceptStatus = $vendorEnqRel[0]->getAcceptstatus();
		                    		if($acceptStatus == 1) {
			                    		$vendorEnqRelrepo = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($vEnqID);
			                    		$em->flush();
										$vendorEnqRelrepo->setAcceptstatus(null);
										$vendorEnqRelrepo->setFlag(null);
										$em->flush();
									} else {
										$session->getFlashBag()->add('error','Vendor "'.$vendorname.'" have not accepted the job no: "'.$jobno.'" so lead cannot be returned.');
										return $this->redirect($this->generateUrl('bizbids_lead_dedcution_return_upload'));
									}
		                    	}

								$categoryid = $enquiryObject->getCategory();
								$vendorcategoriesrelObject = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findOneBy(array('vendorid'=>$vendorid, 'categoryid'=>$categoryid, 'flag'=>1));

								$currentLeads = $vendorcategoriesrelObject->getLeadpack();
								// Return Leads
		                    	$newPackValue = $currentLeads + 1;
								$vendorcategoriesrelObject->setLeadpack($newPackValue);

								$em->persist($vendorcategoriesrelObject);
								$em->flush();

								$leadTransaction = new Leadtransactionhistory();

								$created = new \DateTime();

	                            $leadTransaction->setOrderNumber($enquiryid);
	                            $leadTransaction->setVendorid($vendorid);
	                            $leadTransaction->setType('Return');
	                            $leadTransaction->setCreated($created);
	                            $leadTransaction->setCategoryid($categoryid);
	                            $leadTransaction->setLeadcount($currentLeads);
	                            $leadTransaction->setNewleadcount($newPackValue);

	                            $em->persist($leadTransaction);
	                            $em->flush();

		                    	$session->getFlashBag()->add('success','Vendor '.$vendorname.' returning a lead against job no:'.$enquiryid.' is successful.');
		                    	return $this->redirect($this->generateUrl('bizbids_lead_dedcution_return_upload'));
	                    	}
						}
					}/*"return" ends*/
				}
			}
			return $response;

		} else {
			$session = $this->container->get('session');
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function leadrefundAction($offset, Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){
			$sesionid = $session->get('uid');

			$em = $this->getDoctrine()->getManager();

			$start = (( $offset * 10) - 10);

			$profileid = 3;
			$cquery = $em->createQueryBuilder()
				->select('a.userid', 'a.bizname')
				->from('BBidsBBidsHomeBundle:Account', 'a')
				->where('a.profileid = :profileid')
				->add('orderBy', 'a.bizname')
				->setParameter('profileid', $profileid);

			$contacts = $cquery->getQuery()->getArrayResult();

			$ccount = count($contacts);
			if($contacts){
				for($i = 0; $i < $ccount; $i++){
					$uid = $contacts[$i]['userid'];
					$c[$uid] = $contacts[$i]['bizname'];
				}
			}
			else{
				$c=0;
			}
			$searchform = $this->createFormBuilder()

                                ->add('contactname', 'choice', array('choices'=>$c, 'multiple'=>false, 'expanded'=>false, 'empty_data'=>null, 'empty_value'=>'Choose Business Name'))
                                ->add('search', 'submit')
                                ->getForm();

	                $searchform->handleRequest($request);

			if($request->isMethod('POST')){
				if($searchform->isValid()){
					$bizid = $searchform['contactname']->getData();

					$query = $em->createQueryBuilder()
	                        ->select('a.contactname, a.bizname, c.category, l.leadcount, l.newleadcount, l.created, l.type')
			                ->from('BBidsBBidsHomeBundle:Leadtransactionhistory' ,'l')
	                		->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with',  'l.vendorid = a.userid')
	                        ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'c.id = l.categoryid')
							->where('l.vendorid = :userid')
    		                ->add('orderBy', 'l.created DESC')
							->setParameter('userid', $bizid);

    		        $credits = $query->getQuery()->getArrayResult();
    		        // echo"<pre/>";print_r($credits);exit;
					$count = count($credits);
					$from = 0;
					$to = $count;
					return $this->render('BBidsBBidsHomeBundle:Admin:getleadcredits.html.twig', array('credits'=>$credits, 'from'=>$from, 'to'=>$to, 'count'=>$count, 'form'=>$searchform->createView()));
				}
			}

			$query = $em->createQueryBuilder()
				->select('a.contactname, a.bizname, c.category, l.leadcount, l.newleadcount, l.created, l.type')
				->from('BBidsBBidsHomeBundle:Leadtransactionhistory' ,'l')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with',  'l.vendorid = a.userid')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'c.id = l.categoryid')
				->add('orderBy', 'l.created DESC')
				->setFirstResult($start)
				->setMaxResults(10);

			$credits = $query->getQuery()->getArrayResult();
			// echo"<pre/>";print_r($credits);exit;
			$from = $start + 1;

			$to = $start + 10;

			$creditsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Leadtransactionhistory')->findAll();

			$count = count($creditsArray);

			if($to > $count)
				$to = $count;
			return $this->render('BBidsBBidsHomeBundle:Admin:getleadcredits.html.twig', array('credits'=>$credits, 'from'=>$from, 'to'=>$to, 'count'=>$count, 'form'=>$searchform->createView()));
		}
	}

	public function reviewsUploadAjaxAction(Request $request) {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getManager();
        $authorid = $request->query->get('customer_id');
        if($request->query->get('type') == 'enq'){
	        $vendorID = $request->query->get('vendor_id');
	        if($vendorID) {
	        	$query = $em->createQueryBuilder()
	        			->select('v.enquiryid')
	        			->from('BBidsBBidsHomeBundle:Vendorenquiryrel','v')
	        			->where('v.vendorid = :vendorid')
						->setParameter('vendorid', $vendorID);
				$vendorEnqIDs = $query->getQuery()->getScalarResult();
	        }
	        $result = array();
	        foreach ($vendorEnqIDs as $vendorEnqID) {
	            $result[$vendorEnqID['enquiryid']] = $vendorEnqID['enquiryid'];
	        }
	        return new JsonResponse($result);
        }
        $transform = function($item) {
		    return $item['userid'];
		};
        $enquiryids = array_map($transform, $em->createQueryBuilder()
			->select('e.id as userid')
			->from('BBidsBBidsHomeBundle:Enquiry', 'e')
			->where('e.authorid = :authorid')
			->setParameter('authorid', $authorid)->getQuery()->getScalarResult());

        if($enquiryids) {
        	$query = $em->createQueryBuilder()
        			->select('v.vendorid,a.bizname')
        			->from('BBidsBBidsHomeBundle:Vendorenquiryrel','v')
        			->innerJoin('BBidsBBidsHomeBundle:Account','a','with', 'a.userid = v.vendorid')
        			->where('v.enquiryid IN (:ids)')->add('orderBy','a.bizname')->setParameter('ids', $enquiryids);
			$vendorArray = $query->getQuery()->getResult();
			// echo "<pre/>";print_r($vendorArray);exit;
        }
        $result = array();
        foreach ($vendorArray as $vendor) {
            $result[$vendor['vendorid']] = $vendor['bizname'];
        }
        // echo "<pre/>";print_r($result);exit;
        return new JsonResponse($result);
    }

	function vendReviewsUploadAction(Request $request)
	{
		$session = $this->container->get('session');

		$em = $this->getDoctrine()->getManager();
		if($session->has('uid') && $session->get('pid')==1) {
			$profileid = 2;
			$cquery = $em->createQueryBuilder()
				->select('a.userid', 'a.contactname')
				->from('BBidsBBidsHomeBundle:Account', 'a')
				->where('a.profileid = :profileid')
				->add('orderBy', 'a.contactname')
				->setParameter('profileid', $profileid);
			$customersObject = $cquery->getQuery()->getResult();

			/*$searchform = $this->createFormBuilder()
                        ->add('customername', 'choice', array('choices'=>$customersObject, 'multiple'=>false, 'expanded'=>false, 'empty_data'=>null, 'empty_value'=>'Choose Business Name'))
                        ->add('vendorname', 'choice', array('choices'=>$customersObject, 'multiple'=>false, 'expanded'=>false, 'empty_data'=>null, 'empty_value'=>'Choose Business Name'))
                        ->add('search', 'submit')
                        ->getForm();*/


			$searchform = $this->createForm(new UploadReviewsForm($this->getDoctrine()->getManager()));

            $searchform->handleRequest($request);
	        $response =  $this->render('BBidsBBidsHomeBundle:Admin:upload_reviews.html.twig', array('form'=>$searchform->createView()));
			if($request->isMethod('POST')) {
				if($searchform->isValid()) {

				}
			}
			return $response;

		} else {
			$session = $this->container->get('session');
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	function anonymousReviewsUploadAction(Request $request)
	{
		$session = $this->container->get('session');

		$em = $this->getDoctrine()->getManager();
		if($session->has('uid') && $session->get('pid')==1) {
			$profileid = 3;
			$cquery = $em->createQueryBuilder()
				->select('a.userid', 'a.contactname')
				->from('BBidsBBidsHomeBundle:Account', 'a')
				->where('a.profileid = :profileid')
				->add('orderBy', 'a.contactname')
				->setParameter('profileid', $profileid);
			$customersObject = $cquery->getQuery()->getResult();


			$searchform = $this->createForm(new UploadReviewsForm($this->getDoctrine()->getManager()));

			$form = $this->createFormBuilder()
            ->add('customer_name', 'text', array('data'=>'','label'=>'Customer Name', 'attr'=>array('placeholder'=>'Enter Customer Name','maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')))
			->add('vendor', 'entity', array(
					'property' => 'bizname',
				    'class' => 'BBidsBBidsHomeBundle:Account',
				    'query_builder' => function(EntityRepository $er) {
				    	$profileid = 3;
				        return $er->createQueryBuilder('a')
							->where('a.profileid = :profileid')
							->add('orderBy', 'a.contactname')
							->setParameter('profileid', $profileid);
				    },
				))
			->add('businessknow', 'choice', array('label'=>'How do you know this business?', 'choices'=>array('I hired them'=>'I hired them', 'I spoke with them but did not hire'=>'I spoke with them but did not hire'), 'mapped'=>false, 'expanded'=>false, 'multiple'=>false, 'empty_value'=>'Select a value', 'empty_data'=>null))
			->add('businessrecomond','choice', array('label'=>'Would you recommend this business?', 'choices'=>array('No'=>'No', 'Yes'=>'Yes'), 'mapped'=>false, 'expanded'=>false, 'multiple'=>false))
			->add('ratework', 'hidden', array('mapped'=>false))
			->add('rateservice','hidden', array('mapped'=>false))
			->add('ratemoney', 'hidden', array('mapped'=>false))
			->add('ratebusiness', 'hidden', array('mapped'=>false))
			->add('servicesummary', 'textarea', array('label'=>'What else would you like others to know about your service?', 'mapped'=>false))
			->add('notchoose', 'choice', array('label'=>'Why didn’t you hire this company? (can choose multiple boxes)', 'choices'=>array('Although they were good, I hired another business'=>'Although they were good, I hired another business', 'I hired a business outside of Business Bid'=>'I hired a business outside of Business Bid', 'I had to cancel the project'=>'I had to cancel the project', 'They showed poor customer service'=>'They showed poor customer service','They were too expensive'=>'They were too expensive', "They didn't contact me"=>'They didn’t contact me', 'They missed or were late for a scheduled appointment'=>'They missed or were late for a scheduled appointment','Something else (please include in comment below)'=>'Something else (please include in comment below)'), 'mapped'=>false, 'expanded'=>true, 'multiple'=>true))
			->add('serviceexperience', 'textarea', array('mapped'=>false))
			->add('submit','submit',array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
            ->getForm();
            // $response =  $this->render('BBidsBBidsHomeBundle:Admin:upload_reviews.html.twig', array('form'=>$searchform->createView(),'anonymousForm'=>$anonymousForm->createView()));
            $response =  $this->render('BBidsBBidsHomeBundle:Admin:anonymous_reviews.html.twig', array('form'=>$form->createView()));

	        $form->handleRequest($request);
			if($request->isMethod('POST')){
				if($form->isValid()){
					$customerName      = $form['customer_name']->getData();
					$vendorAccID       = $form['vendor']->getData();
					$businessknow      = $form['businessknow']->getData();
					$businessrecomond  = $form['businessrecomond']->getData();
					$ratework          = $form['ratework']->getData();
					$rateservice       = $form['rateservice']->getData();
					$ratemoney         = $form['ratemoney']->getData();
					$ratebusiness      = $form['ratebusiness']->getData();
					$servicesummary    = $form['servicesummary']->getData();
					$notchoose         = $form['notchoose']->getData();
					$serviceexperience = $form['serviceexperience']->getData();

					$rating = round((($ratework + $rateservice + $ratemoney + $ratebusiness) / 4), 1);

					$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->find($vendorAccID);
					if ($accountArray) {
						$vendorid = $accountArray->getUserid();
					}

					// echo"<pre/>";print_r($vendorid);exit;
					$created = new \Datetime();

					$customerName = (empty($customerName)) ? 'Anonymous': $customerName;
					$author = 'Anonymous';
					$enquiryid = 0;
					$authorid = 0;

					$em = $this->getDoctrine()->getManager();

					$reviews = new Reviews();

					$reviews->setVendorid($vendorid);
					$reviews->setServiceexperience($serviceexperience);
					$reviews->setReview($serviceexperience);
					$reviews->setAuthor($customerName);
					$reviews->setCreated($created);
					$reviews->setModstatus(1);
					$reviews->setStatus(0);
					$reviews->setRating($rating);
					$reviews->setEnquiryid($enquiryid);
					$reviews->setAuthorid($authorid);
					$reviews->setBusinessknow($businessknow);
					$reviews->setBusinessrecomond($businessrecomond);
					$reviews->setServicesummary($servicesummary);
					$reviews->setShowname($author);
					$reviews->setServiceexperience($serviceexperience);

					$em->persist($reviews);
					$em->flush();


					$ratingArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Ratings')->findBy(array('vendorid'=>$vendorid));

					foreach($ratingArray as $r){
						$vrating = $r->getRating();
						$id = $r->getId();
					}

					if(isset($id)){

						if($vrating!=0)
							$updaterating = round((($rating + $vrating)/2),2);
						else
							$updaterating = $rating;

						$em = $this->getDoctrine()->getManager();

						$ratings = $em->getRepository('BBidsBBidsHomeBundle:Ratings')->find($id);

						$ratings->setRating($updaterating);

						$em->flush();
					}
					else {
						$em = $this->getDoctrine()->getManager();

						$ratings = new Ratings();

						$ratings->setVendorid($vendorid);
						$ratings->setRating($rating);

						$em->persist($ratings);
						$em->flush();
					}
					$session->getFlashBag()->add('success', 'Review Post successful. Review is published.');
					return $this->redirect($this->generateUrl('bizbids_vendor_reviews_upload'));
				}
			}

			return $response;

		} else {
			$session = $this->container->get('session');
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}



	function getleadscreditAction()
	{
		$session = $this->container->get('session');

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('l.leadcount, l.newleadcount, l.leadcredited, l.created, c.category, a.bizname, a.userid')
			->from('BBidsBBidsHomeBundle:Leadcredit', 'l')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'c.id = l.categoryid')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = l.userid')
			->setFirstResult($start)
			->setMaxResults(10);

		$leads = $query->getQuery()->getArrayResult();

		$leadsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Leadcredit')->findAll();
		$count = count($leadsArray);
	}



	public function viewtryleadsAction($offset)
	{
		$em = $this->getDoctrine()->getManager();

                $start = (($offset * 10) - 10);

		$query = $em->createQueryBuilder()
			->select('o.id, o.orderid, o.ordernumber, o.authorid, o.status')
			->from('BBidsBBidsHomeBundle:Ordertotryleadsrel', 'o')
			->add('orderBy', 'o.id DESC')
			->setFirstResult($start)
			->setMaxResults(10);

		$orders = $query->getQuery()->getResult();

		$from = $start+1;

                $to = $start + 10;

		 $query = $em->createQueryBuilder()
                        ->select('count(o.id)')
                        ->from('BBidsBBidsHomeBundle:Ordertotryleadsrel', 'o');

                $count = $query->getQuery()->getSingleScalarResult();

		return $this->render('BBidsBBidsHomeBundle:Admin:viewtryleads.html.twig', array('orders'=>$orders, 'from'=>$from, 'to'=>$to, 'count'=>$count));
	}

	public function adminReportAction(Request $request)
	{
		$session = $this->container->get('session');

		if($session->has('uid') && $session->get('pid')==1){


		$enquiry = new Enquiry();

		$form = $this->createFormBuilder($enquiry)
			    ->add('entity', 'choice' ,array('choices'=>array( '1'=>'Vendors', '2'=>'Customers','3'=>'vendor Orders',  '4'=>'Business Name','5'=>'Categories','6'=>'City','7'=>'Lead Package','8'=>'Payment Type','9'=>'Amount','10'=>'Job Requests','11'=>'Support Tickets'), 'empty_data'=>2,  'empty_value'=>'Select Entity', 'mapped'=>false))
				->add('fromdate','text',array('mapped'=>false,'label'=>'From Date','read_only' => true))
				->add('todate','text',array('mapped'=>false,'label'=>'To Date','read_only' => true))
                        ->add('filter', 'submit')
                        ->getForm();
       	$em = $this->getDoctrine()->getManager();
     		  $form->handleRequest($request);
		if($request->isMethod('POST')){
			if($form->isValid()){
				$entity = $form['entity']->getData();
				$fromdate = $form['fromdate']->getData();
				$todate = $form['todate']->getData();

				if($fromdate == "" && $todate != ""){
					$session = $this->container->get('session');

					$session->getFlashBag()->add('error','Please select From date');

					return $this->render('BBidsBBidsHomeBundle:Admin:admin_report.html.twig', array('form'=>$form->createView()));
				}

				else if($todate == "" && $fromdate != ""){
					$session = $this->container->get('session');

                                        $session->getFlashBag()->add('error','Please select To date');

					return $this->render('BBidsBBidsHomeBundle:Admin:admin_report.html.twig', array('form'=>$form->createView()));

				}
				else if($entity == "" && $fromdate == "" && $todate == ""){
					$session = $this->container->get('session');

                                        $session->getFlashBag()->add('error','Please select atleast one filter criteria');

					return $this->render('BBidsBBidsHomeBundle:Admin:admin_report.html.twig', array('form'=>$form->createView()));

				}
				else if($entity == 10 ){ // enquiries

					 $totquery = $em->createQueryBuilder()
			                     ->select('count(e.id)')
        		                     ->from('BBidsBBidsHomeBundle:Enquiry', 'e')
	                	              ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
                                		->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'e.authorid=u.id')
										->add('orderBy', 'e.id DESC');


					$count = $totquery->getQuery()->getSingleScalarResult();


			      		 $query = $em->createQueryBuilder()
			                     ->select('e.id, e.subj, e.description, e.city, e.category, e.authorid, e.status, e.created, c.category, u.email')
        		                     ->from('BBidsBBidsHomeBundle:Enquiry', 'e')
	                	              ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'e.category=c.id')
                                		->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'e.authorid=u.id')
											->add('orderBy', 'e.id DESC');



				}
				else if($entity == 3){ //  orders

					$totquery = $em->createQueryBuilder()
			                    	->select('count(o.id)')
						->from('BBidsBBidsHomeBundle:Order', 'o')
						->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
						->add('orderBy', 'o.created DESC');


					$count = $totquery->getQuery()->getSingleScalarResult();

					$query = $em->createQueryBuilder()
			                    	->select('o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname')
						->from('BBidsBBidsHomeBundle:Order', 'o')
						->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
						->add('orderBy', 'o.created DESC');

				}
				else if($entity == 2){ // users

					$totquery = $em->createQueryBuilder()
			                        ->select('count(u.id)')
						   ->from('BBidsBBidsHomeBundle:User', 'u')
						   ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id = a.userid')
			                        ->add('orderBy', 'u.id DESC');
					$count = $totquery->getQuery()->getSingleScalarResult();

					$query = $em->createQueryBuilder()
			                        ->select('u.id, u.email, u.created, u.updated, u.lastaccess, u.status, a.profileid, a.contactname')
						   ->from('BBidsBBidsHomeBundle:User', 'u')
						   ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id = a.userid')
			                        ->add('orderBy', 'u.id DESC');

				}


			$entityres = $query->getQuery()->getResult();



				return $this->render('BBidsBBidsHomeBundle:Admin:admin_report.html.twig', array('entity'=>$entity,'form'=>$form->createView(),'res'=>$entityres));
			}
		}

        return $this->render('BBidsBBidsHomeBundle:Admin:admin_report.html.twig', array('entity'=>0,'form'=>$form->createView(), 'res'=>array()));
	}else {
			$session = $this->container->get('session');

                        $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}

	}



	public function vieworderAction($id,$type)
	{
		$em = $this->getDoctrine()->getManager();

		$order = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Order')->findBy(array('id'=>$id));

		$query = $em->createQueryBuilder()
			->select('c.category, l.leadpack')
			->from('BBidsBBidsHomeBundle:Categories', 'c')
			->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' ,'c.id = l.categoryid')
			->where('l.ordernumber = :id')
			->setParameter('id', $id);

		$categories = $query->getQuery()->getResult();
		// echo '<pre/>';print_r($categories);exit;
		//$categories = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Orderproductsrel')->findBy(array('ordernumber'=>$id));
		$chequeArray = array();
		$vendorCompName = '';
		if($type == 'Check Payment') {
			$chequeDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Checkpaymenthistory')->findOneBy(array('orderNumber'=>'BB'.$id));			// echo '<pre/>';print_r($chequeDetails);exit;

			$chequeArray['address'] = $chequeDetails->getAddress();
			$chequeArray['city'] = $chequeDetails->getCity();
			$chequeArray['contactnumber'] = $chequeDetails->getContactNumber();
			$chequeArray['contactperson'] = $chequeDetails->getContactPerson();
			$chequeArray['bankname'] = $chequeDetails->getBankName();
			$chequeArray['checknumber'] = $chequeDetails->getCheckNumber();

			$vendorid = $chequeDetails->getAuthor();
			$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
			$vendorCompName = $account->getBizname();
		} else if($type == 'Cash Payment') {
			$cashDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Cashpaymenthistory')->findOneBy(array('orderNumber'=>'BB'.$id));			// echo '<pre/>';print_r($chequeDetails);exit;

			$chequeArray['address'] = $cashDetails->getAddress();
			$chequeArray['city'] = $cashDetails->getCity();
			$chequeArray['contactnumber'] = $cashDetails->getContactNumber();
			$chequeArray['contactperson'] = $cashDetails->getContactPerson();

			$vendorid = $cashDetails->getAuthor();
			$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
			$vendorCompName = $account->getBizname();
		}

		return $this->render('BBidsBBidsHomeBundle:Admin:order.html.twig', array('order'=>$order, 'categories'=>$categories, 'formData'=>$chequeArray, 'vendorCompName'=>$vendorCompName));
	}

	public function orderapproveAction($id)
	{
		$em    = $this->getDoctrine()->getManager();
		$created = new \DateTime();

		$order = $em->getRepository('BBidsBBidsHomeBundle:Order')->findOneBy(array('id'=>$id));
		// echo '<pre/>';print_r($order);exit;
		$orderNumber = $order->getOrdernumber();
		$payoption = $order->getPayoption();
		$order->setStatus(1);

		$order->setTransactionstatus('Successful');
		$em->flush();

		//Set check or cash payment history table to successful
		if($payoption == 'Cash Payment') {
			$subject = 'Cash Clearence Update';

			$cashPaymenthistory = $em->getRepository('BBidsBBidsHomeBundle:Cashpaymenthistory')->findOneBy(array('orderNumber'=>$orderNumber));
			$cashPaymenthistory->setUpdated($created);
			$cashPaymenthistory->setStatus(1);
			$em->flush();
		} else {
			$subject = 'Cheque Clearence Update';

			$checkPaymenthistory = $em->getRepository('BBidsBBidsHomeBundle:Checkpaymenthistory')->findOneBy(array('orderNumber'=>$orderNumber));
			$checkPaymenthistory->setUpdated($created);
			$checkPaymenthistory->setStatus(1);
			$em->flush();
		}



		$vendorid   = $order->getAuthor();
		$ono        = $order->getOrdernumber();
		$leadsArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Orderproductsrel')->findBy(array('ordernumber'=>$id));

		/* One Charge 600 AED */
		$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
		$vendorcontactname = $account->getContactname();

        $account->setOtcStatus(1);
        $em->persist($account);
		$em->flush();

		$userArray    = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
		$vendoremail     = $userArray->getEmail();

		foreach($leadsArray as $l) {
			$categoryid = $l->getCategoryid();
			$pack       = $l->getLeadpack();

			// Disable the manual category mapping
			$manualVendCatMapping = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findOneBy(array('vendorID'=>$vendorid));
			if(!empty($manualVendCatMapping)) {

				$manualVendCatMapping->setStatus(0);

				$em->persist($manualVendCatMapping);
				$em->flush();
			}

			//Step 1 insert into SUbCategories
			$subCategoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));

			foreach ($subCategoryArray as $subCategory) {
				$subCatid = $subCategory->getId();
				$subCategoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorsubcategoriesrel')->findOneBy(array('subcategoryid'=>$subCatid, 'vendorid'=>$vendorid));

				if(is_null($subCategoryArray))
				{
					$newVendorSubCat = new Vendorsubcategoriesrel();
					$newVendorSubCat->setVendorid($vendorid);
					$newVendorSubCat->setSubcategoryid($subCatid);
					$newVendorSubCat->setStatus(1);

					$em->persist($newVendorSubCat);
					$em->flush();
				} else {
					$subCategoryArray->setStatus(1);
					$this->getDoctrine()->getManager()->flush();
				}
			}
			//end of step 1

			//Step 3 insert into bbids_vendororderproductsrel
			$vendorOrderProductRel = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendororderproductsrel')->findBy(array('vendorid'=>$vendorid,'categoryid'=>$categoryid));
			// echo '<pre/>';print_r($vendorOrderProductRel);exit;
			if(empty($vendorOrderProductRel))
			{
				$newVendorOrderProductRel = new Vendororderproductsrel();
				$newVendorOrderProductRel->setVendorid($vendorid);
				$newVendorOrderProductRel->setCategoryid($categoryid);
				$newVendorOrderProductRel->setLeadpack($pack);

				$em->persist($newVendorOrderProductRel);
				$em->flush();
			} else {
				foreach ($vendorOrderProductRel as $vendorCatOrdersValue) {
					$lastOrderPackValue = $vendorCatOrdersValue->getLeadpack();
					$newOrderPackValue  = $lastOrderPackValue + $pack;
					$vendorCatOrdersValue->setLeadpack($newOrderPackValue);
					$this->getDoctrine()->getManager()->flush();
				}
			}

			//end of step 3

			$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid, 'categoryid'=>$categoryid));

			foreach($categoriesArray as $c){
				$leadpack = $c->getLeadpack();
				$id       = $c->getId();
			}

			if(isset($leadpack)){
				$em    = $this->getDoctrine()->getManager();
				$lead  = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($id);
				$leadp = $leadpack + $pack;

				$lead->setLeadpack($leadp);
				$lead->setStatus(1);

				$em->persist($lead);
				$em->flush();

			} else {
				$em = $this->getDoctrine()->getManager();

				$vendorcat = new Vendorcategoriesrel();

				$vendorcat->setVendorid($vendorid);
				$vendorcat->setCategoryid($categoryid);
				$vendorcat->setLeadpack($pack);
				$vendorcat->setStatus(1);
				$vendorcat->setFlag(1);

				$em->persist($vendorcat);
				$em->flush();

			}
		}

		// Send cheaque clearence email
		$body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:cheque_clearance.html.twig', array('OrderNumber'=>$orderNumber,'name'=>$vendorcontactname));

		$this->sendEmail($vendoremail,$subject,$body,$vendorid);

		$session = $this->container->get('session');
		$session->getFlashBag()->add('message', 'Vendor Order '.$ono.' has now been approved and the leads have been credited to the respective vendor account');
		return $this->redirect($this->generateUrl('b_bids_b_bids_admin_orders', array('offset'=>1)));
	}

	public function ordersAction($offset, Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid') && $session->get('pid') == 1){
		$em = $this->getDoctrine()->getManager();
		$connection =$em->getConnection();

		$start = (($offset * 10) - 10);

		 $query = $em->createQueryBuilder()
			->select('o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack')
			->from('BBidsBBidsHomeBundle:Order', 'o')
			->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' , 'substring(o.ordernumber,3)=l.ordernumber')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with' , 'l.categoryid=c.id')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
			->add('orderBy', 'o.created DESC')
			->setFirstResult($start)
			->setMaxResults(10);

		$orders = $query->getQuery()->getResult();

//print_r($orders);exit;
		$from = $start+1;

                $to = $start + 10;

		$query = $em->createQueryBuilder()
			->select('count(o.id)')
			->from('BBidsBBidsHomeBundle:Order', 'o')
			->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' , 'substring(o.ordernumber,3)=l.ordernumber')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with' , 'l.categoryid=c.id')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author');

		$count = $query->getQuery()->getSingleScalarResult();

		$order = new Order();

		$AccountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('profileid'=>3));

		$AccountList = array();

		foreach($AccountArray as $A){
			$id = $A->getUserid();
			$Accountname2 = $A->getContactname();
			$Accountname = $A->getBizname();
			$AccountList[] = $Accountname;
		}


		$categories = new Categories();
		 $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

		$categoryList = array();

			foreach($categoriesArray as $category)
			{
				$categoryid = $category->getId();
				$categoryname = $category->getCategory();
				$categoryList[$categoryid] = $categoryname;
			}

		$form = $this->createFormBuilder()
			->add('vendorname', 'text', array('mapped'=>false,'required'=>false,'label'=>'Vendor Business Name'))
			->add('category','choice', array('label'=>'Category ','required'=>false,'choices'=>$categoryList, 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
			->add('fromdate','text',array('mapped'=>false,'label'=>'From Date','read_only' => true))
            ->add('todate','text',array('mapped'=>false,'label'=>'To Date','read_only' => true))
            ->add('filter', 'submit')
            ->getForm();

		$form->handleRequest($request);
		if($request->isMethod('POST')){
			if($form->isValid()){
				$vendorname = $form['vendorname']->getData();
				$fromdate = $form['fromdate']->getData();
				$todate = $form['todate']->getData();
				$category = $form['category']->getData();

				if($fromdate == "" && $todate != ""){
					$session = $this->container->get('session');
					$session->getFlashBag()->add('error','Please select From date');
					return $this->render('BBidsBBidsHomeBundle:Admin:orderlist.html.twig', array('accountList'=>$AccountList,'orders'=>$orders, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
				}
				else if($todate == "" && $fromdate != ""){
					$session = $this->container->get('session');
	                $session->getFlashBag()->add('error','Please select To date');
	                return $this->render('BBidsBBidsHomeBundle:Admin:orderlist.html.twig', array('accountList'=>$AccountList,'orders'=>$orders, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
				}
				else if($vendorname == "" && $fromdate == "" && $todate == "" && $category==""){
					$session = $this->container->get('session');
                    $session->getFlashBag()->add('error','Please select atleast one filter criteria');
                    return $this->render('BBidsBBidsHomeBundle:Admin:orderlist.html.twig', array('accountList'=>$AccountList,'orders'=>$orders, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));

				}
				else if($fromdate == "" && $todate == "" && $category==""){
					$query = $connection->prepare("SELECT o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack FROM bbids_order o JOIN bbids_orderproductsrel l ON substring(o.ordernumber,3)=l.ordernumber JOIN bbids_categories c ON l.categoryid=c.id JOIN bbids_account a ON a.userid=o.author WHERE a.bizname LIKE  '$vendorname' ORDER BY o.created DESC");

				}
				else if($vendorname == "" && $category==""){
					$query = $connection->prepare("SELECT o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack FROM bbids_order o JOIN bbids_orderproductsrel l ON substring(o.ordernumber,3)=l.ordernumber JOIN bbids_categories c ON l.categoryid=c.id JOIN bbids_account a ON a.userid=o.author WHERE  date(o.created) between '$fromdate' AND '$todate' ORDER BY o.created DESC");

				}
				else if($vendorname == "" && $fromdate == "" && $todate == ""){
					$query = $connection->prepare("SELECT o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack FROM bbids_order o JOIN bbids_orderproductsrel l ON substring(o.ordernumber,3)=l.ordernumber JOIN bbids_categories c ON l.categoryid=c.id JOIN bbids_account a ON a.userid=o.author WHERE  c.id = $category ORDER BY o.created DESC");

				}

				else if($vendorname == ""){
					$query = $connection->prepare("SELECT o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack FROM bbids_order o JOIN bbids_orderproductsrel l ON substring(o.ordernumber,3)=l.ordernumber JOIN bbids_categories c ON l.categoryid=c.id JOIN bbids_account a ON a.userid=o.author WHERE  date(o.created) between '$fromdate' AND '$todate' AND c.id = $category ORDER BY o.created DESC");

				}
				else if($category == ""){


					 $query = $connection->prepare("SELECT o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack FROM bbids_order o JOIN bbids_orderproductsrel l ON substring(o.ordernumber,3)=l.ordernumber JOIN bbids_categories c ON l.categoryid=c.id JOIN bbids_account a ON a.userid=o.author WHERE  date(o.created) between '$fromdate' AND '$todate'  AND a.bizname LIKE  '$vendorname' ORDER BY o.created DESC");


				}

				else {
					$fromDateSearch	=	$toDateSearch	=	'';
					if($fromdate!=''){
						$fromDateSearch	= "date(o.created) between '$fromdate'";
					}
					if($todate!=''){
						$toDateSearch	= "AND '$todate' AND";
					}

					$query = $connection->prepare("SELECT o.id, o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack FROM bbids_order o JOIN bbids_orderproductsrel l ON substring(o.ordernumber,3)=l.ordernumber JOIN bbids_categories c ON l.categoryid=c.id JOIN bbids_account a ON a.userid=o.author WHERE  ".$fromDateSearch." ".$toDateSearch."  c.id = $category AND a.bizname LIKE  '$vendorname' ORDER BY o.created DESC");

				}
					$query->execute();
                    $orders = $query->fetchAll();

				$count=count($orders);



				return $this->render('BBidsBBidsHomeBundle:Admin:orderlist.html.twig', array('accountList'=>$AccountList,'orders'=>$orders, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
			}
		}
		return $this->render('BBidsBBidsHomeBundle:Admin:orderlist.html.twig', array('accountList'=>$AccountList,'orders'=>$orders, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
	}
	else
	{
						$session = $this->container->get('session');

                        $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
	}

	}

	public function publishreviewAction($reviewid)
	{
		$em = $this->getDoctrine()->getManager();

		$review = $em->getRepository('BBidsBBidsHomeBundle:Reviews')->find($reviewid);
		//print_r($review);exit;
		$vendorid = $review->getVendorid();
		$enquiryid = $review->getEnquiryid();
		$authorid = $review->getAuthorid();

		$review->setModstatus(1);

		$query = $em->createQueryBuilder()
			->select('c.category', 'e.city')
			->from('BBidsBBidsHomeBundle:Categories','c')
			->innerJoin('BBidsBBidsHomeBundle:Enquiry', 'e', 'with', 'e.category=c.id')
			->where('e.id = :enquiryid')
			->setParameter('enquiryid', $enquiryid);

		$cats = $query->getQuery()->getArrayResult();


			//print_r($cats);exit;

		$count = count($cats);

		for($i = 0; $i < $count; $i++){
			$category = $cats[$i]['category'];
			$city = $cats[$i]['city'];
		}
		//echo $category;exit;
		if($authorid){
			$custArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$authorid));

			foreach($custArray as $c){

				$custname = $c->getContactname();
			}

			$custemail = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$authorid));

			foreach($custemail as $ce){
				$customeremail = $ce->getEmail();
			}

			$url = 'https://api.sendgrid.com/';
			$user = 'businessbid';
			$pass = '!3zxih1x0';
			$email = $customeremail;
			$subject = 'Reveiew and Ratings Updates';
			$body=$this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:publishemailcust.html.twig', array('name'=>$custname));

			$message = array(
			'api_user'  => $user,
			'api_key'   => $pass,
			'to'        => $email,
			'subject'   => $subject,
			'html'      => $body,
			'text'      => $body,
			'from'      => 'infosupport@businessbid.ae',
			 );


			$request =  $url.'api/mail.send.json';

			// Generate curl request
			$sess = curl_init($request);
			// Tell curl to use HTTP POST
			curl_setopt ($sess, CURLOPT_POST, true);
			// Tell curl that this is the body of the POST
			curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
			// Tell curl not to return headers, but do return the response
			curl_setopt($sess, CURLOPT_HEADER, false);
			curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

			// obtain response
			$response = curl_exec($sess);
			curl_close($sess);

			// Capture sent email to user
			$this->userToEmail($authorid,11,'support@businessbid.ae',$customeremail,$subject,$body);
		}

		$vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$vendorid));

		foreach($vendorArray as $v){
			$vendor = $v->getBizname();
			$name = $v->getContactname();
		}

		$vendorName = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$vendorid));

		foreach($vendorName as $n){
			$email = $n->getEmail();
		}


		$created = new \Datetime();
		$activity = new Activity();

		$activity->setCategory($category);
		$activity->setMessage($vendor);
		$activity->setCreated($created);
		$activity->setCity($city);
		$activity->setAuthorid($authorid);
		$activity->setType(2);

		$em->persist($activity);
		$em->flush();

		// Update to email to db
		$url     = 'https://api.sendgrid.com/';
		$user    = 'businessbid';
		$pass    = '!3zxih1x0';
		$email   = $email;
		$subject = 'Reveiew and Ratings Updates';
		$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:publishemail.html.twig', array('name'=>$name));

		$message = array(
			'api_user'  => $user,
			'api_key'   => $pass,
			'to'        => $email,
			'subject'   => $subject,
			'html'      => $body,
			'text'      => $body,
			'from'      => 'infosupport@businessbid.ae',
		 );


		$request =  $url.'api/mail.send.json';

		// Generate curl request
		$sess = curl_init($request);
		// Tell curl to use HTTP POST
		curl_setopt ($sess, CURLOPT_POST, true);
		// Tell curl that this is the body of the POST
		curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
		// Tell curl not to return headers, but do return the response
		curl_setopt($sess, CURLOPT_HEADER, false);
		curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

		// obtain response
		$response = curl_exec($sess);
		curl_close($sess);

		// Capture sent email to user
		$this->userToEmail($vendorid,11,'infosupport@businessbid.ae',$email,$subject,$body);

		return $this->redirect($this->generateUrl('b_bids_b_bids_review', array('reviewid'=>$reviewid)));
	}

	public function reviewAction($reviewid)
	{
		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('c.category,r.id, r.review, r.author, r.created, r.modstatus, r.rating, a.contactname, a.bizname, r.businessknow, r.businessrecomond, r.servicesummary')
			->from('BBidsBBidsHomeBundle:Reviews','r')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' ,'r.vendorid=a.userid')
			->innerJoin('BBidsBBidsHomeBundle:Enquiry','e', 'with' ,'r.enquiryid=e.id')
			->innerJoin('BBidsBBidsHomeBundle:Categories','c', 'with' ,'e.category=c.id')
			->where('r.id = :reviewid')
			->setParameter('reviewid', $reviewid);
		$review = $query->getQuery()->getResult();

		return $this->render('BBidsBBidsHomeBundle:Admin:viewreview.html.twig', array('review'=>$review));
	}

	public function reviewsAction($offset,Request $request)
	{

		$session = $this->container->get('session');

		if($session->has('uid')){

			$start = (($offset * 10) - 10);

			$em = $this->getDoctrine()->getManager();
			$connection=$em->getConnection();

			$query = $em->createQueryBuilder()
					->select('r.review, r.id, r.author, r.created, r.rating, r.modstatus, b.contactname, b.bizname,r.businessknow,r.businessrecomond,c.category')
					->from('BBidsBBidsHomeBundle:Reviews', 'r')
					->leftJoin('BBidsBBidsHomeBundle:Account','a', 'with' ,'a.userid=r.authorid')
					->leftJoin('BBidsBBidsHomeBundle:Account','b', 'with' ,'b.userid=r.vendorid')
					->innerJoin('BBidsBBidsHomeBundle:Enquiry','e', 'with' ,'r.enquiryid=e.id')
					->innerJoin('BBidsBBidsHomeBundle:Categories','c', 'with' ,'e.category=c.id')
					->add('orderBy', 'r.created DESC')
					->setFirstResult($start)
					->setMaxResults(10);

			$reviews = $query->getQuery()->getResult();

			$from = $start + 1;
			$to = $start + 10;

			$query2 = $em->createQueryBuilder()
					->select('count(r.id)')
					->from('BBidsBBidsHomeBundle:Reviews', 'r')
					->innerJoin('BBidsBBidsHomeBundle:Account','a', 'with' ,'a.userid=r.authorid')
					->innerJoin('BBidsBBidsHomeBundle:Enquiry','e', 'with' ,'r.enquiryid=e.id')
					->innerJoin('BBidsBBidsHomeBundle:Categories','c', 'with' ,'e.category=c.id');

			$count = $query2->getQuery()->getSingleScalarResult();

			if($to > $count)
				$to = $count;

			$review = new Reviews();

			$AccountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('profileid'=>3));

			$AccountList = array();

			foreach($AccountArray as $A){
				$id = $A->getUserid();
				$Accountname = $A->getbizname();
				$AccountList[] = $Accountname;
			}
			$custArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('profileid'=>2));

			$custList = array();

			foreach($custArray as $A){
				$id = $A->getUserid();
				$custname = $A->getContactname();
				$custList[] = $custname;
			}

			$categories = new Categories();
			$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

			$categoryList = array();

			foreach($categoriesArray as $category)
			{
				$categoryid = $category->getId();
				$categoryname = $category->getCategory();
				$categoryList[$categoryid] = $categoryname;
			}
			$status=2;
			$form = $this->createFormBuilder()
				->add('customername', 'text', array('mapped'=>false,'required'=>false,'label'=>'Customer Name'))
				->add('vendorname', 'text', array('mapped'=>false,'required'=>false,'label'=>'Vendor Business Name'))
				->add('category','choice', array('label'=>'Category ','required'=>false,'choices'=>$categoryList, 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))

				->add('status', 'choice' ,array('choices'=>array('0'=>'Unpublished', '1'=>'Published'), 'empty_data'=>2, 'data'=>$status, 'empty_value'=>'Select Status', 'mapped'=>false))
				->add('fromdate','text',array('mapped'=>false,'label'=>'From Date','read_only' => true))
				->add('todate','text',array('mapped'=>false,'label'=>'To Date','read_only' => true))
					->add('filter', 'submit')
				->getForm();
			// echo '<pre/>';print_r($reviews);exit;
			$form->handleRequest($request);
			if($request->isMethod('POST')) {
				if($form->isValid()) {

				$vendorname = $form['vendorname']->getData();
				$customername = $form['customername']->getData();
				$fromdate = $form['fromdate']->getData();
				$todate = $form['todate']->getData();
				$category = $form['category']->getData();
				$rstatus = $form['status']->getData();

				if($fromdate == "" && $todate == ""){
					$session = $this->container->get('session');
					$session->getFlashBag()->add('error','Please select Date parameters');
					return $this->render('BBidsBBidsHomeBundle:Admin:reviews.html.twig', array('custList'=>$custList,'accountList'=>$AccountList,'reviews'=>$reviews, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
				}
				else if($fromdate != "" && $todate == ""){
					$session = $this->container->get('session');
					$session->getFlashBag()->add('error','Please select Date parameters');
					return $this->render('BBidsBBidsHomeBundle:Admin:reviews.html.twig', array('custList'=>$custList,'accountList'=>$AccountList,'reviews'=>$reviews, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
				}
				else if($fromdate == "" && $todate != ""){
					$session = $this->container->get('session');
					$session->getFlashBag()->add('error','Please select Date parameters');
					return $this->render('BBidsBBidsHomeBundle:Admin:reviews.html.twig', array('custList'=>$custList,'accountList'=>$AccountList,'reviews'=>$reviews, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
				}
				else if($customername== '' && $vendorname =='' && $category == '')
				{
					 $query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid  JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE date(r.created) between '$fromdate' AND '$todate' AND r.modstatus=$rstatus");
				}
				else if($customername== '' && $vendorname =='')
				{
					 $query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid  JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE date(r.created) between '$fromdate' AND '$todate' AND r.modstatus=$rstatus AND c.id = $category");
				}
				else if($vendorname==''&& $category=='')
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE r.modstatus=$rstatus AND date(r.created) between '$fromdate' AND '$todate' AND r.author LIKE '$customername' ");
				}
				else if($vendorname=='')
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid  JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE r.modstatus=$rstatus AND date(r.created) between '$fromdate' AND '$todate' AND c.id = $category AND r.author LIKE '$customername' ");
				}
				else if($customername=='' && $category=='')
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE r.modstatus=$rstatus AND date(r.created) between '$fromdate' AND '$todate' AND a.bizname LIKE  '$vendorname' ");
				}
				else if($customername=='')
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE r.modstatus=$rstatus AND date(r.created) between '$fromdate' AND '$todate' AND c.id = $category AND a.bizname LIKE  '$vendorname' ");
				}
				else if($category=='')
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE r.modstatus=$rstatus AND date(r.created) between '$fromdate' AND '$todate' AND r.author LIKE '$customername' AND a.bizname LIKE  '$vendorname' ");
				}
				else if($rstatus=='')
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid JOIN bbids_enquiry e ON r.enquiryid=e.id JOIN bbids_categories c ON e.category=c.id WHERE date(r.created) between '$fromdate' AND '$todate' AND c.id = $category AND r.author LIKE '$customername' AND a.bizname LIKE  '$vendorname' ");
				}
				else
				{
					$query = $connection->prepare("SELECT * FROM bbids_reviews r JOIN bbids_account a ON  a.userid=r.vendorid JOIN bbids_categories c ON e.category=c.id JOIN bbids_enquiry e ON r.enquiryid=e.id WHERE r.modstatus=$rstatus AND date(r.created) between '$fromdate' AND '$todate' AND c.id = $category AND r.author LIKE '$customername' AND a.bizname LIKE  '$vendorname' ");
				}


				$query->execute();
				$reviews = $query->fetchAll();

				$count=count($reviews);

				if($to > $count && $count !=0) {
					$to = $count;
				}
				return $this->render('BBidsBBidsHomeBundle:Admin:reviews.html.twig', array('custList'=>$custList,'accountList'=>$AccountList,'reviews'=>$reviews, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));
				}
			}
			return $this->render('BBidsBBidsHomeBundle:Admin:reviews.html.twig', array('custList'=>$custList,'accountList'=>$AccountList,'reviews'=>$reviews, 'form'=>$form->createView(), 'from'=>$from, 'to'=>$to, 'count'=>$count));

		}
		else {
			$session = $this->container->get('session');

            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function deletekeywordAction($keyid)
	{
		$em = $this->getDoctrine()->getManager();

		$key = $em->getRepository('BBidsBBidsHomeBundle:Keyword')->find($keyid);

		$em->remove($key);

		$em->flush();

		$session = $this->container->get('session');

        $session->getFlashBag()->add('message','The record has been successfully deleted');

        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
	}

	public function editkeywordAction(Request $request, $keyid)
	{
		$parentArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Keyword')->findBy(array('id'=>$keyid));

		foreach($parentArray as $p){
			$pid = $p->getParentid();
			$keyword = $p->getKeyword();
		}

		$catArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$pid));

		foreach($catArray as $c){
			$category = $c->getCategory();
		}

		$keywords = new Keyword();

                        $form = $this->createFormBuilder($keywords)
                                ->add('category','text', array('label'=>'Parent Category', 'mapped'=>false, 'data'=>$category, 'read_only'=>TRUE))
                                ->add('keyword','text', array('label'=>'Add a Keyword', 'data'=>$keyword, 'attr'=>array('placeholder'=>'Keyword')))
                                ->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                                ->getForm();

                        $form->handleRequest($request);

                        if($request->isMethod('POST')){
                                if($form->isValid()){
                                        $key = $form['keyword']->getData();
                                        $cat = $form['category']->getData();

                                        $em = $this->getDoctrine()->getManager();

                                        $keyword = $em->getRepository('BBidsBBidsHomeBundle:Keyword')->find($keyid);

                                        $keyword->setKeyword($key);

                                        $em->flush();

                                        $session = $this->container->get('session');
                                        $session->getFlashBag()->add('message','New Keyword '.$key.' Updated  successfully!!');
                                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
                                }
				else
					return $this->render('BBidsBBidsHomeBundle:Admin:addkeyword.html.twig', array('form'=>$form->createView()));
			}
			return $this->render('BBidsBBidsHomeBundle:Admin:addkeyword.html.twig', array('form'=>$form->createView()));
	}

	public function editsubcategoryAction(Request $request, $categoryid)
	{

		$subArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));

		foreach($subArray as $s){
			$parentid = $s->getParentid();
			$subname = $s->getCategory();
			$description = $s->getDescription();
		}

		$catArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$parentid));

		foreach($catArray as $c){
			$category = $c->getCategory();

		}

		$categories = new Categories();

                $form = $this->createFormBuilder($categories)
                        ->add('category','text', array('label'=>'Parent Category', 'data'=>$category, 'read_only'=>TRUE))
	                ->add('subcategory','text', array('label'=>'Subcategory Name', 'data'=>$subname, 'mapped'=>false, 'attr'=>array('placeholder'=>'Subcategory')))
                        ->add('description', 'textarea', array('label'=>'Enter description', 'data'=>$description, 'required'=>FALSE))
                        ->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                        ->getForm();

		$form->handleRequest($request);

                if($request->isMethod('POST')){
                         if($form->isValid()){
                                        $sub = $form['subcategory']->getData();
                                        $cat = $form['category']->getData();
                                        $desc = $form['description']->getData();
                                        $em = $this->getDoctrine()->getManager();
                                        //echo $sub;//exit;

                                        $categories = $em->getRepository('BBidsBBidsHomeBundle:Categories')->find($categoryid);
										//print_r($categories);exit;
										$categories->setParentid($parentid);
                                        $categories->setCategory($sub);
                                        $categories->setStatus(1);
                                        $categories->setDescription($desc);
                                        $categories->setSegmentid(0);
                                        $categories->setPrice(0);
                                       $em->flush();

                                        $session = $this->container->get('session');
                                        $session->getFlashBag()->add('message','Subcategory '.$sub.' updated successfully!!');
                                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
                                }
                                else {
                                        return $this->render('BBidsBBidsHomeBundle:Admin:addsubcategory.html.twig', array('form'=>$form->createView()));
                                }
                        }


		return $this->render('BBidsBBidsHomeBundle:Admin:addsubcategory.html.twig', array('form'=>$form->createView()));


	}

	public function deletecategoryAction($categoryid)
	{
		$em = $this->getDoctrine()->getManager();

                $category = $em->getRepository('BBidsBBidsHomeBundle:Categories')->find($categoryid);

                $em->remove($category);

                $em->flush();

		$session = $this->container->get('session');

                $session->getFlashBag()->add('message','The record has been successfully deleted');

                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
	}

	public function enablecategoryAction($catid,$stat)
	{

		 $em = $this->getDoctrine()->getManager();

				//~ $request = $this->get('request');
				//~ $stat   = $request->get('stat');
				//~ $categoryid   = $request->get('catid');

				$category = $em->getRepository('BBidsBBidsHomeBundle:Categories')->find($catid);
				$category->setStatus($stat);
				$em->flush();


				$session = $this->container->get('session');

                $session->getFlashBag()->add('message','The Status has been successfully updated');

                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
	}

	public function editcategoryAction(Request $request, $categoryid)
	{

		$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));

		foreach($categoryArray as $c){
			$segmentid   = $c->getSegmentid();
			$category    = $c->getCategory();
			$description = $c->getDescription();
			$price       = $c->getPrice();
			$suite       = $c->getSuite();
			$status      = $c->getStatus();
		}

		$segments = array();

        $segmentArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>111111, 'parentid'=>111111));

        foreach($segmentArray as $s){
            $id = $s->getId();
            $cat = $s->getCategory();
            $segments[$id] = $cat;
        }

	    $categories = new Categories();
	    $getRegisterFee = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Registrationfeecatrel')->findOneBy(array('categoryid'=>$categoryid,'status'=>1));
	    $fee = (empty($getRegisterFee)) ? 600: $getRegisterFee->getPrice();
	    $form = $this->createFormBuilder($categories)
	            ->add('segmentid', 'choice', array('label'=> 'Segment ID','choices'=>$segments, 'data'=>$segmentid, 'empty_value'=>'Choose a segment', 'empty_data'=>null))
	            ->add('category', 'text', array('label'=> 'Enter category Name', 'data'=>$category))
	            ->add('price', 'text', array('label'=>'Enter the lead price', 'data'=>$price))
	            ->add('description', 'textarea', array('label'=>'Enter description', 'required'=>FALSE, 'data'=>$description))
	            ->add('status', 'choice', array('choices'=>array(0=>'Active', 1=>'Inactive'), 'empty_value'=>'Select status', 'empty_data'=>null, 'data'=>$status))
				->add('suite', 'choice', array('choices'=>array(1=>'General', '2'=>'Popular'), 'data'=>$suite, 'empty_value'=> 'Choose a suite', 'empty_data'=>null))
				->add('registrationfee', 'text', array('label'=> 'Enter Registration Fee', 'data'=>$fee,'mapped'=>FALSE))
	            ->add('submit','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                ->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				$segmentid   = $form['segmentid']->getData();
				$category    = $form['category']->getData();
				$status      = $form['status']->getData();
				$description = $form['description']->getData();
				$price       = $form['price']->getData();
				$suite       = $form['suite']->getData();

				$newFee = $form['registrationfee']->getData();

				$categories = new Categories();

				$em = $this->getDoctrine()->getManager();

				$categories = $em->getRepository('BBidsBBidsHomeBundle:Categories')->find($categoryid);

				$categories->setSegmentid($segmentid);
				$categories->setSegmentid($segmentid);
                $categories->setParentid(0);
                $categories->setCategory($category);
                $categories->setPrice($price);
                $categories->setDescription($description);
                $categories->setSuite($suite);
                $categories->setStatus($status);

				$em->flush();
				if(empty($getRegisterFee)) {
					$created     = new \DateTime();

					$registrationFee = new Registrationfeecatrel();

					$registrationFee->setCategoryid($categoryid);
					$registrationFee->setPrice($newFee);
					$registrationFee->setStatus(1);
					$registrationFee->setCreated($created);

					$em->persist($registrationFee);
					$em->flush();

				} else {
					$getRegisterFee->setPrice($newFee);
					$em->flush();
				}

				$session = $this->container->get('session');

                $session->getFlashBag()->add('message', 'Category '.$category.' has been updated successfully!!');

                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
			}
			else
				return $this->render('BBidsBBidsHomeBundle:Admin:addcategory.html.twig', array('form'=>$form->createView()));
		}
		return $this->render('BBidsBBidsHomeBundle:Admin:addcategory.html.twig', array('form'=>$form->createView()));
	}

	public function addcategoryAction(Request $request)
	{
		$segments = array();

		$segmentArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('segmentid'=>111111, 'parentid'=>111111));

		foreach($segmentArray as $s){
			$id = $s->getId();
			$cat = $s->getCategory();
			$segments[$id] = $cat;
		}

		$categories = new Categories();
	    $form = $this->createFormBuilder($categories)
			->add('segmentid', 'choice', array('label'=> 'Segment ID','choices'=>$segments, 'empty_value'=>'Choose a segment', 'empty_data'=>null))
			->add('category', 'text', array('label'=> 'Enter category Name'))
			->add('price', 'text', array('label'=>'Enter the lead price'))
			->add('description', 'textarea', array('label'=>'Enter description', 'required'=>FALSE))
			->add('status', 'choice', array('choices'=>array(0=>'Active', 1=>'Inactive'), 'empty_value'=>'Seletec status', 'empty_data'=>null))
			->add('submit','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->add('suite','choice', array('choices'=>array(1=>'General', 2=>'Popular'), 'empty_value'=>'Select a suite', 'empty_data'=>null))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			if($form->isValid()){
				$price = $form['price']->getData();

				if(!is_numeric($price)){
					$session = $this->container->get('session');

					$session->getFlashBag()->add('error','Price field cannot contain characters');

					return $this->render('BBidsBBidsHomeBundle:Admin:addcategory.html.twig', array('form'=>$form->createView()));
				}

				$segmentid = $form['segmentid']->getData();
				$category = $form['category']->getData();
				$status = $form['status']->getData();
				$description = $form['description']->getData();
				$price = $form['price']->getData();

				$categories = new Categories();

				$categories->setSegmentid($segmentid);
				$categories->setParentid(0);
				$categories->setCategory($category);
				$categories->setPrice($price);
				$categories->setDescription($description);
				$categories->setSuite(1);
				$categories->setStatus($status);

				$em = $this->getDoctrine()->getManager();
				$em->persist($categories);
				$em->flush();

				$session = $this->container->get('session');

				$session->getFlashBag()->add('message', 'Category '.$category.' has been added successfully!!');

				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
			}
		}

		return $this->render('BBidsBBidsHomeBundle:Admin:addcategory.html.twig', array('form'=>$form->createView()));
	}

	public function keywordsAction($categoryid)
	{
		$session = $this->container->get('session');

        if($session->has('uid') && $session->get('pid') == 1){
            $keywords = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Keyword')->findBy(array('parentid'=>$categoryid));

            return $this->render('BBidsBBidsHomeBundle:Admin:keywords.html.twig', array('keywords'=>$keywords, 'categoryid'=>$categoryid));
        }
        else {
            $session = $this->container->get('session');

            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }

	}

	public function subcategoriesAction($categoryid)
	{
		$session = $this->container->get('session');

                if($session->has('uid') && $session->get('pid') == 1){
			$subcategories = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));

			return $this->render('BBidsBBidsHomeBundle:Admin:subcategories.html.twig', array('subcategories'=>$subcategories, 'categoryid'=>$categoryid));
		}
		else {
                        $session = $this->container->get('session');

                        $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
                }


	}

	public function addkeywordAction(Request $request, $categoryid)
	{
		$session = $this->container->get('session');

                if($session->has('uid') && $session->get('pid') == 1){

                        $categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));

                        foreach($categoryArray as $c){
                                $category = $c->getCategory();
                        }

			$keywords = new Keyword();

			$form = $this->createFormBuilder($keywords)
                                ->add('category','text', array('label'=>'Parent Category', 'mapped'=>false, 'data'=>$category, 'read_only'=>TRUE))
                                ->add('keyword','textarea', array('label'=>'Add a Keyword', 'attr'=>array('placeholder'=>'Keyword')))
                                ->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                                ->getForm();

			$form->handleRequest($request);

                        if($request->isMethod('POST')){
                                if($form->isValid()){
                                        $key = $form['keyword']->getData();
                                        $cat = $form['category']->getData();
                                        $keyArray = @explode(",", $key);

                                        $em = $this->getDoctrine()->getManager();

                                         foreach ($keyArray as $keyValue) {
											if($keyValue != '') {
												$keyword = new Keyword();
												$keyword->setParentid($categoryid);
												$keyword->setKeyword($keyValue);

												$em->persist($keyword);
											}
                                        }
                                        $em->flush();

                                        $session = $this->container->get('session');
                                        $session->getFlashBag()->add('message','New Keyword '.$key.' added under '.$cat.' successfully!!');
                                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
                                }
                                else {
                                        return $this->render('BBidsBBidsHomeBundle:Admin:addkeyword.html.twig', array('form'=>$form->createView()));
                                }
                        }
			return $this->render('BBidsBBidsHomeBundle:Admin:addkeyword.html.twig', array('form'=>$form->createView()));
		}
		else {
			$session = $this->container->get('session');

                        $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function addsubcategoryAction(Request $request, $categoryid)
	{
		$session = $this->container->get('session');

		if($session->has('uid') && $session->get('pid') == 1){

			$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$categoryid));

			foreach($categoryArray as $c){
				$category = $c->getCategory();
			}

			$categories = new Categories();

			$form = $this->createFormBuilder($categories)
				->add('category','text', array('label'=>'Parent Category', 'data'=>$category, 'read_only'=>TRUE))
				->add('subcategory','text', array('label'=>'Subcategory Name', 'mapped'=>false, 'attr'=>array('placeholder'=>'Subcategory')))
				->add('description', 'textarea', array('label'=>'Enter description', 'required'=>FALSE))
				->add('submit', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))

				->getForm();

			$form->handleRequest($request);

			if($request->isMethod('POST')){
			   	if($form->isValid()){
					$sub = $form['subcategory']->getData();
					$cat = $form['category']->getData();
					$desc = $form['description']->getData();
					$em = $this->getDoctrine()->getManager();

					if(strpos($sub,",")) {
						$subcategories = explode(",",$sub);
						$batchSize = 5;$i = 0;
						foreach ($subcategories as $value) {
							if($value != '')
							{
					            $categories = new Categories();

								$categories->setParentid($categoryid);
								$categories->setCategory($value);
								$categories->setStatus(1);
								$categories->setDescription($desc);
								$categories->setSegmentid(0);
								$categories->setPrice(0);

					            $em->persist($categories);
					            if (($i % $batchSize) === 0) {
					                $em->flush();
					                $em->clear(); // Detaches all objects from Doctrine!
					            }
					            $i++;
							}
		                }
					} else {
						$categories = new Categories();

						$categories->setParentid($categoryid);
						$categories->setCategory($sub);
						$categories->setStatus(1);
						$categories->setDescription($desc);
						$categories->setSegmentid(0);
						$categories->setPrice(0);
					}

					$em->persist($categories);
					$em->flush();

					$session = $this->container->get('session');
					$session->getFlashBag()->add('message','New subcategory '.$sub.' added under '.$cat.' successfully!!');
					return $this->redirect($this->generateUrl('b_bids_b_bids_admin_categories'));
				}
				else {
					return $this->render('BBidsBBidsHomeBundle:Admin:addsubcategory.html.twig', array('form'=>$form->createView()));
				}
			}
			return $this->render('BBidsBBidsHomeBundle:Admin:addsubcategory.html.twig', array('form'=>$form->createView()));
		}
		else {
			$session = $this->container->get('session');

                        $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}


	}

	public function categoriesAction()
	{
		$session = $this->container->get('session');

                if($session->has('uid') && $session->get('pid') == 1){
                        $categories  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0));

			return $this->render('BBidsBBidsHomeBundle:Admin:categorylist.html.twig', array('categories'=>$categories));
                }
                else {
                        $session = $this->container->get('session');

                        $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

                        return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
                }

	}

	public function enquirydeleteAction(Request $request)
	{

		$enquiryid=$request->get('enquiryid');
		$em = $this->getDoctrine()->getManager();

		$enquiry = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);

		$em->remove($enquiry);

		$em->flush();

		$vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('enquiryid'=>$enquiryid));

		foreach($vendorArray as $v){
			$id = $v->getId();

			$em = $this->getDoctrine()->getManager();

			$vendor = $em->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->find($id);

			$em->remove($vendor);

			$em->flush();
		}

		//~ $categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Enquirycategoriesrel')->findBy(array('enquiryid'=>$enquiryid));
	//~
		//~ foreach($categoryArray as $c){
			//~ $id = $c->getId();
//~
			//~ $em = $this->getDoctrine()->getManager();
//~
                        //~ $category = $em->getRepository('BBidsBBidsHomeBundle:Enquirycategoriesrel')->find($id);
//~
                        //~ $em->remove($category);
//~
                        //~ $em->flush();
		//~ }

		$subcategoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:EnquirySubcategoryRel')->findBy(array('enquiryid'=>$enquiryid));

                foreach($subcategoryArray as $c){
                        $id = $c->getId();

                        $em = $this->getDoctrine()->getManager();

                        $subcategory = $em->getRepository('BBidsBBidsHomeBundle:EnquirySubcategoryRel')->find($id);

                        $em->remove($subcategory);

                        $em->flush();
                }

		$session = $this->container->get('session');

		$session->getFlashBag()->add('message','The record has been successfully deleted');

		return $this->redirect($this->generateUrl('b_bids_b_bids_admin_enquiries', array('offset'=>1)));



	}

	public function mappedvendorsAction($enquiryid)
	{
		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select(array('e.enquiryid, e.vendorid, e.acceptstatus, u.email, a.contactname, a.bizname,a.smsphone,m.status as softstatus'))
			->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
			->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'e.vendorid=u.id')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'a.userid=e.vendorid')
			->leftJoin('BBidsBBidsHomeBundle:Vendorcategorymapping','m','WITH','m.vendorID = e.vendorid')
			->add('where', 'e.enquiryid= :enquiryid')
			->add('groupBy', 'u.id')
			->setParameter('enquiryid', $enquiryid);

		$mapping = $query->getQuery()->getResult();

		  $query = $em->createQueryBuilder()
                        ->select('count(ve.id)')
                        ->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 've')
                        ->add('where', 've.enquiryid = :enquiryid')
                        ->andWhere('ve.acceptstatus =1')
                        ->setParameter('enquiryid', $enquiryid);

         $enquiryacceptcount = $query->getQuery()->getSingleScalarResult();

         $enquiry   = $em->getRepository('BBidsBBidsHomeBundle:Enquiry')->find($enquiryid);
         $expStatus = $enquiry->getExpstatus();
         // echo $expStatus;exit;
		return $this->render('BBidsBBidsHomeBundle:Admin:viewmappedvendors.html.twig', array('mapping'=>$mapping,'enquiryacceptcount'=>$enquiryacceptcount,'enquiryid'=>$enquiryid, 'expStatus'=>$expStatus));
	}

	public function enquiryviewAction($enquiryid)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();

        if($session->has('uid') && $session->get('pid')==1) {

			$enquiries = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Enquiry')->findBy(array('id'=>$enquiryid));

	        foreach($enquiries as $e){
		        $category = $e->getCategory();
				$authorid = $e->getAuthorid();
     		}


            $categories = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$category));

	        foreach($categories as $c){
        	    $cat = $c->getCategory();
            }

			$users = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$authorid));

			foreach($users as $u){
				$email = $u->getEmail();
			}

			$account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$authorid));
			//print_r($account);exit;
			foreach($account as $a){
				$name = $a->getContactname();
				$mobile = $a->getSmsphone();
			}

			$subCatQuery = $em->createQueryBuilder()
						->select('c.category')
						->from('BBidsBBidsHomeBundle:Categories', 'c')
						->innerJoin('BBidsBBidsHomeBundle:EnquirySubcategoryRel', 'vc', 'with',  'vc.subcategoryid = c.id')
						->where('vc.enquiryid = :enquiryid')
						->setParameter('enquiryid', $enquiryid);

			$subCategory = $subCatQuery->getQuery()->getArrayResult();

			$subCategoryString = implode(', ', array_map(function($el){ return $el['category']; }, $subCategory));

			$formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));

	        return $this->render('BBidsBBidsHomeBundle:Admin:enquiryview.html.twig', array('enquiries'=>$enquiries, 'category'=>$cat, 'email2'=>$email,'name2'=>$name,'mobile'=>$mobile,'formExtraFields'=>$formExtraFields,'subCategoryString'=>$subCategoryString));
		}
		else {
			$session = $this->container->get('session');

			$session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}

	}

	public function enquiriesAction(Request $request)
	{
		$session = $this->container->get('session');

		if($session->has('uid') && $session->get('pid')==1){

			$session = $this->container->get('session');
			$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

			$categoryList = array();
			foreach($categoriesArray as $category) {
                $categoryList[] = $category->getCategory();
            }

		 	$cityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();
			$cityList = array();
			foreach($cityArray as $city) {
	            $cityList[] = $city->getCity();
	        }
			return $this->render('BBidsBBidsHomeBundle:Admin:enquirylist.html.twig', array('categoryList'=>$categoryList,'cityList'=>$cityList));
		}
		else {
			$session = $this->container->get('session');
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function viewuserAction($userid, Request $request)
	{
		$uid = $userid;
		$session = $this->container->get('session');

                if($session->has('uid') && $session->get('pid')==1){
			//~ $users = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));
			//~ $accounts = $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));


			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
					->select('u.email, a.contactname, a.bizname,a.smsphone,a.profileid,a.address,u.created,u.updated,u.lastaccess,u.status')
					->from('BBidsBBidsHomeBundle:User', 'u')
					->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id=a.userid')
					->add('where', 'u.id= :uid')
					->setParameter('uid', $userid);

			$mapping = $query->getQuery()->getResult();

			foreach($mapping as $m)
			{
				 $profileid=$m['profileid'];
				 $name = $m['contactname'];
				 $email = $m['email'];
			}


			if($profileid==3)
			{
				$query = $em->createQueryBuilder()
					->select('c.category,a.profileid,c.id')
					->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
					->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'v.vendorid=a.userid')
					->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'v.categoryid=c.id')
					->add('where', 'v.vendorid= :uid')
					->setParameter('uid', $userid);

				$vendorArray = $query->getQuery()->getResult();
				//~ print_r($vendorArray);exit;

				foreach($vendorArray as $v)
				{
					$categoryid=$v['id'];
				 $category = $v['category'];
				}
			}

			else{
				$vendorArray='';
			}
			//($vendorArray){
			$leadcredit = new Leadcredit();
			$form = $this->createFormBuilder($leadcredit)
				->add('categoryid', 'hidden',array('mapped'=>false))
				->add('category','hidden', array('mapped'=>false))
				->add('credit', 'text', array('label'=>'Enter number of leads', 'mapped'=>false, 'attr'=>array('size'=>3, 'maxlength'=>3)))
				->add('submit', 'submit')
				->getForm();

			$form->handleRequest($request);

			if($request->isMethod('POST')){
				if($form->isValid()){
					$categoryid = $form['categoryid']->getData();
					$category = $form['category']->getData();
					$credit = $form['credit']->getData();
			//echo '<<>>'.$categoryid.'<<>>'.$category.'<<>>'.$credit;exit;
					$leadArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$userid, 'categoryid'=>$categoryid, 'status'=>1));

					foreach($leadArray as $l){
						$leadpack = $l->getLeadpack();

						$lead = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($l->getId());

						$lead->setLeadPack($leadpack + $credit);
						$lead->setStatus(1);

						$em->flush();
					}
					$leadArray2 = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendororderproductsrel')->findBy(array('vendorid'=>$userid, 'categoryid'=>$categoryid));

					foreach($leadArray2 as $ll){
						$leadpack2 = $ll->getLeadpack();

						$lead2 = $em->getRepository('BBidsBBidsHomeBundle:Vendororderproductsrel')->find($ll->getId());

						$lead2->setLeadPack($leadpack2 + $credit);


						$em->flush();
					}

					$leadcredit =  new Leadcredit();

					$leadcredit->setUserid($userid);
					$leadcredit->setCategoryid($categoryid);
					$leadcredit->setLeadcount($leadpack);
					$leadcredit->setNewleadcount($leadpack + $credit);
					$leadcredit->setLeadcredited($credit);
					$leadcredit->setCreated(new \DateTime());
					$leadcredit->setStatus(1);

					$em->persist($leadcredit);
					$em->flush();


					$newpack = $leadpack + $credit;

					/*$url     = 'https://api.sendgrid.com/';
					$user    = 'businessbid';
					$pass    = 'n9(Sg;iz{0{Qf>x';*/
					$subject = 'Free Trial Activated';

                	$body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Admin:leadcreditemail.html.twig', array('name'=>$name, 'credit'=>$credit, 'categoryname'=>$category, 'leadcount'=>$newpack));
                    /* $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                     );


                    $request =  $url.'api/mail.send.json';

                    // Generate curl request
                    $sess = curl_init($request);
                    // Tell curl to use HTTP POST
                    curl_setopt ($sess, CURLOPT_POST, true);
                    // Tell curl that this is the body of the POST
                    curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
                    // Tell curl not to return headers, but do return the response
                    curl_setopt($sess, CURLOPT_HEADER, false);
                    curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

                    // obtain response
                    $response = curl_exec($sess);
                    curl_close($sess);*/

                    // Capture sent email to user
					$this->userToEmail($uid,11,'support@businessbid.ae',$email,$subject,$body);

					$session->getFlashBag()->add('success', $credit.' leads were successfully credited towards '.$name.' for '.$category);
				}
			}

			return $this->render('BBidsBBidsHomeBundle:Admin:viewuser.html.twig', array('users'=>$mapping, 'form'=>$form->createView(), 'vendorcat'=>$vendorArray));
		}
		else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}

	}

	public function edituserAction(Request $request, $userid)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();

		if($session->has('uid') && $session->get('pid')==1){
			$uid = $userid;

			$account = new Account();

			$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));
			foreach($userArray as $u)
			{
				$email     = $u->getEmail();
				$status    = $u->getStatus();
			}

			$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

			foreach($accountArray as $a)
			{
				$profileid   = $a->getProfileid();
				$address     = $a->getAddress();
				$smsphone    = $a->getSmsphone();
				$homephone   = $a->getHomephone();
				$bizname     = $a->getBizname();
				$contactname = $a->getContactname();
				$fax         = $a->getFax();
				$logoImage   = $a->getLogopath();
				$description = $a->getDescription();
				$testAccount = $a->getTestAccount();
			}
			//echo $smsphone;exit;
			if(!empty($smsphone))
			{
				$sms      = explode('-',$smsphone);
				$smscode  = $sms[0];
				$smsphone = $sms[1];
			}
			if(!empty($homephone)){

				$home      = explode('-',$homephone);
				$homecode  = $home[0];
				$homephone = $home[1];
			} else {
				$homecode = '';
				$homephone = '';
			}

			//editing vendor profile use this forms
			if($profileid == 3) {

				$expLicense = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorlicenserel')->findBy(array('vendorid'=>$uid));
				$vendorProducts = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorproductsrel')->findBy(array('vendorid'=>$uid));
				$vendorCertifications = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcertificationrel')->findBy(array('vendorid'=>$uid));
				$vendorcityArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcityrel')->findBy(array('vendorid'=>$uid));
				$cityList 	= $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:City')->findAll();

        		$logoObject = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorlogorel')->findOneBy(array('vendorid'=>$uid));

				$vendoralbum = $em->createQueryBuilder()
					->select('a')
					->from('BBidsBBidsHomeBundle:Vendoralbumrel', 'a')
					->where('a.vendorid = :vendorid')
					->setParameter('vendorid', $uid)
					->add('groupBy', 'a.albumid');

				$vendoralbumArray = $vendoralbum->getQuery()->getResult();
                /*echo '<pre/>';print_r($vendoralbumArray);exit;
				$vendoralbumArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendoralbumrel')->findBy(array('vendorid'=>$uid));*/

				$showcat = $em->createQueryBuilder()
					->select('c.category,c.id')
					->from('BBidsBBidsHomeBundle:Vendorservices', 'c')
					->where('c.vendorid = :vendorid')
					->setParameter('vendorid', $uid);

				$dispcat = $showcat->getQuery()->getArrayResult();

				$cityArray = array();
				foreach($cityList as $c){
					$city   = $c->getCity();
					$cityid = $c->getId();
					if(!empty($city))
						$cityArray[$cityid] = $city;
				}
				$cities2 = array();
				foreach($vendorcityArray as $vc){
					$city2 = $vc->getCity();
				 	$cities2[$city2] = $city2;
				}

				$logoform = $this->createFormBuilder()
				            ->setAction($this->generateUrl('b_bids_b_bids_vendor_save_logo'))
							->add('vendor','hidden',array('data'=>$uid))
				            ->add('image', 'file', array('mapped'=>false, 'required'=>false))
				            ->add('Upload','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
				            ->getForm();

	            $licenseform = $this->createFormBuilder()
				            ->setAction($this->generateUrl('b_bids_b_bids_vendorlicense'))
				            ->add('vendor','hidden',array('data'=>$uid))
				            ->add('expirydate','text', array('label' =>'Expiry Date','mapped'=>false))
				            ->add('licensefile','file', array('label' =>'License File','mapped'=>false))
				            ->add('save', 'submit')
				            ->getForm();

				$cityform = $this->createFormBuilder()
							->setAction($this->generateUrl('b_bids_b_bids_vendor_save_city'))
				            ->add('vendor','hidden',array('data'=>$uid))
							->add('city','choice', array('choices'=>$cityArray, 'multiple'=>true, 'expanded'=>true, 'data'=>$cities2,'mapped'=>false, 'required'=>false))
							->add('save', 'submit')
							->getForm();

				$productform = $this->createFormBuilder()
							->setAction($this->generateUrl('b_bids_b_bids_vendor_save_product'))
				            ->add('vendor','hidden',array('data'=>$uid))
							->add('product','text', array('mapped'=>false, 'required'=>false))
							->add('save', 'submit')
							->getForm();

				$albumform = $this->createFormBuilder()
							->setAction($this->generateUrl('b_bids_b_bids_vendor_save_album'))
				            ->add('vendor','hidden',array('data'=>$uid))
							->add('albumname','text',array('label'=>'Album Name','mapped'=>false))
							// ->add('description', 'textarea', array('required'=>false ,'attr'=>array('placeholder'=>'Enter album description')))
							->add('albumimages','file',array('label'=>'','mapped'=>false,'attr'=>array("multiple" => "multiple")))
							->add('save', 'submit')
							->getForm();
			}

			$form = $this->createFormBuilder()
				->add('address', 'textarea', array('label'=>'Address', 'required'=> false, 'data'=>$address))
				->add('updateemailid', 'text', array('label'=>'Update Email ID', 'required'=> false))
				->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'data'=>$smscode,'label'=>' Enter Contact Number','empty_value'=>'Area Code', 'mapped'=>FALSE))
			    ->add('smsphone','text', array('data'=>$smsphone))
			     ->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'data'=>$homecode,'label'=>'Business Number','empty_value'=>'Area Code', 'mapped'=>FALSE, 'required'=> false))
			    ->add('homephone', 'text', array('data'=>$homephone, 'required'=>false))
			    ->add('bizname', 'text', array('label'=>'Business name', 'data'=>$bizname, 'required'=>false))
			 	->add('contactname', 'text', array('label'=>'Contact name', 'data'=>$contactname, 'required'=>false))
				->add('fax', 'text', array('label'=>'Fax Phone', 'required'=>false, 'data'=>$fax,'attr'=>array('size'=>10)))
				->add('status','choice', array('label'=>'Status', 'data'=>$status, 'mapped'=>false, 'choices'=>array(1=>'Active', 0=>'Inactive', 'required'=>false)))
				->add('update', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
				->add('description', 'textarea', array('data'=>$description,'required'=>false ,'attr'=>array('placeholder'=>'Enter your description')))
				->getForm();

				$form->handleRequest($request);

				if($request->isMethod('POST')){
					if($form->isValid()){

						$mobilecode  = $form['mobilecode']->getData();
						$smsphone    = $form['smsphone']->getData();
						$homecode    = $form['homecode']->getData();
						$homephone   = $form['homephone']->getData();
						$fax         = $form['fax']->getData();
						$status      = $form['status']->getData();
						$address     = $form['address']->getData();
						$bizname     = $form['bizname']->getData();
						$contactname = $form['contactname']->getData();
						$emailupdate = $form['updateemailid']->getData();
						$newDescription = $form['description']->getData();



						$em = $this->getDoctrine()->getManager();

						if(!is_numeric($smsphone)){

							$session = $this->container->get('session');

							$session->getFlashbag()->add('error','Mobile number should not contain strings');

							return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('editemail'=>$email,'form'=>$form->createView()));
						}

						if(strlen($smsphone) != 7)
						{

							$session = $this->container->get('session');
							$session->getFlashBag()->add('error','Mobile number field should be of 7 digits');
							return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('editemail'=>$email,'form'=>$form->createView()));
                        }
						if(isset($homephone) && $homephone!="" && !is_numeric($homephone)){


							$session = $this->container->get('session');

                            $session->getFlashbag()->add('error','Home phone should not contain strings');

                            return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('editemail'=>$email,'form'=>$form->createView()));

						}

						if(isset($homephone) && ($homephone != "" || $homecode != "")) {
							if(strlen($homephone) != 7) {

								$session = $this->container->get('session');
								$session->getFlashbag()->add('error','Home phone should contain 7 digits');
                                return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('editemail'=>$email,'form'=>$form->createView()));

                            }
						}
						if($fax!="" && !is_numeric($fax)){
							$session = $this->container->get('session');

                            $session->getFlashbag()->add('error','Fax number should not contain strings');

                            return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('editemail'=>$email,'form'=>$form->createView()));
						}



						$userArray4  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$emailupdate));

						foreach($userArray4 as $uses){
							  $uids = $uses->getId();
						}

						if(isset($uids)){
							$session = $this->container->get('session');
							$session->getFlashbag()->add('error','Email id already exists. Please try a different email');

                            return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('editemail'=>$email,'form'=>$form->createView()));
						}



						if($emailupdate)
						{
							$status=1;
							$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

							$user->setStatus($status);
							$user->setEmail($emailupdate);
						    $em->flush();

						    $url = 'https://api.sendgrid.com/';
							$user = 'businessbid';
							$pass = '!3zxih1x0';
							$subject = 'Welcome to the BusinessBid Network';

							$body = $this->renderView('BBidsBBidsHomeBundle:Admin:emailidupdation.html.twig', array('userid'=>$uid, 'name'=>$contactname));

							$message = array(
								'api_user'  => $user,
								'api_key'   => $pass,
								'to'        => $email,
								'subject'   => $subject,
								'html'      => $body,
								'text'      => $body,
								'from'      => 'infosupport@businessbid.ae',
							 );


							$request =  $url.'api/mail.send.json';

							// Generate curl request
							$sess = curl_init($request);
							// Tell curl to use HTTP POST
							curl_setopt ($sess, CURLOPT_POST, true);
							// Tell curl that this is the body of the POST
							curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
							// Tell curl not to return headers, but do return the response
							curl_setopt($sess, CURLOPT_HEADER,false);
							curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

							// obtain response
							$response = curl_exec($sess);
							curl_close($sess);

		                    // Capture sent email to user
							$this->userToEmail($uid,11,'support@businessbid.ae',$email,$subject,$body);
						}

						else
						{
							$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);
					        $user->setStatus($status);
							$em->flush();
						}

						$smsphone2 = $mobilecode.'-'.$smsphone;
						$homephone2 = $homecode.'-'.$homephone;

						$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
						foreach($accountArray as $a)
						{
							$accountid = $a->getId();
						}

						$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->find($accountid);

						$account->setSmsphone($smsphone2);
						$account->setAddress($address);
						$account->setContactname($contactname);
						$account->setHomephone($homephone2);
						$account->setBizname($bizname);
						$account->setFax($fax);
						$account->setdescription($newDescription);

						$em->flush();

						$session = $this->container->get('session');

						$session->getFlashBag()->add('message','The record has been updated successfully!!');

						return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$uid)));


					}
				}
				if($profileid == 3) {
					return $this->render('BBidsBBidsHomeBundle:Admin:vendor_profile_edit.html.twig', array('form'=>$form->createView(),'logoform'=>$logoform->createView(), 'licenseform'=>$licenseform->createView(), 'cityform'=>$cityform->createView(), 'productform'=>$productform->createView(), 'albumform'=>$albumform->createView(),'editemail'=>$email, 'expLicense'=>$expLicense, 'products'=>$vendorProducts, 'certifications'=>$vendorCertifications, 'logoImage'=>$logoImage, 'vendorid'=>$userid, 'albums'=>$vendoralbumArray,'displaycat'=>$dispcat,'testAccount'=>$testAccount,'logoObject'=>$logoObject));
				} else {
					return $this->render('BBidsBBidsHomeBundle:Admin:useredit.html.twig', array('form'=>$form->createView(), 'editemail'=>$email));
				}
		}
		else{
			$session = $this->container->get('session');

			$session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}


	public function deleteuserAction(Request $request)
	{

		$uid = $request->get('userid');

		$em = $this->getDoctrine()->getManager();

		$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

		$em->remove($user);

		$em->flush();

		$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

		foreach($accountArray as $a){
			$accountid = $a->getId();
		}

		$em = $this->getDoctrine()->getManager();

		$account = $em->getRepository('BBidsBBidsHomeBundle:account')->find($accountid);

		$em->remove($account);

		$em->flush();

		$session = $this->container->get('session');

		$session->getFlashBag()->add('message', 'The user record has been deleted successfully!!');

		return $this->redirect($this->generateUrl('b_bids_b_bids_admin_users'));
	}

	public function indexAction()
	{
		$session = $this->container->get('session');

		if($session->has('uid')){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_dashboard'));
		}
		else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	function usersAction($offset, Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){
		$start = (($offset * 10) - 10);

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('u.id, u.email, u.created, u.updated, u.lastaccess, u.status, a.profileid, a.contactname,a.smsphone,u.logincount')
			->from('BBidsBBidsHomeBundle:User', 'u')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id = a.userid')
			->add('where','a.profileid !=1')
			->add('orderBy', 'u.id DESC')
			->setFirstResult($start)
             ->setMaxResults(10);

		$users = $query->getQuery()->getResult();

		$from = $start+1;

		$to = $start + 10;

		$query = $em->createQueryBuilder()
				->select('count(u.id)')
				->from('BBidsBBidsHomeBundle:User', 'u')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id = a.userid')
				->add('where','a.profileid !=1');

		$count = $query->getQuery()->getSingleScalarResult();

		if($to > $count)
			$to = $count;

		$user = new User();

		$Account = new Account();
		$q1 = $em->createQueryBuilder()
				->select('a.contactname')
				->from('BBidsBBidsHomeBundle:Account','a');
		$res = $q1->getQuery()->getResult();
		//print_r($res);exit;


		$form = $this->createFormBuilder()
			->add('name','choice', array('required'=>false,'choices'=>array(''=>'All' ,'2'=>'Customer','3'=>'Vendor'), 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
			->add('logincount','choice', array('label'=>'Login Count','required'=>false,'choices'=>array(''=>'All' ,'Zero'=>'Zero'), 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
			//~ ->add('date','text',array('mapped'=>false, 'required'=>false))
			->add('fromdate','text',array('label'=>'From Date','attr'=>array('autocomplete'=>'off')))
            ->add('todate','text',array('label'=>'To Date','attr'=>array('autocomplete'=>'off')))
			->add('filter', 'submit', array('attr'=>array('class'=>'btn btn-success')))
			->getForm();
		$response = $this->render('BBidsBBidsHomeBundle:Admin:userlist.html.twig', array('users'=>$users, 'from'=>$from, 'form'=>$form->createView(), 'to'=>$to, 'count'=>$count));
		$form->handleRequest($request);
		if($request->isMethod('POST')){
			if($form->isValid()){
				$profile    = $form['name']->getData();
				$fromdate   = $form['fromdate']->getData();
				$todate     = $form['todate']->getData();
				$logincount = $form['logincount']->getData();

				echo '<<>>'.$profile.'<<>>'.$fromdate.'<<>>'.$todate.'<< logincount >>'.$logincount;

				$em         = $this->getDoctrine()->getManager();
				$connection = $em->getConnection();
				if($fromdate == "" && $todate == ""){
					$session->getFlashBag()->add('error', 'Please choose atleast select dates.');
					return $response;
				}
				else if($fromdate == "" && $todate != "") {
					$session->getFlashBag()->add('error','Please select From date');
					return $response;
				}
				else if($todate == "" && $fromdate != "") {
                    $session->getFlashBag()->add('error','Please select To date');
                    return $response;
				}
				else {
					$sql = "SELECT u.id, u.email, u.created, u.updated, u.lastaccess, u.status, a.profileid, a.contactname,a.smsphone,u.logincount FROM bbids_user u JOIN bbids_account a ON u.id=a.userid  WHERE ";
					if(empty($logincount) && empty($profile)) {
						$query = $connection->prepare($sql."a.profileid !=1 AND date(u.created) BETWEEN '$fromdate' AND '$todate' ORDER BY u.id DESC");
	                    $query->execute();
					} else if($logincount == 'Zero' && $profile != "") {
						$query = $connection->prepare($sql."a.profileid !='$profile' AND u.logincount IS NULL AND date(u.created) BETWEEN '$fromdate' AND '$todate' ORDER BY u.id DESC");
	                    $query->execute();
					} else if($logincount == 'Zero' && $profile == "") {
						$query = $connection->prepare($sql."a.profileid !=1 AND u.logincount IS NULL AND date(u.created) BETWEEN '$fromdate' AND '$todate' ORDER BY u.id DESC");
	                    $query->execute();
					}
					else if($profile=="") {
						$query = $connection->prepare($sql."a.profileid !=1 AND date(u.created) BETWEEN '$fromdate' AND '$todate' ORDER BY u.id DESC");
	                    $query->execute();
					}
					else if($fromdate == "" && $todate == "") {
						$query = $connection->prepare($sql."a.profileid ='$profile'  ORDER BY u.id DESC");
						$query->execute();
					}
					else if($profile !="" && $fromdate == "" && $todate == "") {
						$query = $connection->prepare($sql."a.profileid ='$profile'  ORDER BY u.id DESC");
						$query->execute();
					}
					else {
						$query = $connection->prepare($sql."a.profileid ='$profile' AND date(u.created) between '$fromdate' AND '$todate' ORDER BY u.id DESC");
						$query->execute();
					}
				}

				$users = $query->fetchAll();

				return $this->render('BBidsBBidsHomeBundle:Admin:userlist.html.twig', array('users'=>$users, 'from'=>$from, 'form'=>$form->createView(), 'to'=>$to, 'count'=>$count));

			}
		}
		return $response;
		}


		else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function usersDatatableAction($offset, Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid')){

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQueryBuilder()
				->select('u.id, u.email, u.created, u.updated, u.lastaccess, u.status, a.profileid, a.contactname,a.smsphone')
				->from('BBidsBBidsHomeBundle:User', 'u')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id = a.userid')
				->add('where','a.profileid !=1')
				->add('orderBy', 'u.id DESC');

			$users = $query->getQuery()->getResult();
			return $this->render('BBidsBBidsHomeBundle:Admin:userlist_datatable.html.twig', array('users'=>$users));
		} else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function freetrailAction($offset, Request $request)
	{
		$session = $this->container->get('session');
		if($session->has('uid'))
		{


		$start = (($offset * 10) - 10);

		$em = $this->getDoctrine()->getManager();

		$query = $em->createQueryBuilder()
			->select('f.id,f.userid,f.orderno,f.duration,f.leads,f.category, u.email, f.created,a.bizname, f.updated, f.status,u.id,c.category,a.contactname')
			->from('BBidsBBidsHomeBundle:Freetrail', 'f')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'f.category = c.id')
			->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'u.id = f.userid')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'a.userid = f.userid')
			->add('orderBy', 'f.id DESC')
			->setFirstResult($start)
            ->setMaxResults(10);



		 $users = $query->getQuery()->getResult();

		$countdays = count($users);

                for($i = 0; $i<$countdays; $i++){
                $duration=$users[$i]['duration'];
                $updateddate=$users[$i]['updated'];
                 if($updateddate != ''){
                $newdate = date_format($updateddate,'Y-m-d');
					$date = strtotime(date("Y-m-d", strtotime($newdate)) . " +".$duration."days");
                $expirydate= date("Y-m-d",$date);
                $users[$i]['daysdif'] = $expirydate;
                 }
                 else{
                $users[$i]['daysdif'] = '';
                    }
                 }

		$from = $start+1;

		$to = $start + 10;


		$query = $em->createQueryBuilder()
			->select('count(f.id)')
			->from('BBidsBBidsHomeBundle:Freetrail', 'f')
			->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'f.category = c.id')
			->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'u.id = f.userid')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'a.userid = f.userid');

		$count = $query->getQuery()->getSingleScalarResult();



			$AccountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('profileid'=>3));

		$AccountList = array();

		foreach($AccountArray as $A){
			$id = $A->getUserid();
			$Accountname2 = $A->getContactname();
			$Accountname = $A->getbizname();
			$AccountList[] = $Accountname;
		}


		$categories = new Categories();
		 $categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0));

		$categoryList = array();

			foreach($categoriesArray as $category)
			{
				$categoryid = $category->getId();
				$categoryname = $category->getCategory();
				$categoryList[$categoryid] = $categoryname;
			}

		$user = new User();

		$form = $this->createFormBuilder()

			->add('vendorname', 'text', array('mapped'=>false,'required'=>false,'label'=>'Vendor Business Name'))
			->add('category','choice', array('label'=>'Category ','required'=>false,'choices'=>$categoryList, 'empty_value'=>'All', 'empty_data'=>null,'multiple'=>FALSE,'expanded'=>FALSE))
			->add('fromdate','text',array('mapped'=>false,'label'=>'From Date','read_only' => true))
			->add('todate','text',array('mapped'=>false ,'label'=>'To Date','read_only' => true))
			->add('filter', 'submit', array('attr'=>array('class'=>'btn btn-success')))
			->getForm();

		$form->handleRequest($request);
		if($request->isMethod('POST')){
			if($form->isValid()){
				 $vendorname = $form['vendorname']->getData();
				 $category = $form['category']->getData();
				 $fromdate = $form['fromdate']->getData();
				 $todate = $form['todate']->getData();

				$em = $this->getDoctrine()->getManager();
				$connection=$em->getConnection();


				if($fromdate == "" && $todate == ""){
					$session = $this->container->get('session');

					$session->getFlashBag()->add('error', 'Please choose Date parameters');

					return $this->render('BBidsBBidsHomeBundle:Admin:freetrail.html.twig', array('accountList'=>$AccountList,'users'=>$users, 'from'=>$from, 'form'=>$form->createView(), 'to'=>$to, 'count'=>$count));

				}

				if($vendorname=="" && $category == ""){

					  $query = $connection->prepare("SELECT * FROM bbids_freetrail f JOIN bbids_categories c ON f.category = c.id JOIN bbids_user u ON u.id = f.userid JOIN bbids_account a ON a.userid = f.userid WHERE  date(f.created) between '$fromdate' AND '$todate' ");
                       $query->execute();

				}
				else if($vendorname==""){

					 $query = $connection->prepare("SELECT * FROM bbids_freetrail f JOIN bbids_categories c ON f.category = c.id JOIN bbids_user u ON u.id = f.userid JOIN bbids_account a ON a.userid = f.userid WHERE  date(f.created) between '$fromdate' AND '$todate' AND f.category = $category ");
					  $query->execute();
				}
				else if($category == ""){

                   $query = $connection->prepare("SELECT * FROM bbids_freetrail f JOIN bbids_categories c ON f.category = c.id JOIN bbids_user u ON u.id = f.userid JOIN bbids_account a ON a.userid = f.userid WHERE  date(f.created) between '$fromdate' AND '$todate' AND a.bizname LIKE '$vendorname' ");
				   $query->execute();
				}



				else {

						  $query = $connection->prepare("SELECT * FROM bbids_freetrail f JOIN bbids_categories c ON f.category = c.id JOIN bbids_user u ON u.id = f.userid JOIN bbids_account a ON a.userid = f.userid WHERE  date(f.created) between '$fromdate' AND '$todate' AND a.bizname LIKE '$vendorname' AND f.category = $category");
						  $query->execute();

				}

				$users = $query->fetchAll();;


				 $countdays = count($users);
                for($i = 0; $i<$countdays; $i++){
                $duration=$users[$i]['duration'];
                $updateddate=$users[$i]['updated'];
                 if($updateddate != ''){

                //~ echo $newdate = date_format($updateddate,'Y-m-d');exit;
				 $date = strtotime(date("Y-m-d", strtotime($updateddate)) . " +".$duration."days");
                $expirydate= date("Y-m-d",$date);
                $users[$i]['daysdif'] = $expirydate;
                 }
                 else{
                $users[$i]['daysdif'] = '';
                    }
                 }



				return $this->render('BBidsBBidsHomeBundle:Admin:freetrail.html.twig', array('accountList'=>$AccountList,'users'=>$users, 'from'=>$from, 'form'=>$form->createView(), 'to'=>$to, 'count'=>$count));

			}
		}


		if($to > $count)
		$to=$count;
		return $this->render('BBidsBBidsHomeBundle:Admin:freetrail.html.twig', array('accountList'=>$AccountList,'users'=>$users, 'from'=>$from, 'form'=>$form->createView(), 'to'=>$to, 'count'=>$count));

	}
		else {
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}


	public function creditfreetrailAction(Request $request, $userid)
	{
		$session = $this->container->get('session');

		if($session->has('uid') && $session->get('pid')==1){
			$uid = $userid;




			$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));
			foreach($userArray as $u){
				$email  = $u->getEmail();

			}

			$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
			//echo $uid;print_r($accountArray);exit;
			foreach($accountArray as $a){
				$profileid = $a->getProfileid();
				$contactname = $a->getContactname();

			}
			//echo $contactname;exit;

			 $freetrailArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Freetrail')->findBy(array('userid'=>$uid));
			foreach($freetrailArray as $f)
			{
				$catid = $f->getCategory();
				$leads = $f->getLeads();
				$status = $f->getStatus();
				$orderno = $f->getOrderno();
			}
			//print_r($freetrailArray);exit;

			$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('id'=>$catid));
			foreach($categoriesArray as $c)
			{
				$categoryname = $c->getCategory();

			}

			$freetrail = new Freetrail();

			$form = $this->createFormBuilder($freetrail)

				->add('leads', 'text', array('label'=>'Lead Pack', 'attr'=>array('placeholder'=>'Please enter number of leads')))
				->add('duration','choice', array('label'=>'Duration', 'data'=>$status, 'mapped'=>false, 'choices'=>array(15=>'15 days', 30=>'1 month',30=>'1 month',60=>'2 months',90=>'3 months',120=>'4 months',150=>'5 months',180=>'6 months',210=>'7 months',240=>'8 months',270=>'9 months',300=>'10 months',330=>'11 months',365=>'1 Year'), 'empty_data'=>NULL, 'empty_value'=>'Please select the free duration'))

				->add('update', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
				->getForm();

				$form->handleRequest($request);

				if($request->isMethod('POST')){
					if($form->isValid()){

						$creditleads = $form['leads']->getData();
						$creditduration = $form['duration']->getData();
						$freetrailupdateArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Freetrail')->findBy(array('userid'=>$uid));
						foreach($freetrailupdateArray as $fupdate){
							$freetrailid = $fupdate->getId();
							$freetrailleads = $fupdate->getLeads();
						}

						//~ echo $freetrailid;exit;
						$updated = new  \Datetime();

						if($freetrailleads=='' ||$freetrailleads==0)
						{

					  $freetrailupdate = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Freetrail')->find($freetrailid);

						$freetrailupdate->setStatus(1);
						$freetrailupdate->setDuration($creditduration);
						$freetrailupdate->setLeads($creditleads);
                        $freetrailupdate->setUpdated($updated);


						$this->getDoctrine()->getManager()->flush();

					}

						$em = $this->getDoctrine()->getManager();

						$vendorcatArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$uid, 'flag'=>2));
						foreach($vendorcatArray as $vc){
							$vid = $vc->getId();

						}

						$vendorcat = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->find($vid);

						$vendorcat->setLeadpack($creditleads);

						$em->flush();
						$session = $this->container->get('session');

						$session->getFlashBag()->add('message','The record has been updated successfully!!');

						$em = $this->getDoctrine()->getManager();
						$query = $em->createQueryBuilder()
							->select('f.id,f.userid,f.orderno,f.category, u.email,a.contactname,f.leads,f.duration,f.created,u.id,c.category')
							->from('BBidsBBidsHomeBundle:Freetrail', 'f')
							->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'u.id = f.userid')
							->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'a.userid = f.userid')
							->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'c.id = f.category')
							->add('where','f.userid = :uid')
							->setParameter('uid', $uid);


							$usersemail = $query->getQuery()->getArrayResult();
								//print_r($usersemail);exit;
							$url = 'https://api.sendgrid.com/';
							$user = 'businessbid';
							$pass = '!3zxih1x0';
							$subject = 'Free Trial Activated';
							$body=$this->renderView('BBidsBBidsHomeBundle:Admin:freetrailemail.html.twig', array('users'=>$usersemail));
							$message = array(
							'api_user'  => $user,
							'api_key'   => $pass,
							'to'        => $email,
							'subject'   => $subject,
							'html'      => $body,
							'text'      => $body,
							'from'      => 'infosupport@businessbid.ae',
							 );


							$request =  $url.'api/mail.send.json';

							// Generate curl request
							$sess = curl_init($request);
							// Tell curl to use HTTP POST
							curl_setopt ($sess, CURLOPT_POST, true);
							// Tell curl that this is the body of the POST
							curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
							// Tell curl not to return headers, but do return the response
							curl_setopt($sess, CURLOPT_HEADER, false);
							curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

							// obtain response
							$response = curl_exec($sess);
							curl_close($sess);

							// Capture sent email to user
							$this->userToEmail($uid,11,'infosupport@businessbid.ae',$email,$subject,$body);

							return $this->redirect($this->generateUrl('b_bids_b_bids_freetrails', array('offset'=>1)));
					}
				}

			return $this->render('BBidsBBidsHomeBundle:Admin:freetrailleads.html.twig', array('form'=>$form->createView(), 'editemail'=>$orderno, 'categoryname'=>$categoryname, 'vendorname'=>$contactname));
		}
		else{
			$session = $this->container->get('session');

			$session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	public function homeAction()
	{
		$session = $this->container->get('session');

                if($session->has('uid')){
			if($session->get('pid')!=1){
				$session->getFlashBag()->add('error','Your are not authorised to access this area');

				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
			}

			$year = date('Y');

			$em = $this->getDoctrine()->getmanager();

			$query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
				->add('where', 'e.created >= CURRENT_DATE()');

			$totalenquirycount = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e');

			$totalenquirycount2 = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(o.id)')
				->from('BBidsBBidsHomeBundle:Order','o')
				->add('where', 'o.created >= CURRENT_DATE()');

			$totalordercount = $query->getQuery()->getSingleScalarResult();

			$query = $em->createQueryBuilder()
				->select('count(u.id)')
				->from('BBidsBBidsHomeBundle:User', 'u')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id=a.userid')
				->add('where' , 'a.profileid=3');


			$totalusercountforapproval = $query->getQuery()->getSingleScalarResult();
			$query = $em->createQueryBuilder()
				->select('count(u.id)')
				->from('BBidsBBidsHomeBundle:User', 'u')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id=a.userid')
				->add('where' , 'a.profileid=2');


			$totalcustcount = $query->getQuery()->getSingleScalarResult();

			 $query = $em->createQueryBuilder()
				->select('count(e.id)')
				->from('BBidsBBidsHomeBundle:Vendorenquiryrel', 'e')
				->add('where' , 'e.acceptstatus is Null ');

			$totalenquiryawaitingcount = $query->getQuery()->getSingleScalarResult();

			 $query = $em->createQueryBuilder()
				->select('count(r.id)')
				->from('BBidsBBidsHomeBundle:Reviews', 'r')
				->innerJoin('BBidsBBidsHomeBundle:Account','a', 'with' ,'a.userid=r.authorid')
				->innerJoin('BBidsBBidsHomeBundle:Enquiry','e', 'with' ,'r.enquiryid=e.id')
				->innerJoin('BBidsBBidsHomeBundle:Categories','c', 'with' ,'e.category=c.id')
				->add('where' , 'r.modstatus=0 ');

			$totalreviewsawaitingcount = $query->getQuery()->getSingleScalarResult();

			//echo $totalreviewsawaitingcount;exit;

			$query = $em->createQueryBuilder()
				->select('o.ordernumber, u.email, o.status, o.created, o.id, o.amount, o.author,a.bizname,a.contactname,c.category,l.leadpack,o.payoption')
				->from('BBidsBBidsHomeBundle:Order', 'o')
				->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'WITH', 'o.author=u.id')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id=a.userid')
				->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'WITH', 'substring(o.ordernumber,3)=l.ordernumber')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'WITH', 'l.categoryid=c.id')
				->add('orderBy', 'o.id DESC')
				->setFirstResult(0)
				->setMaxResults(9);

			$orders = $query->getQuery()->getResult();
		 //print_r($orders);exit;
			$query = $em->createQueryBuilder()
				->select('e.subj', 'e.id', 'c.category', 'e.authorid', 'u.email', 'e.created' ,'e.status,a.contactname,e.location,a.smsphone,ve.acceptstatus')
				->from('BBidsBBidsHomeBundle:Enquiry', 'e')
				->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'e.category = c.id')
				->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'with', 'e.authorid=u.id')
				->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'u.id=a.userid')
				->leftJoin('BBidsBBidsHomeBundle:Vendorenquiryrel', 've', 'with', 've.acceptstatus IS NOT NULL AND e.id=ve.enquiryid')
				->add('orderBy','e.id DESC')
				->setFirstResult(0)
				->setMaxResults(9);

			$enquiries = $query->getQuery()->getResult();
			// echo '<pre/>';print_r($enquiries);exit;

			$em = $this->getDoctrine()->getEntityManager();
			$connection = $em->getConnection();
			$statement = $connection->prepare("SELECT count(id) AS rc FROM bbids_enquiry GROUP BY DATE(created) ORDER BY rc ASC;");
			$statement->execute();
			$results = $statement->fetchAll();
			$res=array_map('current',$results);
			$c = count($res);
			for ($n=0;$n<$c;$n++) {
			  $res[$n] = (int) $res[$n];
			}


		 //~ echo '<pre/>';print_r($orders);
		//~ exit;

			//print_r($enquirygraphcount);

			/* $series = array(
			array('name'=> "Number of Enquiries: ", "data"=>$res)
				);*/

			$series = array(array('name'=> "Enquiries per Month", 'data'=>$res));
			// echo '<pre/>';print_r($res);
		 //~ echo '<pre/>';print_r($series);
			 //~ exit;
			$ob = new HighChart();
			$ob->chart->renderTo('linechart');
			$ob->title->text('Enquiries per Month');
			$ob->xAxis->title(array('text'=> "Days"));
			$ob->yAxis->title(array('text'=>'Number of enquiries'));
			$ob->series($series);


			return $this->render('BBidsBBidsHomeBundle:Admin:home.html.twig', array('totalcustcount'=>$totalcustcount,'totalenquirycount2'=>$totalenquirycount2,'totalenquirycount'=>$totalenquirycount, 'totalordercount'=>$totalordercount, 'totalusercountforapproval'=>$totalusercountforapproval,'totalenquiryawaitingcount'=>$totalenquiryawaitingcount, 'orders'=>$orders, 'enquiries'=>$enquiries, 'chart'=>$ob,'totalreviewsawaitingcount'=>$totalreviewsawaitingcount));
        }
        else {
                return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	public function loginAction(Request $request)
	{
		$USER = new User();

		$session = $this->container->get('session');

		if($session->has('uid')){
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_home'));
		}
		else{
			$form = $this->createFormBuilder($USER)
				->add('email','email', array('label'=>'Enter your email: ', 'required'=>TRUE, 'attr'=>array('size'=>20, 'placeholder'=>'Your Email Address')))
				->add('password','password', array('label'=>'Your Password: ','required'=>TRUE, 'attr'=>array('size'=>20,'placeholder'=>'Your password')))
                                ->add('Login','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))

				->getForm();
			$form->handleRequest($request);

            if($request->isMethod('POST')) {
				$email = $form['email']->getData();
				$password = md5($form['password']->getData());
				$userAuth = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('email'=>$email, 'password'=>$password));
				foreach($userAuth as $u){
					$uid = $u->getId();
				}
				//echo $uid;exit;

				if(isset($uid)){
					$userStatus = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));

					foreach($userStatus as $u){
						$status = $u->getStatus();
					}

				if($status == 1){
					$accountAuth = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

					foreach($accountAuth as $a){
						$accountid = $a->getId();
						$name = $a->getContactname();
						$pid = $a->getProfileid();
					}
					// echo $pid;exit;
					if($pid != 1){
						$session = $this->container->get('session');
						$session->getFlashBag()->add('error', '<p>Please check your login credentials and try again.</p> <p>If issue persists, speak to your administrator.</p>');
						return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
					}

	                $session->set('uid',$uid);
	                $session->set('pid',$pid);
	                $session->set('aid',$accountid);
	                $session->Set('email',$email);
	                $session->set('name',$name);

					$em = $this->getDoctrine()->getManager();
					$u = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

					$dateNow = new \DateTime();

					$u->setLastaccess($dateNow);

					$em->flush();

					return $this->redirect($this->generateUrl('b_bids_b_bids_admin_home'));
				}
				else{
					return $this->render('::error.html.twig', array('error'=>'Your account is Blocked. Please contact the system administrator'));
					}
			}
				else{
					$session = $this->container->get('session');

					$session->getFlashBag()->add('error','<p>Please check your login credentials and try again.</p> <p>If issue persists, speak to your administrator.</p>');
					return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
				}
			}
		}
		return $this->render('BBidsBBidsHomeBundle:Home:login.html.twig', array('form'=>$form->createView()));
 	}
	public function categorylistAction()
	{
		$categoryList = $this->getDoctrine()->getRepository();
	}

	public function editreviewAction(Request $request,$reviewid)
	{
		$session = $this->container->get('session');

		$reviewArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Reviews')->findBy(array('id'=>$reviewid));

			foreach($reviewArray as $a){
				$review = $a->getReview();
				$servicesummary = $a->getServicesummary();
				$serviceexperience = $a->getServiceexperience();

			}


		$form = $this->createFormBuilder()
			->add('review', 'text', array('label'=>'Review','data'=>$review))
			->add('servicesummary', 'text', array('label'=>'Service Summary','data'=>$servicesummary))
			->add('serviceexperience', 'text', array('label'=>'Service Experience','data'=>$serviceexperience))
			->add('update', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
			->getForm();

		$form->handleRequest($request);

		if($request->isMethod('POST')){
			$review = $form['review']->getData();
			$servicesummary = $form['servicesummary']->getData();
			$serviceexp = $form['serviceexperience']->getData();

			$em = $this->getDoctrine()->getManager();

			$reviews = $em->getRepository('BBidsBBidsHomeBundle:Reviews')->find($reviewid);

			$reviews->setReview($review);
			$reviews->setServicesummary($servicesummary);
			$reviews->setServiceexperience($serviceexp);
			$em->persist($reviews);
			$em->flush();


			//return $this->redirect($this->generateUrl('b_bids_b_bids_admin_home'));
			$parstatus=2;
			$offset=1;
			return $this->redirect($this->generateUrl('b_bids_b_bids_reviews', array('offset'=>$offset,'status'=>$parstatus)));


		}


		return $this->render('BBidsBBidsHomeBundle:Admin:editreview.html.twig', array('form'=>$form->createView()));
	}

	public function editadminAction(Request $request, $userid)
	{
		$session = $this->container->get('session');

		if($session->has('uid') && $session->get('pid')==1){
			$uid = $userid;



			$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findBy(array('id'=>$uid));
			//print_r($userArray);exit;
			foreach($userArray as $u){
				$email  = $u->getEmail();
				$password  = $u->getPassword();
				$status = $u->getStatus();
			}

			$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));

			foreach($accountArray as $a){
				$profileid = $a->getProfileid();
				$address = $a->getAddress();
				$smsphone = $a->getSmsphone();
				$homephone = $a->getHomephone();
				$bizname = $a->getBizname();
				$contactname = $a->getContactname();
				$fax = $a->getFax();
			}

			$form = $this->createFormBuilder()

				->add('username', 'text', array('label'=>'User Name','data'=>$email,'read_only' => true))
				->add('npassword', 'password', array('label'=>'New Password', 'required'=> false))
				->add('rpassword', 'password', array('label'=>'ReEnter Password', 'required'=> false))
				->add('address', 'textarea', array('label'=>'Address', 'required'=> false, 'data'=>$address))
				->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Contact Number ','empty_value'=>'Area Code', 'mapped'=>FALSE))
				->add('smsphone', 'text', array('label'=>'SMS-Phone', 'data'=>$smsphone, 'invalid_message'=>'Mobile Number field cannot contain strings', 'attr'=>array('size'=>10), 'constraints'=> new Assert\Length(array('min'=>7, 'max'=>7, 'minMessage'=>'Please enter a 7 digit mobile number', 'maxMessage'=>'Please enter a 7 digit mobile number'))))
				->add('homecode','choice',array('choices'=>array('1'=>'02' ,'2'=>'03','3'=>'04','4'=>'06'),'label'=>'Phone Number ','empty_value'=>'Area Code', 'mapped'=>FALSE))
				->add('homephone','text', array('label'=>'Home Phone', 'required'=>false, 'data'=>$homephone,'attr'=>array('size'=>10), 'constraints'=> new Assert\Length(array('min'=>7, 'max'=>7, 'minMessage'=>'Please enter 7 digit home phone', 'maxMessage'=>'Please enter 7 digit home phone'))))
				->add('bizname', 'text', array('label'=>'Business Name', 'data'=>$bizname, 'required'=>false))
			 	->add('contactname', 'text', array('label'=>'Contact Name', 'data'=>$contactname, 'required'=>false))
				->add('fax', 'text', array('label'=>'Fax Phone', 'required'=>false, 'data'=>$fax,'attr'=>array('size'=>10)))
				->add('update', 'submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
				->getForm();

				$form->handleRequest($request);

				if($request->isMethod('POST')){
					if($form->isValid()){

						$npassword = $form['npassword']->getData();
						$rpassword = md5($form['rpassword']->getData());
						$smsphone = $form['smsphone']->getData();
						$homephone = $form['homephone']->getData();
						$fax = $form['fax']->getData();

						$address = $form['address']->getData();
						$bizname = $form['bizname']->getData();
						$contactname = $form['contactname']->getData();

						if(!is_numeric($smsphone)){
							$session = $this->container->get('session');

							$session->getFlashbag()->add('error','Mobile number should not contain strings');

							return $this->render('BBidsBBidsHomeBundle:Admin:edituser.html.twig', array('email'=>$email,'form'=>$form->createView()));
						}
						if($homephone!="" && !is_numeric($homephone)){
							$session = $this->container->get('session');

                                                        $session->getFlashbag()->add('error','Home phone should not contain strings');

                                                        return $this->render('BBidsBBidsHomeBundle:Admin:edituser.html.twig', array('email'=>$email,'form'=>$form->createView()));

						}
						if($fax!="" && !is_numeric($fax)){
							$session = $this->container->get('session');

                                                        $session->getFlashbag()->add('error','Fax number should not contain strings');

                                                        return $this->render('BBidsBBidsHomeBundle:Admin:edituser.html.twig', array('email'=>$email,'form'=>$form->createView()));
						}

						$em = $this->getDoctrine()->getManager();

						$user = $em->getRepository('BBidsBBidsHomeBundle:User')->find($uid);

						$user = $a->setPassword($rpassword);
						//$user->setStatus($status);

						$em->flush();

						$accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$uid));
						foreach($accountArray as $a){
							$accountid = $a->getId();

						}

						$account = new Account();

						$em = $this->getDoctrine()->getManager();

						$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->find($accountid);

						$account->setSmsphone($smsphone);
						$account->setAddress($address);
						$account->setContactname($contactname);
						$account->setHomephone($homephone);
						$account->setBizname($bizname);
						$account->setFax($fax);

						$em->flush();

						$session = $this->container->get('session');

						$session->getFlashBag()->add('message','The record has been updated successfully!!');

						return $this->redirect($this->generateUrl('b_bids_b_bids_admin_edit', array('userid'=>$uid)));


					}
				}
				return $this->render('BBidsBBidsHomeBundle:Admin:adminedit.html.twig', array('form'=>$form->createView(), 'editemail'=>$email));
		}
		else{
			$session = $this->container->get('session');

			$session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');

			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
		}
	}

	private function _datatableAdminUsers()
	{


		$em = $this->getDoctrine()->getManager();
		$query = $em->createQueryBuilder()
			->select('u.id, u.email, u.created, u.updated, u.lastaccess, u.status, a.profileid, a.contactname,a.smsphone')
			->from('BBidsBBidsHomeBundle:User', 'u')
			->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'WITH', 'u.id = a.userid')
			->add('where','a.profileid !=1')
			->add('orderBy', 'u.id DESC');
			/*$abc = $query->getQuery()->getResult();
			echo '<pre/>';
print_r($abc);
exit;*/

		$datatable = $this->get('datatable')
						->setFields(
                        array(
							"Date Created"     => 'u.created',
							"Profile"          => 'a.profileid',
							"Name"             => 'a.contactname',
							"Email"            => 'u.email',
							"Contact Number"   => 'a.smsphone',
							"Status"           => 'u.status',
							"Action"           => 'a.userid',
							"Updated Date"     => 'u.updated',
							"Last Access Date" => 'u.lastaccess',
							"_identifier_"     => 'u.id'
                            )
        				)
        				->setSearch(true)
                		->setHasAction(false);
		$datatable->getQueryBuilder()->setDoctrineQueryBuilder($query);
    	return $datatable;
	}

	public function usersGridAction()
	{
	   return $this->_datatableAdminUsers()->execute();                                      // call the "execute" method in your grid action
	}

	function usersNewAction()
	{
		$this->_datatableAdminUsers();
		return $this->render('BBidsBBidsHomeBundle:Admin:userlist_new.html.twig');
	}

	function vendorReportsAction()
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	$searchform = $this->createFormBuilder()
        				->setAction($this->generateUrl('b_bids_b_bids_exportall_vendors'))
                        ->add('bSearchable_0', 'hidden') //date
                        ->add('bSearchable_1', 'hidden')
                        ->add('bSearchable_2', 'hidden') //name
                        ->add('bSearchable_3', 'hidden') //email
                        ->add('bSearchable_4', 'hidden') //contact
                        ->add('bSearchable_5', 'hidden') //status
                        ->add('bSearchable_6', 'hidden')
                        ->add('bSearchable_7', 'hidden')
                        ->getForm();

			return $this->render('BBidsBBidsHomeBundle:Admin:vendor_reports.html.twig',array( 'form'=>$searchform->createView()));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function customerReportsAction()
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	$searchform = $this->createFormBuilder()
        				->setAction($this->generateUrl('b_bids_b_bids_exportall_customer'))
                        ->add('bSearchable_0', 'hidden') //date
                        ->add('bSearchable_1', 'hidden')
                        ->add('bSearchable_2', 'hidden') //name
                        ->add('bSearchable_3', 'hidden') //email
                        ->add('bSearchable_4', 'hidden') //contact
                        ->add('bSearchable_5', 'hidden') //status
                        ->add('bSearchable_6', 'hidden')
                        ->add('bSearchable_7', 'hidden')
                        ->getForm();
			return $this->render('BBidsBBidsHomeBundle:Admin:customer_reports.html.twig',array( 'form'=>$searchform->createView()));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function orderReportsAction()
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {

        	$searchform = $this->createFormBuilder()
        				->setAction($this->generateUrl('b_bids_b_bids_exportall_orders'))
                        ->add('bSearchable_0', 'hidden') //date
                        ->add('bSearchable_1', 'hidden') //Order no
                        ->add('bSearchable_2', 'hidden') //Vendor name
                        ->add('bSearchable_3', 'hidden') //Bizname
                        ->add('bSearchable_4', 'hidden') //Category
                        ->add('bSearchable_5', 'hidden') //Lead Package
                        ->add('bSearchable_6', 'hidden') //Payment Type
                        ->add('bSearchable_7', 'hidden') //Amount
                        ->getForm();

			return $this->render('BBidsBBidsHomeBundle:Admin:order_reports.html.twig', array('form'=>$searchform->createView()));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function categoriesReportsAction()
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
			return $this->render('BBidsBBidsHomeBundle:Admin:categories_reports.html.twig');
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function enquiriesReportsAction()
	{
		$session = $this->container->get('session');

        if($session->has('uid') && $session->get('pid') == 1) {
			$em    = $this->getDoctrine()->getManager();
			$query = $em->createQueryBuilder()
					  ->select('c.category')
					  ->from('BBidsBBidsHomeBundle:Categories', 'c')
					  ->add('where','c.parentid=0');
			$categories = $query->getQuery()->getResult();

			$catList    = array();
			foreach($categories as $cat) {
				$catList[] = $cat['category'];
			}

			$searchform = $this->createFormBuilder()
        				->setAction($this->generateUrl('b_bids_b_bids_exportall_jobrequest'))
                        ->add('bSearchable_0', 'hidden') //date
                        ->add('bSearchable_1', 'hidden') //Job Req no
                        ->add('bSearchable_2', 'hidden') //Subject
                        ->add('bSearchable_3', 'hidden') //Categories
                        ->add('bSearchable_4', 'hidden') //Description
                        ->add('bSearchable_5', 'hidden') //City
                        ->add('bSearchable_6', 'hidden') //Email
                        ->add('bSearchable_7', 'hidden') //Status
                        ->getForm();

			return $this->render('BBidsBBidsHomeBundle:Admin:enquiries_reports.html.twig', array('categories'=>$catList,'form'=>$searchform->createView()));
        } else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function exportOrdersToExcelAction()
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
			// Create new PHPExcel object
			$objPHPExcel = new \PHPExcel();

			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("Maarten Balliauw")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");


			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Date Recieved')
						->setCellValue('B1', 'Order Number')
						->setCellValue('C1', 'Vendor Name')
						->setCellValue('D1', 'Business Name')
						->setCellValue('E1', 'Category')
						->setCellValue('F1', 'Lead Package')
						->setCellValue('G1', 'Payment Type')
						->setCellValue('H1', 'Amount');
        	$formInputs = $this->getRequest()->request->all('form');
        	if(isset($formInputs['form'])) { //FOr selected output

        		$searchInputsArray = $formInputs['form'];

        		$em = $this->getDoctrine()->getEntityManager();
        		$connection = $em->getConnection();
        		$aColumns = array( "o.created", "o.ordernumber", "a.contactname", "a.bizname", "c.category", "l.leadpack", "o.payoption", "o.amount" );

        		$sWhere = "";
        		/* Individual column filtering */
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if ( isset($searchInputsArray['bSearchable_'.$i]) && $searchInputsArray['bSearchable_'.$i] != '' )
					{
						if ( $sWhere == "" )
						{
							$sWhere = "WHERE ";
						}
						else
						{
							$sWhere .= " AND ";
						}
						$columnFilterValue = ($searchInputsArray['bSearchable_' . $i]);
						$rangeSeparator = "~";
			            $columnFilterRangeMatches = explode($rangeSeparator, $columnFilterValue);
						if($aColumns[$i] == 'o.created') {
			                if (!empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " BETWEEN '" . $columnFilterRangeMatches[0] . "' and '" . $columnFilterRangeMatches[1] . "' ";
			                else if (empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " <= '" . $columnFilterRangeMatches[1] . "' ";
			                else if (!empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " >= '" . $columnFilterRangeMatches[0] . "' ";
			    		}
						else if($aColumns[$i] == 'o.amount') {
			                if (!empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " BETWEEN '" . $columnFilterRangeMatches[0] . "' and '" . $columnFilterRangeMatches[1] . "' ";
			                else if (empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " <= '" . $columnFilterRangeMatches[1] . "' ";
			                else if (!empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " >= '" . $columnFilterRangeMatches[0] . "' ";
			    		}
						else if($aColumns[$i] == 'l.leadpack') {
			                $sWhere .= $aColumns[$i]." = '".($searchInputsArray['bSearchable_'.$i])."' ";
			    		}
				        else
							$sWhere .= $aColumns[$i]." LIKE '%".($searchInputsArray['bSearchable_'.$i])."%' ";
					}
				}

				$sSelect = "";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sSelect .= $aColumns[$i] .' as `'.$aColumns[$i].'`, ';
				}
				$sSelect = substr_replace( $sSelect, "", -2 );

				$sQuery = "SELECT SQL_CALC_FOUND_ROWS $sSelect FROM bbids_order o INNER JOIN bbids_orderproductsrel l ON  substring(o.ordernumber,3)=l.ordernumber  INNER JOIN bbids_categories c ON l.categoryid=c.id  INNER JOIN bbids_account a ON a.userid=o.author $sWhere ORDER BY o.created DESC";
				// echo $sQuery;exit;
        		$query= $connection->prepare($sQuery);
        		$query->execute();
                $orders = $query->fetchAll();
                // echo '<pre/>';print_r($orders);exit;
                $opt =  $orders;
				$ordersCount = count($orders);
				for($k=0;$k<$ordersCount;$k++)
				{
					$j = $k+2;
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['o.created'])
								->setCellValue('B'.$j, $opt[$k]['o.ordernumber'])
								->setCellValue('C'.$j, $opt[$k]['a.contactname'])
								->setCellValue('D'.$j, $opt[$k]['a.bizname'])
								->setCellValue('E'.$j, $opt[$k]['c.category'])
								->setCellValue('F'.$j, $this->getLeadpack($opt[$k]['l.leadpack']))
								->setCellValue('G'.$j, $opt[$k]['o.payoption'])
								->setCellValue('H'.$j, $opt[$k]['o.amount']);
				}
        	} else {
				$em = $this->getDoctrine()->getManager();

				$query = $em->createQueryBuilder()
							->select('o.status, o.ordernumber, o.created, o.amount, o.payoption, a.contactname, a.bizname,c.category,l.leadpack')
							->from('BBidsBBidsHomeBundle:Order', 'o')
							->innerJoin('BBidsBBidsHomeBundle:Orderproductsrel', 'l', 'with' , 'substring(o.ordernumber,3)=l.ordernumber')
							->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with' , 'l.categoryid=c.id')
							->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'a.userid=o.author')
							->add('orderBy', 'o.created DESC');

				$orders = $query->getQuery()->getScalarResult();

				$opt =  $orders;
				$ordersCount = count($orders);
				for($k=0;$k<$ordersCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['created'])
								->setCellValue('B'.$j, $opt[$k]['ordernumber'])
								->setCellValue('C'.$j, $opt[$k]['contactname'])
								->setCellValue('D'.$j, $opt[$k]['bizname'])
								->setCellValue('E'.$j, $opt[$k]['category'])
								->setCellValue('F'.$j, $this->getLeadpack($opt[$k]['leadpack']))
								->setCellValue('G'.$j, $opt[$k]['payoption'])
								->setCellValue('H'.$j, $opt[$k]['amount']);
				}
			}
			$objPHPExcel->getActiveSheet()->setTitle('Order Report');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$response = new Response();
			// Redirect output to a client’s web browser (Excel5)
			$response->headers->set('Content-Type', 'application/vnd.ms-excel');
			$response->headers->set('Content-Disposition', 'attachment;filename="Order Report.xls"');
			$response->headers->set('Cache-Control', 'max-age=0');
			// If you're serving to IE 9, then the following may be needed
		    $response->headers->set('Cache-Control', 'max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			$response->headers->set('Expires', 'Mon 26 Jul 1997 05:00:00 GMT'); // Date in the past

			$response->headers->set('Cache-Control',' cache, must-revalidate'); // HTTP/1.1
			$response->headers->set('Pragma', 'public'); // HTTP/1.0

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


			$response->sendHeaders();

			$response->setContent($objWriter->save('php://output'));

		   	return $response;
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}


	function exportCustomersToExcelAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	$objPHPExcel = new \PHPExcel();

			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("Maarten Balliauw")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Date Recieved')
						->setCellValue('B1', 'Contact Name')
						->setCellValue('C1', 'Email')
						->setCellValue('D1', 'Phone')
						->setCellValue('E1', 'Status')
						->setCellValue('F1', 'Updated');

        	$formInputs = $this->getRequest()->request->all('form');
        	if(isset($formInputs['form'])) { //FOr selected output

        		$searchInputsArray = $formInputs['form'];

        		$em = $this->getDoctrine()->getEntityManager();
        		$connection = $em->getConnection();
        		$aColumns = array("u.created","a.contactname", "u.email",  "a.smsphone",  "u.status", "u.updated", "u.lastaccess");
        		$sWhere = '';
        		/* Individual column filtering */
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if ( isset($searchInputsArray['bSearchable_'.$i]) && $searchInputsArray['bSearchable_'.$i] != '' )
					{
						if ( $sWhere == "" )
						{
							$sWhere .= " AND ";
						}
						$columnFilterValue = ($searchInputsArray['bSearchable_' . $i]);
						$rangeSeparator = "~";
			            $columnFilterRangeMatches = explode($rangeSeparator, $columnFilterValue);
						 if($aColumns[$i] == 'u.created') {
			                if (empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1])){
			                			                        /*$sWhere .= " 0 = 0 ";*/}
			                else if (!empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= " DATE( ".$aColumns[$i].")" . " BETWEEN '" . $columnFilterRangeMatches[0] . "' and '" . $columnFilterRangeMatches[1] . "' ";
			                else if (empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " <= '" . $columnFilterRangeMatches[1] . "' ";
			                else if (!empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " >= '" . $columnFilterRangeMatches[0] . "' ";
			    		}
				        else
							$sWhere .= $aColumns[$i]." LIKE '%".($searchInputsArray['bSearchable_'.$i])."%' ";
					}
				}

				$sSelect = "";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sSelect .= $aColumns[$i] .' as `'.$aColumns[$i].'`, ';
				}
				$sSelect = substr_replace( $sSelect, "", -2 );

				$sQuery = "SELECT SQL_CALC_FOUND_ROWS $sSelect FROM bbids_user u INNER JOIN bbids_account a ON  u.id = a.userid WHERE a.profileid =2 $sWhere order by u.created DESC";
				// echo $sQuery;exit;
        		$query= $connection->prepare($sQuery);
        		$query->execute();
                $customers = $query->fetchAll();
                // echo '<pre/>';print_r($customers);exit;

                $opt =  $customers;
				$customersCount = count($customers);
				for($k=0;$k<$customersCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['u.created'])
								->setCellValue('B'.$j, $opt[$k]['a.contactname'])
								->setCellValue('C'.$j, $opt[$k]['u.email'])
								->setCellValue('D'.$j, $opt[$k]['a.smsphone'])
								->setCellValue('E'.$j, $this->getProfileStatus($opt[$k]['u.status']))
								->setCellValue('F'.$j, $opt[$k]['u.updated']);
				}
        	} else {
				$em = $this->getDoctrine()->getManager();

				$query = $em->createQueryBuilder()
							->select('u.status,a.contactname,u.created,u.updated,u.email,a.contactname,a.smsphone')
							->from('BBidsBBidsHomeBundle:User', 'u')
							->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'u.id=a.userid')
							->add('where','a.profileid=2')
							->add('orderBy', 'u.created DESC');

				$customers = $query->getQuery()->getScalarResult();

				$opt =  $customers;
				$customersCount = count($customers);
				for($k=0;$k<$customersCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['created'])
								->setCellValue('B'.$j, $opt[$k]['contactname'])
								->setCellValue('C'.$j, $opt[$k]['email'])
								->setCellValue('D'.$j, $opt[$k]['smsphone'])
								->setCellValue('E'.$j, $this->getProfileStatus($opt[$k]['status']))
								->setCellValue('F'.$j, $opt[$k]['updated']);
				}
			}
			$objPHPExcel->getActiveSheet()->setTitle('Customer Report');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$response = new Response();
			// Redirect output to a client’s web browser (Excel5)
			$response->headers->set('Content-Type', 'application/vnd.ms-excel');
			$response->headers->set('Content-Disposition', 'attachment;filename="Customer Report.xls"');
			$response->headers->set('Cache-Control', 'max-age=0');
			// If you're serving to IE 9, then the following may be needed
		    $response->headers->set('Cache-Control', 'max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			$response->headers->set('Expires', 'Mon 26 Jul 1997 05:00:00 GMT'); // Date in the past

			$response->headers->set('Cache-Control',' cache, must-revalidate'); // HTTP/1.1
			$response->headers->set('Pragma', 'public'); // HTTP/1.0

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

			$response->sendHeaders();

			$response->setContent($objWriter->save('php://output'));

		   	return $response;

		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function exportVendorToExcelAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {

        	$objPHPExcel = new \PHPExcel();

			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("Maarten Balliauw")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Created Date')
						->setCellValue('B1', 'Contact Name')
						->setCellValue('C1', 'Business Name')
						->setCellValue('D1', 'Email')
						->setCellValue('E1', 'Sms Phone')
						->setCellValue('F1', 'Business Phone')
						->setCellValue('G1', 'Address')
						->setCellValue('H1', 'Fax Number')
						->setCellValue('I1', 'Description')
						->setCellValue('J1', 'Status')
						->setCellValue('K1', 'updated');
			$formInputs = $this->getRequest()->request->all('form');
        	if(isset($formInputs['form'])) { //FOr selected output

        		$searchInputsArray = $formInputs['form'];

        		$em = $this->getDoctrine()->getEntityManager();
        		$connection = $em->getConnection();
        		$aColumns = array("u.created", "a.profileid", "a.contactname", "u.email",  "a.smsphone",  "u.status", "u.updated", "u.lastaccess");
        		$sWhere = '';
        		for ( $i=0 ; $i<count($searchInputsArray) ; $i++ )
				{
					if ( isset($searchInputsArray['bSearchable_'.$i]) && $searchInputsArray['bSearchable_'.$i] != '' )
					{
						if ( $sWhere == "" )
						{
							$sWhere = "AND ";
						}
						$columnFilterValue = ($searchInputsArray['bSearchable_' . $i]);
						$rangeSeparator = "~";
			            $columnFilterRangeMatches = explode($rangeSeparator, $columnFilterValue);
						 if($aColumns[$i] == 'u.created') {
			                if (empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1])){
			                	//$sWhere .= " 0 = 0 ";
			                }
			                else if (!empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " BETWEEN '" . $columnFilterRangeMatches[0] . "' and '" . $columnFilterRangeMatches[1] . "' ";
			                else if (empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " <= '" . $columnFilterRangeMatches[1] . "' ";
			                else if (!empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " >= '" . $columnFilterRangeMatches[0] . "' ";
			    		}
				        else
							$sWhere .= $aColumns[$i]." LIKE '%".($searchInputsArray['bSearchable_'.$i])."%' ";
					}
				}
				$sSelect = "";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sSelect .= $aColumns[$i] .' as `'.$aColumns[$i].'`, ';
				}
				$sSelect = substr_replace( $sSelect, "", -2 );

				$sQuery = "SELECT SQL_CALC_FOUND_ROWS $sSelect, u.status, a.bizname, a.homephone, a.address, a.fax, a.description FROM bbids_user u INNER JOIN bbids_account a ON  u.id = a.userid WHERE a.profileid =3 $sWhere order by u.created DESC";
				// echo $sQuery;exit;
        		$query= $connection->prepare($sQuery);
        		$query->execute();
                $vendors = $query->fetchAll();
                // echo '<pre/>';print_r($vendors);exit;
                $opt =  $vendors;
				$vendorsCount = count($vendors);
				for($k=0;$k<$vendorsCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['u.created'])
								->setCellValue('B'.$j, $opt[$k]['a.contactname'])
								->setCellValue('C'.$j, $opt[$k]['bizname'])
								->setCellValue('D'.$j, $opt[$k]['u.email'])
								->setCellValue('E'.$j, $opt[$k]['a.smsphone'])
								->setCellValue('F'.$j, $opt[$k]['homephone'])
								->setCellValue('G'.$j, $opt[$k]['address'])
								->setCellValue('H'.$j, $opt[$k]['fax'])
								->setCellValue('I'.$j, $opt[$k]['description'])
								->setCellValue('J'.$j, $this->getProfileStatus($opt[$k]['status']))
								->setCellValue('K'.$j, $opt[$k]['u.updated']);
				}

        	} else {
				$em = $this->getDoctrine()->getManager();

				$query = $em->createQueryBuilder()
							->select('u.status,a.contactname,a.bizname,a.homephone,a.fax,a.address,a.description,u.created,u.updated,u.email,a.contactname,a.smsphone')
							->from('BBidsBBidsHomeBundle:User', 'u')
							->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'u.id=a.userid')
							->add('where','a.profileid=3')
							->add('orderBy', 'u.created DESC');

				$vendors = $query->getQuery()->getScalarResult();

				$opt =  $vendors;
				$vendorsCount = count($vendors);
				for($k=0;$k<$vendorsCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['created'])
								->setCellValue('B'.$j, $opt[$k]['contactname'])
								->setCellValue('C'.$j, $opt[$k]['bizname'])
								->setCellValue('D'.$j, $opt[$k]['email'])
								->setCellValue('E'.$j, $opt[$k]['smsphone'])
								->setCellValue('F'.$j, $opt[$k]['homephone'])
								->setCellValue('G'.$j, $opt[$k]['address'])
								->setCellValue('H'.$j, $opt[$k]['fax'])
								->setCellValue('I'.$j, $opt[$k]['description'])
								->setCellValue('J'.$j, $this->getProfileStatus($opt[$k]['status']))
								->setCellValue('K'.$j, $opt[$k]['updated']);
				}
			}
			$objPHPExcel->getActiveSheet()->setTitle('Vendor Report');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$response = new Response();
			// Redirect output to a client’s web browser (Excel5)
			$response->headers->set('Content-Type', 'application/vnd.ms-excel');
			$response->headers->set('Content-Disposition', 'attachment;filename="Vendor Report.xls"');
			$response->headers->set('Cache-Control', 'max-age=0');
			// If you're serving to IE 9, then the following may be needed
		    $response->headers->set('Cache-Control', 'max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			$response->headers->set('Expires', 'Mon 26 Jul 1997 05:00:00 GMT'); // Date in the past

			$response->headers->set('Cache-Control',' cache, must-revalidate'); // HTTP/1.1
			$response->headers->set('Pragma', 'public'); // HTTP/1.0

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


			$response->sendHeaders();

			$response->setContent($objWriter->save('php://output'));

		   	return $response;

		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function exportJobrequestToExcelAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	$objPHPExcel = new \PHPExcel();

			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("Maarten Balliauw")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Created Date')
						->setCellValue('B1', 'Job Request Number')
						->setCellValue('C1', 'Subject')
						->setCellValue('D1', 'Categories')
						->setCellValue('E1', 'Description')
						->setCellValue('F1', 'City')
						->setCellValue('G1', 'Email')
						->setCellValue('H1', 'Status');
        	$formInputs = $this->getRequest()->request->all('form');
        	if(isset($formInputs['form'])) { //FOr selected output
				$searchInputsArray = $formInputs['form'];

        		$em = $this->getDoctrine()->getEntityManager();
        		$connection = $em->getConnection();
        		$aColumns = array( "e.created", "e.id", "e.subj", "c.category", "e.description", "e.city",  "u.email", "e.status" );
        		$sWhere = '';
        		/* Individual column filtering */
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if ( isset($searchInputsArray['bSearchable_'.$i]) && $searchInputsArray['bSearchable_'.$i] != '' )
					{
						if ( $sWhere == "" ) {
							$sWhere .= " WHERE ";
						} else {
							$sWhere .= " AND ";
						}

						$columnFilterValue = ($searchInputsArray['bSearchable_' . $i]);
						$rangeSeparator = "~";
			            $columnFilterRangeMatches = explode($rangeSeparator, $columnFilterValue);
						 if($aColumns[$i] == 'e.created') {
			                if (!empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " BETWEEN '" . $columnFilterRangeMatches[0] . "' and '" . $columnFilterRangeMatches[1] . "' ";
			                else if (empty($columnFilterRangeMatches[0]) && !empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " <= '" . $columnFilterRangeMatches[1] . "' ";
			                else if (!empty($columnFilterRangeMatches[0]) && empty($columnFilterRangeMatches[1]))
			                        $sWhere .= $aColumns[$i] . " >= '" . $columnFilterRangeMatches[0] . "' ";
			    		}
				        else
							$sWhere .= $aColumns[$i]." LIKE '%".($searchInputsArray['bSearchable_'.$i])."%' ";
					}
				}

				$sSelect = "";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sSelect .= $aColumns[$i] .' as `'.$aColumns[$i].'`, ';
				}
				$sSelect = substr_replace( $sSelect, "", -2 );

				$sQuery = "SELECT SQL_CALC_FOUND_ROWS $sSelect FROM bbids_enquiry e INNER JOIN bbids_categories c ON  e.category=c.id  INNER JOIN bbids_user u ON e.authorid=u.id $sWhere ORDER BY e.created DESC";

        		// echo $sQuery;exit;
        		$query= $connection->prepare($sQuery);
        		$query->execute();
                $jobRequest = $query->fetchAll();
                // echo '<pre/>';print_r($jobRequest);exit;
                $opt =  $jobRequest;
				$jobRequestCount = count($jobRequest);
				for($k=0;$k<$jobRequestCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['e.created'])
								->setCellValue('B'.$j, $opt[$k]['e.id'])
								->setCellValue('C'.$j, $opt[$k]['e.subj'])
								->setCellValue('D'.$j, $opt[$k]['c.category'])
								->setCellValue('E'.$j, $opt[$k]['e.description'])
								->setCellValue('F'.$j, $opt[$k]['e.city'])
								->setCellValue('G'.$j, $opt[$k]['u.email'])
								->setCellValue('H'.$j, $this->getProfileStatus($opt[$k]['e.status']));
				}
        	} else {
				$em = $this->getDoctrine()->getManager();

				$query = $em->createQueryBuilder()
							->select('e.status,u.email,e.city,e.description,c.category,e.subj,e.id,e.created')
							->from('BBidsBBidsHomeBundle:Enquiry', 'e')
							->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with' , 'e.category=c.id')
							->innerJoin('BBidsBBidsHomeBundle:User', 'u', 'with' , 'e.authorid=u.id')
							->add('orderBy', 'e.created DESC');

				$jobRequest = $query->getQuery()->getScalarResult();


				$opt =  $jobRequest;
				$jobRequestCount = count($jobRequest);
				for($k=0;$k<$jobRequestCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['created'])
								->setCellValue('B'.$j, $opt[$k]['id'])
								->setCellValue('C'.$j, $opt[$k]['subj'])
								->setCellValue('D'.$j, $opt[$k]['category'])
								->setCellValue('E'.$j, $opt[$k]['description'])
								->setCellValue('F'.$j, $opt[$k]['city'])
								->setCellValue('G'.$j, $opt[$k]['email'])
								->setCellValue('H'.$j, $this->getProfileStatus($opt[$k]['status']));
				}
			}

			$objPHPExcel->getActiveSheet()->setTitle('Job Request Report');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$response = new Response();
			// Redirect output to a client’s web browser (Excel5)
			$response->headers->set('Content-Type', 'application/vnd.ms-excel');
			$response->headers->set('Content-Disposition', 'attachment;filename="Job Request Report.xls"');
			$response->headers->set('Cache-Control', 'max-age=0');
			// If you're serving to IE 9, then the following may be needed
		    $response->headers->set('Cache-Control', 'max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			$response->headers->set('Expires', 'Mon 26 Jul 1997 05:00:00 GMT'); // Date in the past

			$response->headers->set('Cache-Control',' cache, must-revalidate'); // HTTP/1.1
			$response->headers->set('Pragma', 'public'); // HTTP/1.0

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

			$response->sendHeaders();

			$response->setContent($objWriter->save('php://output'));

		   	return $response;

		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function exportCategoriesToExcelAction()
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	if(false) { //FOr selected output

        	} else {
				$em = $this->getDoctrine()->getManager();

				$query = $em->createQueryBuilder()
							->select('c.category,c.price,c.description')
							->from('BBidsBBidsHomeBundle:Categories', 'c')
							->add('where','c.parentid=0');

				$customers = $query->getQuery()->getScalarResult();

				$objPHPExcel = new \PHPExcel();

				$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
											 ->setLastModifiedBy("Maarten Balliauw")
											 ->setTitle("Office 2007 XLSX Test Document")
											 ->setSubject("Office 2007 XLSX Test Document")
											 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
											 ->setKeywords("office 2007 openxml php")
											 ->setCategory("Test result file");


				// Add some data
				$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', 'Categories')
							->setCellValue('B1', 'Description')
							->setCellValue('C1', 'Price');
				$opt =  $customers;
				$customersCount = count($customers);
				for($k=0;$k<$customersCount;$k++)
				{
					$j = $k+2;

					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$j, $opt[$k]['category'])
								->setCellValue('B'.$j, $opt[$k]['description'])
								->setCellValue('C'.$j, $opt[$k]['price']);
				}

				$objPHPExcel->getActiveSheet()->setTitle('Categories List');

				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				$response = new Response();
				// Redirect output to a client’s web browser (Excel5)
				$response->headers->set('Content-Type', 'application/vnd.ms-excel');
				$response->headers->set('Content-Disposition', 'attachment;filename="Categories List Report.xls"');
				$response->headers->set('Cache-Control', 'max-age=0');
				// If you're serving to IE 9, then the following may be needed
			    $response->headers->set('Cache-Control', 'max-age=1');

				// If you're serving to IE over SSL, then the following may be needed
				$response->headers->set('Expires', 'Mon 26 Jul 1997 05:00:00 GMT'); // Date in the past

				$response->headers->set('Cache-Control',' cache, must-revalidate'); // HTTP/1.1
				$response->headers->set('Pragma', 'public'); // HTTP/1.0

				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


				$response->sendHeaders();

				$response->setContent($objWriter->save('php://output'));

			   	return $response;
			}
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	public function getLeadpack($packValue)
	{
		if ($packValue     == "10") {  return 'Bronze'; }
		else if ($packValue == "25") {  return 'Silver'; }
		else if ($packValue == "50") { return 'Gold'; }
		else { return 'Platinum'; }
	}

	public function getProfileStatus($profileValue)
	{
		return ($profileValue == "1") ? 'Active':'Inactive';
	}

	function savelogoAction(Request $request)
	{
		if($request->isMethod('POST')) {
			$data = $request->request->all();
			$vendorid = $data['form']['vendor'];
		}
		$logoform = $this->createFormBuilder()
					->setAction($this->generateUrl('b_bids_b_bids_vendor_save_logo'))
                    ->add('vendor','hidden',array('data'=>$vendorid))
                    ->add('image', 'file', array('mapped'=>false, 'required'=>false))
                    ->add('Upload','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
                    ->getForm();
		$logoform->handleRequest($request);
		if($logoform->isValid()) {
			$filename = $_FILES["form"]['tmp_name']['image'];
			$imageTypeArray = array(0=>'UNKNOWN',1=>'GIF',2=>'JPEG',3=>'PNG',4=>'SWF',5=>'PSD',6=>'BMP',7=>'TIFF_II',8=>'TIFF_MM',9=>'JPC',10=>'JP2',11=>'JPX',12=>'JB2',13=>'SWC',14=>'IFF',15=>'WBMP',16=>'XBM',17=>'ICO',18=>'COUNT');
    		list($width, $height, $type, $attr) = getimagesize($filename);
			$profileimage = $vendorid.'-logo.'.strtolower($imageTypeArray[$type]);//$_FILES["form"]["name"]['image'];
			$dir = "/var/www/html/web/logo/";
			$logoform['image']->getData()->move($dir, $profileimage);

			$em = $this->getDoctrine()->getManager();
			/*$userArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('userid'=>$vendorid));

			foreach($userArray as $u){
				$id = $u->getId();
			}*/
			/*$account = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
			$account->setLogopath($profileimage);
            $em->flush();*/

            $vendorLogoObject = $em->getRepository('BBidsBBidsHomeBundle:Vendorlogorel')->findOneBy(array('vendorid'=>$vendorid));
            if(is_null($vendorLogoObject))
			{
				$newVendorlogo = new Vendorlogorel();
				$newVendorlogo->setVendorid($vendorid);
				$newVendorlogo->setFileName($profileimage);

				$em->persist($newVendorlogo);
				$em->flush();
			} else {
				$vendorLogoObject->setFileName($profileimage);

				$em->persist($vendorLogoObject);
				$em->flush();
			}
            $session = $this->container->get('session');
			$session->getFlashbag()->add('success','Logo update successful');

			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		} else {
			$session = $this->container->get('session');
			$session->getFlashbag()->add('error','Failed to update');
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vendorid)));
		}
	}

	function vedorOnOffServiceAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
			$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));
			$catArray = array();

			foreach($categoryArray as $c){
				$catname = $c->getCategory();
				$catArray[] = $catname;
			}

			return $this->render('BBidsBBidsHomeBundle:Admin:vendor_on_off.html.twig',array('catArray'=>$catArray));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function vendorOnOffAjaxAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	$data = $request->request->all();
            // echo '<pre/>';print_r(json_decode($data['data']));exit;
            $vIdsArray = json_decode($data['data']);
            $category = urldecode($data['category']);
            $categoryID = 0;
            if($category !='') {
            	$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('category'=>$category));
            	$categoryID = $categoryArray->getId();
            }
            // echo $categoryID;exit;
            $em = $this->getDoctrine()->getManager();
            $status = 1;
            $status = ($data['modetype'] == 2)? 0:1;

			if($categoryID != 0)
            $query = $em->createQueryBuilder()
                        ->update('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->set("v.status",$status)->where('v.categoryid = :categoryid')->andWhere("v.vendorid IN(:vIds)")->setParameter('categoryid', $categoryID)
                        ->setParameter('vIds', array_values($vIdsArray));
			else
			$query = $em->createQueryBuilder()
                        ->update('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                        ->set("v.status",$status)->where("v.vendorid IN(:vIds)")
                        ->setParameter('vIds', array_values($vIdsArray));

            $vendorCatUpdate = $query->getQuery()->getScalarResult();
            $response['status'] = 'success';
            $response['message'] = 'Update successful';
            return new JsonResponse( $response );
        } else {
            $response['message'] = 'Please check your login credentials and try again. If issue persists, speak to your administrator.';
            $response['status'] = 'error';
            return new JsonResponse( $response );
        }
	}

	function vendorLeadsListAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
			$searchform = $this->createFormBuilder()
                        ->setAction($this->generateUrl('b_bids_b_bids_exportall_leadhistory'))
                        ->add('bSearchable_0', 'hidden') //Company Name
                        ->add('bSearchable_1', 'hidden') //vendor Name
                        ->add('bSearchable_2', 'hidden') //Contact Number
                        ->add('bSearchable_3', 'hidden') //Category
                        ->add('bSearchable_4', 'hidden') //Lead Count
                        ->add('bSearchable_5', 'hidden') //Total Lead
                        ->getForm();
			$categoryArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));
			$catArray = array();

			foreach($categoryArray as $c){
				$catname = $c->getCategory();
				$catArray[] = $catname;
			}

			return $this->render('BBidsBBidsHomeBundle:Admin:vendor_leads_list.html.twig',array('catArray'=>$catArray,'form'=>$searchform->createView()));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function exportAllLeadsListAction(Request $request)
	{
		$session = $this->container->get('session');
        if($session->has('uid') && $session->get('pid') == 1) {
        	$objPHPExcel = new \PHPExcel();

            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Company Name')
                        ->setCellValue('B1', 'Vendor Name')
                        ->setCellValue('C1', 'Contact Number')
                        ->setCellValue('D1', 'Category')
                        ->setCellValue('E1', 'Current Lead Count')
                        ->setCellValue('F1', 'Total Lead Count');
            $formInputs = $this->getRequest()->request->all('form');
            if(isset($formInputs['form'])) { //FOr selected output

                $searchInputsArray = $formInputs['form'];

                $em = $this->getDoctrine()->getEntityManager();
                $connection = $em->getConnection();
                $aColumns = array("a.bizname","a.contactname","a.smsphone","c.category","v.leadpack","o.leadpack");
                $sWhere = '';
                for ( $i=0 ; $i<count($searchInputsArray) ; $i++ )
                {
                    if ( isset($searchInputsArray['bSearchable_'.$i]) && $searchInputsArray['bSearchable_'.$i] != '' )
                    {
                        if ( $sWhere != "" )
                        {
                            $sWhere .= " AND ";
                        }
                        $columnFilterValue = ($searchInputsArray['bSearchable_' . $i]);
                        $rangeSeparator = "~";
                        $sWhere .= $aColumns[$i]." LIKE '%".($searchInputsArray['bSearchable_'.$i])."%' ";
                    }
                }
                $sSelect = "";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                    $sSelect .= $aColumns[$i] .' as `'.$aColumns[$i].'`, ';
                }
                $sSelect = substr_replace( $sSelect, "", -2 );

                $sQuery = "SELECT SQL_CALC_FOUND_ROWS $sSelect FROM bbids_vendorcategoriesrel v INNER JOIN bbids_categories c ON v.categoryid = c.id INNER JOIN bbids_account a on v.vendorid = a.userid INNER JOIN bbids_vendororderproductsrel o ON v.categoryid = o.categoryid AND v.vendorid = o.vendorid WHERE $sWhere  group by v.id order by v.leadpack ASC";
                // echo $sQuery;exit;
                $query = $connection->prepare($sQuery);
                $query->execute();
                $vendorsLeadList = $query->fetchAll();
                // echo '<pre/>';print_r($vendors);exit;
                $opt =  $vendorsLeadList;
                $vendorsLeadListCount = count($vendorsLeadList);
                for($k=0;$k<$vendorsLeadListCount;$k++)
                {
                    $j = $k+2;

                    $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$j, $opt[$k]['a.bizname'])
                                ->setCellValue('B'.$j, $opt[$k]['a.contactname'])
                                ->setCellValue('C'.$j, $opt[$k]['a.smsphone'])
                                ->setCellValue('D'.$j, $opt[$k]['c.category'])
                                ->setCellValue('E'.$j, $opt[$k]['v.leadpack'])
                                ->setCellValue('F'.$j, $opt[$k]['o.leadpack']);
                }

            } else {
                $em = $this->getDoctrine()->getManager();

                $query = $em->createQueryBuilder()
                            ->select('a.bizname,a.contactname,a.smsphone,c.category,v.leadpack,o.leadpack as total_leads')
                            ->from('BBidsBBidsHomeBundle:Vendorcategoriesrel', 'v')
                            ->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'v.categoryid = c.id')
                            ->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with' , 'v.vendorid = a.userid')
                            ->innerJoin('BBidsBBidsHomeBundle:Vendororderproductsrel', 'o', 'with' , 'v.categoryid = o.categoryid')
                            ->add('groupBy', 'v.id')
                            ->add('orderBy', 'v.leadpack ASC');

                $vendorsLeadList = $query->getQuery()->getScalarResult();
                // echo '<pre/>';print_r($vendorsLeadList);exit;

                $opt =  $vendorsLeadList;
                $vendorsLeadListCount = count($vendorsLeadList);
                for($k=0;$k<$vendorsLeadListCount;$k++)
                {
                    $j = $k+2;

                    $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$j, $opt[$k]['bizname'])
                                ->setCellValue('B'.$j, $opt[$k]['contactname'])
                                ->setCellValue('C'.$j, $opt[$k]['smsphone'])
                                ->setCellValue('D'.$j, $opt[$k]['category'])
                                ->setCellValue('E'.$j, $opt[$k]['leadpack'])
                                ->setCellValue('F'.$j, $opt[$k]['total_leads']);
                }
            }
            $objPHPExcel->getActiveSheet()->setTitle('Vendor Lead History');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            $response = new Response();
            // Redirect output to a client’s web browser (Excel5)
            $response->headers->set('Content-Type', 'application/vnd.ms-excel');
            $response->headers->set('Content-Disposition', 'attachment;filename="Vendor Lead History.xls"');
            $response->headers->set('Cache-Control', 'max-age=0');
            // If you're serving to IE 9, then the following may be needed
            $response->headers->set('Cache-Control', 'max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            $response->headers->set('Expires', 'Mon 26 Jul 1997 05:00:00 GMT'); // Date in the past

            $response->headers->set('Cache-Control',' cache, must-revalidate'); // HTTP/1.1
            $response->headers->set('Pragma', 'public'); // HTTP/1.0

            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


            $response->sendHeaders();

            $response->setContent($objWriter->save('php://output'));

            return $response;
		}else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	protected function userToEmail($fromUID,$toUID,$fromEmail,$email,$subject,$body) {

        $em = $this->getDoctrine()->getManager();
        $useremail = new Usertoemail();

		$useremail->setFromuid($fromUID);
		$useremail->setTouid($toUID);//11 means admin
		$useremail->setFromemail($fromEmail); //support@businessbid.ae
		$useremail->setToemail($email);
		$useremail->setEmailsubj($subject);
		$useremail->setCreated(new \Datetime());
		$useremail->setEmailmessage($body);
		$useremail->setEmailtype(1);
		$useremail->setStatus(1);
        $useremail->setReadStatus(0);

		$em->persist($useremail);
		$em->flush();
	}

	function vendorCatgoryMappingAction(Request $request)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
        if($session->has('uid') && $session->get('pid') == 1) {
        	$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

			$categoryList = array();

			foreach($categoriesArray as $category){
				$categoryid = $category->getId();
				$categoryname = $category->getCategory();
				$categoryList[$categoryid] = $categoryname;
			}

			$query = $em->createQueryBuilder()
					->select('m.startDate, m.endDate, m.description, m.status, m.categoryID as cid, m.vendorID as vid, a.contactname, c.category')
					->from('BBidsBBidsHomeBundle:Vendorcategorymapping', 'm')
					->innerJoin('BBidsBBidsHomeBundle:Account', 'a', 'with', 'a.userid = m.vendorID')
					->innerJoin('BBidsBBidsHomeBundle:Categories', 'c', 'with', 'c.id = m.categoryID')
					->add('orderBy', 'm.created DESC');

			$mappingList = $query->getQuery()->getResult();
			// echo '<pre/>';print_r($mappingList);exit;

        	$form = $this->createFormBuilder()
					->add('category','choice', array('label'=>'Category ','choices'=>$categoryList, 'empty_value'=>'Select a category', 'empty_data'=>NULL,'multiple'=>FALSE,'expanded'=>FALSE))
					->add('vid','hidden',array('required'=>FALSE))
					->add('vendor_name','text',array('label'=>'Vendor Name','attr'=>array('placeholder'=>'Start typing vendor name to autocomplete')))
					->add('from_date','text',array('label'=>'From Date(Reference Date)','attr'=>array('placeholder'=>'Activation reference date')))
					->add('to_date','text',array('label'=>'To Date(Expiry Date)','attr'=>array('placeholder'=>'Expiry Date')))
					->add('description', 'textarea', array('label'=>'Enter description', 'data'=>'', 'required'=>FALSE,'attr'=>array('placeholder'=>'Optional information')))
					->add('Submit','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
					->getForm();
            $form->handleRequest($request);
         	if($request->isMethod('POST')){
				if($form->isValid()) {
					/*$data = $form->getData();
					echo '<pre/>';print_r($data);exit;*/

					$catid       = $form['category']->getData();
					if($catid == '') {
						$session->getFlashbag()->add('error','Please select category from the list');
						return $this->render('BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig', array('form'=>$form->createView(),'mappingList'=>$mappingList));
					}

					$vendorID    = $form['vid']->getData();
					if($vendorID == '') {
						$session->getFlashbag()->add('error','Please select a vendor from the list');
						return $this->render('BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig', array('form'=>$form->createView(),'mappingList'=>$mappingList));
					}

					$vendorName  = $form['vendor_name']->getData();
					$startDate   = $form['from_date']->getData(); // Reference Date
					$endDate     = $form['to_date']->getData(); // Expiry date

					$description = $form['description']->getData();

					$vendorCatIDExist = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorID,'categoryid'=>$catid,'flag'=>1));
					// echo '<pre/>';print_r($vendorCatIDExist);exit;
					if(empty($vendorCatIDExist))
					{
						$newVendorCatrel = new Vendorcategoriesrel();
						$newVendorCatrel->setVendorid($vendorID);
						$newVendorCatrel->setCategoryid($catid);
						$newVendorCatrel->setLeadpack(0);
						$newVendorCatrel->setStatus(1);
						$newVendorCatrel->setFlag(1);

						$em->persist($newVendorCatrel);
						$em->flush();

						$created = new \DateTime();

						$vendCatMapping = new Vendorcategorymapping();
						$vendCatMapping->setCategoryID($catid);
						$vendCatMapping->setVendorID($vendorID);
						$vendCatMapping->setStartDate(new \DateTime($startDate));
						$vendCatMapping->setEndDate(new \DateTime($endDate));
						$vendCatMapping->setDescription($description);
						$vendCatMapping->setCreated($created);
						$vendCatMapping->setStatus(1);

						$em = $this->getDoctrine()->getManager();
						$em->persist($vendCatMapping);
						$em->flush();

						$session->getFlashBag()->add('success','Vendor mapping successful');
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_category_mapping'));

					} else {
						$session->getFlashBag()->add('error','Vendor already mapped to this category Or have purchased this category.');
						return $this->render('BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig', array('form'=>$form->createView(),'mappingList'=>$mappingList));
					}
				}
			}
			return $this->render('BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig', array('form'=>$form->createView(),'mappingList'=>$mappingList));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function categoryFormElementsAction(Request $request)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
        if($session->has('uid') && $session->get('pid') == 1) {
        	$categoriesArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>0,'status'=>1));

			$categoryList = array();

			foreach($categoriesArray as $category){
				$categoryid = $category->getId();
				$categoryname = $category->getCategory();
				$categoryList[$categoryid] = $categoryname;
			}

            $elementTypes = $em->getRepository('BBidsBBidsHomeBundle:FormElementTypes')
                        ->createQueryBuilder('e')
                        ->select('e.name')
                        ->getQuery()
                        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $elementTypesArr = array_column($elementTypes, 'name');

        	$form = $this->createFormBuilder()
					// ->add('category','choice', array('label'=>'Category ','choices'=>$categoryList, 'empty_value'=>'Select a category', 'empty_data'=>NULL,'multiple'=>FALSE,'expanded'=>FALSE))
					->add('category','text',array('label'=>'Category id','attr'=>array('placeholder'=>'Enter category id')))
					->add('element_type','choice', array('label'=>'Element Type ','choices'=>$elementTypesArr, 'empty_value'=>'Select a category', 'empty_data'=>NULL,'multiple'=>FALSE,'expanded'=>FALSE))
					->add('multiple_choice','choice', array('label'=>'Multiple Choice','choices'=>array(1=>'Yes',0=>'No'), 'multiple'=>FALSE,'expanded'=>FALSE))
					->add('element_name','text',array('label'=>'Element Name','attr'=>array('placeholder'=>'Field name')))
					->add('extra_option','text',array('label'=>'Extra Elements', 'required'=>FALSE,'attr'=>array('placeholder'=>'For eg. Other')))
					->add('description', 'textarea', array('label'=>'Enter description', 'data'=>'', 'required'=>FALSE,'attr'=>array('placeholder'=>'Enter seprated by comma')))
					->add('Submit','submit', array('attr'=>array('class'=>'btn btn-success btn-getquotes text-center')))
					->getForm();
            $form->handleRequest($request);
         	if($request->isMethod('POST')){
				if($form->isValid()) {
					/*$data = $form->getData();
					echo '<pre/>';print_r($data);exit;*/

					$catid       = $form['category']->getData();
					if($catid == '') {
						$session->getFlashbag()->add('error','Please select category from the list');
						return $this->render('BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig', array('form'=>$form->createView(),'mappingList'=>$mappingList));
					}


					$elementType    = $form['element_type']->getData();
					$extraOption    = $form['extra_option']->getData();
					$multipleChoice = $form['multiple_choice']->getData();
					$elementName    = $form['element_name']->getData();
					$description    = $form['description']->getData();

					$newFormElements = new FormElements();
					$newFormElements->setCategoryId($catid);
					$newFormElements->setElementTypeId($elementType+1);
					$newFormElements->setCaption($elementName);
					if(($elementType+1) == 1)
						$newFormElements->setExtraOptions($description);
					else
						$newFormElements->setExtraOptions($extraOption);
					$newFormElements->setExtraOptions($extraOption);
					$newFormElements->setMultipleSelection($multipleChoice);

					$em->persist($newFormElements);
					$em->flush();
					$formElementId = $newFormElements->getId();
					if(!empty($description))
					{
						if(($elementType+1) == 2)
						{
							$fieldValues = explode(',', $description);

							foreach ($fieldValues as $fieldValue) {
								$newElementListValues = new ElementListValues();
								$newElementListValues->setFormElementsId($formElementId);
								$newElementListValues->setName(trim($fieldValue));
								$em->persist($newElementListValues);
							}

							$em->flush(); //Persist objects that did not make up an entire batch
	                		$em->clear();
						}
					}

					$session->getFlashBag()->add('success','Form Element added successfully');
					return $this->redirect($this->generateUrl('b_bids_b_bids_category_form_elements'));
				}
			}
			return $this->render('BBidsBBidsHomeBundle:Admin:category_form_element.html.twig', array('form'=>$form->createView()));
		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function toggleCategoryMappingAction($vid,$cid,$type)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
        if($session->has('uid') && $session->get('pid') == 1) {

			$vendorOrderList = $em->getRepository('BBidsBBidsHomeBundle:Order')->findBy(array('author'=>$vid,'transactionstatus'=>'Successful'));
			//This means vendor did not purchase any category so can be deactivate and remove from Vendorcategoriesrel
			if(empty($vendorOrderList)) {
				if ($type == 2) {
					$vendorMappedCategory = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findOneBy(array('vendorid'=>$vid,'categoryid'=>$cid));

					$em->remove($vendorMappedCategory);
		        	$em->flush();

		        	// Deacativate the mapping
		        	$vendCatMapping = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findOneBy(array('vendorID'=>$vid,'categoryID'=>$cid));

		        	$vendCatMapping->setStatus(0);

					$em = $this->getDoctrine()->getManager();
					$em->persist($vendCatMapping);
					$em->flush();

					$session->getFlashBag()->add('success','Vendor category mapping deactivate successful');
					return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_category_mapping'));
				} else if ($type == 1) {
					// activating the mapping
	                $vendCatMapping = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findOneBy(array('vendorID'=>$vid,'categoryID'=>$cid));

	                $vendCatMapping->setStatus(1);

	                $em = $this->getDoctrine()->getManager();
	                $em->persist($vendCatMapping);
	                $em->flush();

	                $vendorCatIDExist = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vid,'categoryid'=>$cid,'flag'=>1));
	                    // echo '<pre/>';print_r($vendorCatIDExist);exit;
	                if(empty($vendorCatIDExist))
	                {
	                    $newVendorCatrel = new Vendorcategoriesrel();
	                    $newVendorCatrel->setVendorid($vid);
	                    $newVendorCatrel->setCategoryid($cid);
	                    $newVendorCatrel->setLeadpack(0);
	                    $newVendorCatrel->setStatus(1);
	                    $newVendorCatrel->setFlag(1);

	                    $em->persist($newVendorCatrel);
	                    $em->flush();
	                }

	                $session->getFlashBag()->add('success','Vendor category mapping activation successful');
	                return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_category_mapping'));
				}

			} else {
				$purchaseFlag = FALSE;
				foreach ($vendorOrderList as $orderList) {
					$orderNumber = $orderList->getOrdernumber();
					$orderNumber = preg_replace("/[^0-9]/", '',$orderNumber);
					// echo 'orderNumber'.$orderNumber;exit;
					$vendorOrderProductRel = $em->getRepository('BBidsBBidsHomeBundle:Orderproductsrel')->findBy(array('ordernumber'=>$orderNumber,'categoryid'=>$cid));
					if(!empty($vendorOrderProductRel)){
						$purchaseFlag = TRUE;
						break;
					}
				}

				//check vendor having order for this category or not ?
				if($purchaseFlag) {
					$session->getFlashBag()->add('error','Vendor have purchase the category so it can not be deactivated Or activated');
					return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_category_mapping'));
				} else {
					if ($type == 2) {
						$vendorMappedCategory = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findOneBy(array('vendorid'=>$vid,'categoryid'=>$cid));

						$em->remove($vendorMappedCategory);
			        	$em->flush();

			        	// Deacativate the mapping
			        	$vendCatMapping = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findOneBy(array('vendorID'=>$vid,'categoryID'=>$cid));

			        	$vendCatMapping->setStatus(0);

						$em = $this->getDoctrine()->getManager();
						$em->persist($vendCatMapping);
						$em->flush();

						$session->getFlashBag()->add('success','Vendor category mapping deactivate successful');
						return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_category_mapping'));
					} else if ($type == 1) {
						// activating the mapping
		                $vendCatMapping = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findOneBy(array('vendorID'=>$vid,'categoryID'=>$cid));

		                $vendCatMapping->setStatus(1);

		                $em = $this->getDoctrine()->getManager();
		                $em->persist($vendCatMapping);
		                $em->flush();

		                $vendorCatIDExist = $em->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vid,'categoryid'=>$cid,'flag'=>1));
		                    // echo '<pre/>';print_r($vendorCatIDExist);exit;
		                if(empty($vendorCatIDExist))
		                {
		                    $newVendorCatrel = new Vendorcategoriesrel();
		                    $newVendorCatrel->setVendorid($vid);
		                    $newVendorCatrel->setCategoryid($cid);
		                    $newVendorCatrel->setLeadpack(0);
		                    $newVendorCatrel->setStatus(1);
		                    $newVendorCatrel->setFlag(1);

		                    $em->persist($newVendorCatrel);
		                    $em->flush();
		                }

		                $session->getFlashBag()->add('success','Vendor category mapping activation successful');
	                	return $this->redirect($this->generateUrl('b_bids_b_bids_vendor_category_mapping'));

					}
				}
			}

		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	function toggleVendTestAccountAction($vid,$status)
	{
		$session = $this->container->get('session');
		$em = $this->getDoctrine()->getManager();
        if($session->has('uid') && $session->get('pid') == 1) {
			if ($status == 'Yes' OR $status == 'No') {

				$flag = ($status == 'No') ? null : 'Yes';
				$vendorAccount = $em->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vid));
				$vendorAccount->setTestAccount($flag);
	        	$em->flush();

				$session->getFlashBag()->add('success','Vendor test account update to "'.$status.'" successful');
				return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));
			}
			$session->getFlashBag()->add('success','Invalid vendor account status');
			return $this->redirect($this->generateUrl('b_bids_b_bids_admin_user_edit', array('userid'=>$vid)));


		} else {
            $session->getFlashBag()->add('error','Please check your login credentials and try again. If issue persists, speak to your administrator.');
            return $this->redirect($this->generateUrl('b_bids_b_bids_admin_login'));
        }
	}

	private function sendEmail($email,$subject,$body,$uid)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'businessbid';
        $pass = '!3zxih1x0';
        /*$subject = 'Welcome to the BusinessBid Network';

        $body=*/
        $em = $this->getDoctrine()->getManager();
        $useremail = new Usertoemail();

		$useremail->setFromuid($uid);
		$useremail->setTouid(11);//11 means admin
		$useremail->setFromemail('support@businessbid.ae');
		$useremail->setToemail($email);
		$useremail->setEmailsubj($subject);
		$useremail->setCreated(new \Datetime());
		$useremail->setEmailmessage($body);
		$useremail->setEmailtype(1);
		$useremail->setStatus(1);
        $useremail->setReadStatus(0);

		$em->persist($useremail);
		$em->flush();

        $message = array(
	        'api_user'  => $user,
	        'api_key'   => $pass,
	        'to'        => $email,
	        'subject'   => $subject,
	        'html'      => $body,
	        'text'      => $body,
	        'from'      => 'support@businessbid.ae',
         );


        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $sess = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($sess, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        // Tell curl not to return headers, but do return the response
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($sess);
        curl_close($sess);
    }

}/*End of Class*/

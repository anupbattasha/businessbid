<?php
namespace BBids\BBidsHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BBids\BBidsHomeBundle\Controller\CategoryController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Mapping\ClassMetaData;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use BBids\BBidsHomeBundle\Entity\Enquiry;
use BBids\BBidsHomeBundle\Entity\Categories;
use BBids\BBidsHomeBundle\Entity\EnquirySubcategoryRel;
use BBids\BBidsHomeBundle\Entity\User;
use BBids\BBidsHomeBundle\Entity\Vendorenquiryrel;
use BBids\BBidsHomeBundle\Entity\Account;
use BBids\BBidsHomeBundle\Entity\Activity;
use BBids\BBidsHomeBundle\Entity\Ratings;
use BBids\BBidsHomeBundle\Entity\Freetrail;
use BBids\BBidsHomeBundle\Entity\Contactus;
use BBids\BBidsHomeBundle\Entity\Vendorcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Vendorcityrel;
use BBids\BBidsHomeBundle\Entity\Vendorsubcategoriesrel;
use BBids\BBidsHomeBundle\Entity\Sessioncategoriesrel;
use BBids\BBidsHomeBundle\Entity\Usertoemail;
use BBids\BBidsHomeBundle\Entity\Categoriesextrafieldsrel;
use BBids\BBidsHomeBundle\Entity\Vendorcategorymapping;
use BBids\BBidsHomeBundle\Entity\Sessionquotes;

/**
 * This is base category controller containing some basic functions
 *
 */

class BaseCategoryController extends Controller
{

    function categoriesAction(Request $request)
    {
        $session   = $this->container->get('session');
        $loginFlag = ($session->has('uid')) ? TRUE : FALSE;
        if ($request->getMethod() == 'POST') {
                $requestData = $this->get('request')->request->all();
                $form = $requestData["categories_form"];
                // echo 'valid';var_dump($form);exit;

                $em = $this->getDoctrine()->getManager();

                $categoryid       = $form['category'];
                $categorynamelist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findOneBy(array('id'=>$categoryid));
                $categoryname     = $categorynamelist->getCategory();
                $subcategoryList  = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categories')->findBy(array('parentid'=>$categoryid));
                $subcategory      = [];

                foreach($subcategoryList as $sublist){
                    $subid = $sublist->getId();
                    $subcat = $sublist->getCategory();
                    $subcategory[$subid] = $subcat;
                }

                $response['step'] = 1;

                $subcategoryids = $form['subcategories'];
                $subcount       = count($subcategoryids);

                if($subcount == 0) {
                    $response['error'] = "Please select atleast one subcategory";
                    return new JsonResponse( $response );
                }

                /*Extra fields for step 2*/
                $response['step'] = 2;



                $formOptions = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesformrel')->findOneBy(array('categoryID'=>$categoryid));

                $callMethod = $formOptions->getFormObjName();
                $callMethod = lcfirst($callMethod);

                $childCategory = new CategoryController();
                list($extraFieldsarray, $childResponse, $responseMsg, ) = $childCategory->$callMethod($form);

                if(!$childResponse) {
                    return new JsonResponse( $responseMsg );
                }
                // echo "new validation success<pre/>";print_r($extraFieldsarray);exit;
                /*Extra fields for step 2 ends */

                $response['step'] = 3;
                $description = $form['description'];
                if(strlen($description) > 120){
                    $response['error'] = "Description is more than 120 characters. Please reduce the additional information and submit form again.";
                    return new JsonResponse( $response );
                }

                $hire = $form['hire'];
                if($hire == "") {
                    $response['error'] = 'Request timeline  field cannot be blank';
                    return new JsonResponse( $response );
                }

                if($session->has('uid')) {
                    // User is logged in and not a admin
                    $userid = $sessionUserid = $session->get('uid');
                    $sessionPid    = $session->get('pid');

                    if($sessionPid == 1) {
                        $response['error'] = 'You are looged as admin. Please log out to post a job';
                        return new JsonResponse( $response );
                    }

                    $uDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                    $smsphone     = $uDetails->getSmsphone();
                    $customerName = $uDetails->getContactname();
                    $profileid    = $uDetails->getProfileid();

                    $locationtype = $form['locationtype'];
                    if($locationtype == 'UAE') {
                        $location = $form['location'];
                        $city     = $form['city'];
                    } else {
                        $location = $form['country'];
                        $city     = $form['cityint'];
                    }
                } else {
                    // User is not logged in or new user then check for user exist
                    $email = $form['email'];

                    if($email == "") {
                        $response['error'] = 'Email  field cannot be blank';
                        return new JsonResponse( $response );
                    }

                    $contactname = $form['contactname'];

                    if (ctype_alpha(str_replace(' ', '', $contactname)) === false) {
                        $response['error'] = 'Contact name should contain only alphabets and spaces';
                        return new JsonResponse( $response );
                    }

                    $smsmobile = $form['mobile'];

                    if($smsmobile) {
                        if(!is_numeric($smsmobile)){
                            $response['error'] = 'Mobile phone field should contain only numbers.';
                            return new JsonResponse( $response );
                        }
                    }

                    $locationtype = $form['locationtype'];
                    if($locationtype == 'UAE') {
                        $smsphone = $form['mobilecode']."-".$form['mobile'];
                        $home     = $form['homecode'];
                        if(!empty($home))
                            $homephone = $form['homecode']."-".$form['homephone'];
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecode'];
                        $phone    = $form['homephone'];

                        $location = $form['location'];
                        $city     = $form['city'];

                    } else {
                        $smsphone = $form['mobilecodeint']."-".$form['mobileint'];
                        $home     = $form['homecodeint'];
                        if(!empty($home))
                            $homephone = $form['homecodeint']."-".$form['homephoneint'];
                        else
                            $homephone = "";

                        $smscode  = $form['mobilecodeint'];
                        $phone    = $form['homephoneint'];

                        $location = $form['country'];
                        $city     = $form['cityint'];
                    }

                    $userDetails = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('email'=>$email));
                    if(!empty($userDetails)) {
                        $userid = $userDetails->getId();
                        $account = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$userid));

                        $existingSMSPhone = $account->getSmsphone();
                        $contactname      = $account->getContactname();
                        $profileid        = $account->getProfileid();

                        if($smsphone != $existingSMSPhone) { //Over write the smsphone number

                            $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('smsphone'=>$smsphone));
                            if(empty($smsphoneExist)) {
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            } else{
                                // echo "<pre/>";print_r($smsphoneExist);exit;
                                $smsphoneUserid = $smsphoneExist->getId();
                                if($userid != $smsphoneUserid) {
                                    $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                                    return new JsonResponse( $response );
                                }
                                $account->setSmsphone($smsphone);
                                $em->flush();
                            }
                        }

                    } else {
                        $smsphoneExist = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findBy(array('smsphone'=>$smsphone));
                        if(empty($smsphoneExist)) {
                            $mobilecode = rand(100000,999999);
                            $userid = $this->createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype);
                            // Send a welcome email to the user
                            $this->sendWelcomeEmail($contactname,$userid,$email,$mobilecode);
                        } else{
                            $response['error'] = 'Mobile number is already in use by other user. Please use same email id or login to use this number';
                            return new JsonResponse( $response );
                        }
                    }
                }/*end of else*/

                $categoryid = $form['category'];

                // Insert into bbids_enquiry table
                $enquiryid = $this->createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire);

                // Insert into bbids_enquirysubcategoryrel table
                $eSubCategoryString = '';
                $eSubCategoryString = $this->createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount);

                $eSubCategoryString = (strpos($eSubCategoryString,"Other")) ? (str_replace('Other', $subcategoryOther, $eSubCategoryString)) : $eSubCategoryString;

                if($subcount > 1) {
                    if(strlen($eSubCategoryString) > 100){
                        $eSubCategoryString = substr($eSubCategoryString,0,100).'...';
                    }
                } else {
                    $subCatID = $subcategoryids[0];
                    $eSubCategoryString = $subcategory[$subCatID];
                }

                // Insert the extra fields
                $em = $this->getDoctrine()->getManager();
                $batchSize = 5;$i = 0;
                foreach ($extraFieldsarray as $key => $value) {
                    $newFields = new Categoriesextrafieldsrel;
                    $newFields->setEnquiryID($enquiryid);
                    $newFields->setKeyName($key);
                    $newFields->setKeyValue($value);
                    $em->persist($newFields);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $i++;
                }
                $em->flush(); //Persist objects that did not make up an entire batch
                $em->clear();

                // Insert into bbids_vendorenquiryrel table
                $reqtype = $form['reqtype'];
                if($reqtype == 'popup')
                    $this->mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);
                else
                    $this->mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire);

                /*Send sms customer for confirmation only blonging to UAE */
                if($locationtype == 'UAE') {

                    $smsphoneArray = explode("-",$smsphone);
                    $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                    $smsText = "BusinessBid has logged your request $enquiryid for $eSubCategoryString. Vendors will contact you shortly.";
                    $smsresponse = $this->sendSMSToPhone($smsphone, $smsText,'Longtext');/*SMS function is blocked*/
                }
                if($session->has('uid'))
                    $email = $session->get('email');

                $this->sendEnquiryConfirmationEmail($userid,$email,$enquiryid);

                // Insert into activity table to show on home page
                $this->setNewActivity($city,$categoryname,$userid);

                $response['jobNo'] = $enquiryid;
                $response['success'] = true;


            return new JsonResponse( $response );
        }
        return $this->render("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:$twigFileName.html.twig", array('categoryForm' => $form->createView()));

    }
    /**
     * Create new user
     *
     * @author Irfan Baig <irfan.blackhawk@gmail.com>
     */
    protected function createUserAccount($email,$smsphone,$homephone,$contactname,$mobilecode,$locationtype)
    {
        $em = $this->getDoctrine()->getManager();

        $user       = new User();
        $created    = new \DateTime();
        // $userhash   = $this->generateHash(16);

        $user->setPassword(md5($mobilecode));

        $user->setEmail($email);
        $user->setCreated($created);
        $user->setUpdated($created);
        // $user->setUserhash($userhash);
        if($locationtype == 'UAE')
            $user->setStatus(1);
        else
            $user->setStatus(4);
        $user->setMobilecode($mobilecode);

        $em->persist($user);
        $em->flush();

        $userid = $user->getId();

        $account = new Account();

        $account->setProfileid(2);
        $account->setUserid($userid);
        $account->setSmsphone($smsphone);
        $account->setHomephone($homephone);
        $account->setContactname($contactname);

        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        return $userid;
    }

    /**
     * Create new job
     *
     * @author Irfan Baig <irfan.blackhawk@gmail.com>
     */
    protected function createNewEnquiry($description,$city,$categoryid,$userid,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();

        $created = new \DateTime();
        $enquiry = new Enquiry();
        $enquiry->setSubj('No Subject');
        $enquiry->setDescription($description);
        $enquiry->setCity($city);
        $enquiry->setCategory($categoryid);
        $enquiry->setAuthorid($userid);
        $enquiry->setCreated($created);
        $enquiry->setUpdated($created);

        $enquiry->setStatus(1);

        $enquiry->setLocation($location);
        $enquiry->setHirePriority($hire);

        $em = $this->getDoctrine()->getManager();
        $em->persist($enquiry);
        $em->flush();

        $enquiryid = $enquiry->getId();

        return $enquiryid;
    }

    /**
     * Add sub categories selected to specific job
     *
     * @author Irfan Baig <irfan.blackhawk@gmail.com>
     */
    protected function createEnqSubCatRel($subcategoryids,$enquiryid,$subcategory,$subcount)
    {
        $em = $this->getDoctrine()->getManager();
        $eSubCategoryString = '';
        for($i=0; $i<$subcount; $i++) {
            $subCatID = $subcategoryids[$i];
            $enquirysubrel = new EnquirySubcategoryRel();
            $enquirysubrel->setEnquiryid($enquiryid);
            $enquirysubrel->setSubcategoryid($subcategoryids[$i]);

            $eSubCategoryString .= $subcategory[$subCatID].", ";

            $em = $this->getDoctrine()->getManager();
            $em->persist($enquirysubrel);
            $em->flush();
        }
        return $eSubCategoryString;
    }

    /**
     * @author Irfan Baig <irfan.blackhawk@gmail.com>
     */
    protected function mapSelVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        //$session->start();
        $quoteID  = $session->getId();

        $hireArray = array('No Priority', 'Urgent', '1-3 days', '4-7 days', '7-14 days', 'Just Planning','Flexible Dates');

        $formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));
        $extraInfoSMS = $emailBody = '';
        $extraInfoSMS .= "Job No: ".$enquiryid.'\r\n';
        $extraInfoSMS .= $eSubCategoryString.'\r\n';
        if($formExtraFields) {
            foreach ($formExtraFields as $field) {
                $extraInfoSMS .= $field->getKeyName().': '.$field->getKeyValue().'\r\n';
            }
            $emailBody = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig',array('formExtraFields'=>$formExtraFields,'jobNo'=>$enquiryid, 'city'=>$city,'hire'=>$hire, 'description'=>$description, 'eSubCategoryString'=>$eSubCategoryString));
        }
        $extraInfoSMS .= "City: ".$city.'\r\n';
        $extraInfoSMS .= "Priority: ".$hireArray[$hire].'\r\n';
        if($description)
            $extraInfoSMS .= "Info: ".$description.'\r\n';

        if($session->has('maptovendorid')) {
            $vendorid    = $session->get('maptovendorid');
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid,'flag'=>1));

            foreach($vendorArray as $vendor){
                $status = $vendor->getStatus();
                if($status == 1) {
                    $vendorenquiryrel = new Vendorenquiryrel();

                    $vendorenquiryrel->setVendorid($vendorid);
                    $vendorenquiryrel->setEnquiryid($enquiryid);

                    $em->persist($vendorenquiryrel);
                    $em->flush();

                    // Send sms to all vendors
                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                    if($accountArray) {
                        // Send sms to mobile number provided.
                        $vendorName    = $accountArray->getContactname();
                        $smsphone      = $accountArray->getSmsphone();
                        $smsphoneArray = explode("-",$smsphone);
                        $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                        $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                        $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');/*SMS function is blocked*/
                        /*End of sms to vendors*/
                        // Send email notification to all vendors
                        $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                        if($vendorUser) {
                            $vendorEmail = $vendorUser->getEmail();
                            $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                        }
                    }
                }
            }
        } else {
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Sessionquotes')->findBy(array('categoryid'=>$categoryid,'quoteid'=>$quoteID,'status'=>1));
            $enqcount = 0;
            if(empty($vendorArray)) {

            } else {
                foreach($vendorArray as $vendor) {
                    $enqcount++;
                    $vendorid       = $vendor->getVendorid();

                    $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                    if(empty($vendenqArray)) {

                        $vendorenquiryrel = new Vendorenquiryrel();

                        $vendorenquiryrel->setVendorid($vendorid);
                        $vendorenquiryrel->setEnquiryid($enquiryid);

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($vendorenquiryrel);
                        $em->flush();

                        // Send sms to all manual mapped vendors
                        $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                        if($accountArray) {
                            // Send sms to mobile number provided.
                            $vendorName    = $accountArray->getContactname();
                            $smsphone      = $accountArray->getSmsphone();
                            $smsphoneArray = explode("-",$smsphone);
                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                            $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                            /*End of sms to vendors*/

                            // Send email notification to all manual mapped vendors
                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                            if($vendorUser) {
                               $vendorEmail = $vendorUser->getEmail();
                               $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                            }
                        }
                    }
                }
            }
        }
    }

    protected function mapVendorEnquiryRel($categoryid,$enquiryid,$city,$smsphone,$eSubCategoryString,$description,$location,$hire)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $hireArray = array('No Priority', 'Urgent', '1-3 days', '4-7 days', '7-14 days', 'Just Planning','Flexible Dates');

        $formExtraFields = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Categoriesextrafieldsrel')->findBy(array('enquiryID'=>$enquiryid));
        $extraInfoSMS = $emailBody = '';
        $extraInfoSMS .= "Job No : ".$enquiryid.'\r\n';
        $extraInfoSMS .= $eSubCategoryString.'\r\n';
        if($formExtraFields) {
            foreach ($formExtraFields as $field) {
                $extraInfoSMS .= $field->getKeyName().' : '.$field->getKeyValue().'\r\n';
            }
            $emailBody = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig',array('formExtraFields'=>$formExtraFields,'jobNo'=>$enquiryid, 'city'=>$city,'hire'=>$hire, 'description'=>$description, 'eSubCategoryString'=>$eSubCategoryString));
        }
        $extraInfoSMS .= "City : ".$city.'\r\n';
        $extraInfoSMS .= "Priority : ".$hireArray[$hire].'\r\n';
        if($description)
            $extraInfoSMS .= "Info : ".$description.'\r\n';

        if($session->has('maptovendorid')) {
            $vendorid    = $session->get('maptovendorid');
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('vendorid'=>$vendorid,'flag'=>1));

            foreach($vendorArray as $vendor){
                $status = $vendor->getStatus();
                if($status == 1) {
                    $vendorenquiryrel = new Vendorenquiryrel();

                    $vendorenquiryrel->setVendorid($vendorid);
                    $vendorenquiryrel->setEnquiryid($enquiryid);

                    $em->persist($vendorenquiryrel);
                    $em->flush();

                    // Send sms to all vendors
                    $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                    if($accountArray) {
                        // Send sms to mobile number provided.
                        $vendorName    = $accountArray->getContactname();
                        $smsphone      = $accountArray->getSmsphone();
                        $smsphoneArray = explode("-",$smsphone);
                        $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                        $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                        $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');/*SMS function is blocked*/
                        /*End of sms to vendors*/
                        // Send email notification to all vendors
                        $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                        if($vendorUser) {
                            $vendorEmail = $vendorUser->getEmail();
                            $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                        }
                    }
                }
            }
        } else {
            $vendorArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategoriesrel')->findBy(array('categoryid'=>$categoryid,'flag'=>1));
            $enqcount = 0;
            foreach($vendorArray as $vendor) {
                $enqcount++;
                $vendorid       = $vendor->getVendorid();
                $status         = $vendor->getStatus();
                $vendorleadpack = $vendor->getLeadpack();

                $manualVendCatMapping = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorcategorymapping')->findBy(array('categoryID'=>$categoryid,'vendorID'=>$vendorid,'status'=>1));
                if(empty($manualVendCatMapping)) {
                    // if($status == 1 && $vendorleadpack !=0 ) { logic changed
                    if($status == 1) {
                       $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                        if(empty($vendenqArray)) {
                             //if($enqcount == 1){
                            $vendorenquiryrel = new Vendorenquiryrel();

                            $vendorenquiryrel->setVendorid($vendorid);
                            $vendorenquiryrel->setEnquiryid($enquiryid);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($vendorenquiryrel);
                            $em->flush();

                            // Send sms to all vendors
                            $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                            if($accountArray) {
                                // Send sms to mobile number provided.
                                $vendorName    = $accountArray->getContactname();
                                $smsphone      = $accountArray->getSmsphone();
                                $smsphoneArray = explode("-",$smsphone);
                                $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                                $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                                $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                                /*End of sms to vendors*/

                                // Send email notification to all vendors
                                $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                                if($vendorUser) {
                                   $vendorEmail = $vendorUser->getEmail();
                                   $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                                }
                            }
                        }
                    }
                } else {
                    // This means that vendor is manually mapped by admin
                    $vendenqArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Vendorenquiryrel')->findBy(array('vendorid'=>$vendorid, 'enquiryid'=>$enquiryid));
                    if(empty($vendenqArray)) {

                        // Send sms to all manual mapped vendors
                        $accountArray = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:Account')->findOneBy(array('userid'=>$vendorid));
                        if($accountArray) {
                            // Send sms to mobile number provided.
                            $vendorName    = $accountArray->getContactname();
                            $smsphone      = $accountArray->getSmsphone();
                            $smsphoneArray = explode("-",$smsphone);
                            $smsphone      = $smsphoneArray[0].$smsphoneArray[1];

                            $vendorSMSText = $extraInfoSMS."Please reply BID ".$enquiryid." for the customer information";
                            $smsresponse = $this->sendSMSToPhone($smsphone, $vendorSMSText,'Longtext');
                            /*End of sms to vendors*/

                            // Send email notification to all manual mapped vendors
                            $vendorUser = $this->getDoctrine()->getRepository('BBidsBBidsHomeBundle:User')->findOneBy(array('id'=>$vendorid));
                            if($vendorUser) {
                               $vendorEmail = $vendorUser->getEmail();
                               $this->sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$emailBody,$vendorName);
                            }
                        }
                    }
                }

            }
        }
    }

    protected function setNewActivity($city,$categoryname,$userid)
    {
        $activity = new Activity();
        $created  = new \DateTime();

        $activity->setMessage('New Enquiry');
        $activity->setCity($city);
        $activity->setCategory($categoryname);
        $activity->setAuthorid($userid);
        $activity->setCreated($created);
        $activity->setType(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($activity);
        $em->flush();
    }

    protected function sendVendorNofifyEmail($vendorid,$vendorEmail,$enquiryid,$body,$vendorName)
    {

        $body = str_replace("Dear Vendor,", "Dear $vendorName", $body);
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($vendorid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($vendorEmail);
        $useremail->setEmailsubj('JOB REQUEST '.$enquiryid);
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'JOB REQUEST '.$enquiryid;


        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $vendorEmail,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendEnquiryConfirmationEmail($userid,$email,$enquiryid)
    {
        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:enquiry_confirmation_email.html.twig',array('enquiryid'=>$enquiryid));
        $em = $this->getDoctrine()->getManager();

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('BusinessBid Network Enquiry Successfully Placed');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'BusinessBid Network Enquiry Successfully Placed';


        $json_string = array(
          'to' => array(
            $email,'rachelle@businessbid.ae','greeth@businessbid.ae','management@businessbid.ae'
          ),
          'category' => 'test_category'
        );
        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function sendWelcomeEmail($name,$userid,$email,$password)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $this->renderView('BBidsBBidsHomeBundle:Emailtemplates/Customer:inline_activationemail.html.twig', array('userid'=>$userid,'name'=>$name,'email'=>$email,'password'=>$password));

        $useremail = new Usertoemail();

        $useremail->setFromuid(11);
        $useremail->setTouid($userid);
        $useremail->setFromemail('infosupport@businessbid.ae');
        $useremail->setToemail($email);
        $useremail->setEmailsubj('Welcome to the BusinessBid Network');
        $useremail->setCreated(new \Datetime());
        $useremail->setEmailmessage($body);
        $useremail->setEmailtype(1);
        $useremail->setStatus(1);
        $useremail->setReadStatus(0);

        $em->persist($useremail);
        $em->flush();

        $url     = 'https://api.sendgrid.com/';
        $user    = 'businessbid';
        $pass    = '!3zxih1x0';
        $subject = 'Welcome to the BusinessBid Network';

        $message = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'to'        => $email,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body,
                    'from'      => 'support@businessbid.ae',
                );


        $request =  $url.'api/mail.send.json';


        $sess = curl_init($request);
        curl_setopt ($sess, CURLOPT_POST, true);
        curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
        curl_setopt($sess, CURLOPT_HEADER,false);
        curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($sess);
        curl_close($sess);
    }

    protected function generateHash($length)
    {
        $characters   = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for($i=0; $i< $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    protected function sendSMSToPhone($phoneNumber, $text, $type='')
    {

        //$phoneNumber = substr($phoneNumber, 1);
        $subphone = substr($phoneNumber, 1);
        $smsphone = "971".$subphone;

         if(strpos($text, 'for the customer information'))
            $senderid = 3579;
        else
            $senderid = 'Businessbids';

        $smsJson = '{
                    "authentication": {
                        "username": "agefilms",
                        "password": "12345Uae"
                    },
                    "messages": [
                        {
                            "sender": "'.$senderid.'",
                            "text": "'.$text.'",';
        if($type) {
            $smsJson .= '"type":"longSMS",';
        }
        $smsJson .= '
                            "recipients": [
                                {
                                    "gsm": "'.$smsphone.'"
                                }
                            ]
                        }
                    ]
                }';


        $encodeSMS = base64_encode(json_encode($smsJson));
        $ch = curl_init('http://api.infobip.com/api/v3/sendsms/json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $smsJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: api.infobip.com',
            'Accept: */*',
            'Content-Length: ' . strlen($smsJson))
        );

        $result = curl_exec($ch);
        // $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // echo "status : ".$http_status;
         // echo 'response: '.$result;
        $response        = new Response($result);
        $encodedResponse = base64_encode(json_encode($response));

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $sentSMS = $connection->prepare("INSERT INTO `bbids_sent_sms_log` (`id`, `txn_id`, `sent_text`, `resonse`, `posted_date`) VALUES (NULL, $phoneNumber, '$encodeSMS', '$encodedResponse', NOW())");
        $sentSMS->execute();

        return $response;
    }

    protected function reCaptchaValidation($challengeField,$responseField)
    {
        require_once('/var/www/html/web/gocaptcha/recaptchalib.php');
        $privatekey = '6LcCrQMTAAAAAIz-hCCncUjsw9SpGtvF0CZDimeh';
        $resp = recaptcha_check_answer ($privatekey,
                        $_SERVER["REMOTE_ADDR"],
                        $challengeField,
                        $responseField);

        if ($resp->is_valid) {
            return 'Valid';
        } else {
            # set the error code so that we can display it
           $error = $resp->error;
           return $error;
        }
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}

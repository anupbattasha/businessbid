<?php
namespace BBids\BBidsHomeBundle\Controller;

use BBids\BBidsHomeBundle\Controller\BaseCategoryController;

class CategoryController extends BaseCategoryController
{
    public function validateField($field, $fieldOther = '', $fieldOtherFlag, $multiple)
    {
        if($multiple)
        {
            $counter = count($field);
            if($counter == 0) {
                return ['', 'Select at least one option', false];
            }

            $fieldResult = implode(', ', $field);

            if(strpos($fieldResult,'Other') AND $fieldOtherFlag) {
                if(empty($fieldOther)) {
                    return ['', 'Please fill the other text box', false];
                }
                $fieldResult = (strpos($fieldResult,'Other')) ? (str_replace('Other', $fieldOther, $fieldResult)) : $fieldResult;
                return [$fieldResult,'',true];
            }

            return [$fieldResult,'',true];

        } else {
            if(empty($field)) {
                return ['', 'Select at least one option', false];
            }
            $fieldResult = $field;
            if(($fieldResult == 'Other' OR $fieldResult == 'Multiple') AND ($fieldOtherFlag)) {
                if(empty($fieldOther)) {
                    return ['', 'Please fill the other text box', false];
                }
                $fieldResult = $fieldOther;
            }
            return [$fieldResult,'',true];
        }
    }

    public function educationLearningCategory($form)
    {
        $subcategoryids = $form['subcategories'];
        $extraFieldsarray = [];
        foreach ($subcategoryids as $subcategoryid) {
            switch ($subcategoryid) {
                case '1061' :
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['acedemics'], $form['acedemics_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Acedemics'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['subjects'], $form['subjects_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Subjects'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1062':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['music_type'], $form['music_type_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Music'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1063':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['dan_sing_type'], $form['dan_sing_type_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Singing'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1064':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['dan_sing_type'], $form['dan_sing_type_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Dancing'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1065':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['art_draw_type'], $form['art_draw_type_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Art and Drawing'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
            }
        }

        $onsite = $form['onsite'];
        if($onsite == '') {
            return [$extraFieldsarray, false, 'Select onsite'];
        } else {
            $extraFieldsarray['Onsite'] = $onsite;
        }

        $travel = $form['travel'];
        if($travel == '') {
            return [$extraFieldsarray, false, 'Select travel'];
        } else {
            $extraFieldsarray['Travel'] = $travel;
        }

        $frequency = $form['frequency'];
        if($frequency == '') {
            return [$extraFieldsarray, false, 'Select frequency'];
        } else {
            $extraFieldsarray['Frequency'] = $frequency;
        }

        return [$extraFieldsarray, true, ''];
    }

    public function itSupportCategory($form)
    {
        $subcategoryids = $form['subcategories'];
        $extraFieldsarray = [];
        foreach ($subcategoryids as $subcategoryid) {
            switch ($subcategoryid) {
                case '1076' :
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['automation'], $form['automation_other'], true, false);
                    if($fieldResponse) {
                        $extraFieldsarray['Automation'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['automation_services'], $form['automation_services_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Automation Services'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '990':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['app_os'], $form['app_os_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['OS'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['app_device'], $form['app_device_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['App device'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1077':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['security_systems'], $form['security_systems_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Security systems'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1078':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['access_control'], $form['access_control_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Access control'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '1079':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['website_development'], $form['website_development_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Website development'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
            }
        }

        $people = $form['people'];
        if($people == '') {
            return [$extraFieldsarray, false, 'Select people'];
        } else {
            $extraFieldsarray['People'] = $people;
        }

        $often = $form['often'];
        if($often == '') {
            return [$extraFieldsarray, false, 'Select often'];
        } else {
            $extraFieldsarray['Frequency'] = $often;
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['business'], $form['business_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Business'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        return [$extraFieldsarray, true, ''];
    }


    public function interiorDesignersFitoutCategory($form)
    {
        $subcategoryids = $form['subcategories'];
        $extraFieldsarray = [];
        foreach ($subcategoryids as $subcategoryid) {
            switch ($subcategoryid) {
                case '1080':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['fitout_construction'], $form['fitout_construction_other'], true, true);
                    if($fieldResponse) {
                        $extraFieldsarray['Construction'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
            }
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['dimensions'], $form['dimensions_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Dimensions of property'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['property_type'], $form['property_type_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Type of property'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        return [$extraFieldsarray, true, ''];
    }

    public function movingStorageCategory($form)
    {
        $subcategoryids = $form['subcategories'];
        $extraFieldsarray = [];
        foreach ($subcategoryids as $subcategoryid) {
            switch ($subcategoryid) {
                case '839' :
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['moving_from'], '', false, false);
                    if($fieldResponse) {
                        $extraFieldsarray['Moving from'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['moving_to'], '', false, false);
                    if($fieldResponse) {
                        $extraFieldsarray['Moving to'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '840':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['moving_from'], '', false, false);
                    if($fieldResponse) {
                        $extraFieldsarray['Moving from'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['moving_to'], '', false, false);
                    if($fieldResponse) {
                        $extraFieldsarray['Moving to'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
                case '841':
                    list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['duration'], $form['duration_other'], true, false);
                    if($fieldResponse) {
                        $extraFieldsarray['Storage for'] = $fieldResult;
                    } else {
                        return [$extraFieldsarray, false, $fieldErrorMsg];
                    }
                break;
            }
        }

        $packing = $form['packing'];
        if($packing == '') {
            return [$extraFieldsarray, false, 'Select packing'];
        } else {
            $extraFieldsarray['Packing'] = $packing;
        }

        $usage = $form['usage'];
        if($usage == '') {
            return [$extraFieldsarray, false, 'Select usage'];
        } else {
            $extraFieldsarray['Usage type'] = $usage;
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['premises'], $form['premises_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Premises size'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        return [$extraFieldsarray, true, ''];
    }

    public function sportsSwimmingCategory($form)
    {
        $extraFieldsarray = [];

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['coaching_for'], $form['coaching_for_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Coaching for'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['coaching_type'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Coaching Type'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['trainee_age'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Trainee Age'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['frequency'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Frequency'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['travel'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Willing to travel'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        return [$extraFieldsarray, true, ''];
    }

    public function personalFitnessTrainer($form)
    {
        $extraFieldsarray = [];

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['coaching_units'], $form['coaching_units_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Coaching for'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['coaching_age_range'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Coaching age'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['frequency'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Frequency'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['travel'], '', false, false);
        if($fieldResponse) {
            $extraFieldsarray['Willing to travel'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        return [$extraFieldsarray, true, ''];
    }

    public function petServices($form)
    {
        $extraFieldsarray = [];

        $eventPlaned = $form['event_planed'];
        if($eventPlaned == '') {
            return [$extraFieldsarray, false, 'Select event date'];
        } else {
            $extraFieldsarray['Event'] = $eventPlaned;
        }

        list($fieldResult, $fieldErrorMsg, $fieldResponse) = $this->validateField($form['service_hours'], $form['service_hours_other'], true, false);
        if($fieldResponse) {
            $extraFieldsarray['Service hours'] = $fieldResult;
        } else {
            return [$extraFieldsarray, false, $fieldErrorMsg];
        }

        $petType = $form['pet_type'];
        if($petType == '') {
            return [$extraFieldsarray, false, 'Select pet type'];
        } else {
            $extraFieldsarray['Pet'] = $petType;
        }

        $petBreed = $form['pet_breed'];
        if($petBreed == '') {
            return [$extraFieldsarray, false, 'Select pet breed'];
        } else {
            $extraFieldsarray['Pet breed'] = $petBreed;
        }

        $petAge = $form['pet_age'];
        if($petAge == '') {
            return [$extraFieldsarray, false, 'Select pet age'];
        } else {
            $extraFieldsarray['Pet age'] = $petAge;
        }

        $frequency = $form['frequency'];
        if($frequency == '') {
            return [$extraFieldsarray, false, 'Select service frequency'];
        } else {
            $extraFieldsarray['Frequency'] = $frequency;
        }

        return [$extraFieldsarray, true, ''];
    }
}/*End of class*/

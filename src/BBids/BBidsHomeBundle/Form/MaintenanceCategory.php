<?php
// src/BBids/BBidsHomeBundle/Form/MaintenanceCategory.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MaintenanceCategory extends AbstractType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $handyman       = array('Furniture Assembly'=>'Furniture Assembly','Upholstery Install & Repair'=>'Upholstery Install & Repair','Wallpaper Install & Repair'=>'Wallpaper Install & Repair','Carpet Install & Repair'=>'Carpet Install & Repair','Other'=>'Other');
        $appliances     = array('Oven & Stove Repair'=>'Oven & Stove Repair','Washing Machine Repair'=>'Washing Machine Repair','Refrigerator Repair'=>'Refrigerator Repair','Dishwasher Repair'=>'Dishwasher Repair','Other'=>'Other');
        $plumbing       = array('Blocked Drains'=>'Blocked Drains','Hotwater Install & Repair'=>'Hotwater Install & Repair','Gas Fittings Install & Repair'=>'Gas Fittings Install & Repair','Bathroom Tiling & Resurfacing'=>'Bathroom Tiling & Resurfacing','Toilet & Bathroom Repairs'=>'Toilet & Bathroom Repairs','Waterproofing Install & Repair'=>'Waterproofing Install & Repair','Other'=>'Other');
        $painting       = array('Outdoor Painting'=>'Outdoor Painting','Indoor Painting'=>'Indoor Painting','Other'=>'Other');
        $electrical     = array('Outlets & Fixtures'=>'Outlets & Fixtures','Wiring & Panel Upgrade'=>'Wiring & Panel Upgrade','Alarms & Smoke Detectors'=>'Alarms & Smoke Detectors','Powerpoints & Switches'=>'Powerpoints & Switches','Maintenance & Testing'=>'Maintenance & Testing','Other'=>'Other');
        $kindofProperty = array('Residential'=>'Residential','Commercial'=>'Commercial','Industrial'=>'Industrial',"Other"=>"Other");

        $carpentry = array('Install'=>'Install','Repair'=>'Repair');
        $joinery   = array("Doors & Windows"=>"Doors & Windows","Shelves & Cabinets"=>"Shelves & Cabinets","Wardrobes & Storage"=>"Wardrobes & Storage","Other"=>"Other");

        $flooring     = array('Install'=>'Install','Repair'=>'Repair');
        $flooringType = array("Timber or Wooden"=>"Timber or Wooden","Ceramic Tiles"=>"Ceramic Tiles","Marble"=>"Marble","Other"=>"Other");
        $dimensions   = array('0-50 m'=>'0-50 m','50-100 m'=>'50-100 m','100-200 m'=>'100-200 m','200-500 m'=>'200-500 m','500 m+'=>'500+ m',"Other"=>"Other");

        $location   = array('UAE'=>'UAE','International'=>'International');


        $usertype   = ($options['data']['loginFlag']) ? 'Yes' : 'No';
        $reqType   = $options['data']['reqType'];

        $builder->setAction($options['data']['action'])
                ->add('usertype','hidden', array('data'=>$usertype))
                ->add('reqtype','hidden', array('data'=>$reqType))
                ->add('category','hidden', array('data'=>$options['data']['categoryid']))
                ->add('subcategory','choice', array('choices'=>$options['data']['subcategory'], 'multiple'=>TRUE, 'expanded'=>TRUE,  'label'=>'Select a subcategory', 'empty_value'=>'Choose subcategories', 'empty_data'=>null,   'required'=>TRUE))

                ->add('handyman','choice', array('choices'=>$handyman, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('handyman_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('appliances','choice', array('choices'=>$appliances, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('appliances_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('painting','choice', array('choices'=>$painting, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('painting_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('plumbing','choice', array('choices'=>$plumbing, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('plumbing_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('electrical','choice', array('choices'=>$electrical, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('electrical_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('carpentry','choice', array('choices'=>$carpentry, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('joinery','choice', array('choices'=>$joinery, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('joinery_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('flooring','choice', array('choices'=>$flooring, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('flooring_type','choice', array('choices'=>$flooringType, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('flooring_type_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('property_type','choice', array('choices'=>$kindofProperty, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'attr' => array('class' => 'form-control')))
                ->add('property_type_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))

                ->add('dimensions','choice', array('choices'=>$dimensions, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'attr' => array('class' => 'form-control')))
                ->add('dimensions_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control')))



                ->add('locationtype','choice', array('choices'=>$location, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('city','choice', array('label'=>'Choose your City*', 'choices'=>$options['data']['cityArray'], 'empty_value'=>'Choose your City', 'empty_data'=>null,'required'=>FALSE,'data' => $options['data']['selectedCity']))
                ->add('location', 'text', array('label'=>'Specify an Area*','required'=>FALSE,'attr' => array('class' => 'form-control','maxlength' => 50)))

                ->add('country', 'text', array('label'=>'Enter your Country*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter Country Name','maxlength' => 50)))
                ->add('cityint', 'text', array('label'=>'Enter your City*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter City Name','maxlength' => 50)));

                if(!$options['data']['loginFlag']) {
                    $builder->add('contactname','text', array('label'=>'Enter Full Name*', 'attr' => array('class' => 'form-control','maxlength' => 30)))
                    ->add('email','email', array('label'=>'Enter E-mail*', 'attr' => array('class' => 'form-control','maxlength' => 50)))


                    ->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Enter Mobile Number*','empty_value'=>'', 'required'=>FALSE))
                    ->add('mobile', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 7),'required'=>FALSE))
                    ->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'label'=>'Enter Phone Number','empty_value'=>'', 'required'=>FALSE))
                    ->add('homephone', 'text', array('label'=>FALSE,  'required'=>FALSE,'attr'=>array('size'=>7, 'placeholder'=>'Phone Number','maxlength' => 7)))


                    ->add('mobilecodeint', 'text', array('data'=>'','label'=>'Enter Mobile Number*',  'required'=>FALSE,'attr'=>array('placeholder'=>'','class' => 'form-control','maxlength' => 30)))
                    ->add('mobileint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 30),'required'=>FALSE))
                    ->add('homecodeint', 'text', array('data'=>'','label'=>'Enter Phone Number',   'required'=>FALSE,'attr'=>array('placeholder'=>'','class' => 'form-control','maxlength' => 30)))
                    ->add('homephoneint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Phone Number','class' => 'form-control','maxlength' => 30),'required'=>FALSE));
                }

            $builder->add('hire','choice', array('choices'=> array('1'=>'Urgent', '2'=>'1-3 days', '3'=>'4-7 days','4'=>'7-14 days','5'=>'Just Planning'),  'label'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('description','textarea', array('required'=>FALSE, 'label'=>FALSE, 'attr'=>array('placeholder'=>'','data-toggle'=>'popover','data-placement'=>'right','class' => 'form-control','maxlength' => 120)))

                ->add('postJob', 'submit', array('attr'=>array('class'=>'btn btn-success')));
    }

    public function getName()
    {
        return 'categories_form';
    }
}

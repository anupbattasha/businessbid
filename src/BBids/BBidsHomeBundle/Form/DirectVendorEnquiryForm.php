<?php
// src/BBids/BBidsHomeBundle/Form/DirectVendorEnquiryForm.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DirectVendorEnquiryForm extends AbstractType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction('/web/stagging/')
            ->add('contactname','text', array('label'=>'NAME*', 'attr' => array('class' => 'form-control','maxlength' => 30,'autocomplete' => 'off')))
            ->add('email','email', array('label'=>'EMAIL*', 'attr' => array('class' => 'form-control','maxlength' => 50,'autocomplete' => 'off')))
            ->add('mobile', 'text', array('label'=>'PHONE*','attr'=>array('size'=>7,'placeholder'=>'Phone Number','class' => 'form-control','maxlength' => 7,'autocomplete' => 'off')))
            ->add('description','textarea', array('label'=>'COMMENTS*', 'attr'=>array('placeholder'=>'','data-toggle'=>'popover','data-placement'=>'right','class' => 'form-control','maxlength' => 120)))

            ->add('postJob', 'submit', array('attr'=>array('class'=>'btn btn-default'),'label'=>'GET FREE QUOTE'));
    }

    public function getName()
    {
        return 'direct_enq_form';
    }
}

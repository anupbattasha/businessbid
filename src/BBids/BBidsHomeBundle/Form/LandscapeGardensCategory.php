<?php
// src/BBids/BBidsHomeBundle/Form/LandscapeGardensCategory.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LandscapeGardensCategory extends AbstractType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyType = array('Residential'=>'Residential', 'Commercial'=>'Commercial', 'Government'=>'Government', 'Other'=>'Other');
        $serviceList  = array('Landscape & Garden Design'=>'Landscape & Garden Design', 'Landscape Construction'=>'Landscape Construction', 'Lawn Mowing'=>'Lawn Mowing', 'Land Clearing & Tree Removal'=>'Land Clearing & Tree Removal', 'Synthetic Turfs'=>'Synthetic Turfs','Garden Maintenance'=>'Garden Maintenance','Garden Flowers & Planting'=>'Garden Flowers & Planting','Other'=>'Other');
        $landscapingService = array('Decking'=>'Decking','Pergola'=>'Pergola','Patio'=>'Patio','Gazebo'=>'Gazebo','Water Features'=>'Water Features','Other'=>'Other');
        $irrigationService = array('Drip Irrigation'=>'Drip Irrigation','Spray Irrigation'=>'Spray Irrigation','Misting Systems'=>'Misting Systems','Sprinkler System'=>'Sprinkler System','Other'=>'Other');
        $swimmingService = array("Design & Install"=>"Design & Install","Repair"=>"Repair","Maintenance"=>"Maintenance","Other"=>"Other");
        $dimensions   = array('0-50 m'=>'0-50 m','50-100 m'=>'50-100 m','100-200 m'=>'100-200 m','200-500 m'=>'200-500 m','500 m+'=>'500 m');

        $location   = array('UAE'=>'UAE','International'=>'International');


        $usertype   = ($options['data']['loginFlag']) ? 'Yes' : 'No';
        $reqType   = $options['data']['reqType'];

        $builder->setAction($options['data']['action'])
                ->add('usertype','hidden', array('data'=>$usertype))
                ->add('reqtype','hidden', array('data'=>$reqType))
                ->add('category','hidden', array('data'=>$options['data']['categoryid']))
                ->add('subcategory','choice', array('choices'=>$options['data']['subcategory'], 'multiple'=>TRUE, 'expanded'=>TRUE,  'label'=>'Select a subcategory', 'empty_value'=>'Choose subcategories', 'empty_data'=>null,   'required'=>TRUE))


                ->add('property_type','choice', array('choices'=>$propertyType, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('property_type_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'class'=>'form-control')))

                ->add('service_list','choice', array('choices'=>$serviceList, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('service_list_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control')))
                // ->add('service_none','choice', array('choices'=>array('none'=>'none'), 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('landscape_service','choice', array('choices'=>$landscapingService, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('landscape_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control')))
                // ->add('landscape_none','choice', array('choices'=>array('none'=>'none'), 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('irrigation','choice', array('choices'=>$irrigationService, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('irrigation_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control')))
                // ->add('irrigation_none','choice', array('choices'=>array('none'=>'none'), 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('swimming','choice', array('choices'=>$swimmingService, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('swimming_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control')))

                ->add('dimensions','choice', array('choices'=>$dimensions, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('locationtype','choice', array('choices'=>$location, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('city','choice', array('label'=>'Choose your City*', 'choices'=>$options['data']['cityArray'], 'empty_value'=>'Choose your City', 'empty_data'=>null,'required'=>FALSE,'data' => $options['data']['selectedCity']))
                ->add('location', 'text', array('label'=>'Specify an Area*','required'=>FALSE,'attr' => array('class' => 'form-control','maxlength' => 50)))

                ->add('country', 'text', array('label'=>'Enter Your Country*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter Country Name','maxlength' => 50)))
                ->add('cityint', 'text', array('label'=>'Enter Your City*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter City Name','maxlength' => 50)));

                if(!$options['data']['loginFlag']) {
                    $builder->add('contactname','text', array('label'=>'Enter Full Name*', 'attr' => array('class' => 'form-control','maxlength' => 30)))
                    ->add('email','email', array('label'=>'Enter E-mail*', 'attr' => array('class' => 'form-control','maxlength' => 50)))


                    ->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Enter Mobile Number*','empty_value'=>'', 'required'=>FALSE))
                    ->add('mobile', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 7),'required'=>FALSE))
                    ->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'label'=>'Enter Phone Number','empty_value'=>'', 'required'=>FALSE))
                    ->add('homephone', 'text', array('label'=>FALSE,  'required'=>FALSE,'attr'=>array('size'=>7, 'placeholder'=>'Phone Number','maxlength' => 7)))


                    ->add('mobilecodeint', 'text', array('data'=>'','label'=>'Enter Mobile Number*',  'required'=>FALSE,'attr'=>array('placeholder'=>'','class' => 'form-control','maxlength' => 30)))
                    ->add('mobileint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 30),'required'=>FALSE))
                    ->add('homecodeint', 'text', array('data'=>'','label'=>'Enter Phone Number',   'required'=>FALSE,'attr'=>array('placeholder'=>'','class' => 'form-control','maxlength' => 30)))
                    ->add('homephoneint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Phone Number','class' => 'form-control','maxlength' => 30),'required'=>FALSE));
                }

            $builder->add('hire','choice', array('choices'=> array('1'=>'Urgent', '2'=>'1-3 days', '3'=>'4-7 days','4'=>'7-14 days','6'=>'Flexible Dates'),  'label'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('description','textarea', array('required'=>FALSE, 'label'=>FALSE, 'attr'=>array('placeholder'=>'','data-toggle'=>'popover','data-placement'=>'right','class' => 'form-control','maxlength' => 120)))

                ->add('postJob', 'submit', array('attr'=>array('class'=>'btn btn-success')));
    }

    public function getName()
    {
        return 'categories_form';
    }
}

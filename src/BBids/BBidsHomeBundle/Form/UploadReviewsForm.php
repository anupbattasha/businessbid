<?php
// src/BBids/BBidsHomeBundle/Form/UploadReviewsForm.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UploadReviewsForm extends AbstractType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $vendorsArray = array();
        $builder->add('customername', 'text', array('label'=>'Enter Customer Name*','attr' => array('class' => 'form-control','placeholder'=>'Start typing customer name..','maxlength' => 50,'autocomplete' => 'off')))
                ->add('uid','hidden')
                ->add('vendorname', 'choice', array('choices'=>$vendorsArray, 'multiple'=>false, 'expanded'=>false, 'empty_data'=>null, 'empty_value'=>'Choose Customer Name'))
                ->add('enquiry', 'choice', array('choices'=>$vendorsArray, 'multiple'=>false, 'expanded'=>false, 'empty_data'=>null, 'empty_value'=>'Choose Vendor First'))
                ->add('write', 'submit',array('attr'=>array('class'=>'btn btn-success')));
    }

    function getName()
    {
        return 'uploadreviews_form';
    }
}

<?php
// src/BBids/BBidsHomeBundle/Form/EventManagmentCategory.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EventManagmentCategory extends AbstractType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$services = array('Music'=>'Music','Entertainers'=>'Entertainers','Master of Ceremony (MC)'=>'Master of Ceremony (MC)','Models'=>'Models','Hosts & Hostesses'=>'Hosts & Hostesses','Waiting Staff'=>'Waiting Staff');*/

        $eventType     = array('Corporate Event'=>'Corporate Event','Birthday'=>'Birthday','Private Event'=>'Private Event','Other'=>'Other');
        $duration      = array('Single Day'=>'Single Day','Multiple Days'=>'Multiple Days');
        $musicServices = array('DJ'=>'DJ','Singer'=>'Singer','Live Band'=>'Live Band','Dancers'=>'Dancers','Other'=>'Other');
        $entertainers  = array('Live Performance'=>'Live Performance','Stand-Up Comedian'=>'Stand-Up Comedian','Magician'=>'Magician','Clown'=>'Clown','Caricaturist'=>'Caricaturist','Face Painter'=>'Face Painter','Balloon Artist'=>'Balloon Artist','Bubble Artist'=>'Bubble Artist','Other'=>'Other');
        $models        = array('2-5'=>'2-5','5-10'=>'5-10','10-20'=>'10-20','20+'=>'20+');
        $hosts         = array('2-5'=>'2-5','5-10'=>'5-10','10-20'=>'10-20','20+'=>'20+');
        $staff         = array('2-5'=>'2-5','5-10'=>'5-10','10-20'=>'10-20','20+'=>'20+');
        $guests        = array('25-50'=>'25-50','50-75'=>'50-75','75-100'=>'75-100','100-150'=>'100-150','150-200'=>'150-200','200+'=>'200+');

        $location   = array('UAE'=>'UAE','International'=>'International');


        $usertype   = ($options['data']['loginFlag']) ? 'Yes' : 'No';
        $reqType   = $options['data']['reqType'];

        $builder->setAction($options['data']['action'])
                ->add('usertype','hidden', array('data'=>$usertype))
                ->add('reqtype','hidden', array('data'=>$reqType))
                ->add('category','hidden', array('data'=>$options['data']['categoryid']))
                ->add('subcategory','choice', array('choices'=>$options['data']['subcategory'], 'multiple'=>TRUE, 'expanded'=>TRUE,  'label'=>'Select a subcategory', 'empty_value'=>'Choose subcategories', 'empty_data'=>null))
                ->add('subcategory_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control','placeholder'=>'other')))

                ->add('event_services','choice', array('choices'=>$eventType, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,'attr' => array('class' => 'form-control')))
                ->add('event_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control','placeholder'=>'other')))


                ->add('event_planed', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('maxlength' => 50,'class'=>'form-control calender','autocomplete' => 'off')))

                ->add('duration','choice', array('choices'=>$duration, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('music_services','choice', array('choices'=>$musicServices, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('music_services_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')))

                ->add('entertainers','choice', array('choices'=>$entertainers, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('entertainers_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')))

                ->add('models','choice', array('choices'=>$models, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('models_none','choice', array('choices'=>array('Undecided'=>'Undecided'), 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('hosts','choice', array('choices'=>$hosts, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('hosts_none','choice', array('choices'=>array('Undecided'=>'Undecided'), 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('staff','choice', array('choices'=>$staff, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>FALSE,'attr' => array('class' => 'form-control')))

                ->add('guests','choice', array('choices'=>$guests, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null, 'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('locationtype','choice', array('choices'=>$location, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('city','choice', array('label'=>'Choose your City*', 'choices'=>$options['data']['cityArray'], 'empty_value'=>'Choose your City', 'empty_data'=>null,'required'=>FALSE,'data' => $options['data']['selectedCity']))
                ->add('location', 'text', array('label'=>'Specify an Area*','required'=>FALSE,'attr' => array('class' => 'form-control','maxlength' => 50,'autocomplete' => 'off')))

                ->add('country', 'text', array('label'=>'Enter your Country*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter Country Name','maxlength' => 50,'autocomplete' => 'off')))
                ->add('cityint', 'text', array('label'=>'Enter your City*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter City Name','maxlength' => 50,'autocomplete' => 'off')));

                if(!$options['data']['loginFlag']) {
                    $builder->add('contactname','text', array('label'=>'Enter Full Name*', 'attr' => array('class' => 'form-control','maxlength' => 30,'autocomplete' => 'off')))
                    ->add('email','email', array('label'=>'Enter E-mail*', 'attr' => array('class' => 'form-control','maxlength' => 50,'autocomplete' => 'off')))


                    ->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Enter Mobile Number*','empty_value'=>'', 'required'=>FALSE))
                    ->add('mobile', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 7,'autocomplete' => 'off'),'required'=>FALSE))
                    ->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'label'=>'Enter Phone Number','empty_value'=>'', 'required'=>FALSE))
                    ->add('homephone', 'text', array('label'=>FALSE,  'required'=>FALSE,'attr'=>array('size'=>7, 'placeholder'=>'Phone Number','maxlength' => 7,'autocomplete' => 'off')))


                    ->add('mobilecodeint', 'text', array('data'=>'','label'=>'Enter Mobile Number*',  'required'=>FALSE,'attr'=>array('class' => 'form-control','maxlength' => 30,'autocomplete' => 'off')))
                    ->add('mobileint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 30,'autocomplete' => 'off'),'required'=>FALSE))
                    ->add('homecodeint', 'text', array('data'=>'','label'=>'Enter Phone Number',   'required'=>FALSE,'attr'=>array('class' => 'form-control','maxlength' => 30,'autocomplete' => 'off')))
                    ->add('homephoneint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Phone Number','class' => 'form-control','maxlength' => 30,'autocomplete' => 'off'),'required'=>FALSE));
                }

            $builder->add('hire','choice', array('choices'=> array('1'=>'Urgent', '2'=>'1-3 days', '3'=>'4-7 days','4'=>'7-14 days','5'=>'Just Planning'),  'label'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('description','textarea', array('required'=>FALSE, 'label'=>FALSE, 'attr'=>array('placeholder'=>'','data-toggle'=>'popover','data-placement'=>'right','class' => 'form-control','maxlength' => 120)))

                ->add('postJob', 'submit', array('attr'=>array('class'=>'btn btn-success')));
    }

    public function getName()
    {
        return 'categories_form';
    }
}


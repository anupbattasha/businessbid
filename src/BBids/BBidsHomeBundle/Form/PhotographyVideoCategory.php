<?php
// src/BBids/BBidsHomeBundle/Form/photographyVideoCategory.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PhotographyVideoCategory extends AbstractType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $eventService   = array('Wedding'=>'Wedding','Product'=>'Product','Graduation'=>'Graduation','Fashion'=>'Fashion','Baby'=>'Baby','Landscape'=>'Landscape','Portrait / Headshot'=>'Portrait / Headshot','Product'=>'Product','Life Event'=>'Life Event','Corporate / Business'=>'Corporate / Business','Birthday'=>'Birthday', 'Other'=>'Other');

        $serviceType = array('Indoor'=>'Indoor','Studio'=>'Studio','Outdoor'=>'Outdoor', 'Other'=>'Other');

        $imgVideoUsage = array('Editorial / Magazine'=>'Editorial / Magazine','Website / Digital'=>'Website / Digital','Personal Use'=>'Personal Use','Print / Catalogue'=>'Print / Catalogue','Large Format Prints'=>'Large Format Prints', 'Other'=>'Other');

        $imgVideoFormat = array('Digital or Online' => 'Digital or Online', 'Album'=>'Album', 'CD/DVD'=>'CD/DVD', 'Physical Prints'=>'Physical Prints', 'Other'=>'Other');

        $location   = array('UAE'=>'UAE','International'=>'International');


        $usertype   = ($options['data']['loginFlag']) ? 'Yes' : 'No';
        $reqType   = $options['data']['reqType'];

        $builder->setAction($options['data']['action'])
                ->add('usertype','hidden', array('data'=>$usertype))
                ->add('reqtype','hidden', array('data'=>$reqType))
                ->add('category','hidden', array('data'=>$options['data']['categoryid']))
                ->add('subcategory','choice', array('choices'=>$options['data']['subcategory'], 'multiple'=>TRUE, 'expanded'=>TRUE,  'label'=>'Select a subcategory', 'empty_value'=>'Choose subcategories', 'empty_data'=>null,   'required'=>TRUE))

                ->add('event_planed', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('maxlength' => 50,'class'=>'form-control calender','autocomplete' => 'off')))
                ->add('event_service','choice', array('choices'=>$eventService, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('event_service_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control','placeholder'=>'other')))
                ->add('service_type','choice', array('choices'=>$serviceType, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('service_type_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control','placeholder'=>'other')))
                ->add('usage','choice', array('choices'=>$imgVideoUsage, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('usage_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control','placeholder'=>'other')))
                ->add('format','choice', array('choices'=>$imgVideoFormat, 'multiple'=>TRUE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))
                ->add('format_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other', 'maxlength' => 30,'class'=>'form-control','placeholder'=>'other')))

                ->add('locationtype','choice', array('choices'=>$location, 'multiple'=>FALSE, 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,   'required'=>TRUE,'attr' => array('class' => 'form-control')))

                ->add('city','choice', array('label'=>'Choose your City*', 'choices'=>$options['data']['cityArray'], 'empty_value'=>'Choose your City', 'empty_data'=>null,'required'=>FALSE,'data' => $options['data']['selectedCity']))
                ->add('location', 'text', array('label'=>'Specify an Area*','required'=>FALSE,'attr' => array('class' => 'form-control','maxlength' => 50)))

                ->add('country', 'text', array('label'=>'Enter your Country*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter Country Name','maxlength' => 50)))
                ->add('cityint', 'text', array('label'=>'Enter your City*','required'=>FALSE,'attr' => array('class' => 'form-control','placeholder'=>'Enter City Name','maxlength' => 50)));

                if(!$options['data']['loginFlag']) {
                    $builder->add('contactname','text', array('label'=>'Enter Full Name*', 'attr' => array('maxlength' => 30)))
                    ->add('email','email', array('label'=>'Enter E-mail*', 'attr' => array('maxlength' => 50)))


                    ->add('mobilecode','choice',array('choices'=>array('050'=>'050' ,'052'=>'052' ,'055'=>'055','056'=>'056'),'label'=>'Enter Mobile Number*','empty_value'=>'', 'required'=>FALSE))
                    ->add('mobile', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 7),'required'=>FALSE))
                    ->add('homecode','choice',array('choices'=>array('02'=>'02' ,'03'=>'03','04'=>'04','06'=>'06','07'=>'07','09'=>'09'),'label'=>'Enter Phone Number','empty_value'=>'', 'required'=>FALSE))
                    ->add('homephone', 'text', array('label'=>FALSE,  'required'=>FALSE,'attr'=>array('size'=>7, 'placeholder'=>'Phone Number','maxlength' => 7)))


                    ->add('mobilecodeint', 'text', array('data'=>'','label'=>'Enter mobile Number*',  'required'=>FALSE,'attr'=>array('placeholder'=>'','class' => 'form-control','maxlength' => 30)))
                    ->add('mobileint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Mobile Number','class' => 'form-control','maxlength' => 30),'required'=>FALSE))
                    ->add('homecodeint', 'text', array('data'=>'','label'=>'Enter Phone Number',   'required'=>FALSE,'attr'=>array('placeholder'=>'','class' => 'form-control','maxlength' => 30)))
                    ->add('homephoneint', 'text', array( 'attr'=>array('size'=>7,'placeholder'=>'Phone Number','class' => 'form-control','maxlength' => 30),'required'=>FALSE));
                }

            $builder->add('hire','choice', array('choices'=> array('1'=>'Urgent', '2'=>'1-3 days', '3'=>'4-7 days','4'=>'7-14 days','5'=>'Just Planning'),  'label'=>FALSE,'attr' => array('class' => 'form-control')))
                ->add('description','textarea', array('required'=>FALSE, 'label'=>FALSE, 'attr'=>array('placeholder'=>'','data-toggle'=>'popover','data-placement'=>'right','class' => 'form-control','maxlength' => 120)))

                ->add('postJob', 'submit', array('attr'=>array('class'=>'btn btn-success')));
    }

    public function getName()
    {
        return 'categories_form';
    }
}

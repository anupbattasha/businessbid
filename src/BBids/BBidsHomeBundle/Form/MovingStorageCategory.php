<?php
// src/BBids/BBidsHomeBundle/Form/MovingStorageCategory.php

namespace BBids\BBidsHomeBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MovingStorageCategory extends BaseType {

    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->remove('subcategory_other');
        $builder->add('event_planed', 'text', array('label'=>FALSE, 'attr'=>array('maxlength' => 50,'class'=>'form-control calender','autocomplete' => 'off')));

        $elementTypes = $this->getElementTypes();

        foreach ($this->formConfig as $key => $field) {
            $fieldType = $field['elementTypeId'];
            if($fieldType == 2)
            {
                $fieldOptions = $this->getFormValues($field['id']);
                $builder->add($field['caption'], $elementTypes[$fieldType-1], array('choices'=>$fieldOptions, 'multiple'=>$field['multipleSelection'], 'expanded'=>TRUE, 'empty_value'=>null, 'empty_data'=>null,'required'=>TRUE,'attr' => array('class' => 'form-control')));
                if($field['extraOptions'] == 'yes')
                    $builder->add($field['caption'].'_other', 'text', array('required'=>FALSE,'label'=>FALSE, 'attr'=>array('placeholder'=>'Other','maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')));
            } else if ($fieldType == 1) {
                $builder->add($field['caption'], 'text', array('label'=>FALSE, 'attr'=>array('placeholder'=>$field['extraOptions'],'maxlength' => 30,'class'=>'form-control','autocomplete' => 'off')));
            }
        }
    }

    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'validation_groups' => false,
        ));
    }


    public function getName()
    {
        return 'categories_form';
    }
}

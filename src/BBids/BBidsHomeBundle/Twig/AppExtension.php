<?php
// src/BBids/BBidsHomeBundle/Twig/AppExtension.php
namespace BBids\BBidsHomeBundle\Twig;

use Symfony\Component\HttpKernel\KernelInterface;

class AppExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            'roundrating' => new \Twig_Function_Method($this, 'roundRating')
        );
    }

    public function roundRating($num)
    {
        if($num >= ($half = ($ceil = ceil($num))- 0.5) + 0.25) return $ceil;
        else if($num < $half - 0.25) return floor($num);
        else return $half;
    }

    public function getName()
    {
        return 'app_extension';
    }
}
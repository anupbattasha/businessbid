<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vendorcertificationrel
 */
class Vendorcertificationrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var string
     */
    private $certification;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Vendorcertificationrel
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set certification
     *
     * @param string $certification
     * @return Vendorcertificationrel
     */
    public function setCertification($certification)
    {
        $this->certification = $certification;

        return $this;
    }

    /**
     * Get certification
     *
     * @return string 
     */
    public function getCertification()
    {
        return $this->certification;
    }
    /**
     * @var string
     */
    private $file;


    /**
     * Set file
     *
     * @param string $file
     * @return Vendorcertificationrel
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
}

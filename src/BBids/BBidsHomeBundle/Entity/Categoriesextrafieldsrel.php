<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Categoriesextrafieldsrel
 */
class Categoriesextrafieldsrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $enquiryID;

    /**
     * @var string
     */
    private $keyName;

    /**
     * @var string
     */
    private $keyValue;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enquiryID
     *
     * @param string $enquiryID
     *
     * @return Categoriesextrafieldsrel
     */
    public function setEnquiryID($enquiryID)
    {
        $this->enquiryID = $enquiryID;

        return $this;
    }

    /**
     * Get enquiryID
     *
     * @return string
     */
    public function getEnquiryID()
    {
        return $this->enquiryID;
    }

    /**
     * Set keyName
     *
     * @param string $keyName
     *
     * @return Categoriesextrafieldsrel
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;

        return $this;
    }

    /**
     * Get keyName
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Set keyValue
     *
     * @param string $keyValue
     *
     * @return Categoriesextrafieldsrel
     */
    public function setKeyValue($keyValue)
    {
        $this->keyValue = $keyValue;

        return $this;
    }

    /**
     * Get keyValue
     *
     * @return string
     */
    public function getKeyValue()
    {
        return $this->keyValue;
    }
}

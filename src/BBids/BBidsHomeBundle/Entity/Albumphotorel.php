<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Albumphotorel
 */
class Albumphotorel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $albumid;

    /**
     * @var string
     */
    private $photoid;

    /**
     * @var string
     */
    private $phototag;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set albumid
     *
     * @param string $albumid
     * @return Albumphotorel
     */
    public function setAlbumid($albumid)
    {
        $this->albumid = $albumid;

        return $this;
    }

    /**
     * Get albumid
     *
     * @return string 
     */
    public function getAlbumid()
    {
        return $this->albumid;
    }

    /**
     * Set photoid
     *
     * @param string $photoid
     * @return Albumphotorel
     */
    public function setPhotoid($photoid)
    {
        $this->photoid = $photoid;

        return $this;
    }

    /**
     * Get photoid
     *
     * @return string 
     */
    public function getPhotoid()
    {
        return $this->photoid;
    }

    /**
     * Set phototag
     *
     * @param string $phototag
     * @return Albumphotorel
     */
    public function setPhototag($phototag)
    {
        $this->phototag = $phototag;

        return $this;
    }

    /**
     * Get phototag
     *
     * @return string 
     */
    public function getPhototag()
    {
        return $this->phototag;
    }
}

<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Leadtransactionhistory
 */
class Leadtransactionhistory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var integer
     */
    private $categoryid;

    /**
     * @var integer
     */
    private $leadcount;

    /**
     * @var integer
     */
    private $newleadcount;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     *
     * @return Leadtransactionhistory
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Leadtransactionhistory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     *
     * @return Leadtransactionhistory
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set categoryid
     *
     * @param integer $categoryid
     *
     * @return Leadtransactionhistory
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set leadcount
     *
     * @param integer $leadcount
     *
     * @return Leadtransactionhistory
     */
    public function setLeadcount($leadcount)
    {
        $this->leadcount = $leadcount;

        return $this;
    }

    /**
     * Get leadcount
     *
     * @return integer
     */
    public function getLeadcount()
    {
        return $this->leadcount;
    }

    /**
     * Set newleadcount
     *
     * @param integer $newleadcount
     *
     * @return Leadtransactionhistory
     */
    public function setNewleadcount($newleadcount)
    {
        $this->newleadcount = $newleadcount;

        return $this;
    }

    /**
     * Get newleadcount
     *
     * @return integer
     */
    public function getNewleadcount()
    {
        return $this->newleadcount;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Leadtransactionhistory
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}

<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ordertotryleadsrel
 */
class Ordertotryleadsrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orderid;

    /**
     * @var string
     */
    private $ordernumber;

    /**
     * @var integer
     */
    private $authorid;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderid
     *
     * @param integer $orderid
     * @return Ordertotryleadsrel
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer 
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * Set ordernumber
     *
     * @param string $ordernumber
     * @return Ordertotryleadsrel
     */
    public function setOrdernumber($ordernumber)
    {
        $this->ordernumber = $ordernumber;

        return $this;
    }

    /**
     * Get ordernumber
     *
     * @return string 
     */
    public function getOrdernumber()
    {
        return $this->ordernumber;
    }

    /**
     * Set authorid
     *
     * @param integer $authorid
     * @return Ordertotryleadsrel
     */
    public function setAuthorid($authorid)
    {
        $this->authorid = $authorid;

        return $this;
    }

    /**
     * Get authorid
     *
     * @return integer 
     */
    public function getAuthorid()
    {
        return $this->authorid;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Ordertotryleadsrel
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}

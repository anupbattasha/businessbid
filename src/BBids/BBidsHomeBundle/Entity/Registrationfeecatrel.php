<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Registrationfeecatrel
 */
class Registrationfeecatrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $categoryid;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryid
     *
     * @param string $categoryid
     *
     * @return Registrationfeecatrel
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return string
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Registrationfeecatrel
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Registrationfeecatrel
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Registrationfeecatrel
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}

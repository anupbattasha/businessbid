<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Categoriesformrel
 */
class Categoriesformrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $categoryID;

    /**
     * @var string
     */
    private $formObjName;

    /**
     * @var string
     */
    private $formAction;

    /**
     * @var string
     */
    private $twigName;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryID
     *
     * @param integer $categoryID
     *
     * @return Categoriesformrel
     */
    public function setCategoryID($categoryID)
    {
        $this->categoryID = $categoryID;

        return $this;
    }

    /**
     * Get categoryID
     *
     * @return integer
     */
    public function getCategoryID()
    {
        return $this->categoryID;
    }

    /**
     * Set formObjName
     *
     * @param string $formObjName
     *
     * @return Categoriesformrel
     */
    public function setFormObjName($formObjName)
    {
        $this->formObjName = $formObjName;

        return $this;
    }

    /**
     * Get formObjName
     *
     * @return string
     */
    public function getFormObjName()
    {
        return $this->formObjName;
    }

    /**
     * Set formAction
     *
     * @param string $formAction
     *
     * @return Categoriesformrel
     */
    public function setFormAction($formAction)
    {
        $this->formAction = $formAction;

        return $this;
    }

    /**
     * Get formAction
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->formAction;
    }

    /**
     * Set twigName
     *
     * @param string $twigName
     *
     * @return Categoriesformrel
     */
    public function setTwigName($twigName)
    {
        $this->twigName = $twigName;

        return $this;
    }

    /**
     * Get twigName
     *
     * @return string
     */
    public function getTwigName()
    {
        return $this->twigName;
    }
    /**
     * @var string
     */
    private $formAjaxAction;


    /**
     * Set formAjaxAction
     *
     * @param string $formAjaxAction
     *
     * @return Categoriesformrel
     */
    public function setFormAjaxAction($formAjaxAction)
    {
        $this->formAjaxAction = $formAjaxAction;

        return $this;
    }

    /**
     * Get formAjaxAction
     *
     * @return string
     */
    public function getFormAjaxAction()
    {
        return $this->formAjaxAction;
    }
}

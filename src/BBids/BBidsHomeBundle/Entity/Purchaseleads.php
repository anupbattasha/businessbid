<?php
// src/BBid/BBidsHomeBundle/Entity/Purchaseleads.php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Purchaseleads
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $purchaseid;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var string
     */
    private $categoryid;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $plan;

    /**
     * @var string
     */
    private $price;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purchaseid
     *
     * @param integer $purchaseid
     * @return Purchaseleads
     */
    public function setPurchaseid($purchaseid)
    {
        $this->purchaseid = $purchaseid;

        return $this;
    }

    /**
     * Get purchaseid
     *
     * @return integer
     */
    public function getPurchaseid()
    {
        return $this->purchaseid;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Purchaseleads
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set categoryid
     *
     * @param string $categoryid
     * @return Purchaseleads
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return string
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Purchaseleads
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set plan
     *
     * @param string $plan
     * @return Purchaseleads
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Purchaseleads
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Purchaseleads
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * @var string
     */
    private $status;


    /**
     * Set status
     *
     * @param string $status
     * @return Purchaseleads
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}

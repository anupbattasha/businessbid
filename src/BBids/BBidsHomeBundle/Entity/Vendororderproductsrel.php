<?php
// src/BBids/BBidsHomeBundle/Entity/Vendororderproductsrel.php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Vendororderproductsrel 
{

    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var integer
     */
    private $categoryid;

    /**
     * @var integer
     */
    private $leadpack;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Vendororderproductsrel
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set categoryid
     *
     * @param integer $categoryid
     * @return Vendororderproductsrel
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set leadpack
     *
     * @param integer $leadpack
     * @return Vendororderproductsrel
     */
    public function setLeadpack($leadpack)
    {
        $this->leadpack = $leadpack;

        return $this;
    }

    /**
     * Get leadpack
     *
     * @return integer 
     */
    public function getLeadpack()
    {
        return $this->leadpack;
    }
}

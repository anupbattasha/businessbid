<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Vendorcategorymapping
 */
class Vendorcategorymapping
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $categoryID;

    /**
     * @var integer
     */
    private $vendorID;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryID
     *
     * @param integer $categoryID
     *
     * @return Vendorcategorymapping
     */
    public function setCategoryID($categoryID)
    {
        $this->categoryID = $categoryID;

        return $this;
    }

    /**
     * Get categoryID
     *
     * @return integer
     */
    public function getCategoryID()
    {
        return $this->categoryID;
    }

    /**
     * Set vendorID
     *
     * @param integer $vendorID
     *
     * @return Vendorcategorymapping
     */
    public function setVendorID($vendorID)
    {
        $this->vendorID = $vendorID;

        return $this;
    }

    /**
     * Get vendorID
     *
     * @return integer
     */
    public function getVendorID()
    {
        return $this->vendorID;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Vendorcategorymapping
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Vendorcategorymapping
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Vendorcategorymapping
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Vendorcategorymapping
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Vendorcategorymapping
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}

<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Vendorlogorel
 */
class Vendorlogorel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var integer
     */
    private $flag;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     *
     * @return Vendorlogorel
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return Vendorlogorel
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     *
     * @return Vendorlogorel
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }
}

<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Categorypagesetup
 */
class Categorypagesetup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $pageTitle;

    /**
     * @var string
     */
    private $pageHeading;

    /**
     * @var string
     */
    private $pageSummary;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pageTitle
     *
     * @param string $pageTitle
     *
     * @return Categorypagesetup
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * Get pageTitle
     *
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * Set pageHeading
     *
     * @param string $pageHeading
     *
     * @return Categorypagesetup
     */
    public function setPageHeading($pageHeading)
    {
        $this->pageHeading = $pageHeading;

        return $this;
    }

    /**
     * Get pageHeading
     *
     * @return string
     */
    public function getPageHeading()
    {
        return $this->pageHeading;
    }

    /**
     * Set pageSummary
     *
     * @param string $pageSummary
     *
     * @return Categorypagesetup
     */
    public function setPageSummary($pageSummary)
    {
        $this->pageSummary = $pageSummary;

        return $this;
    }

    /**
     * Get pageSummary
     *
     * @return string
     */
    public function getPageSummary()
    {
        return $this->pageSummary;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Categorypagesetup
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Categorypagesetup
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Categorypagesetup
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}

<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * FormElements
 */
class FormElements
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $categoryId;

    /**
     * @var integer
     */
    private $elementTypeId;

    /**
     * @var string
     */
    private $caption;

    /**
     * @var string
     */
    private $extraOptions;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return FormElements
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set elementTypeId
     *
     * @param integer $elementTypeId
     *
     * @return FormElements
     */
    public function setElementTypeId($elementTypeId)
    {
        $this->elementTypeId = $elementTypeId;

        return $this;
    }

    /**
     * Get elementTypeId
     *
     * @return integer
     */
    public function getElementTypeId()
    {
        return $this->elementTypeId;
    }

    /**
     * Set caption
     *
     * @param string $caption
     *
     * @return FormElements
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set extraOptions
     *
     * @param string $extraOptions
     *
     * @return FormElements
     */
    public function setExtraOptions($extraOptions)
    {
        $this->extraOptions = $extraOptions;

        return $this;
    }

    /**
     * Get extraOptions
     *
     * @return string
     */
    public function getExtraOptions()
    {
        return $this->extraOptions;
    }
    /**
     * @var boolean
     */
    private $multipleSelection;


    /**
     * Set multipleSelection
     *
     * @param boolean $multipleSelection
     *
     * @return FormElements
     */
    public function setMultipleSelection($multipleSelection)
    {
        $this->multipleSelection = $multipleSelection;

        return $this;
    }

    /**
     * Get multipleSelection
     *
     * @return boolean
     */
    public function getMultipleSelection()
    {
        return $this->multipleSelection;
    }
}

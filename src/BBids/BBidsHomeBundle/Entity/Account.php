<?php
// src/BBid/BBidsHomeBundle/Entity/Account.php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Account
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $profileid;

    /**
     * @var integer
     */
    private $smsphone;

    /**
     * @var string
     */
    private $homephone;

    /**
     * @var string
     */
    private $bizname;

    /**
     * @var string
     */
    private $contactname;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $fax;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Account
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     * @return Account
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer 
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set smsphone
     *
     * @param integer $smsphone
     * @return Account
     */
    public function setSmsphone($smsphone)
    {
        $this->smsphone = $smsphone;

        return $this;
    }

    /**
     * Get smsphone
     *
     * @return integer 
     */
    public function getSmsphone()
    {
        return $this->smsphone;
    }

    /**
     * Set homephone
     *
     * @param string $homephone
     * @return Account
     */
    public function setHomephone($homephone)
    {
        $this->homephone = $homephone;

        return $this;
    }

    /**
     * Get homephone
     *
     * @return string 
     */
    public function getHomephone()
    {
        return $this->homephone;
    }

    /**
     * Set bizname
     *
     * @param string $bizname
     * @return Account
     */
    public function setBizname($bizname)
    {
        $this->bizname = $bizname;

        return $this;
    }

    /**
     * Get bizname
     *
     * @return string 
     */
    public function getBizname()
    {
        return $this->bizname;
    }

    /**
     * Set contactname
     *
     * @param string $contactname
     * @return Account
     */
    public function setContactname($contactname)
    {
        $this->contactname = $contactname;

        return $this;
    }

    /**
     * Get contactname
     *
     * @return string 
     */
    public function getContactname()
    {
        return $this->contactname;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Account
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Account
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }
    /**
     * @var string
     */
    private $description;


    /**
     * Set description
     *
     * @param string $description
     * @return Account
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var string
     */
    private $logopath;


    /**
     * Set logopath
     *
     * @param string $logopath
     * @return Account
     */
    public function setLogopath($logopath)
    {
        $this->logopath = $logopath;

        return $this;
    }

    /**
     * Get logopath
     *
     * @return string 
     */
    public function getLogopath()
    {
        return $this->logopath;
    }
    /**
     * @var string
     */
    private $otc_status;


    /**
     * Set otc_status
     *
     * @param string $otcStatus
     * @return Account
     */
    public function setOtcStatus($otcStatus)
    {
        $this->otc_status = $otcStatus;

        return $this;
    }

    /**
     * Get otc_status
     *
     * @return string 
     */
    public function getOtcStatus()
    {
        return $this->otc_status;
    }
    /**
     * @var string
     */
    private $test_account;


    /**
     * Set testAccount
     *
     * @param string $testAccount
     *
     * @return Account
     */
    public function setTestAccount($testAccount)
    {
        $this->test_account = $testAccount;

        return $this;
    }

    /**
     * Get testAccount
     *
     * @return string
     */
    public function getTestAccount()
    {
        return $this->test_account;
    }
}

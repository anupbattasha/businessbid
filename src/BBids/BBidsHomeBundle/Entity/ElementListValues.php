<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * ElementListValues
 */
class ElementListValues
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $categoryId;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return ElementListValues
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ElementListValues
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var integer
     */
    private $formElementsId;


    /**
     * Set formElementsId
     *
     * @param integer $formElementsId
     *
     * @return ElementListValues
     */
    public function setFormElementsId($formElementsId)
    {
        $this->formElementsId = $formElementsId;

        return $this;
    }

    /**
     * Get formElementsId
     *
     * @return integer
     */
    public function getFormElementsId()
    {
        return $this->formElementsId;
    }
}

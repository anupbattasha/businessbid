<?php

namespace BBids\BBidsHomeBundle\Entity;

/**
 * Vendorservices
 */
class Vendorservices
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var string
     */
    private $category;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     *
     * @return Vendorservices
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Vendorservices
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Vendorservices
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}

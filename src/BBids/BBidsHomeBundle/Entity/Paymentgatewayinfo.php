<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paymentgatewayinfo
 */
class Paymentgatewayinfo
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var string
     */
    private $payAmount;

    /**
     * @var string
     */
    private $payResult;

    /**
     * @var string
     */
    private $payRescode;

    /**
     * @var string
     */
    private $payTranid;

    /**
     * @var string
     */
    private $payAuthcode;

    /**
     * @var string
     */
    private $payError;

    /**
     * @var string
     */
    private $payErrorText;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $payStatus;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     * @return Paymentgatewayinfo
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Paymentgatewayinfo
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Paymentgatewayinfo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set payAmount
     *
     * @param string $payAmount
     * @return Paymentgatewayinfo
     */
    public function setPayAmount($payAmount)
    {
        $this->payAmount = $payAmount;

        return $this;
    }

    /**
     * Get payAmount
     *
     * @return string 
     */
    public function getPayAmount()
    {
        return $this->payAmount;
    }

    /**
     * Set payResult
     *
     * @param string $payResult
     * @return Paymentgatewayinfo
     */
    public function setPayResult($payResult)
    {
        $this->payResult = $payResult;

        return $this;
    }

    /**
     * Get payResult
     *
     * @return string 
     */
    public function getPayResult()
    {
        return $this->payResult;
    }

    /**
     * Set payRescode
     *
     * @param string $payRescode
     * @return Paymentgatewayinfo
     */
    public function setPayRescode($payRescode)
    {
        $this->payRescode = $payRescode;

        return $this;
    }

    /**
     * Get payRescode
     *
     * @return string 
     */
    public function getPayRescode()
    {
        return $this->payRescode;
    }

    /**
     * Set payTranid
     *
     * @param string $payTranid
     * @return Paymentgatewayinfo
     */
    public function setPayTranid($payTranid)
    {
        $this->payTranid = $payTranid;

        return $this;
    }

    /**
     * Get payTranid
     *
     * @return string 
     */
    public function getPayTranid()
    {
        return $this->payTranid;
    }

    /**
     * Set payAuthcode
     *
     * @param string $payAuthcode
     * @return Paymentgatewayinfo
     */
    public function setPayAuthcode($payAuthcode)
    {
        $this->payAuthcode = $payAuthcode;

        return $this;
    }

    /**
     * Get payAuthcode
     *
     * @return string 
     */
    public function getPayAuthcode()
    {
        return $this->payAuthcode;
    }

    /**
     * Set payError
     *
     * @param string $payError
     * @return Paymentgatewayinfo
     */
    public function setPayError($payError)
    {
        $this->payError = $payError;

        return $this;
    }

    /**
     * Get payError
     *
     * @return string 
     */
    public function getPayError()
    {
        return $this->payError;
    }

    /**
     * Set payErrorText
     *
     * @param string $payErrorText
     * @return Paymentgatewayinfo
     */
    public function setPayErrorText($payErrorText)
    {
        $this->payErrorText = $payErrorText;

        return $this;
    }

    /**
     * Get payErrorText
     *
     * @return string 
     */
    public function getPayErrorText()
    {
        return $this->payErrorText;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Paymentgatewayinfo
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set payStatus
     *
     * @param string $payStatus
     * @return Paymentgatewayinfo
     */
    public function setPayStatus($payStatus)
    {
        $this->payStatus = $payStatus;

        return $this;
    }

    /**
     * Get payStatus
     *
     * @return string 
     */
    public function getPayStatus()
    {
        return $this->payStatus;
    }
    /**
     * @var string
     */
    private $purchaseID;


    /**
     * Set purchaseID
     *
     * @param string $purchaseID
     * @return Paymentgatewayinfo
     */
    public function setPurchaseID($purchaseID)
    {
        $this->purchaseID = $purchaseID;

        return $this;
    }

    /**
     * Get purchaseID
     *
     * @return string 
     */
    public function getPurchaseID()
    {
        return $this->purchaseID;
    }
    /**
     * @var string
     */
    private $userIP;


    /**
     * Set userIP
     *
     * @param string $userIP
     * @return Paymentgatewayinfo
     */
    public function setUserIP($userIP)
    {
        $this->userIP = $userIP;

        return $this;
    }

    /**
     * Get userIP
     *
     * @return string 
     */
    public function getUserIP()
    {
        return $this->userIP;
    }
}

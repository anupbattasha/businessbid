<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Vendorlicenserel
{
   
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var \DateTime
     */
    private $expdate;

    /**
     * @var string
     */
    private $file;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Vendorlicenserel
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set expdate
     *
     * @param \DateTime $expdate
     * @return Vendorlicenserel
     */
    public function setExpdate($expdate)
    {
        $this->expdate = $expdate;

        return $this;
    }

    /**
     * Get expdate
     *
     * @return \DateTime 
     */
    public function getExpdate()
    {
        return $this->expdate;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Vendorlicenserel
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
    /**
     * @var string
     */
    private $licenseName;


    /**
     * Set licenseName
     *
     * @param string $licenseName
     *
     * @return Vendorlicenserel
     */
    public function setLicenseName($licenseName)
    {
        $this->licenseName = $licenseName;

        return $this;
    }

    /**
     * Get licenseName
     *
     * @return string
     */
    public function getLicenseName()
    {
        return $this->licenseName;
    }
}

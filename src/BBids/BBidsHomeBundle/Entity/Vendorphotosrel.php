<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vendorphotosrel
 */
class Vendorphotosrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var string
     */
    private $photourl;

    /**
     * @var string
     */
    private $content;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Vendorphotosrel
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set photourl
     *
     * @param string $photourl
     * @return Vendorphotosrel
     */
    public function setPhotourl($photourl)
    {
        $this->photourl = $photourl;

        return $this;
    }

    /**
     * Get photourl
     *
     * @return string 
     */
    public function getPhotourl()
    {
        return $this->photourl;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Vendorphotosrel
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
}

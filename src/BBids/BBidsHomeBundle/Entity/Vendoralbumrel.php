<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vendoralbumrel
 */
class Vendoralbumrel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var string
     */
    private $albumid;

    /**
     * @var string
     */
    private $albumname;

    /**
     * @var integer
     */
    private $photocount;

    /**
     * @var string
     */
    private $defaultphotopath;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Vendoralbumrel
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set albumid
     *
     * @param string $albumid
     * @return Vendoralbumrel
     */
    public function setAlbumid($albumid)
    {
        $this->albumid = $albumid;

        return $this;
    }

    /**
     * Get albumid
     *
     * @return string 
     */
    public function getAlbumid()
    {
        return $this->albumid;
    }

    /**
     * Set albumname
     *
     * @param string $albumname
     * @return Vendoralbumrel
     */
    public function setAlbumname($albumname)
    {
        $this->albumname = $albumname;

        return $this;
    }

    /**
     * Get albumname
     *
     * @return string 
     */
    public function getAlbumname()
    {
        return $this->albumname;
    }

    /**
     * Set photocount
     *
     * @param integer $photocount
     * @return Vendoralbumrel
     */
    public function setPhotocount($photocount)
    {
        $this->photocount = $photocount;

        return $this;
    }

    /**
     * Get photocount
     *
     * @return integer 
     */
    public function getPhotocount()
    {
        return $this->photocount;
    }

    /**
     * Set defaultphotopath
     *
     * @param string $defaultphotopath
     * @return Vendoralbumrel
     */
    public function setDefaultphotopath($defaultphotopath)
    {
        $this->defaultphotopath = $defaultphotopath;

        return $this;
    }

    /**
     * Get defaultphotopath
     *
     * @return string 
     */
    public function getDefaultphotopath()
    {
        return $this->defaultphotopath;
    }
    /**
     * @var string
     */
    private $summary;

    /**
     * @var \DateTime
     */
    private $updated;


    /**
     * Set summary
     *
     * @param string $summary
     * @return Vendoralbumrel
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string 
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Vendoralbumrel
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}

<?php
// src/BBids/BBidsHomeBundle/Entity/Reviews.php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Reviews
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var string
     */
    private $review;

    /**
     * @var string
     */
    private $author;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var integer
     */
    private $modstatus;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Reviews
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set review
     *
     * @param string $review
     * @return Reviews
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return string 
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Reviews
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Reviews
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modstatus
     *
     * @param integer $modstatus
     * @return Reviews
     */
    public function setModstatus($modstatus)
    {
        $this->modstatus = $modstatus;

        return $this;
    }

    /**
     * Get modstatus
     *
     * @return integer 
     */
    public function getModstatus()
    {
        return $this->modstatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Reviews
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var string
     */
    private $rating;


    /**
     * Set rating
     *
     * @param string $rating
     * @return Reviews
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return string 
     */
    public function getRating()
    {
        return $this->rating;
    }
    /**
     * @var integer
     */
    private $enquiryid;


    /**
     * Set enquiryid
     *
     * @param integer $enquiryid
     * @return Reviews
     */
    public function setEnquiryid($enquiryid)
    {
        $this->enquiryid = $enquiryid;

        return $this;
    }

    /**
     * Get enquiryid
     *
     * @return integer 
     */
    public function getEnquiryid()
    {
        return $this->enquiryid;
    }
    /**
     * @var integer
     */
    private $authorid;


    /**
     * Set authorid
     *
     * @param integer $authorid
     * @return Reviews
     */
    public function setAuthorid($authorid)
    {
        $this->authorid = $authorid;

        return $this;
    }

    /**
     * Get authorid
     *
     * @return integer 
     */
    public function getAuthorid()
    {
        return $this->authorid;
    }
    /**
     * @var string
     */
    private $businessknow;

    /**
     * @var string
     */
    private $businessrecomond;

    /**
     * @var string
     */
    private $ratework;

    /**
     * @var string
     */
    private $rateservice;

    /**
     * @var string
     */
    private $ratemoney;

    /**
     * @var string
     */
    private $ratebusiness;

    /**
     * @var string
     */
    private $servicesummary;

    /**
     * @var string
     */
    private $showname;

    /**
     * @var string
     */
    private $notchoose;

    /**
     * @var string
     */
    private $serviceexperience;


    /**
     * Set businessknow
     *
     * @param string $businessknow
     * @return Reviews
     */
    public function setBusinessknow($businessknow)
    {
        $this->businessknow = $businessknow;

        return $this;
    }

    /**
     * Get businessknow
     *
     * @return string 
     */
    public function getBusinessknow()
    {
        return $this->businessknow;
    }

    /**
     * Set businessrecomond
     *
     * @param string $businessrecomond
     * @return Reviews
     */
    public function setBusinessrecomond($businessrecomond)
    {
        $this->businessrecomond = $businessrecomond;

        return $this;
    }

    /**
     * Get businessrecomond
     *
     * @return string 
     */
    public function getBusinessrecomond()
    {
        return $this->businessrecomond;
    }

    /**
     * Set ratework
     *
     * @param string $ratework
     * @return Reviews
     */
    public function setRatework($ratework)
    {
        $this->ratework = $ratework;

        return $this;
    }

    /**
     * Get ratework
     *
     * @return string 
     */
    public function getRatework()
    {
        return $this->ratework;
    }

    /**
     * Set rateservice
     *
     * @param string $rateservice
     * @return Reviews
     */
    public function setRateservice($rateservice)
    {
        $this->rateservice = $rateservice;

        return $this;
    }

    /**
     * Get rateservice
     *
     * @return string 
     */
    public function getRateservice()
    {
        return $this->rateservice;
    }

    /**
     * Set ratemoney
     *
     * @param string $ratemoney
     * @return Reviews
     */
    public function setRatemoney($ratemoney)
    {
        $this->ratemoney = $ratemoney;

        return $this;
    }

    /**
     * Get ratemoney
     *
     * @return string 
     */
    public function getRatemoney()
    {
        return $this->ratemoney;
    }

    /**
     * Set ratebusiness
     *
     * @param string $ratebusiness
     * @return Reviews
     */
    public function setRatebusiness($ratebusiness)
    {
        $this->ratebusiness = $ratebusiness;

        return $this;
    }

    /**
     * Get ratebusiness
     *
     * @return string 
     */
    public function getRatebusiness()
    {
        return $this->ratebusiness;
    }

    /**
     * Set servicesummary
     *
     * @param string $servicesummary
     * @return Reviews
     */
    public function setServicesummary($servicesummary)
    {
        $this->servicesummary = $servicesummary;

        return $this;
    }

    /**
     * Get servicesummary
     *
     * @return string 
     */
    public function getServicesummary()
    {
        return $this->servicesummary;
    }

    /**
     * Set showname
     *
     * @param string $showname
     * @return Reviews
     */
    public function setShowname($showname)
    {
        $this->showname = $showname;

        return $this;
    }

    /**
     * Get showname
     *
     * @return string 
     */
    public function getShowname()
    {
        return $this->showname;
    }

    /**
     * Set notchoose
     *
     * @param string $notchoose
     * @return Reviews
     */
    public function setNotchoose($notchoose)
    {
        $this->notchoose = $notchoose;

        return $this;
    }

    /**
     * Get notchoose
     *
     * @return string 
     */
    public function getNotchoose()
    {
        return $this->notchoose;
    }

    /**
     * Set serviceexperience
     *
     * @param string $serviceexperience
     * @return Reviews
     */
    public function setServiceexperience($serviceexperience)
    {
        $this->serviceexperience = $serviceexperience;

        return $this;
    }

    /**
     * Get serviceexperience
     *
     * @return string 
     */
    public function getServiceexperience()
    {
        return $this->serviceexperience;
    }
}

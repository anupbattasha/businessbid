<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Leadcredit
 */
class Leadcredit
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $categryid;

    /**
     * @var integer
     */
    private $leadcount;

    /**
     * @var integer
     */
    private $newleadcount;

    /**
     * @var integer
     */
    private $leadcredited;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Leadcredit
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set categryid
     *
     * @param integer $categryid
     * @return Leadcredit
     */
    public function setCategryid($categryid)
    {
        $this->categryid = $categryid;

        return $this;
    }

    /**
     * Get categryid
     *
     * @return integer 
     */
    public function getCategryid()
    {
        return $this->categryid;
    }

    /**
     * Set leadcount
     *
     * @param integer $leadcount
     * @return Leadcredit
     */
    public function setLeadcount($leadcount)
    {
        $this->leadcount = $leadcount;

        return $this;
    }

    /**
     * Get leadcount
     *
     * @return integer 
     */
    public function getLeadcount()
    {
        return $this->leadcount;
    }

    /**
     * Set newleadcount
     *
     * @param integer $newleadcount
     * @return Leadcredit
     */
    public function setNewleadcount($newleadcount)
    {
        $this->newleadcount = $newleadcount;

        return $this;
    }

    /**
     * Get newleadcount
     *
     * @return integer 
     */
    public function getNewleadcount()
    {
        return $this->newleadcount;
    }

    /**
     * Set leadcredited
     *
     * @param integer $leadcredited
     * @return Leadcredit
     */
    public function setLeadcredited($leadcredited)
    {
        $this->leadcredited = $leadcredited;

        return $this;
    }

    /**
     * Get leadcredited
     *
     * @return integer 
     */
    public function getLeadcredited()
    {
        return $this->leadcredited;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Leadcredit
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Leadcredit
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var integer
     */
    private $categoryid;


    /**
     * Set categoryid
     *
     * @param integer $categoryid
     * @return Leadcredit
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }
}

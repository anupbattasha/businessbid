<?php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usertoemail
 */
class Usertoemail
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $fromuid;

    /**
     * @var integer
     */
    private $touid;

    /**
     * @var string
     */
    private $fromemail;

    /**
     * @var string
     */
    private $toemail;

    /**
     * @var string
     */
    private $emailmessage;

    /**
     * @var integer
     */
    private $emailtype;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromuid
     *
     * @param integer $fromuid
     * @return Usertoemail
     */
    public function setFromuid($fromuid)
    {
        $this->fromuid = $fromuid;

        return $this;
    }

    /**
     * Get fromuid
     *
     * @return integer 
     */
    public function getFromuid()
    {
        return $this->fromuid;
    }

    /**
     * Set touid
     *
     * @param integer $touid
     * @return Usertoemail
     */
    public function setTouid($touid)
    {
        $this->touid = $touid;

        return $this;
    }

    /**
     * Get touid
     *
     * @return integer 
     */
    public function getTouid()
    {
        return $this->touid;
    }

    /**
     * Set fromemail
     *
     * @param string $fromemail
     * @return Usertoemail
     */
    public function setFromemail($fromemail)
    {
        $this->fromemail = $fromemail;

        return $this;
    }

    /**
     * Get fromemail
     *
     * @return string 
     */
    public function getFromemail()
    {
        return $this->fromemail;
    }

    /**
     * Set toemail
     *
     * @param string $toemail
     * @return Usertoemail
     */
    public function setToemail($toemail)
    {
        $this->toemail = $toemail;

        return $this;
    }

    /**
     * Get toemail
     *
     * @return string 
     */
    public function getToemail()
    {
        return $this->toemail;
    }

    /**
     * Set emailmessage
     *
     * @param string $emailmessage
     * @return Usertoemail
     */
    public function setEmailmessage($emailmessage)
    {
        $this->emailmessage = $emailmessage;

        return $this;
    }

    /**
     * Get emailmessage
     *
     * @return string 
     */
    public function getEmailmessage()
    {
        return $this->emailmessage;
    }

    /**
     * Set emailtype
     *
     * @param integer $emailtype
     * @return Usertoemail
     */
    public function setEmailtype($emailtype)
    {
        $this->emailtype = $emailtype;

        return $this;
    }

    /**
     * Get emailtype
     *
     * @return integer 
     */
    public function getEmailtype()
    {
        return $this->emailtype;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Usertoemail
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var string
     */
    private $emailsubj;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Set emailsubj
     *
     * @param string $emailsubj
     * @return Usertoemail
     */
    public function setEmailsubj($emailsubj)
    {
        $this->emailsubj = $emailsubj;

        return $this;
    }

    /**
     * Get emailsubj
     *
     * @return string 
     */
    public function getEmailsubj()
    {
        return $this->emailsubj;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Usertoemail
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * @var integer
     */
    private $readStatus;


    /**
     * Set readStatus
     *
     * @param integer $readStatus
     *
     * @return Usertoemail
     */
    public function setReadStatus($readStatus)
    {
        $this->readStatus = $readStatus;

        return $this;
    }

    /**
     * Get readStatus
     *
     * @return integer
     */
    public function getReadStatus()
    {
        return $this->readStatus;
    }
}

<?php
// src/BBid/BBidsHomeBundle/Entity/Smsreplies.php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Smsreplies
{
	
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $parentid;

    /**
     * @var string
     */
    private $reply;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return Smsreplies
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer 
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set reply
     *
     * @param string $reply
     * @return Smsreplies
     */
    public function setReply($reply)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Get reply
     *
     * @return string 
     */
    public function getReply()
    {
        return $this->reply;
    }
    /**
     * @var string
     */
    private $systemresponse;


    /**
     * Set systemresponse
     *
     * @param string $systemresponse
     * @return Smsreplies
     */
    public function setSystemresponse($systemresponse)
    {
        $this->systemresponse = $systemresponse;

        return $this;
    }

    /**
     * Get systemresponse
     *
     * @return string 
     */
    public function getSystemresponse()
    {
        return $this->systemresponse;
    }
}

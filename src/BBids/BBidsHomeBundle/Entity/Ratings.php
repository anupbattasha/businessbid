<?php
// src/BBids/BBidsHomeBundle/Entity/Ratings.php

namespace BBids\BBidsHomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Ratings
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vendorid;

    /**
     * @var integer
     */
    private $rating;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vendorid
     *
     * @param integer $vendorid
     * @return Ratings
     */
    public function setVendorid($vendorid)
    {
        $this->vendorid = $vendorid;

        return $this;
    }

    /**
     * Get vendorid
     *
     * @return integer 
     */
    public function getVendorid()
    {
        return $this->vendorid;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return Ratings
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }
}

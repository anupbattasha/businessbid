<?php

/* BBidsBBidsHomeBundle:Home:sitemap.html.twig */
class __TwigTemplate_bb83c4a6566bcbfd8817ca9a7573cb015c638a30a512afeefa8fd47e4e430b98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:sitemap.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "

<div id=\"contact\" class=\"contact-detail\">
\t<div class=\"container\">
\t\t<div class=\"page-title\">
\t\t\t<h1>Site Map</h1>
\t\t</div>
\t\t<div class=\"col-sm-4 sitemap-block\">
\t\t<h3>About BusinessBid</h3>
\t\t\t<ul class=\"menu nav\">
\t\t\t\t<li class=\"first leaf\"><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\" title=\"\" >About Us</a></li>
\t\t\t\t<li class=\"leaf active\"><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\" title=\"\" class=\"active\">Meet the Team</a></li>
\t\t\t\t<li class=\"leaf active\"><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\" title=\"\" class=\"active\">Career Opportunities</a></li>
\t\t\t\t<li class=\"last leaf active\"><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\" title=\"\" class=\"active\">Valued Partners</a></li>
\t\t\t\t<li class=\"leaf active\"><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#contact\" title=\"\" class=\"active\">Contact Us</a></li>
\t\t\t</ul>
\t\t</div>
\t\t<div class=\"col-sm-4 sitemap-block\">
\t\t  <h3>Vendor Resources</h3>
\t\t\t<ul class=\"menu nav\">
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\" title=\"\">Login</a></li>
\t\t\t\t<li class=\"first leaf active\"><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("bizbids_are_you_a_vendor");
        echo "\" title=\"\" class=\"active\">Grow Your Business</a></li>
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\" title=\"\">How it Works</a></li>
\t\t\t\t<li class=\"first leaf\"><a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" title=\"\">Become a Vendor</a></li>
\t\t\t\t<li class=\"leaf active\"><a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\" title=\"\" class=\"active\">FAQ's &amp; Support</a></li>
\t\t\t</ul>
\t\t</div>
\t\t<div class=\"col-sm-4 sitemap-block\">
\t\t <h3>BusinessBid Services</h3>
\t\t \t<ul class=\"menu nav\">
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Vendor Search</a></li>
\t\t\t\t<li class=\"first leaf active\"><a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\" >Vendor Reviews</a></li>

\t\t\t</ul>
\t\t</div>
\t\t<div class=\"col-sm-4 sitemap-block\">
\t\t <h3>Client Services</h3>
\t\t \t <ul class=\"menu nav\">
\t\t\t\t<li class=\"first leaf\"><a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_job");
        echo "\" title=\"\">Post a Job</a></li>
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\" title=\"\">Find a Vendor</a></li>
\t\t\t\t<li class=\"leaf\"><a href=\"#\" title=\"\">Search Reviews</a></li>
\t\t\t\t<li class=\"last leaf\"><a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\" title=\"\">Write a Review</a></li>
\t\t\t</ul>
\t\t</div>
\t\t<div class=\"col-sm-4 sitemap-block\">
\t\t <h3>Client Resources</h3>
\t\t \t <ul class=\"menu nav\">
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\" title=\"\">Login</a></li>
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
\t\t\t\t<!--<li class=\"first leaf\"><a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\" title=\"\">Become a Consumer</a></li> -->
\t\t\t\t<li class=\"last leaf active\"><a href=\"http:../../../resource_centre\" title=\"\" class=\"active\">Resource Center</a></li>
\t\t\t\t<li class=\"leaf active\"><a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\" title=\"\" class=\"active\">FAQ's &amp; Support</a></li>
\t\t\t\t</ul>
\t\t</div>
\t\t<div class=\"col-sm-4 sitemap-block\">
\t\t<h3>BusinessBid Info</h3>
\t\t \t <ul class=\"menu nav\">
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
\t\t\t\t<li class=\"leaf\"><a href=\"";
        // line 66
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
\t\t\t\t<li class=\"last leaf active\"><a href=\"";
        // line 67
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_job");
        echo "\" title=\"\">Post a Job</a></li>
\t\t\t\t<li class=\"leaf active\"><a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#contact\" title=\"\" class=\"active\">Contact Us</a></li>

\t\t\t\t</ul>
\t\t</div>
\t</div>
</div>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:sitemap.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 68,  164 => 67,  160 => 66,  156 => 65,  147 => 59,  142 => 57,  138 => 56,  134 => 55,  125 => 49,  120 => 47,  116 => 46,  106 => 39,  102 => 38,  93 => 32,  89 => 31,  85 => 30,  81 => 29,  77 => 28,  68 => 22,  64 => 21,  60 => 20,  56 => 19,  52 => 18,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

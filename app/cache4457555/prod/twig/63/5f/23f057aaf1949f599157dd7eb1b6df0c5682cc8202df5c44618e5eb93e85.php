<?php

/* BBidsBBidsHomeBundle:Home:reviews_list.html.twig */
class __TwigTemplate_635f23f057aaf1949f599157dd7eb1b6df0c5682cc8202df5c44618e5eb93e85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:reviews_list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_reviewform_post", array("catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<style>
    .pac-container:after{
        content:none !important;
    }
    .pac-container {
        background-color: #FFF;
        z-index: 1050;
        position: fixed;
        display: inline-block;
        float: left;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">

<div class=\"container\">
\t<div class=\"breadcrum\">Home / Vendor Review & Rating / ";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["categoryname"]) ? $context["categoryname"] : null), "html", null, true);
        echo "</div>
\t <div class=\"vendor_revirw_area\">
             <div class=\"inner_vendor_area\">
                 <div class=\"child_inned_vendor\">
                     <div class=\"vendor_top_menu_drop\">

                         <div class=\"vendor_top_menu_drop_one\">
                            <select class=\"form-control\" name=\"categories\">
                                ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 32
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($context["category"], "id", array()) == (isset($context["catid"]) ? $context["catid"] : null))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "                            </select>
                         </div>
                         <div class=\"vendor_top_menu_drop_two\">
                                <select class=\"form-control\" name=\"sorting\">
                                  <option value=\"hrated\">Ratings: Highest Rated</option>
                                  ";
        // line 40
        echo "                                  ";
        // line 41
        echo "                                  <option value=\"atoz\">Alphabetical (A-Z)</option>
                                  <option value=\"ztoa\">Alphabetical (Z-A)</option>
                                </select>
                         </div>
                     </div>
                     <div class=\"gray_box_area\">
                         <div class=\"gray_box_area_child\">
                             <p>Select up to 5 of the following companies and click Compare Quotes when you are done. You will then receive quotations from the companies that are available to take up your project.</p>
                         </div>
                     </div>
                     <div class=\"vendor_child_header\">
                         <h2>";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["categoryname"]) ? $context["categoryname"] : null), "html", null, true);
        echo "</h2>
                     </div>
                    ";
        // line 54
        if ( !twig_test_empty((isset($context["reviewList"]) ? $context["reviewList"] : null))) {
            // line 55
            echo "                        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reviewList"]) ? $context["reviewList"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                // line 56
                echo "                     <div class=\"vendor_child_main_content\">
                         <div class=\"vendor_child_main_content_img\">
                            ";
                // line 58
                if ((null === $this->getAttribute($context["review"], "fileName", array()))) {
                    // line 59
                    echo "                             <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/noimage.png"), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "bizname", array()), "html", null, true);
                    echo "\">
                            ";
                } else {
                    // line 61
                    echo "                                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("logo/" . $this->getAttribute($context["review"], "fileName", array()))), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "bizname", array()), "html", null, true);
                    echo "\">
                            ";
                }
                // line 63
                echo "                         </div>
                         <div class=\"vendor_child_main_content_txt\">
                             <h2><a href=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => $this->getAttribute($context["review"], "userid", array()), "catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "bizname", array()), "html", null, true);
                echo "</a></h2>
                             <p><b>Cities Serviced:</b>
                                ";
                // line 67
                if ($this->getAttribute((isset($context["vcGroup"]) ? $context["vcGroup"] : null), $this->getAttribute($context["review"], "userid", array()), array(), "array", true, true)) {
                    // line 68
                    echo "                                ";
                    echo twig_escape_filter($this->env, (twig_slice($this->env, twig_join_filter($this->getAttribute((isset($context["vcGroup"]) ? $context["vcGroup"] : null), $this->getAttribute($context["review"], "userid", array()), array(), "array"), ", "), 0, 60) . "..."), "html", null, true);
                    echo "
                                ";
                } else {
                    // line 70
                    echo "                                    N/A
                                ";
                }
                // line 72
                echo "                            </p>
                             <p><b>Services Offered:</b>
                                ";
                // line 74
                if ($this->getAttribute((isset($context["vsGroup"]) ? $context["vsGroup"] : null), $this->getAttribute($context["review"], "userid", array()), array(), "array", true, true)) {
                    // line 75
                    echo "                                    <ul>";
                    echo twig_escape_filter($this->env, (twig_slice($this->env, twig_join_filter($this->getAttribute((isset($context["vsGroup"]) ? $context["vsGroup"] : null), $this->getAttribute($context["review"], "userid", array()), array(), "array"), ", "), 0, 60) . "..."), "html", null, true);
                    echo "</ul>
                                ";
                } else {
                    // line 77
                    echo "                                    N/A
                                ";
                }
                // line 79
                echo "                             </p>
                         </div>
                         <div class=\"vendor_child_main_content_rating\">
                             <div class=\"rating_inner\">
                                 <ul>
                                     ";
                // line 85
                echo "
                                    ";
                // line 86
                if (($this->getAttribute($context["review"], "rating", array()) == 0)) {
                    // line 87
                    echo "                                        <li><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                    echo "\" alt=\"star0\"></li><li><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                    echo "\" alt=\"star0\"></li><li><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                    echo "\" alt=\"star0\"></li><li><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                    echo "\" alt=\"star0\"></li><li><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                    echo "\" alt=\"star0\"></li>
                                    ";
                } else {
                    // line 89
                    echo "                                     <li><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("img/star" . $this->env->getExtension('app_extension')->roundRating(twig_round($this->getAttribute($context["review"], "rating", array()), 1, "floor"))) . ".png")), "html", null, true);
                    echo "\" alt=\"star";
                    echo twig_escape_filter($this->env, twig_round($this->getAttribute($context["review"], "rating", array()), 1, "floor"), "html", null, true);
                    echo "\"></li>
                                     ";
                }
                // line 91
                echo "
                                 </ul>
                                ";
                // line 93
                if ($this->getAttribute($this->getAttribute((isset($context["rcGroup"]) ? $context["rcGroup"] : null), $this->getAttribute($context["review"], "userid", array()), array(), "array", false, true), 0, array(), "array", true, true)) {
                    // line 94
                    echo "                                 <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => $this->getAttribute($context["review"], "userid", array()), "catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
                    echo "\"><p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["rcGroup"]) ? $context["rcGroup"] : null), $this->getAttribute($context["review"], "userid", array()), array(), "array"), 0, array(), "array"), "html", null, true);
                    echo " &nbsp;Verified Review(s)</p></a>
                                ";
                }
                // line 96
                echo "                             </div>
                             <div class=\"rating_inner_text\">
                                 <h3>(";
                // line 98
                echo twig_escape_filter($this->env, twig_round($this->getAttribute($context["review"], "rating", array()), 1, "floor"), "html", null, true);
                echo "/5.0)</h3>
                             </div>
                             <div class=\"rating_checkbox\">
                                <label><!-- Squared TWO -->
                                <div class=\"squaredTwo\">
                                    <input type=\"checkbox\" value=\"";
                // line 103
                echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "userid", array()), "html", null, true);
                echo "\" id=\"squared";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\" name=\"check";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\" class=\"vcheck\" />
                                    <label for=\"squared";
                // line 104
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "\"></label>
                                </div>
                                Request Quote</label>
                             </div>
                         </div>
                     </div>
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo "                    ";
        }
        // line 112
        echo "                    ";
        // line 113
        echo "                     <div class=\"get_mached_right_way\">
                         <div class=\"get_mached_right_way_img\">
                             <img src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/matchedlogo.png"), "html", null, true);
        echo "\" alt=\"\">
                         </div>
                         <div class=\"get_mached_right_way_text\">
                             <h2>Get matched Right Away</h2>
                             <p>Use our vendor match technology to receive quotation from the most appropriate vendors immediately.</p>
                         </div>
                         <div class=\"get_mached_right_way_btn\">
                             <button type=\"submit\" class=\"mached_btn\">GET MATCHED NOW</button>
                         </div></div>
                          <div class=\"pagenavarea\">
                            <div class=\"pagenavarea_inner\">
                                ";
        // line 126
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["filterForm"]) ? $context["filterForm"] : null), 'form');
        echo "
                                ";
        // line 127
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
                             </div>
                         </div>
                        <!-- Modal -->
                        <div id=\"myModal\" class=\"modal fade\" role=\"dialog\" data-backdrop=\"static\">
                          <div class=\"modal-dialog\">
                            <!-- Modal content-->
                            <div class=\"modal-content\">

                              <div class=\"modal-body\">
                                ";
        // line 137
        $this->loadTemplate((("BBidsBBidsHomeBundle:Categoriesform/Reviewforms:" . (isset($context["twigFileName"]) ? $context["twigFileName"] : null)) . ".html.twig"), "BBidsBBidsHomeBundle:Home:reviews_list.html.twig", 137)->display($context);
        // line 138
        echo "                              ";
        // line 141
        echo "                            </div>
                          </div>
                            </div>
                        </div>


                 </div>
                 <div id=\"sticky-anchor\"></div>
                 <div id=\"sticky\" class=\"child_inned_sideber_vendpr\">
                     <div class=\"child_sideber_vendor\">
                         <h2>Select up to 5 of the following companies and click Compare Quotes when you are done. You will then receive quotations from the companies that are available to take up your project.</h2>
                         ";
        // line 152
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["quoteForm"]) ? $context["quoteForm"] : null), 'form_start');
        echo "
                          <div class=\"form-group\">
                            ";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com1", array()), 'label');
        echo "
                            ";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com1", array()), 'widget');
        echo "
                          </div>
                          <div class=\"form-group\">
                            ";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com2", array()), 'label');
        echo "
                            ";
        // line 159
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com2", array()), 'widget');
        echo "
                          </div>
                          <div class=\"form-group\">
                            ";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com3", array()), 'label');
        echo "
                            ";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com3", array()), 'widget');
        echo "
                          </div>
                          <div class=\"form-group\">
                            ";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com4", array()), 'label');
        echo "
                            ";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com4", array()), 'widget');
        echo "
                          </div>
                          <div class=\"form-group\">
                            ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com5", array()), 'label');
        echo "
                            ";
        // line 171
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["quoteForm"]) ? $context["quoteForm"] : null), "com5", array()), 'widget');
        echo "
                          </div>
                          <button type=\"button\" class=\"compare_btn\" id=\"compareQuotes\">COMPARE QUOTES</button>
                          ";
        // line 174
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["quoteForm"]) ? $context["quoteForm"] : null), 'form_end');
        echo "
                          <h4>• You will receive quotes from the first 3 available companies.</h4>
                     </div>
                 </div>
                 ";
        // line 181
        echo "             </div>
         </div>

<!-- Footer Script Here -->
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js\"></script>
<script>
var textBoxCount\t=\t1;
    function scrollBox (tag) {
        \$('#myModal').animate({scrollTop: tag.position().top -10},'slow');
    }
    \$(document).ready(function(){

        if (\$('#sticky').length) { // make sure \"#sticky\" element exists
          var el = \$('#sticky');
          var stickyTop = \$('#sticky').offset().top; // returns number
          var stickyHeight = \$('#sticky').height();

          \$(window).scroll(function(){ // scroll event
              var limit = \$('div.pagenavarea').offset().top - stickyHeight - 20;

              var windowTop = \$(window).scrollTop(); // returns number

              if (stickyTop < windowTop){
                 el.css({ position: 'fixed', top: 0 , right: 100 });
              }
              else {
                 el.css('position','static');
              }

              if (limit < windowTop) {
              var diff = limit - windowTop;
              el.css({top: diff});
              }
            });
       }
        var forms = [
            '[ name=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
          ];
        var validationFlag = true;
          \$( forms.join(',') ).submit( function( e ){
            e.preventDefault();
            console.log(\"tried submitting\")
            if(validationFlag) {
                postForm( \$(this), function( response ){
                    if(response.hasOwnProperty('error')) {
                        alert(response.error);
                        var i = 1;
                        for (i = 1; i < 5; i++) {
                            \$(\"div#step\"+i).hide();
                        };
                        \$(\"div#step\"+response.step).show();
                    } else {
                        \$('p#enquiryResult:contains(\"processing\")').text(\$('p#enquiryResult:contains(\"processing\")').text().replace('processing',response.jobNo));
                        \$(\"#category_form\")[0].reset();
                    }
                });
            }
            return false;
          });
        function postForm( \$form, callback ) {
              \$.ajax({
                type        : \$form.attr( 'method' ),
                beforeSend  : function() { NProgress.inc() },
                url         : \$form.attr( 'action' ),
                data        : \$form.serialize(),
                success     : function(data) {
                    callback( data );
                    NProgress.done();
                },
                error : function (xhr) {
                    alert(\"Error occured.please try again\");
                    NProgress.done();
                }
              });
        }

        \$('select[name=\"categories\"]').on('change', function() {
            var redirectUrl = \"";
        // line 264
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => "category"));
        echo "\";
            var res = redirectUrl.replace(\"category\", \$(this).val());
            window.location.assign(res);
        });
    ";
        // line 268
        if (twig_test_empty((isset($context["vList"]) ? $context["vList"] : null))) {
            // line 269
            echo "    var vList = {};
    ";
        } else {
            // line 271
            echo "    var vList = ";
            echo twig_jsonencode_filter((isset($context["vList"]) ? $context["vList"] : null));
            echo ";
    selVendorsInit(vList);
    ";
        }
        // line 274
        echo "    \$('div.squaredTwo :checkbox').click(function() {
        var \$this = \$(this),reqType,boxValue;
        boxValue = \$this.val();
        if (\$this.is(':checked')) {
            reqType = \"add\";
        } else {
            reqType = \"del\";
        }

        var len = 0;
        \$.each(vList,function (n,i) {
            len += (i.status == '0') ? 0: 1;
        })
        if((len >= 5) && (reqType == 'add')) {
            alert('You have already selected 5 companies for your request. \\n Click \"OK\" and change companies selection.')
            return false;
        }


        \$.post('";
        // line 293
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviews_quote");
        echo "',
        {
            boxValue : boxValue,
            reqType : reqType,
            catid : ";
        // line 297
        echo twig_escape_filter($this->env, (isset($context["catid"]) ? $context["catid"] : null), "html", null, true);
        echo "
        },
        function(data,status) {
        var textBoxArray\t=\tnew Array();
            vList[boxValue] = {}
            vList[boxValue].name = data;

            vList[boxValue].status = (reqType == 'add') ? 1 : 0;
            //console.log('status ->'+vList[boxValue].status);
            //console.log('Data ->'+data);

            if(vList[boxValue].status==1){

             jQuery('input#form_com'+textBoxCount).val(data);
             textBoxCount++;
            }
            if(vList[boxValue].status==0){
\t\t\t\tfor(var j=1;j<=5;j++){
\t\t\t\t\tif(jQuery('input#form_com'+j).val()!='' && jQuery('input#form_com'+j).val()!=data){
\t\t\t\t\t\ttextBoxArray.push(jQuery('input#form_com'+j).val());
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\ttextBoxCount\t=\ttextBoxArray.length;
\t\t\t\ttextBoxCount++;
\t\t\t\tvar l=1;
\t\t\t\tfor(var k=0;k<textBoxArray.length;k++){
\t\t\t\t\tjQuery('input#form_com'+l).val(textBoxArray[k]);
\t\t\t\t\tl++;
\t\t\t\t}
\t\t\t\tfor(var k=textBoxArray.length;k<=5;k++){
\t\t\t\t\tjQuery('input#form_com'+l).val('');
\t\t\t\t\t//l++;
\t\t\t\t}
            }

            return false;
            var i = 0;
            jQuery.each(vList,function(index, item) {
                i++;

                if(item.status == 1){
                      textBoxArray.push(item.name);
                    }
                else{
                    }
                if(i == 5) return false;
            }),
            textBoxInsertFn(textBoxArray);
        })
    });
function textBoxInsertFn(name){

var k=1;
for(var j=0;j<name.length;j++){
\tif(k<5){
\t\tjQuery('input#form_com'+k).val(name[j]);
\t\tk++;
\t}
}
for(var m=j;m<=5;m++){
jQuery('input#form_com'+k).val('');
}


}
    \$('#compareQuotes').click(function(){
        var i = 0;
        jQuery.each(vList,function(index, item) {
            if(item.status == 1) {
                i++;
            }
            if(i == 5) return false;
        })
        if(i == 0) {
            alert(\"Select at least one vendor.\")
            \$('#myModal').modal('hide');
        } else {
            \$('#myModal').modal('show');
        }
   });


});
</script>
<script type=\"text/javascript\" src=\"";
        // line 381
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 382
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script type=\"text/javascript\" src=\"";
        // line 383
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("js/reviewsformsvalidation/" . (isset($context["twigFileName"]) ? $context["twigFileName"] : null)) . ".min.js")), "html", null, true);
        echo "\" ></script>
</div>

";
        // line 386
        $this->displayBlock('footerScript', $context, $blocks);
    }

    public function block_footerScript($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:reviews_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  656 => 386,  650 => 383,  646 => 382,  642 => 381,  555 => 297,  548 => 293,  527 => 274,  520 => 271,  516 => 269,  514 => 268,  507 => 264,  463 => 223,  419 => 181,  412 => 174,  406 => 171,  402 => 170,  396 => 167,  392 => 166,  386 => 163,  382 => 162,  376 => 159,  372 => 158,  366 => 155,  362 => 154,  357 => 152,  344 => 141,  342 => 138,  340 => 137,  327 => 127,  323 => 126,  309 => 115,  305 => 113,  303 => 112,  300 => 111,  279 => 104,  271 => 103,  263 => 98,  259 => 96,  251 => 94,  249 => 93,  245 => 91,  237 => 89,  223 => 87,  221 => 86,  218 => 85,  211 => 79,  207 => 77,  201 => 75,  199 => 74,  195 => 72,  191 => 70,  185 => 68,  183 => 67,  176 => 65,  172 => 63,  164 => 61,  156 => 59,  154 => 58,  150 => 56,  132 => 55,  130 => 54,  125 => 52,  112 => 41,  110 => 40,  103 => 34,  88 => 32,  84 => 31,  73 => 23,  67 => 20,  53 => 8,  50 => 7,  45 => 4,  37 => 3,  32 => 2,  11 => 1,);
    }
}

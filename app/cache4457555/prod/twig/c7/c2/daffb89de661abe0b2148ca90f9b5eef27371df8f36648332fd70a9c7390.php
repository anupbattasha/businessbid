<?php

/* BBidsBBidsHomeBundle:Home:feedback.html.twig */
class __TwigTemplate_c7c2daffb89de661abe0b2148ca90f9b5eef27371df8f36648332fd70a9c7390 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:feedback.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "

<div id=\"contact\" class=\"contact-detail\">
\t<div class=\"container\">
\t
\t\t<div class=\"page-title\">
\t\t\t<h1>Feedback Form</h1>
\t\t\t<p>Your feedback is important to us please send us a message using the form below</p>
\t\t\t";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 17
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 18
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div> 
\t
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
\t\t\t";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 23
            echo "
\t\t\t\t<div class=\"alert alert-success\">";
            // line 24
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>\t
\t\t\t
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "\t\t\t
\t\t\t";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 29
            echo "
\t\t\t<div class=\"alert alert-success\">";
            // line 30
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "\t\t</div>
\t\t<div class=\"contact-content\">
\t\t\t<div class=\"column-left\">
\t\t\t\t<div class=\"feedback-form\">
\t\t\t\t
\t\t\t\t
\t\t\t\t";
        // line 39
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("onsubmit" => "return feedBackValidate();")));
        echo "
\t\t\t\t\t<div class=\"control\">";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'label');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"control\">";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"control\">";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subject", array()), 'label');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subject", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"control\"> ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "feedback", array()), 'label');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "feedback", array()), 'widget');
        echo "<p class=\"help-block\">(maximum 1000 characters)</p></div>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<div class=\"captcha-block\">
\t\t\t\t\t\t\t<div class=\"col-sm-5 captcha-image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vvv/captcha.php"), "html", null, true);
        echo "\" id=\"captcha\" /><br/>
\t\t\t\t\t\t\t\t\t<a href=\"#\" onclick=\"scrollTo();\"
\t\t\t\t\t\t\t\t\t\tid=\"change-image\">Not readable? Change text.</a><br/><br/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-sm-7 captcha-text\">
\t\t\t\t\t\t\t\t";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "verification", array()), 'widget');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"control\">";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "submit", array()), 'widget');
        echo "</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"control-group\">";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo " </div>
\t\t\t\t";
        // line 59
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"column-right\">
\t\t\t<div class=\"column-map\">
\t\t\t\t<p>If you would like to come and visit us, we are located at the address below: <br><br>Suite 503, Level 5,
Indigo Icon Tower, <br>Cluster F, Jumeirah Lakes Tower - Dubai, UAE<br>Sunday-Thursday 9 am - 6 pm</p>
\t\t\t</div>   
\t\t\t</div> 

\t\t</div>
\t\t<div class=\"align-center\"><img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo " \"> </div>
\t</div>  
</div>
<script type=\"text/javascript\">
\tfunction scrollTo() {
\t\tvar pathUrl = \"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vvv/captcha.php"), "html", null, true);
        echo "\"+\"?\";
\t\tdocument.getElementById('captcha').src=pathUrl+Math.random();
        \$('html, body').animate({ scrollTop: \$('.feedback-form').offset().top }, \"slow\");
        document.getElementById('form_verification').focus();
        return false;
    }\t
    function feedBackValidate () {\t\t
\t\tvar enteredText = \$(\"#form_verification\").val();\t\t
\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vvv/check.php"), "html", null, true);
        echo "\",
\t\t\tdata: {\"code\": enteredText},
\t\t\tsuccess: function (result) {\t\t\t\t
\t\t\t\tif(result == 'true') {\t\t\t\t\t
\t\t\t\t\treturn true;
\t\t\t\t} else {
\t\t\t\t\talert(\"Invalid Captcha\");
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t}
\t  \t});
\t}
</script>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:feedback.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 86,  187 => 76,  179 => 71,  164 => 59,  160 => 58,  155 => 56,  148 => 52,  140 => 47,  132 => 43,  127 => 42,  121 => 41,  116 => 40,  112 => 39,  104 => 33,  95 => 30,  92 => 29,  88 => 28,  85 => 27,  76 => 24,  73 => 23,  69 => 22,  66 => 21,  57 => 18,  54 => 17,  50 => 16,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

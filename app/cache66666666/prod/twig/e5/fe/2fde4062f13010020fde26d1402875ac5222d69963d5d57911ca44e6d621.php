<?php

/* ::vendor_dashboard.html.twig */
class __TwigTemplate_e5fe2fde4062f13010020fde26d1402875ac5222d69963d5d57911ca44e6d621 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'chartscript' => array($this, 'block_chartscript'),
            'jquery' => array($this, 'block_jquery'),
            'customcss' => array($this, 'block_customcss'),
            'header' => array($this, 'block_header'),
            'menu' => array($this, 'block_menu'),
            'topmenu' => array($this, 'block_topmenu'),
            'pageblock' => array($this, 'block_pageblock'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'dashboardgrid' => array($this, 'block_dashboardgrid'),
            'dashboard' => array($this, 'block_dashboard'),
            'statistics' => array($this, 'block_statistics'),
            'latestorders' => array($this, 'block_latestorders'),
            'enquiries' => array($this, 'block_enquiries'),
            'footer' => array($this, 'block_footer'),
            'megamenu' => array($this, 'block_megamenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<head>
\t<title>Business BID</title>
\t<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<meta http-equiv=\"Cache-control\" content=\"public\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">

\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">

\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/eticket.css"), "html", null, true);
        echo "\">
\t<!--[if lt IE 9]>
  \t<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/html5shiv.min.js"), "html", null, true);
        echo "\"></script>
  \t<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.min.js"), "html", null, true);
        echo "\"></script>
\t<![endif]-->
\t\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.10.2.min.js"), "html", null, true);
        echo "\"></script>
\t";
        // line 27
        echo "\t<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js\"></script>
\t<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js\"></script>

\t <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/highcharts.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/modules/exporting.js"), "html", null, true);
        echo "\"></script>
\t<script>

\t</script>
<!---main menu start -->


<script>

\t \$(function(){
    \$(\".dropdown\").hover(
            function() {
                \$('.dropdown-menu', this).stop( true, true ).fadeIn(\"fast\");
                \$(this).toggleClass('open');
                \$('b', this).toggleClass(\"caret caret-up\");
            },
            function() {
                \$('.dropdown-menu', this).stop( true, true ).fadeOut(\"fast\");
                \$(this).toggleClass('open');
                \$('b', this).toggleClass(\"caret caret-up\");
            });
    });

</script>
<script>
\$(document).ready(function(){
\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

});
</script>

<script>
\t\$(document).ready(function(){

\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$('#category').autocomplete({
\t      \tsource: function( request, response ) {
\t      \t\$.ajax({
\t      \turl : realpath+\"ajax-info.php\",
\t      \tdataType: \"json\",
\tdata: {
\t   name_startsWith: request.term,
\t   type: 'category'
\t},
\t success: function( data ) {
\t response( \$.map( data, function( item ) {

\treturn {
\tlabel: item,
\tvalue: item
\t}

\t}));
\t},
\tselect: function(event, ui) {
                               alert( \$(event.target).val() );
                            }
\t      \t});
\t      \t},
\t      \tautoFocus: true,
\t      \tminLength: 2

\t      });

});
\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_subcategory\").html('<option value=\"0\">Select</option>');
\t\$(\"#form_category\").on('change',function (){
\tvar ax = \t\$(\"#form_category\").val();

\t\$.ajax({url:realpath+\"pullSubCategoryByCategory.php?categoryid=\"+ax,success:function(result){

\t\$(\"#form_subcategory\").html(result);
\t}});
\t});

});

\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_email\").on('input',function (){
\tvar ax = \t\$(\"#form_email\").val();
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}
\t});

});

\t\$(document).ready(function(){
\t\$('#form_description').attr(\"maxlength\",\"300\");
\t\$('#form_location').attr(\"maxlength\",\"30\");

\t});

</script>
<script type='text/javascript' >

\tfunction jsfunction(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//~ alert(xmlhttp.responseText);
\t//alert('fghf');
\tdocument.getElementById(\"searchval\").innerHTML=xmlhttp.responseText;
\tmegamenublogAjax();
\tmegamenubycatAjax();
\tmegamenubycatVendorSearchAjax();
\tmegamenubycatVendorReviewAjax();
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"ajax-info.php\",true);
\txmlhttp.send();



\t}

function megamenublogAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t\t\t\t\t\t\t\t\t//document.getElementById(\"megamenu1\").innerHTML=xmlhttp.responseText;
\t\t/*var mvsa = document.getElementById(\"megamenu1Anch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Resourcesegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}*/
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menublog.php\",true);
\t//xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);

\txmlhttp.send();



\t}
\tfunction megamenubycatAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\tdocument.getElementById(\"megamenubycatUl\").innerHTML=xmlhttp.responseText;
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menubycat.php\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorSearchAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t//console.log(xmlhttp.responseText);
\t\tvar mvsa = document.getElementById(\"megamenuVenSearchAnch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Searchsegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t//document.getElementById(\"megamenuVenSearchAnch\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorSearch\",true);
\txmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorReviewAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();

\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t\t//alert(xmlhttp.responseText);
\t\t\tvar mvsa = document.getElementById(\"megamenuVenReviewAnch\");
\t\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Reviewsegment-drop\") {
\t\t\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t\t\t  break;
\t\t\t\t\t}
\t\t\t}

\t\t//document.getElementById(\"megamenuVenReviewUl\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t  xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorReview\",true);
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorReview\",true);
\txmlhttp.send();



\t}

</script>

<script type=\"text/javascript\">
function show_submenu(b, menuid)
{
\talert(menuid);

\tdocument.getElementById(b).style.display=\"block\";
\tif(menuid!=\"megaanchorbycat\"){
\tdocument.getElementById(menuid).className = \"setbg\";
\t}
}

function hide_submenu(b, menuid)
{


\tsetTimeout(dispminus(b,menuid), 200);



\t//document.getElementById(b).onmouseover = function() { document.getElementById(b).style.display=\"block\"; }
\t//document.getElementById(b).onmouseout = function() { document.getElementById(b).style.display=\"none\"; }
\t//document.getElementById(b).style.display=\"none\";
}

function dispminus(subid,menuid){
\t//if(menuid!=\"megaanchorbycat\"){
\t//document.getElementById(menuid).className = \"unsetbg\";
\t//}
\t//document.getElementById(subid).style.display=\"none\";


\t}


\$(document).ready(function(){
  \$(\"#flip\").click(function(){
    \$(\"#panel\").slideToggle(\"slow\");
  });

});

 \$(document).ready(function(){
      \$(\".Searchsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Searchsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });
 \$(document).ready(function(){
      \$(\".Reviewsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Reviewsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });


</script>
<!---main menu ends - -->
";
        // line 392
        $this->displayBlock('chartscript', $context, $blocks);
        // line 396
        echo "
";
        // line 397
        $this->displayBlock('jquery', $context, $blocks);
        // line 400
        echo "
";
        // line 401
        $this->displayBlock('customcss', $context, $blocks);
        // line 403
        echo "

</head>
<body>

";
        // line 408
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 409
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 410
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 411
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 412
        echo "

";
        // line 414
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 415
            echo "<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 contact-info\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>
\t\t\t\t\t<span>(04) 42 13 777</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3 left-clear\">
\t\t\t\t\t<span class=\"contact-no\"><a href=\"";
            // line 424
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
            echo "\">Contact Us</a></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6 clear-right dash-right-top-links\">
\t\t\t\t<ul>
\t\t\t\t\t<li>Welcome, <span class=\"profile-name\">";
            // line 429
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
\t\t\t\t\t<li><a href=\"";
            // line 430
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
        }
        // line 437
        echo "
<div class=\"main_container\">
\t";
        // line 439
        $this->displayBlock('header', $context, $blocks);
        // line 448
        echo "\t";
        $this->displayBlock('menu', $context, $blocks);
        // line 512
        echo "

\t";
        // line 514
        $this->displayBlock('pageblock', $context, $blocks);
        // line 678
        echo "\t</div>
</div>
";
        // line 680
        $this->displayBlock('footer', $context, $blocks);
        // line 688
        echo "\t";
        $this->displayBlock('megamenu', $context, $blocks);
        // line 704
        echo "<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
<script type=\"text/javascript\">
window.onload = jsfunction();
</script>
</html>
";
    }

    // line 392
    public function block_chartscript($context, array $blocks = array())
    {
        // line 393
        echo "

";
    }

    // line 397
    public function block_jquery($context, array $blocks = array())
    {
        // line 398
        echo "
";
    }

    // line 401
    public function block_customcss($context, array $blocks = array())
    {
    }

    // line 439
    public function block_header($context, array $blocks = array())
    {
        // line 440
        echo "\t<div class=\"header container\">
    \t<div class=\"col-md-6 logo\">
\t\t<a href=\"";
        // line 442
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"  >\t<img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"> </a>
        </div>
        <div class=\"col-md-6\">
        </div>
    </div>
\t";
    }

    // line 448
    public function block_menu($context, array $blocks = array())
    {
        // line 449
        echo "\t<div class=\"menu_block\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"navbar-header col-sm-6 \">
\t\t\t\t\t<!--<ul class=\"page-title\"><li>DASHBOARD</li></ul>-->
\t\t\t\t</div>
\t\t\t\t";
        // line 454
        $this->displayBlock('topmenu', $context, $blocks);
        // line 509
        echo "        \t</div>
  \t\t</div>
\t";
    }

    // line 454
    public function block_topmenu($context, array $blocks = array())
    {
        // line 455
        echo "\t\t\t\t<div class=\"container\">
\t<div class=\"row\">

        <nav class=\"navbar navbar-default\" role=\"navigation\">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\">
        <li class=\"active\"><a href=\"";
        // line 474
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>

        <li class=\"dropdown\">
          <a href=\"";
        // line 477
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Find Services Professionals<b class=\"caret\"></b></a>
          <ul class=\"dropdown-menu\">
            ";
        // line 479
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
          </ul>
        </li>
         <li class=\"dropdown\">
          <a href=\"";
        // line 483
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Review <b class=\"caret\"></b></a>
          <ul class=\"dropdown-menu\">
            ";
        // line 485
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
          </ul>
        </li>
      </ul>

      <ul class=\"nav navbar-nav navbar-right  col-md-5\">
         <form class=\"navbar-form navbar-left\" action=\"";
        // line 491
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
        <div class=\"form-group\">
          <input id='category' type=\"text\" class=\"form-control serice-required\" placeholder=\"Type the serive you require\"><datalist id=\"searchval\"></datalist>
        </div>
        <button type=\"submit\" class=\"btn btn-default serice-required-btn\">Search</button>
      </form>
      </ul>
    </div><!-- /.navbar-collapse -->

</nav>

\t</div>
</div>



\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 514
    public function block_pageblock($context, array $blocks = array())
    {
        // line 515
        echo "\t\t<div class=\"page-bg\">
    \t<div class=\"container inner_container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"dashboard-panel\">
\t\t\t\t";
        // line 519
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 557
        echo "
\t\t";
        // line 558
        $this->displayBlock('dashboardgrid', $context, $blocks);
        // line 675
        echo "    \t</div>
    \t</div>
\t";
    }

    // line 519
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 520
        echo "\t\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 523
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
        echo "\">Vendor Dashboard</a></li>
\t\t\t\t\t\t\t <li><a class=\"account\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">My Account <span class=\"caret\"></span></a>
\t\t\t\t\t\t\t\t<ul id=\"collapseOne\" class=\"panel-collapse collapse menu-col\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 526
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_account");
        echo "\">My Profile</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 527
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendorupdatepasssword");
        echo "\">Change Password</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 528
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendorupdatemail");
        echo "\">Update Email</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 529
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendordeleteAccount");
        echo "\">Delete Account</a>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        // line 532
        if ((array_key_exists("emailCount", $context) && ((isset($context["emailCount"]) ? $context["emailCount"] : null) != 0))) {
            // line 533
            echo "                            <li>
                            \t<a class=\" messages1\" href=\"";
            // line 534
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_mymessages");
            echo "\">Messages<span class=\"mess-read\">NEW</span></a>
                            </li>
                            ";
        } else {
            // line 537
            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a class=\"messages\" href=\"";
            // line 538
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_mymessages");
            echo "\">Messages</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        }
        // line 541
        echo "\t\t\t\t\t      \t<li><a class=\"reviews\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_enquiries");
        echo "\">My Job Requests</a></li>
\t\t\t\t\t      \t";
        // line 543
        echo "\t\t\t\t\t      \t<li><a class=\"lead-purchases\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_purchase_leads");
        echo "\">Purchase Leads</a></li>
\t\t\t\t        \t<li><a class=\"orders\" href=\"";
        // line 544
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders");
        echo "\">View All Orders</a></li>
\t\t\t\t        \t<li><a class=\"enquiry\" href=\"";
        // line 545
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_my_reviews");
        echo "\">Reviews and Ratings</a></li>
\t\t\t\t        \t<li><a class=\"lead-management\" href=\"";
        // line 546
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_leads_history");
        echo "\">Lead Management</a></li>
\t\t\t\t\t        <li><a class=\"support\" href=\"";
        // line 547
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_home");
        echo "\">Support</a>
\t\t\t\t\t\t\t\t<!--<ul class=\"ticket-links\">
\t\t\t\t\t\t\t\t\t<li><a class=\"creat-ticket\" href=\"";
        // line 549
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_support");
        echo "\">Create Ticket</a></li>
                                \t<li><a class=\"view-ticket\" href=\"";
        // line 550
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_supportview");
        echo "\">View Ticket</a></li>
\t\t\t\t\t\t\t\t</ul>-->
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 558
    public function block_dashboardgrid($context, array $blocks = array())
    {
        // line 559
        echo "        \t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t\t\t<div class=\"page-title\"><h1>Summary Of Vendor Account</h1>
\t\t\t\t\t\t<a class=\"switch-user switch-user-vendor\" href=\"";
        // line 561
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
        echo "\">Switch to My Customer Profile</a></div>
\t\t\t\t<div class=\"dashboard-container\">

        \t\t<div class=\"overview_block\">

\t\t\t\t";
        // line 566
        $this->displayBlock('dashboard', $context, $blocks);
        // line 603
        echo "
\t\t\t\t";
        // line 604
        $this->displayBlock('statistics', $context, $blocks);
        // line 609
        echo "\t\t\t</div>

        \t\t<div class=\"col-md-12 mid\">
        \t\t\t<div class=\"overview_block block\">
\t\t\t\t\t<div class=\"latest-orders\">
\t\t\t\t\t\t<h2>Latest 10 Orders</h2>
\t\t\t\t\t\t";
        // line 615
        $this->displayBlock('latestorders', $context, $blocks);
        // line 640
        echo "            \t\t\t\t</div>

\t\t\t\t\t<div class=\"latest-enquires\">
\t\t\t\t\t\t<h2>Last 10 Enquires</h2>
\t\t\t\t\t\t";
        // line 644
        $this->displayBlock('enquiries', $context, $blocks);
        // line 669
        echo "\t\t\t\t\t</div>
            \t\t\t</div>
        \t\t</div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t";
    }

    // line 566
    public function block_dashboard($context, array $blocks = array())
    {
        // line 567
        echo "\t\t\t\t\t<div class=\"col-md-6 overview-cotainer\">
\t\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>Total Job Requests</td>
\t\t\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 571
        echo twig_escape_filter($this->env, (isset($context["totalenquirycount"]) ? $context["totalenquirycount"] : null), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>Total Job Requests Accepted</td>
\t\t\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 576
        echo twig_escape_filter($this->env, (isset($context["totalusercountforapproval"]) ? $context["totalusercountforapproval"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>Total Orders</td>
\t\t\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 580
        echo twig_escape_filter($this->env, (isset($context["totalordercount"]) ? $context["totalordercount"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Overall Ratings (Highest is 5) </td>
\t\t\t\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 584
        if (((isset($context["enquiriesvendorcount"]) ? $context["enquiriesvendorcount"] : null) == 1)) {
            echo twig_escape_filter($this->env, (isset($context["enquiriesvendorcount"]) ? $context["enquiriesvendorcount"] : null), "html", null, true);
        } else {
            echo " N/A ";
        }
        echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td colspan=\"2\">
\t\t\t\t\t\t\t\t\t<div>Service Categories</div>
\t\t\t\t\t\t\t\t\t<ul class=\"pull-left overview-catgories\">
\t\t\t\t\t\t\t\t\t\t";
        // line 590
        if (twig_test_empty((isset($context["orders"]) ? $context["orders"] : null))) {
            // line 591
            echo "\t\t\t\t\t\t\t\t\t\t<p>N/A</p>
\t\t\t\t\t\t\t\t\t\t";
        } else {
            // line 593
            echo "\t\t\t\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
                // line 594
                echo "\t\t\t\t\t\t\t\t\t\t<li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "category", array()), "html", null, true);
                echo "</li>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 596
            echo "\t\t\t\t\t\t\t\t\t\t";
        }
        // line 597
        echo "\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</table>
        \t    \t\t\t</div>
\t\t\t\t";
    }

    // line 604
    public function block_statistics($context, array $blocks = array())
    {
        // line 605
        echo "\t\t\t\t\t<div class=\"col-md-6 statistics last\">
         \t\t\t\t\t<div id=\"linechart\" style=\"min-width: 300px; margin: 0 auto\"></div>
            \t\t\t\t</div>
\t\t\t\t";
    }

    // line 615
    public function block_latestorders($context, array $blocks = array())
    {
        // line 616
        echo "\t\t\t\t\t\t<table border=\"1\">
\t\t\t\t\t\t\t<thead><tr>
\t\t\t\t\t\t\t\t<th>Ordered Date</th>
\t\t\t\t\t\t\t\t<th>Order Number</th>
\t\t\t\t\t\t\t\t<th>Status</th>

\t\t\t\t\t\t\t\t<th>Amount</th></tr></thead>
\t\t\t\t                                ";
        // line 623
        if (twig_test_empty((isset($context["enquiries"]) ? $context["enquiries"] : null))) {
            // line 624
            echo "                                                                <tr><td>No Records Found<td></tr>
                                                                 ";
        } else {
            // line 626
            echo "
\t\t\t\t\t\t\t\t\t\t";
            // line 627
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["enquiry"]) {
                // line 628
                echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>";
                // line 629
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 630
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_order_view", array("id" => $this->getAttribute($context["enquiry"], "id", array()))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "ordernumber", array()), "html", null, true);
                echo "</a></td>
\t\t\t\t\t\t\t\t<td>";
                // line 631
                if (($this->getAttribute($context["enquiry"], "status", array()) == 1)) {
                    echo " processed ... ";
                } else {
                    echo " processing ";
                }
                echo "</td>

\t\t\t\t\t\t\t\t<td>";
                // line 633
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "amount", array()), "html", null, true);
                echo "</td>

\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquiry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 637
            echo "                                                       ";
        }
        // line 638
        echo "\t\t\t\t\t\t</table>
\t\t\t\t\t\t";
    }

    // line 644
    public function block_enquiries($context, array $blocks = array())
    {
        // line 645
        echo "\t\t\t\t\t\t<table border=\"1\">
\t\t\t\t\t\t\t\t<thead><tr>
\t\t\t\t\t\t\t\t\t<th>Job Requested Date</th>
\t\t\t\t\t\t\t\t\t<th>Subject</th>
\t\t\t\t\t\t\t\t\t<th>Category</th>
\t\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t\t</tr></thead>
                                     ";
        // line 652
        if (twig_test_empty((isset($context["enquiriesvendor"]) ? $context["enquiriesvendor"] : null))) {
            // line 653
            echo "                                      <tr><td>No Records Found<td></tr>
                                     ";
        } else {
            // line 655
            echo "
\t\t\t\t\t\t\t\t\t";
            // line 656
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiriesvendor"]) ? $context["enquiriesvendor"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["enqvendor"]) {
                // line 657
                echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 658
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enqvendor"], "created", array()), "Y-m-d"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td><strong>";
                // line 659
                if (twig_test_empty($this->getAttribute($context["enqvendor"], "acceptstatus", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "subj", array()), "html", null, true);
                } else {
                    echo "<a href=\" ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_view_consumer", array("id" => $this->getAttribute($context["enqvendor"], "authorid", array()), "eid" => $this->getAttribute($context["enqvendor"], "id", array()))), "html", null, true);
                    echo " \">";
                    if ((twig_length_filter($this->env, $this->getAttribute($context["enqvendor"], "subj", array())) > 20)) {
                        echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enqvendor"], "subj", array()), 0, 20), "html", null, true);
                        echo "... ";
                    } else {
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "subj", array()), "html", null, true);
                        echo " ";
                    }
                    echo "</a>";
                }
                echo "</strong></td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 660
                echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "category", array()), "html", null, true);
                echo "</td>

\t\t\t\t\t\t\t\t\t<td>";
                // line 662
                if (twig_test_empty($this->getAttribute($context["enqvendor"], "acceptstatus", array()))) {
                    echo " <span class=\"not-accepted\">Not Accepted</span> ";
                } else {
                    echo " <span class=\"accepted\">Accepted </span>";
                }
                echo "</td>

\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enqvendor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 666
            echo "                                                                ";
        }
        // line 667
        echo "                        \t\t\t\t\t\t</table>
\t\t\t\t\t\t";
    }

    // line 680
    public function block_footer($context, array $blocks = array())
    {
        // line 681
        echo "    \t<div class=\"footer-dashboard text-center\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"col-sm-6 footer-messages\">Copyright 2013 - 2014 BusinessBid</div>
    \t\t\t<div class=\"col-sm-6 clear-right\"></div>
\t\t\t<div>
    \t</div>
";
    }

    // line 688
    public function block_megamenu($context, array $blocks = array())
    {
        // line 689
        echo "\t<div id=\"megamenuVenSearch\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" ><div><ul id=\"megamenuVenSearchUl\"></ul></div></div>
\t<div id=\"megamenuVenReview\" class=\"submenu menu-mega megamenubg-1 megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');\"><div class=\"review-menu-left\"><ul id=\"megamenuVenReviewUl\"></ul></div><div  class=\"review-menu-right\"><button class=\"btn btn-success write-button\" type=\"button\" >Write a Review</button></div></div>
\t<ul id=\"megamenu1\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenu1','megamenu1Anch');\" onMouseOut=\"javascript:hide_submenu12('megamenu1','megamenu1Anch');\" ></ul>
\t<div id=\"megamenubycat\" class=\"submenu menu-megabycat megamenubg\" onMouseOver=\"javascript:show_submenu('megamenubycat','megaanchorbycat');\" onMouseOut=\"javascript:hide_submenu('megamenubycat','megaanchorbycat');\"><div><ul id=\"megamenubycatUl\"></ul></div><div></div></div>

\t";
        // line 694
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 695
            echo "
\t<script type=\"text/javascript\">
\tdocument.getElementById(\"megamenuVenSearch\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenuVenReview\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenu1\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenubycat\").setAttribute(\"class\", \"submenu menu-megabycat-login megamenubg\");
\t</script>
\t";
        }
        // line 703
        echo "\t";
    }

    public function getTemplateName()
    {
        return "::vendor_dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1138 => 703,  1128 => 695,  1126 => 694,  1119 => 689,  1116 => 688,  1106 => 681,  1103 => 680,  1098 => 667,  1095 => 666,  1081 => 662,  1076 => 660,  1057 => 659,  1053 => 658,  1050 => 657,  1046 => 656,  1043 => 655,  1039 => 653,  1037 => 652,  1028 => 645,  1025 => 644,  1020 => 638,  1017 => 637,  1007 => 633,  998 => 631,  992 => 630,  988 => 629,  985 => 628,  981 => 627,  978 => 626,  974 => 624,  972 => 623,  963 => 616,  960 => 615,  953 => 605,  950 => 604,  941 => 597,  938 => 596,  929 => 594,  924 => 593,  920 => 591,  918 => 590,  905 => 584,  898 => 580,  891 => 576,  883 => 571,  877 => 567,  874 => 566,  865 => 669,  863 => 644,  857 => 640,  855 => 615,  847 => 609,  845 => 604,  842 => 603,  840 => 566,  832 => 561,  828 => 559,  825 => 558,  814 => 550,  810 => 549,  805 => 547,  801 => 546,  797 => 545,  793 => 544,  788 => 543,  783 => 541,  777 => 538,  774 => 537,  768 => 534,  765 => 533,  763 => 532,  757 => 529,  753 => 528,  749 => 527,  745 => 526,  739 => 523,  734 => 520,  731 => 519,  725 => 675,  723 => 558,  720 => 557,  718 => 519,  712 => 515,  709 => 514,  687 => 491,  678 => 485,  673 => 483,  666 => 479,  661 => 477,  655 => 474,  634 => 455,  631 => 454,  625 => 509,  623 => 454,  616 => 449,  613 => 448,  601 => 442,  597 => 440,  594 => 439,  589 => 401,  584 => 398,  581 => 397,  575 => 393,  572 => 392,  560 => 704,  557 => 688,  555 => 680,  551 => 678,  549 => 514,  545 => 512,  542 => 448,  540 => 439,  536 => 437,  526 => 430,  522 => 429,  514 => 424,  503 => 415,  501 => 414,  497 => 412,  495 => 411,  493 => 410,  491 => 409,  489 => 408,  482 => 403,  480 => 401,  477 => 400,  475 => 397,  472 => 396,  470 => 392,  107 => 32,  103 => 31,  99 => 30,  94 => 27,  90 => 25,  86 => 24,  78 => 19,  74 => 18,  69 => 16,  64 => 14,  60 => 13,  56 => 12,  52 => 11,  48 => 10,  42 => 7,  34 => 1,);
    }
}

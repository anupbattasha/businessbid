<?php

/* BBidsBBidsHomeBundle:Home:vendorfaq.html.twig */
class __TwigTemplate_204beb05691dd08462e2e001c815faaf740f5145c867a076c62b46012041b2e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:vendorfaq.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "
<div class=\"container faq-block faq-container\" id=\"venFAQ\">
        <h1>Frequently Asked Questions</h1>

        <div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                         <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How does BusinessBid work?</a>
                         </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                                        The first step involves BusinessBid identifying customers looking for assistance from service professionals like yourselves to complete their specific projects. Our unique lead management system then matches these leads with service providers based on availability, service type and location. We then forward project details via text message instantly so that you can view the job and respond if you are interested.
                        </div>
                </div>
        </div>

        <div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                         <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How much control do I have over the leads I receive?</a>
                         </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                                        You can define your service categories and area preferences to ensure you only receive the types of requests you want. What's even better is you only pay for the leads you choose to accept. For example you might receive 6 lead requests and choose only 2 and you only pay for 2 leads.
                        </div>
                </div>
        </div>




<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                         <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">Do you offer any tools and support to manage my leads?</a>
                         </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                                        Yes. Our personalized dashboard provides an online account with that gives you full control, 24/7. You can viewand manage all the leads provided, leads chosen, outstanding jobs, completed jobs, customer reviews, and even control your budgets by selecting appropriate lead packages. <a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" title=\"\">Sign up</a> today to learn more!
                        </div>
                </div>
        </div>

        <div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                         <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How much do leads cost?</a>
                         </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                                        The price of each lead varies depending on the industry and the service provided. You can learn more about our pricing matrix by contacting our support centre or viewing the pricing packages when you create an online account. You will find that our pricing model is extremely rewarding as usually a single job will cover the price of a whole years advertising and lead purchases.
                        </div>
                </div>
        </div>

        <div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How do I join BusinessBid?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       To begin your <a href=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" title=\"\">Sign up</a>, just click on the sign up button or contact our support centre on 04 42 13 777.
                   </div>
                </div>
        </div>

\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">I am unable to sign in to my account. What should I do?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       If you are having trouble signing in, there are a few things to check.
\t\t\t\t\t   <ul>
\t\t\t\t\t    <li>Double check that you are using the correct email or username and password combination.</li>
\t\t\t\t\t\t<li>If you have forgotten your username or password, you can <a href=\"";
        // line 91
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_forgot_password");
        echo "\" >reset</a> it here.</li>
\t\t\t\t\t\t<li>If you are still having trouble; submit a support ticket and we can see if there is another issue with your account</li>
\t\t\t\t\t   </ul>
                   </div>
                </div>
        </div>

\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How do I update or reset my password?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                        To update your password; sign in to your account. Click My Profile tab and then \"Update My Profile\" and you can update your contact information from there.
\t\t\t\t\t   <ul>
\t\t\t\t\t    <li>If you have forgotten your BusinessBid password and cannot sign in, <a href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "\" title=\"\">click here</a> and enter the email address that is registered to your BusinessBid account. Instructions to reset your password will be sent to that email address.</li>
\t\t\t\t\t\t</ul>
                   </div>
                </div>
        </div>

\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How do I update my email address or personal details?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       To update your email address; sign in to your account. Click My Profile tab and then \"Update My Profile\" and you can update your contact information from there.
                   </div>
                </div>
        </div>

\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-heading panel-question\">
\t\t\t\t  <h4 class=\"panel-title\">
\t\t\t\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapsesixteen\">How do I change my Email/SMS Notifications?</a>
\t\t\t\t\t </h4>
\t\t\t\t </div>
\t\t\t\t<div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
\t\t\t\t\t\t\tTo update these preferences; sign in to your account.  Click My Profile tab and then “Update My Profile” and you can update your contact information from there.
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How do I subscribe to or unsubscribe from BusinessBid newsletters?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       Please email our customer centre support on <a href=\"";
        // line 147
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a>
                   </div>
                </div>

        </div>

\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">What payment methods can be used?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       The following payment methods can be used
\t\t\t\t\t   <ul>
\t\t\t\t\t    <li>Credit Card</li>
\t\t\t\t\t\t<li>PayPal</li>
\t\t\t\t\t\t<li>Cheque</li>
\t\t\t\t\t\t</ul>
                   </div>
                </div>
        </div>

\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">Can I dispute a customer lead?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       Whilst we ensure you receive the highest quality lead, if you would like to discuss or dispute a particular lead you can send a request through your account.
                   </div>
                </div>
        </div>


\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">What if a customer has posted a negative review about my business?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       <p>We know how disappointing it can be to receive a negative review on our site. BusinessBid's default position is not to delete, censor, or edit reviews. We value honesty and transparency, and therefore a review will only be removed if there was no quote sent to the customer or if the review violates our <a href=\"\" data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a>. We do, however, encourage you to respond to the review and/or reach out to the customer if you feel there is an opportunity to do better. Review responses are best when they succinctly state your thoughts in friendly yet professional tone. A quality response should explain any points in the review you want to clarify for future customers.</p>
<p>If you would to discuss a particular customer review or would like BusinessBid assistance to respond to thet review; you can send a request by raising a ticket through your account.</p>
                   </div>
                </div>
        </div>

\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">How do change my Email/SMS Notifications?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                      To update these preferences; sign in to your account. Click My Profile tab and then \"Update My Profile\" and you can update your contact information from there.
                   </div>
                </div>
        </div>


\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">The customer requests are not relevant for the service I provide. What do I do?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       Please ensure you have chosen the service you provide carefully in your profile. To review/update these preferences; sign in to your account and go to \"My Profile\". Alternatively you can call us or email us on <a href=\"";
        // line 221
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a> and on of our vendor support members will assist you.
                   </div>
                </div>
        </div>

\t\t\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">Can I add more than one service to my account?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       Absolutely you can. To add more services sign in to your account and go to \"My Profile\" (provide appropriate link). Alternatively you can call us or email us on <a href=\"";
        // line 234
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a> and on of our vendor support members will assist you.
                   </div>
                </div>
        </div>

\t\t\t\t<div class=\"panel\">
                <div class=\"panel-heading panel-question\">
                  <h4 class=\"panel-title\">
                     <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\">I am not receiving many customer requests, what can I do?</a>
                  </h4>
                </div>
                <div id=\"collapseE\">
                   <div class=\"panel-body panel-answer\">
                       If you are receiving fewer customer requests than normal; it could be that there are fewer customer requests in your service category, potentially due to natural market fluctuation or seasonality. You can try opting into more service categories. If this is still a concern please contact our support team at <a href=\"";
        // line 247
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a> and one of members will address it accordingly.
                   </div>
                </div>
        </div>





\t\t<div>
\t\t<br>
\t\t\t<h4>Still does not answer your question?</h4>
\t\t\t<h5>Please email us on <a href=\"";
        // line 259
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a> or contact the vendor support line on 04 42 13 777</h5>
\t\t</div>

</div>

</div>

<script>
\$('data-toggle=\"modal')
  e.preventDefault()
</script>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:vendorfaq.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  314 => 259,  299 => 247,  283 => 234,  267 => 221,  190 => 147,  148 => 108,  128 => 91,  109 => 75,  80 => 49,  37 => 8,  34 => 7,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:are_youa_vendor.html.twig */
class __TwigTemplate_d2b5348f361f61278918601d46d7910de1fd8a47014cb6e9aede4ca08637ba70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:are_youa_vendor.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Service Professional Customer Leads";
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        // line 7
        echo "       <div class=\"leads_area\">
           <div class=\"inner_leads\">
               <div class=\"leads_inner_img\">
                   <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/atm_card.png"), "html", null, true);
        echo "\" alt=\"Our ATM CARD IMAGES\">
               </div>
               <div class=\"leads_inner_content\">
                   <h1>BUSINESSBID LEADS</h1>
                   <p>BusinessBid uses a pay-as-you-go model where you only pay for leads you are interested in. You get a detailed job requirement and pay only if you would like to contact the customer for a meeting and a quote. With the BusinessBid model you have complete freedom and choice to take up jobs that work best for you.</p>
              <a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" class=\"sign_up_btn\">SIGN UP NOW</a>
               </div>
           </div>
       </div>
       <div class=\"purchase_area\">
           <div class=\"inner_purchase\">
               <h2>How do I purchase BusinessBid Leads?</h2>
               <div class=\"inner_bid_lids\">
                   <img src=";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/home.jpg"), "html", null, true);
        echo " alt=\"Home Icon\">
                   <h5>Choose your</h5>
                   <h5>service categories</h5>
               </div>
               <div class=\"bid_arrows\">
                   <img src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo " alt=\"arrow\">
               </div>
               <div class=\"inner_bid_lids\">
                   <img src=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/round_tir.png"), "html", null, true);
        echo " alt=\"Rounded Icon\">
                   <h5>Choose your</h5>
                   <h5>lead package</h5>
               </div>
               <div class=\"bid_arrows\">
                   <img src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo " alt=\"arrow\">
               </div>
               <div class=\"inner_bid_lids\">
                   <img src=";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper.png"), "html", null, true);
        echo " alt=\"Preper Icon\">
                   <h5>Convenient</h5>
                   <h5>payment options</h5>
               </div>
           </div>
       </div>
       <div class=\"service_categories_area\">
           <div class=\"inner-service-categories\">
               <div class=\"inner-service-categories_content\">
                   <h2>01</h2>
                   <h3>Choose your service categories</h3>
                   <p>As UAE’s largest service provisioning portal, BusinessBid operates in a multitude of different industries. You are able to receive jobs in any of these service categories provided that you or your company are qualified to take on these projects. Just select your categories from a list and add them to your portfolio.</p>
               </div>
               <div class=\"inner-service-categories_img\">
                   <img src=";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/service-caregory.png"), "html", null, true);
        echo " alt=\"service categories\">
               </div>
           </div>
       </div>
       <div class=\"lead_package_area\">
           <div class=\"inner_lead_package\">
               <div class=\"lead_package_area_img\">
                   <img src=";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/packeges.png"), "html", null, true);
        echo " alt=\"Lead Package Area\">
               </div>
               <div class=\"lead_package_area_content\">
                   <h2>02</h2>
                   <h3>Choose your lead package</h3>
                   <p>The price of each lead varies depending on the industry and the number of leads purchased. Packages of 10, 25, 50 and 100 Leads can be purchased, each with incremental levels of discount.  As an example, the silver package of 25 leads comes with a 5% discount, while the Platinum package of 100 leads gets you a 15% discount.</p>
               </div>
           </div>
       </div>
       <div class=\"payment_categories_area\">
           <div class=\"inner-payment-categories\">
               <div class=\"inner-payment-categories_content\">
                   <h2>03</h2>
                   <h3>Convenient payment options</h3>
                   <p>Once you have chosen your service categories and lead packages, you are able to pay for them either online through credit card or Paypal, or offline with a cheque. Once your payment is received your account is topped up and you can begin receiving and accepting customer leads.</p>
               </div>
               <div class=\"inner-payment-categories_img\">
                   <img src=";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tream.png"), "html", null, true);
        echo " alt=\"payment categories\">
               </div>
           </div>
       </div>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
   ";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:are_youa_vendor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 77,  126 => 60,  116 => 53,  99 => 39,  93 => 36,  85 => 31,  79 => 28,  71 => 23,  60 => 15,  52 => 10,  47 => 7,  44 => 6,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }
}

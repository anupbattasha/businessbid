<?php

/* BBidsBBidsHomeBundle:Home:node6.html.twig */
class __TwigTemplate_cbda04b4579c7352ae8ae618d4c4a4a02f10a97065f894dcd0bbb3f6ba9c8aea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node6.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container faq-container\">
<div class=\"breadcrum\">Home > FAQs</div>
<div class=\"page-title\"><h1>Frequently Asked Questions</h1></div>
 <h2>About BusinessBid</h2>
<div class=\"panel-group\" id=\"accordion\">
<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">How can BusinessBid help me with my project?</a>
\t  </h4>
\t </div>
 <div id=\"collapseOne\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\t We are UAE’s biggest and largest service portal covering 700+ individual services across 

\t\tthe residential, commercial, corporate, automotive and lifestyle industries. Our team brings 

\t\ta cumulative total of 50+ years of service industry experience from many different global 

\t\tmarketplaces. Our unique lead management system, our comprehensive Review section, along with 

\t\tan extensive <a href=\"/resource_centre\">Resource Centre</a> with a library of useful articles and information provides our users 

\t\twith an effective tool to find the most suitable vendor.  
  </div>
</div>
</div>
<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\">Does BusinessBid operate in my city?</a>
\t  </h4>
\t </div>
 <div id=\"collapseTwo\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\tYes, BusinessBid vendors operate across all cities within the UAE. 
  </div>
</div>
</div>
<h2>Advantages of Using BusinessBid</h2>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseThree\">What are my benefits as a BusinessBid user? </a>
\t </h4>
  </div>
 <div id=\"collapseThree\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\t BusinessBid’s aim is to provide the consumer with a one-stop online portal which will provide 

\t\tthem with reliable, prompt, high quality and value for money quotes from service professionals 

\t\tacross residential, commercial, automotive and lifestyle related projects and requirements. Some of 

\t\tthe key benefits are: 
\t\t<ul>

\t\t <li> Convenience: Allows you to receive multiple quotes from various vendors with a single 

\t\tform and eliminates the hassle of multiple searches. </li>

\t\t <li> Competitive Pricing: Receive at least three quotations from vendors so you know you 

\t\thave a choice to get the most competitive pricing! </li>

\t\t <li> Speed: As vendors are competing with each other you can be assured that you will get a 

\t\tswift response. No more waiting around! </li>

\t\t <li> Reputation: All vendors are pre-qualified and verified to ensure they are reputable and 

\t\tskilled. </li>

\t\t <li> Quality: Our vendor rating system focusses on vendors delivering high quality service 

\t\tand a positive user experience.</li>

\t\t <li> Free to Use: The website is completely free for users with no search or membership fees 

\t\tcharged ever!  </li>

\t\t <li> Portal: Get access to your personal services dashboard where you can view and manage 

\t\tyour jobs from anywhere, anytime. </li>

\t\t</ul>
 </div>
</div>
</div>
<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseFour\"> How can BusinessBid ensure that I find the right service vendor?</a>
\t  </h4>
\t </div>
<div id=\"collapseFour\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\t\tBusinessBid matches your project with the most suitable pre-screened vendors using our unique 

\t\t\tvendor matching technology. The process is extremely simple and all that you are required to do is 

\t\t\tfill out a simple form describing your project. You will then receive quotes from up to three vendors 

\t\t\twho you can screen based on a personal meeting as well as by reviewing actual reviews & ratings 

\t\t\tprovided by previous users based on their experience with the vendor.
\t</div>
</div>
</div>


<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseFive\"> Is there an obligation to use vendors recommended by BusinessBid?</a>
\t  </h4>
\t </div>
<div id=\"collapseFive\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\"> Not at all. However, our process ensures that you have the choice to use the best service providers for your project needs.
   </div>
</div>
</div>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseSix\"> Does BusinessBid provide any due diligence before recommending vendors?</a>
\t  </h4>
\t </div>
<div id=\"collapseSix\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\tBusinessBid performs an extensive due diligence process prior to selecting and recommending any vendor. We ensure that we bring reliable, reputable and highly skilled service professionals into  our network that can deliver high quality projects consistently and create a positive experience for our users.
 </div>
</div>
</div>
<h2> Feedback & Reviews from Real Users. </h2>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseSeven\"> What are Ratings & Reviews? Are they relevant?</a>
\t\t </h4>
\t </div>
<div id=\"collapseSeven\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">

\t\t\t<p> Ratings & Reviews are an important tool we offer to our users. They help us keep track of the 

\t\t\tquality of service provided by our member service professionals. For all requests you place with 

\t\t\tBusinessBid, you'll be asked to submit Ratings & Reviews for the service professionals you choose 

\t\t\tbased on your experience with them. When you do, you not only help other consumers make the 

\t\t\tright choice, you also contribute to the overall quality of service offered. Click here for a Ratings & 

\t\t\tReviews sample.</p>

\t\t\t<p>We at BusinessBid value our customers feedback. We use the information provided by users on the 

\t\t\texperiences they have had to rate a vendor based on metrics such as quality, pricing and overall 

\t\t\tcustomer service. This information helps other users to make an informed decision in selecting 

\t\t\tspecific vendors for their projects. Also, vendor feedback helps them improve their quality of service 

\t\t\tand ensures they can deliver an outstanding customer experience. You can view a sample Ratings & 

\t\t\tReviews by clicking here. <a href=\"";
        // line 177
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Rating & Reviews</a></p>
     </div>
    </div>
</div>
<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEight\">  When can I provide my vendor feedback to BusinessBid?</a>
\t\t </h4>
\t </div>
<div id=\"collapseEight\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">

\t The ideal time to provide feedback would be when the project has been completed. You will 

\treceive notification from our team that prompts you to complete a short feedback form on our website.
\t</div>
</div>
</div>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseNine\"> What features does my online account provide? </a>
\t\t </h4>
\t </div>
<div id=\"collapseNine\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\t Once users sign up, they have access to an online account that is located on the header banner of 

\t\tour homepage and called “My BusinessBid”. Clicking on this will lead you to the customer dashboard 

\t\t<a href=\"";
        // line 209
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\">Click here to register</a> which includes several useful features such as the following:

\t\t<ul>

\t\t<li> Update your personal details and profile.</li>

\t\t<li> View and manage your project requests</li>

\t\t<li> Complete your reviews & feedback </li>
\t\t</ul>
\t</div>
</div>
</div>
<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTen\">Where can I find useful information for my projects?</a>
\t\t </h4>
\t </div>
<div id=\"collapseTen\" class=\"panel-collapse collapse\">
   <div class=\"panel-body panel-answer\">
\t\tWe understand that your project is very important to you, and you want it done right. To ensure you 

\t\thave the best information available, we have created an ever expanding library of useful articles and 

\t\trelated information across all our service categories. You can access this information by clicking on 

\t\tResource Centre on our homepage. <a href=\"http://www.businessbid.ae/resource_centre\">Resource center</a>
\t</div>
</div>
</div>

<h2>Vendor Qualification</h2>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEleven\"> How do we qualify our vendors?</a>
\t\t </h4>
\t </div>
\t<div id=\"collapseEleven\" class=\"panel-collapse collapse\">
\t   <div class=\"panel-body panel-answer\">
\t\t\t\tBefore finalizing a vendor, users can look at customer feedback and reviews to establish that 
\t
\t\t\t\tthe vendor is reputable. In addition, BusinessBid conducts regular vendor due diligence analysis 
\t
\t\t\t\tto confirm that the vendor business is in operation and is in good standing.
\t\t</div>
\t</div>
</div>


<!--new faq starting here-->
<h2>Account & General</h2>
<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwelve\"> I am unable to sign in to my account. What should I do?</a>
\t\t </h4>
\t </div>
\t<div id=\"collapseTwelve\" class=\"panel-collapse collapse\">
\t   <div class=\"panel-body panel-answer\">
\t\t\t\tIf you are having trouble signing in, there are a few things to check.
\t\t\t\t<ul>
\t\t\t\t<li>Double check that you are using the correct email or username and password combination.</li>
\t\t\t\t<li>If you have forgotten your username or password, you can <a href=\"";
        // line 274
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_forgot_password");
        echo "\" >reset</a> it here.</li>
\t\t\t\t<li>If you are still having trouble; submit a support ticket and we can see if there is another issue with your account.</li>\t\t\t\t\t\t
\t\t\t\t</ul>
\t\t</div>
\t</div>
</div>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapsethirteen\">How do I update or reset my password?</a>
\t\t </h4>
\t </div>
\t<div id=\"collapsethirteen\" class=\"panel-collapse collapse\">
\t   <div class=\"panel-body panel-answer\">
\t\t\t\tTo update your password; sign in to your account.  Click My Profile tab and then “Update My Profile” and you can update your contact information from there.
\t\t\t\t<ul>
\t\t\t\t\t<li>If you have forgotten your BusinessBid password and cannot sign in, <a href=\"";
        // line 291
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_forgot_password");
        echo "\" >click here</a> and enter the email address that is registered to your BusinessBid account. Instructions to reset your password will be sent to that email address.</li>
\t\t\t\t</ul>
\t\t</div>
\t</div>
</div>

<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapsefourteen\"> How do I update my email address or personal details?</a>
\t\t </h4>
\t </div>
\t<div id=\"collapsefourteen\" class=\"panel-collapse collapse\">
\t   <div class=\"panel-body panel-answer\">
\t\t\t\tTo update your email address; sign in to your account.  Click My Profile tab and then “Update My Profile” and you can update your contact information from there.
\t\t</div>
\t</div>
</div>


<div class=\"panel\">
    <div class=\"panel-heading panel-question\">
      <h4 class=\"panel-title\">
\t\t <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapsefifteen\">How do I subscribe to or unsubscribe from BusinessBid newsletters?</a>
\t\t </h4>
\t </div>
\t<div id=\"collapsefifteen\" class=\"panel-collapse collapse\">
\t   <div class=\"panel-body panel-answer\">
\t\t\t\tPlease email our customer centre support on <a href=\"";
        // line 319
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a>
\t\t</div>
\t</div>
</div>


<!--new faq ending here-->


\t\t<div>
\t\t<br>
\t\t<br>
\t\t\t<h4>Still does not answer your question?</h4>
\t\t\t<h5>Please email us on <a href=\"";
        // line 332
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">support@businessbid.ae</a> or contact the customer support line on 04 42 13 777.</h5>
\t\t</div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node6.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  378 => 332,  362 => 319,  331 => 291,  311 => 274,  243 => 209,  208 => 177,  37 => 8,  34 => 7,  29 => 3,  11 => 1,);
    }
}

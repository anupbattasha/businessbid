<?php

/* BBidsBBidsHomeBundle:User:leads_plan.html.twig */
class __TwigTemplate_e129392a2b1fc6567df421d1801171fa11620254912322447bac69796f24aab1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leads_plan.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script>
\$('.bbspop').popover();
\$('.bbspop').popover({ trigger: \"hover\" });
</script>

<div class=\"container\">

<div class=\"lead-packages-page-title\"><h1>Select Your Lead Packages</h1></div>
<div>
    ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 14
            echo "
    <div class=\"alert alert-danger\">";
            // line 15
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 20
            echo "
    <div class=\"alert alert-success\">";
            // line 21
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
    ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
    <div class=\"alert alert-success\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "</div>
<div id=\"spinner\" style=\"margin: 0px; padding: 0px; position: fixed; right: 0px;
  top: 0px; width: 100%; height: 100%; background-color: #666666; z-index: 30001;
  opacity: .8; filter: alpha(opacity=70); display: none\">
  <p style=\"position: absolute; top: 30%; left: 45%; color: White;\">
    Please wait ...<img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/waiting.gif"), "html", null, true);
        echo "\" alt=\"Loading\">
  </p>
</div>
\t\t<table class=\"table package\">
\t\t\t\t<thead>
\t\t\t\t\t";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["selectedCatsArray"]) ? $context["selectedCatsArray"] : null));
        foreach ($context['_seq'] as $context["keys"] => $context["value"]) {
            // line 41
            echo "\t\t\t\t\t<tr>";
            // line 42
            echo "\t\t\t\t\t\t<th colspan=\"6\">
\t\t\t\t\t\t<h5 class=\"gray-bg-head\"> <span>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "category", array()), "html", null, true);
            echo "</span></h5>
\t\t\t\t\t\t\t<div class=\"featuresclass bronze\">
\t\t\t\t\t\t\t\t<h1 class=\"bro\">Bronze</h1>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h2>10</h2>
\t\t\t\t\t\t\t\t\t\t<h5>Leads</h5>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h6>AED</h6>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"bro\">";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "bPrice", array()), "html", null, true);
            echo "<span> / Lead</span></h4>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t<span class=\"select-package\" onClick=\"updatePrice(this);return false;\" id=\"category";
            // line 56
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "_1\"><a>Select Package</a></span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"featuresclass silver\">
\t\t\t\t\t\t\t\t<h1 class=\"sil\">Silver</h1>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h2>25</h2>
\t\t\t\t\t\t\t\t\t\t<h5>Leads</h5>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h6>AED</h6>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"sil\">";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "sPrice", array()), "html", null, true);
            echo "<span> / Lead</span></h4>
\t\t\t\t\t\t\t\t\t\t<h6><strong>SAVE 5%</strong></h6>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t<span class=\"select-package\" onClick=\"updatePrice(this);return false;\" id=\"category";
            // line 71
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "_2\"><a>Select Package</a></span>
\t \t\t\t\t        </div>
\t\t\t\t\t\t\t<div class=\"featuresclass gold\">
\t\t\t\t\t\t\t\t<h1 class=\"gold\">Gold</h1>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h2>50</h2>
\t\t\t\t\t\t\t\t\t\t<h5>Leads</h5>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h6>AED</h6>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"gold\">";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "gPrice", array()), "html", null, true);
            echo "<span> / Lead</span></h4>
\t\t\t\t\t\t\t\t\t\t<h6><strong>SAVE 10%</strong></h6>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t<span class=\"select-package\" onClick=\"updatePrice(this);return false;\" id=\"category";
            // line 86
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "_3\" ><a>Select Package</a></span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"featuresclass platinum\">
\t\t\t\t\t\t\t<h1 class=\"plat\" >Platinum</h1>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h2>100</h2>
\t\t\t\t\t\t\t\t\t\t<h5>Leads</h5>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<h6>AED</h6>
\t\t\t\t\t\t\t\t\t\t<h4 class=\"plat\">";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "pPrice", array()), "html", null, true);
            echo "<span> / Lead</span></h4>
\t\t\t\t\t\t\t\t\t\t<h6><strong>SAVE 15%</strong></h6>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t<span class=\"select-package\" onClick=\"updatePrice(this);return false;\" id=\"category";
            // line 101
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "_4\"><a>Select Package</a></span>
\t\t\t\t\t\t</div></th>
\t\t\t\t    </tr>";
            // line 104
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 105
        echo "\t\t\t</thead>
\t\t</table>

\t<div class=\"col-sm-12 plan-details\">
\t\t<h2>You are subscribing for</h2>
\t\t<div class=\"plan-table\">
\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">
\t\t\t\t<thead>
\t\t\t\t  <tr>
\t\t\t\t\t<td width=\"216\">Package Details</td>
\t\t\t\t\t<td width=\"197\">Price</td>
\t\t\t\t\t<td width=\"189\">Discount</td>
\t\t\t\t\t<td width=\"191\">Total</td>
\t\t\t\t  </tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t";
        // line 121
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["selectedCatsArray"]) ? $context["selectedCatsArray"] : null));
        foreach ($context['_seq'] as $context["keys"] => $context["value"]) {
            // line 122
            echo "\t\t\t\t  <tr>
\t\t\t\t\t<td id=\"categoryName_";
            // line 123
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["value"], "category", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td id=\"actualPrice_";
            // line 124
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "\"></td>
\t\t\t\t\t<td id=\"discountPrice_";
            // line 125
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "\"></td>
\t\t\t\t\t<td id=\"totalPrice_";
            // line 126
            echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
            echo "\"></td>
\t\t\t\t  </tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "\t\t\t\t";
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            // line 130
            echo "\t\t\t\t  <tr>
\t\t\t\t\t<td>Account Setup Fee <span><a tabindex=\"0\" class=\"popover-icon\" role=\"button\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"bottom\" data-trigger=\"hover\" title=\"The Account Setup fee is one time charge and includes:\" data-content=\"<ul><li>Creation of dedicated webpage with a full business profie.</li> <li>Access to 24/7 online dashboard with reporting features.</li> <li>Compilation and publication of 5 recent customer reviews.</li></ul>\"></a></span></td>
\t\t\t\t\t<td>AED ";
            // line 132
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>AED 0</td>
\t\t\t\t\t<td>AED ";
            // line 134
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t  </tr>
\t\t\t\t  </tbody>
\t\t\t  \t";
        }
        // line 138
        echo "\t\t\t\t<tfoot>
\t\t\t\t   <tr>
\t\t\t\t\t<td><strong>Grand Total </strong></td>
\t\t\t\t\t<td>&nbsp;</td>
\t\t\t\t\t<td>&nbsp;</td>
\t\t\t\t\t<td id=\"grandTotal\"><strong>";
        // line 143
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            echo "AED ";
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
        }
        echo "</strong></td>
\t\t\t\t  </tr>
\t\t\t\t</tfoot>

\t\t\t</table>
\t\t</div>
\t\t<div class=\"plan-action-links\">
\t\t\t<span class=\"back-package\"><a href=\"#\" class=\"btn btn-success\">Back</a></span>
\t\t\t<span class=\"proceed-package\"><a class=\"btn btn-success\" id=\"proceedPayment\">Proceed to Payment</a></span>
\t\t</div>
\t</div>
</div>
<script type=\"text/javascript\">
var planObj    = ";
        // line 156
        echo twig_jsonencode_filter((isset($context["selectedCatsArray"]) ? $context["selectedCatsArray"] : null));
        echo ";
var grandTotal = [];
";
        // line 158
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            // line 159
            echo "\tvar OTC = ";
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo ";
";
        } else {
            // line 161
            echo "\tvar OTC = 0;
";
        }
        // line 163
        echo "var maxCats    = ";
        echo twig_escape_filter($this->env, (isset($context["totalSelectedCats"]) ? $context["totalSelectedCats"] : null), "html", null, true);
        echo ";
var payUrl     = \"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_pymt_form", array("purchaseID" => (isset($context["purchaseID"]) ? $context["purchaseID"] : null))), "html", null, true);
        echo "\";
var purchaseID = \"";
        // line 165
        echo twig_escape_filter($this->env, (isset($context["purchaseID"]) ? $context["purchaseID"] : null), "html", null, true);
        echo "\";
var redeemUrl  = \"";
        // line 166
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_store_lead_plan");
        echo "\";
var disabled \t= false;
</script>
<script type=\"text/javascript\" src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/leads.plan.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t\t\$(function () {
  \$('[data-toggle=\"popover\"]').popover()
})
\t</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leads_plan.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  342 => 169,  336 => 166,  332 => 165,  328 => 164,  323 => 163,  319 => 161,  313 => 159,  311 => 158,  306 => 156,  287 => 143,  280 => 138,  273 => 134,  268 => 132,  264 => 130,  261 => 129,  252 => 126,  248 => 125,  244 => 124,  238 => 123,  235 => 122,  231 => 121,  213 => 105,  207 => 104,  202 => 101,  195 => 97,  181 => 86,  174 => 82,  160 => 71,  153 => 67,  139 => 56,  133 => 53,  120 => 43,  117 => 42,  115 => 41,  111 => 40,  103 => 35,  96 => 30,  87 => 27,  84 => 26,  80 => 25,  77 => 24,  68 => 21,  65 => 20,  61 => 19,  58 => 18,  49 => 15,  46 => 14,  42 => 13,  31 => 4,  28 => 3,  11 => 1,);
    }
}

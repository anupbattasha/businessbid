<?php

/* BBidsBBidsHomeBundle:User:leads_paymentform.html.twig */
class __TwigTemplate_f97b9b481553203424660acf761606a77bb63f92382b54d16c1576047640312c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "
\t<style type=\"text/css\">
\t.camouflage{
\t\tdisplay: none;
\t}
\t.exposed{
\t\tdisplay: block;
\t}
\t.errorinput{ content: attr(title); color: red; margin-left: 0.6rem; }
\t</style>
\t<script>
\t\$(document).ready(function () {
\tvar token=readCookie('token');
    if (token>2) {
        eraseCookie('token');
    } else {

    }
});

\t\$(document).ready(function(){
\t
\$('.bbspop').popover();
\$('.bbspop').popover({ trigger: \"hover\" });
    \$('[data-toggle=\"popover\"]').popover();
    \$('#form_ccExpDate_month,#form_ccExpDate_year').width(60);
    \$('#form_ccSubmit').hide();
    \$(\"#form_ccNumber\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCNum').html('Enter card number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorCCNum').html('Numbers only !');
        } else {
            \$('small#errorCCNum').html('');
            var result = getCreditCardType(ccVal);
            if(result == 'unknown'){
                \$('small#errorCCNum').html('Only Master and Visa card allowed');
            } else {
                \$('small#errorCCNum').html('');
            }

            var result = isValidCreditCard(type, ccnum);
            if(result) {
                \$('small#errorCCNum').html('');
            } else {
                \$('small#errorCCNum').html('Invalid card number');
            }
        }
    })
    \$(\"#form_ccName\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCName').html('Enter card holder\\'s name.');
        } else if(!isNaN(ccVal)) {
            \$('small#errorCCName').html('Numbers not allowed !');
        } else {
            \$('small#errorCCName').html('');
        }
    })
    \$(\"#form_ccCVV\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCVV').html('Enter cvv number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorCCVV').html('Numbers only!');
        } else if (ccVal.length > 3){
            \$('small#errorCCVV').html('Only 3 digits allowed.');
        } else {
            \$('small#errorCCVV').html('');
        }
    })
    \$(\"input[name='payType'],div#editAdress\").click(function (){
        var payType = \$(\"input[name='payType']:checked\").val();
        if(payType == 'check pickup') {
            \$( 'div#checkFillForm' ).toggleClass( \"camouflage\" );
            \$( 'div#checkAddShow' ).toggleClass( \"camouflage\" );
        }
        else if(payType == 'cash pickup') {
            \$( 'div#cashFillForm' ).toggleClass( \"camouflage\" );
            \$( 'div#cashAddShow' ).toggleClass( \"camouflage\" );
        }
    });
    \$('.btn-success').click(function () {
        var payType = \$(\"input[name='payType']:checked\").val();//\$(\"input:radio[name='payType']\").val();
        alert('Your '+payType+' payment is being processed. Please click OK and do not leave this page');
        if(payType == 'cc') {
            \$( \"#ccForm\" ).submit();
        } else if (payType == 'paypal') {
             \$( \"#payPalForm\" ).submit();
        } else if (payType == 'check pickup') {
            \$( \"#checkForm\" ).submit();
        } else if (payType == 'cash pickup') {
            \$( \"#cashForm\" ).submit();
        }
    })
    \$('#form_ccExpDate_month').change(function () {
        var ccmonth = \$(this).val();
        if(ccmonth == '') {
            \$('small#errorCCExp').html('Select expiry month.');
        } else {
            \$('small#errorCCExp').html('');
        }
    })
    \$('#form_ccExpDate_year').change(function () {
        var ccmonth = \$(this).val();
        if(ccmonth == '') {
            \$('small#errorCCExp').html('Select expiry year.');
        } else {
            \$('small#errorCCExp').html('');
        }
    })

    \$(\"#form_contactnumber\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorConNum').html('Enter Mobile Contact Number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorConNum').html('Numbers only!');
        } else {
            \$('small#errorConNum').html('');
        }
    })
});
function getCreditCardType(accountNumber)
{

  //start without knowing the credit card type
  var result = \"unknown\";

  //first check for MasterCard

  if (/^5[1-5]/.test(accountNumber))
  {
    result = \"mastercard\";
  }

  //then check for Visa
  else if (/^4/.test(accountNumber))
  {
    result = \"visa\";
  }

  //then check for AmEx
  /*else if (/^3[47]/.test(accountNumber))
  {
    result = \"amex\";
  }*/

  return result;
}

function isValidCreditCard(type, ccnum)
{
/* Visa: length 16, prefix 4, dashes optional.
Mastercard: length 16, prefix 51-55, dashes optional.
Discover: length 16, prefix 6011, dashes optional.
American Express: length 15, prefix 34 or 37.
Diners: length 14, prefix 30, 36, or 38. */

  var re = new Regex({ \"visa\": \"/^4\\d{3}-?\\d{4}-?\\d{4}-?\\d\",
                       \"mc\": \"/^5[1-5]\\d{2}-?\\d{4}-?\\d{4}-?\\d{4}\$/\",
                       \"disc\": \"/^6011-?\\d{4}-?\\d{4}-?\\d{4}\$/\",
                       \"amex\": \"/^3[47]\\d{13}\$/\",
                       \"diners\": \"/^3[068]\\d{12}\$/\"}[type.toLowerCase()])

   if (!re.test(ccnum)) return false;
   // Remove all dashes for the checksum checks to eliminate negative numbers
   ccnum = ccnum.split(\"-\").join(\"\");
   // Checksum (\"Mod 10\")
   // Add even digits in even length strings or odd digits in odd length strings.
   var checksum = 0;
   for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
      checksum += parseInt(ccnum.charAt(i-1));
   }
   // Analyze odd digits in even length strings or even digits in odd length strings.
   for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
      var digit = parseInt(ccnum.charAt(i-1)) * 2;
      if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
   }
   if ((checksum % 10) == 0) return true; else return false;
}
\t
\t</script>
\t
<div class=\"container\" >
<div>
    ";
        // line 191
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 192
            echo "
    <div class=\"alert alert-danger\">";
            // line 193
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 196
        echo "
    ";
        // line 197
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 198
            echo "
    <div class=\"alert alert-success\">";
            // line 199
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 202
        echo "
    ";
        // line 203
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 204
            echo "
    <div class=\"alert alert-success\">";
            // line 205
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 208
        echo "</div>

<div class=\"col-sm-12\">
\t<div class=\"package-details\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" id=\"selectpackagedetails\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Package Details </td>
\t\t\t\t\t<td width=\"197\">Price</td>
\t\t\t\t\t<td width=\"189\">Discount</td>
\t\t\t\t\t<td width=\"189\">Total</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 222
        $context["OTC"] = 0;
        // line 223
        echo "\t\t\t\t";
        $context["grandTotal"] = 0;
        // line 224
        echo "\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["purchasingLeads"]) ? $context["purchasingLeads"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lead"]) {
            // line 225
            echo "\t\t\t\t\t";
            $context["priceArray"] = twig_split_filter($this->env, $this->getAttribute($context["lead"], "price", array()), " - ");
            // line 226
            echo "
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">";
            // line 228
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "plan", array()), "html", null, true);
            echo " Package: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "category", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t";
            // line 229
            $context["multiplier"] = $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
            // line 230
            echo "\t\t\t\t\t<td width=\"197\">AED ";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">
\t\t\t\t\t\t";
            // line 232
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                // line 233
                echo "\t\t\t\t\t\t\t";
                $context["selPlan"] = $this->getAttribute((isset($context["leadsPack"]) ? $context["leadsPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
                // line 234
                echo "\t\t\t\t\t\t\tAED ";
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")) - ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array"))), "html", null, true);
                echo "
\t\t\t\t\t\t";
            } else {
                // line 236
                echo "\t\t\t\t\t\t\tAED 0
\t\t\t\t\t\t";
            }
            // line 238
            echo "\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 239
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")));
            } else {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)));
            }
            echo "</td>

\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lead'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 243
        echo "\t\t\t\t";
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            // line 244
            echo "\t\t\t\t\t";
            $context["OTC"] = (isset($context["registerFee"]) ? $context["registerFee"] : null);
            // line 245
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Account Setup Fee <span><a tabindex=\"0\" class=\"popover-icon\" role=\"button\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"bottom\" data-trigger=\"hover\" title=\"The Account Setup fee is one time charge and includes:\" data-content=\"<ul><li>Creation of dedicated webpage with a full business profie.</li> <li>Access to 24/7 online dashboard with reporting features.</li> <li>Compilation and publication of 5 recent customer reviews.</li></ul>\"></a></span>
\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"197\">AED ";
            // line 248
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">AED 0</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 250
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t</tbody>
\t\t\t\t";
        }
        // line 254
        echo "\t\t\t\t<tfoot>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Grand Total</td>
\t\t\t\t\t<td width=\"197\"></td>
\t\t\t\t\t<td width=\"189\"></td>
\t\t\t\t\t<td width=\"189\"><strong>AED ";
        // line 259
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</strong></td>
\t\t\t\t</tr>
\t\t\t\t</tfoot>
\t\t</table>
\t</div>
\t<div class=\"paymentmethod\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\">Payment Method</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\"><strong>How would you like to pay <span class=\"payment_value\">";
        // line 273
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</span> AED ?</td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t</table>
\t\t<div class=\"credit-card-option col-sm-12 position-in-opt left-clear\" style=\"width:100%; float:left;\">
\t\t\t<div class=\"col-sm-12 position-in-opt\"><input type=\"radio\" name=\"payType\" checked=\"checked\" value=\"cc\" />Credit Card</div>

\t\t\t<div class=\"col-sm-9\">

        <script src=\"https://checkout.com/cdn/js/checkout.js\"></script>

            <form class=\"payment-form\">
                <script>
                  var publickey = 'pk_04d5b10a-845e-4267-866b-7a16910dd048'; //PublicKey provided by Checkout

                  Checkout.render({
                      debugMode: true,
                      namespace: 'CheckoutIntegration',
                      publicKey: publickey,
                      paymentToken: \"";
        // line 292
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\",
                      customerEmail: '";
        // line 293
        echo twig_escape_filter($this->env, (isset($context["usermail"]) ? $context["usermail"] : null), "html", null, true);
        echo "',
                      customerName: '";
        // line 294
        echo twig_escape_filter($this->env, (isset($context["custome"]) ? $context["custome"] : null), "html", null, true);
        echo "',
                      paymentMode: 'card',
                      value: ";
        // line 296
        echo twig_escape_filter($this->env, (((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)) * 100), "html", null, true);
        echo ",
                      currency: 'AED',
                      forceMobileRedirect: true,
                      payButtonSelector: '#payButtonId',
                      widgetContainerSelector: '.payment-form',
                      useCurrencyCode: false,
                      
                       styling: {
                            widgetColor: 'rgba(204, 204, 204, 0.16)',
                            themeColor: '',
                            buttonColor: '',
                            buttonLabelColor: '',
                            logoUrl: '',
                            formButtonColor: '',
                            formButtonLabelColor: '',
                            iconColor: '',
                            overlayShade: 'dark',
                            overlayOpacity: 0.8,
                            buttonLabel: 'Visa Checkout'
                        },

                 
                      cardCharged: function (event) {
\t\t\t\t\t  console.log(event);
                          document.getElementById('cko-cc-paymenToken').value = event.data.paymentToken;
                          document.getElementById('payment-tokenform').submit();
                      }

                  });
\t\t\t\t  
\t\t\t\t  
                </script>

            </form>
\t\t\t<form name=\"payment-tokenform\" method=\"post\" id=\"payment-tokenform\" action=\"http://www.businessbid.ae/vendor/home\">
                <input type=\"hidden\" id =\"cko-cc-paymenToken\" name =\"cko-cc-paymenToken\" />
            </form>
\t\t\t</div>
\t\t\t<div class=\"col-sm-3\"><img src=\"";
        // line 334
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/payment-icon.jpg"), "html", null, true);
        echo "\" /></div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"check pickup\" />Cheque Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"checkAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 344
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"checkFillForm\">
\t\t\t\t";
        // line 350
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_start', array("attr" => array("id" => "checkForm")));
        echo "
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\"><label>Company Name:</label></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 355
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 358
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 359
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 362
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 363
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 366
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 367
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 370
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 371
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 374
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 375
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 378
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 379
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 382
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 383
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr class=\"camouflage\">
\t\t\t\t\t\t\t<td width=\"20%\"></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 387
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "Submit", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t\t";
        // line 391
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_end');
        echo "
\t\t\t</div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"cash pickup\" />Cash Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"cashAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 402
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"cashFillForm\">
                ";
        // line 408
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_start', array("attr" => array("id" => "cashForm")));
        echo "
                <table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
                    <tbody>
                        <tr>
                            <td width=\"20%\"><label>Company Name:</label></td>
                            <td width=\"80%\">";
        // line 413
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 416
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
                            <td width=\"80%\">";
        // line 417
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 420
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
                            <td width=\"80%\">";
        // line 421
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 424
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 425
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 428
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 429
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 432
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 433
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr class=\"camouflage\">
                            <td width=\"20%\"></td>
                            <td width=\"80%\">";
        // line 437
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "Submit", array()), 'widget');
        echo "</td>
                        </tr>
                    </tbody>
                </table>
                ";
        // line 441
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_end');
        echo "
            </div>
\t\t</div>

\t\t";
        // line 460
        echo "\t</div>
</div>
\t<div class=\"row text-right update-account\">
\t\t<a href=\"#\" class=\"btn btn-success\"> Place Order </a>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  694 => 460,  687 => 441,  680 => 437,  673 => 433,  669 => 432,  663 => 429,  659 => 428,  653 => 425,  649 => 424,  643 => 421,  639 => 420,  633 => 417,  629 => 416,  623 => 413,  615 => 408,  606 => 402,  592 => 391,  585 => 387,  578 => 383,  574 => 382,  568 => 379,  564 => 378,  558 => 375,  554 => 374,  548 => 371,  544 => 370,  538 => 367,  534 => 366,  528 => 363,  524 => 362,  518 => 359,  514 => 358,  508 => 355,  500 => 350,  491 => 344,  478 => 334,  437 => 296,  432 => 294,  428 => 293,  424 => 292,  402 => 273,  385 => 259,  378 => 254,  371 => 250,  366 => 248,  361 => 245,  358 => 244,  355 => 243,  339 => 239,  336 => 238,  332 => 236,  326 => 234,  323 => 233,  321 => 232,  315 => 230,  313 => 229,  307 => 228,  303 => 226,  300 => 225,  295 => 224,  292 => 223,  290 => 222,  274 => 208,  265 => 205,  262 => 204,  258 => 203,  255 => 202,  246 => 199,  243 => 198,  239 => 197,  236 => 196,  227 => 193,  224 => 192,  220 => 191,  31 => 4,  28 => 3,  11 => 1,);
    }
}

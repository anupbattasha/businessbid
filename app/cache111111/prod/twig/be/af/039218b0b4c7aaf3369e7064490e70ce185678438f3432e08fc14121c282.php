<?php

/* BBidsBBidsHomeBundle:Home:consumerratings.html.twig */
class __TwigTemplate_beaf039218b0b4c7aaf3369e7064490e70ce185678438f3432e08fc14121c282 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:consumerratings.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_enquiries($context, array $blocks = array())
    {
        // line 4
        echo "<style>
.collapsedd .collapsed-content{
display: none;
list-style: none;
margin: 0;
padding: 0;
}
</style>
<script>
\$(document).ready(function(){
\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
       
        // Create stars holder
        var \$span = \$('<span />').width(size);
        
        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();


\t\$(\".moreview\").click(function(){
\t\$(\".moreview\").not(this).html('View job request details');
\t\t\$(\".collapsedd\").not(this).children().children('div').slideUp('10000');
  \t\t\$(this).closest(\".collapsedd\").children().children('div').slideDown('10000');
\t\tif(\$(this).html() == 'close')
\t\t\t\$(this).html('View job request details');
\t\telse
\t\t\t\$(this).html('close');
\t\t
\t});\t
\t


});
</script>
<div class=\"col-md-9 dashboard-rightpanel\">
<div class=\"row\">
<div class=\"page-title\">
<h1>My Customer Account</h1>
</div>
<div class=\"panel-title\"><h2>Summary of the Reviews</h2></div>
";
        // line 52
        if ( !twig_test_empty((isset($context["reviews"]) ? $context["reviews"] : null))) {
            // line 53
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 54
                echo "<div class=\"rating-reviews\">
<div class=\"form-item\">
<div class=\"subject\">";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "subj", array()), "html", null, true);
                echo "</div>
<div class=\"date\">";
                // line 57
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-m-d H:m:s"), "html", null, true);
                echo "<div class=\"rating\"><span></span><span class=\"stars\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "rating", array()), "html", null, true);
                echo "</span></div></div>
<label class=\"business-name\">";
                // line 58
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "bizname", array()), "html", null, true);
                echo "</label>
<div class=\"review\">
<!--<a href=\"";
                // line 60
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_mapped_reviews", array("rid" => $this->getAttribute($context["r"], "revid", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "review", array()), "html", null, true);
                echo "</a>-->
";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "review", array()), "html", null, true);
                echo "</div>
<div class=\"view-details\">
<!--<a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_mapped_enquiry", array("eid" => $this->getAttribute($context["r"], "id", array()))), "html", null, true);
                echo "\">view Job Request Details</a>-->

    <div class=\"collapsedd\"> 
        <span> 
            <a class=\"reviewss\"><span class=\"moreview\">view Job Request Details</span></a> 
            <div class=\"collapsed-content\">
\t\t\t<div class=\"col-sm-6 post-requirement gray-bg\">
\t\t\t\t<h2>Job Request Details</h2>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Date</label><div class=\"field-value\">";
                // line 72
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-d-m"), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Category</label><div class=\"field-value\">";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "category", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Subject</label><div class=\"field-value\">";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "subj", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>City</label><div class=\"field-value\">";
                // line 81
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "location", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Description</label><div class=\"field-value\">";
                // line 84
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "description", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"col-sm-6 post-requirement gray-bg\">
\t\t\t\t<h2>Job Review Details</h2>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Date</label><div class=\"field-value\">";
                // line 91
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-d-m"), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Category</label><div class=\"field-value\">";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "category", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Subject</label><div class=\"field-value\">";
                // line 97
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "subj", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Business Now</label><div class=\"field-value\">";
                // line 100
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessknow", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Business Recomendation</label><div class=\"field-value\">";
                // line 103
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessrecomond", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Service Experience</label><div class=\"field-value\">";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "serviceexperience", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Service Summary</label><div class=\"field-value\">";
                // line 109
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "servicesummary", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t</div> 
        </span>
    </div> 
</div>
</div>
</div>
\t\t\t\t\t\t
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 120
            echo "
<div>Now showing : ";
            // line 121
            echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
            echo " of ";
            echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
            echo " records</div>
<div>";
            // line 122
            $context["Epage_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 4)));
            // line 123
            echo "<ul class=\"pagination\">

  <li><a href=\"";
            // line 125
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ratings", array("offset" => 1));
            echo "\">&laquo;</a></li>
";
            // line 126
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 127
                echo "
  <li><a href=\"";
                // line 128
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_ratings", array("offset" => $context["i"])), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</a></li>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "
  <li><a href=\"";
            // line 132
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_ratings", array("offset" =>  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null))), "html", null, true);
            echo "\">&raquo;</a></li>
</ul>
</div>
";
        } else {
            // line 136
            echo "<table>
<tr><td align=\"center\">No Reviews Available</td></tr>
</table>
";
        }
        // line 140
        echo "
</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:consumerratings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 140,  265 => 136,  258 => 132,  255 => 131,  244 => 128,  241 => 127,  237 => 126,  233 => 125,  229 => 123,  227 => 122,  219 => 121,  216 => 120,  199 => 109,  193 => 106,  187 => 103,  181 => 100,  175 => 97,  169 => 94,  163 => 91,  153 => 84,  147 => 81,  141 => 78,  135 => 75,  129 => 72,  117 => 63,  112 => 61,  106 => 60,  101 => 58,  95 => 57,  91 => 56,  87 => 54,  83 => 53,  81 => 52,  31 => 4,  28 => 3,  11 => 1,);
    }
}

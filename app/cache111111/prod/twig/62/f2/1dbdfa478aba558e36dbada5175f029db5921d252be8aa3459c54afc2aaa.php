<?php

/* BBidsBBidsHomeBundle:Categoriesform:air_conditioning_cooling_form.html.twig */
class __TwigTemplate_62f21dbdfa478aba558e36dbada5175f029db5921d252be8aa3459c54afc2aaa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:air_conditioning_cooling_form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Acquire Air-Conditioning & Cooling Quotes Easily";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "
<style>
    .pac-container:after{
        content:none !important;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 20
            echo "
        <div class=\"alert alert-danger\">";
            // line 21
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
        ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
        <div class=\"alert alert-success\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "
        <div class=\"alert alert-success\">";
            // line 32
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "    </div>
<div class=\"cat-form\">
  <div class=\"photo_form_one_area modal1\">
    <h1>Looking for Air-Conditioning & Air-Cooling Services in the UAE</h1>
    <p>Get competitive quotes from Air-Conditioning companies in the UAE in 3 easy steps by completing the simple form below.
      <br>OR<br><br><span class=\"call-us\">CALL (04) 4213777</span></p>
  </div>

        ";
        // line 43
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "category_form")));
        echo "
        <div id=\"step1\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 1 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Air-Conditioner-1.png"), "html", null, true);
        echo "\" alt=\"Air Conditioning\">
                  </div>
                  ";
        // line 82
        $context["oddoreven"] = "even";
        // line 83
        echo "                  ";
        $context["subcategoryLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array())) - 1);
        // line 84
        echo "                  ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array())) % 2 == 1)) {
            // line 85
            echo "                    ";
            $context["oddoreven"] = "odd";
            // line 86
            echo "                  ";
        }
        // line 87
        echo "                  <div class=\"inner_photo_one_form_table\">
                      <h3>What services do you require?</h3>
                      <div class=\"child_pro_tabil_main kindofService\">
                          ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 91
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 92
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"child_pho_table_left_inner_text\" >
                                    ";
                // line 94
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                                    ";
                // line 95
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory_other", array()), 'widget', array("attr" => array("class" => "subcategory_other")));
                echo "
                                    <h5>(max 30 characters)</h5>
                                </div>
                              </div>
                            ";
            } else {
                // line 100
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"";
                // line 101
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["subcategoryLength"]) ? $context["subcategoryLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                    ";
                // line 102
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                                </div>
                              </div>
                            ";
            }
            // line 106
            echo "                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "                          ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 108
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner_text innertext2\" ></div>
                           </div>
                          ";
        }
        // line 112
        echo "                          <div class=\"error\" id=\"kind_service\"></div>
                      </div>
                  <div class=\"photo_contunue_btn proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue1\">CONTINUE</a>
                  </div>
                  </div>
              </div>
          </div>
          </div>


        ";
        // line 124
        echo "        <div id=\"step2\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 2 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 157
        $context["oddoreven"] = "even";
        // line 158
        echo "          ";
        $context["productLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "product", array())) - 1);
        // line 159
        echo "          ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "product", array())) % 2 == 1)) {
            // line 160
            echo "            ";
            $context["oddoreven"] = "odd";
            // line 161
            echo "          ";
        }
        // line 162
        echo "          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form productService\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Air-Conditioner-2a.png"), "html", null, true);
        echo "\" alt=\"Air Conditioning Product\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                    <h3>What is the product?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 170
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "product", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 171
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 172
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"child_pho_table_left_inner_text\" >
                                ";
                // line 174
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                                ";
                // line 175
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "product_other", array()), 'widget', array("attr" => array("class" => "product_other")));
                echo "
                                <h5>(max 30 characters)</h5>
                            </div>
                          </div>
                        ";
            } else {
                // line 180
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"";
                // line 181
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["productLength"]) ? $context["productLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                ";
                // line 182
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                            </div>
                          </div>
                        ";
            }
            // line 186
            echo "                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 187
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 188
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text\" ></div>
                       </div>
                      ";
        }
        // line 192
        echo "                      <div class=\"error\" id=\"product\"></div>
                  </div>
                </div>
          </div>
        </div>
        ";
        // line 197
        $context["oddoreven"] = "even";
        // line 198
        echo "        ";
        $context["serviceLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service", array())) - 1);
        // line 199
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service", array())) % 2 == 1)) {
            // line 200
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 201
            echo "        ";
        }
        // line 202
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form services\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Air-Conditioner-2b.png"), "html", null, true);
        echo "\" alt=\"Air Conditioning Service\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                    <h3>Which service do you require?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 210
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 211
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 212
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"child_pho_table_left_inner_text\" >
                                ";
                // line 214
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                                ";
                // line 215
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service_other", array()), 'widget', array("attr" => array("class" => "service_other")));
                echo "
                                <h5>(max 30 characters)</h5>
                            </div>
                          </div>
                        ";
            } else {
                // line 220
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"";
                // line 221
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["serviceLength"]) ? $context["serviceLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                ";
                // line 222
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                            </div>
                          </div>
                        ";
            }
            // line 226
            echo "                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 227
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 228
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                       </div>
                      ";
        }
        // line 232
        echo "                      <div class=\"error\" id=\"services\"></div>
                  </div>
                </div>
          </div>
        </div>
        ";
        // line 237
        $context["oddoreven"] = "even";
        // line 238
        echo "        ";
        $context["ageLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "age", array())) - 1);
        // line 239
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "age", array())) % 2 == 1)) {
            // line 240
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 241
            echo "        ";
        }
        // line 242
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form age\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Air-Conditioner-2c.png"), "html", null, true);
        echo "\" alt=\"Cooling Device\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                    <h3>How old is this device?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 250
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "age", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 251
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 252
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"child_pho_table_left_inner_text\" >
                                ";
                // line 254
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                                ";
                // line 255
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "age_other", array()), 'widget', array("attr" => array("class" => "age_other")));
                echo "
                                <h5>(max 30 characters)</h5>
                            </div>
                          </div>
                        ";
            } else {
                // line 260
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"";
                // line 261
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["ageLength"]) ? $context["ageLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                ";
                // line 262
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                            </div>
                          </div>
                        ";
            }
            // line 266
            echo "                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 267
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 268
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                       </div>
                      ";
        }
        // line 272
        echo "                      <div class=\"error\" id=\"age\"></div>
                  </div>
                </div>
          </div>
        </div>
        ";
        // line 277
        $context["oddoreven"] = "even";
        // line 278
        echo "        ";
        $context["oftenLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "often", array())) - 1);
        // line 279
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "often", array())) % 2 == 1)) {
            // line 280
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 281
            echo "        ";
        }
        // line 282
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 285
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/require.png"), "html", null, true);
        echo "\" alt=\"Cooling Require\">
                </div>
                <div class=\"inner_photo_one_form_table often\">
                    <h3>How often do you need this service?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 290
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "often", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 291
            echo "                        <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                          <div class=\"";
            // line 292
            if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["oftenLength"]) ? $context["oftenLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                echo "child_pho_table_left_inner";
            } else {
                echo "child_pho_table_left_inner";
            }
            echo "\" >
                              ";
            // line 293
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                          </div>
                        </div>
                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 297
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 298
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner innertext2\"></div>
                       </div>
                      ";
        }
        // line 302
        echo "                      <div class=\"error\" id=\"often\"></div>
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <div class=\"proceed\">
                        <a href=\"javascript:void(0)\" id=\"continue2\" class=\"button3\"><button type=\"button\">CONTINUE</button></a>
                    </div>
                    <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback1\">BACK</a></div>
                  </div>
                </div>
            </div>
        </div>
      </div>
      ";
        // line 315
        echo "      ";
        // line 316
        echo "        <div id=\"step3\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                  <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 3 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 349
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 350
            echo "          <div class=\"photo_form_main\">
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form name\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/name.png"), "html", null, true);
            echo "\" alt=\"Name\">
                  </div>
                  <div class=\"inner_photo_one_form_table\" >
                      <h3>";
            // line 357
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h3>
                          ";
            // line 358
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "form-input name")));
            echo "
                      <div class=\"error\" id=\"name\" ></div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form  email\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 366
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/email.png"), "html", null, true);
            echo "\" alt=\"Email\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 369
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo " </h3>
                     ";
            // line 370
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-input email_id")));
            echo "
                     <div class=\"error\" id=\"email\" ></div>
                  </div>
              </div>
          </div>
          ";
        }
        // line 376
        echo "          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form location\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 379
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location.png"), "html", null, true);
        echo "\" alt=\"Location\">
                  </div>
                  <div class=\"inner_photo_one_form_table res2\" >
                      <h3>Select your Location*</h3>
                      <div class=\"child_pro_tabil_main res21\">
                          <div class=\"child_accounting_three_left\">
                              ";
        // line 385
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => " land_location")));
        echo "
                                  <p>";
        // line 386
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"child_accounting_three_right\">
                             ";
        // line 389
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "land_location")));
        echo "
                                  <p>";
        // line 390
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"error\" id=\"location\"></div>
                      </div>
                  </div>
              </div>
          </div>


              ";
        // line 399
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            echo " <div class=\"photo_one_form for-uae \">
              <div class=\"inner_photo_one_form mask domcity_cont\" id=\"domCity\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
            // line 403
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <h3>";
            // line 406
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h3>
                    <div class=\"child_pro_tabil_main city_cont\">
                      ";
            // line 408
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "")));
            echo "
                    </div>
                  </div>
                </div>
                <div class=\"error\" id=\"acc_city\"></div>
              </div>
              ";
        }
        // line 415
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 416
            echo "              <div class=\"inner_photo_one_form mask\" id=\"domArea\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 418
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location icon.png"), "html", null, true);
            echo "\" alt=\"Location Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 421
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h3>
                     ";
            // line 422
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_area\"></div>
              </div>
              ";
        }
        // line 427
        echo "
              ";
        // line 428
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 429
            echo "              <div class=\"mask\" id=\"domContact\" >
                <div class=\"inner_photo_one_form\" style=\"margin-top: 40px;\" id=\"uae_mobileno\">
                    <div class=\"inner_photo_one_form_img\">
                        <img src=\"";
            // line 432
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                    </div>
                    <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                        <h3>";
            // line 435
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h3>


                        <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                        ";
            // line 439
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
            echo "
                        ";
            // line 440
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                    </div>
                    <div class=\"error\" id=\"acc_mobile\"></div>
                </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top: 40px;\" >
                        <div class=\"inner_photo_one_form_img\">
                            <img src=\"";
            // line 447
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                        </div>
                        <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                            <h3>";
            // line 450
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h3>


                             <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                            ";
            // line 454
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
            echo "
                            ";
            // line 455
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                        </div>
                    </div>
              </div></div>
                    ";
        }
        // line 461
        echo "

            <div class=\"photo_one_form for-foreign\">
              ";
        // line 464
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 465
            echo "              <div class=\"inner_photo_one_form mask intCountry_cont\" id=\"intCountry\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 467
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/international icon.png"), "html", null, true);
            echo "\" alt=\"international Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 470
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h3>
                     ";
            // line 471
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcountry\"></div>
              </div>
              <div class=\"inner_photo_one_form mask\" id=\"intCity\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 477
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 480
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h3>
                      ";
            // line 481
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcity\"></div>
              </div>
              ";
        }
        // line 486
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 487
            echo "              <div class=\"mask\" id=\"intContact\">
                <div class=\"inner_photo_one_form\" style=\"margin-top:40px;\" id=\"int_mobileno\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 490
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 493
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 495
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget');
            echo "
                      ";
            // line 496
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intmobile\"></div>
              </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 502
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 505
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 507
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget');
            echo "
                      ";
            // line 508
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
              </div>
             </div>
              ";
        }
        // line 513
        echo "            </div>

            <div class=\"photo_one_form\" id=\"hireBlock\">
              <div class=\"inner_photo_one_form hire_cont\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 519
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/looking to hire.png"), "html", null, true);
        echo "\" alt=\"Looking To Hire\">
                  </div>
                  <div class=\"inner_photo_one_form_table resp1\">
                    <h3>When are you looking to hire?*</h3>
                    <div class=\"child_pro_tabil_main  \">
                      ";
        // line 524
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                  </div>
                </div>
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 529
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/information.png"), "html", null, true);
        echo "\" alt=\"Information\">
                </div>
                <div class=\"inner_photo_one_form_table resp1\">
                    <h3>Is there any other additional information you would like to provide?</h3>
                    ";
        // line 533
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "
                     <br> <h5 class=\"char\">(max 120 characters)</h5>
                </div>
                ";
        // line 543
        echo "                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue3\" class=\"button3\">";
        // line 545
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "CONTINUE", "attr" => array("class" => "")));
        echo "</a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback2\">BACK</a></div>
                </div>
              </div>
          </div>
        </div>
        </script></div>
        ";
        // line 554
        echo "        ";
        // line 555
        echo "        <div id=\"step4\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <p>Page 4 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <h3 class=\"thankyou-heading\"><b>Thank you, you’re done!</b></h3>
              <p class=\"thankyou-content\" id=\"enquiryResult\">Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!</p>
              <p class=\"thankyou-content\">If you thought BusinessBid was useful to you, why not share the love? We’d greatly appreciate it. Like us on
              Facebook or follow us on Twitter to to keep updated on all the news and best deals.</p>
              <div class=\"social-icon\">
                <a href=\"https://twitter.com/\"><img src=\"";
        // line 594
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/twitter.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Twitter Bird\"/></a>
                <a href=\"https://www.facebook.com/\"><img src=\"";
        // line 595
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Facebook Sign\" /></a>
              </div>
              <div id=\"thankyou-btn\"  class=\"inner_photo_one_form\">
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 599
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">LOG ANOTHER REQUEST</a>
              </div>
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 602
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" >CONTINUE TO HOMEPAGE</a>
              </div>
              </div>
          </div>
        </div>
        ";
        // line 608
        echo "           ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
</div>
</div>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script type=\"text/javascript\">
    function scrollBox (tag) {
        \$('html,body').animate({scrollTop: tag.position().top -10},'slow');
    }
    \$(document).ready(function () {
        var forms = [
            '[ name=\"";
        // line 623
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
          ];
        var validationFlag = true;
          \$( forms.join(',') ).submit( function( e ){
            e.preventDefault();
            console.log(\"tried submitting\")
            if(validationFlag) {
                postForm( \$(this), function( response ){
                    if(response.hasOwnProperty('error')) {
                        alert(response.error);
                        var i = 1;
                        for (i = 1; i < 5; i++) {
                            \$(\"div#step\"+i).hide();
                        };
                        \$(\"div#step\"+response.step).show();
                    } else {
                      \$(\"#category_form\")[0].reset();
                        var form=document.createElement(\"form\");form.setAttribute(\"method\",\"post\"),form.setAttribute(\"action\",'";
        // line 640
        echo $this->env->getExtension('routing')->getPath("bizbids_job_post_sucessful");
        echo "');var hiddenField=document.createElement(\"input\");hiddenField.setAttribute(\"name\",\"jobno\"),hiddenField.setAttribute(\"value\",response.jobNo),hiddenField.setAttribute(\"type\",\"hidden\"),form.appendChild(hiddenField),document.body.appendChild(form),form.submit();
                    }
                });
                ga('send', 'event', 'Button', 'Click', 'Form Sent');
            }
            return false;
          });
        function postForm( \$form, callback ) {
            NProgress.start();
              \$.ajax({
                type        : \$form.attr( 'method' ),
                beforeSend  : function() { NProgress.inc() },
                url         : \$form.attr( 'action' ),
                data        : \$form.serialize(),
                success     : function(data) {
                  callback( data );
                  NProgress.done();
                },
                error : function (xhr) {
                    alert(\"Error occured.please try again\");
                    NProgress.done();
                }
              });
        }
    })
</script>
<script type=\"text/javascript\" src=\"";
        // line 666
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 667
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script type=\"text/javascript\" src=\"";
        // line 668
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("js/reviewsformsvalidation/" . (isset($context["twigFileName"]) ? $context["twigFileName"] : null)) . ".min.js")), "html", null, true);
        echo "\" ></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:air_conditioning_cooling_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1322 => 668,  1318 => 667,  1314 => 666,  1285 => 640,  1265 => 623,  1246 => 608,  1238 => 602,  1232 => 599,  1225 => 595,  1221 => 594,  1180 => 555,  1178 => 554,  1167 => 545,  1163 => 543,  1157 => 533,  1150 => 529,  1142 => 524,  1134 => 519,  1126 => 513,  1118 => 508,  1114 => 507,  1109 => 505,  1103 => 502,  1094 => 496,  1090 => 495,  1085 => 493,  1079 => 490,  1074 => 487,  1071 => 486,  1063 => 481,  1059 => 480,  1053 => 477,  1044 => 471,  1040 => 470,  1034 => 467,  1030 => 465,  1028 => 464,  1023 => 461,  1014 => 455,  1010 => 454,  1003 => 450,  997 => 447,  987 => 440,  983 => 439,  976 => 435,  970 => 432,  965 => 429,  963 => 428,  960 => 427,  952 => 422,  948 => 421,  942 => 418,  938 => 416,  935 => 415,  925 => 408,  920 => 406,  914 => 403,  907 => 399,  895 => 390,  891 => 389,  885 => 386,  881 => 385,  872 => 379,  867 => 376,  858 => 370,  854 => 369,  848 => 366,  837 => 358,  833 => 357,  827 => 354,  821 => 350,  819 => 349,  784 => 316,  782 => 315,  768 => 302,  762 => 298,  759 => 297,  739 => 293,  731 => 292,  726 => 291,  709 => 290,  701 => 285,  696 => 282,  693 => 281,  690 => 280,  687 => 279,  684 => 278,  682 => 277,  675 => 272,  669 => 268,  666 => 267,  652 => 266,  643 => 262,  635 => 261,  630 => 260,  622 => 255,  618 => 254,  612 => 252,  609 => 251,  592 => 250,  584 => 245,  579 => 242,  576 => 241,  573 => 240,  570 => 239,  567 => 238,  565 => 237,  558 => 232,  552 => 228,  549 => 227,  535 => 226,  526 => 222,  518 => 221,  513 => 220,  505 => 215,  501 => 214,  495 => 212,  492 => 211,  475 => 210,  467 => 205,  462 => 202,  459 => 201,  456 => 200,  453 => 199,  450 => 198,  448 => 197,  441 => 192,  435 => 188,  432 => 187,  418 => 186,  409 => 182,  401 => 181,  396 => 180,  388 => 175,  384 => 174,  378 => 172,  375 => 171,  358 => 170,  350 => 165,  345 => 162,  342 => 161,  339 => 160,  336 => 159,  333 => 158,  331 => 157,  296 => 124,  283 => 112,  277 => 108,  274 => 107,  260 => 106,  251 => 102,  243 => 101,  238 => 100,  230 => 95,  226 => 94,  220 => 92,  217 => 91,  200 => 90,  195 => 87,  192 => 86,  189 => 85,  186 => 84,  183 => 83,  181 => 82,  176 => 80,  136 => 43,  126 => 35,  117 => 32,  114 => 31,  109 => 30,  100 => 27,  97 => 26,  93 => 25,  90 => 24,  81 => 21,  78 => 20,  74 => 19,  68 => 16,  64 => 15,  56 => 9,  53 => 8,  48 => 5,  45 => 4,  37 => 3,  31 => 2,  11 => 1,);
    }
}

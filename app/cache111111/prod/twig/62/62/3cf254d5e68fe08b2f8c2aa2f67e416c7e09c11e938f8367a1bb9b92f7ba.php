<?php

/* BBidsBBidsHomeBundle:Mailtemplates:invoice.html.twig */
class __TwigTemplate_62623cf254d5e68fe08b2f8c2aa2f67e416c7e09c11e938f8367a1bb9b92f7ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"printTable\" class=\"print-table\">
  <tr>
    <td height=\"204\" align=\"center\" valign=\"top\">
      <table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td width=\"459\" height=\"82\" style=\"padding:0px; height:110px;\"><a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"http://115.124.120.36/img/logo.jpg\" ></a></td>
          <td width=\"292\" align=\"left\" valign=\"middle\"  style=\"font-family:Arial,  Helvetica, sans-serif; font-weight:600; font-size:40px;line-height:36px;padding:3px 0px 3px 10px; margin-left:8px; font-weight:bolder;\">INVOICE</td>
        </tr>
      </table>
      <table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td width=\"465\" height=\"80\" align=\"left\" valign=\"top\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:30px;padding:10px 10px 20px 1px; margin-left:8px; \">
              <p>BusinessBid DMCC<br />
              503, Indigo Icon Tower<br />
              Cluster F, JLT-Dubai, UAE<br />
              (04) 4213777<br />
            <u>www.businessbid.ae</u></p>
          </td>
          <td width=\"292\" align=\"left\" valign=\"top\"  style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:30px;padding:10px 10px 3px 10px; margin-left:8px; \">\t\t\t          <p>Invoice Date: ";
        // line 19
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["paydate"]) ? $context["paydate"] : null), "d-m-Y"), "html", null, true);
        echo "<br>
             Invoice #: ";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["ordernumber"]) ? $context["ordernumber"] : null), "html", null, true);
        echo "<br>
             Payment Type: ";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["paytype"]) ? $context["paytype"] : null), "html", null, true);
        echo "</p></td>
        </tr>
        <tr>
          <td height=\"70\" align=\"left\" valign=\"top\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:30px;padding:3px 10px 20px 1px; margin-left:8px; \">SOLD TO<br />
            ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["bizname"]) ? $context["bizname"] : null), "html", null, true);
        echo "<br />
            ";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["address"]) ? $context["address"] : null), "html", null, true);
        echo ", U.A.E<br />
            ";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["smsphone"]) ? $context["smsphone"] : null), "html", null, true);
        echo "<br />
          ";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : null), "html", null, true);
        echo "</td>
          <td align=\"left\" valign=\"top\"  style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:30px;padding:3px 10px 3px 10px; margin-left:8px; \">&nbsp;</td>
        </tr>
      </table>
      <table width=\"900\" border=\"1\" cellspacing=\"0\" cellpadding=\"15\">
        <tr>
          <td height=\"72\" colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"#3670b7\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:35px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; color:#fff;\">CHARGE SUMMARY</td>
        </tr>
        <tr>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\"  style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Lead Package</td>
          <td width=\"304\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Number of Leads</td>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Service Category</td>
        </tr>
        ";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 42
            echo "        <tr>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; font-weight:600; margin-left:8px; \">";
            // line 43
            if (($this->getAttribute($context["c"], "leadpack", array()) == 10)) {
                echo "Bronze";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 25)) {
                echo "Silver ";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 50)) {
                echo " Gold ";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 100)) {
                echo " Platinum ";
            }
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif;  font-weight:600;font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "leadpack", array()), "html", null, true);
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
            echo "</td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "      </table><br/><br/>
      <table width=\"900\" border=\"1\" cellspacing=\"0\" cellpadding=\"15\">
        <tr>
          <td height=\"72\" colspan=\"5\" align=\"center\" valign=\"middle\" bgcolor=\"#3670b7\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:35px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; color:#fff;\">TRANSACTIONS</td>
        </tr>
        <tr>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Date</td>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Description</td>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Amount (AED)</td>
        </tr>
        ";
        // line 58
        $context["grandtotal"] = 0;
        // line 59
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order"]) ? $context["order"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            // line 60
            echo "        <tr>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 61
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["o"], "created", array()), "d-m-Y"), "html", null, true);
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 62
            if (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 10)) {
                echo "Bronze";
            } elseif (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 25)) {
                echo "Silver ";
            } elseif (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 50)) {
                echo " Gold ";
            } elseif (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 100)) {
                echo " Platinum ";
            }
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "amount", array()), "html", null, true);
            echo "</td>
          ";
            // line 64
            $context["grandtotal"] = ((isset($context["grandtotal"]) ? $context["grandtotal"] : null) + $this->getAttribute($context["o"], "amount", array()));
            // line 65
            echo "        </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "        <tr>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
        // line 68
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["paydate"]) ? $context["paydate"] : null), "d-m-Y"), "html", null, true);
        echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Grand Total</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
        // line 70
        echo twig_escape_filter($this->env, (isset($context["grandtotal"]) ? $context["grandtotal"] : null), "html", null, true);
        echo "</td>
        </tr>
      </table>    </td>
    </tr>
    <tr>
     <td height=\"40\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:14px;padding:0px 0px 20px 0px; margin-left:8px; font-style:italic; \"><p>Please note that this is an electronically generated invoice</p></td>
    </tr>
    <tr>
      <td height=\"32\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:20px;line-height:37px;padding:3px 10px 3px 10px;  margin-left:0px; \">Thank you for your business</td>
    </tr>
    <tr style=\"padding-bottom:40px;\"><td height=\"51\"><img  width=\"330\" src=\"http://115.124.120.36/img/business-bid-seal.jpg\" ></td></tr>
     <tr>
      <td style=\"width:100%; height:150px;\"></td>
    </tr>
    <tr>
      <td><hr /></td>
    </tr>
    <tr>
      <td height=\"33\" align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:16px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">BusinessBid DMCC is registered and licensed as a Freezone Company under the Rules & Regulations of DMCC.</td>
    </tr>
  </table>";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Mailtemplates:invoice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 70,  192 => 68,  189 => 67,  174 => 65,  172 => 64,  168 => 63,  156 => 62,  152 => 61,  149 => 60,  131 => 59,  129 => 58,  117 => 48,  108 => 45,  104 => 44,  92 => 43,  89 => 42,  85 => 41,  69 => 28,  65 => 27,  61 => 26,  57 => 25,  50 => 21,  46 => 20,  42 => 19,  26 => 6,  19 => 1,);
    }
}

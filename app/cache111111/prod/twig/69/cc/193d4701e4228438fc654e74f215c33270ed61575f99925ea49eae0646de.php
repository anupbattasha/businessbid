<?php

/* BBidsBBidsHomeBundle:Home:forgotpassword.html.twig */
class __TwigTemplate_69cc193d4701e4228438fc654e74f215c33270ed61575f99925ea49eae0646de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
<title>Password Reset</title>
</head>

<body>
<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"98%\">
  <tbody><tr>
   <td  style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;color:#000;\" valign=\"top\">
\t\t\t<!-- <h1 style=\"font-family:Helvetica,Arial,sans-serif;color:#222222;font-size:28px;line-height:normal;letter-spacing:-1px\">
\t\t  Reset your password
\t\t</h1> -->

\t\t <p  style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.</p>

\t\t<p  style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\"><b>Click the link below to reset your password using our secure server:<br><a href=\"http://www.businessbid.ae";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_user_email_verify", array("userid" => (isset($context["userid"]) ? $context["userid"] : null), "userhash" => (isset($context["userhash"]) ? $context["userhash"] : null))), "html", null, true);
        echo "?type=fp\" style=\"color: #0099FF;\" target=\"_blank\" >Reset password</a></b></p>

\t\t<br><br>
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">Regards<br>BusinessBid Team</p>
\t\t<hr style=\"margin-top:30px;border:none;border-top:1px solid #ccc\">


\t\t<p align=\"left\"  style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;color:#000;\">
\t\t\tIf you did not request to have your password reset you can safely ignore this email. Rest assured your customer account is safe.</p>
\t\t<p align=\"left\"  style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;color:#000;\">If clicking the link doesn't seem to work, you can copy and paste the link into your browser's address window, or retype it there. Once you have returned to <a href=\"http://www.businessbid.ae\">BusinessBid.ae</a>, we will give instructions for resetting your password.</p>
\t\t<p align=\"left\"  style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;color:#000;\"><a href=\"http://www.businessbid.ae\">BusinessBid.ae</a> will never e-mail you and ask you to disclose or verify your <a href=\"http://www.businessbid.ae\">BusinessBid.ae</a> password, credit card, or banking account number. If you receive a suspicious e-mail with a link to update your account information, do not click on the link--instead, report the e-mail to <a href=\"http://www.businessbid.ae\">BusinessBid.ae</a> for investigation. Thanks for visiting <a href=\"http://www.businessbid.ae\">BusinessBid.ae</a></p>

    </td>
  </tr>
</tbody></table>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:forgotpassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 18,  19 => 1,);
    }
}

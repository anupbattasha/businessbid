<?php

/* BBidsBBidsHomeBundle:User:enquiriesconsumer.html.twig */
class __TwigTemplate_c31a7a93eb1a651ba92b10e9a02716c38cb20156019e48ad3ebdfc7d75c5fddd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:enquiriesconsumer.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_enquiries($context, array $blocks = array())
    {
        // line 4
        echo "


<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" >
\t\$(document).ready(function() {
    \$('#example').DataTable({
    \t\"searching\": false,
    \t\"lengthChange\": false,
    \t\"bSort\": false
    });
} );
</script>
<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>My Job Request</h1></div>

<div>
<table id=\"example\">
\t<thead>
\t<tr>
\t\t<th>Date</th>
\t\t<th>Job Request Number</th>
\t\t<th>Category</th>
\t\t<th>Subject</th>
\t\t<th>City</th>
\t\t<th>Description</th>
\t\t<th>My Job Request Details</th>
\t</tr>
\t</thead>

\t";
        // line 35
        if ( !twig_test_empty((isset($context["enquiries"]) ? $context["enquiries"] : null))) {
            // line 36
            echo "\t<tbody>
\t\t";
            // line 37
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 38
                echo "
\t\t\t\t<tr>
\t\t\t\t\t<td>";
                // line 40
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["e"], "created", array()), "Y-d-m"), "html", null, true);
                echo "</td>
\t\t\t\t\t<td><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"myModalEnq\" data-enq=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
                echo "</a></td>
\t\t\t\t\t<td>";
                // line 42
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "category", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td>";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "subj", array()), "html", null, true);
                echo "</a></td>
\t\t\t\t\t<td>";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "location", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td>";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "description", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t<td><a href=\"";
                // line 46
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_mapped_vendor", array("eid" => $this->getAttribute($context["e"], "id", array()))), "html", null, true);
                echo "\">View Matched Vendors</a></td>
\t\t\t\t</tr>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "\t\t</tbody>

\t\t";
        }
        // line 52
        echo "\t</table>
</div>

<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Additional Information</h4>
      </div>
      <div class=\"modal-body\">
      </div>
    </div>
  </div>
</div>


</div>
<script>
\t\$(document).ready(function(){\$(\".myModalEnq\").on(\"click\",function(){\$(\".modal-body\").html(\"Loading Data..... Please wait.\");var a=\$(this).attr(\"data-enq\"),t=\"";
        // line 72
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_form_extra_fields", array("id" => "enq"));
        echo "\",d=t.replace(\"enq\",a);\$.ajax({url:d,success:function(a){\$(\".modal-body\").html(a)}})})});
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:enquiriesconsumer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 72,  120 => 52,  115 => 49,  106 => 46,  102 => 45,  98 => 44,  94 => 43,  90 => 42,  84 => 41,  80 => 40,  76 => 38,  72 => 37,  69 => 36,  67 => 35,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}

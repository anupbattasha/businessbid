<?php

/* BBidsBBidsHomeBundle:Home:customermessages.html.twig */
class __TwigTemplate_8181c6e5df2f279031e614464472e1d20e1e42272ed3e75676b4c6cbdf5c442f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:customermessages.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
            'dashboard' => array($this, 'block_dashboard'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_enquiries($context, array $blocks = array())
    {
        // line 4
        echo "
</script>
<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\">

<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
<script>
\$(function() {
\t\$(\"#datepicker3\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true,
\t\tmaxDate: '+2y',
\t\tyearRange: '2014:2024',
\t\tonSelect: function(date){
\t\t\tvar selectedDate = new Date(date);
\t\t\tvar msecsInADay = 86400000;
\t\t\tvar endDate = new Date(selectedDate.getTime() + msecsInADay);

\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"minDate\", endDate );
\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"maxDate\", '+2y' );
\t\t}
\t});

\t\$(\"#datepicker4\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true
\t});


});
</script>
\t";
        // line 34
        $this->displayBlock('dashboard', $context, $blocks);
        // line 71
        echo "
";
    }

    // line 34
    public function block_dashboard($context, array $blocks = array())
    {
        // line 35
        echo "\t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t<div class=\"page-title\"><h1>My Messages</h1></div>
\t\t\t";
        // line 38
        echo "\t\t\t<div class=\"message-reviews\">
\t\t\t\t";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 40
            echo "
    ";
            // line 41
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t\t\t";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t\t\t<div class=\"message-reviews-filter\">
\t\t\t\t\t<div class=\"col-md-4 side-clear\"><div class=\"label\">From</div>";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from", array()), 'errors');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from", array()), 'widget', array("id" => "datepicker3"));
        echo "</div>
\t\t\t\t\t<div class=\"col-md-4 side-clear\"><div class=\"label\">To</div>";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to", array()), 'errors');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to", array()), 'widget', array("id" => "datepicker4"));
        echo "</div>
\t\t\t\t\t<div class=\"col-md-4 search\">";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"col-md-4 search-button\">";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "search", array()), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
\t\t\t\t</div>
\t\t\t\t";
        // line 51
        if (twig_test_empty((isset($context["emails"]) ? $context["emails"] : null))) {
            // line 52
            echo "\t\t\t\t\t<div class=\"message-alert alert alert-success\">
\t\t\t\t\t\tYou have not received any emails.
\t\t\t\t\t</div>
\t\t\t\t";
        } else {
            // line 56
            echo "\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["emails"]) ? $context["emails"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["email"]) {
                // line 57
                echo "
\t\t\t\t\t<div class=\"form-item messages-wrapper\">
\t\t\t\t\t\t<div class=\"subject\">";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($context["email"], "fromemail", array()), "html", null, true);
                echo "</div>
\t\t\t\t\t\t<div class=\"date\">";
                // line 60
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["email"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</div>
\t\t\t\t\t\t<div id=\"menu";
                // line 61
                echo "\" class=\"example_menu\">
\t\t\t\t\t\t\t<div class=\"review\"><a class=\"collapsed\">";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["email"], "emailsubj", array()), "html", null, true);
                echo "</a><ul><li>";
                echo $this->getAttribute($context["email"], "emailmessage", array());
                echo "</li></ul></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "\t\t\t\t";
        }
        // line 67
        echo "\t\t\t</div>

\t\t</div>
\t";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:customermessages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 67,  168 => 66,  156 => 62,  153 => 61,  149 => 60,  145 => 59,  141 => 57,  136 => 56,  130 => 52,  128 => 51,  121 => 49,  115 => 48,  109 => 47,  103 => 46,  97 => 44,  88 => 41,  85 => 40,  81 => 39,  78 => 38,  74 => 35,  71 => 34,  66 => 71,  64 => 34,  32 => 4,  29 => 3,  11 => 1,);
    }
}

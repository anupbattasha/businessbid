<?php

/* BBidsBBidsHomeBundle:Home:vendor_profile.html.twig */
class __TwigTemplate_436f59b525e6462ecf87c30745519170a63a5a16a7af255b23548737472c52df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:vendor_profile.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Vendor Profile";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<meta name=\"robots\" content=\"noindex, follow\">";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr-2.8.3.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.scrollbox.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jssor.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jssor.slider.js"), "html", null, true);
        echo "\"></script>



<script>
        jQuery(document).ready(function (\$) {
            var options = {
                \$AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                \$AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                \$SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                \$DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the \$DragOrientation should be the same as \$PlayOrientation when \$DisplayPieces is greater than 1, or parking position is not 0)
                \$UISearchMode: 0,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).

                \$ThumbnailNavigatorOptions: {
                    \$Class: \$JssorThumbnailNavigator\$,              //[Required] Class to create thumbnail navigator instance
                    \$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    \$Loop: 1,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
                    \$SpacingX: 3,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    \$SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    \$DisplayPieces: 6,                              //[Optional] Number of pieces to display, default value is 1
                    \$ParkingPosition: 253,                          //[Optional] The offset position to park thumbnail,

                    \$ArrowNavigatorOptions: {
                        \$Class: \$JssorArrowNavigator\$,              //[Requried] Class to create arrow navigator instance
                        \$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        \$AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        \$Steps: 6                                       //[Optional] Steps to go for each navigation request, default value is 1
                    }
                }
            };

            var jssor_slider1 = new \$JssorSlider\$(\"slider1_container\", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.\$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.\$ScaleWidth(Math.min(parentWidth, 720));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            \$(window).bind(\"load\", ScaleSlider);
            \$(window).bind(\"resize\", ScaleSlider);
            \$(window).bind(\"orientationchange\", ScaleSlider);
            //responsive code end
        });
    </script>

<script type=\"text/javascript\">
  \$('#demo2').scrollbox({
    linear: true,
    step: 1,
    delay: 0,
    speed: 100
  });
</script>
<link type=\"text/css\" href = \"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.galleryview-3.0-dev.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<script>
\$(document).ready(function(){
  \$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

  \$(\"button[data-target^='.bs-example-modal-lg_']\").click(function(){
  var clss = \$(this).attr('data-target');

    var cs = clss.split(\"_\");

      \$('#myGallery'+cs[1]).galleryView();


  });

\$('#getcontvalue').hide();
  \$('#getcontbutton').click(function(){
    \$('#getcontvalue').toggle();
  });

\$('div[id^=\"morediv\"]').hide();
  \$('span[id^=\"seemore\"]').click(function(){
    var idd = \$(this).attr('id');
    var arr = idd.split(\"_\");

    \$('#lessdiv_'+arr[1]).hide();
    \$('#morediv_'+arr[1]).show();
  });
    \$('span[id^=\"seeless\"]').click(function(){
      var idd = \$(this).attr('id');
      var arr = idd.split(\"_\");
      \$('#morediv_'+arr[1]).hide();
      \$('#lessdiv_'+arr[1]).show();
    });

});
</script>

<div class=\"container vendor-view\">
  <div class=\"breadcrum\">Home / Vendor Review / ";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "bizname", array()), "html", null, true);
        echo "</div>
     <div id=\"homes\" class=\"two_vendor_page_area\">
            <div class=\"two_vendor_page_inner\">
                <div class=\"two_vendor_child\">
                    <div class=\"two_child_inner_vebdor\">
                        <div class=\"two_vendor_child_inner_pre\">
                            <div class=\"two_vendor_child_inner_pre_img\">
                              ";
        // line 131
        if ((null === $this->getAttribute((isset($context["logoObject"]) ? $context["logoObject"] : null), "fileName", array()))) {
            // line 132
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/noimage2.jpg"), "html", null, true);
            echo "\" alt=\"\">
                              ";
        } else {
            // line 134
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("logo/" . $this->getAttribute((isset($context["logoObject"]) ? $context["logoObject"] : null), "fileName", array()))), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "bizname", array()), "html", null, true);
            echo "\">
                              ";
        }
        // line 136
        echo "                            </div>
                            <div class=\"two_vendor_child_inner_pre_txt\">
                                <h2>";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "bizname", array()), "html", null, true);
        echo "</h2>
                                 <div class=\"rating_inner\">
                                     <ul>
                                        ";
        // line 141
        if (($this->getAttribute((isset($context["finalRating"]) ? $context["finalRating"] : null), "rating", array()) == 0)) {
            // line 142
            echo "                                        <li><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
            echo "\" alt=\"star0\"></li><li><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
            echo "\" alt=\"star0\"></li><li><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
            echo "\" alt=\"star0\"></li><li><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
            echo "\" alt=\"star0\"></li><li><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
            echo "\" alt=\"star0\"></li>
                                        ";
        } else {
            // line 144
            echo "                                         <li><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("img/star" . $this->env->getExtension('app_extension')->roundRating(twig_round($this->getAttribute((isset($context["finalRating"]) ? $context["finalRating"] : null), "rating", array()), 1, "floor"))) . ".png")), "html", null, true);
            echo "\" alt=\"star";
            echo twig_escape_filter($this->env, twig_round($this->getAttribute((isset($context["finalRating"]) ? $context["finalRating"] : null), "rating", array()), 1, "floor"), "html", null, true);
            echo "\"></li>
                                         ";
        }
        // line 146
        echo "                                     </ul>
                                </div> </br>
                                <p><span>";
        // line 148
        echo twig_escape_filter($this->env, twig_round($this->getAttribute((isset($context["finalRating"]) ? $context["finalRating"] : null), "rating", array()), 1, "floor"), "html", null, true);
        echo "</span>| ";
        echo twig_escape_filter($this->env, (isset($context["noOfReviews"]) ? $context["noOfReviews"] : null), "html", null, true);
        echo " Verified Review(s)</p>
                            </div>
                            <div class=\"two_vendor_child_inner_pre_btn\">
                                <button type=\"button\" class=\"compare_btn\" data-toggle=\"modal\" data-target=\"#myModal\">SEND ENQUIRY </button>
                                <button type=\"button\" class=\"compare_btn\" id=\"getcontbutton\">VIEW CONTACT DETAILS </button>
                                ";
        // line 153
        if ( !twig_test_empty((isset($context["profile"]) ? $context["profile"] : null))) {
            // line 154
            echo "                                <div id=\"getcontvalue\" class=\"contactdetails mask\" ><h2>Contact details</h2> <p><label>Mobile </label> : ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "smsphone", array()), "html", null, true);
            echo " </p><p><label>Landline </label> : ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "homephone", array()), "html", null, true);
            echo "</p>  ";
            echo "</div>
                                ";
        }
        // line 156
        echo "                            </div>
                            <!-- Modal -->
                            <div class=\"modal fade bs-example-modal-sm\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
                              <div class=\"modal-dialog\" role=\"document\">
                                <div class=\"modal-content1\">
                                  <div class=\"modal-body\">
                                    ";
        // line 163
        echo "                                    <div class=\"main_area\">
                                      <div class=\"quort_area\">
                                          <div class=\"inner_quort_area\">
                                            <button id=\"closeModal\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                              <h2>MAKE AN ENQUIRY</h2>
                                              <p>Fill out the form to request a free estimate.</p>
                                              ";
        // line 169
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["enqForm"]) ? $context["enqForm"] : null), 'form_start', array("attr" => array("onsubmit" => "return validateForm();", "id" => "enqForm")));
        echo "
                                              <div class=\"form_quort\">
                                                    <div class=\"form-group1 contactname\">
                                                      ";
        // line 172
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "contactname", array()), 'label');
        echo "
                                                      ";
        // line 173
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "contactname", array()), 'widget');
        echo "
                                                      <small id=\"contactname\"></small>
                                                    </div>
                                                    <div class=\"form-group1 email\">
                                                      ";
        // line 177
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "email", array()), 'label');
        echo "
                                                      ";
        // line 178
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "email", array()), 'widget');
        echo "
                                                      <small id=\"email\"></small>
                                                    </div>
                                                    <div class=\"form-group1 phone\">
                                                      ";
        // line 182
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "mobile", array()), 'label');
        echo "
                                                      ";
        // line 183
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "mobile", array()), 'widget');
        echo "
                                                      <small id=\"phone\"></small>
                                                    </div>
                                                    <div class=\"form-group1 description\">
                                                      ";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "description", array()), 'label');
        echo "
                                                      ";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "description", array()), 'widget');
        echo "
                                                      <p>* Denotes a required field.</p>
                                                    </div>
                                                    ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["enqForm"]) ? $context["enqForm"] : null), "postJob", array()), 'widget');
        echo "
                                                    <div class=\"footer_text\">
                                                    </div>
                                              </div>
                                              ";
        // line 195
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["enqForm"]) ? $context["enqForm"] : null), 'form_end');
        echo "
                                          </div>
                                      </div>
                                  </div>
                                    ";
        // line 200
        echo "                                  </div>

                                </div>
                              </div>
                            </div>
                            ";
        // line 206
        echo "                        </div>
                        <div class=\"two_vendor_child_inner\">
                              <div class=\"two_vendor_child_mainmenu\">
                                  <ul>
                                      <li><a href=\"#home\" class=\"active\">Profile</a></li>
                                      <li><a href=\"#photos\">Photos</a></li>
                                      <li><a href=\"#reviews\">Ratings & Reviews</a></li>
                                      <li></li>
                                  </ul>
                              </div>

                          <!-- Tab panes -->
                          <div class=\"tab-content\">
                            <div id=\"home\">
                                <h2>";
        // line 220
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "bizname", array()), "html", null, true);
        echo "</h2>
                                <div id=\"profile_child\">
                                  ";
        // line 222
        if ( !twig_test_empty($this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "description", array()))) {
            // line 223
            echo "                                    ";
            // line 224
            echo "                                    <p>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "description", array()), "html", null, true);
            echo "</p>
                                  ";
        } else {
            // line 226
            echo "                                    <h2>No description</h2>
                                    <p>No description</p>
                                  ";
        }
        // line 229
        echo "                                </div>
                                <div class=\"profile_child_one\">
                                    <h3>Services Portfolio</h3>
                                    ";
        // line 232
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 233
            echo "                                      <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array(), "array"), "html", null, true);
            echo "</p>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 235
        echo "
                                    ";
        // line 236
        if ( !twig_test_empty((isset($context["services"]) ? $context["services"] : null))) {
            // line 237
            echo "                                      ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["services"]) ? $context["services"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["service"]) {
                // line 238
                echo "                                        <p>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["service"], "category", array()), "html", null, true);
                echo "</p>
                                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['service'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 240
            echo "                                    ";
        }
        // line 241
        echo "
                                    <h3>Certifications</h3>
                                    ";
        // line 243
        if ( !twig_test_empty((isset($context["certifications"]) ? $context["certifications"] : null))) {
            // line 244
            echo "                                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["certifications"]) ? $context["certifications"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 245
                echo "                                    <p> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "certification", array()), "html", null, true);
                echo " </p>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 247
            echo "                                    ";
        } else {
            // line 248
            echo "                                      <p>No certifications available</p>
                                    ";
        }
        // line 250
        echo "
                                    <h3>Products</h3>
                                    ";
        // line 252
        if ( !twig_test_empty((isset($context["products"]) ? $context["products"] : null))) {
            // line 253
            echo "                                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["pr"]) {
                // line 254
                echo "                                    <p> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pr"], "product", array()), "html", null, true);
                echo " </p>
                                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pr'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 256
            echo "                                    ";
        } else {
            // line 257
            echo "                                      <p>No products available</p>
                                    ";
        }
        // line 259
        echo "                                </div>
                                <div class=\"profile_child_two\">
                                    <h3>Service Area </h3>
                                    ";
        // line 262
        if ( !twig_test_empty((isset($context["cities"]) ? $context["cities"] : null))) {
            // line 263
            echo "                                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cities"]) ? $context["cities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
                // line 264
                echo "                                      <p>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "city", array()), "html", null, true);
                echo "</p>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 266
            echo "                                    ";
        } else {
            // line 267
            echo "                                      <p>No service area</p>
                                    ";
        }
        // line 269
        echo "
                                    <h3>Business License</h3>
                                    ";
        // line 271
        if ( !twig_test_empty((isset($context["expLicense"]) ? $context["expLicense"] : null))) {
            // line 272
            echo "                                      <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("img/licenses/" . $this->getAttribute((isset($context["expLicense"]) ? $context["expLicense"] : null), "file", array())) . "")), "html", null, true);
            echo "\" target=\"_blank\">
                                      <img src=\"";
            // line 273
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper_three.png"), "html", null, true);
            echo "\" alt=\"Praper_three\">
                                      CLICK TO VIEW</a>
                                    ";
        } else {
            // line 276
            echo "                                      <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper_three.png"), "html", null, true);
            echo "\" alt=\"Praper_three\">
                                      <p>No license to view</p>
                                    ";
        }
        // line 279
        echo "                                </div>
                            </div>
                        </div>

                              <div class=\"two_vendor_child_mainmenu\">
                                  <ul>
                                      <li><a href=\"#home\">Profile</a></li>
                                      <li><a href=\"#photos\" class=\"active\">Photos</a></li>
                                      <li><a href=\"#reviews\">Ratings & Reviews</a></li>
                                      <li></li>
                                  </ul>
                              </div>
                            <div id=\"photos\">
                                <h2>Photos</h2>
                                <div id=\"photos_child\">
                                    <h2>My Work</h2>
                                    <p>";
        // line 295
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["albums"]) ? $context["albums"] : null)), "html", null, true);
        echo " Projects</p>


    <!-- Jssor Slider Begin -->
    <!-- To move inline styles to css file/block, please specify a class name for each element. -->
    <div id=\"slider1_container\" style=\"position: relative; width: 720px;
        height: 480px; overflow: hidden;\">

        <!-- Loading Screen -->
        <div u=\"loading\" style=\"position: absolute; top: 0px; left: 0px;\">
            <div style=\"filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;\">
            </div>
            <div style=\"position: absolute; display: block; background: url(\"img/loading.gif\") no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;\">
            </div>
        </div>

        <!-- Slides Container -->
        <div u=\"slides\" style=\"cursor: move; position: absolute; left: 0px; top: 0px; width: 720px; height: 480px;
            overflow: hidden;\">
            ";
        // line 316
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        foreach ($context['_seq'] as $context["i"] => $context["album"]) {
            // line 317
            echo "              ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["photos"]) ? $context["photos"] : null), $context["i"], array(), "array"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["j"] => $context["p"]) {
                // line 318
                echo "                ";
                $context["albumid"] = $this->getAttribute($context["p"], "albumid", array());
                // line 319
                echo "                ";
                $context["photoid"] = $this->getAttribute($context["p"], "photoid", array());
                // line 320
                echo "                ";
                $context["ext"] = $this->getAttribute($context["p"], "phototag", array());
                // line 321
                echo "                ";
                $context["imagepath"] = $this->env->getExtension('assets')->getAssetUrl((((("gallery/" . (isset($context["albumid"]) ? $context["albumid"] : null)) . "/") . (isset($context["albumid"]) ? $context["albumid"] : null)) . $this->getAttribute($context["loop"], "index", array())));
                // line 322
                echo "            <div>
                <img u=\"image\" src=\"";
                // line 323
                echo twig_escape_filter($this->env, (isset($context["imagepath"]) ? $context["imagepath"] : null), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumname", array()), "html", null, true);
                echo "\">
                <img u=\"thumb\" src=\"";
                // line 324
                echo twig_escape_filter($this->env, (isset($context["imagepath"]) ? $context["imagepath"] : null), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumname", array()), "html", null, true);
                echo "\">
            </div>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['j'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 327
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 328
        echo "          ";
        // line 342
        echo "

        </div>
        <!--#region Thumbnail Navigator Skin Begin -->

        <!-- thumbnail navigator container -->
        <div u=\"thumbnavigator\" class=\"jssort07\" style=\"width: 720px; height: 100px; left: 0px; bottom: 0px;\">
            <!-- Thumbnail Item Skin Begin -->
            <div u=\"slides\" style=\"cursor: default;\">
                <div u=\"prototype\" class=\"p\">
                    <div u=\"thumbnailtemplate\" class=\"i\"></div>
                    <div class=\"o\"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
            <!--#region Arrow Navigator Skin Begin -->
            <!-- Help: http://www.jssor.com/development/slider-with-arrow-navigator-jquery.html -->

            <!-- Arrow Left -->
            <span u=\"arrowleft\" class=\"jssora11l\" style=\"top: 123px; left: 8px;\">
            </span>
            <!-- Arrow Right -->
            <span u=\"arrowright\" class=\"jssora11r\" style=\"top: 123px; right: 11px;\">
            </span>
            <!--#endregion Arrow Navigator Skin End -->
        </div>
        <!--#endregion Thumbnail Navigator Skin End -->

        <!-- Trigger -->
    </div>

                            </div>


                              <div class=\"two_vendor_child_mainmenu\">
                                  <ul>
                                      <li><a href=\"#home\">Profile</a></li>
                                      <li><a href=\"#photos\">Photos</a></li>
                                      <li><a href=\"#reviews\" class=\"active\">Ratings & Reviews</a></li>
                                      <li></li>
                                  </ul>
                              </div>
                            <div id=\"reviews\">
                                <h2>Ratings & Reviews</h2>
                               ";
        // line 387
        echo "                                ";
        if (((isset($context["war"]) ? $context["war"] : null) == 3)) {
            // line 388
            echo "                                  <p> You are logged in as <h3>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "bizname", array()), "html", null, true);
            echo "</h3></p>
                                ";
        } elseif ((        // line 389
(isset($context["war"]) ? $context["war"] : null) == 2)) {
            // line 390
            echo "                                  ";
            $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
            // line 391
            echo "                                  ";
            $context["returnurl"] = ("b_bids_b_bids_vendor+" . (isset($context["vendorid"]) ? $context["vendorid"] : null));
            // line 392
            echo "                                  ";
            $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
            // line 393
            echo "                                  <button type=\"submit\" class=\"reviews_button_review\" onclick=\"window.open('";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_login", array("returnurl" => (isset($context["returnurl"]) ? $context["returnurl"] : null))), "html", null, true);
            echo "', '_self');\">Write a Review</button>
                                ";
        } elseif ((        // line 394
(isset($context["war"]) ? $context["war"] : null) == 1)) {
            // line 395
            echo "                                  ";
            $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
            // line 396
            echo "                                  ";
            $context["returnurl"] = ((((("/post/review/" . (isset($context["userid"]) ? $context["userid"] : null)) . "/") . (isset($context["vendorid"]) ? $context["vendorid"] : null)) . "/") . (isset($context["enqid"]) ? $context["enqid"] : null));
            // line 397
            echo "                                    <button type=\"submit\" class=\"reviews_button_review\" onclick=\"window.open('";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_review", array("authorid" => (isset($context["userid"]) ? $context["userid"] : null), "vendorid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "enquiryid" => (isset($context["enqid"]) ? $context["enqid"] : null))), "html", null, true);
            echo "', '_self');\">Write a Review</button>
                                ";
        } elseif ((        // line 398
(isset($context["war"]) ? $context["war"] : null) == 0)) {
            // line 399
            echo "                                   <p> You do not have any enquiry mapped against this vendor to write a review </p>
                                ";
        }
        // line 401
        echo "
                                <div id=\"reviews_child\">
                                    <ul>
                                        <li><h4>Sort by newest</h4></li>
                                        <li>
                                          <select class=\"dropdown_select\" name=\"dateSorting\" id=\"dateSorting\">
                                          <option value=\"\">Date</option>
                                          <option value=\"";
        // line 408
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "?sort=date&type=asc#reviews\">Date Ascending</option>
                                          <option value=\"";
        // line 409
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "?sort=date&type=desc#reviews\">Date Decending</option>
                                          </select>
                                        </li>
                                        <li>";
        // line 413
        echo "                                          <select class=\"dropdown_select\" name=\"ratingSorting\" id=\"ratingSorting\">
                                          <option value=\"Rating\"><a href=\"?sort=rating\">Rating</a></option>
                                          <option value=\"";
        // line 415
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "?sort=rating&type=asc#reviews\">Rating Ascending</option>
                                          <option value=\"";
        // line 416
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "catid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "?sort=rating&type=desc#reviews\">Rating Decending</option>
                                          </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class=\"comments_area\">
                                    ";
        // line 422
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 423
            echo "                                    <div class=\"reviews_inner_rating\">
                                         <div class=\"rating_inner rating_inners\">
                                             <ul>
                                                 ";
            // line 426
            if (($this->getAttribute($context["r"], "rating", array()) == 0)) {
                // line 427
                echo "                                                  <li><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                echo "\" alt=\"star0\"></li><li><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                echo "\" alt=\"star0\"></li><li><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                echo "\" alt=\"star0\"></li><li><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                echo "\" alt=\"star0\"></li><li><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/srar_blanck.png"), "html", null, true);
                echo "\" alt=\"star0\"></li>
                                                  ";
            } else {
                // line 429
                echo "                                                   <li><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("img/star" . $this->env->getExtension('app_extension')->roundRating(twig_round($this->getAttribute($context["r"], "rating", array()), 1, "floor"))) . ".png")), "html", null, true);
                echo "\" alt=\"star";
                echo twig_escape_filter($this->env, twig_round($this->getAttribute($context["r"], "rating", array()), 1, "floor"), "html", null, true);
                echo "\"></li>
                                                   ";
            }
            // line 431
            echo "                                             </ul>
                                         </div>
                                        <p><span>";
            // line 433
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "rating", array()), "html", null, true);
            echo "</span>| ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-m-d"), "html", null, true);
            echo "</p>
                                    </div>
                                    <div class=\"reviews_inner_rating_text\">
                                        <p><span>";
            // line 436
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "author", array()), "html", null, true);
            echo "</span>";
            // line 437
            echo "</p>
                                        <p class=\"botm\">";
            // line 438
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "review", array()), "html", null, true);
            echo "</p>
                                    </div>
                                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 441
        echo "                          <div class=\"border_area\"></div>
                         ";
        // line 457
        echo "                                </div>
                            </div>
                          </div>
                              <div class=\"two_vendor_child_mainmenu\">
                                  <ul>
                                      <li><a href=\"#home\" class=\"active\">Profile</a></li>
                                      <li><a href=\"#photos\">Photos</a></li>
                                      <li><a href=\"#reviews\">Ratings & Reviews</a></li>
                                      <li><a href=\"#homes\">Top of Page</a></li>
                                  </ul>
                              </div>

                        </div>
                       </div>
                    </div>
                    <div id=\"sticky-anchor\"></div>
              <div id=\"sticky\" class=\"two_vendor_sideber\"><!-- Sidber -->
                <div class=\"two_vendor_sideber_inner\">
                  <h2>Find Companies</h2>
                  <p>Just tell us a little bit about what you need, and receive quotations from leading companies in the UAE.</p>
                  <a href=\"";
        // line 477
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\"><button type=\"button\" class=\"compare_btn\">COMPARE QUOTES NOW</button></a>
                  ";
        // line 483
        echo "                </div>
              </div>
</div>
</div>

<script type=\"text/javascript\">
\$(document).ready(function(){
  \$('select#ratingSorting,select#dateSorting').on('change', function () {
      var url = \$(this).val(); // get selected value
      if (url !='') { // require a URL
          window.location = url; // redirect
      }
      return false;
  });
  \$(\"#direct_enq_form_mobile\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#phone').html('Enter phone number.');
        } else if(isNaN(ccVal)) {
            \$('small#phone').html('Numbers only !');
        }
  });


});
  function validateForm () {
    var name =  /^[a-zA-Z ]*\$/, email = /^[a-zA-Z0-9.!#\$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*\$/;
    if(\$('#direct_enq_form_contactname').length > 0) {
      if(\$('#direct_enq_form_contactname').val() == '')
      {
          \$('small#name').text(\"Please enter your full name\");
          \$('div.contactname').css(\"border\",\"2px solid red\");
          return false;
      }
      else if(!name.test(\$('#direct_enq_form_contactname').val()))
      {
          \$('small#name').text(\"Please enter a valid full name\");
          \$('div.contactname').css(\"border\",\"2px solid red\");
          return false;
      }
      \$('div.contactname').removeAttr('style');\$('small#name').empty();
    }
    if(\$('#direct_enq_form_email').length > 0) {
      if(\$('#direct_enq_form_email').val() == '')
      {
          \$('small#email').text(\"Please enter your Email-Id\");
          \$('div.email').css(\"border\",\"2px solid red\");
          return false;
      }
      else if(!email.test(\$('#direct_enq_form_email').val()))
      {
          \$('small#email').text(\"Please enter a valid Email-Id\");
          \$('div.email').css(\"border\",\"2px solid red\");
          return false;
      }
      \$('div.email').removeAttr('style');\$('small#email').empty();
    }
    if(\$('#direct_enq_form_mobile').length && (\$('#direct_enq_form_mobile').val() == ''))
    {
        \$('small#phone').html(\"Please enter a valid phone number\");
        \$('div.phone').css(\"border\",\"2px solid red\");
        return false;
    }
    \$('div.phone').removeAttr('style');\$('small#phone').empty();
    if(\$('#direct_enq_form_description').length && (\$('#direct_enq_form_description').val() == ''))
    {
        \$('small#description').html(\"Please enter description\");
        \$('div.description').css(\"border\",\"2px solid red\");
        return false;
    }
    \$('div.description').removeAttr('style');\$('small#description').empty();
    \$.ajax({
       type: \"POST\",
       url: '";
        // line 556
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_direct_enquiry");
        echo "',
       data: \$(\"#enqForm\").serialize(), // serializes the form's elements.
       success: function(data)
       {
          // \$('#myModal').modal('hide');
          alert(\"Enquiry sent successful\");
          \$('div#myModal').removeAttr('style');
          \$('body').removeClass('modal-open');
          \$('.modal-backdrop').remove();
          \$(\"#enqForm\")[0].reset();
       }
    });
    return false;
  }
</script>


</div>


";
    }

    // line 577
    public function block_footerScript($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:vendor_profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  963 => 577,  938 => 556,  863 => 483,  859 => 477,  837 => 457,  834 => 441,  825 => 438,  822 => 437,  819 => 436,  811 => 433,  807 => 431,  799 => 429,  785 => 427,  783 => 426,  778 => 423,  774 => 422,  765 => 416,  761 => 415,  757 => 413,  751 => 409,  747 => 408,  738 => 401,  734 => 399,  732 => 398,  727 => 397,  724 => 396,  721 => 395,  719 => 394,  714 => 393,  711 => 392,  708 => 391,  705 => 390,  703 => 389,  698 => 388,  695 => 387,  649 => 342,  647 => 328,  641 => 327,  622 => 324,  616 => 323,  613 => 322,  610 => 321,  607 => 320,  604 => 319,  601 => 318,  583 => 317,  579 => 316,  555 => 295,  537 => 279,  530 => 276,  524 => 273,  519 => 272,  517 => 271,  513 => 269,  509 => 267,  506 => 266,  497 => 264,  492 => 263,  490 => 262,  485 => 259,  481 => 257,  478 => 256,  469 => 254,  464 => 253,  462 => 252,  458 => 250,  454 => 248,  451 => 247,  442 => 245,  437 => 244,  435 => 243,  431 => 241,  428 => 240,  419 => 238,  414 => 237,  412 => 236,  409 => 235,  400 => 233,  396 => 232,  391 => 229,  386 => 226,  380 => 224,  378 => 223,  376 => 222,  371 => 220,  355 => 206,  348 => 200,  341 => 195,  334 => 191,  328 => 188,  324 => 187,  317 => 183,  313 => 182,  306 => 178,  302 => 177,  295 => 173,  291 => 172,  285 => 169,  277 => 163,  269 => 156,  260 => 154,  258 => 153,  248 => 148,  244 => 146,  236 => 144,  222 => 142,  220 => 141,  214 => 138,  210 => 136,  202 => 134,  196 => 132,  194 => 131,  184 => 124,  128 => 71,  65 => 11,  61 => 10,  57 => 9,  52 => 8,  49 => 7,  44 => 4,  38 => 3,  32 => 2,  11 => 1,);
    }
}

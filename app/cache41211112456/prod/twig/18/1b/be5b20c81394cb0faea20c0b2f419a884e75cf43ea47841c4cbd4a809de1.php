<?php

/* BBidsBBidsHomeBundle:Admin:emailidupdation.html.twig */
class __TwigTemplate_181bbe5b20c81394cb0faea20c0b2f419a884e75cf43ea47841c4cbd4a809de1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<p>
<b>
<span style=\"font-size:10.0pt;font-family:Helvetica,sans-serif\"><b>Dear ";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ",</b></span>
</b><br/>
<span style=\"font-size:10.0pt;font-family:Helvetica,sans-serif\">
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left\"><strong>Your Email ID has been updated by admin.</p>
</span>
<span>
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left\"><strong>If you have any questions please feel free to contact our support center on <a style=\"color: #0099FF;\">(04) 42 13 777</a>.</strong></P>
<br>
<br>
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left\">Regards
<br>
BusinessBid</p>
</span>
</p>
</p>
</html>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:emailidupdation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }
}

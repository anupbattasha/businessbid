<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">


    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 162
        $this->displayBlock('customcss', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('javascripts', $context, $blocks);
        // line 309
        echo "
";
        // line 310
        $this->displayBlock('customjs', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('jquery', $context, $blocks);
        // line 317
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 323
        echo "</head>
";
        // line 324
        ob_start();
        // line 325
        echo "<body>

";
        // line 327
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 328
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 329
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 330
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 331
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 342
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 343
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 345
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 346
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 349
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 350
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 352
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 353
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 359
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 366
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 369
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 372
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 374
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 379
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 384
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 385
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 387
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 390
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 391
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 395
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 402
        $this->displayBlock('maincontent', $context, $blocks);
        // line 633
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 636
        $this->displayBlock('footer', $context, $blocks);
        // line 749
        echo "    <!-- footer block ends -->
</div>";
        // line 752
        $this->displayBlock('footerScript', $context, $blocks);
        // line 756
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 763
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_5dc16744365c20a74d60aa979ed5469c940b010aa8908929e43595f58f38699f = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c55a195ed163e56434fff34005a5aeccdff7ea8640d190510c95459996471dd5 = "http://www.businessbid.ae/contactus") && ('' === $__internal_c55a195ed163e56434fff34005a5aeccdff7ea8640d190510c95459996471dd5 || 0 === strpos($__internal_5dc16744365c20a74d60aa979ed5469c940b010aa8908929e43595f58f38699f, $__internal_c55a195ed163e56434fff34005a5aeccdff7ea8640d190510c95459996471dd5)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_290122f567bb6fe6e7d0eab02a4ce46fcb08fa0143aa06e4887cb8cfd6b03dd0 = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_de36cc3a38c2a7f5282a8981b46e79d40b6406ed58ab40296e34c56f26d83cef = "http://www.businessbid.ae/node3") && ('' === $__internal_de36cc3a38c2a7f5282a8981b46e79d40b6406ed58ab40296e34c56f26d83cef || 0 === strpos($__internal_290122f567bb6fe6e7d0eab02a4ce46fcb08fa0143aa06e4887cb8cfd6b03dd0, $__internal_de36cc3a38c2a7f5282a8981b46e79d40b6406ed58ab40296e34c56f26d83cef)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_9253611ff0dfc60a6cc84ebdb088d8af8c7c0a8e56925afa45e479d14786028a = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_4ecaf8b7c3edbd0a9e90bd413e179661d772f9093878948770451cb231d6886e = "http://www.businessbid.ae/review/login") && ('' === $__internal_4ecaf8b7c3edbd0a9e90bd413e179661d772f9093878948770451cb231d6886e || 0 === strpos($__internal_9253611ff0dfc60a6cc84ebdb088d8af8c7c0a8e56925afa45e479d14786028a, $__internal_4ecaf8b7c3edbd0a9e90bd413e179661d772f9093878948770451cb231d6886e)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_694268b020eb4703d781d5a78dbbfbe2620bd40be805850364346c1e85d01719 = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5dc8fb943dd9dcb3431a4c50792e4754373cf0b3d280d38a069a83de7874c6ae = "http://www.businessbid.ae/node1") && ('' === $__internal_5dc8fb943dd9dcb3431a4c50792e4754373cf0b3d280d38a069a83de7874c6ae || 0 === strpos($__internal_694268b020eb4703d781d5a78dbbfbe2620bd40be805850364346c1e85d01719, $__internal_5dc8fb943dd9dcb3431a4c50792e4754373cf0b3d280d38a069a83de7874c6ae)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_8fc7dbe7590043550d30b474828e3da4958d58dc38c45720b6b371d5aa9b8992 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8c01283d39b5a0aa7dce1153ea8dfd51f78660f1298e1373554224f8ebc387e8 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_8c01283d39b5a0aa7dce1153ea8dfd51f78660f1298e1373554224f8ebc387e8 || 0 === strpos($__internal_8fc7dbe7590043550d30b474828e3da4958d58dc38c45720b6b371d5aa9b8992, $__internal_8c01283d39b5a0aa7dce1153ea8dfd51f78660f1298e1373554224f8ebc387e8)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_90b71244c6a730193f1066c1768f47ff1a28a3f53d02d87746dba6d8e7edc1e0 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f39b9e167fbdcc1c5a077e0c18658d32a4594181f9604b039e29eb807a554841 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_f39b9e167fbdcc1c5a077e0c18658d32a4594181f9604b039e29eb807a554841 || 0 === strpos($__internal_90b71244c6a730193f1066c1768f47ff1a28a3f53d02d87746dba6d8e7edc1e0, $__internal_f39b9e167fbdcc1c5a077e0c18658d32a4594181f9604b039e29eb807a554841)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_61404de9c6060273c6031739ee2415e5908ff9931b856b09e87afd47f241723b = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d08acbdd4dfa3783706dee2c9b75dfe30f998580bb1284024185723191d9faef = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_d08acbdd4dfa3783706dee2c9b75dfe30f998580bb1284024185723191d9faef || 0 === strpos($__internal_61404de9c6060273c6031739ee2415e5908ff9931b856b09e87afd47f241723b, $__internal_d08acbdd4dfa3783706dee2c9b75dfe30f998580bb1284024185723191d9faef)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_8cfc80222a79814a58d7311d764aaf443d16512d001e3efeb32b63005c322492 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_7985000a3fc91b2e19ba2b84cc1924009ea6cf1f709a20a5e29116e401f05879 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_7985000a3fc91b2e19ba2b84cc1924009ea6cf1f709a20a5e29116e401f05879 || 0 === strpos($__internal_8cfc80222a79814a58d7311d764aaf443d16512d001e3efeb32b63005c322492, $__internal_7985000a3fc91b2e19ba2b84cc1924009ea6cf1f709a20a5e29116e401f05879)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_ea6c0cdd7f5dcf1cecac812170671c98566c7a6dddab717d4e099470a762576a = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_02094edea2f0a6d6fed37a5f820ac25e4656f65a7be064d61715a4d52d66befe = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_02094edea2f0a6d6fed37a5f820ac25e4656f65a7be064d61715a4d52d66befe || 0 === strpos($__internal_ea6c0cdd7f5dcf1cecac812170671c98566c7a6dddab717d4e099470a762576a, $__internal_02094edea2f0a6d6fed37a5f820ac25e4656f65a7be064d61715a4d52d66befe)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_41992ba16a8b3cf59d0768367ec1f28c1fd9bb99fa27cc12f8110d324e6f5d0c = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_33c8598033e623443244e48b6e004dde518b05fb29ce6f327dfe3d6c93bbc633 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_33c8598033e623443244e48b6e004dde518b05fb29ce6f327dfe3d6c93bbc633 || 0 === strpos($__internal_41992ba16a8b3cf59d0768367ec1f28c1fd9bb99fa27cc12f8110d324e6f5d0c, $__internal_33c8598033e623443244e48b6e004dde518b05fb29ce6f327dfe3d6c93bbc633)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_a911f73b0e56e0a7030479e54a7a69b8ec4ee1b7c0d9c298b35e6d1b7820b63e = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a3351de0d985a744363243e7db4d0f09916d8a338439bea62ecff158468a9de1 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_a3351de0d985a744363243e7db4d0f09916d8a338439bea62ecff158468a9de1 || 0 === strpos($__internal_a911f73b0e56e0a7030479e54a7a69b8ec4ee1b7c0d9c298b35e6d1b7820b63e, $__internal_a3351de0d985a744363243e7db4d0f09916d8a338439bea62ecff158468a9de1)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_43736d5870c69df7d7524f9215b60a3ebe02c1d701f43082742386300fbf7c68 = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_9474caa5d1bdf4a14f3a1f8b2801afaddbba711e2aa09479a63f05318f7790cc = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_9474caa5d1bdf4a14f3a1f8b2801afaddbba711e2aa09479a63f05318f7790cc || 0 === strpos($__internal_43736d5870c69df7d7524f9215b60a3ebe02c1d701f43082742386300fbf7c68, $__internal_9474caa5d1bdf4a14f3a1f8b2801afaddbba711e2aa09479a63f05318f7790cc)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_52363b075936139e7d7ecb7b08ed434b6dbf079f5f2d85e5e01c3f72eb61db58 = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6aae3f398e1a41fa7cfd5b9e2fa76c0e662c207a93bf8b8390296804b80ee5b0 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_6aae3f398e1a41fa7cfd5b9e2fa76c0e662c207a93bf8b8390296804b80ee5b0 || 0 === strpos($__internal_52363b075936139e7d7ecb7b08ed434b6dbf079f5f2d85e5e01c3f72eb61db58, $__internal_6aae3f398e1a41fa7cfd5b9e2fa76c0e662c207a93bf8b8390296804b80ee5b0)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_b4be38111c6e781cb66a79d54d35f9d378b0f6516da74794c85eeefbd4c665a1 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3b98194d112d1459aa07d64304ee73a909a92f65dc08ff4be3083421e79f156f = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_3b98194d112d1459aa07d64304ee73a909a92f65dc08ff4be3083421e79f156f || 0 === strpos($__internal_b4be38111c6e781cb66a79d54d35f9d378b0f6516da74794c85eeefbd4c665a1, $__internal_3b98194d112d1459aa07d64304ee73a909a92f65dc08ff4be3083421e79f156f)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_fe8a708d147cc847ecc1e295c702f9a513330a53ad3c3781b3157a587ca8fcfb = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_477ed4b58bf2b3ab71808e4e37d0582410cbbb5adf683c68fc72710a88e277bd = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_477ed4b58bf2b3ab71808e4e37d0582410cbbb5adf683c68fc72710a88e277bd || 0 === strpos($__internal_fe8a708d147cc847ecc1e295c702f9a513330a53ad3c3781b3157a587ca8fcfb, $__internal_477ed4b58bf2b3ab71808e4e37d0582410cbbb5adf683c68fc72710a88e277bd)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_e436b7ef74cec860a990a2bdd2833fc849f030cb112a7449c8f0ec0f307a15fb = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_bb1f855b8e01e21d2c32888023238051224ea379f80c274a7fed693d3ccfd427 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_bb1f855b8e01e21d2c32888023238051224ea379f80c274a7fed693d3ccfd427 || 0 === strpos($__internal_e436b7ef74cec860a990a2bdd2833fc849f030cb112a7449c8f0ec0f307a15fb, $__internal_bb1f855b8e01e21d2c32888023238051224ea379f80c274a7fed693d3ccfd427)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_2794356f7679493bddd2de5a0efff235d3c03cc8eed8e12fbb14928f58ac1a92 = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3f9e5b02b35cab848c68d1018aae5558b5ce109bfc4c107b63c476679481f07f = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_3f9e5b02b35cab848c68d1018aae5558b5ce109bfc4c107b63c476679481f07f || 0 === strpos($__internal_2794356f7679493bddd2de5a0efff235d3c03cc8eed8e12fbb14928f58ac1a92, $__internal_3f9e5b02b35cab848c68d1018aae5558b5ce109bfc4c107b63c476679481f07f)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_dbc018e2076b8ca8488b74ce25b6fb60c8d00a782425304f5caeabae564bb97b = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_befe8ccf83c0921b37b9d60212ea78d6226f8ea7bdc374d0b3d4e0d96a1c3f2c = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_befe8ccf83c0921b37b9d60212ea78d6226f8ea7bdc374d0b3d4e0d96a1c3f2c || 0 === strpos($__internal_dbc018e2076b8ca8488b74ce25b6fb60c8d00a782425304f5caeabae564bb97b, $__internal_befe8ccf83c0921b37b9d60212ea78d6226f8ea7bdc374d0b3d4e0d96a1c3f2c)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_a802ba99f428fc0a6265c46da4011a957c3ae89808370684246b502881ad5e16 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2d8bbc471e774525f6628ef7240a5a47d044d95f0ecaf964c075fda1b1c81172 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_2d8bbc471e774525f6628ef7240a5a47d044d95f0ecaf964c075fda1b1c81172 || 0 === strpos($__internal_a802ba99f428fc0a6265c46da4011a957c3ae89808370684246b502881ad5e16, $__internal_2d8bbc471e774525f6628ef7240a5a47d044d95f0ecaf964c075fda1b1c81172)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_1c0eba13807a02859d960d57f9223b3daad13f235b7839ce1b8f751942a09b15 = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2609f23494f2008f308819905cfa28bf6ba1854fcca19085f16c26eab08f55ec = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_2609f23494f2008f308819905cfa28bf6ba1854fcca19085f16c26eab08f55ec || 0 === strpos($__internal_1c0eba13807a02859d960d57f9223b3daad13f235b7839ce1b8f751942a09b15, $__internal_2609f23494f2008f308819905cfa28bf6ba1854fcca19085f16c26eab08f55ec)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_7e8118bb1562ce0c9bec79a27cc0e8ec3b97121ffde26a22a034a06f16bd0f1a = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_7acb02eae8ec47e31bd3b779ade9399c5a80561f579c6b8917dca34e5e610575 = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_7acb02eae8ec47e31bd3b779ade9399c5a80561f579c6b8917dca34e5e610575 || 0 === strpos($__internal_7e8118bb1562ce0c9bec79a27cc0e8ec3b97121ffde26a22a034a06f16bd0f1a, $__internal_7acb02eae8ec47e31bd3b779ade9399c5a80561f579c6b8917dca34e5e610575)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_ba3dc00a5afb4f425bb26bbd70508ef12dbb878b24a4d9a772f3f54d29ba8c17 = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f822d17b37c319e1dbe3d1dd02cb0e1a047f8f9c8844eb5738f9ce6541aad49f = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_f822d17b37c319e1dbe3d1dd02cb0e1a047f8f9c8844eb5738f9ce6541aad49f || 0 === strpos($__internal_ba3dc00a5afb4f425bb26bbd70508ef12dbb878b24a4d9a772f3f54d29ba8c17, $__internal_f822d17b37c319e1dbe3d1dd02cb0e1a047f8f9c8844eb5738f9ce6541aad49f)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_b46ffca6b5efd0f14a18b8a7be339a59c5338a84cd88c893401746a8502ad741 = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ca87d95e0d0ea33b47b3cc2777a01db7e6e17f8481808fc0c41002521bf8fa2f = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_ca87d95e0d0ea33b47b3cc2777a01db7e6e17f8481808fc0c41002521bf8fa2f || 0 === strpos($__internal_b46ffca6b5efd0f14a18b8a7be339a59c5338a84cd88c893401746a8502ad741, $__internal_ca87d95e0d0ea33b47b3cc2777a01db7e6e17f8481808fc0c41002521bf8fa2f)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_a49d8d04b88fadf14f3dbfa2cc5651f2b6a7a9f5b07a49a72d7e3f08a0866d8b = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b35cdf56bd61bdc4c643cc89ac96efc17d114484ed212214262e2fd5e7a0a09d = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_b35cdf56bd61bdc4c643cc89ac96efc17d114484ed212214262e2fd5e7a0a09d || 0 === strpos($__internal_a49d8d04b88fadf14f3dbfa2cc5651f2b6a7a9f5b07a49a72d7e3f08a0866d8b, $__internal_b35cdf56bd61bdc4c643cc89ac96efc17d114484ed212214262e2fd5e7a0a09d)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_fdeb138dec141ff7ccb086b7aa865aca03835531ff3a9645b540adaca448f87b = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_762acf283b8f1895502416e94d01e5a19d4ddf33ad3d3b3bc5787cac85d266b3 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_762acf283b8f1895502416e94d01e5a19d4ddf33ad3d3b3bc5787cac85d266b3 || 0 === strpos($__internal_fdeb138dec141ff7ccb086b7aa865aca03835531ff3a9645b540adaca448f87b, $__internal_762acf283b8f1895502416e94d01e5a19d4ddf33ad3d3b3bc5787cac85d266b3)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_dba7e550d6b057029d7317b98c8daa342cd299d82743a2c2ac0491e3d739cddc = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a03e08aecfb162279aee4a9129d547727aa16f83dc6b6872b2002ecdbebf84c8 = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_a03e08aecfb162279aee4a9129d547727aa16f83dc6b6872b2002ecdbebf84c8 || 0 === strpos($__internal_dba7e550d6b057029d7317b98c8daa342cd299d82743a2c2ac0491e3d739cddc, $__internal_a03e08aecfb162279aee4a9129d547727aa16f83dc6b6872b2002ecdbebf84c8)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_2d098acd7cabf067f34dc6d5588b3fc4aac28580186aa71a61562f42bba75778 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_885b2d68b6a1520a08d5da2da37a8defe2d67023902d00fb6a6c6efc0fda7d96 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_885b2d68b6a1520a08d5da2da37a8defe2d67023902d00fb6a6c6efc0fda7d96 || 0 === strpos($__internal_2d098acd7cabf067f34dc6d5588b3fc4aac28580186aa71a61562f42bba75778, $__internal_885b2d68b6a1520a08d5da2da37a8defe2d67023902d00fb6a6c6efc0fda7d96)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_c1088e7f1287fe75b3cfccfc56fae14b62d3d8da0645ff6d1eafaf21fe456804 = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_19e3f4532828844b151128b1914971fa707ebb5c2742e791857ae9742e644b79 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_19e3f4532828844b151128b1914971fa707ebb5c2742e791857ae9742e644b79 || 0 === strpos($__internal_c1088e7f1287fe75b3cfccfc56fae14b62d3d8da0645ff6d1eafaf21fe456804, $__internal_19e3f4532828844b151128b1914971fa707ebb5c2742e791857ae9742e644b79)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_afcdee75d67a2da8f2f4cf252fe426bfe47a45f1344ea0f5d32b76a7a6fd148b = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3c20633a7c2b8382d054cdf6cd3bd6cba155d3da43da3702e8a8a294fb4a4bae = "http://www.businessbid.ae/node2") && ('' === $__internal_3c20633a7c2b8382d054cdf6cd3bd6cba155d3da43da3702e8a8a294fb4a4bae || 0 === strpos($__internal_afcdee75d67a2da8f2f4cf252fe426bfe47a45f1344ea0f5d32b76a7a6fd148b, $__internal_3c20633a7c2b8382d054cdf6cd3bd6cba155d3da43da3702e8a8a294fb4a4bae)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_8db0347299b0dbdd9b1fba22b650aeae7609fa20dd5bf63c32316876b43ceeba = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5d9bef7b0013bbcfe38de15d95350330449827a1496c26efc3c4e27e9d143e99 = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_5d9bef7b0013bbcfe38de15d95350330449827a1496c26efc3c4e27e9d143e99 || 0 === strpos($__internal_8db0347299b0dbdd9b1fba22b650aeae7609fa20dd5bf63c32316876b43ceeba, $__internal_5d9bef7b0013bbcfe38de15d95350330449827a1496c26efc3c4e27e9d143e99)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_744f38b69a0b9d29a34776c6248d481609db54a22cc20b5d75cd566430fc3158 = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_bdf4d8c2aa7868dd9cb67c1c9203831ae1a0f02930fff4079d0257b96d1c40ed = "http://www.businessbid.ae/faq") && ('' === $__internal_bdf4d8c2aa7868dd9cb67c1c9203831ae1a0f02930fff4079d0257b96d1c40ed || 0 === strpos($__internal_744f38b69a0b9d29a34776c6248d481609db54a22cc20b5d75cd566430fc3158, $__internal_bdf4d8c2aa7868dd9cb67c1c9203831ae1a0f02930fff4079d0257b96d1c40ed)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_e151c9089a87c130a5a34ba36595842aeef0c236624bf030c0db1a0d345d4d78 = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_677e53574f00542474501cd548d3b2ab8e43047ed77d4a1e691465b9b321577f = "http://www.businessbid.ae/node4") && ('' === $__internal_677e53574f00542474501cd548d3b2ab8e43047ed77d4a1e691465b9b321577f || 0 === strpos($__internal_e151c9089a87c130a5a34ba36595842aeef0c236624bf030c0db1a0d345d4d78, $__internal_677e53574f00542474501cd548d3b2ab8e43047ed77d4a1e691465b9b321577f)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_6b5828f72274092696cdbe7f572ca408b2327bee43aa75f8000f2fe4cb0cf7d1 = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_e00df772041a49518c5f3844d3964ae9bc8787ce3487c85bb6ee54d7ea992cf1 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_e00df772041a49518c5f3844d3964ae9bc8787ce3487c85bb6ee54d7ea992cf1 || 0 === strpos($__internal_6b5828f72274092696cdbe7f572ca408b2327bee43aa75f8000f2fe4cb0cf7d1, $__internal_e00df772041a49518c5f3844d3964ae9bc8787ce3487c85bb6ee54d7ea992cf1)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_e02da72484c5f0e947201914c4fb12ea533f5d41ecbe980a8eeea689e119070f = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b5e7dbb524db1eebda7879096506912249ef70a1f8ff27b5100c897c6db8bc3e = "http://www.businessbid.ae/node6") && ('' === $__internal_b5e7dbb524db1eebda7879096506912249ef70a1f8ff27b5100c897c6db8bc3e || 0 === strpos($__internal_e02da72484c5f0e947201914c4fb12ea533f5d41ecbe980a8eeea689e119070f, $__internal_b5e7dbb524db1eebda7879096506912249ef70a1f8ff27b5100c897c6db8bc3e)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_7c5bca8127ec59e0618f94ad86f55c0f46b257f14b561322886f5ca8b85e4ceb = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a96f5327e4f1c4c934cdfdd90e8732dae04160bbe90f3a4a22e5a2de09143d6d = "http://www.businessbid.ae/feedback") && ('' === $__internal_a96f5327e4f1c4c934cdfdd90e8732dae04160bbe90f3a4a22e5a2de09143d6d || 0 === strpos($__internal_7c5bca8127ec59e0618f94ad86f55c0f46b257f14b561322886f5ca8b85e4ceb, $__internal_a96f5327e4f1c4c934cdfdd90e8732dae04160bbe90f3a4a22e5a2de09143d6d)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_3ffad8b65c73dae57bc9c99b21c3faff9521a0b966e7f56e54f451d747a10adf = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0d2da04f2043b6274b46351eefeca63605401d3e4b47b305363cdd2be5b0a794 = "http://www.businessbid.ae/sitemap") && ('' === $__internal_0d2da04f2043b6274b46351eefeca63605401d3e4b47b305363cdd2be5b0a794 || 0 === strpos($__internal_3ffad8b65c73dae57bc9c99b21c3faff9521a0b966e7f56e54f451d747a10adf, $__internal_0d2da04f2043b6274b46351eefeca63605401d3e4b47b305363cdd2be5b0a794)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_6fab999be9ece0d765cb3ba38f16ed46f0d4934a5ce0e873d9803d91afbf827c = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0a00e2e69a513357c4799cf8e9e257234d7ca6bacee603311d37bf0e6a50b81b = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_0a00e2e69a513357c4799cf8e9e257234d7ca6bacee603311d37bf0e6a50b81b || 0 === strpos($__internal_6fab999be9ece0d765cb3ba38f16ed46f0d4934a5ce0e873d9803d91afbf827c, $__internal_0a00e2e69a513357c4799cf8e9e257234d7ca6bacee603311d37bf0e6a50b81b)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_92c10b1fc0372ac25bc8c6f1fb185493431ca538021456efda3f1f95d554be83 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_78b42c196bbffdd28670753cdeeb6cd7e0e2f043041064ce02a4bf4d2c59d52e = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_78b42c196bbffdd28670753cdeeb6cd7e0e2f043041064ce02a4bf4d2c59d52e || 0 === strpos($__internal_92c10b1fc0372ac25bc8c6f1fb185493431ca538021456efda3f1f95d554be83, $__internal_78b42c196bbffdd28670753cdeeb6cd7e0e2f043041064ce02a4bf4d2c59d52e)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_70a8c8e838a5eef6f3e372d7d856bc7e2f038d4f9d241d21be681b5f4a397910 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_457344c9a61aa340e8cddb27f83ca737d504f579d1e42b6e6181eb80843ba2c7 = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_457344c9a61aa340e8cddb27f83ca737d504f579d1e42b6e6181eb80843ba2c7 || 0 === strpos($__internal_70a8c8e838a5eef6f3e372d7d856bc7e2f038d4f9d241d21be681b5f4a397910, $__internal_457344c9a61aa340e8cddb27f83ca737d504f579d1e42b6e6181eb80843ba2c7)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_55ce532684109a1f2fd454da592f182372f833410fc29de92d36e376aebf64f9 = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a60d628f7709b1fd4223410ce99ec5596810efd3926a16e411bc3a6cd049e9e8 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_a60d628f7709b1fd4223410ce99ec5596810efd3926a16e411bc3a6cd049e9e8 || 0 === strpos($__internal_55ce532684109a1f2fd454da592f182372f833410fc29de92d36e376aebf64f9, $__internal_a60d628f7709b1fd4223410ce99ec5596810efd3926a16e411bc3a6cd049e9e8)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 162
    public function block_customcss($context, array $blocks = array())
    {
        // line 163
        echo "
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "<script>
\$(document).ready(function(){
    ";
        // line 169
        if (array_key_exists("keyword", $context)) {
            // line 170
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 174
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 310
    public function block_customjs($context, array $blocks = array())
    {
        // line 311
        echo "
";
    }

    // line 314
    public function block_jquery($context, array $blocks = array())
    {
        // line 315
        echo "
";
    }

    // line 317
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 318
        echo "
<!-- Google Analytics -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>

";
    }

    // line 402
    public function block_maincontent($context, array $blocks = array())
    {
        // line 403
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 490
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 530
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 560
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 561
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 563
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 565
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 571
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 572
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 574
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 576
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 581
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 582
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 584
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 586
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 593
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 594
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 596
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 598
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 604
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 605
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 607
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 609
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 615
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 616
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 618
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 620
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 490
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 491
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 500
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 501
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 502
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 503
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 504
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 505
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 506
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 508
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 509
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 510
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 512
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 519
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 525
        echo "    </div>
            </div>


    ";
    }

    // line 519
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 520
        echo "    <div class=\"request\">
    <h2>";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 636
    public function block_footer($context, array $blocks = array())
    {
        // line 637
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 666
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 687
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 688
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 740
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 742
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 752
    public function block_footerScript($context, array $blocks = array())
    {
        // line 753
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 754
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1411 => 754,  1406 => 753,  1403 => 752,  1392 => 742,  1387 => 740,  1347 => 703,  1339 => 698,  1335 => 697,  1331 => 696,  1320 => 688,  1317 => 687,  1313 => 685,  1304 => 679,  1300 => 678,  1296 => 677,  1292 => 676,  1288 => 675,  1277 => 667,  1273 => 666,  1264 => 660,  1260 => 659,  1256 => 658,  1252 => 657,  1248 => 656,  1227 => 637,  1224 => 636,  1216 => 521,  1213 => 520,  1210 => 519,  1202 => 525,  1200 => 519,  1193 => 514,  1186 => 512,  1170 => 510,  1167 => 509,  1164 => 508,  1150 => 506,  1147 => 505,  1144 => 504,  1142 => 503,  1138 => 502,  1135 => 501,  1131 => 500,  1121 => 493,  1117 => 491,  1114 => 490,  1098 => 620,  1092 => 618,  1086 => 616,  1084 => 615,  1076 => 609,  1070 => 607,  1064 => 605,  1062 => 604,  1054 => 598,  1048 => 596,  1042 => 594,  1040 => 593,  1031 => 586,  1025 => 584,  1019 => 582,  1017 => 581,  1010 => 576,  1004 => 574,  998 => 572,  996 => 571,  988 => 565,  982 => 563,  976 => 561,  974 => 560,  942 => 530,  940 => 490,  925 => 478,  914 => 470,  903 => 462,  892 => 454,  881 => 446,  871 => 439,  853 => 424,  843 => 417,  833 => 410,  824 => 403,  821 => 402,  813 => 318,  810 => 317,  805 => 315,  802 => 314,  797 => 311,  794 => 310,  775 => 294,  653 => 174,  647 => 170,  645 => 169,  641 => 167,  638 => 166,  633 => 163,  630 => 162,  626 => 127,  623 => 126,  617 => 109,  613 => 107,  611 => 106,  608 => 105,  606 => 104,  603 => 103,  601 => 102,  598 => 101,  596 => 100,  593 => 99,  591 => 98,  588 => 97,  586 => 96,  583 => 95,  581 => 94,  578 => 93,  576 => 92,  573 => 91,  571 => 90,  568 => 89,  566 => 88,  563 => 87,  561 => 86,  558 => 85,  556 => 84,  553 => 83,  551 => 82,  548 => 81,  546 => 80,  543 => 79,  541 => 78,  538 => 77,  536 => 76,  533 => 75,  531 => 74,  528 => 73,  526 => 72,  523 => 71,  521 => 70,  518 => 69,  516 => 68,  513 => 67,  511 => 66,  508 => 65,  506 => 64,  503 => 63,  501 => 62,  498 => 61,  496 => 60,  493 => 59,  491 => 58,  488 => 57,  486 => 56,  483 => 55,  481 => 54,  478 => 53,  476 => 52,  473 => 51,  471 => 50,  468 => 49,  466 => 48,  463 => 47,  461 => 46,  458 => 45,  456 => 44,  453 => 43,  451 => 42,  448 => 41,  446 => 40,  443 => 39,  441 => 38,  438 => 37,  436 => 36,  433 => 35,  431 => 34,  428 => 33,  426 => 32,  423 => 31,  421 => 30,  418 => 29,  416 => 28,  413 => 27,  411 => 26,  408 => 25,  406 => 24,  403 => 23,  401 => 22,  398 => 21,  396 => 20,  393 => 19,  391 => 18,  388 => 17,  386 => 16,  383 => 15,  381 => 14,  378 => 13,  376 => 12,  373 => 11,  368 => 763,  359 => 756,  357 => 752,  354 => 749,  352 => 636,  347 => 633,  345 => 402,  336 => 395,  322 => 392,  319 => 391,  317 => 390,  311 => 387,  307 => 385,  305 => 384,  297 => 379,  289 => 374,  284 => 372,  278 => 369,  273 => 367,  269 => 366,  260 => 359,  244 => 354,  240 => 353,  236 => 352,  232 => 350,  230 => 349,  224 => 346,  218 => 345,  214 => 343,  212 => 342,  201 => 336,  194 => 331,  192 => 330,  190 => 329,  188 => 328,  186 => 327,  182 => 325,  180 => 324,  177 => 323,  175 => 317,  173 => 314,  170 => 313,  168 => 310,  165 => 309,  163 => 166,  160 => 165,  158 => 162,  153 => 160,  149 => 159,  140 => 153,  135 => 151,  131 => 150,  126 => 148,  122 => 147,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

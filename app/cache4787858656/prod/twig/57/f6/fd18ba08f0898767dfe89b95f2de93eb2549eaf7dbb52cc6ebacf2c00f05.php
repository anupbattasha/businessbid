<?php

/* BBidsBBidsHomeBundle:Home:order.html.twig */
class __TwigTemplate_57f6fd18ba08f0898767dfe89b95f2de93eb2549eaf7dbb52cc6ebacf2c00f05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:order.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">
  <form action=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_invoice_reciept_pdf", array("id" => (isset($context["ordernumber"]) ? $context["ordernumber"] : null))), "html", null, true);
        echo "\">
    <button >Print/Save Receipt</button>
  </form>
<div id=\"data\" >

<div class=\"page-title\"><div class=\"col-sm-8\"><h1></h1></div></div>
<div>
\t";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 13
            echo "
\t<div class=\"alert alert-danger\">";
            // line 14
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "
\t";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
\t<div class=\"alert alert-success\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
\t";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "
\t<div class=\"alert alert-success\">";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "</div>

<div class=\"row\">
";
        // line 64
        echo "
";
        // line 66
        echo "
<table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"printTable\" class=\"print-table\">
  <tr>
    <td height=\"204\" align=\"center\" valign=\"top\" style=\" padding:50px 50px 0;\">
      <table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"print-table\">
        <tr>
          <td width=\"459\" height=\"8

          2\" style=\"padding:0px\"><a href=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"http://115.124.120.36/img/logo.jpg\" ></a></td>
          <td width=\"292\" align=\"left\" valign=\"middle\"  style=\"font-family:Arial, Helvetica, sans-serif; font-size:40px;line-height:36px;padding:3px 0px 3px 10px; margin-left:8px; font-weight:bolder;\">INVOICE</td>
        </tr>
      </table>
      <table width=\"900\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"print-table\">
        <tr>
          <td width=\"465\" height=\"80\" align=\"left\" valign=\"top\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:30px;padding:10px 0px 20px 1px; margin-left:8px; \">
              <p>BusinessBid DMCC<br />
              503, Indigo Icon Tower<br />
              Cluster F, JLT-Dubai, UAE<br />
              (04) 4213777<br />
            <u>www.businessbid.ae</u></p>
          </td>
          <td width=\"292\" align=\"left\" valign=\"top\"  style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:30px;padding:10px 10px 3px 10px; margin-left:8px; \">\t\t\t          <p>Invoice Date: ";
        // line 87
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["paydate"]) ? $context["paydate"] : null), "d-m-Y"), "html", null, true);
        echo "<br>
             Invoice #: ";
        // line 88
        echo twig_escape_filter($this->env, (isset($context["ordernumber"]) ? $context["ordernumber"] : null), "html", null, true);
        echo "<br>
             Payment Type: ";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["paytype"]) ? $context["paytype"] : null), "html", null, true);
        echo "</p></td>
        </tr>
        <tr>
          <td height=\"70\" align=\"left\" valign=\"top\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:30px;padding:3px 10px 20px 1px; margin-left:8px; \">SOLD TO<br />
            ";
        // line 93
        echo twig_escape_filter($this->env, (isset($context["bizname"]) ? $context["bizname"] : null), "html", null, true);
        echo "<br />
            ";
        // line 94
        echo twig_escape_filter($this->env, (isset($context["address"]) ? $context["address"] : null), "html", null, true);
        echo ", U.A.E<br />
            ";
        // line 95
        echo twig_escape_filter($this->env, (isset($context["smsphone"]) ? $context["smsphone"] : null), "html", null, true);
        echo "<br />
          ";
        // line 96
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : null), "html", null, true);
        echo "</td>
          <td align=\"left\" valign=\"top\"  style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:30px;padding:3px 10px 3px 10px; margin-left:8px; \">&nbsp;</td>
        </tr>
      </table>
      <table width=\"900\" border=\"1\" cellspacing=\"0\" cellpadding=\"15\">
        <tr>
          <td height=\"72\" colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"#3670b7\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:35px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; color:#fff;\">CHARGE SUMMARY</td>
        </tr>
        <tr>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\"  style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Lead Package</td>
          <td width=\"304\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Number of Leads</td>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Service Category</td>
        </tr>
        ";
        // line 109
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 110
            echo "        <tr>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 111
            if (($this->getAttribute($context["c"], "leadpack", array()) == 10)) {
                echo "Bronze";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 25)) {
                echo "Silver ";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 50)) {
                echo " Gold ";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 100)) {
                echo " Platinum ";
            }
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "leadpack", array()), "html", null, true);
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
            echo "</td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "      </table><br/><br/>
      <table width=\"900\" border=\"1\" cellspacing=\"0\" cellpadding=\"15\">
        <tr>
          <td height=\"72\" colspan=\"5\" align=\"center\" valign=\"middle\" bgcolor=\"#3670b7\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:35px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; color:#fff;\">TRANSACTIONS</td>
        </tr>
        <tr>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Date</td>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Description</td>
          <td width=\"298\" align=\"center\" valign=\"middle\" bgcolor=\"#e1e1e1\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Amount (AED)</td>
        </tr>
        ";
        // line 126
        $context["grandtotal"] = 0;
        // line 127
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order"]) ? $context["order"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            // line 128
            echo "        <tr>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 129
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["o"], "created", array()), "d-m-Y"), "html", null, true);
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 130
            if (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 10)) {
                echo "Bronze";
            } elseif (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 25)) {
                echo "Silver ";
            } elseif (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 50)) {
                echo " Gold ";
            } elseif (($this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "leadpack", array()) == 100)) {
                echo " Platinum ";
            }
            echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "amount", array()), "html", null, true);
            echo "</td>
          ";
            // line 132
            $context["grandtotal"] = ((isset($context["grandtotal"]) ? $context["grandtotal"] : null) + $this->getAttribute($context["o"], "amount", array()));
            // line 133
            echo "        </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "        <tr>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
        // line 136
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["paydate"]) ? $context["paydate"] : null), "d-m-Y"), "html", null, true);
        echo "</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:18px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">Grand Total</td>
          <td align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-weight:600; font-size:20px;line-height:37px;padding:3px 10px 3px 10px; margin-left:8px; \">";
        // line 138
        echo twig_escape_filter($this->env, (isset($context["grandtotal"]) ? $context["grandtotal"] : null), "html", null, true);
        echo "</td>
        </tr>
      </table>    </td>
    </tr>
    <tr>
      <td height=\"40\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:0px 0px 9px 50px; margin-left:8px; font-style:italic; \"><p>Please note that this is an electronically generated invoice</p></td>
    </tr>
    <tr>
      <td height=\"32\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:18px;line-height:37px;padding:20px 10px 3px 50px;  margin-left:8px; margin-top:10px; \">Thank you for your business.</td>
    </tr>
    <tr><td height=\"51\" style=\"padding:20px 10px 3px 50px;\"><img  width=\"330\" src=\"http://115.124.120.36/img/business-bid-seal.jpg\" ></td></tr>
    <tr>
      <td style=\"font-family:Arial, Helvetica, sans-serif; font-size:20px;line-height:7px; padding:30px 0px 0px; margin-top:10px; \"><hr /></td>
    </tr>
    <tr>
      <td height=\"33\" align=\"center\" valign=\"middle\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:14px;line-height:37px;padding:3px 10px 30px 10px; margin-left:8px; \">BusinessBid DMCC is registered and licensed as a Freezone Company under the Rules & Regulations of DMCC.</td>
    </tr>
  </table>
";
        // line 157
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 157,  287 => 138,  282 => 136,  279 => 135,  264 => 133,  262 => 132,  258 => 131,  246 => 130,  242 => 129,  239 => 128,  221 => 127,  219 => 126,  207 => 116,  198 => 113,  194 => 112,  182 => 111,  179 => 110,  175 => 109,  159 => 96,  155 => 95,  151 => 94,  147 => 93,  140 => 89,  136 => 88,  132 => 87,  116 => 74,  106 => 66,  103 => 64,  98 => 29,  89 => 26,  86 => 25,  82 => 24,  79 => 23,  70 => 20,  67 => 19,  63 => 18,  60 => 17,  51 => 14,  48 => 13,  44 => 12,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:vendor_on_off.html.twig */
class __TwigTemplate_7393d8ed2c0beedbd4a896ee899c69a1ca743747b8ecc901ecb648c4a0f11883 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:vendor_on_off.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/dataTables.tableTools.css"), "html", null, true);
        echo "\">
";
        // line 6
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/dataTables.tableTools.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"//cdn.datatables.net/plug-ins/1.10.7/api/fnReloadAjax.js\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>


<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Vendor Activation Service</h1></div>

<div>
    ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 20
            echo "    <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 23
            echo "    <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "    <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "<div class=\"admin_filter report-filter\">
    <div class=\"col-md-3 \" id=\"companyNameFilter\"></div>
    <div class=\"col-md-3\" id=\"vendorFilter\"></div>
    <div class=\"col-md-3\" id=\"categoryFilter\"></div>
    <strong>Note</strong>: Select category in order to deactivate vendor(s) in respective category. If not selected then vendor(s) will be deactivated in all categories.
    <div class=\"col-md-5 clear-right action-links\">
    <span class=\"reset form_submit\" onclick=\"toggleVendors(1);\">Activate</span>
    <span class=\"reset form_submit\" onclick=\"toggleVendors(2);\">Deactivate</span>
    <span class=\"reset\"><a href=\"#\" class=\"form_submit\" onclick=\"fnResetFilters();\">Reset Filter</a></span>
    </div>
</div>

<div class=\"latest-orders\">
<table id=\"example\">
    <thead>
        <tr>
            <th>Company Name</th>
            <th>Vendor Name</th>
            <th>Contact Number</th>
            <th>Category</th>
            <th>User</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan=\"8\" class=\"dataTables_empty\">Loading data from server... Please wait</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th>Company Name</th>
            <th>Vendor Name</th>
            <th>Contact Number</th>
            <th>Category</th>
            <th>User</th>
            <th>Status</th>
        </tr>
    </tfoot>
</table>
</div>
</div>

</div>
<script type=\"text/javascript\" charset=\"utf-8\">
    \$(document).ready(function() {
        \$(\"#example\").dataTable({sDom:'T<\"clear\">lfrtip',oTableTools:{sRowSelect:\"multi\",aButtons:[]},sPaginationType:\"full_numbers\",aaSortingFixed:[[4,\"asc\"]],aLengthMenu:[[5,10,25,50,-1],[5,10,25,50,\"All\"]],bProcessing:!0,bServerSide:!0,oLanguage:{sEmptyTable:\"\",sInfoEmpty:\"\",sProcessing:\"Processing please wait\",sInfoFiltered:\"\",oPaginate:{sFirst:\"First\",sPrevious:\"<<\",sNext:\">>\",sLast:\"Last\"},sZeroRecords:\"ZeroRecords\",sSearch:\"Search\",sLoadingRecords:\"LoadingRecords\"},aoColumnDefs:[{aTargets:[4],bVisible:!1,bSearchable:!1}],sEmptyTable:\"There are no records\",sAjaxSource:\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/vendor_on_off.php"), "html", null, true);
        echo "\",fnServerParams:function(){}}).columnFilter({aoColumns:[{sSelector:\"#companyNameFilter\"},{sSelector:\"#vendorFilter\"},null,{sSelector:\"#categoryFilter\",type:\"select\",values: ";
        echo twig_jsonencode_filter((isset($context["catArray"]) ? $context["catArray"] : null));
        echo " },null,null]}),\$(\"#example tbody\").on(\"click\",\"tr\",function(){\$(this).toggleClass(\"selected\")});
    });

    function toggleVendors(modetype) {
        var selected = [], table = \$('#example').dataTable(), oTT = TableTools.fnGetInstance( 'example' ), aData = oTT.fnGetSelectedData();
        \$.each( aData, function( key, value ) {
          // console.log( key + \": \" + value['4'] );
            var index = \$.inArray(value['4'], selected);
            if ( index === -1 ) {
                selected.push( value['4'] );
            }
        });
        var dataString = JSON.stringify(selected), category = \$('select.select_filter').val();
        \$.ajax({url:\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_on_off_ajax");
        echo "\",type:\"POST\",data:{data:dataString,modetype:modetype,category:category},success:function(){alert(\"Update succesfull..\");table.fnReloadAjax()},error:function(a){console.log(a.message)}});
    }

    function fnResetFilters() {
        var table=\$(\"#example\").dataTable();table.fnFilterClear(),\$(\"input\").val(\"\"),\$(\"div#statusFilter\").find(\"select option:eq(0)\").prop(\"selected\",!0);
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:vendor_on_off.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 87,  154 => 74,  106 => 28,  97 => 26,  92 => 25,  83 => 23,  78 => 22,  69 => 20,  65 => 19,  54 => 11,  50 => 10,  46 => 9,  41 => 7,  36 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:vendor_search_home.html.twig */
class __TwigTemplate_423aee3f6a1ba87e4fa9279ba08455ee511f5f2d205eb3c572792a29e63058cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:vendor_search_home.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Categories Home";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 13
            echo "
        <div class=\"alert alert-danger\">";
            // line 14
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
        <div class=\"alert alert-success\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 24
            echo "
        <div class=\"alert alert-success\">";
            // line 25
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "    </div>

</div>
<!-- DROPDOWN AREA
    ================================================== -->
        <div class=\"six_area\">
            <div class=\"six_child_area\">
                <div class=\"inner_six_area\">
                    <h2>SELECT A SERVICE CATEGORY</h2>
                    <div class=\"six_tabil\">
                        <select name=\"categories\" id=\"category\">
                            <option value=\"\">Select a category</option>
                        ";
        // line 40
        if ( !twig_test_empty((isset($context["categories"]) ? $context["categories"] : null))) {
            // line 41
            echo "                            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 42
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                        ";
        }
        // line 45
        echo "                        </select>
                    </div>
                    <div class=\"six_btn\"><a href=\"#\" id=\"get_quotes\">START FREE QUOTES</a></div>
                </div>
            </div>
        </div>

    <!-- SERVICE CATEGORIES AREA
    ================================================== -->
    <div class=\"service_cat_area\">
        <div class=\"inner_service_cat_area\">
            <h1>OR</h1>
            <h2>CLICK ON A SERVICE CATEGORY</h2>
            <ul>
                <li><a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
        echo "\"><div class=\"serv_one\"></div></a><p>Accounting & Auditing</p></li>
                <li><a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
        echo "\"><div class=\"serv_two\"></div></a><p>Catering Services</p></li>
                <li><a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
        echo "\"><div class=\"serv_three\"></div></a><p>Interior Design & Fit Out Contractors</p></li>
                <li><a href=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
        echo "\"><div class=\"serv_four\"></div></a><p>IT Support</p></li>
                <li><a href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
        echo "\"><div class=\"serv_five\"></div></a><p>Signage and Signboard</p></li>
                <li><a href=\"";
        // line 64
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
        echo "\"><div class=\"serv_six\"></div></a><p>Landscaping & Gardening</p></li>
                <li><a href=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 994));
        echo "\"><div class=\"serv_seven\"></div></a><p>Maintenance</p></li>
                <li><a href=\"";
        // line 66
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 79));
        echo "\"><div class=\"serv_eight\"></div></a><p>Business Cards & Printing Press</p></li>
                <li class=\"serv_blanck\"></li>
                <li><a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 995));
        echo "\"><div class=\"serv_nine\"></div></a><p>Commercial Cleaning</p></li>
                <li><a href=\"";
        // line 69
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 114));
        echo "\"><div class=\"serv_ten\"></div></a><p>Photography</p></li>
                <li><a href=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 112));
        echo "\"><div class=\"serv_eleven\"></div></a><p>Pest Control</p></li>
            </ul>
        </div>
    </div>
<script type=\"text/javascript\">
\$(document).ready(function(){
    \$('select[name=\"categories\"]').bind('change',function () {
        var cat = \$(this).val(), urlPath = \"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => "cid"));
        echo "\";
        urlPath = urlPath.replace(\"cid\", cat);
        \$(\"#get_quotes\").attr('href', urlPath);
    });
});
</script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-62204441-1', 'auto');ga('send', 'pageview');
</script>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:vendor_search_home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 77,  196 => 70,  192 => 69,  188 => 68,  183 => 66,  179 => 65,  175 => 64,  171 => 63,  167 => 62,  163 => 61,  159 => 60,  155 => 59,  139 => 45,  136 => 44,  125 => 42,  120 => 41,  118 => 40,  104 => 28,  95 => 25,  92 => 24,  87 => 23,  78 => 20,  75 => 19,  71 => 18,  68 => 17,  59 => 14,  56 => 13,  52 => 12,  47 => 9,  44 => 8,  39 => 5,  36 => 4,  30 => 2,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:categorylist.html.twig */
class __TwigTemplate_cf7f3d767f9938c0eb26c61e0bd28d865bfd6e31d944994c7bc872323b2c5870 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:categorylist.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">

 function deletefunc(catid, catname){
\tvar con = confirm(\"Are you sure to delete  \"+catname+ \"? The record once deleted cannot be retrived back!\");
\tif(con){
\t\tvar url = \"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_delete_category", array("categoryid" => "categoryID"));
        echo "\";
\t\tvar urlset = url.replace('categoryID', catid);
\t\t\twindow.open(urlset,\"_self\");
\t\treturn true;
\t}else{
\t\treturn false;
\t}
 }
 
 function statusfunc(categoryID,catname,status){
 
\t\t\t
\t\t\tvar redirUrl = \"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_enable_category", array("catid" => "categoryID", "stat" => "status")), "html", null, true);
        echo "\";
\t\t\tredirUrl = redirUrl.replace('categoryID', categoryID);
\t\t\tredirUrl = redirUrl.replace('status', status);
\t\t\twindow.open(redirUrl,\"_self\");
\t\t 
 }
</script>
<div class=\"inner_container container\">
<div class=\"\"><h3>Category List <a class=\"btn btn-success pull-right\"  href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_add_category");
        echo "\">Add Category</a></h3> </div>
\t<div class=\"segment-content row\">
\t";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 32
            echo "
\t<div class=\"alert alert-danger\">";
            // line 33
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "\t
\t";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 38
            echo "
\t<div class=\"alert alert-success\">";
            // line 39
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 43
            echo "
\t<div class=\"alert alert-success\">";
            // line 44
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "\t<div class=\"latest-orders\">
\t<table>
        <thead>
         <tr>
            <th>Category Name</th>
            <th>Description</th>
            <th>Price (AED)</th>
            <th>Sub Links</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
         
\t";
        // line 60
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 61
            echo "\t <tr>
\t\t
\t\t<td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "description", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "price", array()), "html", null, true);
            echo "</td>
\t\t<td><a href=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_subcategories", array("categoryid" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\" >Subcategory</a> | <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_keywords", array("categoryid" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\" >Keyword</a></td>
\t\t<td>
\t\t
\t
\t\t
\t\t<a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_edit_category", array("categoryid" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\"> Edit</a>
\t\t<a href=\"#\" onclick=\"deletefunc('";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "')\" class=\"btn btn-danger\"> Delete</a>
\t\t";
            // line 73
            if (($this->getAttribute($context["category"], "status", array()) == 1)) {
                // line 74
                echo "\t\t<a href=\"#\" onclick=\"statusfunc('";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "', '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
                echo "','0')\" class=\"btn btn-danger\">Disable</a>
\t\t";
            } else {
                // line 76
                echo "\t\t<a href=\"#\" onclick=\"statusfunc('";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "', '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
                echo "','1')\" class=\"btn btn-warning\">Enable</a>
\t\t";
            }
            // line 78
            echo "\t\t</td>
\t</tr>\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "\t</tbody>
\t</table>
\t</div>
</div>

</div> 
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:categorylist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 81,  194 => 78,  186 => 76,  178 => 74,  176 => 73,  170 => 72,  166 => 71,  156 => 66,  152 => 65,  148 => 64,  144 => 63,  140 => 61,  136 => 60,  121 => 47,  112 => 44,  109 => 43,  104 => 42,  95 => 39,  92 => 38,  88 => 37,  85 => 36,  76 => 33,  73 => 32,  69 => 31,  64 => 29,  53 => 21,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }
}

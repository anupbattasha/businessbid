<?php

/* BBidsBBidsHomeBundle:User:leads_paymentform.html.twig */
class __TwigTemplate_f97b9b481553203424660acf761606a77bb63f92382b54d16c1576047640312c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script>
<script>
\$('.bbspop').popover();
\$('.bbspop').popover({ trigger: \"hover\" });
</script>
</script>
\t<style type=\"text/css\">
\t.camouflage{
\t\tdisplay: none;
\t}
\t.exposed{
\t\tdisplay: block;
\t}
\t.errorinput{ content: attr(title); color: red; margin-left: 0.6rem; }
\t</style>
<div class=\"container\">
<div>
    ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 22
            echo "
    <div class=\"alert alert-danger\">";
            // line 23
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
    ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 28
            echo "
    <div class=\"alert alert-success\">";
            // line 29
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "
    ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 34
            echo "
    <div class=\"alert alert-success\">";
            // line 35
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "</div>

<div class=\"col-sm-12\">
\t<div class=\"package-details\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" id=\"selectpackagedetails\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Package Details </td>
\t\t\t\t\t<td width=\"197\">Price</td>
\t\t\t\t\t<td width=\"189\">Discount</td>
\t\t\t\t\t<td width=\"189\">Total</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 52
        $context["OTC"] = 0;
        // line 53
        echo "\t\t\t\t";
        $context["grandTotal"] = 0;
        // line 54
        echo "\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["purchasingLeads"]) ? $context["purchasingLeads"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lead"]) {
            // line 55
            echo "\t\t\t\t\t";
            $context["priceArray"] = twig_split_filter($this->env, $this->getAttribute($context["lead"], "price", array()), " - ");
            // line 56
            echo "
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "plan", array()), "html", null, true);
            echo " Package: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "category", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t";
            // line 59
            $context["multiplier"] = $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
            // line 60
            echo "\t\t\t\t\t<td width=\"197\">AED ";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">
\t\t\t\t\t\t";
            // line 62
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                // line 63
                echo "\t\t\t\t\t\t\t";
                $context["selPlan"] = $this->getAttribute((isset($context["leadsPack"]) ? $context["leadsPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
                // line 64
                echo "\t\t\t\t\t\t\tAED ";
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")) - ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array"))), "html", null, true);
                echo "
\t\t\t\t\t\t";
            } else {
                // line 66
                echo "\t\t\t\t\t\t\tAED 0
\t\t\t\t\t\t";
            }
            // line 68
            echo "\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 69
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")));
            } else {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)));
            }
            echo "</td>

\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lead'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "\t\t\t\t";
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            // line 74
            echo "\t\t\t\t\t";
            $context["OTC"] = (isset($context["registerFee"]) ? $context["registerFee"] : null);
            // line 75
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Account Setup Fee <span><a tabindex=\"0\" class=\"popover-icon\" role=\"button\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"bottom\" data-trigger=\"hover\" title=\"The Account Setup fee is one time charge and includes:\" data-content=\"<ul><li>Creation of dedicated webpage with a full business profie.</li> <li>Access to 24/7 online dashboard with reporting features.</li> <li>Compilation and publication of 5 recent customer reviews.</li></ul>\"></a></span>
\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"197\">AED ";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">AED 0</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 80
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t</tbody>
\t\t\t\t";
        }
        // line 84
        echo "\t\t\t\t<tfoot>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Grand Total</td>
\t\t\t\t\t<td width=\"197\"></td>
\t\t\t\t\t<td width=\"189\"></td>
\t\t\t\t\t<td width=\"189\"><strong>AED ";
        // line 89
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</strong></td>
\t\t\t\t</tr>
\t\t\t\t</tfoot>
\t\t</table>
\t</div>
\t<div class=\"paymentmethod\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\">Payment Method</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\"><strong>How would you like to pay <span class=\"payment_value\">";
        // line 103
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</span> AED ?</td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t</table>
\t\t<div class=\"credit-card-option col-sm-12 position-in-opt left-clear\" style=\"width:100%; float:left;\">
\t\t\t<div class=\"col-sm-12 position-in-opt\"><input type=\"radio\" name=\"payType\" checked=\"checked\" value=\"cc\" />Credit Card</div>

\t\t\t<div class=\"col-sm-9\">
\t\t\t\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>
        <script src=\"https://sandbox.checkout.com/cdn/js/checkout.js\"></script>

            <form class=\"payment-form\">
                <script>
                  var publickey = 'pk_test_519b2513-80ad-4f1f-8524-1d583f5ccbbd'; //PublicKey provided by Checkout

                  Checkout.render({
                      debugMode: true,
                      namespace: 'CheckoutIntegration',
                      publicKey: publickey,
                      paymentToken: \"";
        // line 122
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\",
                      customerEmail: '";
        // line 123
        echo twig_escape_filter($this->env, (isset($context["usermail"]) ? $context["usermail"] : null), "html", null, true);
        echo "',
                      customerName: '";
        // line 124
        echo twig_escape_filter($this->env, (isset($context["custome"]) ? $context["custome"] : null), "html", null, true);
        echo "',
                      paymentMode: 'card',
                      value: ";
        // line 126
        echo twig_escape_filter($this->env, (isset($context["amounttotal"]) ? $context["amounttotal"] : null), "html", null, true);
        echo ",
                      currency: 'AED',
                      forceMobileRedirect: true,
                      payButtonSelector: '#payButtonId',
                      widgetContainerSelector: '.payment-form',
                      useCurrencyCode: false,
                      
                       styling: {
                            widgetColor: 'rgba(204, 204, 204, 0.16)',
                            themeColor: '',
                            buttonColor: '',
                            buttonLabelColor: '',
                            logoUrl: '',
                            formButtonColor: '',
                            formButtonLabelColor: '',
                            iconColor: '',
                            overlayShade: 'dark',
                            overlayOpacity: 0.8,
                            buttonLabel: 'Visa Checkout'
                        },

                 
                      cardCharged: function (event) {
\t\t\t\t\t  console.log(event);
\t\t\t\t\t  alert(\"werwrwr\");
                          document.getElementById('cko-cc-paymenToken').value = event.data.paymentToken;
                          document.getElementById('payment-tokenform').submit();
                      }

                  });
                </script>

            </form>
\t\t\t<form name=\"payment-tokenform\" method=\"post\" id=\"payment-tokenform\" action=\"verify.php\">
                <input type=\"hidden\" id =\"cko-cc-paymenToken\" name =\"cko-cc-paymenToken\" />
            </form>
\t\t\t</div>
\t\t\t<div class=\"col-sm-3\"><img src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/payment-icon.jpg"), "html", null, true);
        echo "\" /></div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"check pickup\" />Cheque Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"checkAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 173
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"checkFillForm\">
\t\t\t\t";
        // line 179
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_start', array("attr" => array("id" => "checkForm")));
        echo "
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\"><label>Company Name:</label></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 184
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 195
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 196
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 199
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 203
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 207
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 208
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 211
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 212
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr class=\"camouflage\">
\t\t\t\t\t\t\t<td width=\"20%\"></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 216
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "Submit", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t\t";
        // line 220
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_end');
        echo "
\t\t\t</div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"cash pickup\" />Cash Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"cashAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 231
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"cashFillForm\">
                ";
        // line 237
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_start', array("attr" => array("id" => "cashForm")));
        echo "
                <table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
                    <tbody>
                        <tr>
                            <td width=\"20%\"><label>Company Name:</label></td>
                            <td width=\"80%\">";
        // line 242
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 245
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
                            <td width=\"80%\">";
        // line 246
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 249
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
                            <td width=\"80%\">";
        // line 250
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 253
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 254
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 257
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 258
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 261
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 262
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr class=\"camouflage\">
                            <td width=\"20%\"></td>
                            <td width=\"80%\">";
        // line 266
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "Submit", array()), 'widget');
        echo "</td>
                        </tr>
                    </tbody>
                </table>
                ";
        // line 270
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_end');
        echo "
            </div>
\t\t</div>

\t\t";
        // line 289
        echo "\t</div>
</div>
\t<div class=\"row text-right update-account\">
\t\t<a href=\"#\" class=\"btn btn-success\"> Place Order </a>
\t</div>
</div>
<script type=\"text/javascript\" src=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/leads.paymentform.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  531 => 295,  523 => 289,  516 => 270,  509 => 266,  502 => 262,  498 => 261,  492 => 258,  488 => 257,  482 => 254,  478 => 253,  472 => 250,  468 => 249,  462 => 246,  458 => 245,  452 => 242,  444 => 237,  435 => 231,  421 => 220,  414 => 216,  407 => 212,  403 => 211,  397 => 208,  393 => 207,  387 => 204,  383 => 203,  377 => 200,  373 => 199,  367 => 196,  363 => 195,  357 => 192,  353 => 191,  347 => 188,  343 => 187,  337 => 184,  329 => 179,  320 => 173,  307 => 163,  267 => 126,  262 => 124,  258 => 123,  254 => 122,  232 => 103,  215 => 89,  208 => 84,  201 => 80,  196 => 78,  191 => 75,  188 => 74,  185 => 73,  169 => 69,  166 => 68,  162 => 66,  156 => 64,  153 => 63,  151 => 62,  145 => 60,  143 => 59,  137 => 58,  133 => 56,  130 => 55,  125 => 54,  122 => 53,  120 => 52,  104 => 38,  95 => 35,  92 => 34,  88 => 33,  85 => 32,  76 => 29,  73 => 28,  69 => 27,  66 => 26,  57 => 23,  54 => 22,  50 => 21,  31 => 4,  28 => 3,  11 => 1,);
    }
}

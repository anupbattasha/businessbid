<?php

/* BBidsBBidsHomeBundle:Admin:home.html.twig */
class __TwigTemplate_6ddac3a099e0422c7e9deecb7dfe1ef9567a70407ab7d4ac26050bb4f9a001c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:home.html.twig", 1);
        $this->blocks = array(
            'chartscript' => array($this, 'block_chartscript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_chartscript($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">
    ";
        // line 5
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart"]) ? $context["chart"] : null));
        echo "
</script>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}

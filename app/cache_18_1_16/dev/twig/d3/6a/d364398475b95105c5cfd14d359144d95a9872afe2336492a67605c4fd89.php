<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">


    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 162
        $this->displayBlock('customcss', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('javascripts', $context, $blocks);
        // line 309
        echo "
";
        // line 310
        $this->displayBlock('customjs', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('jquery', $context, $blocks);
        // line 317
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 323
        echo "</head>
";
        // line 324
        ob_start();
        // line 325
        echo "<body>

";
        // line 327
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "uid"), "method");
        // line 328
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "pid"), "method");
        // line 329
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "email"), "method");
        // line 330
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "name"), "method");
        // line 331
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 342
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 343
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 345
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 346
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 349
(isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 350
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 352
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 353
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 359
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 366
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 369
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 372
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 374
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 379
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 384
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 385
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 387
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 390
(isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 391
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 395
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 402
        $this->displayBlock('maincontent', $context, $blocks);
        // line 633
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 636
        $this->displayBlock('footer', $context, $blocks);
        // line 749
        echo "    <!-- footer block ends -->
</div>";
        // line 752
        $this->displayBlock('footerScript', $context, $blocks);
        // line 756
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 763
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_bd49b7b64350db2ec75d1d42f558459798845aeeafea456b7944924c7cb584b5 = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_8e0ea88571ac1f232410734f20af1e79611697d310b27ec68fcdd9bd06a489e4 = "http://www.businessbid.ae/contactus") && ('' === $__internal_8e0ea88571ac1f232410734f20af1e79611697d310b27ec68fcdd9bd06a489e4 || 0 === strpos($__internal_bd49b7b64350db2ec75d1d42f558459798845aeeafea456b7944924c7cb584b5, $__internal_8e0ea88571ac1f232410734f20af1e79611697d310b27ec68fcdd9bd06a489e4)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_239f26591d6ed5a38a24f653f15cb5fc6371d60ef33e9163419d43052c66214e = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_b767f7ad703d04e9ced0d77bc9d6226ede053be8853f93335b8cb685128bf168 = "http://www.businessbid.ae/node3") && ('' === $__internal_b767f7ad703d04e9ced0d77bc9d6226ede053be8853f93335b8cb685128bf168 || 0 === strpos($__internal_239f26591d6ed5a38a24f653f15cb5fc6371d60ef33e9163419d43052c66214e, $__internal_b767f7ad703d04e9ced0d77bc9d6226ede053be8853f93335b8cb685128bf168)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_8d214754183eed40650c71b394d50c9d0d3adc78fa86884e8eca4fd4efcaa0d8 = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_eee97b372d64cf01418fe994d29b11a94e20d3d0e42686e5930f7f41584e04d3 = "http://www.businessbid.ae/review/login") && ('' === $__internal_eee97b372d64cf01418fe994d29b11a94e20d3d0e42686e5930f7f41584e04d3 || 0 === strpos($__internal_8d214754183eed40650c71b394d50c9d0d3adc78fa86884e8eca4fd4efcaa0d8, $__internal_eee97b372d64cf01418fe994d29b11a94e20d3d0e42686e5930f7f41584e04d3)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_3c787e0a21494acf2c44d23cd4788864cd71dd069c016a86c91f05e03f2c9756 = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ff594d2508f41b3315afe5cd9ef5abca892faf3f5933284d2b79314c5bc4804c = "http://www.businessbid.ae/node1") && ('' === $__internal_ff594d2508f41b3315afe5cd9ef5abca892faf3f5933284d2b79314c5bc4804c || 0 === strpos($__internal_3c787e0a21494acf2c44d23cd4788864cd71dd069c016a86c91f05e03f2c9756, $__internal_ff594d2508f41b3315afe5cd9ef5abca892faf3f5933284d2b79314c5bc4804c)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_15ecd5970bb3f4cf650d355d970ab6e2b8d400859f770c3931acdf06ad412984 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_0396fcb89b6c5d11348adf9dba8d6186388b8265958a689cdb761a6d0c4ace23 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_0396fcb89b6c5d11348adf9dba8d6186388b8265958a689cdb761a6d0c4ace23 || 0 === strpos($__internal_15ecd5970bb3f4cf650d355d970ab6e2b8d400859f770c3931acdf06ad412984, $__internal_0396fcb89b6c5d11348adf9dba8d6186388b8265958a689cdb761a6d0c4ace23)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_aca27d51230b151204ad54c14f4fcde7890befd7f397f8d370481fec0434064c = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_6ed3c8a946f20e0fc0f31dd81097d3ef0650d0e2d0ee614b50aa0a8f8492ba40 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_6ed3c8a946f20e0fc0f31dd81097d3ef0650d0e2d0ee614b50aa0a8f8492ba40 || 0 === strpos($__internal_aca27d51230b151204ad54c14f4fcde7890befd7f397f8d370481fec0434064c, $__internal_6ed3c8a946f20e0fc0f31dd81097d3ef0650d0e2d0ee614b50aa0a8f8492ba40)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_1a673b934f978ec81535a2acd4aa8185e66fe5d30a1b203c1ed48fcbb0ba11b6 = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_81b0b7efe07d914d5244594885f46b52c3307de3ed6e18daaede2a56bd22c164 = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_81b0b7efe07d914d5244594885f46b52c3307de3ed6e18daaede2a56bd22c164 || 0 === strpos($__internal_1a673b934f978ec81535a2acd4aa8185e66fe5d30a1b203c1ed48fcbb0ba11b6, $__internal_81b0b7efe07d914d5244594885f46b52c3307de3ed6e18daaede2a56bd22c164)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_a1492a24e5e7a8ba5744887420e1895bd78059f6362145ec6bdf5356b25e89c1 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_4947a14bdbc0683f3cd4b15cf2fb857ac6588126be018a4191182503a7b77cc5 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_4947a14bdbc0683f3cd4b15cf2fb857ac6588126be018a4191182503a7b77cc5 || 0 === strpos($__internal_a1492a24e5e7a8ba5744887420e1895bd78059f6362145ec6bdf5356b25e89c1, $__internal_4947a14bdbc0683f3cd4b15cf2fb857ac6588126be018a4191182503a7b77cc5)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_14b521fc60a23c5786008353b10bd9f52a85ad7378f96953a529f5c435a79806 = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_744e8256e87cb79872443cd173b8876dd967b1ebefadb767e9fc53d5a89c9a0a = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_744e8256e87cb79872443cd173b8876dd967b1ebefadb767e9fc53d5a89c9a0a || 0 === strpos($__internal_14b521fc60a23c5786008353b10bd9f52a85ad7378f96953a529f5c435a79806, $__internal_744e8256e87cb79872443cd173b8876dd967b1ebefadb767e9fc53d5a89c9a0a)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_a3ffe1ecdaec2ac4180deb4d7a1af1799b193b7b63ca2412d58d36b2723959c2 = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_31b43a724c303d604815f40888c7c2cfe7e73eee4824dffdd080e24d6a44ade6 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_31b43a724c303d604815f40888c7c2cfe7e73eee4824dffdd080e24d6a44ade6 || 0 === strpos($__internal_a3ffe1ecdaec2ac4180deb4d7a1af1799b193b7b63ca2412d58d36b2723959c2, $__internal_31b43a724c303d604815f40888c7c2cfe7e73eee4824dffdd080e24d6a44ade6)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_15aea005bc693a7ecf510ceae4d9e8539e305057950cfb68e7b7b04018bf02a8 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_6ce58e44ea3e04577fe81d14461c29048366aa133e7a334cb78e859e47c49592 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_6ce58e44ea3e04577fe81d14461c29048366aa133e7a334cb78e859e47c49592 || 0 === strpos($__internal_15aea005bc693a7ecf510ceae4d9e8539e305057950cfb68e7b7b04018bf02a8, $__internal_6ce58e44ea3e04577fe81d14461c29048366aa133e7a334cb78e859e47c49592)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_73acea83ec12110d124bb20123bb4e46c9d19b92b52b2dd9d134b6a74f6f58c7 = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_0038cf517da3569fe0cb1178b0a4a5f775583c888f9b10cf4793dc81e11f07bb = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_0038cf517da3569fe0cb1178b0a4a5f775583c888f9b10cf4793dc81e11f07bb || 0 === strpos($__internal_73acea83ec12110d124bb20123bb4e46c9d19b92b52b2dd9d134b6a74f6f58c7, $__internal_0038cf517da3569fe0cb1178b0a4a5f775583c888f9b10cf4793dc81e11f07bb)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_dc79899c4cba131813d49be77f8002332f36c4f2ce00f173cd596e870d15214b = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_6044cfcbc3f43d372f35caa5f2b35ec839715d3d8422e68349e1f8d7e35d30af = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_6044cfcbc3f43d372f35caa5f2b35ec839715d3d8422e68349e1f8d7e35d30af || 0 === strpos($__internal_dc79899c4cba131813d49be77f8002332f36c4f2ce00f173cd596e870d15214b, $__internal_6044cfcbc3f43d372f35caa5f2b35ec839715d3d8422e68349e1f8d7e35d30af)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_3a3b0cc17bf0b16c70d7c0af11ebe3584e6ec1df64cad87fa46ef88d84e80cc6 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_23e87291c51c178622f1969003cbd69358a0f670242de5effce8f56b1888dba9 = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_23e87291c51c178622f1969003cbd69358a0f670242de5effce8f56b1888dba9 || 0 === strpos($__internal_3a3b0cc17bf0b16c70d7c0af11ebe3584e6ec1df64cad87fa46ef88d84e80cc6, $__internal_23e87291c51c178622f1969003cbd69358a0f670242de5effce8f56b1888dba9)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_f071186ad9e7f4f5d9db334ef61793ce060b9899fda4d8b93986bda76e17a489 = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_e83c585361c551b08e48422bdcc7d8f36bde5aace353141c5bb39bd276ca5525 = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_e83c585361c551b08e48422bdcc7d8f36bde5aace353141c5bb39bd276ca5525 || 0 === strpos($__internal_f071186ad9e7f4f5d9db334ef61793ce060b9899fda4d8b93986bda76e17a489, $__internal_e83c585361c551b08e48422bdcc7d8f36bde5aace353141c5bb39bd276ca5525)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_2e1c944db0fc12b697991efc02ddd0be7da0fe3fd178c605473368d5fc192437 = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_3e3f5503638c9b39fe87208f26c1a51a3f85be764bb10dd76ba62ea09d0ff450 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_3e3f5503638c9b39fe87208f26c1a51a3f85be764bb10dd76ba62ea09d0ff450 || 0 === strpos($__internal_2e1c944db0fc12b697991efc02ddd0be7da0fe3fd178c605473368d5fc192437, $__internal_3e3f5503638c9b39fe87208f26c1a51a3f85be764bb10dd76ba62ea09d0ff450)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_2d3f1e634b9ed57aed10f5fe059f2080e4d3bb4ce3ab51229cb3466a251adea3 = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_5475d5b8a7be08260207f44c86be9d12df72f490fe277a9e62689ae75242f49a = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_5475d5b8a7be08260207f44c86be9d12df72f490fe277a9e62689ae75242f49a || 0 === strpos($__internal_2d3f1e634b9ed57aed10f5fe059f2080e4d3bb4ce3ab51229cb3466a251adea3, $__internal_5475d5b8a7be08260207f44c86be9d12df72f490fe277a9e62689ae75242f49a)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_1553f49d1102ff18324a597cf11892db826f46ded25407b0cccafd49f942e219 = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ffa603e3bef71554bdda08272b3b65043e75388179d1144468eec0fac08d7109 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_ffa603e3bef71554bdda08272b3b65043e75388179d1144468eec0fac08d7109 || 0 === strpos($__internal_1553f49d1102ff18324a597cf11892db826f46ded25407b0cccafd49f942e219, $__internal_ffa603e3bef71554bdda08272b3b65043e75388179d1144468eec0fac08d7109)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_55a441912c502afc02b4adda6349099b8526c529f2d630ed089fad12173242c7 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_b6faa6c90de46d1ba516fa5f3ed0cdfdbe30a335075eaadcb9cecdb4b4092736 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_b6faa6c90de46d1ba516fa5f3ed0cdfdbe30a335075eaadcb9cecdb4b4092736 || 0 === strpos($__internal_55a441912c502afc02b4adda6349099b8526c529f2d630ed089fad12173242c7, $__internal_b6faa6c90de46d1ba516fa5f3ed0cdfdbe30a335075eaadcb9cecdb4b4092736)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_20b499d3e2e41c22e8a8c30e5259dd4928591f8fa9aa760a7aaf48443ca1f59c = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_cb06408ad74466339bc1b06d71a83cde0f73fabe3ed6b26ad3863ec6114b4672 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_cb06408ad74466339bc1b06d71a83cde0f73fabe3ed6b26ad3863ec6114b4672 || 0 === strpos($__internal_20b499d3e2e41c22e8a8c30e5259dd4928591f8fa9aa760a7aaf48443ca1f59c, $__internal_cb06408ad74466339bc1b06d71a83cde0f73fabe3ed6b26ad3863ec6114b4672)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_84cc1de0ea02398709b08ac688373a0a72f7c3f0da5d10ca1970daeaa3c87f7b = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_9e6459a255ae5f6deb0f3100260e33fc5b5fdba811e33e1db74b915083d54d64 = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_9e6459a255ae5f6deb0f3100260e33fc5b5fdba811e33e1db74b915083d54d64 || 0 === strpos($__internal_84cc1de0ea02398709b08ac688373a0a72f7c3f0da5d10ca1970daeaa3c87f7b, $__internal_9e6459a255ae5f6deb0f3100260e33fc5b5fdba811e33e1db74b915083d54d64)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_ffef7b537f94c1bcf3139fe9dd00bd30f90878ec8b4870858341e016ee1fcfae = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_46eb303c03cbc443e4533c5d61059db5c32bc11bedd3b21a513e08178f72a485 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_46eb303c03cbc443e4533c5d61059db5c32bc11bedd3b21a513e08178f72a485 || 0 === strpos($__internal_ffef7b537f94c1bcf3139fe9dd00bd30f90878ec8b4870858341e016ee1fcfae, $__internal_46eb303c03cbc443e4533c5d61059db5c32bc11bedd3b21a513e08178f72a485)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_972446337b81edc142e696ec672bc155c4c4d01c8f16951835b21baa1761213d = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_01dab0ceaf61d4ef92543fa08d0fcdfa241e4195b7b10ce7310abade9a17aa31 = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_01dab0ceaf61d4ef92543fa08d0fcdfa241e4195b7b10ce7310abade9a17aa31 || 0 === strpos($__internal_972446337b81edc142e696ec672bc155c4c4d01c8f16951835b21baa1761213d, $__internal_01dab0ceaf61d4ef92543fa08d0fcdfa241e4195b7b10ce7310abade9a17aa31)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_75201fd1b1dacff4ae01fbbf37e05616627a27bd05c51e5f523a30482f2363f7 = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_6d7be9211129cc314524568df307b2e3fe0de50b0432eb3ac499b5f59640af87 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_6d7be9211129cc314524568df307b2e3fe0de50b0432eb3ac499b5f59640af87 || 0 === strpos($__internal_75201fd1b1dacff4ae01fbbf37e05616627a27bd05c51e5f523a30482f2363f7, $__internal_6d7be9211129cc314524568df307b2e3fe0de50b0432eb3ac499b5f59640af87)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_31b71db3021d0891e797ba73df0a88637e500aa67d0595dadacdccd9bc504d60 = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ad7c679075700d71ce906213a0c1c68d4f0829769d51e30fd19bf4a92a3920b2 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_ad7c679075700d71ce906213a0c1c68d4f0829769d51e30fd19bf4a92a3920b2 || 0 === strpos($__internal_31b71db3021d0891e797ba73df0a88637e500aa67d0595dadacdccd9bc504d60, $__internal_ad7c679075700d71ce906213a0c1c68d4f0829769d51e30fd19bf4a92a3920b2)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_5d02abc24dff401f0b2050c2769e69eca6fd0ffce3acc15c33c4e7f7a1f46fa9 = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2135108eb57539609206466af28f7ac465bc56cd19efb263858124d4db9de42a = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_2135108eb57539609206466af28f7ac465bc56cd19efb263858124d4db9de42a || 0 === strpos($__internal_5d02abc24dff401f0b2050c2769e69eca6fd0ffce3acc15c33c4e7f7a1f46fa9, $__internal_2135108eb57539609206466af28f7ac465bc56cd19efb263858124d4db9de42a)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_62fceda5db37b92948b44effd576a609a7a71720070d06ef5eed0a1e7b855422 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_7b1c5a0a08c3d0f090dd6b2d4ea6b80c821e6ee32b6677177711491ccfecaa94 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_7b1c5a0a08c3d0f090dd6b2d4ea6b80c821e6ee32b6677177711491ccfecaa94 || 0 === strpos($__internal_62fceda5db37b92948b44effd576a609a7a71720070d06ef5eed0a1e7b855422, $__internal_7b1c5a0a08c3d0f090dd6b2d4ea6b80c821e6ee32b6677177711491ccfecaa94)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_2c05ad4d8b5499836b45fbdbea0ed970a9d4a1776b9d57eb7a19d54b1dcc7138 = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_c5541417b3c8c055d5853432ab01db44ca848f8c3fdf47707bec269a7e8d5efd = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_c5541417b3c8c055d5853432ab01db44ca848f8c3fdf47707bec269a7e8d5efd || 0 === strpos($__internal_2c05ad4d8b5499836b45fbdbea0ed970a9d4a1776b9d57eb7a19d54b1dcc7138, $__internal_c5541417b3c8c055d5853432ab01db44ca848f8c3fdf47707bec269a7e8d5efd)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : $this->getContext($context, "aboutus")) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : $this->getContext($context, "meetteam")) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : $this->getContext($context, "career")) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : $this->getContext($context, "valuedpartner")) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_ff9bf10d38d3c6d354be3344c3603ee9099e5c438d57172ea0cfe334e41a8763 = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_be1b39f0e1fc6cd77837e6ddffcd6250f3b44abf2c8d3122c8053e8f786ecd3e = "http://www.businessbid.ae/node2") && ('' === $__internal_be1b39f0e1fc6cd77837e6ddffcd6250f3b44abf2c8d3122c8053e8f786ecd3e || 0 === strpos($__internal_ff9bf10d38d3c6d354be3344c3603ee9099e5c438d57172ea0cfe334e41a8763, $__internal_be1b39f0e1fc6cd77837e6ddffcd6250f3b44abf2c8d3122c8053e8f786ecd3e)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_5791174f269cf939de8fe0639b5f872c8d1016e3cdd670f8d31ad1109affdbe8 = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_39502d2c53a22e711258f4ef6e057fac45f9af7ca796b9bcf29df5f061beaa0b = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_39502d2c53a22e711258f4ef6e057fac45f9af7ca796b9bcf29df5f061beaa0b || 0 === strpos($__internal_5791174f269cf939de8fe0639b5f872c8d1016e3cdd670f8d31ad1109affdbe8, $__internal_39502d2c53a22e711258f4ef6e057fac45f9af7ca796b9bcf29df5f061beaa0b)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_f0c9b5e4567c5e8b265eafcb4f08f51a466321f56fe5f22e8e64726a56e0f423 = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_fcbc20455bcfa68d9996663b43c399b574c7dd1449ded090c7a98f971fb793a3 = "http://www.businessbid.ae/faq") && ('' === $__internal_fcbc20455bcfa68d9996663b43c399b574c7dd1449ded090c7a98f971fb793a3 || 0 === strpos($__internal_f0c9b5e4567c5e8b265eafcb4f08f51a466321f56fe5f22e8e64726a56e0f423, $__internal_fcbc20455bcfa68d9996663b43c399b574c7dd1449ded090c7a98f971fb793a3)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_b5a538eac8d98b4a9a781bd5c62755d61984914227f0d5c6fad3a873756d625a = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_5cd38b5cfc8bad07b70438bd7c42f1d2d90b2bfb054c4aaf26f00d6b9cc2c944 = "http://www.businessbid.ae/node4") && ('' === $__internal_5cd38b5cfc8bad07b70438bd7c42f1d2d90b2bfb054c4aaf26f00d6b9cc2c944 || 0 === strpos($__internal_b5a538eac8d98b4a9a781bd5c62755d61984914227f0d5c6fad3a873756d625a, $__internal_5cd38b5cfc8bad07b70438bd7c42f1d2d90b2bfb054c4aaf26f00d6b9cc2c944)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_7e3bfee3391a8a3e2c3112fb35978dcc9a41ac6ae9d691446c88f59606be5a3e = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_8da6b3870390dbbda3d6d0b6467024852919a98d0494ea3af3e2ac9886d27b68 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_8da6b3870390dbbda3d6d0b6467024852919a98d0494ea3af3e2ac9886d27b68 || 0 === strpos($__internal_7e3bfee3391a8a3e2c3112fb35978dcc9a41ac6ae9d691446c88f59606be5a3e, $__internal_8da6b3870390dbbda3d6d0b6467024852919a98d0494ea3af3e2ac9886d27b68)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_8ea9191da9da2df74a795bddfb8747ee10edd06ce81078ffcb84f69b0397427a = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_8325fbb0fe60613331b6bccd4071b85e9161f16f14ef9ce1a564a84fdefa25c3 = "http://www.businessbid.ae/node6") && ('' === $__internal_8325fbb0fe60613331b6bccd4071b85e9161f16f14ef9ce1a564a84fdefa25c3 || 0 === strpos($__internal_8ea9191da9da2df74a795bddfb8747ee10edd06ce81078ffcb84f69b0397427a, $__internal_8325fbb0fe60613331b6bccd4071b85e9161f16f14ef9ce1a564a84fdefa25c3)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_45dcf3ffac20cc82845b2fd74081c612e93471695560803c7dc3767adf5fb7bb = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_b9794ac9b0ce1d9503d02c27346553ebdfe846b7893ac82ee8ab7c0d117e0d9f = "http://www.businessbid.ae/feedback") && ('' === $__internal_b9794ac9b0ce1d9503d02c27346553ebdfe846b7893ac82ee8ab7c0d117e0d9f || 0 === strpos($__internal_45dcf3ffac20cc82845b2fd74081c612e93471695560803c7dc3767adf5fb7bb, $__internal_b9794ac9b0ce1d9503d02c27346553ebdfe846b7893ac82ee8ab7c0d117e0d9f)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_08f0f0a7838679a8fb57e45e84f43689c7a82ecea42acf0b80b646434d860dcb = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_a22613b73fb04590a948d3adf1126c56eebae3f5205025ce13dd18424afebb69 = "http://www.businessbid.ae/sitemap") && ('' === $__internal_a22613b73fb04590a948d3adf1126c56eebae3f5205025ce13dd18424afebb69 || 0 === strpos($__internal_08f0f0a7838679a8fb57e45e84f43689c7a82ecea42acf0b80b646434d860dcb, $__internal_a22613b73fb04590a948d3adf1126c56eebae3f5205025ce13dd18424afebb69)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_b0eeb66bbec73acc8e67d925e509c1784eb12012f0472986084969e1df2bcf5e = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_f5f9df6313cdb17d482a327fbf6e278efe07050d7383b2a1849f1cfff491d1e5 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_f5f9df6313cdb17d482a327fbf6e278efe07050d7383b2a1849f1cfff491d1e5 || 0 === strpos($__internal_b0eeb66bbec73acc8e67d925e509c1784eb12012f0472986084969e1df2bcf5e, $__internal_f5f9df6313cdb17d482a327fbf6e278efe07050d7383b2a1849f1cfff491d1e5)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_94f574a10fe792fdedba00494260c4bcbdcd8e6583c874cbf4f3f888d6a8eb12 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_7a56f4ad189db0b5233105928a743cbb2f4f28239786132c88f009a82050d989 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_7a56f4ad189db0b5233105928a743cbb2f4f28239786132c88f009a82050d989 || 0 === strpos($__internal_94f574a10fe792fdedba00494260c4bcbdcd8e6583c874cbf4f3f888d6a8eb12, $__internal_7a56f4ad189db0b5233105928a743cbb2f4f28239786132c88f009a82050d989)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_12d43c8e32e6fe3f6f6753bf9deee08d7087161b37cac2e191c63846bcdb1ad8 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_e0f75bed840c8ce6d20cbbd700f50c73199981db7365610b7cde780101e39e90 = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_e0f75bed840c8ce6d20cbbd700f50c73199981db7365610b7cde780101e39e90 || 0 === strpos($__internal_12d43c8e32e6fe3f6f6753bf9deee08d7087161b37cac2e191c63846bcdb1ad8, $__internal_e0f75bed840c8ce6d20cbbd700f50c73199981db7365610b7cde780101e39e90)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_fd9c368d076f86cfa73649dd82471c80e331354625a548df9ae9d7029205bdb1 = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2d5075fb503a8166b5a4d1ca912ae6603591de2a643485a57e7fa815562a8aab = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_2d5075fb503a8166b5a4d1ca912ae6603591de2a643485a57e7fa815562a8aab || 0 === strpos($__internal_fd9c368d076f86cfa73649dd82471c80e331354625a548df9ae9d7029205bdb1, $__internal_2d5075fb503a8166b5a4d1ca912ae6603591de2a643485a57e7fa815562a8aab)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 162
    public function block_customcss($context, array $blocks = array())
    {
        // line 163
        echo "
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "<script>
\$(document).ready(function(){
    ";
        // line 169
        if (array_key_exists("keyword", $context)) {
            // line 170
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 174
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 310
    public function block_customjs($context, array $blocks = array())
    {
        // line 311
        echo "
";
    }

    // line 314
    public function block_jquery($context, array $blocks = array())
    {
        // line 315
        echo "
";
    }

    // line 317
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 318
        echo "
<!-- Google Analytics -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>

";
    }

    // line 402
    public function block_maincontent($context, array $blocks = array())
    {
        // line 403
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 490
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 530
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 560
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 561
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 563
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 565
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 571
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 572
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 574
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 576
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 581
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 582
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 584
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 586
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 593
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 594
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 596
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 598
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 604
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 605
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 607
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 609
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 615
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 616
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 618
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 620
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 490
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 491
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 500
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : $this->getContext($context, "activities")));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 501
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 502
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 503
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 504
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 505
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : $this->getContext($context, "fullname")), " ");
                // line 506
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : $this->getContext($context, "firstName")), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 508
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 509
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : $this->getContext($context, "fullname")), " ");
                // line 510
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : $this->getContext($context, "firstName")), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 512
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 519
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 525
        echo "    </div>
            </div>


    ";
    }

    // line 519
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 520
        echo "    <div class=\"request\">
    <h2>";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : $this->getContext($context, "enquiries")), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 636
    public function block_footer($context, array $blocks = array())
    {
        // line 637
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 666
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 687
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 688
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 740
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 742
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 752
    public function block_footerScript($context, array $blocks = array())
    {
        // line 753
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 754
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1411 => 754,  1406 => 753,  1403 => 752,  1392 => 742,  1387 => 740,  1347 => 703,  1339 => 698,  1335 => 697,  1331 => 696,  1320 => 688,  1317 => 687,  1313 => 685,  1304 => 679,  1300 => 678,  1296 => 677,  1292 => 676,  1288 => 675,  1277 => 667,  1273 => 666,  1264 => 660,  1260 => 659,  1256 => 658,  1252 => 657,  1248 => 656,  1227 => 637,  1224 => 636,  1216 => 521,  1213 => 520,  1210 => 519,  1202 => 525,  1200 => 519,  1193 => 514,  1186 => 512,  1170 => 510,  1167 => 509,  1164 => 508,  1150 => 506,  1147 => 505,  1144 => 504,  1142 => 503,  1138 => 502,  1135 => 501,  1131 => 500,  1121 => 493,  1117 => 491,  1114 => 490,  1098 => 620,  1092 => 618,  1086 => 616,  1084 => 615,  1076 => 609,  1070 => 607,  1064 => 605,  1062 => 604,  1054 => 598,  1048 => 596,  1042 => 594,  1040 => 593,  1031 => 586,  1025 => 584,  1019 => 582,  1017 => 581,  1010 => 576,  1004 => 574,  998 => 572,  996 => 571,  988 => 565,  982 => 563,  976 => 561,  974 => 560,  942 => 530,  940 => 490,  925 => 478,  914 => 470,  903 => 462,  892 => 454,  881 => 446,  871 => 439,  853 => 424,  843 => 417,  833 => 410,  824 => 403,  821 => 402,  813 => 318,  810 => 317,  805 => 315,  802 => 314,  797 => 311,  794 => 310,  775 => 294,  653 => 174,  647 => 170,  645 => 169,  641 => 167,  638 => 166,  633 => 163,  630 => 162,  626 => 127,  623 => 126,  617 => 109,  613 => 107,  611 => 106,  608 => 105,  606 => 104,  603 => 103,  601 => 102,  598 => 101,  596 => 100,  593 => 99,  591 => 98,  588 => 97,  586 => 96,  583 => 95,  581 => 94,  578 => 93,  576 => 92,  573 => 91,  571 => 90,  568 => 89,  566 => 88,  563 => 87,  561 => 86,  558 => 85,  556 => 84,  553 => 83,  551 => 82,  548 => 81,  546 => 80,  543 => 79,  541 => 78,  538 => 77,  536 => 76,  533 => 75,  531 => 74,  528 => 73,  526 => 72,  523 => 71,  521 => 70,  518 => 69,  516 => 68,  513 => 67,  511 => 66,  508 => 65,  506 => 64,  503 => 63,  501 => 62,  498 => 61,  496 => 60,  493 => 59,  491 => 58,  488 => 57,  486 => 56,  483 => 55,  481 => 54,  478 => 53,  476 => 52,  473 => 51,  471 => 50,  468 => 49,  466 => 48,  463 => 47,  461 => 46,  458 => 45,  456 => 44,  453 => 43,  451 => 42,  448 => 41,  446 => 40,  443 => 39,  441 => 38,  438 => 37,  436 => 36,  433 => 35,  431 => 34,  428 => 33,  426 => 32,  423 => 31,  421 => 30,  418 => 29,  416 => 28,  413 => 27,  411 => 26,  408 => 25,  406 => 24,  403 => 23,  401 => 22,  398 => 21,  396 => 20,  393 => 19,  391 => 18,  388 => 17,  386 => 16,  383 => 15,  381 => 14,  378 => 13,  376 => 12,  373 => 11,  368 => 763,  359 => 756,  357 => 752,  354 => 749,  352 => 636,  347 => 633,  345 => 402,  336 => 395,  322 => 392,  319 => 391,  317 => 390,  311 => 387,  307 => 385,  305 => 384,  297 => 379,  289 => 374,  284 => 372,  278 => 369,  273 => 367,  269 => 366,  260 => 359,  244 => 354,  240 => 353,  236 => 352,  232 => 350,  230 => 349,  224 => 346,  218 => 345,  214 => 343,  212 => 342,  201 => 336,  194 => 331,  192 => 330,  190 => 329,  188 => 328,  186 => 327,  182 => 325,  180 => 324,  177 => 323,  175 => 317,  173 => 314,  170 => 313,  168 => 310,  165 => 309,  163 => 166,  160 => 165,  158 => 162,  153 => 160,  149 => 159,  140 => 153,  135 => 151,  131 => 150,  126 => 148,  122 => 147,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Customer:inline_activationemail.html.twig */
class __TwigTemplate_18f25f92bfd23b6af4fee7e37ecaa62f4c9ee0d78414abba62781e0cfa58e50d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table width=\"100%\" bgcolor=\"#939598\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"> <tr> <td colspan=\"2\" align=\"center\" valign=\"middle\" bgcolor=\"#939598\"> <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr> <td>&nbsp;</td></tr><tr> <td height=\"23\" bgcolor=\"#f36e23\">&nbsp;</td></tr><tr> <td height=\"92\" align=\"center\" bgcolor=\"#FFFFFF\"><img src=\"http://115.124.120.36/web/img/logo.png\" ></td></tr><tr> <td height=\"34\" bgcolor=\"#FFFFFF\">&nbsp;</td></tr><tr> <td align=\"center\" bgcolor=\"#FFFFFF\"><table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr> <td align=\"center\" style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\"> <p style=\"font-size:24px; margin-bottom:10px; font-weight:600; color:#1e7dc0;\"> Welcome to the BusinessBid Network.</p></td></tr><tr> <td align=\"left\" style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\"> <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">Your BusinessBid account has now been created and you can login using your username and the temporary password listed below. You can change your password at anytime through your dashboard.</p></tr><tr> <td>&nbsp;</td></tr><tr>";
        if (array_key_exists("email", $context)) {
            echo "<td > <table width=\"280\" align=\"left\" cellspacing=\"2\" cellpadding=\"0\" border=\"0\"> <tbody> <tr> <td align=\"left\">Link : <a href=\"http://www.businessbid.ae/login\">www.businessbid.ae/login</a></td></tr><tr> <td align=\"left\">User Name :";
            echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
            echo "</td></tr><tr> <td align=\"left\">Password :";
            echo twig_escape_filter($this->env, (isset($context["password"]) ? $context["password"] : $this->getContext($context, "password")), "html", null, true);
            echo "</td></tr></tbody> </table> </td>";
        }
        echo "</tr><tr> <td>&nbsp;</td></tr><tr> <td align=\"center\"></td></tr><tr> <td align=\"left\" style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\"> <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">With your own BusinessBid account, you can be assured that you have access to qualified vendors to assist you with your projects anytime in a few easy steps.</p><p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">Please remember to write a review on your experiences. This will help us to serve you better.</p><p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">And of course, if you have any questions or need help call us on 04 42 13 777.</p></td></tr><tr> <td>&nbsp;</td></tr><tr> <td><p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">Regards <br>BusinessBid Team</p></td></tr><tr> <td>&nbsp;</td></tr></table></td></tr><tr> <td bgcolor=\"#eeeeee\">&nbsp;</td></tr><tr> <td bgcolor=\"#eeeeee\">&nbsp;</td></tr><tr> <td bgcolor=\"#939598\">&nbsp;</td></tr><tr> <td bgcolor=\"#939598\">&nbsp;</td></tr></table> </td></tr></table>";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Customer:inline_activationemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}

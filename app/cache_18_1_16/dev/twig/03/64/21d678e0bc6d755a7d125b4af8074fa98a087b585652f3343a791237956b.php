<?php

/* BBidsBBidsHomeBundle:Admin:enquirylist.html.twig */
class __TwigTemplate_036421d678e0bc6d755a7d125b4af8074fa98a087b585652f3343a791237956b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:enquirylist.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).on('click', '.myModalEnq',function(){\$(\".modal-body\").html(\"Loading Data..... Please wait.\");var a=\$(this).attr(\"data-enq\"),t=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_form_extra_fields", array("id" => "enq"));
        echo "\",d=t.replace(\"enq\",a);\$.ajax({url:d,success:function(a){\$(\".modal-body\").html(a)}})});
\$(document).ready(function() {
    \$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
    \$.datepicker.regional[\"\"].changeMonth = true;
    \$.datepicker.regional[\"\"].changeYear = true;
    \$.datepicker.setDefaults(\$.datepicker.regional['']);

    \$('#jobList').dataTable({
        \"sPaginationType\": \"full_numbers\",
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sEmptyTable\": '',
            \"sInfoEmpty\": '',
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
        \"sEmptyTable\": \"There are no records\",
        \"sAjaxSource\": \"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/joblist.php"), "html", null, true);
        echo "\",
        \"fnServerParams\": function(aoData) {

        },
        \"fnRowCallback\": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            \$(nRow).children().each(function(index, td) {
            \tif(index == 7){
            \t\tif (\$(td).html() == 0) {
            \t\t\t\$(td).css(\"background-color\", \"#FFDE00\");
            \t\t}
            \t}
            })

        }
    }).columnFilter({
        aoColumns: [{
            type: \"date-range\",
            sSelector: \"#dateFilter\",
            sRangeFormat: \"<label>Date</label> {from} {to}\"
        }, null, null, {
            type: 'select',
            values : ";
        // line 61
        echo twig_jsonencode_filter((isset($context["categoryList"]) ? $context["categoryList"] : $this->getContext($context, "categoryList")));
        echo ",
            sSelector: '#categoryFilter'
        }, null, {
            type: 'select',
            values: ";
        // line 65
        echo twig_jsonencode_filter((isset($context["cityList"]) ? $context["cityList"] : $this->getContext($context, "cityList")));
        echo ",
            sSelector: '#cityFilter'
        }, null, null,null]
    });
});
</script>
<script type=\"text/javascript\">

 function deletefunc(enquiryid){
\tvar con = confirm('Are you sure to delete the Enquiry? The record once deleted cannot be retrived back!');
\tif(con){
\t\tvar url = \"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_enquiry_delete", array("enquiryid" => "ENQID"));
        echo "\";
\t\tvar urlset = url.replace('ENQID', enquiryid);
\t\twindow.open(urlset,\"_self\");
\t\treturn true;
\t}else{
\t\treturn false;
\t}
 }
</script>
<div class=\"page-bg\">
    <div class=\"container inner_container admin-dashboard\">

<div class=\"page-title\"><h1>JOB REQUESTS</h1></div>

<div>
\t";
        // line 91
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 92
            echo "
\t<div class=\"alert alert-danger\">";
            // line 93
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "
\t";
        // line 97
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 98
            echo "
\t<div class=\"alert alert-success\">";
            // line 99
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "
\t";
        // line 103
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 104
            echo "
\t<div class=\"alert alert-success\">";
            // line 105
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "</div>

<div class=\"admin_filter report-filter\">
    <div class=\"col-md-3 side-clear\" id=\"dateFilter\"></div>
    <div class=\"col-md-3\" id=\"categoryFilter\"></div>
    <div class=\"col-md-3\" id=\"cityFilter\"></div>
</div>
<div class=\"latest-orders\">
\t<table id=\"jobList\">
\t\t<thead>
\t\t<tr>
\t\t\t<th>Date Recieved</th>
\t\t\t<th>Job Request Number</th>
\t\t\t<th>Subject</th>
\t\t\t<th>Category</th>
\t\t\t<th>Description</th>
\t\t\t<th>City</th>
\t\t\t<th>Email</th>
\t\t\t<th>Status</th>
\t\t\t<th>Action</th>
\t\t</tr>
\t\t</thead>
\t\t<tbody><tr><td colspan=\"9\" class=\"dataTables_empty\">Loading data from server... Please wait</td></tr></tbody>
\t\t<tfoot>
\t\t<tr>
\t\t\t<th>Date Recieved</th>
\t\t\t<th>Job Request Number</th>
\t\t\t<th>Subject</th>
\t\t\t<th>Category</th>
\t\t\t<th>Description</th>
\t\t\t<th>City</th>
\t\t\t<th>Email</th>
\t\t\t<th>Status</th>
\t\t\t<th>Action</th>
\t\t</tr>
\t\t</tfoot>
\t</table>
</div>
</div>

<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Additional Information</h4>
      </div>
      <div class=\"modal-body\">
      </div>
    </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:enquirylist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 108,  191 => 105,  188 => 104,  184 => 103,  181 => 102,  172 => 99,  169 => 98,  165 => 97,  162 => 96,  153 => 93,  150 => 92,  146 => 91,  128 => 76,  114 => 65,  107 => 61,  83 => 40,  49 => 9,  44 => 7,  40 => 6,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}

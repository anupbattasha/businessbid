<?php

/* BBidsBBidsHomeBundle:Categoriesform:education_learning_form.html.twig */
class __TwigTemplate_5ebaa52174aa303e43a2eb0679340548511a7ec8eab3f52dcfda7d7bb35ce67e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:education_learning_form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Best Approach To GetQuotes on Education & Learning";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : $this->getContext($context, "catid")))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "
<style>
    .pac-container:after{
        content:none !important;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/validationEngine.jquery.css"), "html", null, true);
        echo "\" type=\"text/css\"/>
<script type=\"text/javascript\">
  function checkForOption (field, rules, i, options) {
    if(field.prev(':checkbox').is(\":checked\"))
    {
      console.log('called')
      rules.push('required');
      return \"Please fill in the other field\";
    }
  }
</script>
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "
        <div class=\"alert alert-danger\">";
            // line 32
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
        ";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 37
            echo "
        <div class=\"alert alert-success\">";
            // line 38
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 42
            echo "
        <div class=\"alert alert-success\">";
            // line 43
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "    </div>
<div class=\"cat-form\">
  <div class=\"photo_form_one_area modal1\">
    <h1>Looking for Education & Learning Services Services in the UAE</h1>
    <p>Want to learn to play the guitar, interested in dance classes or need academic assistance? Get quotes from experienced and reliable professionals by submitting the simple form.
      <br>OR<br><br><span class=\"call-us\">CALL (04) 4213777</span></p>
  </div>

        ";
        // line 54
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "category_form")));
        echo "
        <div id=\"step1\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 1 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-1.png"), "html", null, true);
        echo "\" alt=\"education\">
                  </div>
                  ";
        // line 93
        $context["oddoreven"] = "even";
        // line 94
        echo "                  ";
        $context["subcateriesLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subcategories", array())) - 1);
        // line 95
        echo "                  ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subcategories", array())) % 2 == 1)) {
            // line 96
            echo "                    ";
            $context["oddoreven"] = "odd";
            // line 97
            echo "                  ";
        }
        // line 98
        echo "                  <div class=\"inner_photo_one_form_table\">
                      <h3>What learning service is required?</h3>
                      <div class=\"child_pro_tabil_main kindofService\">
                          ";
        // line 101
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subcategories", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 102
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 103
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"child_pho_table_left_inner_text\" >
                                    ";
                // line 105
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                    ";
                // line 106
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subcategory_other", array()), 'widget', array("attr" => array("class" => "subcategory_other validate[condRequired[categories_form_subcategories_5]]")));
                echo "
                                    <h5>(max 30 characters)</h5>
                                </div>
                              </div>
                            ";
            } else {
                // line 111
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"";
                // line 112
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "even") && ((isset($context["subcateriesLength"]) ? $context["subcateriesLength"] : $this->getContext($context, "subcateriesLength")) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                    ";
                // line 113
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                                </div>
                              </div>
                            ";
            }
            // line 117
            echo "                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "                          ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 119
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                           </div>
                          ";
        }
        // line 123
        echo "                          <div class=\"error\" id=\"kind_service\"></div>
                      </div>
                  <div class=\"photo_contunue_btn proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue1\">CONTINUE</a>
                  </div>
                  </div>
              </div>
          </div>
        </div>


        ";
        // line 135
        echo "        <div id=\"step2\" style=\"display:none\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 2 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 168
        $context["oddoreven"] = "even";
        // line 169
        echo "          ";
        $context["acedemicsLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acedemics", array())) - 1);
        // line 170
        echo "          ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acedemics", array())) % 2 == 1)) {
            // line 171
            echo "            ";
            $context["oddoreven"] = "odd";
            // line 172
            echo "          ";
        }
        // line 173
        echo "          <div class=\"photo_one_form\" id=\"acedemicsBlock\" style=\"display:none\">
            <div class=\"inner_photo_one_form acedemics\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-2.png"), "html", null, true);
        echo "\" alt=\"tutoring\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                    <h3>The tutoring is required for?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 181
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acedemics", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 182
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 183
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"child_pho_table_left_inner_text\" >
                                    ";
                // line 185
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                    ";
                // line 186
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acedemics_other", array()), 'widget', array("attr" => array("class" => "acedemic_other validate[condRequired[categories_form_acedemics_4]]")));
                echo "
                                    <h5>(max 30 characters)</h5>
                                </div>
                              </div>
                            ";
            } else {
                // line 191
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"";
                // line 192
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "even") && ((isset($context["acedemicsLength"]) ? $context["acedemicsLength"] : $this->getContext($context, "acedemicsLength")) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                    ";
                // line 193
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                                </div>
                              </div>
                            ";
            }
            // line 197
            echo "                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 198
        echo "                          ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 199
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                           </div>
                          ";
        }
        // line 203
        echo "                      <div class=\"error\" id=\"acedemics\"></div>
                  </div>
                </div>
          </div>
        </div>
        ";
        // line 208
        $context["oddoreven"] = "even";
        // line 209
        echo "        ";
        $context["subjectsLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subjects", array())) - 1);
        // line 210
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subjects", array())) % 2 == 1)) {
            // line 211
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 212
            echo "        ";
        }
        // line 213
        echo "        <div class=\"photo_one_form\" id=\"subjectsBlock\" style=\"display:none\">
          <div class=\"inner_photo_one_form subjects\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-2b.png"), "html", null, true);
        echo "\" alt=\"subjects\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>The tutoring is required for what subjects?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 221
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subjects", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 222
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 223
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 225
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 226
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subjects_other", array()), 'widget', array("attr" => array("class" => "subject_other validate[condRequired[categories_form_subjects_3]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 231
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"";
                // line 232
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "even") && ((isset($context["subjectsLength"]) ? $context["subjectsLength"] : $this->getContext($context, "subjectsLength")) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                  ";
                // line 233
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 237
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 238
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 239
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 243
        echo "                    <div class=\"error\" id=\"subjects\"></div>
                </div>
              </div>
          </div>
        </div>
        ";
        // line 248
        $context["oddoreven"] = "even";
        // line 249
        echo "        ";
        $context["music_typeLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "music_type", array())) - 1);
        // line 250
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "music_type", array())) % 2 == 1)) {
            // line 251
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 252
            echo "        ";
        }
        // line 253
        echo "        <div class=\"photo_one_form\" id=\"musicBlock\" style=\"display:none\">
          <div class=\"inner_photo_one_form musicType\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-music 2a.png"), "html", null, true);
        echo "\" alt=\"lessons\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>What lessons do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 261
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "music_type", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 262
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 263
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 265
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 266
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "music_type_other", array()), 'widget', array("attr" => array("class" => "music_other validate[condRequired[categories_form_music_type_4]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 271
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"";
                // line 272
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "even") && ((isset($context["music_typeLength"]) ? $context["music_typeLength"] : $this->getContext($context, "music_typeLength")) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                  ";
                // line 273
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 277
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 278
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 279
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 283
        echo "                    <div class=\"error\" id=\"musicType\"></div>
                </div>
              </div>
          </div>
        </div>
        ";
        // line 288
        $context["oddoreven"] = "even";
        // line 289
        echo "        ";
        $context["dan_sing_typeLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dan_sing_type", array())) - 1);
        // line 290
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dan_sing_type", array())) % 2 == 1)) {
            // line 291
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 292
            echo "        ";
        }
        // line 293
        echo "        <div class=\"photo_one_form\" id=\"danSingTypeBlock\" style=\"display:none\">
          <div class=\"inner_photo_one_form danSingType\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-Singning 2a.png"), "html", null, true);
        echo "\" alt=\"lessons require\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>What lessons do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 301
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dan_sing_type", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 302
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 303
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 305
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 306
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dan_sing_type_other", array()), 'widget', array("attr" => array("class" => "dan_sing_other validate[condRequired[categories_form_dan_sing_type_3]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 311
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"";
                // line 312
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "even") && ((isset($context["dan_sing_typeLength"]) ? $context["dan_sing_typeLength"] : $this->getContext($context, "dan_sing_typeLength")) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                  ";
                // line 313
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 317
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 318
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 319
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 323
        echo "                    <div class=\"error\" id=\"danSingType\"></div>
                </div>
              </div>
          </div>
        </div>
        ";
        // line 328
        $context["oddoreven"] = "even";
        // line 329
        echo "        ";
        $context["art_draw_typeLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "art_draw_type", array())) - 1);
        // line 330
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "art_draw_type", array())) % 2 == 1)) {
            // line 331
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 332
            echo "        ";
        }
        // line 333
        echo "        <div class=\"photo_one_form\" id=\"artDrawBlock\" style=\"display:none\">
          <div class=\"inner_photo_one_form artDraw\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 336
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-Art.png"), "html", null, true);
        echo "\" alt=\"calander Icon\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>What lessons do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 341
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "art_draw_type", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 342
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 343
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 345
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 346
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "art_draw_type_other", array()), 'widget', array("attr" => array("class" => "art_draw_other validate[condRequired[categories_form_art_draw_type_3]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 351
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"";
                // line 352
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "even") && ((isset($context["art_draw_typeLength"]) ? $context["art_draw_typeLength"] : $this->getContext($context, "art_draw_typeLength")) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                  ";
                // line 353
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 357
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 358
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 359
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 363
        echo "                    <div class=\"error\" id=\"artDraw\"></div>
                </div>
              </div>
          </div>
        </div>
        ";
        // line 368
        $context["oddoreven"] = "even";
        // line 369
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "onsite", array())) % 2 == 1)) {
            // line 370
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 371
            echo "        ";
        }
        // line 372
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 375
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-onsite teaching.png"), "html", null, true);
        echo "\" alt=\"teaching\">
                </div>
                <div class=\"inner_photo_one_form_table onsite\">
                    <h3>Do you require onsite teaching?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 380
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "onsite", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 381
            echo "                        <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                          <div class=\"child_pho_table_left_inner\" >
                              ";
            // line 383
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                          </div>
                        </div>
                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 387
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 388
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                       </div>
                      ";
        }
        // line 392
        echo "                      <div class=\"error\" id=\"onsite\"></div>
                  </div>
                </div>
            </div>
        </div>
        ";
        // line 397
        $context["oddoreven"] = "even";
        // line 398
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "travel", array())) % 2 == 1)) {
            // line 399
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 400
            echo "        ";
        }
        // line 401
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 404
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-travel coach.png"), "html", null, true);
        echo "\" alt=\"travel\">
                </div>
                <div class=\"inner_photo_one_form_table travel\">
                    <h3>Are you willing to travel to other areas for the coaching?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 409
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "travel", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 410
            echo "                        <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                          <div class=\"child_pho_table_left_inner\" >
                              ";
            // line 412
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                          </div>
                        </div>
                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 416
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 417
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                       </div>
                      ";
        }
        // line 421
        echo "                      <div class=\"error\" id=\"travel\"></div>
                  </div>
                </div>
            </div>
        </div>
        ";
        // line 426
        $context["oddoreven"] = "even";
        // line 427
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "frequency", array())) % 2 == 1)) {
            // line 428
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 429
            echo "        ";
        }
        // line 430
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 433
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/require.png"), "html", null, true);
        echo "\" alt=\"require teaching\">
                </div>
                <div class=\"inner_photo_one_form_table frequency\">
                    <h3>How often do you require this teaching?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 438
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "frequency", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 439
            echo "                        <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                          <div class=\"child_pho_table_left_inner\" >
                              ";
            // line 441
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                          </div>
                        </div>
                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 445
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : $this->getContext($context, "oddoreven")) == "odd")) {
            // line 446
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                       </div>
                      ";
        }
        // line 450
        echo "                      <div class=\"error\" id=\"frequency\"></div>
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <div class=\"proceed\">
                        <a href=\"javascript:void(0)\" id=\"continue2\" class=\"button3\"><button type=\"button\">CONTINUE</button></a>
                    </div>
                    <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback1\">BACK</a></div>
                  </div>
                </div>
            </div>
        </div>
      </div>
      ";
        // line 463
        echo "      ";
        // line 464
        echo "         <div id=\"step3\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                  <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 3 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 497
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 498
            echo "          <div class=\"photo_form_main\">
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form name\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 502
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/name.png"), "html", null, true);
            echo "\" alt=\"Name\">
                  </div>
                  <div class=\"inner_photo_one_form_table\" >
                      <h3>";
            // line 505
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contactname", array()), 'label');
            echo "</h3>
                          ";
            // line 506
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contactname", array()), 'widget', array("attr" => array("class" => "form-input name validate[required,custom[onlyLetterSp]]")));
            echo "
                      <div class=\"error\" id=\"name\" ></div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form  email\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 514
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/email.png"), "html", null, true);
            echo "\" alt=\"Email\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 517
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
            echo " </h3>
                     ";
            // line 518
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-input email_id validate[required,custom[email]]")));
            echo "
                     <div class=\"error\" id=\"email\" ></div>
                  </div>
              </div>
          </div>
          ";
        }
        // line 524
        echo "          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form location\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 527
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location.png"), "html", null, true);
        echo "\" alt=\"Location\">
                  </div>
                  <div class=\"inner_photo_one_form_table res2\" >
                      <h3>Select your Location*</h3>
                      <div class=\"child_pro_tabil_main res21\">
                          <div class=\"child_accounting_three_left\">
                              ";
        // line 533
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "land_location validate[minCheckbox[1]]")));
        echo "
                                  <p>";
        // line 534
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "locationtype", array()), "children", array()), 0, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"child_accounting_three_right\">
                             ";
        // line 537
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "land_location validate[minCheckbox[1]]")));
        echo "
                                  <p>";
        // line 538
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "locationtype", array()), "children", array()), 1, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"error\" id=\"location\"></div>
                      </div>
                  </div>
              </div>
          </div>


              ";
        // line 547
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            echo " <div class=\"photo_one_form for-uae \">
              <div class=\"inner_photo_one_form mask domcity_cont\" id=\"domCity\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
            // line 551
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <h3>";
            // line 554
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city", array()), 'label');
            echo "</h3>
                    <div class=\"child_pro_tabil_main city_cont\">
                      ";
            // line 556
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city", array()), 'widget', array("attr" => array("class" => "validate[condRequired[categories_form_locationtype_0]]")));
            echo "
                    </div>
                  </div>
                </div>
                <div class=\"error\" id=\"acc_city\"></div>
              </div>
              ";
        }
        // line 563
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 564
            echo "              <div class=\"inner_photo_one_form mask\" id=\"domArea\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 566
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location icon.png"), "html", null, true);
            echo "\" alt=\"Location Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 569
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "location", array()), 'label');
            echo "</h3>
                     ";
            // line 570
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "location", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_0]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_area\"></div>
              </div>
              ";
        }
        // line 575
        echo "
              ";
        // line 576
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 577
            echo "              <div class=\"mask\" id=\"domContact\" >
                <div class=\"inner_photo_one_form\" style=\"margin-top: 40px;\" id=\"uae_mobileno\">
                    <div class=\"inner_photo_one_form_img\">
                        <img src=\"";
            // line 580
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                    </div>
                    <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                        <h3>";
            // line 583
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobilecode", array()), 'label');
            echo "</h3>


                        <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                        ";
            // line 587
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobilecode", array()), 'widget', array("attr" => array("class" => "validate[condRequired[categories_form_locationtype_0]]")));
            echo "
                        ";
            // line 588
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_0],custom[number],minSize[6]]")));
            echo "

                    </div>
                    <div class=\"error\" id=\"acc_mobile\"></div>
                </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top: 40px;\" >
                        <div class=\"inner_photo_one_form_img\">
                            <img src=\"";
            // line 595
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                        </div>
                        <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                            <h3>";
            // line 598
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homecode", array()), 'label');
            echo "</h3>


                             <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                            ";
            // line 602
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homecode", array()), 'widget', array("attr" => array("class" => "validate[optional]")));
            echo "
                            ";
            // line 603
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homephone", array()), 'widget', array("attr" => array("class" => "form-input validate[optional,custom[number]]")));
            echo "

                        </div>
                    </div>
              </div></div>
                    ";
        }
        // line 609
        echo "

            <div class=\"photo_one_form for-foreign\">
              ";
        // line 612
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 613
            echo "              <div class=\"inner_photo_one_form mask intCountry_cont\" id=\"intCountry\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 615
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/international icon.png"), "html", null, true);
            echo "\" alt=\"international Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 618
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), 'label');
            echo "</h3>
                     ";
            // line 619
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_1]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcountry\"></div>
              </div>
              <div class=\"inner_photo_one_form mask\" id=\"intCity\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 625
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 628
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cityint", array()), 'label');
            echo "</h3>
                      ";
            // line 629
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cityint", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_1],custom[onlyLetterSp]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcity\"></div>
              </div>
              ";
        }
        // line 634
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 635
            echo "              <div class=\"mask\" id=\"intContact\">
                <div class=\"inner_photo_one_form\" style=\"margin-top:40px;\" id=\"int_mobileno\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 638
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 641
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobilecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 643
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobilecodeint", array()), 'widget', array("attr" => array("class" => "form-control validate[condRequired[categories_form_locationtype_1],custom[number]]")));
            echo "
                      ";
            // line 644
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobileint", array()), 'widget', array("attr" => array("class" => "form-input1 validate[condRequired[categories_form_locationtype_1],custom[number],minSize[6]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intmobile\"></div>
              </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 650
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 653
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 655
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homecodeint", array()), 'widget', array("attr" => array("class" => "form-control validate[optional,custom[number]]")));
            echo "
                      ";
            // line 656
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homephoneint", array()), 'widget', array("attr" => array("class" => "form-input1 validate[optional,custom[number]]")));
            echo "
                  </div>
              </div>
             </div>
              ";
        }
        // line 661
        echo "            </div>

            <div class=\"photo_one_form\" id=\"hireBlock\">
              <div class=\"inner_photo_one_form hire_cont\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 667
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/looking to hire.png"), "html", null, true);
        echo "\" alt=\"Looking To Hire\">
                  </div>
                  <div class=\"inner_photo_one_form_table resp1\">
                    <h3>When are you looking to hire?*</h3>
                    <div class=\"child_pro_tabil_main  \">
                      ";
        // line 672
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hire", array()), 'widget');
        echo "
                    </div>
                  </div>
                </div>
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 677
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/information.png"), "html", null, true);
        echo "\" alt=\"Information\">
                </div>
                <div class=\"inner_photo_one_form_table resp1\">
                    <h3>Is there any other additional information you would like to provide?</h3>
                    ";
        // line 681
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'widget', array("attr" => array("class" => "validate[optional]")));
        echo "
                     <br> <h5 class=\"char\">(max 120 characters)</h5>
                </div>

                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue3\" class=\"button3\">";
        // line 687
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "postJob", array()), 'widget', array("label" => "CONTINUE", "attr" => array("class" => "")));
        echo "</a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback2\">BACK</a></div>
                </div>
              </div>
          </div>
        </div>
        </div>
      ";
        // line 696
        echo "      ";
        // line 697
        echo "        <div id=\"step4\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <p>Page 4 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <h3 class=\"thankyou-heading\"><b>Thank you, you’re done!</b></h3>
              <p class=\"thankyou-content\" id=\"enquiryResult\">Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!</p>
              <p class=\"thankyou-content\">If you thought BusinessBid was useful to you, why not share the love? We’d greatly appreciate it. Like us on
              Facebook or follow us on Twitter to to keep updated on all the news and best deals.</p>
              <div class=\"social-icon\">
                <a href=\"https://twitter.com/\"><img src=\"";
        // line 736
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/twitter.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Twitter Bird\"/></a>
                <a href=\"https://www.facebook.com/\"><img src=\"";
        // line 737
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Facebook Sign\" /></a>
              </div>
              <div id=\"thankyou-btn\"  class=\"inner_photo_one_form\">
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 741
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">LOG ANOTHER REQUEST</a>
              </div>
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 744
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" >CONTINUE TO HOMEPAGE</a>
              </div>
              </div>
          </div>
        </div>
        ";
        // line 750
        echo "           ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
</div>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script type=\"text/javascript\" src=\"";
        // line 758
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 759
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.validationEngine-en.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
<script src=\"";
        // line 760
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.validationEngine.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
<script type=\"text/javascript\" src=\"";
        // line 761
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script>
function scrollBox(tag) {
    \$('html,body').animate({
        scrollTop: tag.position().top - 10
    }, 'slow');
}
\$(document).ready(function() {
  var validationFlag = true;
  // binds form submission and fields to the validation engine
  \$(\"#category_form\").validationEngine({
      ignore: \":hidden\"
  });
  \$('div.proceed').click(function() {
      var clickedStep = \$(this).children(\"a:first\").attr('id'),
          \$step = clickedStep.match(/\\d+/);

      if (\$step) {
          if (\$step == \"1\") {
              valid = \$(\"#category_form\").validationEngine('validate');
              if (valid == false) {
                  \$(\"#category_form\").validationEngine({
                      ignore: \":hidden\"
                  });
                  return false;
              }
              \$(\"[name='categories_form[subcategories][]']:checked:checked:enabled\").each(function() {
                  var selectedSubCategory = \$(this).val();
                  switch (selectedSubCategory) {
                      case '1061':
                          \$('div#acedemicsBlock,div#subjectsBlock').show();
                          break;
                      case '1062':
                          \$('div#musicBlock').show();
                          break;
                      case '1063':
                      case '1064':
                          \$('div#danSingTypeBlock').show();
                          break;
                      case '1065':
                          \$('div#artDrawBlock').show();
                          break;
                  }
              });
          } else if (\$step == \"2\") {

              var valid = \$(\"#category_form\").validationEngine('validate');
              if (valid == false) {
                  \$(\"#category_form\").validationEngine({
                      ignore: \":hidden\"
                  });
                  return false;
              }
          } else if (\$step == \"3\") {
            validationFlag = false;
              valid = \$(\"#category_form\").validationEngine('validate');
              if (valid == false) {
                  \$(\"#category_form\").validationEngine({
                      ignore: \":hidden\"
                  });
                  return false;
              }
              validationFlag = true;
              //\$(\"#category_form\").trigger( \"submit\" );
          }

          \$(\"div#step\" + \$step).hide();
          \$step++;
          \$(\"#category_form\").validationEngine('attach');
          \$(\"div#step\" + \$step).show();
          scrollBox(\$(\"div#step\" + \$step));
          return true;
      }
  });
  \$(\"div.goback\").click(function(){
      var gobackStep = \$(this).children(\"a:first\").attr('id'), \$step = gobackStep.match(/\\d+/);
      \$(\"div#step\"+\$step).show();
      if(\$step == 1)
        \$('div#acedemicsBlock,div#subjectsBlock,div#musicBlock,div#danSingTypeBlock,div#artDrawBlock').hide();
      \$step++;
      \$(\"div#step\"+\$step).hide();
  });
  ";
        // line 844
        echo "  var forms = [
    '[ name=\"";
        // line 845
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
  ];

  \$(forms.join(',')).submit(function(e) {
      e.preventDefault();
      console.log(\"tried submitting\")
      if (validationFlag) {
          postForm(\$(this), function(response) {
              if (response.hasOwnProperty('error')) {
                  alert(response.error);
                  var i = 1;
                  for (i = 1; i < 5; i++) {
                      \$(\"div#step\" + i).hide();
                  };
                  \$(\"div#step\" + response.step).show();
              } else {
                var form=document.createElement(\"form\");form.setAttribute(\"method\",\"post\"),form.setAttribute(\"action\",'";
        // line 861
        echo $this->env->getExtension('routing')->getPath("bizbids_job_post_sucessful");
        echo "');var hiddenField=document.createElement(\"input\");hiddenField.setAttribute(\"name\",\"jobno\"),hiddenField.setAttribute(\"value\",response.jobNo),hiddenField.setAttribute(\"type\",\"hidden\"),form.appendChild(hiddenField),document.body.appendChild(form),form.submit();
                \$(\"#category_form\")[0].reset();
              }
          });
          ga('send', 'event', 'Button', 'Click', 'Form Sent');
      }
      return false;
  });

  function postForm(\$form, callback) {
      NProgress.start();
      \$.ajax({
          type: \$form.attr('method'),
          beforeSend: function() {
              NProgress.inc()
          },
          url: \$form.attr('action'),
          data: \$form.serialize(),
          success: function(data) {
              callback(data);
              NProgress.done();
          },
          error: function(xhr) {
              alert(\"Error occured.please try again\");
              NProgress.done();
          }
      });
  }

});
</script>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:education_learning_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1780 => 861,  1761 => 845,  1758 => 844,  1673 => 761,  1669 => 760,  1665 => 759,  1661 => 758,  1649 => 750,  1641 => 744,  1635 => 741,  1628 => 737,  1624 => 736,  1583 => 697,  1581 => 696,  1570 => 687,  1561 => 681,  1554 => 677,  1546 => 672,  1538 => 667,  1530 => 661,  1522 => 656,  1518 => 655,  1513 => 653,  1507 => 650,  1498 => 644,  1494 => 643,  1489 => 641,  1483 => 638,  1478 => 635,  1475 => 634,  1467 => 629,  1463 => 628,  1457 => 625,  1448 => 619,  1444 => 618,  1438 => 615,  1434 => 613,  1432 => 612,  1427 => 609,  1418 => 603,  1414 => 602,  1407 => 598,  1401 => 595,  1391 => 588,  1387 => 587,  1380 => 583,  1374 => 580,  1369 => 577,  1367 => 576,  1364 => 575,  1356 => 570,  1352 => 569,  1346 => 566,  1342 => 564,  1339 => 563,  1329 => 556,  1324 => 554,  1318 => 551,  1311 => 547,  1299 => 538,  1295 => 537,  1289 => 534,  1285 => 533,  1276 => 527,  1271 => 524,  1262 => 518,  1258 => 517,  1252 => 514,  1241 => 506,  1237 => 505,  1231 => 502,  1225 => 498,  1223 => 497,  1188 => 464,  1186 => 463,  1172 => 450,  1166 => 446,  1163 => 445,  1143 => 441,  1137 => 439,  1120 => 438,  1112 => 433,  1107 => 430,  1104 => 429,  1101 => 428,  1098 => 427,  1096 => 426,  1089 => 421,  1083 => 417,  1080 => 416,  1060 => 412,  1054 => 410,  1037 => 409,  1029 => 404,  1024 => 401,  1021 => 400,  1018 => 399,  1015 => 398,  1013 => 397,  1006 => 392,  1000 => 388,  997 => 387,  977 => 383,  971 => 381,  954 => 380,  946 => 375,  941 => 372,  938 => 371,  935 => 370,  932 => 369,  930 => 368,  923 => 363,  917 => 359,  914 => 358,  900 => 357,  891 => 353,  883 => 352,  878 => 351,  870 => 346,  866 => 345,  860 => 343,  857 => 342,  840 => 341,  832 => 336,  827 => 333,  824 => 332,  821 => 331,  818 => 330,  815 => 329,  813 => 328,  806 => 323,  800 => 319,  797 => 318,  783 => 317,  774 => 313,  766 => 312,  761 => 311,  753 => 306,  749 => 305,  743 => 303,  740 => 302,  723 => 301,  715 => 296,  710 => 293,  707 => 292,  704 => 291,  701 => 290,  698 => 289,  696 => 288,  689 => 283,  683 => 279,  680 => 278,  666 => 277,  657 => 273,  649 => 272,  644 => 271,  636 => 266,  632 => 265,  626 => 263,  623 => 262,  606 => 261,  598 => 256,  593 => 253,  590 => 252,  587 => 251,  584 => 250,  581 => 249,  579 => 248,  572 => 243,  566 => 239,  563 => 238,  549 => 237,  540 => 233,  532 => 232,  527 => 231,  519 => 226,  515 => 225,  509 => 223,  506 => 222,  489 => 221,  481 => 216,  476 => 213,  473 => 212,  470 => 211,  467 => 210,  464 => 209,  462 => 208,  455 => 203,  449 => 199,  446 => 198,  432 => 197,  423 => 193,  415 => 192,  410 => 191,  402 => 186,  398 => 185,  392 => 183,  389 => 182,  372 => 181,  364 => 176,  359 => 173,  356 => 172,  353 => 171,  350 => 170,  347 => 169,  345 => 168,  310 => 135,  297 => 123,  291 => 119,  288 => 118,  274 => 117,  265 => 113,  257 => 112,  252 => 111,  244 => 106,  240 => 105,  234 => 103,  231 => 102,  214 => 101,  209 => 98,  206 => 97,  203 => 96,  200 => 95,  197 => 94,  195 => 93,  190 => 91,  150 => 54,  140 => 46,  131 => 43,  128 => 42,  123 => 41,  114 => 38,  111 => 37,  107 => 36,  104 => 35,  95 => 32,  92 => 31,  88 => 30,  72 => 17,  68 => 16,  64 => 15,  56 => 9,  53 => 8,  48 => 5,  45 => 4,  37 => 3,  31 => 2,  11 => 1,);
    }
}

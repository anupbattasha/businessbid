<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig */
class __TwigTemplate_bb7e32a82eaa8f2a85c03ee92a54d503d6122f057e9d92e13f3dfaaa652617f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["priorityArray"] = array(0 => "No Priority", 1 => "Urgent", 2 => "1-3 days", 3 => "4-7 days", 4 => "7-14 days", 5 => "Just Planning");
        // line 2
        echo "<style type=\"text/css\">
.tg{border-collapse:collapse;border-spacing:0;border-color:#ccc}.tg td{font-family:font-family:Proxinova,Calibri,Arial;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff}.tg th{font-family:font-family:Proxinova,Calibri,Arial;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0}.tg .tg-4eph{background-color:#f9f9f9}
</style>

    <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; margin-top:0px; width:100%;\">Dear Vendor,</p>

    <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; margin-top:0px; width:100%;\">Please note that you have received a job request with the following details. The same job request has been sent to you via text message (SMS).</p>

    <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; margin-top:0px; width:100%;\">Please access your BusinessBid dashboard to accept the job and receive client contact information. You may also accept the job via your phone by replying BID ";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["jobNo"]) ? $context["jobNo"] : $this->getContext($context, "jobNo")), "html", null, true);
        echo " to the text message.</p>



   
    <table class=\"tg\">
      <tr>
        <th class=\"tg-031e\" colspan=\"2\">Job Information</th>
      </tr>
      <tr>
        <td class=\"tg-4eph\">Job reference number</td>
        <td class=\"tg-4eph\">";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["jobNo"]) ? $context["jobNo"] : $this->getContext($context, "jobNo")), "html", null, true);
        echo "</td>
      </tr>
      <tr>
        <td class=\"tg-4eph\">Looking for</td>
        <td class=\"tg-4eph\">";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["eSubCategoryString"]) ? $context["eSubCategoryString"] : $this->getContext($context, "eSubCategoryString")), "html", null, true);
        echo "</td>
      </tr>
    ";
        // line 27
        $context["rowColor"] = "";
        // line 28
        echo "
    ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["formExtraFields"]) ? $context["formExtraFields"] : $this->getContext($context, "formExtraFields")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 30
            echo "      ";
            if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                echo " ";
                $context["rowColor"] = "tg-4eph";
                echo " ";
            } else {
                echo " ";
                $context["rowColor"] = "tg-031e";
                echo " ";
            }
            // line 31
            echo "      <tr>
        <td class=\"";
            // line 32
            echo twig_escape_filter($this->env, (isset($context["rowColor"]) ? $context["rowColor"] : $this->getContext($context, "rowColor")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fields"], "keyName", array()), "html", null, true);
            echo "</td>
        <td class=\"";
            // line 33
            echo twig_escape_filter($this->env, (isset($context["rowColor"]) ? $context["rowColor"] : $this->getContext($context, "rowColor")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fields"], "keyValue", array()), "html", null, true);
            echo "</td>
      </tr>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "        ";
        if ( !twig_test_empty((isset($context["city"]) ? $context["city"] : $this->getContext($context, "city")))) {
            // line 37
            echo "        <tr>
            <td class=\"tg-031e\">Customer City Location</td>
            <td class=\"tg-031e\">";
            // line 39
            echo twig_escape_filter($this->env, (isset($context["city"]) ? $context["city"] : $this->getContext($context, "city")), "html", null, true);
            echo "</td>
        </tr>
        ";
        }
        // line 42
        echo "
        ";
        // line 43
        if ( !twig_test_empty((isset($context["hire"]) ? $context["hire"] : $this->getContext($context, "hire")))) {
            // line 44
            echo "        <tr>
            <td class=\"tg-4eph\">Customer Job Priority</td>
            <td class=\"tg-4eph\">";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["priorityArray"]) ? $context["priorityArray"] : $this->getContext($context, "priorityArray")), (isset($context["hire"]) ? $context["hire"] : $this->getContext($context, "hire")), array(), "array"), "html", null, true);
            echo "</td>
        </tr>
        ";
        }
        // line 49
        echo "
        ";
        // line 50
        if ( !twig_test_empty((isset($context["description"]) ? $context["description"] : $this->getContext($context, "description")))) {
            // line 51
            echo "        <tr>
            <td class=\"tg-031e\">Additional Information</td>
            <td class=\"tg-031e\">";
            // line 53
            echo twig_escape_filter($this->env, (isset($context["description"]) ? $context["description"] : $this->getContext($context, "description")), "html", null, true);
            echo "</td>
        </tr>
        ";
        }
        // line 56
        echo "    </table>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Vendor:vendor_jobnotify_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 56,  155 => 53,  151 => 51,  149 => 50,  146 => 49,  140 => 46,  136 => 44,  134 => 43,  131 => 42,  125 => 39,  121 => 37,  118 => 36,  99 => 33,  93 => 32,  90 => 31,  79 => 30,  62 => 29,  59 => 28,  57 => 27,  52 => 25,  45 => 21,  31 => 10,  21 => 2,  19 => 1,);
    }
}

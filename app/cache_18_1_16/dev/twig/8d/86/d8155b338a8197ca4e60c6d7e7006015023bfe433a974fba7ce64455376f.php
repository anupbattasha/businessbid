<?php

/* ::base_admin.html.twig */
class __TwigTemplate_8d86d8155b338a8197ca4e60c6d7e7006015023bfe433a974fba7ce64455376f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'chartscript' => array($this, 'block_chartscript'),
            'maincontent' => array($this, 'block_maincontent'),
            'header' => array($this, 'block_header'),
            'menu' => array($this, 'block_menu'),
            'topmenu' => array($this, 'block_topmenu'),
            'pageblock' => array($this, 'block_pageblock'),
            'dashboard' => array($this, 'block_dashboard'),
            'statistics' => array($this, 'block_statistics'),
            'latestorders' => array($this, 'block_latestorders'),
            'enquiries' => array($this, 'block_enquiries'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<head>
<title>Business BID Administrator</title>
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">

<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<!--[if lt IE 9]>
  \t<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/html5shiv.min.js"), "html", null, true);
        echo "\"></script>
  \t<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.min.js"), "html", null, true);
        echo "\"></script>
\t<![endif]-->
\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<!--[if IE 8]>
\t\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style-ie.css"), "html", null, true);
        echo "\">
\t<![endif]-->
\t<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/eticket.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.min.css"), "html", null, true);
        echo "\">
";
        // line 23
        echo "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>
";
        // line 25
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/highcharts.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/modules/exporting.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_updateemailid\").on('input',function (){
\t\tvar ax = \t\$(\"#form_updateemailid\").val();
\t\t//alert(ax);
\t\tif(ax==''){
\t\t\t\$(\"#emailexists\").text('');
\t\t} else {

\t\t\tvar filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
\t\t\tvar \$th = \$(this);
\t\t\tif (filter.test(ax)) {
\t\t\t\t\$th.css('border', '3px solid green');
\t\t\t}
\t\t\telse {
\t\t\t\t\$th.css('border', '3px solid red');
\t\t\t\te.preventDefault();
\t\t\t}
\t\t\tif(ax.indexOf('@') > -1 ) {
\t\t\t\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
\t\t\t\t\tif(result == \"exists\") {
\t\t\t\t\t\t\$(\"#emailexists\").text('Email address already exists!');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(\"#emailexists\").text('');
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t});
\t\t\t}

\t\t}
\t});

});
</script>
";
        // line 64
        $this->displayBlock('chartscript', $context, $blocks);
        // line 68
        echo "</head>
<body>
";
        // line 70
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "uid"), "method");
        // line 71
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "pid"), "method");
        // line 72
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "email"), "method");
        // line 73
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "name"), "method");
        // line 74
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 75
            echo "<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 contact-info\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>
\t\t\t\t\t<span>(04) 42 13 777</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3 left-clear\">
\t\t\t\t\t<!--<span class=\"contact-no\"><a href=\"/contact\">Contact Us</a></span>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6 clear-right dash-right-top-links\">
\t\t\t\t<ul>
\t\t\t\t\t<li>Welcome, <span class=\"profile-name\"><a href=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_edit", array("userid" => (isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))), "html", null, true);
            echo " \">";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
            echo "</a></span></li>
\t\t\t\t\t<li><a href=\"";
            // line 90
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
        }
        // line 97
        $this->displayBlock('maincontent', $context, $blocks);
        // line 305
        echo "</body>
</html>
";
    }

    // line 64
    public function block_chartscript($context, array $blocks = array())
    {
        // line 65
        echo "

";
    }

    // line 97
    public function block_maincontent($context, array $blocks = array())
    {
        // line 98
        echo "<div class=\"main_container\" >
\t";
        // line 99
        $this->displayBlock('header', $context, $blocks);
        // line 108
        echo "\t";
        $this->displayBlock('menu', $context, $blocks);
        // line 163
        echo "    \t";
        $this->displayBlock('pageblock', $context, $blocks);
        // line 294
        echo "   ";
        $this->displayBlock('footer', $context, $blocks);
        // line 303
        echo "
";
    }

    // line 99
    public function block_header($context, array $blocks = array())
    {
        // line 100
        echo "\t<div class=\"header container\">
    \t<div class=\"col-md-6\">
\t\t<a href=\"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_home");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"> </a>
        </div>
        <div class=\"col-md-6\">
        </div>
    </div>
\t";
    }

    // line 108
    public function block_menu($context, array $blocks = array())
    {
        // line 109
        echo "    <div class=\"menu_block menu_block-admin\">
    \t<div class=\"container side-clear\">
        \t\t<div class=\"navbar-header col-sm-2 side-clear\">
\t\t\t\t\t<ul class=\"page-title\"><li><a href=\"";
        // line 112
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_home");
        echo "\">DASHBOARD</a></li></ul>
\t\t\t\t</div>
\t\t\t\t";
        // line 114
        $this->displayBlock('topmenu', $context, $blocks);
        // line 160
        echo "        </div>
    </div>
\t";
    }

    // line 114
    public function block_topmenu($context, array $blocks = array())
    {
        // line 115
        echo "\t\t\t\t\t<div class=\"collapse navbar-collapse navHeaderCollapse col-sm-10 side-clear pull-right\">
\t\t\t\t\t\t<ul class=\"nav navbar-nav\">
\t\t\t\t\t\t\t<li><a href=\"";
        // line 117
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_enquiries", array("offset" => 1));
        echo "\">Job Requests</a></li>
\t\t\t\t\t\t\t<li class=\"active\"><a href=\"";
        // line 118
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_freetrails", array("offset" => 1));
        echo "\">Free Trial</a></li>
\t\t\t\t\t\t\t<li class=\"active\"><a href=\"";
        // line 119
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_users", array("offset" => 1));
        echo "\">Users</a></li>
\t\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Manage Vendor</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\"  aria-labelledby=\"dropdownMenu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 123
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_orders", array("offset" => 1));
        echo "\"> Vendor Orders</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_category_mapping");
        echo "\">Vendor Category Mapping</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_on_off_service");
        echo "\">Vendor Status Toggle</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_reviews_upload");
        echo "\" target=\"_blank\">Vendor Upload Reviews</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 127
        echo $this->env->getExtension('routing')->getPath("bizbids_anonymous_reviews_upload");
        echo "\" target=\"_blank\">Vendor Anonymous Reviews</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t\t";
        // line 132
        echo "\t\t\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Report</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\"  aria-labelledby=\"dropdownMenu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 134
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_reports");
        echo "\">Vendor Reports</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 135
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_orders_reports");
        echo "\">Vendor Orders</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 136
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_reports");
        echo "\">Customer Reports</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 137
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_enquiries_reports");
        echo "\">Job Requests</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 138
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_categories_reports");
        echo "\">Categories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 139
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_leads_list");
        echo "\">Vendor Leads List</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 142
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_categories");
        echo "\"> Categories</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 143
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviews", array("offset" => 1));
        echo "\">Reviews</a></li>
\t\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Manage Leads</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\"  aria-labelledby=\"dropdownMenu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 147
        echo $this->env->getExtension('routing')->getPath("bizbids_lead_dedcution_return_upload");
        echo "\">Lead Refund / Deduction</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 148
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_lead_management");
        echo "\">Lead Refund / Deduction History</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"";
        // line 151
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminhome");
        echo "\">Support<span class=\"caret\"></span></a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 153
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminlist", array("usertype" => "Vendor"));
        echo "\">Vendor Ticket List</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 154
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminlist", array("usertype" => 1));
        echo "\">Customer Ticket List</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 163
    public function block_pageblock($context, array $blocks = array())
    {
        // line 164
        echo "\t<div class=\"page-bg\">
    <div class=\"container inner_container admin-dashboard\">
\t";
        // line 166
        $this->displayBlock('dashboard', $context, $blocks);
        // line 202
        echo "
\t";
        // line 203
        $this->displayBlock('statistics', $context, $blocks);
        // line 210
        echo "
\t";
        // line 211
        $this->displayBlock('latestorders', $context, $blocks);
        // line 253
        echo "
\t";
        // line 254
        $this->displayBlock('enquiries', $context, $blocks);
        // line 290
        echo "
    </div>
    </div>
\t";
    }

    // line 166
    public function block_dashboard($context, array $blocks = array())
    {
        // line 167
        echo "        <div class=\"col-md-6\">
        \t<div class=\"overview_block block  overview-cotainer\">
\t\t\t\t<table>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Total Job Requests</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 172
        echo twig_escape_filter($this->env, (isset($context["totalenquirycount"]) ? $context["totalenquirycount"] : $this->getContext($context, "totalenquirycount")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Total  Job Requests This Year</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 176
        echo twig_escape_filter($this->env, (isset($context["totalenquirycount2"]) ? $context["totalenquirycount2"] : $this->getContext($context, "totalenquirycount2")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Vendor Orders</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 180
        echo twig_escape_filter($this->env, (isset($context["totalordercount"]) ? $context["totalordercount"] : $this->getContext($context, "totalordercount")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>No. of Customers</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 184
        echo twig_escape_filter($this->env, (isset($context["totalcustcount"]) ? $context["totalcustcount"] : $this->getContext($context, "totalcustcount")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>No. of Vendors</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 188
        echo twig_escape_filter($this->env, (isset($context["totalusercountforapproval"]) ? $context["totalusercountforapproval"] : $this->getContext($context, "totalusercountforapproval")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Job Requests Not Accepted</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 192
        echo twig_escape_filter($this->env, (isset($context["totalenquiryawaitingcount"]) ? $context["totalenquiryawaitingcount"] : $this->getContext($context, "totalenquiryawaitingcount")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Reviews Awaiting Approval</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 196
        echo twig_escape_filter($this->env, (isset($context["totalreviewsawaitingcount"]) ? $context["totalreviewsawaitingcount"] : $this->getContext($context, "totalreviewsawaitingcount")), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
            </div>
        </div>
\t";
    }

    // line 203
    public function block_statistics($context, array $blocks = array())
    {
        // line 204
        echo "        <div class=\"col-md-6 last statistics\">
        \t<div class=\"overview_block block\">
         <div id=\"linechart\" style=\"min-width: 400px; margin: 0 auto\"></div>
            </div>
        </div>
\t";
    }

    // line 211
    public function block_latestorders($context, array $blocks = array())
    {
        // line 212
        echo "        <div class=\"latest-orders top-spce\">
        \t<div class=\"overview_block block\">
            \t<h2>Last 10 Vendor Orders</h2>
                <table>
\t\t\t\t<thead><tr>
\t\t\t\t<th>Date Received</th>
\t\t\t\t<th>Order Number</th>
\t\t\t\t<th>Vendor Name</th>
\t\t\t\t<th>Business Name</th>
\t\t\t\t<th>Vendor Email-Id</th>
\t\t\t\t<th>Category</th>
\t\t\t\t<th>Lead Package</th>
\t\t\t\t<th>Payment Type</th>
\t\t\t\t<th>Status</th>
\t\t\t\t<th>Amount</th>
\t\t\t\t</tr></thead>
\t\t\t\t<tbody>
\t\t\t\t";
        // line 229
        if ((twig_length_filter($this->env, (isset($context["orders"]) ? $context["orders"] : $this->getContext($context, "orders"))) > 0)) {
            // line 230
            echo "
\t\t\t\t\t";
            // line 231
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : $this->getContext($context, "orders")));
            foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
                // line 232
                echo "\t\t\t\t\t\t<tr><td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["order"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td><td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "ordernumber", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 233
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "contactname", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 234
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "bizname", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 235
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "email", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 236
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "category", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 237
                if (($this->getAttribute($context["order"], "leadpack", array()) == 25)) {
                    echo " Bronze ";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 50)) {
                    echo " Silver
\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 238
$context["order"], "leadpack", array()) == 100)) {
                    echo " Gold ";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 250)) {
                    echo " Platinum ";
                }
                // line 239
                echo "\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>";
                // line 240
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "payoption", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 241
                if (($this->getAttribute($context["order"], "status", array()) == 1)) {
                    echo "<font color=\"green\">Approved</font>";
                } else {
                    echo "<font color=\"red\">Pending</font>";
                }
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 242
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "amount", array()), "html", null, true);
                echo "</td></tr>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 244
            echo "
\t\t\t\t";
        } else {
            // line 246
            echo "\t\t\t\t\t<tr> <td colspan=\"6\" class=\"no-record\"> No Records found  </td></tr>
\t\t\t\t";
        }
        // line 248
        echo "\t\t\t\t</tbody>
\t\t\t\t</table>
            </div>
        </div>
\t";
    }

    // line 254
    public function block_enquiries($context, array $blocks = array())
    {
        // line 255
        echo "        <div class=\"mid latest-enquires assasa\">
        \t<div class=\"overview_block block\">
            \t<h2>Last 10 Job Requests</h2>
                <table>
\t\t\t\t<thead><tr>
\t\t\t\t<th>Date Received</th>
\t\t\t\t<th>Job Request Number</th>
\t\t\t\t<th>Subject</th>
\t\t\t\t<th>Category</th>
\t\t\t\t<th>City</th>
\t\t\t\t<th>Name</th>
\t\t\t\t<th>Email</th>
\t\t\t\t<th>Contact Number</th>
\t\t\t\t<th>Status</th></tr></thead>
\t\t\t\t<tbody>
\t\t\t\t";
        // line 270
        if ((twig_length_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : $this->getContext($context, "enquiries"))) > 0)) {
            // line 271
            echo "\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : $this->getContext($context, "enquiries")));
            foreach ($context['_seq'] as $context["_key"] => $context["enquiry"]) {
                // line 272
                echo "\t\t\t\t\t\t<tr><td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 273
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "id", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 274
                echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enquiry"], "subj", array()), 0, 20), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 275
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "category", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 276
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "location", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 277
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "contactname", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 278
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "email", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 279
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "smsphone", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 280
                if (($this->getAttribute($context["enquiry"], "acceptstatus", array()) == 1)) {
                    echo "<font color=\"green\">Accepted</font>";
                } else {
                    echo " <font color=\"red\">Waiting</font>";
                }
                echo "</td></tr>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquiry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 282
            echo "\t\t\t\t";
        } else {
            // line 283
            echo "\t\t\t\t<tr> <td colspan=\"6\" class=\"no-record\"> No Records found  <td>
\t\t\t\t";
        }
        // line 285
        echo "\t\t\t\t</tbody>
\t\t\t\t</table>
            </div>
        </div>
\t";
    }

    // line 294
    public function block_footer($context, array $blocks = array())
    {
        // line 295
        echo "\t<div class=\"footer-dashboard text-center\">

\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"col-sm-6 footer-messages\">Copyright 2013 - 2014 BusinessBid</div>
    \t\t\t<div class=\"col-sm-6 clear-right\"></div>
\t\t\t<div>
    \t</div>
\t";
    }

    public function getTemplateName()
    {
        return "::base_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  674 => 295,  671 => 294,  663 => 285,  659 => 283,  656 => 282,  644 => 280,  640 => 279,  636 => 278,  632 => 277,  628 => 276,  624 => 275,  620 => 274,  616 => 273,  611 => 272,  606 => 271,  604 => 270,  587 => 255,  584 => 254,  576 => 248,  572 => 246,  568 => 244,  560 => 242,  552 => 241,  548 => 240,  545 => 239,  539 => 238,  533 => 237,  529 => 236,  525 => 235,  521 => 234,  517 => 233,  510 => 232,  506 => 231,  503 => 230,  501 => 229,  482 => 212,  479 => 211,  470 => 204,  467 => 203,  457 => 196,  450 => 192,  443 => 188,  436 => 184,  429 => 180,  422 => 176,  415 => 172,  408 => 167,  405 => 166,  398 => 290,  396 => 254,  393 => 253,  391 => 211,  388 => 210,  386 => 203,  383 => 202,  381 => 166,  377 => 164,  374 => 163,  364 => 154,  360 => 153,  355 => 151,  349 => 148,  345 => 147,  338 => 143,  334 => 142,  328 => 139,  324 => 138,  320 => 137,  316 => 136,  312 => 135,  308 => 134,  304 => 132,  297 => 127,  293 => 126,  289 => 125,  285 => 124,  281 => 123,  274 => 119,  270 => 118,  266 => 117,  262 => 115,  259 => 114,  253 => 160,  251 => 114,  246 => 112,  241 => 109,  238 => 108,  226 => 102,  222 => 100,  219 => 99,  214 => 303,  211 => 294,  208 => 163,  205 => 108,  203 => 99,  200 => 98,  197 => 97,  191 => 65,  188 => 64,  182 => 305,  180 => 97,  170 => 90,  164 => 89,  148 => 75,  146 => 74,  144 => 73,  142 => 72,  140 => 71,  138 => 70,  134 => 68,  132 => 64,  93 => 28,  89 => 27,  83 => 25,  80 => 23,  76 => 21,  72 => 20,  68 => 19,  64 => 18,  60 => 17,  55 => 15,  46 => 9,  42 => 8,  35 => 4,  30 => 1,);
    }
}

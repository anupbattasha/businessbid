<?php

/* BBidsBBidsHomeBundle:Admin:upload_reviews.html.twig */
class __TwigTemplate_7370cfd31e03561730c8a0e5fdaf322f9b7313c1e609f14f585076ec51af8a0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:upload_reviews.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
<div class=\"page-title\"><h1>Upload Vendor Review</h1></div>
<div class=\"edit-user-block\">
<div class=\"form_wrapper col-md-6\">


<div>
    ";
        // line 13
        echo "</div>
    ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "
    <div class=\"alert alert-danger\">";
            // line 16
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "
    <div class=\"alert alert-success\">";
            // line 22
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
    <div class=\"alert alert-success\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "reviewForm")));
        echo "
    <div>
        <div class=\"control-group\">";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "customername", array()), 'label');
        echo " <span>*</span></div> <div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "customername", array()), 'widget');
        echo "</div>
        <div class=\"control-group\">";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'label');
        echo " <span>*</span></div>  <div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'widget');
        echo "</div>
        <div class=\"control-group\">";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "enquiry", array()), 'label');
        echo " <span>*</span></div>  <div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "enquiry", array()), 'widget');
        echo "</div>
        <div class=\"control-group\">
            <div class=\"sameline\">";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "write", array()), 'widget');
        echo "</div>
        </div>
        ";
        // line 39
        echo "    </div>
    ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
</div>
</div>
</div>
<script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\">
    \$(document).ready(function () {

        \$( \"#uploadreviews_form_customername\" ).autocomplete({
          source: \"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("autocomplete/customer_name.php"), "html", null, true);
        echo "\",
          minLength: 2,
          select: function( event, ui ) {
            \$('#uploadreviews_form_uid').val(ui.item.id);

            vendorList(ui.item.id);
          },
          change: function(event,ui)
            {
                if (ui.item==null)
                {
                    \$( this ).val('');
                    \$( this ).focus();
                    \$('#uploadreviews_form_uid').val('');
                }
            }
        });

        function vendorList(userid) {
            \$.ajax({
                type: \"POST\",
                url: \"";
        // line 70
        echo $this->env->getExtension('routing')->getUrl("bizbids_reviews_ajax_call_upload");
        echo "?type=vend&customer_id=\" + userid,
                success: function(data) {
                    // Remove current options
                    \$('#uploadreviews_form_vendorname').html('<option value=\"\">Select a vendor</option>');
                    \$.each(data, function(k, v) {
                        \$('#uploadreviews_form_vendorname').append('<option value=\"' + k + '\">' + v + '</option>');
                    });
                }
            });
        }

        function enqList(userid, vendorid) {
            \$.ajax({
                type: \"POST\",
                url: \"";
        // line 84
        echo $this->env->getExtension('routing')->getUrl("bizbids_reviews_ajax_call_upload");
        echo "?type=enq&customer_id=\" + userid +\"&vendor_id=\"+vendorid,
                success: function(data) {
                    \$('#uploadreviews_form_enquiry').html('<option value=\"\">Select a enquiry no</option>');
                    \$.each(data, function(k, v) {
                        \$('#uploadreviews_form_enquiry').append('<option value=\"' + k + '\">' + v + '</option>');
                    });
                }
            });
        }
        \$('#uploadreviews_form_vendorname').change(function(){
           var vendorid = \$(this).val(), userid = \$('#uploadreviews_form_uid').val();
           if(userid != '' && vendorid != '') {
                enqList(userid, vendorid) ;
           } else {
             alert('Select a vendor or user');
           }
            return false;
        });
        var forms = [
            '[ name=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
          ];
        \$( forms.join(',') ).submit( function( e ){
            e.preventDefault();
            console.log(\"Tried submitting\");
            var vendorid = \$('#uploadreviews_form_vendorname').val(), userid = \$('#uploadreviews_form_uid').val(), eid = \$('#uploadreviews_form_enquiry').val();
            if(vendorid == '' || userid == '' || eid == '') {
                alert('Please select a value');
                return false;
            }
            var reviewUrl = \"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_review", array("authorid" => "aid", "vendorid" => "vid", "enquiryid" => "eid")), "html", null, true);
        echo "\";
            reviewUrl = reviewUrl.replace(\"aid\", userid);
            reviewUrl = reviewUrl.replace(\"vid\", vendorid);
            reviewUrl = reviewUrl.replace(\"eid\", eid);
            window.location.href = reviewUrl;
            return false;
        });

    });
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:upload_reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 113,  206 => 103,  184 => 84,  167 => 70,  143 => 49,  135 => 44,  128 => 40,  125 => 39,  120 => 36,  113 => 34,  107 => 33,  101 => 32,  95 => 30,  86 => 27,  83 => 26,  78 => 25,  69 => 22,  66 => 21,  62 => 20,  59 => 19,  50 => 16,  47 => 15,  43 => 14,  40 => 13,  31 => 4,  28 => 3,  11 => 1,);
    }
}

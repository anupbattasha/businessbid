<?php

/* BBidsBBidsHomeBundle:Categoriesform:catering_services_form.html.twig */
class __TwigTemplate_96a7bda8c49c5e341eb2e839855380789e1d0ff2c4bd6687ad0d58aa3e1be93b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:catering_services_form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Gather Quotes For Exhibitions And Stands Free Of Charge ";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "<style>
    .pac-container:after{
        content:none !important;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
        <div class=\"alert alert-danger\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
        ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "
        <div class=\"alert alert-success\">";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 30
            echo "
        <div class=\"alert alert-success\">";
            // line 31
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "    </div>
<div class=\"cat-form\">
          <div class=\"photo_form_one_area modal1 cater\">
            <h1>Looking for Catering Services in the UAE?</h1>
            <p>Tell us a little bit about your catering requirements and receive multiple quotes in <span>3 easy steps</span>. Compare quotes and choose the best catering company.
      <br>OR<br><br><span class=\"call-us\">CALL (04) 4213777</span></p>
        </div>

        ";
        // line 42
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "category_form")));
        echo "
        <div id=\"step1\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 1 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/catering.png"), "html", null, true);
        echo "\" alt=\"Catering\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>What kind of catering services do you require?</h3>
                      <div class=\"child_pro_tabil_main kindofService\">
                          ";
        // line 84
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 85
            echo "                            <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                              <div class=\"child_pho_table_left_inner\" >
                                  ";
            // line 87
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                              </div>
                            </div>
                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "                          ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array())) % 2 == 1)) {
            // line 92
            echo "                            <div class=\"child_pho_table_right innertext2\">
                               <div class=\"child_pho_table_left_inner\" ></div>
                           </div>
                          ";
        }
        // line 96
        echo "                          <div class=\"error\" id=\"kind_service\"></div>
                      </div>
                      <div class=\"photo_contunue_btn proceed\">
                          <a href=\"javascript:void(0);\" id=\"continue1\">CONTINUE</a>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        ";
        // line 106
        echo "        <div id=\"step2\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 2 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form eventPlaned\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/plan.png"), "html", null, true);
        echo "\" alt=\"Plan\">
                  </div>
                  <div class=\"inner_photo_one_form_table software\">
                      <h3>When is your event planned?</h3>
                      ";
        // line 146
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_planed", array()), 'widget', array("attr" => array("class" => "event")));
        echo "
                  </div>
                  <div class=\"error\" id=\"event_planed\"></div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/station.png"), "html", null, true);
        echo "\" alt=\"Station\">
                  </div>
                  <div class=\"inner_photo_one_form_table liveStation\">
                      <h3>Would you require a Live Station?</h3>
                      <div class=\"child_pro_tabil_main\">
                          <div class=\"child_pho_table_left\">
                             <div class=\"child_pho_table_left_inner\">
                                 ";
        // line 161
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "live_station", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "live1")));
        echo "
                                    ";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "live_station", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                             </div>
                          </div>
                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_right_inner \">
                                 ";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "live_station", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "live1")));
        echo "
                                    ";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "live_station", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                             </div>
                          </div>
                          <div class=\"error\" id=\"cat_livestation\"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/meal.png"), "html", null, true);
        echo "\" alt=\"Meal\">
              </div>
              <div class=\"inner_photo_one_form_table mealtype\">
                  <h3>What type of meal would you like?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 186
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 190
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner_text\">
                             ";
        // line 194
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 4, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 195
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 4, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 205
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner_text\">
                             ";
        // line 208
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mealtype", array()), "children", array()), 5, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 209
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "meal_other", array()), 'widget', array("attr" => array("class" => "meal")));
        echo "
                              <h5>(max 30 characters)</h5>
                         </div>
                      </div>
                      <div class=\"error\" id=\"cat_meal\"></div>
                  </div>
              </div>
            </div>
          </div>
          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/beverage.png"), "html", null, true);
        echo "\" alt=\"Beverage\">
              </div>
              <div class=\"inner_photo_one_form_table beverages\">
                  <h3>Would you like beverages included?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 228
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "beverages", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "beverages")));
        echo "
                             ";
        // line 229
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "beverages", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                            ";
        // line 234
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "beverages", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "beverages")));
        echo "
                            ";
        // line 235
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "beverages", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"error\" id=\"cat_beverages\"></div>
                  </div>
              </div>
            </div>
          </div>
          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/beverages.png"), "html", null, true);
        echo "\" alt=\"beverages Icon\">
              </div>
              <div class=\"inner_photo_one_form_table dessert\">
                  <h3>Would you like dessert included?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                          ";
        // line 253
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dessert", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "desert")));
        echo "
                          ";
        // line 254
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dessert", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                            ";
        // line 259
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dessert", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "desert")));
        echo "
                            ";
        // line 260
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dessert", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                  <div class=\"error\" id=\"cat_dessert\"></div>
                  </div>
              </div>
            </div>
          </div>
          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.png"), "html", null, true);
        echo "\" alt=\"Support\">
              </div>
              <div class=\"inner_photo_one_form_table supportStaff\">
                  <h3>What type of support staff would you like?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                            ";
        // line 278
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "cat_support")));
        echo "
                            ";
        // line 279
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                            ";
        // line 282
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "cat_support")));
        echo "
                                ";
        // line 283
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                            ";
        // line 288
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "cat_support")));
        echo "
                            ";
        // line 289
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 292
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff_other", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "cat_radsupport")));
        echo "
                              ";
        // line 293
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "support_staff_other", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                    <div class=\"error\" id=\"cat_support\"></div>
                  </div>
              </div>
            </div>
          </div>
          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/cuisine.png"), "html", null, true);
        echo "\" alt=\"Cuisine\">
              </div>
              <div class=\"inner_photo_one_form_table cuisine\">
                  <h3>What type of cuisine would you like served?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 322
        echo "                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                            ";
        // line 324
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "cuisine")));
        echo "
                                ";
        // line 325
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner_text\">
                            ";
        // line 328
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "cuisine")));
        echo "
                                ";
        // line 329
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                            ";
        // line 332
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "cuisine")));
        echo "
                                ";
        // line 333
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                            ";
        // line 338
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "cuisine")));
        echo "
                                ";
        // line 339
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner_text\">
                            ";
        // line 342
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine", array()), "children", array()), 4, array(), "array"), 'widget', array("attr" => array("class" => "cuisine")));
        echo "
                            ";
        // line 343
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cuisine_other", array()), 'widget', array("attr" => array("class" => "cuisine")));
        echo "
                            <h5>(max 30 characters)</h5>
                         </div>
                         <div id=\"child-empty-box\" class=\"child_pho_table_right_inner innertext2\" >
                         </div>
                      </div>
                    <div class=\"error\" id=\"cat_cuisine\"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/expecting.png"), "html", null, true);
        echo "\" alt=\"Expecting\">
              </div>
              <div class=\"inner_photo_one_form_table guests\">
                  <h3>How many guests are you expecting?</h3>
                  <div class=\"child_pro_tabil_main\">
                         ";
        // line 363
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "guests", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["guest"]) {
            // line 364
            echo "                          <div class=\"child_accounting_three1_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                            ";
            // line 365
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["guest"], 'widget', array("attr" => array("class" => "guest")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["guest"], 'label');
            echo "
                          </div>
                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['guest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 368
        echo "


                      ";
        // line 402
        echo "                      <div class=\"error\" id=\"cat_guests\"></div>
                  </div>
              </div>
                <div class=\"inner_photo_one_form_table\">
                      <div class=\"proceed\">
                          <a href=\"javascript:void(0)\" id=\"continue2\" class=\"button3\"><button type=\"button\">CONTINUE</button></a>
                      </div>
                      <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback1\">BACK</a></div>
                   </div>
                  </div>
              </div>
          </div>
        </div>
        ";
        // line 416
        echo "
         ";
        // line 418
        echo "        <div id=\"step3\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                  <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 3 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 451
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 452
            echo "          <div class=\"photo_form_main\">
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form name\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 456
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/name.png"), "html", null, true);
            echo "\" alt=\"Name\">
                  </div>
                  <div class=\"inner_photo_one_form_table\" >
                      <h3>";
            // line 459
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h3>
                          ";
            // line 460
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "form-input name")));
            echo "
                      <div class=\"error\" id=\"name\" ></div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form  email\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 468
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/email.png"), "html", null, true);
            echo "\" alt=\"Email\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 471
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo " </h3>
                     ";
            // line 472
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-input email_id")));
            echo "
                     <div class=\"error\" id=\"email\" ></div>
                  </div>
              </div>
          </div>
          ";
        }
        // line 478
        echo "          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form location\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 481
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location.png"), "html", null, true);
        echo "\" alt=\"Location\">
                  </div>
                  <div class=\"inner_photo_one_form_table res2\" >
                      <h3>Select your Location*</h3>
                      <div class=\"child_pro_tabil_main res21\">
                          <div class=\"child_accounting_three_left\">
                              ";
        // line 487
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => " land_location")));
        echo "
                                  <p>";
        // line 488
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"child_accounting_three_right\">
                             ";
        // line 491
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "land_location")));
        echo "
                                  <p>";
        // line 492
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"error\" id=\"location\"></div>
                      </div>
                  </div>
              </div>
          </div>


              ";
        // line 501
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            echo " <div class=\"photo_one_form for-uae \">
              <div class=\"inner_photo_one_form mask domcity_cont\" id=\"domCity\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
            // line 505
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <h3>";
            // line 508
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h3>
                    <div class=\"child_pro_tabil_main city_cont\">
                      ";
            // line 510
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "")));
            echo "
                    </div>
                  </div>
                </div>
                <div class=\"error\" id=\"acc_city\"></div>
              </div>
              ";
        }
        // line 517
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 518
            echo "              <div class=\"inner_photo_one_form mask\" id=\"domArea\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 520
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location icon.png"), "html", null, true);
            echo "\" alt=\"Location Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 523
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h3>
                     ";
            // line 524
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_area\"></div>
              </div>
              ";
        }
        // line 529
        echo "
              ";
        // line 530
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 531
            echo "              <div class=\"mask\" id=\"domContact\" >
                <div class=\"inner_photo_one_form\" style=\"margin-top: 40px;\" id=\"uae_mobileno\">
                    <div class=\"inner_photo_one_form_img\">
                        <img src=\"";
            // line 534
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                    </div>
                    <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                        <h3>";
            // line 537
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h3>


                        <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                        ";
            // line 541
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
            echo "
                        ";
            // line 542
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                    </div>
                    <div class=\"error\" id=\"acc_mobile\"></div>
                </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top: 40px;\" >
                        <div class=\"inner_photo_one_form_img\">
                            <img src=\"";
            // line 549
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                        </div>
                        <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                            <h3>";
            // line 552
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h3>


                             <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                            ";
            // line 556
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
            echo "
                            ";
            // line 557
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                        </div>
                    </div>
              </div></div>
                    ";
        }
        // line 563
        echo "

            <div class=\"photo_one_form for-foreign\">
              ";
        // line 566
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 567
            echo "              <div class=\"inner_photo_one_form mask intCountry_cont\" id=\"intCountry\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 569
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/international icon.png"), "html", null, true);
            echo "\" alt=\"international Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 572
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h3>
                     ";
            // line 573
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcountry\"></div>
              </div>
              <div class=\"inner_photo_one_form mask\" id=\"intCity\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 579
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 582
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h3>
                      ";
            // line 583
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcity\"></div>
              </div>
              ";
        }
        // line 588
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 589
            echo "              <div class=\"mask\" id=\"intContact\">
                <div class=\"inner_photo_one_form\" style=\"margin-top:40px;\" id=\"int_mobileno\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 592
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 595
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 597
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget');
            echo "
                      ";
            // line 598
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intmobile\"></div>
              </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 604
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 607
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 609
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget');
            echo "
                      ";
            // line 610
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
              </div>
             </div>
              ";
        }
        // line 615
        echo "            </div>

            <div class=\"photo_one_form\" id=\"hireBlock\">
              <div class=\"inner_photo_one_form hire_cont\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 621
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/looking to hire.png"), "html", null, true);
        echo "\" alt=\"Looking To Hire\">
                  </div>
                  <div class=\"inner_photo_one_form_table resp1\">
                    <h3>When are you looking to hire?*</h3>
                    <div class=\"child_pro_tabil_main  \">
                      ";
        // line 626
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                  </div>
                </div>
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 631
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/information.png"), "html", null, true);
        echo "\" alt=\"Information\">
                </div>
                <div class=\"inner_photo_one_form_table resp1\">
                    <h3>Is there any other additional information you would like to provide?</h3>
                    ";
        // line 635
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "
                     <br> <h5 class=\"char\">(max 120 characters)</h5>
                </div>
                ";
        // line 645
        echo "                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue3\" class=\"button3\">";
        // line 647
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "CONTINUE", "attr" => array("class" => "")));
        echo "</a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback2\">BACK</a></div>
                </div>
              </div>
          </div>
        </div>
        </script></div>
        ";
        // line 656
        echo "        ";
        // line 657
        echo "        <div id=\"step4\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <p>Page 4 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <h3 class=\"thankyou-heading\"><b>Thank you, you’re done!</b></h3>
              <p class=\"thankyou-content\" id=\"enquiryResult\">Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!</p>
              <p class=\"thankyou-content\">If you thought BusinessBid was useful to you, why not share the love? We’d greatly appreciate it. Like us on
              Facebook or follow us on Twitter to to keep updated on all the news and best deals.</p>
              <div class=\"social-icon\">
                <a href=\"https://twitter.com/\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/twitter.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Twitter Bird\"/></a>
                <a href=\"https://www.facebook.com/\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Facebook Sign\" /></a>
              </div>
              <div id=\"thankyou-btn\"  class=\"inner_photo_one_form\">
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 701
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">LOG ANOTHER REQUEST</a>
              </div>
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 704
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" >CONTINUE TO HOMEPAGE</a>
              </div>
              </div>
          </div>
        </div>
        ";
        // line 710
        echo "        ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
</div>
</div>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script type=\"text/javascript\">
    function scrollBox (tag) {
        \$('html,body').animate({scrollTop: tag.position().top -10},'slow');
    }
    \$(document).ready(function () {
        var forms = [
            '[ name=\"";
        // line 725
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
          ];
        var validationFlag = true;
          \$( forms.join(',') ).submit( function( e ){
            e.preventDefault();
            console.log(\"tried submitting\")
            if(validationFlag) {
                postForm( \$(this), function( response ){
                    if(response.hasOwnProperty('error')) {
                        alert(response.error);
                        var i = 1;
                        for (i = 1; i < 5; i++) {
                            \$(\"div#step\"+i).hide();
                        };
                        \$(\"div#step\"+response.step).show();
                    } else {
                        var form=document.createElement(\"form\");form.setAttribute(\"method\",\"post\"),form.setAttribute(\"action\",'";
        // line 741
        echo $this->env->getExtension('routing')->getPath("bizbids_job_post_sucessful");
        echo "');var hiddenField=document.createElement(\"input\");hiddenField.setAttribute(\"name\",\"jobno\"),hiddenField.setAttribute(\"value\",response.jobNo),hiddenField.setAttribute(\"type\",\"hidden\"),form.appendChild(hiddenField),document.body.appendChild(form),form.submit();
                        \$(\"#category_form\")[0].reset();
                    }
                });
                ga('send', 'event', 'Button', 'Click', 'Form Sent');
            }
            return false;
          });
        function postForm( \$form, callback ) {
            NProgress.start();
              \$.ajax({
                type        : \$form.attr( 'method' ),
                beforeSend  : function() { NProgress.inc() },
                url         : \$form.attr( 'action' ),
                data        : \$form.serialize(),
                success     : function(data) {
                  callback( data );
                  NProgress.done();
                },
                error : function (xhr) {
                    alert(\"Error occured.please try again\");
                    NProgress.done();
                }
              });
        }
    })
</script>
<script type=\"text/javascript\" src=\"";
        // line 768
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 769
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script type=\"text/javascript\" src=\"";
        // line 770
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("js/reviewsformsvalidation/" . (isset($context["twigFileName"]) ? $context["twigFileName"] : null)) . ".min.js")), "html", null, true);
        echo "\" ></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:catering_services_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1249 => 770,  1245 => 769,  1241 => 768,  1211 => 741,  1192 => 725,  1173 => 710,  1165 => 704,  1159 => 701,  1152 => 697,  1148 => 696,  1107 => 657,  1105 => 656,  1094 => 647,  1090 => 645,  1084 => 635,  1077 => 631,  1069 => 626,  1061 => 621,  1053 => 615,  1045 => 610,  1041 => 609,  1036 => 607,  1030 => 604,  1021 => 598,  1017 => 597,  1012 => 595,  1006 => 592,  1001 => 589,  998 => 588,  990 => 583,  986 => 582,  980 => 579,  971 => 573,  967 => 572,  961 => 569,  957 => 567,  955 => 566,  950 => 563,  941 => 557,  937 => 556,  930 => 552,  924 => 549,  914 => 542,  910 => 541,  903 => 537,  897 => 534,  892 => 531,  890 => 530,  887 => 529,  879 => 524,  875 => 523,  869 => 520,  865 => 518,  862 => 517,  852 => 510,  847 => 508,  841 => 505,  834 => 501,  822 => 492,  818 => 491,  812 => 488,  808 => 487,  799 => 481,  794 => 478,  785 => 472,  781 => 471,  775 => 468,  764 => 460,  760 => 459,  754 => 456,  748 => 452,  746 => 451,  711 => 418,  708 => 416,  693 => 402,  688 => 368,  669 => 365,  664 => 364,  647 => 363,  639 => 358,  621 => 343,  617 => 342,  611 => 339,  607 => 338,  599 => 333,  595 => 332,  589 => 329,  585 => 328,  579 => 325,  575 => 324,  571 => 322,  563 => 304,  549 => 293,  545 => 292,  539 => 289,  535 => 288,  527 => 283,  523 => 282,  517 => 279,  513 => 278,  503 => 271,  489 => 260,  485 => 259,  477 => 254,  473 => 253,  463 => 246,  449 => 235,  445 => 234,  437 => 229,  433 => 228,  423 => 221,  408 => 209,  404 => 208,  398 => 205,  394 => 204,  388 => 201,  384 => 200,  376 => 195,  372 => 194,  366 => 191,  362 => 190,  356 => 187,  352 => 186,  342 => 179,  328 => 168,  324 => 167,  316 => 162,  312 => 161,  302 => 154,  291 => 146,  284 => 142,  246 => 106,  235 => 96,  229 => 92,  226 => 91,  206 => 87,  200 => 85,  183 => 84,  175 => 79,  135 => 42,  125 => 34,  116 => 31,  113 => 30,  108 => 29,  99 => 26,  96 => 25,  92 => 24,  89 => 23,  80 => 20,  77 => 19,  73 => 18,  67 => 15,  63 => 14,  56 => 9,  53 => 8,  48 => 5,  45 => 4,  37 => 3,  31 => 2,  11 => 1,);
    }
}

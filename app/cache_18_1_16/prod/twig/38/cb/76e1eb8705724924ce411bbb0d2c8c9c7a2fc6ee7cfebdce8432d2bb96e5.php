<?php

/* BBidsBBidsHomeBundle:Admin:vendor_profile_edit.html.twig */
class __TwigTemplate_38cb76e1eb8705724924ce411bbb0d2c8c9c7a2fc6ee7cfebdce8432d2bb96e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:vendor_profile_edit.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "
<div class=\"container\">
<div class=\"page-title\"><h1>Edit Information</h1></div>
<div class=\"edit-user-block\">

<div class=\"col-md-12 side-clear\">
    ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 10
            echo "
    <div class=\"alert alert-danger\">";
            // line 11
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "
    ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 16
            echo "
    <div class=\"alert alert-success\">";
            // line 17
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
    ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 22
            echo "
    <div class=\"alert alert-success\">";
            // line 23
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "

    ";
        // line 28
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
        ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
\t<div>
\t\t<div>";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "updateemailid", array()), 'errors');
        echo " </div>
\t\t<div>";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "smsphone", array()), 'errors');
        echo " </div>
\t\t<div>";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'errors');
        echo " </div>
    </div>

\t<div class=\"container side-clear\">
\t\t<div class=\"col-sm-8 edit-form-vendor clear-left\">
\t\t\t<div class=\"col-sm-12\"><label>User Email ID </label><span> ";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["editemail"]) ? $context["editemail"] : null), "html", null, true);
        echo "</span></div>
\t\t\t<div class=\"col-sm-6 med-top-space\"><div class=\"field-label\">";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "updateemailid", array()), 'label');
        echo " <span class=\"req\" id=\"emailexists\"></span></div>";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "updateemailid", array()), 'widget');
        echo "</div>
\t\t\t<div class=\"col-sm-6 med-top-space\"><div class=\"field-label\">";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
        echo "</div> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget');
        echo "</div>
\t\t\t<div class=\"col-sm-6 med-top-space\"><div class=\"field-label\">";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
        echo "<span class=\"req\">*</span></div> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "smsphone", array()), 'widget');
        echo "</div>
\t\t\t<div class=\"col-sm-6 med-top-space\"><div class=\"field-label\">";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
        echo "  <span class=\"req\">*</span>:</div><div class=\"area-code\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
        echo " </div> <div class=\"phone-no\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget');
        echo "</div></div>
        \t<div class=\"col-sm-6 med-top-space\">";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "bizname", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "bizname", array()), 'widget');
        echo "</div>
\t\t\t<div class=\"col-sm-6 med-top-space\">";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax", array()), 'widget');
        echo " </div>
            <div class=\"col-sm-6 med-top-space\">";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address", array()), 'widget');
        echo "</div>
\t\t\t<div class=\"col-sm-6 med-top-space\">";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "</div>
        \t<div class=\"col-sm-6 med-top-space\">";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status", array()), 'widget');
        echo " </div>

            <div class=\"col-sm-10 med-top-space\">";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
            ";
        // line 50
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t\t\t</div>

\t\t\t<div class=\"col-sm-4\">
\t\t\t";
        // line 55
        echo "\t\t\t\t<div class=\"profile-name-edit-wrapper\">
\t\t\t\t<label>Vendor Logo</label>
\t\t\t\t<div class=\"profile-name-edit\">


\t\t\t\t";
        // line 60
        if ( !twig_test_empty((isset($context["logoObject"]) ? $context["logoObject"] : null))) {
            // line 61
            echo "                    ";
            $context["logopath"] = $this->env->getExtension('assets')->getAssetUrl(("logo/" . $this->getAttribute((isset($context["logoObject"]) ? $context["logoObject"] : null), "fileName", array())));
            // line 62
            echo "\t\t\t\t\t<div class=\"profile-picture\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["logopath"]) ? $context["logopath"] : null), "html", null, true);
            echo "\" >
\t\t\t\t\t\t<a class=\"change-logo\" href=\"#\" id=\"changeimage\">Change Logo</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"logo-form\" style=\"display:none;\">
\t\t\t\t";
            // line 66
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["logoform"]) ? $context["logoform"] : null), 'form_start');
            echo "
\t\t\t\t<div class=\"col-md-4\">";
            // line 67
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["logoform"]) ? $context["logoform"] : null), "image", array()), 'widget');
            echo "</div>
\t\t\t\t<div class=\"col-md-5\">";
            // line 68
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["logoform"]) ? $context["logoform"] : null), "Upload", array()), 'widget');
            echo "</div>
\t\t\t\t";
            // line 69
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["logoform"]) ? $context["logoform"] : null), 'form_end');
            echo "
\t\t\t\t</div>
\t\t\t\t";
        } else {
            // line 72
            echo "\t\t\t\t\t<div id=\"logo-form\" >
\t\t\t\t\t\t";
            // line 73
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["logoform"]) ? $context["logoform"] : null), 'form_start');
            echo "
\t\t\t\t\t\t<div class=\"col-md-12\">";
            // line 74
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["logoform"]) ? $context["logoform"] : null), "image", array()), 'widget');
            echo "</div>
\t\t\t\t\t\t<div class=\"col-md-12\">";
            // line 75
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["logoform"]) ? $context["logoform"] : null), "Upload", array()), 'widget');
            echo "</div>
\t\t\t\t\t\t";
            // line 76
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["logoform"]) ? $context["logoform"] : null), 'form_end');
            echo "
\t\t\t\t\t</div>
\t\t\t\t";
        }
        // line 79
        echo "\t\t\t</div>
\t\t\t";
        // line 81
        echo "\t\t\t</div>

            <div class=\"profile-name-edit-wrapper\">
                <label>Vendor Test Account Status</label>
                <div class=\"profile-name-edit\">
                    ";
        // line 86
        if (((null === (isset($context["testAccount"]) ? $context["testAccount"] : null)) || ((isset($context["testAccount"]) ? $context["testAccount"] : null) == "No"))) {
            // line 87
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("bizbids_vendor_test_account_toggle", array("vid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "status" => "Yes")), "html", null, true);
            echo "\" class=\"btn btn-warning\">Enable Test Account</a>
                    ";
        } else {
            // line 89
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("bizbids_vendor_test_account_toggle", array("vid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "status" => "No")), "html", null, true);
            echo "\" class=\"btn btn-danger\">Disable Test Account</a>
                    ";
        }
        // line 91
        echo "                </div>
            </div>

\t\t\t</div>
\t\t\t</div>
</div>


<div class=\"col-sm-12 side-clear\">
    ";
        // line 101
        echo "    <div class=\"add-certificate col-sm-4\">
        <h2>Add Vendor Certifications</h2>
        <div class=\"add-certificate-block certificates\">
            <form name=\"form\" method=\"post\" action=\"";
        // line 104
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_save_certification");
        echo "\" onsubmit=\"return certificateValidate();\" enctype=\"multipart/form-data\">
\t\t\t\t<label>Certificate Name</label>
                <span class=\"file-upload-wrapper\">
                    <input type=\"text\" id=\"form_certification\" name=\"certification[]\">
                    ";
        // line 109
        echo "                </span>
                <div id=\"morecertificates\"></div>
                <div class=\"pull-right\">
                    <a class=\"add_certificate_button\"> add more certificates <span class=\"glyphicon glyphicon-plus\"></span></a>
                </div>
                <button type=\"submit\" id=\"form_save\" name=\"form[save]\">Add Certificate</button>
                <input type=\"hidden\" name=\"vendor\" value=\"";
        // line 115
        echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
        echo "\">
            </form>
        </div>

\t\t";
        // line 120
        echo "\t\t<div class=\"list-certificate\">
\t\t\t";
        // line 121
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["certifications"]) ? $context["certifications"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 122
            echo "\t\t\t\t<p><button type=\"button\" class=\"close\" title=\"Delete\"  aria-hidden=\"true\" onclick = \"return deleteEntity('certification','";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\" >&times;</button> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "certification", array()), "html", null, true);
            echo "</p>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "\t\t</div>
    </div>
    ";
        // line 127
        echo "
    ";
        // line 129
        echo "    <div class=\"add-certificate col-sm-4\">
        <h2>Add Your Products</h2>
        <form name=\"form\" method=\"post\" action=\"";
        // line 131
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_save_product");
        echo "\" onsubmit=\"return productValidate();\">
            <div class=\"add-certificate-block products\">
\t\t\t\t<label>Product Name</label>
                <span class=\"file-upload-wrapper\">
                    <input type=\"text\" id=\"form_product\" name=\"product[]\">
                </span>
                <div id=\"moreproducts\"></div>
                <div class=\"pull-right\"><a class=\"add_certificate_button add_products_button\"> add more products <span class=\"glyphicon glyphicon-plus\"></span></a></div>
                <button type=\"submit\" id=\"form_save\" name=\"form[save]\" >Product &amp; Services</button>
            </div>
            <input type=\"hidden\" name=\"vendor\" value=\"";
        // line 141
        echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
        echo "\">
        </form>
\t\t<div class=\"col-sm-12 side-clear\">
\t\t\t";
        // line 145
        echo "\t\t\t\t<div class=\"tab-pane\">
\t\t\t\t\t<div class=\"list-product\">
\t\t\t\t\t\t<div class=\"list-certificate\">
\t\t\t\t\t\t\t";
        // line 148
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 149
            echo "\t\t\t\t\t\t\t\t<p><button type=\"button\" title=\"Delete\" class=\"close\"  aria-hidden=\"true\" onclick = \"return deleteEntity('product','";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\" >&times;</button>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "product", array()), "html", null, true);
            echo " </p>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t</div>

    </div>
    ";
        // line 158
        echo "
    ";
        // line 160
        echo "    <div class=\"tab-pane  col-sm-4\" id=\"license\">
        <div class=\"add-certificate\">
            <h2>Add Vendor Business License</h2>
            <div class=\"newalbum add-certificate-block\">
                ";
        // line 164
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["licenseform"]) ? $context["licenseform"] : null), 'form_start');
        echo "
                ";
        // line 165
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "expirydate", array()), 'label');
        echo "
\t\t\t\t<div class=\"license-upload-wrapper\">
\t\t\t\t";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "expirydate", array()), 'widget', array("id" => "datepicker3"));
        echo "
                <div class=\"business-license-upload\">";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "licensefile", array()), 'widget');
        echo "</div>
\t\t\t\t</div>
                ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "save", array()), 'widget');
        echo "
                ";
        // line 171
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["licenseform"]) ? $context["licenseform"] : null), 'form_end');
        echo "
            </div>
        </div>

\t\t<div class=\"list-certificate\">
            ";
        // line 176
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["expLicense"]) ? $context["expLicense"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 177
            echo "\t\t\t<p>
\t\t\t\t<span><a href=\"";
            // line 178
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/licenses"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "file", array()), "html", null, true);
            echo "\">Click here to download the license</a></span>
\t\t\t\t<span><button type=\"button\" title=\"Delete\" class=\"close\"  aria-hidden=\"true\" onclick = \"return deleteEntity('license','";
            // line 179
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\" >&times;</button><span class=\"Expiry\">License Expiry Date : ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["e"], "expdate", array()), "Y-m-d"), "html", null, true);
            echo "</span></span>
\t\t\t</p>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 182
        echo "        </div>

    </div>
    ";
        // line 186
        echo "
</div>

";
        // line 190
        echo "
<div class=\"col-sm-12 side-clear\">
    ";
        // line 193
        echo "    <div class=\"add-certificate col-sm-4\">
        <h2>Add Vendor Album</h2>
        <div class=\"add-certificate-block certificates vendor-album\">
            <div class=\"license-upload-wrapper\">
            ";
        // line 197
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["albumform"]) ? $context["albumform"] : null), 'form_start', array("attr" => array("onsubmit" => "return albumValidate();")));
        echo "
                ";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumname", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumname", array()), 'widget');
        echo "
               ";
        // line 200
        echo "                <div class=\"business-license-upload\">
                    ";
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumimages", array()), 'widget', array("full_name" => ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumimages", array()), "vars", array()), "full_name", array()) . "[]")));
        echo "
                </div>
                ";
        // line 203
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "save", array()), 'widget');
        echo "
            ";
        // line 204
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["albumform"]) ? $context["albumform"] : null), 'form_end');
        echo "
            </div>
        </div>

        ";
        // line 209
        echo "        <div class=\"list-certificate\">
            ";
        // line 210
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 211
            echo "            ";
            $context["albumid"] = $this->getAttribute($context["a"], "albumid", array());
            // line 212
            echo "            ";
            $context["defaultphotoid"] = $this->getAttribute($context["a"], "defaultphotopath", array());
            // line 213
            echo "            ";
            $context["imagepath"] = $this->env->getExtension('assets')->getAssetUrl((((("gallery/" . (isset($context["albumid"]) ? $context["albumid"] : null)) . "/") . (isset($context["albumid"]) ? $context["albumid"] : null)) . "1"));
            // line 214
            echo "            <div class=\"photo\">
                <button type=\"button\" title=\"Delete Album\" class=\"close\" onclick = \"return deleteEntity('album','";
            // line 215
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\">&times;</button>
                <a target=\"_blank\" href=\"";
            // line 216
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_album_view", array("albumid" => (isset($context["albumid"]) ? $context["albumid"] : null))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["imagepath"]) ? $context["imagepath"] : null), "html", null, true);
            echo "\" width=\"90px\"></a>
                <p>";
            // line 217
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "albumname", array()), "html", null, true);
            echo " </p>";
            // line 218
            echo "            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 220
        echo "        </div>
    </div>
    ";
        // line 223
        echo "
    ";
        // line 225
        echo "    <div class=\"add-certificate col-sm-4\">
        <h2>Add Your City</h2>
        ";
        // line 227
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cityform"]) ? $context["cityform"] : null), 'form_start', array("attr" => array("class" => "city_form")));
        echo "
    <ul>
        ";
        // line 229
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["cityform"]) ? $context["cityform"] : null), "city", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 230
            echo "            <li class=\"city-field-item\">
                ";
            // line 231
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["a"], 'widget');
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["a"], 'label');
            echo "
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 234
        echo "            ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cityform"]) ? $context["cityform"] : null), "save", array()), 'widget', array("attr" => array("class" => "save_city")));
        echo "
        </ul>
    ";
        // line 236
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cityform"]) ? $context["cityform"] : null), 'form_end');
        echo "
    </div>
    ";
        // line 239
        echo "
    ";
        // line 241
        echo "    <div class=\"tab-pane  col-sm-4\" id=\"categories\">
        <div class=\"add-certificate\">
            <h2>Add Vendor Services Provided</h2>
            <div class=\"newalbum add-certificate-block subcategories\">
               <form action=\"";
        // line 245
        echo $this->env->getExtension('routing')->getPath("bizbids_addcat_port");
        echo "\" method=\"post\">
                    <div class=\"morecertificates subcatadd\">
                        <input type=\"text\" id=\"form_cat\" name=\"cat[]\" placeholder=\"Enter your category\">
                    </div>
                    <a id=\"addform1\" class=\"btn btn-success btn-getquotes text-center\">Add</a>
                    <div id=\"moreproducts1\">
                    </div>
                    <input type=\"hidden\" id=\"vendorid\" name=\"vendorid\" value=\"";
        // line 252
        echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
        echo "\">
                    <input type=\"hidden\" id=\"setdata\" value=\"0\">
                    <input type=\"submit\" id=\"savecat\" name=\"form[save]\" value=\"Save\" class=\"btn btn-success btn-getquotes text-center\">
                </form>
            </div>
        </div>

        <div class=\"list-certificate\">
            ";
        // line 260
        if ( !twig_test_empty((isset($context["displaycat"]) ? $context["displaycat"] : null))) {
            // line 261
            echo "            <div class=\"field-item1\"><label>Service Portfolio</label>
                ";
            // line 262
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["displaycat"]) ? $context["displaycat"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                // line 263
                echo "                <div class=\"main-cat\">
                    <div class=\"field-value editcategory-port\" id=\"";
                // line 264
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\">
                        <span class=\"catname\">";
                // line 265
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "category", array()), "html", null, true);
                echo " </span>
                    </div>
                    <span id=\"editdt\" >
                        <input id=\"vendid\" type=\"hidden\" value=\"";
                // line 268
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\">
                        ";
                // line 270
                echo "                        <span class=\"deletecatname-port\"><a href=\"#\" onclick = \"return deleteEntity('category','";
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "','";
                echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
                echo "');\" >Delete</a> </span>
                    </span>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 274
            echo "            </div>
        ";
        }
        // line 276
        echo "        </div>

    </div>
    ";
        // line 280
        echo "
</div>
";
        // line 283
        echo "
<div>
    ";
        // line 289
        echo "</div>
<script src=\"";
        // line 290
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
var certificationDelUrl = \"";
        // line 292
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_certification", array("id" => "cid", "vid" => "vid")), "html", null, true);
        echo "\", productDelUrl = \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_product", array("id" => "pid", "vid" => "vid")), "html", null, true);
        echo "\", licenseDelUrl = \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_lisence", array("id" => "eid", "vid" => "vid")), "html", null, true);
        echo "\", catDelUrl = \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("bizbids_deletecat_port", array("id" => "eid", "vid" => "vid")), "html", null, true);
        echo "\", albumDelUrl = \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_album", array("id" => "eid", "vid" => "vid")), "html", null, true);
        echo "\";
</script>
<script src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/vendor.profile.edit.beautified.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:vendor_profile_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  702 => 294,  689 => 292,  684 => 290,  681 => 289,  677 => 283,  673 => 280,  668 => 276,  664 => 274,  651 => 270,  647 => 268,  641 => 265,  637 => 264,  634 => 263,  630 => 262,  627 => 261,  625 => 260,  614 => 252,  604 => 245,  598 => 241,  595 => 239,  590 => 236,  584 => 234,  574 => 231,  571 => 230,  567 => 229,  562 => 227,  558 => 225,  555 => 223,  551 => 220,  544 => 218,  541 => 217,  535 => 216,  529 => 215,  526 => 214,  523 => 213,  520 => 212,  517 => 211,  513 => 210,  510 => 209,  503 => 204,  499 => 203,  494 => 201,  491 => 200,  485 => 198,  481 => 197,  475 => 193,  471 => 190,  466 => 186,  461 => 182,  448 => 179,  442 => 178,  439 => 177,  435 => 176,  427 => 171,  423 => 170,  418 => 168,  414 => 167,  409 => 165,  405 => 164,  399 => 160,  396 => 158,  388 => 151,  375 => 149,  371 => 148,  366 => 145,  360 => 141,  347 => 131,  343 => 129,  340 => 127,  336 => 124,  323 => 122,  319 => 121,  316 => 120,  309 => 115,  301 => 109,  294 => 104,  289 => 101,  278 => 91,  272 => 89,  266 => 87,  264 => 86,  257 => 81,  254 => 79,  248 => 76,  244 => 75,  240 => 74,  236 => 73,  233 => 72,  227 => 69,  223 => 68,  219 => 67,  215 => 66,  207 => 62,  204 => 61,  202 => 60,  195 => 55,  188 => 50,  184 => 49,  177 => 47,  171 => 46,  165 => 45,  159 => 44,  153 => 43,  145 => 42,  138 => 41,  132 => 40,  126 => 39,  122 => 38,  114 => 33,  110 => 32,  106 => 31,  101 => 29,  97 => 28,  93 => 26,  84 => 23,  81 => 22,  77 => 21,  74 => 20,  65 => 17,  62 => 16,  58 => 15,  55 => 14,  46 => 11,  43 => 10,  39 => 9,  31 => 3,  28 => 2,  11 => 1,);
    }
}

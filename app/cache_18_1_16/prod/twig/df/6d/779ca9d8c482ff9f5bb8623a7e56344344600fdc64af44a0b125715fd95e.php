<?php

/* BBidsBBidsHomeBundle:User:leadsviewhistory.html.twig */
class __TwigTemplate_df6d779ca9d8c482ff9f5bb8623a7e56344344600fdc64af44a0b125715fd95e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leadsviewhistory.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 5
        echo "<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>View Leads History for Category: ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["cat"]) ? $context["cat"] : null), "html", null, true);
        echo " </h1></div>
<div class=\"row\">
<table class=\"col-md-6 vendor-enquiries\">
<tr>
<th>Job Request <br> Recieved Date</th>
<th>Job Request <br> Number</th>
<th>Subject</th>
<th>Job Request <br> Accepted Date</th>
<th>Contact Name</th>
<th>Leads Deducted</th>
</tr>

";
        // line 19
        if ( !twig_test_empty((isset($context["leadsviews"]) ? $context["leadsviews"] : null))) {
            // line 20
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["leadsviews"]) ? $context["leadsviews"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
                // line 21
                echo "<tr>
<td class=\"center-align\">";
                // line 22
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "created", array()), "Y-m-d"), "html", null, true);
                echo "</td>
<td class=\"center-align\">";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "id", array()), "html", null, true);
                echo "</td>
<td><a href=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_enquiry_view", array("enquiryid" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "subj", array()), "html", null, true);
                echo "</a></td>

<td class=\"center-align\">";
                // line 26
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "updated", array()), "Y-m-d"), "html", null, true);
                echo "</td>
<td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "contactname", array()), "html", null, true);
                echo "</td>
<td class=\"center-align\">";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subcount"]) ? $context["subcount"] : null), ($this->getAttribute($context["loop"], "index", array()) - 1), array(), "array"), "html", null, true);
                echo " Leads Deduducted</td>
</tr>

";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "
\t\t\t
";
        } else {
            // line 35
            echo "<tr><td colspan=\"2\" align=\"center\">No Data Available</td></tr>
";
        }
        // line 37
        echo "

</table>

";
        // line 41
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:User:getleadcredit", array("userid" => (isset($context["userid"]) ? $context["userid"] : null))));
        echo "

</div> 
</div> 
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leadsviewhistory.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 41,  124 => 37,  120 => 35,  115 => 32,  97 => 28,  93 => 27,  89 => 26,  82 => 24,  78 => 23,  74 => 22,  71 => 21,  54 => 20,  52 => 19,  37 => 7,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}

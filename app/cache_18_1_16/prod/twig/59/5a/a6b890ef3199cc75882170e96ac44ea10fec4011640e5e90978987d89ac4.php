<?php

/* TicketingTicketTicketBundle:Admin:vendor_ticket_list.html.twig */
class __TwigTemplate_595aa6b890ef3199cc75882170e96ac44ea10fec4011640e5e90978987d89ac4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "TicketingTicketTicketBundle:Admin:vendor_ticket_list.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-1.4.4.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilter.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
\t\$(document).ready( function () {
\t\t\$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
        \$.datepicker.regional[\"\"].changeMonth = true;
        \$.datepicker.regional[\"\"].changeYear = true;
        \$.datepicker.setDefaults(\$.datepicker.regional['']);
\t\t\$('#consumerTicketList').dataTable({
\t\t\t\"bLengthChange\": false,
\t     \t\"iDisplayLength\": 10,
\t     \t\"language\": {
\t     \t\t\"search\": \"_INPUT_\",
\t\t        \"searchPlaceholder\": \"Search records\"
\t\t    },
\t\t    \"sZeroRecords\": \"No Records to dispaly\",
\t\t\t\"sPaginationType\" : 'full_numbers',
\t\t\t\"sDom\": '<\"top\"l>rt<\"bottom\"ip><\"clear\">'
\t\t}).columnFilter({aoColumns:[
\t\t\t\t\t\tnull,
\t\t\t\t\t\t{ type:\"date-range\", sSelector: \"#dateFilter\" },
\t\t\t\t\t\t{ sSelector: \"#ticketidFilter\" },
\t\t\t\t\t\t{ sSelector: \"#subjectFilter\" },
\t\t\t\t\t\t{ type:\"select\", values : [\"Open\", \"Close\", \"Pending\"], sSelector: \"#statusFilter\" }
\t\t\t\t\t\t]}
\t\t\t\t\t);

\t} );
</script>
<div class=\"page-bg\">
\t<div class=\"container\">
\t\t<div class=\"page-title\"><h1>Vendor Tickets</h1></div>
\t\t<div>
\t\t";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 41
            echo "\t\t\t<div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "
\t\t";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 46
            echo "\t\t\t<div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "
\t\t";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 51
            echo "\t\t\t<div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "\t\t</div>
\t\t<div class=\"search_filter consumer-tickets-list-filter\">
\t\t\t<p id=\"ticketidFilter\"></p>
\t\t\t<p id=\"dateFilter\"></p>
\t\t\t<p id=\"subjectFilter\"></p>
\t\t\t<p id=\"statusFilter\"></p>
\t\t</div>
\t\t<div class=\"Create-ticket-form ticket-lists\">

\t\t\t<div class=\"latest-orders\">


\t\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\"consumerTicketList\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t\t\t<th>Ticket ID</th>
\t\t\t\t\t\t\t<th>Subject</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tfoot>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t\t<th>Ticket Id</th>
\t\t\t\t\t\t<th>Subjects</th>
\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 86
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ticketsList"]) ? $context["ticketsList"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 87
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"sl-no\">";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"date-rcvd\">";
            // line 89
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($context["list"], "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"ticket-id\"><a href=\"";
            // line 90
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminview", array("id" => $this->getAttribute($context["list"], "id", array()))), "html", null, true);
            echo "\">#V";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "ticketNo", array()), "html", null, true);
            echo "</a></td>
\t\t\t\t\t\t\t\t<td class=\"subject\">";
            // line 91
            echo twig_escape_filter($this->env, (((twig_length_filter($this->env, $this->getAttribute($context["list"], "ticketSubject", array())) > 50)) ? ((twig_slice($this->env, $this->getAttribute($context["list"], "ticketSubject", array()), 0, 50) . "...")) : ($this->getAttribute($context["list"], "ticketSubject", array()))), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"status\"><span>";
            // line 92
            if (($this->getAttribute($context["list"], "ticketStatus", array()) == 1)) {
                echo "<span class=\"open-status\">Open</span>";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 2)) {
                echo "<span class=\"close-status\">Close</span>";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 3)) {
                echo "}<span class=\"pending-status\">Pending</span>";
            }
            echo "</span></td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div style=\"clear:both; height:10px\"></div>
\t\t</div>
\t\t</div> <!-- container div ends -->

";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:Admin:vendor_ticket_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 95,  207 => 92,  203 => 91,  197 => 90,  193 => 89,  189 => 88,  186 => 87,  169 => 86,  135 => 54,  125 => 51,  121 => 50,  118 => 49,  108 => 46,  104 => 45,  101 => 44,  91 => 41,  87 => 40,  52 => 8,  48 => 7,  44 => 6,  40 => 5,  36 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}

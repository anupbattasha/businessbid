<?php

/* BBidsBBidsHomeBundle:Admin:reviews.html.twig */
class __TwigTemplate_f67e41ed91dfdd3204217d91281b9da5bdf59df3a2a231ecb0560800d7f68ab8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:reviews.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script>
\$(document).ready(function(){
\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

});
</script>

<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\">
<!--<script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>-->
<script src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>
<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
<script>
\$(function() {
\t\$(\"#datepicker3\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true,
\t\tmaxDate: '+2y',
\t\tyearRange: '2014:2024',
\t\tonSelect: function(date){
\t\t\tvar selectedDate = new Date(date);
\t\t\tvar msecsInADay = 86400000;
\t\t\tvar endDate = new Date(selectedDate.getTime() + msecsInADay);

\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"minDate\", endDate );
\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"maxDate\", '+2y' );
\t\t}
\t});

\t\$(\"#datepicker4\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true
\t});

\t\tvar availableVendors = ";
        // line 51
        echo twig_jsonencode_filter((isset($context["accountList"]) ? $context["accountList"] : null));
        echo ";
    \$( \"#form_vendorname\" ).autocomplete({
      source: availableVendors
    });

    var availablecustomers = ";
        // line 56
        echo twig_jsonencode_filter((isset($context["custList"]) ? $context["custList"] : null));
        echo ";
    \$( \"#form_customername\" ).autocomplete({
      source: availablecustomers
    });

});
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Reviews</h1></div>
";
        // line 66
        if (((isset($context["count"]) ? $context["count"] : null) != 0)) {
            // line 67
            echo "<div class=\"row text-right page-counter-history\"><div class=\"counter-box\">Now showing : ";
            echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
            echo " records</div></div>
";
        }
        // line 69
        echo "<div>

\t";
        // line 71
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 72
            echo "
\t<div class=\"alert alert-danger\">";
            // line 73
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "
\t";
        // line 77
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 78
            echo "
\t<div class=\"alert alert-success\">";
            // line 79
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "
\t";
        // line 83
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 84
            echo "
\t<div class=\"alert alert-success\">";
            // line 85
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "<div class=\"admin_filter\">
\t";
        // line 89
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t<div class=\"col-md-2 side-clear\">";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "customername", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "customername", array()), 'widget');
        echo "</div>
\t<div class=\"col-md-2 clear-right\">";
        // line 91
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'widget');
        echo "</div>
\t<div class=\"col-md-2 clear-right\">";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
\t<div class=\"col-md-2 clear-right\" >";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status", array()), 'widget');
        echo "</div>
\t<div class=\"col-md-med\" >";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'widget', array("id" => "datepicker3"));
        echo "</div>
\t<div class=\"col-md-med\">";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'widget', array("id" => "datepicker4"));
        echo " </div>
\t<div class=\"search-block top-space clear-right\"> ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "filter", array()), 'widget', array("label" => "Search"));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
</div>
<div class=\"latest-orders\">
<table>
    <thead>
        <tr>
            <th>Date Received</th>
            <th>Customer Name</th>
            <th>Category</th>
            <th>Vendor Name</th>
            <th>Business Name</th>
            <th>Rating</th>
            <th>Business Recommendation</th>
            <th>Business Engagement</th>
            <th>Review</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
\t";
        // line 115
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 116
            echo "\t<tr>
\t\t<td>";
            // line 117
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</td>

\t\t<td>";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "author", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "category", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "contactname", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "bizname", array()), "html", null, true);
            echo "</td>
\t\t<td><span class=\"stars\">";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "rating", array()), "html", null, true);
            echo "</span></td>
\t\t<td>";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessrecomond", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessknow", array()), "html", null, true);
            echo "</td>
\t\t<td><a href=\"";
            // line 126
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_review", array("reviewid" => $this->getAttribute($context["r"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["r"], "review", array()), 0, 50), "html", null, true);
            echo "...</a></td>
\t\t<td>";
            // line 127
            if (($this->getAttribute($context["r"], "modstatus", array()) == 1)) {
                echo " Published ";
            } elseif (($this->getAttribute($context["r"], "modstatus", array()) == 0)) {
                echo "Not Published ";
            }
            echo "</td>

\t\t<td>";
            // line 129
            if (($this->getAttribute($context["r"], "modstatus", array()) != 1)) {
                echo " <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_publish_review", array("reviewid" => $this->getAttribute($context["r"], "id", array()))), "html", null, true);
                echo "\" onclick=\"return confirm('Are you sure to publish review ?')\">Publish</a> ";
            } else {
                echo " Published ";
            }
            echo "|<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_review_edit", array("reviewid" => $this->getAttribute($context["r"], "id", array()))), "html", null, true);
            echo "\">Edit</a></td>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "</table>
</div>
</div>
";
        // line 134
        if (((isset($context["count"]) ? $context["count"] : null) != 0)) {
            // line 135
            echo "<div class=\"row text-right \">
\t<div class=\"counter-box top-space\">
<div>";
            // line 137
            $context["page_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 10)));
            // line 138
            echo "
<ul class=\"pagination\">

  <li><a href=\"";
            // line 141
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviews", array("offset" => 1));
            echo "\">&laquo;</a></li>
";
            // line 142
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["page_count"]) ? $context["page_count"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 143
                echo "
  <li><a href=\"";
                // line 144
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviews", array("offset" => $context["i"])), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</a></li>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "
  <li><a href=\"";
            // line 148
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviews", array("offset" =>  -(isset($context["page_count"]) ? $context["page_count"] : null))), "html", null, true);
            echo "\">&raquo;</a></li>
</ul>
</div></div>
</div>
";
        } else {
            // line 153
            echo "<div>No Records Available</div>
";
        }
        // line 155
        echo "</div>
</div>



";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  362 => 155,  358 => 153,  350 => 148,  347 => 147,  336 => 144,  333 => 143,  329 => 142,  325 => 141,  320 => 138,  318 => 137,  314 => 135,  312 => 134,  307 => 131,  291 => 129,  282 => 127,  276 => 126,  272 => 125,  268 => 124,  264 => 123,  260 => 122,  256 => 121,  252 => 120,  248 => 119,  243 => 117,  240 => 116,  236 => 115,  212 => 96,  206 => 95,  200 => 94,  194 => 93,  188 => 92,  182 => 91,  176 => 90,  172 => 89,  169 => 88,  160 => 85,  157 => 84,  153 => 83,  150 => 82,  141 => 79,  138 => 78,  134 => 77,  131 => 76,  122 => 73,  119 => 72,  115 => 71,  111 => 69,  103 => 67,  101 => 66,  88 => 56,  80 => 51,  31 => 4,  28 => 3,  11 => 1,);
    }
}

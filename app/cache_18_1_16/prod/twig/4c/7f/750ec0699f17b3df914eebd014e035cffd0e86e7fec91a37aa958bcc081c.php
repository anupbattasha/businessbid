<?php

/* TicketingTicketTicketBundle:vendor:ticketview.html.twig */
class __TwigTemplate_4c7f750ec0699f17b3df914eebd014e035cffd0e86e7fec91a37aa958bcc081c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "TicketingTicketTicketBundle:vendor:ticketview.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["issueType"] = array(0 => "Other Issue", 1 => "Account Suspension", 2 => "Customer Dispute", 3 => "Identity Verification", 4 => "Job Requests", 5 => "Payment Issue", 6 => "Reviews & Ratings", 7 => "Service Categories", 8 => "SMS Notification", 9 => "System Issue", 10 => "Email Address Change", 11 => "Lead Refund");
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"page-bg\">
\t<div class=\"container\">
\t\t<div class=\"dashboard-panel\">
\t\t
\t\t";
        // line 9
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 22
        echo "
\t\t";
        // line 23
        $this->displayBlock('enquiries', $context, $blocks);
        // line 114
        echo "\t\t</div>
\t</div>
</div>
";
    }

    // line 9
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 10
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
        echo "\">Vendor Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"lead-purchases\" href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_home");
        echo "\">Submit Support Ticket</a></li>
                            <li><a class=\"account\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_list");
        echo "\">View All Tickets</a></li>
\t\t\t\t\t\t\t<li><a class=\"livechat\" href=\"#\">Live Chat</a></li>
\t\t\t\t\t\t\t<li><a class=\"contact\" href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#contact\">Contact Us</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t";
    }

    // line 23
    public function block_enquiries($context, array $blocks = array())
    {
        // line 24
        echo "\t\t\t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t\t<div>
\t\t\t\t";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 27
            echo "
\t\t\t\t\t<div class=\"alert alert-danger\">";
            // line 28
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 33
            echo "
\t\t\t\t\t<div class=\"alert alert-success\">";
            // line 34
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 38
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 39
            echo "
\t\t\t\t\t<div class=\"alert alert-success\">";
            // line 40
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "\t\t\t\t</div>
\t\t\t\t\t<div class=\"Create-ticket-form\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t\t\t<label>Request ID</label>
\t\t\t\t\t\t\t\t\t<p>: #V";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketNo", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t\t\t<label>type of Issue</label>
\t\t\t\t\t\t\t\t\t<p>: ";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["issueType"]) ? $context["issueType"] : null), $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketTypeId", array()), array(), "array"), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t\t\t<label>Subject</label>
\t\t\t\t\t\t\t\t\t<p>: ";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketSubject", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t\t\t<label>Status</label>
\t\t\t\t\t\t\t\t\t<p>: ";
        // line 61
        if (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketStatus", array()) == 1)) {
            echo " Open ";
        } elseif (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketStatus", array()) == 2)) {
            echo " Close ";
        } elseif (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketStatus", array()) == 3)) {
            echo " Pending ";
        }
        echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t\t\t\t<label>Last Updated</label>
\t\t\t\t\t\t\t\t\t";
        // line 65
        if ((twig_length_filter($this->env, (isset($context["ticketConversation"]) ? $context["ticketConversation"] : null)) > 0)) {
            // line 66
            echo "\t\t\t\t\t\t\t\t\t";
            $context["ticketConversationSize"] = twig_length_filter($this->env, (isset($context["ticketConversation"]) ? $context["ticketConversation"] : null));
            // line 67
            echo "\t\t\t\t\t\t\t\t\t<p>: ";
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["ticketConversation"]) ? $context["ticketConversation"] : null), ((isset($context["ticketConversationSize"]) ? $context["ticketConversationSize"] : null) - 1), array(), "array"), "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t\t\t";
        } else {
            // line 69
            echo "\t\t\t\t\t\t\t\t\t<p>: ";
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t\t\t";
        }
        // line 71
        echo "\t\t\t\t\t\t\t\t</div>
                \t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t";
        // line 75
        if (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketUploadFile", array()) != "")) {
            // line 76
            echo "\t\t\t\t\t\t\t\t<div class=\"tic-attach-section\">
\t\t\t\t\t\t\t\t\t<label>Attachments</label>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("support/uploads/download.php?filenale=" . $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketUploadFile", array()))), "html", null, true);
            echo "\"  target=\"_blank\">Downloads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        } else {
            // line 81
            echo "\t\t\t\t\t\t\t<div class=\"tic-attach-section\">
\t\t\t\t\t\t\t\t\t<label>Attachments</label>
\t\t\t\t\t\t\t\t\t\t<p>No attachments</p>
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
                \t\t\t
                \t\t\t";
        }
        // line 87
        echo "\t\t\t\t\t\t</div></div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"tic-info-main\">
                 \t\t\t\t<div class=\"tic-info-block\">
                     \t\t\t\t<div class=\"name\">";
        // line 92
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "</div>
                        \t\t\t<div class=\"date\">";
        // line 93
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketDatetime", array()), "date", array()), "html", null, true);
        echo "</div>
                        \t\t\t<div class=\"tic-info-content review\">";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketQuestion", array()), "html", null, true);
        echo "</div>
                    \t\t\t</div>
\t\t\t\t\t\t\t\t";
        // line 96
        if ( !twig_test_empty((isset($context["ticketConversation"]) ? $context["ticketConversation"] : null))) {
            // line 97
            echo "\t\t\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["ticketConversation"]) ? $context["ticketConversation"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["replies"]) {
                // line 98
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"tic-info-block\">
\t\t                     \t\t\t\t<div class=\"name\">";
                // line 99
                if (($this->getAttribute($context["replies"], "userId", array()) != 0)) {
                    echo "  ";
                    echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
                    echo " ";
                } else {
                    echo " Admin ";
                }
                echo "</div>
\t\t                        \t\t\t<div class=\"date\">";
                // line 100
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["replies"], "ticketDatetime", array()), "date", array()), "html", null, true);
                echo "</div>
\t\t                        \t\t\t<div class=\"tic-info-content review\">";
                // line 101
                echo twig_escape_filter($this->env, $this->getAttribute($context["replies"], "ticketResponse", array()), "html", null, true);
                echo "</div>
\t\t                    \t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['replies'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 104
            echo "\t\t\t\t\t\t\t\t";
        }
        // line 105
        echo "\t\t\t\t\t\t\t\t<div class=\"tic-info-reply\">
\t\t\t\t\t\t\t\t\t";
        // line 106
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "
\t\t\t\t\t\t\t\t\t<a class=\"btn btn-sm btn-warning cancel\" href=\"";
        // line 107
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_list");
        echo "\">Cancel</a>
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:vendor:ticketview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 107,  291 => 106,  288 => 105,  285 => 104,  276 => 101,  272 => 100,  262 => 99,  259 => 98,  254 => 97,  252 => 96,  247 => 94,  243 => 93,  239 => 92,  232 => 87,  224 => 81,  218 => 78,  214 => 76,  212 => 75,  206 => 71,  200 => 69,  194 => 67,  191 => 66,  189 => 65,  176 => 61,  169 => 57,  162 => 53,  155 => 49,  147 => 43,  138 => 40,  135 => 39,  131 => 38,  128 => 37,  119 => 34,  116 => 33,  112 => 32,  109 => 31,  100 => 28,  97 => 27,  93 => 26,  89 => 24,  86 => 23,  77 => 17,  72 => 15,  68 => 14,  64 => 13,  59 => 10,  56 => 9,  49 => 114,  47 => 23,  44 => 22,  42 => 9,  36 => 5,  33 => 4,  29 => 1,  27 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:categories_reports.html.twig */
class __TwigTemplate_60753d91ee3d0f3096fa6f2f089cd0cb48a688120f1a52469290e481ed13f69c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:categories_reports.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"//cdn.datatables.net/plug-ins/9dcbecd42ad/api/fnFilterClear.js\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).ready(function() {
    \$('#example').dataTable({
        \"sPaginationType\": \"full_numbers\",
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
        \"sAjaxSource\": \"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/categories_reports.php"), "html", null, true);
        echo "\"
    }).columnFilter({
        aoColumns: [
        {
            sSelector: \"#categoryFilter\"
        }, {
            sSelector: \"#descriptionFilter\"
        }, {
            type: 'number-range',
            sSelector: '#priceFilter',
            sRangeFormat: \"<label>Amount</label> {from} {to}\"
        }]
    });
});
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Categories</h1></div>

<div>
\t";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 53
            echo "
\t<div class=\"alert alert-danger\">";
            // line 54
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "\t
\t";
        // line 58
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 59
            echo "
\t<div class=\"alert alert-success\">";
            // line 60
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "\t
\t";
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 65
            echo "
\t<div class=\"alert alert-success\">";
            // line 66
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "<div class=\"admin_filter report-filter\">\t
\t<div class=\"col-md-4 side-clear\" id=\"categoryFilter\"></div>
    <div class=\"col-md-3 clear-right\" id=\"descriptionFilter\"></div>
    <div class=\"col-md-5 clear-right\" id=\"priceFilter\"></div>
\t<div class=\"col-md-5 clear-right action-links\">
\t<span class=\"export-all\"><a href=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_exportall_categories");
        echo "\" class=\"form_submit\">Export All</a></span>
\t";
        // line 76
        echo "\t<span class=\"reset\"><a href=\"#\" class=\"form_submit\" onclick=\"fnResetFilters();\">Reset</a></span>
\t</div>
</div>
</div>\t
\t\t\t
<div class=\"latest-orders\">
\t\t<table id=\"example\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>Category Name</th>
\t\t\t\t\t<th>Description</th>
\t\t\t\t\t<th>Price (AED)</th>
\t\t\t\t</tr>
\t\t</thead>
\t\t<tbody>
\t\t\t<tr>
\t\t\t\t<td colspan=\"3\" class=\"dataTables_empty\">Loading data from server</td>
\t\t\t</tr>
\t\t</tbody>
\t\t<tfoot>
\t\t\t<tr>
                <th>Category Name</th>
                <th>Description</th>
                <th>Price (AED)</th>
            </tr>
\t\t</tfoot>
\t</table>
</div>
</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:categories_reports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 76,  149 => 74,  142 => 69,  133 => 66,  130 => 65,  126 => 64,  123 => 63,  114 => 60,  111 => 59,  107 => 58,  104 => 57,  95 => 54,  92 => 53,  88 => 52,  65 => 32,  37 => 7,  31 => 5,  28 => 3,  11 => 1,);
    }
}

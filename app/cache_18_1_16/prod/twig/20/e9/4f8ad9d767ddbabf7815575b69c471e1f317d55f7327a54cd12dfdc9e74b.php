<?php

/* BBidsBBidsHomeBundle:Admin:anonymous_reviews.html.twig */
class __TwigTemplate_20e94f8ad9d767ddbabf7815575b69c471e1f317d55f7327a54cd12dfdc9e74b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return $this->loadTemplate(((((isset($context["pid"]) ? $context["pid"] : null) == 1)) ? ("::base_admin.html.twig") : ("::base.html.twig")), "BBidsBBidsHomeBundle:Admin:anonymous_reviews.html.twig", 2);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 2
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        // line 7
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/rating.js"), "html", null, true);
        echo "\"></script>
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/rating.css"), "html", null, true);
        echo "\" media=\"all\">

<script type=\"text/javascript\">
\$(document).ready(function() {
    \$('#rate1').rating('form_ratework','ratingpost.php', {maxvalue:5});
    \$('#rate2').rating('form_rateservice','ratingpost.php', {maxvalue:5});
    \$('#rate3').rating('form_ratemoney','ratingpost.php', {maxvalue:5});
    \$('#rate4').rating('form_ratebusiness','ratingpost.php', {maxvalue:5});

    \$('#notchoose').hide();
    \$('#ratings').hide();
    \$('#form_businessknow').on('change load',function(){
        var value = \$( \"#form_businessknow option:selected\" ).text();
        if(value==\"I hired them\")
        {
            \$('#notchoose').hide();
            \$('#ratings').show();
        }
        else if(value==\"I spoke with them but did not hire\")
        {

            \$('#notchoose').show();
            \$('#ratings').hide();
        }
        else
        {
                \$('#notchoose').hide();
                \$('#ratings').hide();
        }
    });

    \$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

});
</script>

<div class=\"container review-container\">
<!--<div class=\"breadcrum\">Home / Vendor Review / Netiapps Software</div>-->

<div class=\"col-md-8 review-container-left\">
<div><h1>REVIEW </h1></div>
<div>
    ";
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 65
            echo "
    <div class=\"alert alert-danger\">";
            // line 66
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "
    ";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 71
            echo "
    <div class=\"alert alert-success\">";
            // line 72
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 76
            echo "
    <div class=\"alert alert-success\">";
            // line 77
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "</div>
<div class=\"form_wrapper \">

    ";
        // line 83
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
<div>
        <div class=\"control-group\"><label>";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "customer_name", array()), 'label');
        echo "</label></div><div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "customer_name", array()), 'widget');
        echo "</div>
        <div class=\"control-group\"><label>";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendor", array()), 'label');
        echo "</label></div><div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendor", array()), 'widget');
        echo "</div>
        <div class=\"control-group\"><label>";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "businessknow", array()), 'label');
        echo "</label></div><div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "businessknow", array()), 'widget');
        echo "</div>
        <div class=\"control-group\"><label>";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "businessrecomond", array()), 'label');
        echo "</label></div><div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "businessrecomond", array()), 'widget');
        echo "</div>
        <div class=\"control-group\"><label>";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "servicesummary", array()), 'label');
        echo "</label></div><div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "servicesummary", array()), 'widget');
        echo "</div>
        <!--<div class=\"control-group\"><label>";
        // line 90
        echo "</label></div><div class=\"control\">";
        echo "</div>-->
        <div id=\"notchoose\"><h2>";
        // line 91
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "notchoose", array()), 'label');
        echo "</h2><div class=\"control notchoose\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "notchoose", array()), 'widget');
        echo "</div></div>
        <div class=\"control-group\"><label>Service Experience</label></div><div class=\"control\">";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "serviceexperience", array()), 'widget');
        echo "</div>
<div id=\"ratings\">
            <div>
                <div class=\"control-group\"><label>How would you rate work quality?</label></div>
                <div id=\"rate1\" class=\"rating\">&nbsp;</div>

            </div>
            <div>
                <div class=\"control-group\"><label>How would you rate customer service?</label></div>
                <div id=\"rate2\" class=\"rating\">&nbsp;</div>

            </div>
            <div>
                <div class=\"control-group\"><label>How would you rate this vendor as value for money?</label></div>
                <div id=\"rate3\" class=\"rating\">&nbsp;</div>

            </div>
            <div>
                <div class=\"control-group\"><label>What is your overall rating of this business?</label></div>
                <div id=\"rate4\" class=\"rating\">&nbsp;</div>

            </div>

</div>
</div>
<div class=\"note\">Note: All fields are mandatory!</div>
    ";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
</div>
</div>
<div class=\"col-md-4 review-container-right\">
    ";
        // line 144
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:anonymous_reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 144,  228 => 118,  199 => 92,  193 => 91,  189 => 90,  183 => 89,  177 => 88,  171 => 87,  165 => 86,  159 => 85,  154 => 83,  149 => 80,  140 => 77,  137 => 76,  132 => 75,  123 => 72,  120 => 71,  116 => 70,  113 => 69,  104 => 66,  101 => 65,  97 => 64,  38 => 8,  33 => 7,  30 => 6,  26 => 2,  24 => 1,  18 => 2,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:viewreview.html.twig */
class __TwigTemplate_b218d2b8f95704f0add2ae0c343e84ddb53fa244b582af3f6b09e8d67ebe1f57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:viewreview.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script>
\$(document).ready(function(){
\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
       
        // Create stars holder
        var \$span = \$('<span />').width(size);
        
        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

});
</script>
<div class=\"container\"> 
<div class=\"page-title\"><h1>Review Information</h1></div>
<table class=\"col-sm-6 vendor-user gray-bg vendor-enquiries\" width=\"100%\">
\t";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["review"]) ? $context["review"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 28
            echo "\t<tr><td><strong>Date Received</strong></td><td>\t";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Customer Name</strong></td><td>\t";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "author", array()), "html", null, true);
            echo " </td></tr>
\t<tr><td><strong>Category</strong></td><td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "category", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Vendor Name</strong></td><td>\t";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "contactname", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Vendor Busniess</strong></td><td> ";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "bizname", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Rating Review</strong></td><td><span class=\"stars\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "rating", array()), "html", null, true);
            echo " </span></td></tr>
\t<tr><td><strong>Busniess Recommendation</strong></td><td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessrecomond", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Busniess Knowledge</strong></td><td> ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessknow", array()), "html", null, true);
            echo " </td></tr>
\t
\t<tr><td><strong>Service Summary</strong></td><td> ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "servicesummary", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Status</strong></td><td>";
            // line 38
            if (($this->getAttribute($context["r"], "modstatus", array()) == 0)) {
                echo " Not Published\t";
            } elseif (($this->getAttribute($context["r"], "modstatus", array()) == 1)) {
                echo " Published ";
            }
            echo "</td></tr>
\t<tr><td width=\"30%\"><strong>Review</strong></td><td width=\"70%\">";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "review", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td colspan=\"2\" >";
            // line 40
            if (($this->getAttribute($context["r"], "modstatus", array()) != 1)) {
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_publish_review", array("reviewid" => $this->getAttribute($context["r"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-success\">Publish</a> ";
            } else {
                echo " <a href=\"#\" class=\"btn btn-default disabled\" > Published </a>";
            }
            echo "</td></tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo " 

</table>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:viewreview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 41,  110 => 40,  106 => 39,  98 => 38,  94 => 37,  89 => 35,  85 => 34,  81 => 33,  77 => 32,  73 => 31,  69 => 30,  65 => 29,  60 => 28,  56 => 27,  31 => 4,  28 => 3,  11 => 1,);
    }
}

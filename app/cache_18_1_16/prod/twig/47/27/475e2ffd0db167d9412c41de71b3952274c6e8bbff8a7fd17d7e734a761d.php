<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Admin:contactustoadminemail.html.twig */
class __TwigTemplate_4727475e2ffd0db167d9412c41de71b3952274c6e8bbff8a7fd17d7e734a761d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">Dear Admin,</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">MR/MRs ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo " and his/her Email ID is ";
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : null), "html", null, true);
        echo " has Provided his contact Details to BusinessBid</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
<span>
----------------------------------------------------------------------------------------------------------------------------------------
\t<span>Mobile: ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["mobile"]) ? $context["mobile"] : null), "html", null, true);
        echo "</span>
\t<span>Usertype: ";
        // line 8
        if (((isset($context["usertype"]) ? $context["usertype"] : null) == 1)) {
            echo " Others ";
        } elseif (((isset($context["usertype"]) ? $context["usertype"] : null) == 2)) {
            echo " Customer ";
        } else {
            echo " Vendor ";
        }
        echo "</span>
\t<span>Message: ";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</span>
\t
</span>
</p>



";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Admin:contactustoadminemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 9,  36 => 8,  32 => 7,  23 => 3,  19 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:freetrail.html.twig */
class __TwigTemplate_7aa0a274148173f99a3ee38e3612370afef30d198f2d38fa0d138f6f8ce8843e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:freetrail.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">

 function deletefunc(userid, username){
\tvar con = confirm('Are you sure to delete '+username+' ? The record once deleted cannot be retrived back');
\tif(con){
\t\tvar url = \"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_user_delete", array("userid" => "USERID"));
        echo "\";
\t\tvar urlset = url.replace('USERID', userid);
\t\t\twindow.open(urlset,\"_self\");
\t\treturn true;
\t}else{
\t\treturn false;
\t}
 }
</script>
<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\">
<script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
<script src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>
<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
<script>
\$(function() {
\t\$(\"#datepicker3\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true,
\t\tmaxDate: '+2y',
\t\tyearRange: '2014:2024',
\t\tonSelect: function(date){
\t\t\tvar selectedDate = new Date(date);
\t\t\tvar msecsInADay = 86400000;
\t\t\tvar endDate = new Date(selectedDate.getTime() + msecsInADay);

\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"minDate\", endDate );
\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"maxDate\", '+2y' );
\t\t}
\t});

\t\$(\"#datepicker4\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true
\t});

\tvar availableVendors = ";
        // line 44
        echo twig_jsonencode_filter((isset($context["accountList"]) ? $context["accountList"] : null));
        echo ";
    \$( \"#form_vendorname\" ).autocomplete({
      source: availableVendors
    });
});
</script>

<div class=\"page-bg\">\t
    <div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>FREETRAIL LIST</h1></div>
";
        // line 54
        if ((isset($context["count"]) ? $context["count"] : null)) {
            // line 55
            echo "<div class=\"row text-right page-counter-history\">
<div class=\"counter-box\">Now showing : ";
            // line 56
            echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
            echo " of ";
            echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
            echo " records</div></div>
";
        }
        // line 58
        echo "<div>
\t";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 60
            echo "
\t<div class=\"alert alert-danger\">";
            // line 61
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "\t
\t";
        // line 65
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 66
            echo "
\t<div class=\"alert alert-success\">";
            // line 67
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "\t
\t";
        // line 71
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 72
            echo "
\t<div class=\"alert alert-success\">";
            // line 73
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "\t
\t
<div class=\"admin_filter\">\t
\t";
        // line 79
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t
\t\t<div class=\"col-md-3 side-clear\" >  ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'widget');
        echo "</div>
\t\t<div class=\"col-md-3 clear-right\" >  ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
\t    <div class=\"col-md-2 clear-right\" >  ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'widget', array("id" => "datepicker3"));
        echo "</div>
\t    <div class=\"col-md-2 clear-right\">";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'widget', array("id" => "datepicker4"));
        echo " </div>
\t\t<div class=\"col-md-2 top-space clear-right\"> ";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "filter", array()), 'widget', array("label" => "Search"));
        echo " </div>
\t\t
\t";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
</div>\t\t
<div class=\"latest-orders\">
<table>
<thead><tr><th>Date Recieved</th>
<th>Order No</th>
<th>Vendor Name</th>
<th>Business Name</th>
<th>Category</th>
<th>Email</th>
<th>Leads Credited</th>
<th>Leads Credited Date</th>
<th>Status</th>
<th>Expiry Date</th>
<!--<td>Action</td>-->
</tr></thead>
";
        // line 103
        if ( !twig_test_empty((isset($context["users"]) ? $context["users"] : null))) {
            // line 104
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 105
                echo "\t<tr>
\t<td>";
                // line 106
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td>
\t\t<td>";
                // line 107
                if (($this->getAttribute($context["user"], "leads", array()) == 0)) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_freetrail_edit", array("userid" => $this->getAttribute($context["user"], "userid", array()))), "html", null, true);
                    echo "\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "orderno", array()), "html", null, true);
                    echo "</a>";
                } else {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "orderno", array()), "html", null, true);
                }
                echo "</td>
\t\t<td>";
                // line 108
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "contactname", array()), "html", null, true);
                echo "</td>
\t\t<td>";
                // line 109
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "bizname", array()), "html", null, true);
                echo "</td>
\t\t<td>";
                // line 110
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "category", array()), "html", null, true);
                echo "</td>
\t\t<td>";
                // line 111
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
                echo "</td>
\t\t<td>";
                // line 112
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "leads", array()), "html", null, true);
                echo "</td>
\t\t<td>";
                // line 113
                if (($this->getAttribute($context["user"], "updated", array()) == "")) {
                    echo " N/A ";
                } else {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "updated", array()), "Y-m-d h:i:s"), "html", null, true);
                }
                echo "</td>
\t\t<td>";
                // line 114
                if (($this->getAttribute($context["user"], "status", array()) == 1)) {
                    echo " Active ";
                } elseif (($this->getAttribute($context["user"], "status", array()) == 0)) {
                    echo " Inactive  ";
                }
                echo "</td>
                   <td>";
                // line 115
                if (twig_test_empty($this->getAttribute($context["user"], "daysdif", array()))) {
                    echo "  N/A ";
                } else {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "daysdif", array()), "html", null, true);
                    echo " ";
                }
                echo "</td>
\t<!--<td><a href=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_user_edit", array("userid" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
                echo "\">Edit</a> | <a href=\"#\" onclick=\"deletefunc('";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
                echo "', '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
                echo "')\">Delete</a></td>-->
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 118
            echo "\t";
        } else {
            // line 119
            echo "\t<tr>
\t
\t<td>NO Data Available</td>
\t
\t</tr>
\t";
        }
        // line 125
        echo "</table>
</div>
</div>
";
        // line 128
        if ((isset($context["count"]) ? $context["count"] : null)) {
            // line 129
            echo "
<div class=\"row text-right \">
\t<div class=\"counter-box top-space\">
\t";
            // line 132
            $context["page_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 10)));
            echo " 
<ul class=\"pagination\">

  <li><a href=\"";
            // line 135
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_freetrails", array("offset" => 1));
            echo "\">&laquo;</a></li>
";
            // line 136
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["page_count"]) ? $context["page_count"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 137
                echo "
  <li><a href=\"";
                // line 138
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_freetrails", array("offset" => $context["i"])), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</a></li>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 141
            echo "  
  <li><a href=\"";
            // line 142
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_freetrails", array("offset" =>  -(isset($context["page_count"]) ? $context["page_count"] : null))), "html", null, true);
            echo "\">&raquo;</a></li>
</ul>
</div> 
</div>
";
        }
        // line 147
        echo "</div>
</div> 
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:freetrail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 147,  355 => 142,  352 => 141,  341 => 138,  338 => 137,  334 => 136,  330 => 135,  324 => 132,  319 => 129,  317 => 128,  312 => 125,  304 => 119,  301 => 118,  289 => 116,  280 => 115,  272 => 114,  264 => 113,  260 => 112,  256 => 111,  252 => 110,  248 => 109,  244 => 108,  232 => 107,  228 => 106,  225 => 105,  220 => 104,  218 => 103,  199 => 87,  194 => 85,  188 => 84,  182 => 83,  176 => 82,  170 => 81,  165 => 79,  160 => 76,  151 => 73,  148 => 72,  144 => 71,  141 => 70,  132 => 67,  129 => 66,  125 => 65,  122 => 64,  113 => 61,  110 => 60,  106 => 59,  103 => 58,  94 => 56,  91 => 55,  89 => 54,  76 => 44,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:deleteaccountcon.html.twig */
class __TwigTemplate_aa1eadadaba4e9f21f4e26093d40f7c2650bd349af3c008933aa84ccee7f4f0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:deleteaccountcon.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_enquiries($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"col-md-9 dashboard-rightpanel user-profile-basic\">
\t<div class=\"page-title\"><h1>My Account</h1></div>
\t<div class=\"row user-profile-block\">
\t\t<div class=\"user-profile-basic\">
\t\t\t\t
\t\t\t<div class=\"row\">
\t\t\t ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 12
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 13
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div> 
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
\t";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 18
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 19
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>\t
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "\t
\t";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 24
            echo "
\t<div class=\"alert alert-success\">";
            // line 25
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "\t\t\t\t<div class=\"tab-content\">
\t\t\t\t  <div class=\"tab-pane active profile-inform\" id=\"profile\">
\t\t\t\t<div class=\"col-md-8 profile-info delete-account\">
\t\t\t\t<h2>Delete Account</h2>
\t\t\t\t\t<p>We are disappointed to hear that you wish to close your account!  Please call our Support Centre or Email us and we are confident that our team will be able to ease your concerns</p>
\t\t\t\t<h5><strong>Customer Support Line :<a href-\"#\">(04) 42 13 777</a></strong></h5>
\t\t\t\t<h5><strong>Email :<a href-\"#\">support@businessbid.ae</a></strong></h5>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>
\t\t
\t

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:deleteaccountcon.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 28,  84 => 25,  81 => 24,  77 => 23,  74 => 22,  65 => 19,  62 => 18,  58 => 17,  55 => 16,  46 => 13,  43 => 12,  39 => 11,  31 => 5,  28 => 4,  11 => 1,);
    }
}

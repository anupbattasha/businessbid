<?php

/* BBidsBBidsHomeBundle:Admin:enquiries_reports.html.twig */
class __TwigTemplate_922a5d8f24850173196f5617c17e0b51177d0f6497088fa7508382620031f8bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:enquiries_reports.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).ready(function() {

    \$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
    \$.datepicker.regional[\"\"].changeMonth = true;
    \$.datepicker.regional[\"\"].changeYear = true;
    \$.datepicker.setDefaults(\$.datepicker.regional['']);
    \$('#enquiryReports').dataTable({
        \"sPaginationType\": \"full_numbers\",
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
     \"sAjaxSource\": \"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/enquiry_reports.php"), "html", null, true);
        echo "\",
        \"fnServerParams\": function(aoData) {
            if (aoData[36].value != '') {
                aoData[36].value = (aoData[36].value == 'Active') ? 1 : 2;
            }
        }
    }).columnFilter({
        aoColumns: [{
            type: \"date-range\",
            sSelector: \"#dateFilter\",
            sRangeFormat: \"<label>Date</label> {from} {to}\"

        }
        , {
            sSelector: \"#jobnoFilter\"
        }, {
            sSelector: \"#subjectFilter\"
        }, {

            type: 'select',

            values: ";
        // line 58
        echo twig_jsonencode_filter((isset($context["categories"]) ? $context["categories"] : null));
        echo ",
            sSelector: \"#categoryFilter\"
        }, {
            sSelector: \"#decriptionFilter\"
        }, {
            type: 'select',
            values: [\"Dubai\", \"Sharjah\", \"Abu Dhabi\", \"Ajman\", \"Fujairah\", \"Ras Al-Khaimah\", \"Umm Al-Quwain\"],
            sSelector: '#cityFilter'
        }, {
            sSelector: \"#emailFilter\"
        }, {
            type: 'select',
            values: [\"Active\", \"Inactive\"],
            sSelector: '#statusFilter'
        }]
    });
});
</script>
<div class=\"page-bg\">
    <div class=\"container inner_container admin-dashboard\">
        <div class=\"page-title\"><h1>Job Request Reports</h1></div>
        <div>
            ";
        // line 80
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 81
            echo "                <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo "
            ";
        // line 85
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 86
            echo "                <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "
            ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 91
            echo "                <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "            <div>";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "</div>
            <div class=\"admin_filter report-filter\" id=\"example2\">
                <div class=\"col-md-3 side-clear\" id=\"dateFilter\"></div>
                <div class=\"col-md-2 clear-right\" id=\"jobnoFilter\"></div>
                <div class=\"col-md-2 clear-right\" id=\"subjectFilter\"></div>
                <div class=\"col-md-2 clear-right\" id=\"categoryFilter\"></div>
                <div class=\"col-md-2 clear-right\" id=\"decriptionFilter\"></div>
                <div class=\"col-md-2 side-clear med-space\" id=\"cityFilter\"></div>
                <div class=\"col-md-2 clear-right med-space\" id=\"emailFilter\"></div>
                <div class=\"col-md-2 med-space\" id=\"statusFilter\"></div>
                <div class=\"col-md-5 clear-right action-links space\">
                    <span class=\"export-all\"><a href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_exportall_jobrequest");
        echo "\" target=\"_blank\" class=\"form_submit\">Export All</a></span>
                    <span class=\"export-selected\"><a href=\"#\" class=\"form_submit\" onclick=\"getSearchInputs();\">Export Filtered Data</a></span>
                    <span class=\"reset\"><a href=\"#\" class=\"form_submit\" onclick=\"fnResetFilters();\">Reset</a></span>
                </div>
            </div>
        </div>
        <div class=\"latest-orders\">
            <table id=\"enquiryReports\"> <thead> <tr> <th>Date Created</th> <th>Job Request Number</th> <th>Subject</th> <th>Categories</th> <th>Description</th> <th>City</th> <th>Email</th> <th>Status</th> </tr></thead> <tbody> <tr> <td colspan=\"8\" class=\"dataTables_empty\">Loading data from server... Please wait.</td></tr></tbody> <tfoot> <tr> <th>Date Created</th> <th>Job Request Number</th> <th>Subject</th> <th>Categories</th> <th>Description</th> <th>City</th> <th>Email</th> <th>Status</th> </tr></tfoot> </table>
        </div>
    </div>
</div>
</div>
<script type=\"text/javascript\">
    function getSearchInputs () {
        var oTable = \$('#enquiryReports').dataTable();
        var oSettings = oTable.fnSettings();
        var \$form = document.forms[\"form\"];
        \$form.reset();
        var oForm = document.forms[\"form\"].getElementsByTagName(\"input\");
        var dateRange = '';
        if((\$('#enquiryReports_range_from_0').val() != 'undefined') && (\$('#enquiryReports_range_from_0').val() != '')) {
            dateRange = \$('#enquiryReports_range_from_0').val() +'~';
            if(\$('#enquiryReports_range_to_0').val() != 'undefined')
                dateRange += \$('#enquiryReports_range_to_0').val();
        }
        oForm[0].value = dateRange;
        oForm[1].value = oSettings.aoPreSearchCols[1].sSearch;
        oForm[2].value = oSettings.aoPreSearchCols[2].sSearch;
        oForm[3].value = oSettings.aoPreSearchCols[3].sSearch;
        oForm[4].value = oSettings.aoPreSearchCols[4].sSearch;
        oForm[5].value = oSettings.aoPreSearchCols[5].sSearch;
        oForm[6].value = oSettings.aoPreSearchCols[6].sSearch;

        if(oSettings.aoPreSearchCols[7].sSearch != '')
            oForm[7].value = (oSettings.aoPreSearchCols[7].sSearch == 'Active') ? 1 : 2;

        if((oForm[0].value =='') && (oForm[1].value =='') && (oForm[2].value =='') && (oForm[3].value =='') && (oForm[4].value =='') && (oForm[5].value =='') && (oForm[6].value =='') && (oForm[7].value ==''))
            alert('No filtered results');
        else
           \$form.submit();
    }

    function fnResetFilters() {
    \t\$(\"input\").val(\"\");
    \t\$('div#categoryFilter').find('select option:eq(0)').prop('selected', true);
    \t\$('div#cityFilter').find('select option:eq(0)').prop('selected', true);
    \tvar table = \$('#enquiryReports').dataTable();
        table.fnFilterClear();
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:enquiries_reports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 105,  173 => 94,  163 => 91,  159 => 90,  156 => 89,  146 => 86,  142 => 85,  139 => 84,  129 => 81,  125 => 80,  100 => 58,  76 => 37,  44 => 8,  40 => 7,  36 => 6,  31 => 5,  28 => 3,  11 => 1,);
    }
}

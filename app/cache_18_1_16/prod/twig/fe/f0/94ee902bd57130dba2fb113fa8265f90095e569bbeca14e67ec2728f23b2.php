<?php

/* TicketingTicketTicketBundle:Customer:ticketlist.html.twig */
class __TwigTemplate_fef094ee902bd57130dba2fb113fa8265f90095e569bbeca14e67ec2728f23b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "TicketingTicketTicketBundle:Customer:ticketlist.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"page-bg\">\t
\t<div class=\"container\">
\t\t<div class=\"dashboard-panel\">
\t\t";
        // line 7
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 20
        echo "
\t\t";
        // line 21
        $this->displayBlock('enquiries', $context, $blocks);
        // line 76
        echo "\t\t</div>
\t</div>
</div>

";
    }

    // line 7
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 8
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
        echo "\">Customer Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"lead-purchases\" href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_home");
        echo "\">Submit Support Ticket</a></li>
                            <li><a class=\"account\" href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_list");
        echo "\">View All Tickets</a></li>
                            <li><a class=\"livechat\" href=\"#\">Live Chat</a></li>
                            <li><a class=\"contact\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#contact\">Contact Us</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t";
    }

    // line 21
    public function block_enquiries($context, array $blocks = array())
    {
        // line 22
        echo "\t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t<div class=\"page-title\"><h1>All Tickets</h1>\t</div>
\t\t\t<div class=\"search_filter\">
\t\t\t\t<p id=\"ticketidFilter\"></p>
\t\t\t\t<p id=\"dateFilter\"></p>
\t\t\t\t<p id=\"subjectFilter\"></p>
\t\t\t\t<p id=\"statusFilter\"></p>
\t\t\t\t<p id=\"priorityFilter\"></p>
\t\t\t</div>
\t\t\t<div class=\"Create-ticket-form ticket-lists\">
\t\t\t\t
\t\t\t\t<table id=\"vendorTicketList\" class=\"display\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t\t\t<th class=\"ticket-id\">Ticket Id</th>
\t\t\t\t\t\t\t<th >Subject</th>
\t\t\t\t\t\t\t<th>Priority</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tfoot>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t\t<th>Ticket Id</th>
\t\t\t\t\t\t<th>Subject</th>
\t\t\t\t\t\t<th>Priority</th>
\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 56
        echo "\t\t\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listData"]) ? $context["listData"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 57
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"sl-no\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"date-rcvd\">";
            // line 59
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($context["list"], "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"ticket-id\"><a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_view", array("id" => $this->getAttribute($context["list"], "id", array()))), "html", null, true);
            echo "\">#C";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "ticketNo", array()), "html", null, true);
            echo "</a></td>
\t\t\t\t\t\t\t\t<td class=\"subject\">";
            // line 61
            echo twig_escape_filter($this->env, (((twig_length_filter($this->env, $this->getAttribute($context["list"], "ticketSubject", array())) > 50)) ? ((twig_slice($this->env, $this->getAttribute($context["list"], "ticketSubject", array()), 0, 50) . "...")) : ($this->getAttribute($context["list"], "ticketSubject", array()))), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"status\">";
            // line 62
            if (($this->getAttribute($context["list"], "ticketPriority", array()) == 1)) {
                echo "Low ";
            } elseif (($this->getAttribute($context["list"], "ticketPriority", array()) == 2)) {
                echo "Medium";
            } else {
                echo "High";
            }
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"status\"><span>";
            // line 63
            if (($this->getAttribute($context["list"], "ticketStatus", array()) == 1)) {
                echo " <span class=\"open-status\">Open </span>";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 2)) {
                echo "<span class=\"close-status\">Close</span>";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 3)) {
                echo "<span class=\"pending-status\">Pending</span>";
            }
            echo "</span></td>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
<script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-1.4.4.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilter.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/myscript.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t\t";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:Customer:ticketlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 74,  211 => 73,  207 => 72,  203 => 71,  199 => 70,  193 => 66,  170 => 63,  160 => 62,  156 => 61,  150 => 60,  146 => 59,  142 => 58,  139 => 57,  121 => 56,  86 => 22,  83 => 21,  74 => 15,  69 => 13,  65 => 12,  61 => 11,  56 => 8,  53 => 7,  45 => 76,  43 => 21,  40 => 20,  38 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}

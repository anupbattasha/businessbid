<?php

/* BBidsBBidsHomeBundle:Admin:keywords.html.twig */
class __TwigTemplate_a2e78e87a94dbbbce8db0e60cb55fe07c6a3c3f3ac4f6d32c2382b4e7e9611e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:keywords.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">

 function deletefunc(keyid){
\tvar con = confirm(\"Are you sure to delete ? \\n The record once deleted cannot be retrived back!\");
\tif(con){
\t\tvar url = \"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_delete_keyword", array("keyid" => "KEYID"));
        echo "\";
\t\tvar urlset = url.replace('KEYID', keyid);
\t\t\twindow.open(urlset,\"_self\");
\t\treturn true;
\t}else{
\t\treturn false;
\t}
 }
</script>
<div class=\"inner_container container\">
<div class=\"\"><h3>Keyword List <span class=\"pull-right\"><a class=\"btn btn-warning\" href=\"javascript:window.history.go(-1)\">Back</a> <a  class=\"btn btn-success\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_add_keyword", array("categoryid" => (isset($context["categoryid"]) ? $context["categoryid"] : null))), "html", null, true);
        echo "\">Add Keyword</a></span></h3> </div>
\t<div class=\"segment-content row\">
\t";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 22
            echo "
\t<div class=\"alert alert-danger\">";
            // line 23
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
\t";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 28
            echo "
\t<div class=\"alert alert-success\">";
            // line 29
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "
\t";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 34
            echo "
\t<div class=\"alert alert-success\">";
            // line 35
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "\t<table class=\"table table-bordered\">
        <thead>
         <tr>
            <th>Keyword</th>

            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        ";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["keywords"]) ? $context["keywords"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 48
            echo "        <tr>

                <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["key"], "keyword", array()), "html", null, true);
            echo "</td>

\t\t\t\t<td><a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_edit_keyword", array("keyid" => $this->getAttribute($context["key"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\"> Edit</a>
\t\t\t<a href=\"#\" onclick=\"deletefunc('";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["key"], "id", array()), "html", null, true);
            echo "')\" class=\"btn btn-danger\"> Delete</a></td>
         </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "\t\t</tbody>
\t\t</table>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:keywords.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 56,  138 => 53,  134 => 52,  129 => 50,  125 => 48,  121 => 47,  110 => 38,  101 => 35,  98 => 34,  94 => 33,  91 => 32,  82 => 29,  79 => 28,  75 => 27,  72 => 26,  63 => 23,  60 => 22,  56 => 21,  51 => 19,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }
}

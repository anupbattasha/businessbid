<?php

/* BBidsBBidsHomeBundle:User:conupdateemail.html.twig */
class __TwigTemplate_bbf17bbbe924d33cca30ef3bcaaa60cf87dab37a1847391bc09cc7d47534036f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:conupdateemail.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_enquiries($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"col-md-9 dashboard-rightpanel user-profile-basic\">
\t<div class=\"page-title\"><h1>My Account</h1></div>
\t<div class=\"row user-profile-block\">
\t\t<div class=\"user-profile-basic\">
\t\t\t
\t\t\t\t
\t\t\t<div class=\"row\">
\t\t\t ";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 13
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 14
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div> 
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "
\t";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>\t
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "\t
\t";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "
\t<div class=\"alert alert-success\">";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "\t\t\t\t<div class=\"tab-content\">
\t\t\t\t  <div class=\"tab-pane active profile-inform\" id=\"profile\">
\t\t\t\t<div class=\"col-md-8 profile-info delete-account\">
\t\t\t\t<h2>Update Email</h2><br>
\t\t\t\t<p>Please raise a support ticket <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_home");
        echo "\">click here.</a></p>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>
\t\t
\t

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:conupdateemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 33,  94 => 29,  85 => 26,  82 => 25,  78 => 24,  75 => 23,  66 => 20,  63 => 19,  59 => 18,  56 => 17,  47 => 14,  44 => 13,  40 => 12,  31 => 5,  28 => 4,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:mappedvendortocust.html.twig */
class __TwigTemplate_c25269dd2966ecd954bb21fd6e4fef22d25498d57470d13ef376a0955aa3c5cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:mappedvendortocust.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_enquiries($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">
<h1>My Customer Account</h1>
";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 7
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 8
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "
\t";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 13
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 14
            echo $context["flashMessage"];
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "
\t";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
\t<div class=\"alert alert-success\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "<div class=\"page-title\">
<h2>Matched Vendors Details<br>Job Request Number-";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["eid"]) ? $context["eid"] : null), "html", null, true);
        echo "</h2>
</div>

<table>
<tr>
<th>Vendor Name</th>
<th>Business Name</th>
<th>Email ID</th>
<th>Address</th>
<th>Phone No</th>
<th>Description</th>
<th>Action</th></tr>
";
        // line 36
        if ( !twig_test_empty((isset($context["mappedenquiry"]) ? $context["mappedenquiry"] : null))) {
            // line 37
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["mappedenquiry"]) ? $context["mappedenquiry"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                // line 38
                echo "<tr>
<td>";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "contactname", array()), "html", null, true);
                echo "</td>
<td>";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "bizname", array()), "html", null, true);
                echo "</td>
<td>";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "email", array()), "html", null, true);
                echo "</td>
<td>";
                // line 42
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "address", array()), "html", null, true);
                echo "</td>
<td>";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "smsphone", array()), "html", null, true);
                echo "</td>
<td>";
                // line 44
                if ( !twig_test_empty($this->getAttribute($context["m"], "description", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "description", array()), "html", null, true);
                    echo " ";
                } else {
                    echo " N/A ";
                }
                echo "</td>
<td><a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_review", array("authorid" => (isset($context["authorid"]) ? $context["authorid"] : null), "vendorid" => $this->getAttribute($context["m"], "id", array()), "enquiryid" => (isset($context["eid"]) ? $context["eid"] : null))), "html", null, true);
                echo "\">Write a Review</a></td>

</tr>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 50
            echo "<tr>
<td>No matched vendors are available</td>
</tr>
";
        }
        // line 54
        echo "</table>
</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:mappedvendortocust.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 54,  156 => 50,  145 => 45,  136 => 44,  132 => 43,  128 => 42,  124 => 41,  120 => 40,  116 => 39,  113 => 38,  109 => 37,  107 => 36,  92 => 24,  89 => 23,  80 => 20,  77 => 19,  73 => 18,  70 => 17,  61 => 14,  58 => 13,  54 => 12,  51 => 11,  42 => 8,  39 => 7,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}

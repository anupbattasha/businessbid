<?php

/* BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig */
class __TwigTemplate_d0a11f2d8cd9d5cb7d22f8d2bd033990fcb7710eff21a2da5edbf8b879d606aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_enquiries($context, array $blocks = array())
    {
        // line 5
        echo "

<script>
\$(document).ready(function(){
\$('#logo-form').hide();
\$('.albumform').hide();
\t\$('#changeimage').click(function(){

\t\t\$('#logo-form').show(); 
\t});
\t\$('#newalbum').click(function(){
\t\t\$('.albumform').show(); });

\t});

</script>
<div class=\"col-md-9 dashboard-rightpanel user-profile-basic\">
\t<div class=\"page-title\"><h1>My Account</h1></div>
\t<div class=\"row user-profile-block\">
\t\t<div class=\"user-profile-basic\">
\t\t\t
\t\t\t\t
\t\t\t<div class=\"row\">
\t\t\t ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 29
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 30
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div> 
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
\t";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 35
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 36
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>\t
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "\t
\t";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 41
            echo "
\t<div class=\"alert alert-success\">";
            // line 42
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t <div class=\"panel-title\"><h2>Update Password</h2></div>
\t\t\t\t\t <div class=\"tab-pane active profile-inform\" id=\"profile\">
\t\t\t\t\t\t<div class=\"col-md-8 profile-info\">
\t\t\t\t\t\t";
        // line 49
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["passwordform"]) ? $context["passwordform"] : null), 'form_start');
        echo "
\t\t\t\t\t\t";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["passwordform"]) ? $context["passwordform"] : null), 'errors');
        echo "
\t\t\t\t\t\t\t<div class=\"form-item\">";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "password", array()), 'label');
        echo "<span class=\"req\">*</span> :<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "password", array()), 'widget');
        echo "</div></div>
\t\t\t\t\t\t\t<div class=\"form-item\">";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "npassword", array()), 'label');
        echo "<span class=\"req\">*</span> :<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "npassword", array()), 'widget');
        echo " </div></div>
\t\t\t\t\t\t\t<div class=\"form-item\">";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "cpassword", array()), 'label');
        echo "<span class=\"req\">*</span> :<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "cpassword", array()), 'widget');
        echo " </div></div>
\t\t\t\t\t\t\t<div class=\"form-item pull-right text-right\">";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "Submit", array()), 'widget');
        echo "</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 56
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["passwordform"]) ? $context["passwordform"] : null), 'form_end');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>
\t\t</div>
\t\t</div>
\t

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:conpasswordcheck.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 56,  142 => 54,  136 => 53,  130 => 52,  124 => 51,  120 => 50,  116 => 49,  110 => 45,  101 => 42,  98 => 41,  94 => 40,  91 => 39,  82 => 36,  79 => 35,  75 => 34,  72 => 33,  63 => 30,  60 => 29,  56 => 28,  31 => 5,  28 => 4,  11 => 1,);
    }
}

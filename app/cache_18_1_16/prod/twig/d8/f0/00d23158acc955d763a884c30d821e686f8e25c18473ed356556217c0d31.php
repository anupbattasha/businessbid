<?php

/* TicketingTicketTicketBundle:Admin:ticketview.html.twig */
class __TwigTemplate_d8f000d23158acc955d763a884c30d821e686f8e25c18473ed356556217c0d31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "TicketingTicketTicketBundle:Admin:ticketview.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        if (((isset($context["ticketUserType"]) ? $context["ticketUserType"] : null) == 1)) {
            // line 4
            $context["issueType"] = array(0 => "Other Issue", 1 => "Account Suspension", 2 => "Customer Dispute", 3 => "Identity Verification", 4 => "Job Requests", 5 => "Payment Issue", 6 => "Reviews & Ratings", 7 => "Service Categories", 8 => "SMS Notification", 9 => "System Issue", 10 => "Lead Refund");
        } else {
            // line 6
            $context["issueType"] = array(0 => "Other Issue", 1 => "Account Suspension", 2 => "Identity Verification", 3 => "Job Requests", 4 => "Reviews & Ratings", 5 => "System Issue", 6 => "Vendor Dispute", 7 => "Lead Refund");
        }
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_pageblock($context, array $blocks = array())
    {
        // line 10
        echo "<div class=\"page-bg\">
<div class=\"container\">
\t<div class=\"page-title\"><h1>View Ticket</h1></div>
\t\t
\t<div>
\t\t";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 16
            echo "\t\t\t<div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "\t\t
\t\t";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "\t\t\t<div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "\t\t
\t\t";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "\t\t\t<div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "\t</div>\t
\t\t<div class=\"Create-ticket-form\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-10\">
\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t<label>Request ID</label>
\t\t\t\t\t\t<p>: #";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketNo", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t<label>type of Issue</label>
\t\t\t\t\t\t<p>: ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["issueType"]) ? $context["issueType"] : null), $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketTypeId", array()), array(), "array"), "html", null, true);
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t<label>Subject</label>
\t\t\t\t\t\t<p>: ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketSubject", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t<label>Status</label>
\t\t\t\t\t\t<p>: ";
        // line 47
        if (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketStatus", array()) == 1)) {
            echo " Open ";
        } elseif (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketStatus", array()) == 2)) {
            echo " Close ";
        } elseif (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketStatus", array()) == 3)) {
            echo " Pending ";
        }
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tic-section\">
\t\t\t\t\t\t<label>Last Updated</label>
\t\t\t\t\t\t";
        // line 51
        if ((twig_length_filter($this->env, (isset($context["ticketConversation"]) ? $context["ticketConversation"] : null)) > 0)) {
            // line 52
            echo "\t\t\t\t\t\t";
            $context["ticketConversationSize"] = twig_length_filter($this->env, (isset($context["ticketConversation"]) ? $context["ticketConversation"] : null));
            // line 53
            echo "\t\t\t\t\t\t<p>: ";
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["ticketConversation"]) ? $context["ticketConversation"] : null), ((isset($context["ticketConversationSize"]) ? $context["ticketConversationSize"] : null) - 1), array(), "array"), "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</p>
\t\t\t\t\t\t";
        } else {
            // line 55
            echo "\t\t\t\t\t\t<p>: ";
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</p>
\t\t\t\t\t\t";
        }
        // line 57
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t";
        // line 61
        if (($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketUploadFile", array()) != "")) {
            // line 62
            echo "\t\t\t\t\t\t<div class=\"tic-attach-section\">
\t\t\t\t\t\t\t<label>Attachments</label>
\t\t\t\t\t\t\t<p><a href=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("support/uploads/download.php?filenale=" . $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketUploadFile", array()))), "html", null, true);
            echo "\"  target=\"_blank\">Downloads</a></p>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        } else {
            // line 67
            echo "\t\t\t\t\t\t<div class=\"tic-attach-section\">
\t\t\t\t\t\t\t<label>Attachments</label>
\t\t\t\t\t\t\t<p>No attachments</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t";
        }
        // line 73
        echo "\t\t\t\t</div></div>
\t\t\t\t
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"tic-info-main\">
\t\t\t\t\t\t<div class=\"tic-info-block\">
\t\t\t\t\t\t\t<div class=\"name\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["userAccount"]) ? $context["userAccount"] : null), "contactname", array()), "html", null, true);
        echo "</div>
\t\t\t\t\t\t\t<div class=\"date\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketDatetime", array()), "date", array()), "html", null, true);
        echo "</div>
\t\t\t\t\t\t\t<div class=\"tic-info-content review\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ticketDetails"]) ? $context["ticketDetails"] : null), "ticketQuestion", array()), "html", null, true);
        echo "</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        // line 82
        if ( !twig_test_empty((isset($context["ticketConversation"]) ? $context["ticketConversation"] : null))) {
            // line 83
            echo "\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["ticketConversation"]) ? $context["ticketConversation"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["replies"]) {
                // line 84
                echo "\t\t\t\t\t\t\t\t<div class=\"tic-info-block\">
\t\t\t\t\t\t\t\t\t<div class=\"name\">";
                // line 85
                if (($this->getAttribute($context["replies"], "userId", array()) != 0)) {
                    echo "  ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["userAccount"]) ? $context["userAccount"] : null), "contactname", array()), "html", null, true);
                    echo " ";
                } else {
                    echo " Admin ";
                }
                echo "</div>
\t\t\t\t\t\t\t\t\t<div class=\"date\">";
                // line 86
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["replies"], "ticketDatetime", array()), "date", array()), "html", null, true);
                echo "</div>
\t\t\t\t\t\t\t\t\t<div class=\"tic-info-content review\">";
                // line 87
                echo twig_escape_filter($this->env, $this->getAttribute($context["replies"], "ticketResponse", array()), "html", null, true);
                echo "</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['replies'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 90
            echo "\t\t\t\t\t\t";
        }
        // line 91
        echo "\t\t\t\t\t\t<div class=\"tic-info-reply\">
\t\t\t\t\t\t\t";
        // line 92
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "
\t\t\t\t\t\t\t<a class=\"btn btn-sm btn-warning cancel\" href=\"javascript: location.replace(document.referrer);\">Cancel</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:Admin:ticketview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 92,  236 => 91,  233 => 90,  224 => 87,  220 => 86,  210 => 85,  207 => 84,  202 => 83,  200 => 82,  195 => 80,  191 => 79,  187 => 78,  180 => 73,  172 => 67,  166 => 64,  162 => 62,  160 => 61,  154 => 57,  148 => 55,  142 => 53,  139 => 52,  137 => 51,  124 => 47,  117 => 43,  110 => 39,  103 => 35,  95 => 29,  85 => 26,  81 => 25,  78 => 24,  68 => 21,  64 => 20,  61 => 19,  51 => 16,  47 => 15,  40 => 10,  37 => 9,  33 => 1,  30 => 6,  27 => 4,  25 => 3,  11 => 1,);
    }
}

<?php

/* TicketingTicketTicketBundle:vendor:ticketlist.html.twig */
class __TwigTemplate_b9f53510c94bd23ef611c6e103ccec501995581bd4c1da3e9130d3c38277afef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "TicketingTicketTicketBundle:vendor:ticketlist.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "\t<div class=\"page-bg\">\t
\t\t<div class=\"container\">
\t\t<div class=\"dashboard-panel\">
\t\t";
        // line 6
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 19
        echo "
\t\t";
        // line 20
        $this->displayBlock('enquiries', $context, $blocks);
        // line 74
        echo "\t\t</div>
\t</div>
</div>
";
    }

    // line 6
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 7
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
        echo "\">Vendor Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"lead-purchases\" href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_home");
        echo "\">Submit Support Ticket</a></li>
                            <li><a class=\"account\" href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_list");
        echo "\">All Tickets List</a></li>
                            <li><a class=\"livechat\" href=\"#\">Live Chat</a></li>
                            <li><a class=\"contact\" href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#contact\">Contact Us</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t";
    }

    // line 20
    public function block_enquiries($context, array $blocks = array())
    {
        // line 21
        echo "\t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t
\t\t\t<div class=\"page-title\"><h1>All Tickets</h1>\t</div>
\t\t\t<div class=\"search_filter\">
\t\t\t\t<p id=\"ticketidFilter\"></p>
\t\t\t\t<p id=\"dateFilter\"></p>
\t\t\t\t<p id=\"subjectFilter\"></p>
\t\t\t\t<p id=\"statusFilter\"></p>
\t\t\t\t<p id=\"priorityFilter\"></p>
\t\t\t</div>
\t\t\t<div class=\"Create-ticket-form ticket-lists\">
\t\t\t\t<table id=\"vendorTicketList\" class=\"display\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t\t\t<th>Ticket Id</th>
\t\t\t\t\t\t\t<th>Subject</th>
\t\t\t\t\t\t\t<th>Priority</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tfoot>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t\t<th>Ticket Id</th>
\t\t\t\t\t\t<th>Subject</th>
\t\t\t\t\t\t<th>Priority</th>
\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 54
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listData"]) ? $context["listData"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 55
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"sl-no\">";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"date-rcvd\">";
            // line 57
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($context["list"], "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"ticket-id\">#V<a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_view", array("id" => $this->getAttribute($context["list"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "ticketNo", array()), "html", null, true);
            echo "</a></td>
\t\t\t\t\t\t\t\t<td class=\"subject\">";
            // line 59
            echo twig_escape_filter($this->env, (((twig_length_filter($this->env, $this->getAttribute($context["list"], "ticketSubject", array())) > 50)) ? ((twig_slice($this->env, $this->getAttribute($context["list"], "ticketSubject", array()), 0, 50) . "...")) : ($this->getAttribute($context["list"], "ticketSubject", array()))), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"status\">";
            // line 60
            if (($this->getAttribute($context["list"], "ticketPriority", array()) == 1)) {
                echo "Low ";
            } elseif (($this->getAttribute($context["list"], "ticketPriority", array()) == 2)) {
                echo "Medium";
            } else {
                echo "High";
            }
            echo "</td>
\t\t\t\t\t\t\t\t<td class=\"status\"><span>";
            // line 61
            if (($this->getAttribute($context["list"], "ticketStatus", array()) == 1)) {
                echo " <span class=\"open-status\">Open</span> ";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 2)) {
                echo " <span class=\"close-status\">Close</span> ";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 3)) {
                echo " <span class=\"pending-status\">Pending</span> ";
            }
            echo "</span></td>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
<script src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-1.4.4.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilter.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/myscript.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:vendor:ticketlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 72,  209 => 71,  205 => 70,  201 => 69,  197 => 68,  191 => 64,  168 => 61,  158 => 60,  154 => 59,  148 => 58,  144 => 57,  140 => 56,  137 => 55,  120 => 54,  85 => 21,  82 => 20,  73 => 14,  68 => 12,  64 => 11,  60 => 10,  55 => 7,  52 => 6,  45 => 74,  43 => 20,  40 => 19,  38 => 6,  33 => 3,  30 => 2,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:viewuser.html.twig */
class __TwigTemplate_8f6a94cc3424956cf9010e8854ea256ed6769f6be58a58465cb2ae52f22b6974 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:viewuser.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<script>
\$(document).ready(function(){
  \$(\".cat\").click(function(){
   var catid \t= \$(this).attr('data-catid');
   var catname \t= \$(this).attr('data-catname');
   \$('#form_categoryid').val(catid);
   \$('#form_category').val(catname);
  });
});
</script>

<div class=\"page-bg\">
\t<div class=\"container\">
\t\t<div class=\"page-title\"><h1>User Information</h1></div>
\t\t<div class=\"\">
\t\t\t";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "
\t\t\t<div class=\"alert alert-success\">";
            // line 22
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "\t\t</div>
\t\t<div class=\"dashboard-container vendor-user user-information\">
\t\t\t";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 28
            echo "\t\t\t\t<div class=\"field-item\"><label>Date Received</label><div class=\"field-value\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo " </div></div>
\t\t\t\t<div class=\"field-item\"><label>Profile</label><div class=\"field-value\">";
            // line 29
            if (($this->getAttribute($context["user"], "profileid", array()) == 1)) {
                echo " Admin ";
            } elseif (($this->getAttribute($context["user"], "profileid", array()) == 2)) {
                echo " Consumer ";
            } elseif (($this->getAttribute($context["user"], "profileid", array()) == 3)) {
                echo " Vendor ";
            }
            echo " </div></div>
\t\t\t\t<div class=\"field-item\"><label>Name</label><div class=\"field-value\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "contactname", array()), "html", null, true);
            echo " </div></div>
\t\t\t\t<div class=\"field-item\"><label>Email</label><div class=\"field-value\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo " </div></div>
\t\t\t\t<div class=\"field-item\"><label>Contact Number</label><div class=\"field-value\">";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "smsphone", array()), "html", null, true);
            echo " </div></div>
\t\t\t\t";
            // line 33
            if ( !twig_test_empty($this->getAttribute($context["user"], "address", array()))) {
                // line 34
                echo "\t\t\t\t\t<div class=\"field-item\"><label>Address</label><div class=\"field-value\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "address", array()), "html", null, true);
                echo " </div></div>
\t\t\t\t";
            }
            // line 36
            echo "\t\t\t\t";
            if ( !twig_test_empty($this->getAttribute($context["user"], "bizname", array()))) {
                // line 37
                echo "\t\t\t\t\t<div class=\"field-item\"><label>Business/Company name</label><div class=\"field-value\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "bizname", array()), "html", null, true);
                echo " </div></div>
\t\t\t\t";
            }
            // line 39
            echo "\t\t\t\t<div class=\"field-item\"><label>Status</label><div class=\"field-value\"> ";
            if (($this->getAttribute($context["user"], "status", array()) == 1)) {
                echo " Active ";
            } elseif (($this->getAttribute($context["user"], "status", array()) == 2)) {
                echo " Inactive ";
            } elseif (($this->getAttribute($context["user"], "status", array()) == 3)) {
                echo " Blocked ";
            }
            echo " </div></div>
\t\t\t\t
\t\t\t\t<div class=\"field-item\"><label>Updated Date</label><div class=\"field-value\">";
            // line 41
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "updated", array()), "Y-m-d h:i:s"), "html", null, true);
            echo " </div></div>
\t\t\t\t<div class=\"field-item\"><label>Last Access Date</label><div class=\"field-value\"> ";
            // line 42
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "lastaccess", array()), "Y-m-d h:i:s"), "html", null, true);
            echo " </div></div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t\t";
        if ( !twig_test_empty((isset($context["vendorcat"]) ? $context["vendorcat"] : null))) {
            // line 45
            echo "\t\t\t\t<div class=\"field-item\"><label>Categories</label>
\t\t\t\t";
            // line 46
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vendorcat"]) ? $context["vendorcat"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 47
                echo "\t\t\t\t\t<div class=\"cat\" data-catid=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "id", array()), "html", null, true);
                echo "\" data-catname=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
                echo "\"><div class=\"field-value\"><a href=\"#\" data-toggle=\"modal\" data-target=\"#popup\" class=\"category_";
                echo twig_escape_filter($this->env, (($this->getAttribute($context["c"], "id", array()) . "_") . $this->getAttribute($context["c"], "category", array())), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
                echo "</a></div></div>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "\t\t\t\t</div>
\t\t\t";
        }
        // line 51
        echo "\t\t\t<div class=\"modal fade\" id=\"popup\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t\t\t<div class=\"modal-dialog\">
\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
\t\t\t\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Leads Credit</h4>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 59
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t\t\t\t\t<div class=\"catgory-popup\">
\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t<div class=\"control\">";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "credit", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "credit", array()), 'widget');
        echo "</div>
\t\t\t\t\t\t<div class=\"control\">";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "categoryid", array()), 'widget');
        echo "</div>
\t\t\t\t\t\t<div class=\"control\">";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "submit", array()), 'widget');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        // line 70
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:viewuser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 70,  201 => 67,  195 => 64,  191 => 63,  185 => 62,  179 => 59,  169 => 51,  165 => 49,  150 => 47,  146 => 46,  143 => 45,  140 => 44,  132 => 42,  128 => 41,  116 => 39,  110 => 37,  107 => 36,  101 => 34,  99 => 33,  95 => 32,  91 => 31,  87 => 30,  77 => 29,  72 => 28,  68 => 27,  64 => 25,  55 => 22,  52 => 21,  48 => 20,  31 => 5,  28 => 4,  11 => 1,);
    }
}

<?php

/* ::home.html.twig */
class __TwigTemplate_9cb4048f1cfe49646ba0279ec4d72e51220331a9d73ced893873a5653e857b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'googleanalatics' => array($this, 'block_googleanalatics'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html class=\"no-js\" lang=\"\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\"/>
        <title>Hire a Quality Service Professional at a Fair Price</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>

        <link rel=\"apple-touch-icon\" href=\"apple-touch-icon.png\">
        <!-- Place favicon.ico in the root directory -->
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/home-style.css"), "html", null, true);
        echo "\">
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">

        <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/modernizr-2.6.2.min.js"), "html", null, true);
        echo "\"></script>


        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
        ";
        // line 21
        echo "
  <style>
    #sticker {
    visibility:visible
      margin: 0 auto;
      text-shadow: 0 1px 1px rgba(0,0,0,.2);
    }
  </style>


        ";
        // line 31
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 37
        echo "    </head>
    ";
        // line 38
        ob_start();
        // line 39
        echo "    <body>
        ";
        // line 40
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "uid"), "method");
        // line 41
        echo "        ";
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "pid"), "method");
        // line 42
        echo "        ";
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "email"), "method");
        // line 43
        echo "        ";
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "name"), "method");
        // line 44
        echo "        <!--[if lt IE 8]>
            <p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"\"><img src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Business Bid Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 58
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 59
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 61
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 62
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 65
(isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 66
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 68
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
            echo "</span></li>
                        ";
            // line 70
            echo "

       <li><a class='button-login naked' href=\"";
            // line 72
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
      <li class=\"my-account\"><img src=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>

                        </ul>
                    </div>
                    ";
        }
        // line 79
        echo "                </div>
            </div>
        </div>
        ";
        // line 82
        list($context["serviceCatLinks"], $context["reviewCatLinks"], $context["resourceCatLinks"], $context["resource"]) =         array("", "", "", "");
        // line 83
        echo "        ";
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 84
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 85
                echo "                ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 86
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : $this->getContext($context, "serviceCatLinks")) . "<li><a class=\"all-category\" href=\"") . $this->env->getExtension('routing')->getPath("bizbids_vendor_search")) . "\">See All Categories</a></li>");
                    // line 87
                    echo "
                    ";
                    // line 88
                    $context["reviewCatLinks"] = ((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : $this->getContext($context, "reviewCatLinks")) . "<li><a class=\"all-category\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4")) . "\">See All Reviews</a></li>");
                    // line 89
                    echo "                    ";
                    $context["resourceCatLinks"] = ((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : $this->getContext($context, "resourceCatLinks")) . "<li><a class=\"all-category\" href=\"http://www.businessbid.ae/resource_centre/home-2\">See All Articles</a></li>");
                    // line 90
                    echo "                ";
                } else {
                    // line 91
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : $this->getContext($context, "serviceCatLinks")) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Quotes</a></li>");
                    // line 92
                    echo "
                    ";
                    // line 93
                    $context["reviewCatLinks"] = ((((((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : $this->getContext($context, "reviewCatLinks")) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Reviews</a></li>");
                    // line 94
                    echo "                    ";
                    if (twig_test_empty($this->getAttribute($context["category"], "resourceLink", array()))) {
                        // line 95
                        echo "                        ";
                        $context["resource"] = "home-2";
                        // line 96
                        echo "                    ";
                    } else {
                        // line 97
                        echo "                        ";
                        $context["resource"] = $this->getAttribute($context["category"], "resourceLink", array());
                        // line 98
                        echo "                    ";
                    }
                    // line 99
                    echo "                    ";
                    $context["resourceCatLinks"] = ((((((((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : $this->getContext($context, "resourceCatLinks")) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"http://www.businessbid.ae/resource_centre/") . (isset($context["resource"]) ? $context["resource"] : $this->getContext($context, "resource"))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Articles</a></li>");
                    // line 100
                    echo "                ";
                }
                // line 101
                echo "            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "        ";
        } else {
            // line 103
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 104
                echo "            ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 105
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : $this->getContext($context, "serviceCatLinks")) . "<li><a class=\"all-category\" href=\"") . $this->env->getExtension('routing')->getPath("bizbids_vendor_search")) . "\">See All Categories</a></li>");
                    // line 106
                    echo "
                    ";
                    // line 107
                    $context["reviewCatLinks"] = ((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : $this->getContext($context, "reviewCatLinks")) . "<li><a class=\"all-reviews\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4")) . "\">See All Reviews</a></li>");
                    // line 108
                    echo "                    ";
                    $context["resourceCatLinks"] = ((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : $this->getContext($context, "resourceCatLinks")) . "<li><a class=\"all-articles\" href=\"http://www.businessbid.ae/resource_centre/home-2\">See All Articles</a></li>");
                    // line 109
                    echo "                ";
                } else {
                    // line 110
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : $this->getContext($context, "serviceCatLinks")) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Quotes</a></li>");
                    // line 111
                    echo "
                    ";
                    // line 112
                    $context["reviewCatLinks"] = ((((((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : $this->getContext($context, "reviewCatLinks")) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Reviews</a></li>");
                    // line 113
                    echo "                    ";
                    if (twig_test_empty($this->getAttribute($context["category"], "resourceLink", array()))) {
                        // line 114
                        echo "                        ";
                        $context["resource"] = "home-2";
                        // line 115
                        echo "                    ";
                    } else {
                        // line 116
                        echo "                        ";
                        $context["resource"] = $this->getAttribute($context["category"], "resourceLink", array());
                        // line 117
                        echo "                    ";
                    }
                    // line 118
                    echo "                    ";
                    $context["resourceCatLinks"] = ((((((((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : $this->getContext($context, "resourceCatLinks")) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"http://www.businessbid.ae/resource_centre/") . (isset($context["resource"]) ? $context["resource"] : $this->getContext($context, "resource"))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Articles</a></li>");
                    // line 119
                    echo "                ";
                }
                // line 120
                echo "            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 121
            echo "        ";
        }
        // line 122
        echo "        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 127
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 129
        echo (isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : $this->getContext($context, "serviceCatLinks"));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 132
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 134
        echo (isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : $this->getContext($context, "reviewCatLinks"));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource_centre/home-2/\">resource centre</a>
                            <ul>
                                ";
        // line 139
        echo (isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : $this->getContext($context, "resourceCatLinks"));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 144
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 145
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 147
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 150
(isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 151
            echo "                <div class=\"register_menu1\">
     <img src=\"";
            // line 152
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 155
        echo "            </div>
        </div>
        <div class=\"one_area\">
            <div class=\"one_child_area\">
                <div class=\"inner_one_area_opacity\"></div>
                <div class=\"inner_one_area\">
                    <h3>Need to hire a quality service professional at a fair price? Ask us.</h3>
                    <h4>What service do you need? </h4>
                    <div class=\"home_top_sear_btn_n_plac\">
                        <form method=\"GET\" action=\"";
        // line 164
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
                        <input id='category' type=\"text\" name=\"category\" placeholder=\"eg. cleaner, photographer, handyman etc.\">
                        ";
        // line 167
        echo "                        <button type=\"submit\">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"two_area\">
            <div class=\"two_child_area\">
                <div class=\"inner_two_area_coain\">
                    <h4>COMPLETELY FREE TO USE</h4>
                </div>
                <div class=\"inner_two_area_praper\">
                    <h4>COMPARE COMPETITIVE QUOTES</h4>
                </div>
                <div class=\"inner_two_area_star\">
                    <h4>CHOOSE RATED PROFESSIONALS</h4>
                </div>
            </div>
        </div>
        <div class=\"stick_bg_test\">
            <div id=\"sticker\" class=\"sticky_top_menu_area\">
                <div class=\"sticky_top_menu_area_inner\">
                   <div class=\"sticky_heading\">
                       <h2>COMPARE UP TO 3 QUOTES INSTANTLY</h2>
                   </div>
                    <div class=\"sticky_table_menu\">
                        <select name=\"categories\">
                            ";
        // line 194
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 195
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 197
        echo "                        </select>
                    </div>
                    <div class=\"sticky_table_menu_btn\"><a href=\"#\" id=\"get_quotes\">GET QUOTES</a></div>
                </div>
            </div>
        </div>
        <div class=\"three_area\">
            <div class=\"three_child_area\">
                <h2>How does BusinessBid work?</h2>
                <div class=\"inner_three_area\">
                    <img src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/step_one.png"), "html", null, true);
        echo "\" alt=\"Step One\">
                    <h3>Tell us what you need?</h3>
                    <p>Fill out a quote request form.</p>
                </div>
                <div class=\"three_arrow\"><img src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo "\" alt=\"Arrow\"></div>
                <div class=\"inner_three_area\">
                    <img src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/step_two.png"), "html", null, true);
        echo "\" alt=\"Step Two\">
                    <h3>Receive multiple quotes</h3>
                    <p>Compare prices and reviews.</p>
                </div>
                <div class=\"three_arrow\"><img src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo "\" alt=\"Arrow\"></div>
                <div class=\"inner_three_area\">
                    <img src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/stpe_three.png"), "html", null, true);
        echo "\" alt=\"Step Three\">
                    <h3>Hire the best professional</h3>
                    <p>Once done, rate your experience</p>
                </div>
            </div>
        </div>
        <div class=\"fore_area\">
            <div class=\"fore_child_area\">
                <h2>Popular Categories</h2>
                <div class=\"inner_four_area\">
                    <ul>
                        <li class=\"coain\"><a href=\"/resource_centre/aa-2/\">Accounting & Auditing Services</a></li>
                        <li class=\"interior\"><a href=\"/resource_centre/interior\">Interior Design & Fit Out</a></li>
                        <li class=\"signage\"><a href=\"/resource_centre/signage-signboards/\">Signage and Signboards</a></li>
                        <li class=\"catering\"><a href=\"/resource_centre/catering-4/\">Catering Services</a></li>
                    </ul>
                </div>
                <div class=\"inner_four_area middle\">
                    <ul>
                        <li class=\"support\"><a href=\"/resource_centre/it-support\">IT Support</a></li>
                        <li class=\"landscaping\"><a href=\"/resource_centre/ig\">Landscaping and Gardens</a></li>
                        <li class=\"photography\"><a href=\"/resource_centre/photography\">Photography and Video</a></li>
                        <li class=\"offset\"><a href=\"/resource_centre/printing_companies\">Offset Printing</a></li>
                    </ul>
                </div>
                <div class=\"inner_four_area right\">
                    <ul>
                        <li class=\"residential\"><a href=\"/resource_centre/cleaning\">Residential Cleaning</a></li>
                        <li class=\"commercial\"><a href=\"/resource_centre/cleaning\">Commercial Cleaning</a></li>
                        <li class=\"maintenance\"><a href=\"/resource_centre/maintenance\">Maintenance</a></li>
                        <li class=\"control\"><a href=\"/resource_centre/\">Pest Control</a></li>
                    </ul>
                </div>
                <div class=\"fore_btn\">
                    <a href=\"";
        // line 253
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">VIEW ALL CATEGORIES</a>
                </div>
            </div>
        </div>
        <div class=\"five_area\">
            <div class=\"five_child_area\">
                <div class=\"inner_five_text\">
                    <h1>HIRE RATED PROFESSIONALS</h1>
                    <p>BusinessBid screens all service professionals to ensure they are licensed and certified to carry out their activity and have a good reputation in their industry. In addition, customers who have previously hired a vendor are able to rate the overall experience. As a BusinessBid user you have access to thousands of vendor reviews and ratings you can view before hiring a service professional.</p>               <p>Whether you need access to <a href=\"http://www.businessbid.ae/resource_centre/aa-2/\">audit firms</a>, <a href=\"http://www.businessbid.ae/resource_centre/interior/\">interior design companies</a>, <a href=\"http://www.businessbid.ae/resource_centre/catering-4/\">catering businesses</a>, or <a href=\"http://www.businessbid.ae/resource_centre/it-support/\">IT services</a>, we can connect you to screened and approved professionals who you can trust to get things done.</p>
                    <div class=\"five_btn\">
                        <a href=\"";
        // line 263
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Check Reviews & Ratings</a>
                    </div>
                </div>
                <div class=\"inner_five_img\">
                    <img src=\"";
        // line 267
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/window.png"), "html", null, true);
        echo "\" alt=\"Browser Window\">
                </div>
            </div>
        </div>
        <div class=\"six_area\">
            <div class=\"six_child_area\">
                <div class=\"inner_six_area_opaciry\"></div>
                <div class=\"inner_six_area\">
                    <h2>QUICK QUOTES THAT SAVE YOU MONEY</h2>
                    <p>All you need to do is fill out a simple form telling us what you need and we will have the most suitable professionals quote you for your project. You will usually have 3 service professionals contact you so you can compare quotes and ensure you get a great deal. Typically vendors will contact you within 30 minutes or less!</p>
                    <p>At BusinessBid we pride ourselves in being able to provide customers with multiple quotes at fast turnaround times.</p>
                    <form name=\"search\" method=\"GET\" action=\"";
        // line 278
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
                    <div class=\"six_tabil\">
                        <select name=\"category\">
                            ";
        // line 281
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 282
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 284
        echo "                        </select>
                    </div>
                    <input type=\"submit\" class=\"six_btn\" name=\"submit\" value=\"START FREE QUOTES\">
                    </form>
                </div>
            </div>
        </div>
        <div class=\"seven_area\">
            <div class=\"seven_child_area\">
                <h2>ARE YOU A SERVICE PROFESSIONAL?</h2>
                <p>Learn more on how joining BusinessBid can benefit your business.</p>
                <div class=\"seven_btn\"><a href=\"";
        // line 295
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">FIND OUT MORE</a></div>
            </div>
        </div>
        <div class=\"main_footer_area\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>

                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 317
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 318
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 319
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 320
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 321
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 327
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 328
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 337
        echo $this->env->getExtension('routing')->getPath("bizbids_are_you_a_vendor");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 338
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 339
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 340
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 346
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 348
        echo "                            <li><a href=\"/resource_centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 349
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 357
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"#\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 359
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 364
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource_centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 401
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 403
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>
               <div class=\"copy_rightt\">
                <p>© <span>2015 BusinessBid DMCC</span> </p>
            </div>
        </div>

        ";
        // line 413
        echo "        ";
        // line 439
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
        <script>
        \$(window).load(function(){
          \$(\"#sticker\").sticky({ topSpacing: 0, center:true, className:\"hey\" });
        });
      </script>
        <script type=\"text/javascript\">
        \$(document).ready(function(){
            \$('select[name=\"categories\"]').bind('change',function () {
            ";
        // line 448
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 449
            echo "                var cat = \$(this).val(), urlPath = \"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => "cid"));
            echo "\";
            ";
        } else {
            // line 451
            echo "                var cat = \$(this).val(), urlPath = \"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => "cid"));
            echo "\";
            ";
        }
        // line 453
        echo "                urlPath = urlPath.replace(\"cid\", cat);
                \$(\"#get_quotes\").attr('href', urlPath);
            });

            ";
        // line 457
        if (array_key_exists("keyword", $context)) {
            // line 458
            echo "            window.onload = function(){
                document.getElementById(\"submitButton\").click();
            }
            ";
        }
        // line 462
        echo "            var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

            \$('#category').autocomplete({
                source: function( request, response ) {
                \$.ajax({
                    url : realpath+\"ajax-info.php\",
                    dataType: \"json\",
                    data: {
                       name_startsWith: request.term,
                       type: 'category'
                    },
                    success: function( data ) {
                        response( \$.map( data, function( item ) {
                            return {
                                label: item,
                                value: item
                            }
                        }));
                    },
                    select: function(event, ui) {
                        alert( \$(event.target).val() );
                        console.log( \$(event).val() );
                    }
                });
                },autoFocus: true,minLength: 2
            });
        });
        </script>
        <div id=\"fb-root\"></div>
        <script type=\"text/javascript\">
        window.fbAsyncInit = function() {
            FB.init({
            appId      : '754756437905943', // replace your app id here
            status     : true,
            cookie     : true,
            xfbml      : true
            });
        };
        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = \"//connect.facebook.net/en_US/all.js\";
            ref.parentNode.insertBefore(js, ref);
        }(document));

        function FBLogin(){
            FB.login(function(response){
                if(response.authResponse){
                    window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
                }
            }, {scope: 'email,user_likes'});
        }
        </script>
        <script src=\"";
        // line 516
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/jquery.sticky.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 517
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 518
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>

        <!-- Pure Chat Snippet -->
        <script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();
        </script>
        <!-- End Pure Chat Snippet -->
    </body>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 526
        echo "</html>
";
    }

    // line 31
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 32
        echo "        <!-- Google Analytics -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-59054368-1', 'auto');ga('send', 'pageview');
        </script>
        ";
    }

    public function getTemplateName()
    {
        return "::home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  911 => 32,  908 => 31,  903 => 526,  892 => 518,  888 => 517,  884 => 516,  828 => 462,  822 => 458,  820 => 457,  814 => 453,  808 => 451,  802 => 449,  800 => 448,  787 => 439,  785 => 413,  773 => 403,  768 => 401,  728 => 364,  720 => 359,  716 => 358,  712 => 357,  701 => 349,  698 => 348,  694 => 346,  685 => 340,  681 => 339,  677 => 338,  673 => 337,  669 => 336,  658 => 328,  654 => 327,  645 => 321,  641 => 320,  637 => 319,  633 => 318,  629 => 317,  604 => 295,  591 => 284,  580 => 282,  576 => 281,  570 => 278,  556 => 267,  549 => 263,  536 => 253,  499 => 219,  494 => 217,  487 => 213,  482 => 211,  475 => 207,  463 => 197,  452 => 195,  448 => 194,  419 => 167,  414 => 164,  403 => 155,  389 => 152,  386 => 151,  384 => 150,  378 => 147,  374 => 145,  372 => 144,  364 => 139,  356 => 134,  351 => 132,  345 => 129,  340 => 127,  336 => 126,  330 => 122,  327 => 121,  313 => 120,  310 => 119,  307 => 118,  304 => 117,  301 => 116,  298 => 115,  295 => 114,  292 => 113,  290 => 112,  287 => 111,  284 => 110,  281 => 109,  278 => 108,  276 => 107,  273 => 106,  270 => 105,  267 => 104,  249 => 103,  246 => 102,  232 => 101,  229 => 100,  226 => 99,  223 => 98,  220 => 97,  217 => 96,  214 => 95,  211 => 94,  209 => 93,  206 => 92,  203 => 91,  200 => 90,  197 => 89,  195 => 88,  192 => 87,  189 => 86,  186 => 85,  168 => 84,  165 => 83,  163 => 82,  158 => 79,  141 => 73,  137 => 72,  133 => 70,  129 => 68,  125 => 66,  123 => 65,  117 => 62,  111 => 61,  107 => 59,  105 => 58,  96 => 52,  86 => 44,  83 => 43,  80 => 42,  77 => 41,  75 => 40,  72 => 39,  70 => 38,  67 => 37,  65 => 31,  53 => 21,  46 => 16,  41 => 14,  37 => 13,  33 => 12,  20 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Admin:orderlist.html.twig */
class __TwigTemplate_6b8fbbb23e9ad9d4dfc50d229188e84ec93c4688f23aed18e7f43cfe4b1b3f75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:orderlist.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "
<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\">
<script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
<script src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>
<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
<script>
\$(function() {
\t\$(\"#datepicker3\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true,
\t\tmaxDate: '+2y',
\t\tyearRange: '2014:2024',
\t\tonSelect: function(date){
\t\t\tvar selectedDate = new Date(date);
\t\t\tvar msecsInADay = 86400000;
\t\t\tvar endDate = new Date(selectedDate.getTime() + msecsInADay);

\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"minDate\", endDate );
\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"maxDate\", '+2y' );
\t\t}
\t});

\t\$(\"#datepicker4\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true
\t});

\tvar availableVendors = ";
        // line 31
        echo twig_jsonencode_filter((isset($context["accountList"]) ? $context["accountList"] : null));
        echo ";
    \$( \"#form_vendorname\" ).autocomplete({
      source: availableVendors
    });
});
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">

<div class=\"page-title\"><h1>Vendor Orders</h1></div>
";
        // line 41
        if ((isset($context["count"]) ? $context["count"] : null)) {
            // line 42
            echo "<div class=\"row text-right page-counter-history\"><div class=\"counter-box\">Now showing : ";
            echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
            echo " records</div></div>
";
        }
        // line 44
        echo "<div>
\t";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 46
            echo "
\t<div class=\"alert alert-danger\">";
            // line 47
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
\t";
        // line 51
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 52
            echo "
\t<div class=\"alert alert-success\">";
            // line 53
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "
\t";
        // line 57
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 58
            echo "
\t<div class=\"alert alert-success\">";
            // line 59
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "</div>
<div class=\"admin_filter\">
\t";
        // line 64
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t<div class=\"col-md-3 side-clear\">  ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendorname", array()), 'widget');
        echo "</div>
\t<div class=\"col-md-3 clear-right\">  ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
\t<div class=\"col-md-2 clear-right\">  ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'widget', array("id" => "datepicker3"));
        echo "</div>
\t<div class=\"col-md-2 clear-right\">";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'widget', array("id" => "datepicker4"));
        echo " </div>
\t<div class=\"col-md-2 top-space clear-right\"> ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "filter", array()), 'widget', array("label" => "Search"));
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo " </div>
</div>
<div class=\"latest-orders\">
<table>
<thead><tr><th>Date Recieved</th>
<th>Order Number</th>
<th>Vendor Name</th>
<th>Business Name</th>
<th>Category</th>
<th>Lead Package</th>
<th>Payment Type</th>
<th>Amount</th>
<th>Status</th>
</tr></thead>
";
        // line 83
        if ( !twig_test_empty((isset($context["orders"]) ? $context["orders"] : null))) {
            // line 84
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
                // line 85
                echo "
\t<tr>
\t<td>";
                // line 87
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["order"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td>
\t<td><a href=\"";
                // line 88
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_order_view", array("id" => $this->getAttribute($context["order"], "id", array()), "type" => $this->getAttribute($context["order"], "payoption", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "ordernumber", array()), "html", null, true);
                echo "</a></td>
\t<td>";
                // line 89
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "contactname", array()), "html", null, true);
                echo "</td>
\t<td>";
                // line 90
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "bizname", array()), "html", null, true);
                echo "</td>
\t<td>";
                // line 91
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "category", array()), "html", null, true);
                echo "</td>
\t<td>";
                // line 92
                if (($this->getAttribute($context["order"], "leadpack", array()) == 10)) {
                    echo "Bronze";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 25)) {
                    echo "Silver ";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 50)) {
                    echo " Gold ";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 100)) {
                    echo " Platinum ";
                }
                echo "</td>
\t<td>";
                // line 93
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "payoption", array()), "html", null, true);
                echo "</td>
\t<td>";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "amount", array()), "html", null, true);
                echo "</td>
\t<td>";
                // line 95
                if ((($this->getAttribute($context["order"], "status", array()) == 0) || ($this->getAttribute($context["order"], "status", array()) == 3))) {
                    echo " <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_approve_order", array("id" => $this->getAttribute($context["order"], "id", array()))), "html", null, true);
                    echo "\" onclick=\"return confirm('Are you sure to approve order ?')\">Approve</a> ";
                } elseif (($this->getAttribute($context["order"], "status", array()) == 1)) {
                    echo " Approved ";
                }
                echo "</td></tr>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 99
            echo "<tr>
<td>
No Data Available
</td>
</tr>
";
        }
        // line 105
        echo "</table>
";
        // line 106
        if ((isset($context["count"]) ? $context["count"] : null)) {
            // line 107
            echo "<div class=\"row text-right \">
\t<div class=\"counter-box top-space\">
\t";
            // line 109
            $context["Epage_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 10)));
            // line 110
            echo "<ul class=\"pagination\">

  <li><a href=\"";
            // line 112
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_orders", array("offset" => 1));
            echo "\">&laquo;</a></li>
";
            // line 113
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 114
                echo "
  <li><a href=\"";
                // line 115
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_orders", array("offset" => $context["i"])), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</a></li>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 118
            echo "
  <li><a href=\"";
            // line 119
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_orders", array("offset" =>  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null))), "html", null, true);
            echo "\">&raquo;</a></li>
</ul>
</div>
</div>
";
        }
        // line 124
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:orderlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  314 => 124,  306 => 119,  303 => 118,  292 => 115,  289 => 114,  285 => 113,  281 => 112,  277 => 110,  275 => 109,  271 => 107,  269 => 106,  266 => 105,  258 => 99,  242 => 95,  238 => 94,  234 => 93,  222 => 92,  218 => 91,  214 => 90,  210 => 89,  204 => 88,  200 => 87,  196 => 85,  192 => 84,  190 => 83,  172 => 69,  166 => 68,  160 => 67,  154 => 66,  148 => 65,  144 => 64,  140 => 62,  131 => 59,  128 => 58,  124 => 57,  121 => 56,  112 => 53,  109 => 52,  105 => 51,  102 => 50,  93 => 47,  90 => 46,  86 => 45,  83 => 44,  75 => 42,  73 => 41,  60 => 31,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:viewgallery.html.twig */
class __TwigTemplate_0dc23eafebda84f45bc01587d173f67b9280535609ad7d680c9f572c1fba8cc2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:viewgallery.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        // line 7
        echo "<div>
        <span>
";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albuminfo"]) ? $context["albuminfo"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 10
            echo "
              <span> Album Name: ";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "albumname", array()), "html", null, true);
            echo "</span>
              <span> Album Summary: ";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "summary", array()), "html", null, true);
            echo "</span>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "<span>
";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 16
            echo "\t";
            $context["albumid"] = $this->getAttribute($context["p"], "albumid", array());
            // line 17
            echo "\t";
            $context["photoid"] = $this->getAttribute($context["p"], "photoid", array());
            // line 18
            echo "\t";
            $context["ext"] = $this->getAttribute($context["p"], "phototag", array());
            // line 19
            echo "\t";
            $context["imagepath"] = (((("http://115.124.120.36/stagging/web/gallery/" . (isset($context["albumid"]) ? $context["albumid"] : null)) . "/") . (isset($context["albumid"]) ? $context["albumid"] : null)) . $this->getAttribute($context["loop"], "index", array()));
            // line 20
            echo "\t<img src=\"";
            echo twig_escape_filter($this->env, (isset($context["imagepath"]) ? $context["imagepath"] : null), "html", null, true);
            echo "\" height=\"300\" width=\"200\">
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "</span>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:viewgallery.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 22,  92 => 20,  89 => 19,  86 => 18,  83 => 17,  80 => 16,  63 => 15,  60 => 14,  52 => 12,  48 => 11,  45 => 10,  41 => 9,  37 => 7,  34 => 6,  29 => 3,  11 => 1,);
    }
}

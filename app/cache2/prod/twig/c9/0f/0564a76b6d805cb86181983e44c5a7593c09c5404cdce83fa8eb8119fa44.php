<?php

/* BBidsBBidsHomeBundle:Admin:subcategories.html.twig */
class __TwigTemplate_c90f0564a76b6d805cb86181983e44c5a7593c09c5404cdce83fa8eb8119fa44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:subcategories.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">

 function deletefunc(subid, subcatname){
\tvar con = confirm(\"Are you sure to delete  \"+subcatname+ \"? The record once deleted cannot be retrived back!\");
\tif(con){
\t\tvar url = \"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_delete_category", array("categoryid" => "SUBCATID"));
        echo "\";
\t\tvar urlset = url.replace('SUBCATID', subid);
\t\t
\t\t\twindow.open(urlset,\"_self\");
\t\treturn true;
\t}else{
\t\treturn false;
\t}
 }
</script>
<div class=\"inner_container container\">
<div class=\"\"><h3>Sub Category List <span class=\"pull-right\"><a class=\"btn btn-warning\" href=\"javascript:window.history.go(-1)\">Back</a> <a  class=\"btn btn-success\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_add_subcategory", array("categoryid" => (isset($context["categoryid"]) ? $context["categoryid"] : null))), "html", null, true);
        echo "\">Add Sub Category</a></span></h3> </div>
\t<div class=\"segment-content row\">
\t";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 23
            echo "
\t<div class=\"alert alert-danger\">";
            // line 24
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "\t
\t";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 29
            echo "
\t<div class=\"alert alert-success\">";
            // line 30
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 34
            echo "
\t<div class=\"alert alert-success\">";
            // line 35
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "\t<table class=\"table table-bordered\">
        <thead>
         <tr>
            <th>Sub Category</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>\t
\t";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subcategories"]) ? $context["subcategories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["sub"]) {
            // line 48
            echo "\t<tr>
\t\t<td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["sub"], "category", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 50
            if (($this->getAttribute($context["sub"], "status", array()) == 1)) {
                echo " Active ";
            } else {
                echo " Inactive ";
            }
            echo "</td>
\t\t<td>
\t\t\t<a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_edit_subcategory", array("categoryid" => $this->getAttribute($context["sub"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\"> Edit</a>
\t\t\t<a href=\"#\" onclick=\"deletefunc('";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["sub"], "id", array()), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sub"], "category", array()), "html", null, true);
            echo "')\" class=\"btn btn-danger\"> Delete</a>
\t\t</td>
\t</tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "\t</tbody>
\t</table>
</div>
</div> \t
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:subcategories.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 57,  144 => 53,  140 => 52,  131 => 50,  127 => 49,  124 => 48,  120 => 47,  109 => 38,  100 => 35,  97 => 34,  92 => 33,  83 => 30,  80 => 29,  76 => 28,  73 => 27,  64 => 24,  61 => 23,  57 => 22,  52 => 20,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }
}

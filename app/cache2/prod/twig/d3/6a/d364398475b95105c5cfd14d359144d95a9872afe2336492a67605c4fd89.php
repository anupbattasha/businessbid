<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<head>
";
        // line 3
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 4
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 5
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 6
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 7
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 8
        echo "
<title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        // line 109
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 115
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 116
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 118
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 119
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 121
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 124
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 126
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">

    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 159
        $this->displayBlock('customcss', $context, $blocks);
        // line 162
        echo "
";
        // line 163
        $this->displayBlock('javascripts', $context, $blocks);
        // line 306
        echo "
";
        // line 307
        $this->displayBlock('customjs', $context, $blocks);
        // line 310
        echo "
";
        // line 311
        $this->displayBlock('jquery', $context, $blocks);
        // line 314
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 320
        echo "</head>
";
        // line 321
        ob_start();
        // line 322
        echo "<body>

";
        // line 324
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 325
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 326
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 327
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 328
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"\"><img src=\"/stagging/web/img/logo.png\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 339
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 340
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 342
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 343
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 346
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 347
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 349
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 350
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 351
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 356
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 363
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 364
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 366
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 369
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 371
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource_centre/home-2/\">resource centre</a>
                            <ul>
                                ";
        // line 376
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 381
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 382
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 384
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 387
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 388
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 389
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 392
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 399
        $this->displayBlock('maincontent', $context, $blocks);
        // line 630
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 633
        $this->displayBlock('footer', $context, $blocks);
        // line 746
        echo "    <!-- footer block ends -->
</div>";
        // line 749
        $this->displayBlock('footerScript', $context, $blocks);
        // line 753
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 760
        echo "</html>
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        // line 10
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 11
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_d5e2b990e3c0c9fa1396daa5445141715aeda070ee5d127d40ac52afc7b3cf5c = $this->getAttribute($this->getAttribute(        // line 12
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_cb6ad523551a8edc7afd071e2a833c7e9ce3e34a5f5619b8bc4c27301182abee = "http://www.businessbid.ae/contactus") && ('' === $__internal_cb6ad523551a8edc7afd071e2a833c7e9ce3e34a5f5619b8bc4c27301182abee || 0 === strpos($__internal_d5e2b990e3c0c9fa1396daa5445141715aeda070ee5d127d40ac52afc7b3cf5c, $__internal_cb6ad523551a8edc7afd071e2a833c7e9ce3e34a5f5619b8bc4c27301182abee)))) {
            // line 13
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 15
            echo "    Account Login
";
        } elseif ((is_string($__internal_ffa52a83bd4cc32c8a664c342e835efd1d46ea28b3b689da336c72c93496f1ce = $this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_76990d6ad9359a7d5e42dae62d89a27f9f8c88dd9c1620d07efa9853505d2648 = "http://www.businessbid.ae/node3") && ('' === $__internal_76990d6ad9359a7d5e42dae62d89a27f9f8c88dd9c1620d07efa9853505d2648 || 0 === strpos($__internal_ffa52a83bd4cc32c8a664c342e835efd1d46ea28b3b689da336c72c93496f1ce, $__internal_76990d6ad9359a7d5e42dae62d89a27f9f8c88dd9c1620d07efa9853505d2648)))) {
            // line 17
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_f1df4c3efd67719e4f1a55a0430fac1bd8330306dad87c44a3ef53c9c81bcd50 = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_97a10900b3626077a93c8b07ef2630495e146ab798888ffb6ffa22e8571a0fc0 = "http://www.businessbid.ae/review/login") && ('' === $__internal_97a10900b3626077a93c8b07ef2630495e146ab798888ffb6ffa22e8571a0fc0 || 0 === strpos($__internal_f1df4c3efd67719e4f1a55a0430fac1bd8330306dad87c44a3ef53c9c81bcd50, $__internal_97a10900b3626077a93c8b07ef2630495e146ab798888ffb6ffa22e8571a0fc0)))) {
            // line 19
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_7fc65dae59e50ab215af09fab7c55af123cbc1713cf4027fdcac3ccb1122c3e9 = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8abe177164a053be92ce3b4c04bfcdbcbd7ac04c78e8c7bd9d07993beb382da8 = "http://www.businessbid.ae/node1") && ('' === $__internal_8abe177164a053be92ce3b4c04bfcdbcbd7ac04c78e8c7bd9d07993beb382da8 || 0 === strpos($__internal_7fc65dae59e50ab215af09fab7c55af123cbc1713cf4027fdcac3ccb1122c3e9, $__internal_8abe177164a053be92ce3b4c04bfcdbcbd7ac04c78e8c7bd9d07993beb382da8)))) {
            // line 21
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_553aba1178f8fb5033490a294db5136120e744ed071200ae31bfa3fb44bc1959 = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_e30d527e88590d079dedf5f042031512aa866929ca242728c0ae8b430e63cd77 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_e30d527e88590d079dedf5f042031512aa866929ca242728c0ae8b430e63cd77 || 0 === strpos($__internal_553aba1178f8fb5033490a294db5136120e744ed071200ae31bfa3fb44bc1959, $__internal_e30d527e88590d079dedf5f042031512aa866929ca242728c0ae8b430e63cd77)))) {
            // line 23
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_5e49616218a8a14f10725148e8acc2a8121f14946cef61783a1644d1beb58b8f = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_dab7e6bc72283f054dd784a5927e07f68b69f66feb9f7a187a14c17c943727e8 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_dab7e6bc72283f054dd784a5927e07f68b69f66feb9f7a187a14c17c943727e8 || 0 === strpos($__internal_5e49616218a8a14f10725148e8acc2a8121f14946cef61783a1644d1beb58b8f, $__internal_dab7e6bc72283f054dd784a5927e07f68b69f66feb9f7a187a14c17c943727e8)))) {
            // line 25
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_06090f4f14fdbd39bfc25cc978c12cf056713d3b0b1815c5935c1219d0df2685 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_293595d0c2d353501cb27a8fe6f578006e7f5294e35542ce6d138296a621b84d = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_293595d0c2d353501cb27a8fe6f578006e7f5294e35542ce6d138296a621b84d || 0 === strpos($__internal_06090f4f14fdbd39bfc25cc978c12cf056713d3b0b1815c5935c1219d0df2685, $__internal_293595d0c2d353501cb27a8fe6f578006e7f5294e35542ce6d138296a621b84d)))) {
            // line 27
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_054d37828ae666dc305b4eda5bffb9b3e7979d1fbd10ae166db8e51db27acc8e = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_79a1e1fc6f04d3281bec62e55913b6f68faf42f0461b6d0a33aa880a9c37d046 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_79a1e1fc6f04d3281bec62e55913b6f68faf42f0461b6d0a33aa880a9c37d046 || 0 === strpos($__internal_054d37828ae666dc305b4eda5bffb9b3e7979d1fbd10ae166db8e51db27acc8e, $__internal_79a1e1fc6f04d3281bec62e55913b6f68faf42f0461b6d0a33aa880a9c37d046)))) {
            // line 29
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_248d3dac02f979629def1805deb3292a54c48ad56679954d4133d30b2e479a6c = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d60cda8096b513115d950e5bbb34a98bb54656a673b092e9cd9bf6f528f87633 = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_d60cda8096b513115d950e5bbb34a98bb54656a673b092e9cd9bf6f528f87633 || 0 === strpos($__internal_248d3dac02f979629def1805deb3292a54c48ad56679954d4133d30b2e479a6c, $__internal_d60cda8096b513115d950e5bbb34a98bb54656a673b092e9cd9bf6f528f87633)))) {
            // line 31
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_d93ad653bd4aba7a0b98b53d316e3a25a0c672221a19bd18ce71901ff5e3974d = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b2c1dea3a85ced6be5b10043791f33b53fce6a024ab0581a67ff3544b6d12739 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_b2c1dea3a85ced6be5b10043791f33b53fce6a024ab0581a67ff3544b6d12739 || 0 === strpos($__internal_d93ad653bd4aba7a0b98b53d316e3a25a0c672221a19bd18ce71901ff5e3974d, $__internal_b2c1dea3a85ced6be5b10043791f33b53fce6a024ab0581a67ff3544b6d12739)))) {
            // line 33
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_82461504ef0deb3318976636831cc404c3751ead3810556e7a8078355f6f6f9a = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_e7f5656a1e9425edb2f530c5f3489d00cdc4a51ef4b5ba3d14f77713a0155c78 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_e7f5656a1e9425edb2f530c5f3489d00cdc4a51ef4b5ba3d14f77713a0155c78 || 0 === strpos($__internal_82461504ef0deb3318976636831cc404c3751ead3810556e7a8078355f6f6f9a, $__internal_e7f5656a1e9425edb2f530c5f3489d00cdc4a51ef4b5ba3d14f77713a0155c78)))) {
            // line 35
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_7bb5d45ef307b48bb0f55104536fed043caf5682381eddf8df6566c07b2d4d73 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_87675f2a1525ed18844fe44209e3f5653e8228bd4f0894a8f27902e84951e10c = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_87675f2a1525ed18844fe44209e3f5653e8228bd4f0894a8f27902e84951e10c || 0 === strpos($__internal_7bb5d45ef307b48bb0f55104536fed043caf5682381eddf8df6566c07b2d4d73, $__internal_87675f2a1525ed18844fe44209e3f5653e8228bd4f0894a8f27902e84951e10c)))) {
            // line 37
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_545cae1d5db31151ddb19e2632f6e52b3b824c065b5cb3ee416970e859784f90 = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3ebb02373c9865c6054226299526a9185c818f675f34ff62bf169105f529ed17 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_3ebb02373c9865c6054226299526a9185c818f675f34ff62bf169105f529ed17 || 0 === strpos($__internal_545cae1d5db31151ddb19e2632f6e52b3b824c065b5cb3ee416970e859784f90, $__internal_3ebb02373c9865c6054226299526a9185c818f675f34ff62bf169105f529ed17)))) {
            // line 39
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_ec7ecad2c14e741826bf864379d8428d37acebfd3456642b195b0ac908e7e115 = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_fa92295199138b77f258c200a5986e21a2a1d07f185f206f6d161d54cc7b19bf = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_fa92295199138b77f258c200a5986e21a2a1d07f185f206f6d161d54cc7b19bf || 0 === strpos($__internal_ec7ecad2c14e741826bf864379d8428d37acebfd3456642b195b0ac908e7e115, $__internal_fa92295199138b77f258c200a5986e21a2a1d07f185f206f6d161d54cc7b19bf)))) {
            // line 41
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_0423cd83e107169ad2c1e9efb45dbf253ccb6a23bb5534d9365e7fd57e63d1c9 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_57c49fc6f1fe6baa0cf4b7b73abeb1ab383e59393721eaeee1a04332761478ba = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_57c49fc6f1fe6baa0cf4b7b73abeb1ab383e59393721eaeee1a04332761478ba || 0 === strpos($__internal_0423cd83e107169ad2c1e9efb45dbf253ccb6a23bb5534d9365e7fd57e63d1c9, $__internal_57c49fc6f1fe6baa0cf4b7b73abeb1ab383e59393721eaeee1a04332761478ba)))) {
            // line 43
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_f76f4f4fa8a0155bf569a7a4112aa30cfb5f8552183fb7ed54742c7e31d3a8a8 = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2ad6c3f4fe963d74d9891d95cff631355ece8eaf0aa6ff25d4421852ac0299fd = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_2ad6c3f4fe963d74d9891d95cff631355ece8eaf0aa6ff25d4421852ac0299fd || 0 === strpos($__internal_f76f4f4fa8a0155bf569a7a4112aa30cfb5f8552183fb7ed54742c7e31d3a8a8, $__internal_2ad6c3f4fe963d74d9891d95cff631355ece8eaf0aa6ff25d4421852ac0299fd)))) {
            // line 45
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 47
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_6dcd35d687e889b48f8f0dc61bfaceda7f54ee87c43e860edb99567df22911b2 = $this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f7ad802aab770204c5c1e0b0474010d00e340c8821bf52e4199e4f8737743ca9 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_f7ad802aab770204c5c1e0b0474010d00e340c8821bf52e4199e4f8737743ca9 || 0 === strpos($__internal_6dcd35d687e889b48f8f0dc61bfaceda7f54ee87c43e860edb99567df22911b2, $__internal_f7ad802aab770204c5c1e0b0474010d00e340c8821bf52e4199e4f8737743ca9)))) {
            // line 49
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_dec9b61abae5ce37e67cfe54ada7bcd187ca53f23d8de0a165e9833e9cca6d3d = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_11ec7fd0b37afa206fbd1826aa320b86b4581a1fafb73e4d5fad02a859665d33 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_11ec7fd0b37afa206fbd1826aa320b86b4581a1fafb73e4d5fad02a859665d33 || 0 === strpos($__internal_dec9b61abae5ce37e67cfe54ada7bcd187ca53f23d8de0a165e9833e9cca6d3d, $__internal_11ec7fd0b37afa206fbd1826aa320b86b4581a1fafb73e4d5fad02a859665d33)))) {
            // line 51
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_9d6575a80e8c3890e8d0c0ff25393d2c11678b253c458ba8d52bc80cb8ac7ba2 = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_405dbb652fbc00538e3f358794e27cac19b296c0b9c42fd7dd9c2abef9325a97 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_405dbb652fbc00538e3f358794e27cac19b296c0b9c42fd7dd9c2abef9325a97 || 0 === strpos($__internal_9d6575a80e8c3890e8d0c0ff25393d2c11678b253c458ba8d52bc80cb8ac7ba2, $__internal_405dbb652fbc00538e3f358794e27cac19b296c0b9c42fd7dd9c2abef9325a97)))) {
            // line 53
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_fd04408ddb7deaed4240894ae9f03adf4c23d272aa81cb268e1d8c9ff7cde32a = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_e5dbf7c5f76ab7b1ca1c5807bf244fbac4dd68ab948c5c3f0f50b7eadd99e677 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_e5dbf7c5f76ab7b1ca1c5807bf244fbac4dd68ab948c5c3f0f50b7eadd99e677 || 0 === strpos($__internal_fd04408ddb7deaed4240894ae9f03adf4c23d272aa81cb268e1d8c9ff7cde32a, $__internal_e5dbf7c5f76ab7b1ca1c5807bf244fbac4dd68ab948c5c3f0f50b7eadd99e677)))) {
            // line 55
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_327863637370799e4b010a19f60f5061309400c0e92e410aab569756dd9492e6 = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_065a8a297bfe45de4be9707c08cf31c1856eec440883782e0ea316314a122ecb = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_065a8a297bfe45de4be9707c08cf31c1856eec440883782e0ea316314a122ecb || 0 === strpos($__internal_327863637370799e4b010a19f60f5061309400c0e92e410aab569756dd9492e6, $__internal_065a8a297bfe45de4be9707c08cf31c1856eec440883782e0ea316314a122ecb)))) {
            // line 57
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_f729f93ad1c6457eedbfa2e951f02e691caeaddd9f9e92cf871bffc1e4c8ddf8 = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5d0e9ae657b5039294e99b92da9ffddb5d71d73737c9db2099e3ce0ac01e67d7 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_5d0e9ae657b5039294e99b92da9ffddb5d71d73737c9db2099e3ce0ac01e67d7 || 0 === strpos($__internal_f729f93ad1c6457eedbfa2e951f02e691caeaddd9f9e92cf871bffc1e4c8ddf8, $__internal_5d0e9ae657b5039294e99b92da9ffddb5d71d73737c9db2099e3ce0ac01e67d7)))) {
            // line 59
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_772bb5b9a14cfcee5f24c03f2c26f936b7d22a121020764cb8e7da77ce57535c = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3d1a6a3baaeb05a041f8642dda07f7d5d44a7b543fece1126a75c2f8ccfaad45 = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_3d1a6a3baaeb05a041f8642dda07f7d5d44a7b543fece1126a75c2f8ccfaad45 || 0 === strpos($__internal_772bb5b9a14cfcee5f24c03f2c26f936b7d22a121020764cb8e7da77ce57535c, $__internal_3d1a6a3baaeb05a041f8642dda07f7d5d44a7b543fece1126a75c2f8ccfaad45)))) {
            // line 61
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_19ac202dd258b6f9016e7f3367376f7db3bd93a74b10a49d89600485a4841ca1 = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_37d8c899a9c9148c6d40dd9260cc2f96ac8727f9d62afffcb6e741b8820b69b5 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_37d8c899a9c9148c6d40dd9260cc2f96ac8727f9d62afffcb6e741b8820b69b5 || 0 === strpos($__internal_19ac202dd258b6f9016e7f3367376f7db3bd93a74b10a49d89600485a4841ca1, $__internal_37d8c899a9c9148c6d40dd9260cc2f96ac8727f9d62afffcb6e741b8820b69b5)))) {
            // line 63
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_5f2bcf69a773b505c9baff10d0a8bb5de4fb266156f884f881e7766faf32be87 = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c5db073787046eeba3136295776cd98ebe9c66093d5d0898ba5816734d8a1b13 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_c5db073787046eeba3136295776cd98ebe9c66093d5d0898ba5816734d8a1b13 || 0 === strpos($__internal_5f2bcf69a773b505c9baff10d0a8bb5de4fb266156f884f881e7766faf32be87, $__internal_c5db073787046eeba3136295776cd98ebe9c66093d5d0898ba5816734d8a1b13)))) {
            // line 65
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_5a1fa7afc7dea1fc8fdc1a95007c42aa67020cec78d8443a8d9049644e83c5c7 = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f7bdcabb3acf97ce5d0d5b74dfdd0b0b70ee77ebad1247fe0ed59105c80f4824 = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_f7bdcabb3acf97ce5d0d5b74dfdd0b0b70ee77ebad1247fe0ed59105c80f4824 || 0 === strpos($__internal_5a1fa7afc7dea1fc8fdc1a95007c42aa67020cec78d8443a8d9049644e83c5c7, $__internal_f7bdcabb3acf97ce5d0d5b74dfdd0b0b70ee77ebad1247fe0ed59105c80f4824)))) {
            // line 67
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_2889645db197552b7bb4da897635fcafd2272ae4054b407843776f91a9f59d87 = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_af99a8e9e457a2a37b423b6ddd96a6ca8eb6a6bd144a27166149cc329d29a4e9 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_af99a8e9e457a2a37b423b6ddd96a6ca8eb6a6bd144a27166149cc329d29a4e9 || 0 === strpos($__internal_2889645db197552b7bb4da897635fcafd2272ae4054b407843776f91a9f59d87, $__internal_af99a8e9e457a2a37b423b6ddd96a6ca8eb6a6bd144a27166149cc329d29a4e9)))) {
            // line 69
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_3e590187094e09f4d2b9350551c4af091daeaf755a694225ceca55fdef9b5d80 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_df487740c853620adca0861a11e5045188af6a3d63ce78f1820cb7846943a5d8 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_df487740c853620adca0861a11e5045188af6a3d63ce78f1820cb7846943a5d8 || 0 === strpos($__internal_3e590187094e09f4d2b9350551c4af091daeaf755a694225ceca55fdef9b5d80, $__internal_df487740c853620adca0861a11e5045188af6a3d63ce78f1820cb7846943a5d8)))) {
            // line 71
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 72
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 73
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 74
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 75
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 76
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 77
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 78
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 79
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 80
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 81
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_2022447f55c408b8013e45278f3cff2f0069e85f373e61a7a398d39bfdca3825 = $this->getAttribute($this->getAttribute(        // line 82
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f3c7f605771ce17cb8a021d537fcc9a009e3e5009a614726f9dcf1c5b084f90d = "http://www.businessbid.ae/node2") && ('' === $__internal_f3c7f605771ce17cb8a021d537fcc9a009e3e5009a614726f9dcf1c5b084f90d || 0 === strpos($__internal_2022447f55c408b8013e45278f3cff2f0069e85f373e61a7a398d39bfdca3825, $__internal_f3c7f605771ce17cb8a021d537fcc9a009e3e5009a614726f9dcf1c5b084f90d)))) {
            // line 83
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_a5d83045dab715d863c82b22d2c044f8ee8105eb7a13eb99670cc877e473ac5c = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_4001ff3a11e5d43aed6e686488a1f9d85405262fc1ece8a4e84bf465bba85a5f = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_4001ff3a11e5d43aed6e686488a1f9d85405262fc1ece8a4e84bf465bba85a5f || 0 === strpos($__internal_a5d83045dab715d863c82b22d2c044f8ee8105eb7a13eb99670cc877e473ac5c, $__internal_4001ff3a11e5d43aed6e686488a1f9d85405262fc1ece8a4e84bf465bba85a5f)))) {
            // line 85
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_9ffa668d23744dee0d3dc9889df3f41462bb60509ae0c68b7183dbdcdf4c2862 = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5e823b4f9d006e24e0039eaf35cbe3e23c8d0b9c4afc101bd9b1357e7c9a2eab = "http://www.businessbid.ae/faq") && ('' === $__internal_5e823b4f9d006e24e0039eaf35cbe3e23c8d0b9c4afc101bd9b1357e7c9a2eab || 0 === strpos($__internal_9ffa668d23744dee0d3dc9889df3f41462bb60509ae0c68b7183dbdcdf4c2862, $__internal_5e823b4f9d006e24e0039eaf35cbe3e23c8d0b9c4afc101bd9b1357e7c9a2eab)))) {
            // line 87
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_2c7063a1071606db67685084a69dcf8f7178aad2d0acdcb5dcffd94f2069b98e = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3bb2d7fdd02bd003d092ae20c2c334be40339c5e9de7722facbaaf49664479c8 = "http://www.businessbid.ae/node4") && ('' === $__internal_3bb2d7fdd02bd003d092ae20c2c334be40339c5e9de7722facbaaf49664479c8 || 0 === strpos($__internal_2c7063a1071606db67685084a69dcf8f7178aad2d0acdcb5dcffd94f2069b98e, $__internal_3bb2d7fdd02bd003d092ae20c2c334be40339c5e9de7722facbaaf49664479c8)))) {
            // line 89
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_912c5a580a825f0207b1954f4394d2c9502e0b5236a01a130bede61dcdac1193 = ($this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_014fed35a72ae3fa373ba887277ad7dc614840524244e40369c4b00d2cd2df81 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_014fed35a72ae3fa373ba887277ad7dc614840524244e40369c4b00d2cd2df81 || 0 === strpos($__internal_912c5a580a825f0207b1954f4394d2c9502e0b5236a01a130bede61dcdac1193, $__internal_014fed35a72ae3fa373ba887277ad7dc614840524244e40369c4b00d2cd2df81)))) {
            // line 91
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_62fdbbdf75c4175305a453591beae7c5c0235af2b0976839b3c92f68279838fc = $this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_308c5100781603fd5cf02ddf1607b460b558de6d7cc2959060f9ef0f955c1617 = "http://www.businessbid.ae/node6") && ('' === $__internal_308c5100781603fd5cf02ddf1607b460b558de6d7cc2959060f9ef0f955c1617 || 0 === strpos($__internal_62fdbbdf75c4175305a453591beae7c5c0235af2b0976839b3c92f68279838fc, $__internal_308c5100781603fd5cf02ddf1607b460b558de6d7cc2959060f9ef0f955c1617)))) {
            // line 93
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_6114e4da5cda6cb80ac5982d0e57d13ed5edc73757a7c6d73cca5290f4f582a5 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f5d11c5dcb92b704003c03b168f9d248525383f75bb0008f96fab21851407425 = "http://www.businessbid.ae/feedback") && ('' === $__internal_f5d11c5dcb92b704003c03b168f9d248525383f75bb0008f96fab21851407425 || 0 === strpos($__internal_6114e4da5cda6cb80ac5982d0e57d13ed5edc73757a7c6d73cca5290f4f582a5, $__internal_f5d11c5dcb92b704003c03b168f9d248525383f75bb0008f96fab21851407425)))) {
            // line 95
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_17a9ad5077f7f678ce6b1a20a193229124b3cfb4bb9b31795219516ced62f6c7 = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_92007544f52a22fac836173055d354506ceaff3418399d5e92ec58e8cb588d55 = "http://www.businessbid.ae/sitemap") && ('' === $__internal_92007544f52a22fac836173055d354506ceaff3418399d5e92ec58e8cb588d55 || 0 === strpos($__internal_17a9ad5077f7f678ce6b1a20a193229124b3cfb4bb9b31795219516ced62f6c7, $__internal_92007544f52a22fac836173055d354506ceaff3418399d5e92ec58e8cb588d55)))) {
            // line 97
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_4087d740c6dc5a42f703303e386f7e3b464bff17ae8dc46ebcf676c3d8fb7075 = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a0a56b132aab19af260476b6eebc4e77bc9c545069e112ca2ae769ac5aa0a235 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_a0a56b132aab19af260476b6eebc4e77bc9c545069e112ca2ae769ac5aa0a235 || 0 === strpos($__internal_4087d740c6dc5a42f703303e386f7e3b464bff17ae8dc46ebcf676c3d8fb7075, $__internal_a0a56b132aab19af260476b6eebc4e77bc9c545069e112ca2ae769ac5aa0a235)))) {
            // line 99
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_577d1ad000c4412fd0d9d587044f1dd5db2cb0ce9ae4016ab397e268398b9109 = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8f0c14520162726f1d8ec6ece3882394a458dd6fba4105af177b90eb2738b8a1 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_8f0c14520162726f1d8ec6ece3882394a458dd6fba4105af177b90eb2738b8a1 || 0 === strpos($__internal_577d1ad000c4412fd0d9d587044f1dd5db2cb0ce9ae4016ab397e268398b9109, $__internal_8f0c14520162726f1d8ec6ece3882394a458dd6fba4105af177b90eb2738b8a1)))) {
            // line 101
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_592ad6c1de7bd52860e7691c20f6494f05df1d0066ad07cad3d747de4d281a59 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1473491fa325f6068436702c85e74faadcd7a4c657de7ce41bf4d191c462629f = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_1473491fa325f6068436702c85e74faadcd7a4c657de7ce41bf4d191c462629f || 0 === strpos($__internal_592ad6c1de7bd52860e7691c20f6494f05df1d0066ad07cad3d747de4d281a59, $__internal_1473491fa325f6068436702c85e74faadcd7a4c657de7ce41bf4d191c462629f)))) {
            // line 103
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_b60c6e2caef6c02f0ea5d0ff3e4091b5b76070117da13909062bfd31ae7e54f4 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_66bd2679ab7fff831e0c28f2a6eab1a49fe4b7694719cb90b75589840210d6b3 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_66bd2679ab7fff831e0c28f2a6eab1a49fe4b7694719cb90b75589840210d6b3 || 0 === strpos($__internal_b60c6e2caef6c02f0ea5d0ff3e4091b5b76070117da13909062bfd31ae7e54f4, $__internal_66bd2679ab7fff831e0c28f2a6eab1a49fe4b7694719cb90b75589840210d6b3)))) {
            // line 105
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 107
            echo "    Business BID
";
        }
    }

    // line 124
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 125
        echo "    ";
    }

    // line 159
    public function block_customcss($context, array $blocks = array())
    {
        // line 160
        echo "
";
    }

    // line 163
    public function block_javascripts($context, array $blocks = array())
    {
        // line 164
        echo "<script>
\$(document).ready(function(){
    ";
        // line 166
        if (array_key_exists("keyword", $context)) {
            // line 167
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 171
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 307
    public function block_customjs($context, array $blocks = array())
    {
        // line 308
        echo "
";
    }

    // line 311
    public function block_jquery($context, array $blocks = array())
    {
        // line 312
        echo "
";
    }

    // line 314
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 315
        echo "<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-59054368-1', 'auto');ga('send', 'pageview');
</script>
";
    }

    // line 399
    public function block_maincontent($context, array $blocks = array())
    {
        // line 400
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 414
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 421
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 436
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 443
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 451
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 459
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 467
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 475
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 487
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 527
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 557
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 558
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 560
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 562
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 568
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 569
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 571
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 573
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 578
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 579
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 581
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 583
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 590
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 591
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 593
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 595
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 601
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 602
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 604
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 606
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 612
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 613
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 615
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 617
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource_centre/home-2\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 487
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 488
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 490
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 497
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 498
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 499
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 500
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 501
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 502
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 503
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 505
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 506
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 507
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 509
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 511
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 516
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 522
        echo "    </div>
            </div>


    ";
    }

    // line 516
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 517
        echo "    <div class=\"request\">
    <h2>";
        // line 518
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 633
    public function block_footer($context, array $blocks = array())
    {
        // line 634
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 653
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 654
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 655
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 663
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 664
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 672
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 673
        echo $this->env->getExtension('routing')->getPath("bizbids_are_you_a_vendor");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 674
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 682
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 684
        echo "                            <li><a href=\"http://www.businessbid.ae/resource_centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 693
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 694
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"#\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 695
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 700
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource_centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 737
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 739
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 749
    public function block_footerScript($context, array $blocks = array())
    {
        // line 750
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 751
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1403 => 751,  1398 => 750,  1395 => 749,  1384 => 739,  1379 => 737,  1339 => 700,  1331 => 695,  1327 => 694,  1323 => 693,  1312 => 685,  1309 => 684,  1305 => 682,  1296 => 676,  1292 => 675,  1288 => 674,  1284 => 673,  1280 => 672,  1269 => 664,  1265 => 663,  1256 => 657,  1252 => 656,  1248 => 655,  1244 => 654,  1240 => 653,  1219 => 634,  1216 => 633,  1208 => 518,  1205 => 517,  1202 => 516,  1194 => 522,  1192 => 516,  1185 => 511,  1178 => 509,  1162 => 507,  1159 => 506,  1156 => 505,  1142 => 503,  1139 => 502,  1136 => 501,  1134 => 500,  1130 => 499,  1127 => 498,  1123 => 497,  1113 => 490,  1109 => 488,  1106 => 487,  1090 => 617,  1084 => 615,  1078 => 613,  1076 => 612,  1068 => 606,  1062 => 604,  1056 => 602,  1054 => 601,  1046 => 595,  1040 => 593,  1034 => 591,  1032 => 590,  1023 => 583,  1017 => 581,  1011 => 579,  1009 => 578,  1002 => 573,  996 => 571,  990 => 569,  988 => 568,  980 => 562,  974 => 560,  968 => 558,  966 => 557,  934 => 527,  932 => 487,  917 => 475,  906 => 467,  895 => 459,  884 => 451,  873 => 443,  863 => 436,  845 => 421,  835 => 414,  825 => 407,  816 => 400,  813 => 399,  805 => 315,  802 => 314,  797 => 312,  794 => 311,  789 => 308,  786 => 307,  767 => 291,  645 => 171,  639 => 167,  637 => 166,  633 => 164,  630 => 163,  625 => 160,  622 => 159,  618 => 125,  615 => 124,  609 => 107,  605 => 105,  603 => 104,  600 => 103,  598 => 102,  595 => 101,  593 => 100,  590 => 99,  588 => 98,  585 => 97,  583 => 96,  580 => 95,  578 => 94,  575 => 93,  573 => 92,  570 => 91,  568 => 90,  565 => 89,  563 => 88,  560 => 87,  558 => 86,  555 => 85,  553 => 84,  550 => 83,  548 => 82,  545 => 81,  543 => 80,  540 => 79,  538 => 78,  535 => 77,  533 => 76,  530 => 75,  528 => 74,  525 => 73,  523 => 72,  520 => 71,  518 => 70,  515 => 69,  513 => 68,  510 => 67,  508 => 66,  505 => 65,  503 => 64,  500 => 63,  498 => 62,  495 => 61,  493 => 60,  490 => 59,  488 => 58,  485 => 57,  483 => 56,  480 => 55,  478 => 54,  475 => 53,  473 => 52,  470 => 51,  468 => 50,  465 => 49,  463 => 48,  460 => 47,  458 => 46,  455 => 45,  453 => 44,  450 => 43,  448 => 42,  445 => 41,  443 => 40,  440 => 39,  438 => 38,  435 => 37,  433 => 36,  430 => 35,  428 => 34,  425 => 33,  423 => 32,  420 => 31,  418 => 30,  415 => 29,  413 => 28,  410 => 27,  408 => 26,  405 => 25,  403 => 24,  400 => 23,  398 => 22,  395 => 21,  393 => 20,  390 => 19,  388 => 18,  385 => 17,  383 => 16,  380 => 15,  378 => 14,  375 => 13,  373 => 12,  370 => 11,  368 => 10,  365 => 9,  360 => 760,  351 => 753,  349 => 749,  346 => 746,  344 => 633,  339 => 630,  337 => 399,  328 => 392,  314 => 389,  311 => 388,  309 => 387,  303 => 384,  299 => 382,  297 => 381,  289 => 376,  281 => 371,  276 => 369,  270 => 366,  265 => 364,  261 => 363,  252 => 356,  236 => 351,  232 => 350,  228 => 349,  224 => 347,  222 => 346,  216 => 343,  210 => 342,  206 => 340,  204 => 339,  191 => 328,  189 => 327,  187 => 326,  185 => 325,  183 => 324,  179 => 322,  177 => 321,  174 => 320,  172 => 314,  170 => 311,  167 => 310,  165 => 307,  162 => 306,  160 => 163,  157 => 162,  155 => 159,  150 => 157,  146 => 156,  137 => 150,  132 => 148,  128 => 147,  123 => 145,  119 => 144,  114 => 142,  110 => 141,  106 => 140,  102 => 139,  98 => 138,  94 => 137,  83 => 129,  78 => 126,  76 => 124,  71 => 121,  67 => 119,  64 => 118,  60 => 116,  58 => 115,  50 => 109,  48 => 9,  45 => 8,  43 => 7,  41 => 6,  39 => 5,  37 => 4,  35 => 3,  31 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Customer:activateemailstatuscustom.html.twig */
class __TwigTemplate_e2c4be31bda2fee6bebc07756e3d5c7db1b2d09758c0b4ed48cb7561f32bfebd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table width=\"100%\" bgcolor=\"#939598\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
  <tr>
    <td colspan=\"2\" align=\"center\" valign=\"middle\" bgcolor=\"#939598\">
    <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height=\"23\" bgcolor=\"#f36e23\">&nbsp;</td>
  </tr>

<tr>
    <td align=\"center\" bgcolor=\"#FFFFFF\"><table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      ";
        // line 14
        if (twig_test_empty((isset($context["type"]) ? $context["type"] : null))) {
            // line 15
            echo "      <tr>
        <td align=\"center\" style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; \">
        <p style=\"font-size:24px; margin-bottom:10px; font-weight:600; color:#1e7dc0;\">Welcome to the BusinessBid Network.</p></td>
      </tr>

       <tr>
        <td align=\"left\" style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\"><p>BusinessBid is your one stop shop to receive multiple quotes from vendors for over 700 services across the UAE.</p>
          <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">With your own BusinessBid account now, you can be assured that you have access to qualified vendors to assist you with your projects anytime in a few easy steps.</p>
          <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">Please remember to write a review on your experiences. This will help us to serve you better. </p>
          <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">And of course, if you have any questions or need help call us on 04 42 13 777.</p></td>
      </tr>
      ";
        } else {
            // line 27
            echo "            <tr>
              <td align=\"left\"><p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
                Our records indicate that you have recently changed your password.
              </p></td>
            </tr>
            <tr>
              <td align=\"left\"><p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
                If you believe that it was not you, please report this suspicious activity to BusinessBid on<a>support@businessbid.ae</a>or call us on 04 42 13 777.
              </p></td>
            </tr>
      ";
        }
        // line 38
        echo "      <tr>
        <td align=\"center\"></td>
      </tr>

      <tr>
        <td><p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">Regards
\t\t\t\t<br>
\t\t\t\tBusinessBid Team</p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>

    </table></td>
  </tr>

  <tr>
    <td bgcolor=\"#eeeeee\">&nbsp;</td>
  </tr>

  <tr>
    <td bgcolor=\"#eeeeee\">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor=\"#939598\">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor=\"#939598\">&nbsp;</td>
  </tr>
    </table>

    </td>
  </tr>
  </table>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Customer:activateemailstatuscustom.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 38,  50 => 27,  36 => 15,  34 => 14,  19 => 1,);
    }
}

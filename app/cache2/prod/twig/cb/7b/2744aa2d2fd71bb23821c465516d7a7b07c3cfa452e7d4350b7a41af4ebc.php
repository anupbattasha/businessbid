<?php

/* BBidsBBidsHomeBundle:Home:myreviews.html.twig */
class __TwigTemplate_cb7b2744aa2d2fd71bb23821c465516d7a7b07c3cfa452e7d4350b7a41af4ebc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:myreviews.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<style>
.collapsedd .collapsed-content{
display: none;
list-style: none;
margin: 0;
padding: 0;
}
</style>

<script>
\$(document).ready(function(){
\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
       
        // Create stars holder
        var \$span = \$('<span />').width(size);
        
        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();


\t\$(\".moreview\").click(function(){
\t\$(\".moreview\").not(this).html('View job request details');
\t\t\$(\".collapsedd\").not(this).children().children('div').slideUp('10000');
  \t\t\$(this).closest(\".collapsedd\").children().children('div').slideDown('10000');
\t\tif(\$(this).html() == 'close') {
\t\t\t\$(this).html('View job request details');
\t\t\t\$(\".collapsedd\").not(this).children().children('div').slideUp('10000');
\t\t}
\t\telse
\t\t\t\$(this).html('close');
\t\t
\t});
});
</script>

<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>Ratings & Reviews</h1></div>

";
        // line 51
        if ((isset($context["count"]) ? $context["count"] : null)) {
            // line 52
            echo "
<div class=\"row text-right page-counter\"><div class=\"counter-box\"><span class=\"now-show\">Now showing : ";
            // line 53
            echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
            echo " records</span></div></div>
";
        }
        // line 55
        echo "<div>
        ";
        // line 56
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 57
            echo "
        <div class=\"alert alert-danger\">";
            // line 58
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "
";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 63
            echo "
        <div class=\"alert alert-success\">";
            // line 64
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "
        ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 69
            echo "
        <div class=\"alert alert-success\">";
            // line 70
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "</div>

<div class=\"row\">
";
        // line 76
        if ( !twig_test_empty((isset($context["reviews"]) ? $context["reviews"] : null))) {
            // line 77
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 78
                echo "<div class=\"rating-reviews\">
<div class=\"form-item\">
\t<div class=\"subject\">";
                // line 80
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "subj", array()), "html", null, true);
                echo "</div>
\t<div class=\"date\">";
                // line 81
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-m-d H:m:s"), "html", null, true);
                echo "<div class=\"rating\"><span>";
                echo "</span><span class=\"stars\"></span></div></div>
\t<label class=\"business-name\">";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "author", array()), "html", null, true);
                echo "</label>
\t<div class=\"review\"><!--<a href=\"";
                // line 83
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_mapped_reviews_vendor", array("rid" => $this->getAttribute($context["r"], "revid", array()))), "html", null, true);
                echo "\">-->";
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "review", array()), "html", null, true);
                echo "</div>
\t<div class=\"view-details\"><!--<a href=\"";
                // line 84
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_mapped_enquiry_vendor", array("eid" => $this->getAttribute($context["r"], "id", array()))), "html", null, true);
                echo "\">Click here view Description</a>-->
\t\t<div class=\"collapsedd\"> 
        <span> 
            <a class=\"reviewss\"><span class=\"moreview\">view Job Request Details</span></a> 
            <div class=\"collapsed-content\">
\t\t\t<div class=\"col-sm-6 post-requirement gray-bg\">
\t\t\t\t<h2>Job Request Details</h2>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Date</label><div class=\"field-value\">";
                // line 92
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "encreated", array()), "Y-d-m"), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Category</label><div class=\"field-value\">";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "category", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Subject</label><div class=\"field-value\">";
                // line 98
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "subj", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>City</label><div class=\"field-value\">";
                // line 101
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "location", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Description</label><div class=\"field-value\">";
                // line 104
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "description", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"col-sm-6 post-requirement gray-bg\">
\t\t\t\t<h2>Job Review Details</h2>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Date</label><div class=\"field-value\">";
                // line 111
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "encreated", array()), "Y-d-m"), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Category</label><div class=\"field-value\">";
                // line 114
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "category", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Subject</label><div class=\"field-value\">";
                // line 117
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "subj", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Business Now</label><div class=\"field-value\">";
                // line 120
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "businessknow", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Business Recomendation</label><div class=\"field-value\">";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "servicesummary", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Service Experience</label><div class=\"field-value\">";
                // line 126
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "serviceexperience", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-field\">
\t\t\t\t<label>Service Summary</label><div class=\"field-value\">";
                // line 129
                echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "servicesummary", array()), "html", null, true);
                echo "</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t</div> 
        </span>
    </div>
\t</div>
\t
</div>



</div>
\t\t\t
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 144
            if ((isset($context["count"]) ? $context["count"] : null)) {
                // line 145
                echo "

<div>";
                // line 147
                $context["Epage_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 4)));
                // line 148
                echo "<ul class=\"pagination\">

  <li><a href=\"";
                // line 150
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_my_reviews", array("offset" => 1));
                echo "\">&laquo;</a></li>
";
                // line 151
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 152
                    echo "
  <li><a href=\"";
                    // line 153
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_my_reviews", array("offset" => $context["i"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "</a></li>

";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 156
                echo "
  <li><a href=\"";
                // line 157
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_my_reviews", array("offset" =>  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null))), "html", null, true);
                echo "\">&raquo;</a></li>
</ul>
</div>
";
            }
            // line 161
            echo "
";
        } else {
            // line 163
            echo "<table>
<tr><td align=\"center\">No Reviews Available</td></tr>
</table>
";
        }
        // line 167
        echo "
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:myreviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 167,  333 => 163,  329 => 161,  322 => 157,  319 => 156,  308 => 153,  305 => 152,  301 => 151,  297 => 150,  293 => 148,  291 => 147,  287 => 145,  285 => 144,  264 => 129,  258 => 126,  252 => 123,  246 => 120,  240 => 117,  234 => 114,  228 => 111,  218 => 104,  212 => 101,  206 => 98,  200 => 95,  194 => 92,  183 => 84,  177 => 83,  173 => 82,  168 => 81,  164 => 80,  160 => 78,  156 => 77,  154 => 76,  149 => 73,  140 => 70,  137 => 69,  133 => 68,  130 => 67,  121 => 64,  118 => 63,  114 => 62,  111 => 61,  102 => 58,  99 => 57,  95 => 56,  92 => 55,  85 => 53,  82 => 52,  80 => 51,  31 => 4,  28 => 3,  11 => 1,);
    }
}

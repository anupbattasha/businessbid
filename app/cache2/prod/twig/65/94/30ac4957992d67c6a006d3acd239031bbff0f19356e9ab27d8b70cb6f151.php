<?php

/* BBidsBBidsHomeBundle:Admin:order.html.twig */
class __TwigTemplate_659430ac4957992d67c6a006d3acd239031bbff0f19356e9ab27d8b70cb6f151 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:order.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">

<div class=\"page-title\"><h1>VENDOR ORDERS : ";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order"]) ? $context["order"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "ordernumber", array()), "html", null, true);
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </h1></div>
<div>
\t";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "
\t<div class=\"alert alert-danger\">";
            // line 10
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "
\t";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "
\t<div class=\"alert alert-success\">";
            // line 16
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
\t";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "
\t<div class=\"alert alert-success\">";
            // line 22
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "</div>
<div class=\"row orderdetails\">
<table class=\"col-sm-6 gray-bg vendor-enquiries\" width=\"100%\">

\t";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order"]) ? $context["order"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            // line 30
            echo "
\t<tr><td width=\"30%\"><strong>Order Number</strong></td><td width=\"70%\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "ordernumber", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Amount</strong></td><td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "amount", array()), "html", null, true);
            echo "</td></tr>
\t<tr><td><strong>Status</strong></td><td>";
            // line 33
            if (($this->getAttribute($context["o"], "status", array()) == 0)) {
                echo " Not Approved ";
            } elseif (($this->getAttribute($context["o"], "status", array()) == 1)) {
                echo " Approved ";
            }
            echo "</td></tr>
\t<tr><td><strong>Payment Type</strong></td><td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "payoption", array()), "html", null, true);
            echo "</td></tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "</table>
</div>
<div class=\"row orderdetails\">
<table class=\"col-sm-6  gray-bg vendor-enquiries\" width=\"100%\">
<thead><tr><td>Category Name</td><td>No: of Leads</td><td>Lead Pack</td></thead>
\t";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 42
            echo "\t\t<tr><td width=\"50%\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "leadpack", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 44
            if (($this->getAttribute($context["c"], "leadpack", array()) == 10)) {
                echo "Bronze";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 25)) {
                echo "Silver ";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 50)) {
                echo " Gold ";
            } elseif (($this->getAttribute($context["c"], "leadpack", array()) == 100)) {
                echo " Platinum ";
            }
            echo "</td>
\t\t</tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "</table>
</div>
";
        // line 49
        if ( !twig_test_empty((isset($context["formData"]) ? $context["formData"] : null))) {
            // line 50
            echo "<div class=\"row orderdetails\">
<table class=\"col-sm-6  gray-bg vendor-enquiries\" width=\"100%\"><thead><tr> <td colspan=\"2\"><strong>Cheque Payment Details</strong></td></tr></thead><tr> <td class='tg-4eph'><strong>Company Name </strong></td><td class='tg-4eph'>";
            // line 51
            echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
            echo "</td></tr><tr> <td class='tg-031e'><strong>Address </strong></td><td class='tg-031e'>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "address", array(), "array"), "html", null, true);
            echo "</td></tr><tr> <td class='tg-4eph'><strong>City</strong></td><td class='tg-4eph'>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "city", array(), "array"), "html", null, true);
            echo "</td></tr><tr> <td class='tg-031e'><strong>Contact Number </strong></td><td class='tg-031e'>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "contactnumber", array(), "array"), "html", null, true);
            echo "</td></tr><tr> <td class='tg-4eph'><strong>Contact Person</strong></td><td class='tg-4eph'>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "contactperson", array(), "array"), "html", null, true);
            echo "</td></tr>";
            if ($this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "bankname", array(), "array", true, true)) {
                echo "<tr> <td class='tg-031e'><strong>Bank Name </strong></td><td class='tg-031e'>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "bankname", array(), "array"), "html", null, true);
                echo "</td></tr><tr> <td class='tg-031e'><strong>Check Number </strong></td><td class='tg-031e'>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "checknumber", array(), "array"), "html", null, true);
                echo "</td></tr>";
            }
            echo "</table>

</table>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 51,  183 => 50,  181 => 49,  177 => 47,  160 => 44,  156 => 43,  151 => 42,  147 => 41,  140 => 36,  132 => 34,  124 => 33,  120 => 32,  116 => 31,  113 => 30,  109 => 29,  103 => 25,  94 => 22,  91 => 21,  87 => 20,  84 => 19,  75 => 16,  72 => 15,  68 => 14,  65 => 13,  56 => 10,  53 => 9,  49 => 8,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}

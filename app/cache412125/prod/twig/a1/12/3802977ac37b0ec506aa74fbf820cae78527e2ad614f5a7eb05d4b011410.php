<?php

/* BBidsBBidsHomeBundle:Categoriesform:beauty_styling_form.html.twig */
class __TwigTemplate_a1123802977ac37b0ec506aa74fbf820cae78527e2ad614f5a7eb05d4b011410 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:beauty_styling_form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Easy Way To Get Beauty & Styling Quotes Here";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "
<style>
    .pac-container:after{
        content:none !important;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 20
            echo "
        <div class=\"alert alert-danger\">";
            // line 21
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
        ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
        <div class=\"alert alert-success\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "
        <div class=\"alert alert-success\">";
            // line 32
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "    </div>
<div class=\"cat-form\">
  <div class=\"photo_form_one_area modal1\">
    <h1>Looking for Beauty & Styling in the UAE</h1>
    <p>Have a beauty or a styling requirement for an event or occasion? Access competitive quotes from experienced service professionals in Dubai now by completing the short form below.
      <br>OR<br><br><span class=\"call-us\">CALL (04) 4213777</span></p>
  </div>

        ";
        // line 43
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "category_form")));
        echo "
        <div id=\"step1\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 1 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Beauty-1.png"), "html", null, true);
        echo "\" alt=\"Hair services\">
                  </div>
                  ";
        // line 82
        $context["oddoreven"] = "even";
        // line 83
        echo "                  ";
        $context["subcategoryLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array())) - 1);
        // line 84
        echo "                  ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array())) % 2 == 1)) {
            // line 85
            echo "                    ";
            $context["oddoreven"] = "odd";
            // line 86
            echo "                  ";
        }
        // line 87
        echo "                  <div class=\"inner_photo_one_form_table\">
                      <h3>What services do you require?</h3>
                      <div class=\"child_pro_tabil_main kindofService\">
                          ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 91
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 92
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"child_pho_table_left_inner_text\" >
                                    ";
                // line 94
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                                    ";
                // line 95
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory_other", array()), 'widget', array("attr" => array("class" => "subcategory_other")));
                echo "
                                    <h5>(max 30 characters)</h5>
                                </div>
                              </div>
                            ";
            } else {
                // line 100
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"";
                // line 101
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["subcategoryLength"]) ? $context["subcategoryLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                    ";
                // line 102
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                                </div>
                              </div>
                            ";
            }
            // line 106
            echo "                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "                          ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 108
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                           </div>
                          ";
        }
        // line 112
        echo "                          <div class=\"error\" id=\"kind_service\"></div>
                        </div>
                    <div class=\"photo_contunue_btn proceed\">
                        <a href=\"javascript:void(0);\" id=\"continue1\">CONTINUE</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>

        ";
        // line 123
        echo "        <div id=\"step2\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 2 of 4</p>
                    </div>
               </div>
              </div>
          </div>

        ";
        // line 157
        $context["oddoreven"] = "even";
        // line 158
        echo "        ";
        $context["eventLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event", array())) - 1);
        // line 159
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event", array())) % 2 == 1)) {
            // line 160
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 161
            echo "        ";
        }
        // line 162
        echo "          <div class=\"photo_one_form\">
          <div class=\"inner_photo_one_form event\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Beauty-2a.png"), "html", null, true);
        echo "\" alt=\"Corporate Event\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>What event is the service required for?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 170
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 171
            echo "                      ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 172
                echo "                        <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                          <div class=\"child_pho_table_left_inner_text\" >
                              ";
                // line 174
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                              ";
                // line 175
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_other", array()), 'widget', array("attr" => array("class" => "event_other")));
                echo "
                              <h5>(max 30 characters)</h5>
                          </div>
                        </div>
                      ";
            } else {
                // line 180
                echo "                        <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                          <div class=\"";
                // line 181
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["eventLength"]) ? $context["eventLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                              ";
                // line 182
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                          </div>
                        </div>
                      ";
            }
            // line 186
            echo "                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 187
        echo "                    ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 188
            echo "                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                     </div>
                    ";
        }
        // line 192
        echo "                    <div class=\"error\" id=\"event\"></div>
                </div>
              </div>
            </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form eventPlaned\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/scheduled.png"), "html", null, true);
        echo "\" alt=\"Beauty require\">
                  </div>
                  <div class=\"inner_photo_one_form_table software\">
                      <h3>What date do you require the service for?</h3>
                      ";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_planed", array()), 'widget', array("attr" => array("class" => "event")));
        echo "
                  </div>
                  <div class=\"error\" id=\"event_planed\"></div>
              </div>
          </div>
        <div class=\"photo_one_form\">
          <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Beauty-2b.png"), "html", null, true);
        echo "\" alt=\"beauty service\">
              </div>
              <div class=\"inner_photo_one_form_table services\">
                  <h3>Is the service for?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner_text\">
                            ";
        // line 219
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "service")));
        echo "
                            ";
        // line 220
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_left_inner_text\">
                              ";
        // line 225
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "multipleServices")));
        echo "
                              ";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "service_other", array()), 'widget', array("attr" => array("class" => "service")));
        echo "
                              <h5>(max 30 characters)</h5>
                         </div>
                      </div>
                      <div class=\"error\" id=\"services\"></div>
                  </div>
              </div>
          </div>
        </div>
        ";
        // line 235
        $context["oddoreven"] = "even";
        // line 236
        echo "        ";
        $context["onsiteLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "onsite", array())) - 1);
        // line 237
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "onsite", array())) % 2 == 1)) {
            // line 238
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 239
            echo "        ";
        }
        // line 240
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Beauty-2c.png"), "html", null, true);
        echo "\" alt=\"require onsite\">
                </div>
                <div class=\"inner_photo_one_form_table onsite\">
                    <h3>Do you require onsite service?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 248
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "onsite", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 249
            echo "                        <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                          <div class=\"";
            // line 250
            if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["onsiteLength"]) ? $context["onsiteLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                echo "child_pho_table_left_inner";
            } else {
                echo "child_pho_table_left_inner";
            }
            echo "\" >
                              ";
            // line 251
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                          </div>
                        </div>
                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 255
        echo "                      ";
        if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd") && ((isset($context["onsiteLength"]) ? $context["onsiteLength"] : null) != 2))) {
            // line 256
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                       </div>
                      ";
        }
        // line 260
        echo "                      <div class=\"error\" id=\"onsite\"></div>
                  </div>
                </div>
            </div>
        </div>
        ";
        // line 265
        $context["oddoreven"] = "even";
        // line 266
        echo "        ";
        $context["hoursLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hours", array())) - 1);
        // line 267
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hours", array())) % 2 == 1)) {
            // line 268
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 269
            echo "        ";
        }
        // line 270
        echo "        <div class=\"photo_one_form\">
          <div class=\"inner_photo_one_form hours\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 273
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/plan.png"), "html", null, true);
        echo "\" alt=\"beauty long\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>How long will the service be required for?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 278
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hours", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 279
            echo "                      ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 280
                echo "                        <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                          <div class=\"child_pho_table_left_inner_text\" >
                              ";
                // line 282
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                              ";
                // line 283
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hours_other", array()), 'widget', array("attr" => array("class" => "hours_other")));
                echo "
                              <h5>(max 30 characters)</h5>
                          </div>
                        </div>
                      ";
            } else {
                // line 288
                echo "                        <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                          <div class=\"";
                // line 289
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["hoursLength"]) ? $context["hoursLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                              ";
                // line 290
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                          </div>
                        </div>
                      ";
            }
            // line 294
            echo "                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 295
        echo "                    ";
        if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd") && ((isset($context["hoursLength"]) ? $context["hoursLength"] : null) != 2))) {
            // line 296
            echo "                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                     </div>
                    ";
        }
        // line 300
        echo "                    <div class=\"error\" id=\"hours\"></div>
                </div>
                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0)\" id=\"continue2\" class=\"button3\"><button type=\"button\">CONTINUE</button></a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback1\">BACK</a></div>
                </div>
              </div>
        </div>
      </div>
    </div>
      ";
        // line 313
        echo "      ";
        // line 314
        echo "       <div id=\"step3\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                  <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 3 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 347
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 348
            echo "          <div class=\"photo_form_main\">
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form name\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 352
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/name.png"), "html", null, true);
            echo "\" alt=\"Name\">
                  </div>
                  <div class=\"inner_photo_one_form_table\" >
                      <h3>";
            // line 355
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h3>
                          ";
            // line 356
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "form-input name")));
            echo "
                      <div class=\"error\" id=\"name\" ></div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form  email\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 364
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/email.png"), "html", null, true);
            echo "\" alt=\"Email\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 367
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo " </h3>
                     ";
            // line 368
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-input email_id")));
            echo "
                     <div class=\"error\" id=\"email\" ></div>
                  </div>
              </div>
          </div>
          ";
        }
        // line 374
        echo "          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form location\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 377
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location.png"), "html", null, true);
        echo "\" alt=\"Location\">
                  </div>
                  <div class=\"inner_photo_one_form_table res2\" >
                      <h3>Select your Location*</h3>
                      <div class=\"child_pro_tabil_main res21\">
                          <div class=\"child_accounting_three_left\">
                              ";
        // line 383
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => " land_location")));
        echo "
                                  <p>";
        // line 384
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"child_accounting_three_right\">
                             ";
        // line 387
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "land_location")));
        echo "
                                  <p>";
        // line 388
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"error\" id=\"location\"></div>
                      </div>
                  </div>
              </div>
          </div>


              ";
        // line 397
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            echo " <div class=\"photo_one_form for-uae \">
              <div class=\"inner_photo_one_form mask domcity_cont\" id=\"domCity\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
            // line 401
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <h3>";
            // line 404
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h3>
                    <div class=\"child_pro_tabil_main city_cont\">
                      ";
            // line 406
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "")));
            echo "
                    </div>
                  </div>
                </div>
                <div class=\"error\" id=\"acc_city\"></div>
              </div>
              ";
        }
        // line 413
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 414
            echo "              <div class=\"inner_photo_one_form mask\" id=\"domArea\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 416
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location icon.png"), "html", null, true);
            echo "\" alt=\"Location Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 419
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h3>
                     ";
            // line 420
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_area\"></div>
              </div>
              ";
        }
        // line 425
        echo "
              ";
        // line 426
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 427
            echo "              <div class=\"mask\" id=\"domContact\" >
                <div class=\"inner_photo_one_form\" style=\"margin-top: 40px;\" id=\"uae_mobileno\">
                    <div class=\"inner_photo_one_form_img\">
                        <img src=\"";
            // line 430
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                    </div>
                    <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                        <h3>";
            // line 433
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h3>


                        <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                        ";
            // line 437
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
            echo "
                        ";
            // line 438
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                    </div>
                    <div class=\"error\" id=\"acc_mobile\"></div>
                </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top: 40px;\" >
                        <div class=\"inner_photo_one_form_img\">
                            <img src=\"";
            // line 445
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                        </div>
                        <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                            <h3>";
            // line 448
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h3>


                             <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                            ";
            // line 452
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
            echo "
                            ";
            // line 453
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                        </div>
                    </div>
              </div></div>
                    ";
        }
        // line 459
        echo "

            <div class=\"photo_one_form for-foreign\">
              ";
        // line 462
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 463
            echo "              <div class=\"inner_photo_one_form mask intCountry_cont\" id=\"intCountry\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 465
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/international icon.png"), "html", null, true);
            echo "\" alt=\"international Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 468
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h3>
                     ";
            // line 469
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcountry\"></div>
              </div>
              <div class=\"inner_photo_one_form mask\" id=\"intCity\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 475
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 478
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h3>
                      ";
            // line 479
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcity\"></div>
              </div>
              ";
        }
        // line 484
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 485
            echo "              <div class=\"mask\" id=\"intContact\">
                <div class=\"inner_photo_one_form\" style=\"margin-top:40px;\" id=\"int_mobileno\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 488
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 491
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 493
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget');
            echo "
                      ";
            // line 494
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intmobile\"></div>
              </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 500
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 503
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 505
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget');
            echo "
                      ";
            // line 506
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
              </div>
             </div>
              ";
        }
        // line 511
        echo "            </div>

            <div class=\"photo_one_form\" id=\"hireBlock\">
              <div class=\"inner_photo_one_form hire_cont\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 517
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/looking to hire.png"), "html", null, true);
        echo "\" alt=\"Looking To Hire\">
                  </div>
                  <div class=\"inner_photo_one_form_table resp1\">
                    <h3>When are you looking to hire?*</h3>
                    <div class=\"child_pro_tabil_main  \">
                      ";
        // line 522
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                  </div>
                </div>
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 527
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/information.png"), "html", null, true);
        echo "\" alt=\"Information\">
                </div>
                <div class=\"inner_photo_one_form_table resp1\">
                    <h3>Is there any other additional information you would like to provide?</h3>
                    ";
        // line 531
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "
                     <br> <h5 class=\"char\">(max 120 characters)</h5>
                </div>
                ";
        // line 541
        echo "                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue3\" class=\"button3\">";
        // line 543
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "CONTINUE", "attr" => array("class" => "")));
        echo "</a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback2\">BACK</a></div>
                </div>
              </div>
          </div>
        </div>
        </script></div>
        ";
        // line 552
        echo "        ";
        // line 553
        echo "        <div id=\"step4\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <p>Page 4 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <h3 class=\"thankyou-heading\"><b>Thank you, you’re done!</b></h3>
              <p class=\"thankyou-content\" id=\"enquiryResult\">Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!</p>
              <p class=\"thankyou-content\">If you thought BusinessBid was useful to you, why not share the love? We’d greatly appreciate it. Like us on
              Facebook or follow us on Twitter to to keep updated on all the news and best deals.</p>
              <div class=\"social-icon\">
                <a href=\"https://twitter.com/\"><img src=\"";
        // line 592
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/twitter.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Twitter Bird\"/></a>
                <a href=\"https://www.facebook.com/\"><img src=\"";
        // line 593
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Facebook Sign\" /></a>
              </div>
              <div id=\"thankyou-btn\"  class=\"inner_photo_one_form\">
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 597
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">LOG ANOTHER REQUEST</a>
              </div>
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 600
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" >CONTINUE TO HOMEPAGE</a>
              </div>
              </div>
          </div>
        </div>
        ";
        // line 606
        echo "           ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
</div>
</div>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script type=\"text/javascript\">
    function scrollBox (tag) {
        \$('html,body').animate({scrollTop: tag.position().top -10},'slow');
    }
    \$(document).ready(function () {
        var forms = [
            '[ name=\"";
        // line 621
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
          ];
        var validationFlag = true;
          \$( forms.join(',') ).submit( function( e ){
            e.preventDefault();
            console.log(\"tried submitting\")
            if(validationFlag) {
                postForm( \$(this), function( response ){
                    if(response.hasOwnProperty('error')) {
                        alert(response.error);
                        var i = 1;
                        for (i = 1; i < 5; i++) {
                            \$(\"div#step\"+i).hide();
                        };
                        \$(\"div#step\"+response.step).show();
                    } else {
                        var form=document.createElement(\"form\");form.setAttribute(\"method\",\"post\"),form.setAttribute(\"action\",'";
        // line 637
        echo $this->env->getExtension('routing')->getPath("bizbids_job_post_sucessful");
        echo "');var hiddenField=document.createElement(\"input\");hiddenField.setAttribute(\"name\",\"jobno\"),hiddenField.setAttribute(\"value\",response.jobNo),hiddenField.setAttribute(\"type\",\"hidden\"),form.appendChild(hiddenField),document.body.appendChild(form),form.submit();
                        \$(\"#category_form\")[0].reset();
                    }
                });
                ga('send', 'event', 'Button', 'Click', 'Form Sent');
            }
            return false;
          });
        function postForm( \$form, callback ) {
            NProgress.start();
              \$.ajax({
                type        : \$form.attr( 'method' ),
                beforeSend  : function() { NProgress.inc() },
                url         : \$form.attr( 'action' ),
                data        : \$form.serialize(),
                success     : function(data) {
                  callback( data );
                  NProgress.done();
                },
                error : function (xhr) {
                    alert(\"Error occured.please try again\");
                    NProgress.done();
                }
              });
        }
    })
</script>
<script type=\"text/javascript\" src=\"";
        // line 664
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 665
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script type=\"text/javascript\" src=\"";
        // line 666
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("js/reviewsformsvalidation/" . (isset($context["twigFileName"]) ? $context["twigFileName"] : null)) . ".min.js")), "html", null, true);
        echo "\" ></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:beauty_styling_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1264 => 666,  1260 => 665,  1256 => 664,  1226 => 637,  1207 => 621,  1188 => 606,  1180 => 600,  1174 => 597,  1167 => 593,  1163 => 592,  1122 => 553,  1120 => 552,  1109 => 543,  1105 => 541,  1099 => 531,  1092 => 527,  1084 => 522,  1076 => 517,  1068 => 511,  1060 => 506,  1056 => 505,  1051 => 503,  1045 => 500,  1036 => 494,  1032 => 493,  1027 => 491,  1021 => 488,  1016 => 485,  1013 => 484,  1005 => 479,  1001 => 478,  995 => 475,  986 => 469,  982 => 468,  976 => 465,  972 => 463,  970 => 462,  965 => 459,  956 => 453,  952 => 452,  945 => 448,  939 => 445,  929 => 438,  925 => 437,  918 => 433,  912 => 430,  907 => 427,  905 => 426,  902 => 425,  894 => 420,  890 => 419,  884 => 416,  880 => 414,  877 => 413,  867 => 406,  862 => 404,  856 => 401,  849 => 397,  837 => 388,  833 => 387,  827 => 384,  823 => 383,  814 => 377,  809 => 374,  800 => 368,  796 => 367,  790 => 364,  779 => 356,  775 => 355,  769 => 352,  763 => 348,  761 => 347,  726 => 314,  724 => 313,  710 => 300,  704 => 296,  701 => 295,  687 => 294,  678 => 290,  670 => 289,  665 => 288,  657 => 283,  653 => 282,  647 => 280,  644 => 279,  627 => 278,  619 => 273,  614 => 270,  611 => 269,  608 => 268,  605 => 267,  602 => 266,  600 => 265,  593 => 260,  587 => 256,  584 => 255,  564 => 251,  556 => 250,  551 => 249,  534 => 248,  526 => 243,  521 => 240,  518 => 239,  515 => 238,  512 => 237,  509 => 236,  507 => 235,  495 => 226,  491 => 225,  483 => 220,  479 => 219,  469 => 212,  458 => 204,  451 => 200,  441 => 192,  435 => 188,  432 => 187,  418 => 186,  409 => 182,  401 => 181,  396 => 180,  388 => 175,  384 => 174,  378 => 172,  375 => 171,  358 => 170,  350 => 165,  345 => 162,  342 => 161,  339 => 160,  336 => 159,  333 => 158,  331 => 157,  295 => 123,  283 => 112,  277 => 108,  274 => 107,  260 => 106,  251 => 102,  243 => 101,  238 => 100,  230 => 95,  226 => 94,  220 => 92,  217 => 91,  200 => 90,  195 => 87,  192 => 86,  189 => 85,  186 => 84,  183 => 83,  181 => 82,  176 => 80,  136 => 43,  126 => 35,  117 => 32,  114 => 31,  109 => 30,  100 => 27,  97 => 26,  93 => 25,  90 => 24,  81 => 21,  78 => 20,  74 => 19,  68 => 16,  64 => 15,  56 => 9,  53 => 8,  48 => 5,  45 => 4,  37 => 3,  31 => 2,  11 => 1,);
    }
}

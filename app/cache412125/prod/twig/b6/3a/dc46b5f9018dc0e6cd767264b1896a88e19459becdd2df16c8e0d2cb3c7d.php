<?php

/* BBidsBBidsHomeBundle:User:account.html.twig */
class __TwigTemplate_b63adc46b5f9018dc0e6cd767264b1896a88e19459becdd2df16c8e0d2cb3c7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:account.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">
<div class=\"page-title\"><h1>My Profile</h1></div>
";
        // line 7
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 8
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 13
        echo "<div class=\"\">

<div class=\"row\">
\t<div class=\"col-sm-6 vendor-user user-information dashboard-container\">
\t <div class=\"tab-pane active profile-inform\" id=\"profile\">
\t\t";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "\t\t\t<div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
\t\t";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 23
            echo "\t\t\t<div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "\t
\t</div>
\t<div class=\"pane-title\"><h2>Vendor Information</h2></div>

\t";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["account"]) ? $context["account"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["accounts"]) {
            // line 29
            echo "\t<div class=\"field-item\"><label>Contact Name</label><div class=\"field-value\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "contactname", array()), "html", null, true);
            echo "</div></div>
\t";
            // line 30
            if ( !twig_test_empty($this->getAttribute($context["accounts"], "bizname", array()))) {
                // line 31
                echo "\t<div class=\"field-item\"><label>Business Name</label><div class=\"field-value\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "bizname", array()), "html", null, true);
                echo "</div></div>
\t";
            }
            // line 33
            echo "\t<div class=\"field-item\"><label>Mobile Number</label><div class=\"field-value\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "smsphone", array()), "html", null, true);
            echo "</div></div>
\t";
            // line 34
            if ( !twig_test_empty($this->getAttribute($context["accounts"], "homephone", array()))) {
                // line 35
                echo "\t<div class=\"field-item\"><label>Business Phone Number</label><div class=\"field-value\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "homephone", array()), "html", null, true);
                echo "</div></div>
\t";
            }
            // line 37
            echo "\t";
            if ( !twig_test_empty($this->getAttribute($context["accounts"], "fax", array()))) {
                // line 38
                echo "\t<div class=\"field-item\"><label>Fax Number</label><div class=\"field-value\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "fax", array()), "html", null, true);
                echo "</div></div>
\t";
            }
            // line 40
            echo "\t";
            if ( !twig_test_empty($this->getAttribute($context["accounts"], "address", array()))) {
                // line 41
                echo "\t<div class=\"field-item\"><label>Address</label><div class=\"field-value\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "address", array()), "html", null, true);
                echo "</div></div>
\t";
            }
            // line 43
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['accounts'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) ? $context["user"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 45
            echo "\t<div class=\"field-item\"><label>Email Address</label><div class=\"field-value\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["users"], "email", array()), "html", null, true);
            echo "</div></div>
\t<div class=\"field-item\"><label>Created Date</label><div class=\"field-value\"> ";
            // line 46
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["users"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</div></div>
\t<div class=\"field-item\"><label>Updated Date</label><div class=\"field-value\"> ";
            // line 47
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["users"], "updated", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</div></div>
\t<div class=\"field-item\"><label>Last Access Date</label><div class=\"field-value\"> ";
            // line 48
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["users"], "lastaccess", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</div></div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "\t</div>
";
        // line 53
        echo "
\t";
        // line 55
        echo "\t";
        // line 73
        echo "

\t";
        // line 76
        echo "</div>

<div class=\"row text-right update-account\">
\t";
        // line 79
        if (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
            // line 80
            echo "\t\t<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_update", array("vendorid" => (isset($context["uid"]) ? $context["uid"] : null))), "html", null, true);
            echo "\" class=\"btn btn-success\"> Update My Profile </a>
\t";
        } else {
            // line 82
            echo "\t\t<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_update", array("consumerid" => (isset($context["uid"]) ? $context["uid"] : null))), "html", null, true);
            echo "\" class=\"btn btn-success\"> Update My Profile</a>
\t";
        }
        // line 84
        echo "</div>
<div class=\"business-information dashboard-container\">
<div class=\"pane-title\"><h2>Services Provided</h2></div>
<div class=\"sub-pane-title\"><h3>List the services you provide</h3></div>
<div class=\"\">
\t
\t<form action=\"";
        // line 90
        echo $this->env->getExtension('routing')->getPath("bizbids_addcat");
        echo "\" method=\"post\">
\t\t<div class=\"morecertificates subcatadd\">
\t\t\t<input type=\"text\" id=\"form_cat\" name=\"cat[]\" placeholder=\"Enter your category\">
\t\t\t
\t\t</div>
\t\t<a id=\"addform\">Add</a>
\t\t<div id=\"moreproducts\">
\t\t</div>
\t\t
\t\t<input type=\"hidden\" id=\"vendorid\" name=\"vendorid\" value=\"";
        // line 99
        echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
        echo "\">
\t\t<input type=\"hidden\" id=\"setdata\" value=\"0\">
\t\t<input type=\"submit\" id=\"savecat\" name=\"form[save]\" value=\"Save\">
\t\t
\t\t
\t</form>
\t\t
\t
</div>

";
        // line 109
        if ( !twig_test_empty((isset($context["productsArray"]) ? $context["productsArray"] : null))) {
            // line 110
            echo "       <!-- <div class=\"field-item\"><label>Product</label><div class=\"field-value\"></div></div>-->
        ";
        }
        // line 112
        if ( !twig_test_empty((isset($context["maincat"]) ? $context["maincat"] : null))) {
            // line 113
            echo "<div class=\"field-item\"><label>Service Category</label>
";
            // line 114
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["maincat"]) ? $context["maincat"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                // line 115
                echo "        <div class=\"field-value\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "category", array()), "html", null, true);
                echo "</div>
  \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 116
            echo " </div>     ";
        }
        // line 117
        echo "  \t";
        // line 124
        echo " ";
        if ( !twig_test_empty((isset($context["certArray"]) ? $context["certArray"] : null))) {
            // line 125
            echo "        <div class=\"field-item\"><label>Certificates</label>
        ";
            // line 126
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["certArray"]) ? $context["certArray"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 127
                echo "        <div class=\"field-value\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "certification", array()), "html", null, true);
                echo "</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "        </div>
        ";
        }
        // line 131
        echo "   
";
        // line 132
        if ( !twig_test_empty((isset($context["displaycat"]) ? $context["displaycat"] : null))) {
            // line 133
            echo "<div class=\"field-item\"><label>Service Portfolio</label>
\t";
            // line 134
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["displaycat"]) ? $context["displaycat"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                // line 135
                echo "\t<div class=\"main-cat\">
\t\t<div class=\"field-value editcategory\" id=\"";
                // line 136
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\">
\t\t\t<span class=\"catname\">";
                // line 137
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "category", array()), "html", null, true);
                echo " </span>
\t\t\t
\t\t</div>
\t\t<span id=\"editdt\" >
\t\t\t\t<input id=\"vendid\" type=\"hidden\" value=\"";
                // line 141
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t<span class=\"editcatname\"><a href=\"#editdt\" id=\"editcat";
                // line 142
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\" class=\"editcat\">Edit</a>  </span>
\t\t\t\t<span class=\"deletecatname\"><a href=\"";
                // line 143
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("bizbids_deletecat", array("id" => $this->getAttribute($context["a"], "id", array()))), "html", null, true);
                echo "\">Delete</a> </span> 
\t\t\t</span>
\t</div>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "\t</div>
";
        }
        // line 149
        echo "
</div>
</div>

</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:account.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  326 => 149,  322 => 147,  312 => 143,  308 => 142,  304 => 141,  297 => 137,  293 => 136,  290 => 135,  286 => 134,  283 => 133,  281 => 132,  278 => 131,  274 => 129,  265 => 127,  261 => 126,  258 => 125,  255 => 124,  253 => 117,  250 => 116,  241 => 115,  237 => 114,  234 => 113,  232 => 112,  228 => 110,  226 => 109,  213 => 99,  201 => 90,  193 => 84,  187 => 82,  181 => 80,  179 => 79,  174 => 76,  170 => 73,  168 => 55,  165 => 53,  162 => 51,  153 => 48,  149 => 47,  145 => 46,  140 => 45,  135 => 44,  129 => 43,  123 => 41,  120 => 40,  114 => 38,  111 => 37,  105 => 35,  103 => 34,  98 => 33,  92 => 31,  90 => 30,  85 => 29,  81 => 28,  75 => 24,  66 => 23,  62 => 22,  59 => 21,  50 => 19,  46 => 18,  39 => 13,  37 => 8,  35 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}

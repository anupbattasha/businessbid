<?php

/* BBidsBBidsHomeBundle:Home:node3.html.twig */
class __TwigTemplate_f9dc4d82c82fc5b6c328178af86d9700ba16e47e00e63dbf2947b7c41136633f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node3.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_banner($context, array $blocks = array())
    {
        // line 3
        echo "
";
    }

    // line 5
    public function block_maincontent($context, array $blocks = array())
    {
        // line 6
        echo "<script>
\$(document).ready(function(){
\t\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\t\t\$.ajax({url:realpath+\"pullCategoryForFindMeVendor.php\",success:function(result){

\t\t\t\t\t\$(\"#categoryfind\").html(result);
\t\t\t\t}});
\t\t\t\$.ajax({url:realpath+\"pullCityForFindMeVendor.php\",success:function(result){

\t\t\t\t\t\$(\"#city\").html(result);
\t\t\t\t}});
});
</script>
<div class=\"container how-works-block\">
<div class=\"page-title1\"><h1>How Business Bid Works</h1></div>
\t<div class=\"row\">
\t\t<div class=\"how-work-icon1 col-md-5\">
\t\t\t<h2>Tell Us About Your Requirements</h2>
\t\t</div>
\t\t<div class=\"col-md-7\">
\t\t\t<ul>
\t\t\t\t<li>Select a service or service category that is most applicable to your service requirement</li>
\t\t\t\t<li>Provide a brief description that will allow us to match you with the best vendor</li>
\t\t\t</ul>

\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"how-work-icon2 col-md-5\">
\t\t\t<h2>Get Quotations From Approved Vendors</h2>
\t\t</div>
\t\t<div class=\"col-md-7\">
\t\t\t<ul>
\t\t\t\t<li>Receive contacts and quotations from up to 3 screened and approved vendors</li>
\t\t\t\t<li>Check vendor ratings for previous jobs completed for added peace of mind</li>
\t\t\t</ul>

\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"how-work-icon3 col-md-5\">
\t\t\t<h2>Choose the Best Vendor</h2>
\t\t</div>
\t\t<div class=\"col-md-7\">
\t\t\t<ul>
\t\t\t\t<li>Choose the best vendor based on the quotes and previous ratings</li>
\t\t\t\t<li>Rate the vendor for your project based on your experience</li>
\t\t\t</ul>

\t\t</div>
\t</div>

\t<div class=\"how-video\">
\t<h3>It's that Easy!</h3>
\t\t<iframe width=\"50%\" title=\"YouTube video player\" src=\"http://www.youtube.com/embed/7qxGW9Ne5gA?HD=1;rel=0;showinfo=0;controls=0\" height=\"258\" frameborder=\"0\"></iframe>
\t</div>
\t<div class=\"vendor-review-matched row\">
\t\t<div class=\"col-md-4 vendor-review-matched-img2\"></div>
\t\t<div class=\"col-md-8 vendor-review-matched-cont\">
\t\t\t<h2>Get Matched with Screened & Approved Vendors</h2>
\t\t\t<p>Allow us to match you with the most suitable vendors for your project. Receive three quotations for your project by telling us a little about your requirement.</p>
\t\t</div>
\t\t<div class=\"col-md-12 vendor-review-matched-filter\">
\t\t\t<div class=\"col-md-4\"><h3>Find Me Vendors</h3></div>
\t\t\t<form name=\"search\" method=\"request\" action=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
\t\t\t<div class=\"col-md-3\"><select name=\"category\" id=\"categoryfind\"></select></div>
\t\t\t<div class=\"col-md-3\"><select name=\"city\" id=\"city\"></select></div>
\t\t\t<div class=\"col-md-2\"><input type=\"submit\" class=\"btn btn-success\" name=\"submit\" value=\"FIND ME VENDORS\"></div>
\t\t\t</form>
\t\t</div>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 70,  40 => 6,  37 => 5,  32 => 3,  29 => 2,  11 => 1,);
    }
}

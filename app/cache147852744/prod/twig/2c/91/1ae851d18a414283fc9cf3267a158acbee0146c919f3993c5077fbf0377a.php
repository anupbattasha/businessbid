<?php

/* BBidsBBidsHomeBundle:Home:node4.html.twig */
class __TwigTemplate_2c911ae851d18a414283fc9cf3267a158acbee0146c919f3993c5077fbf0377a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node4.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"container\">
    <!-- <div class=\"breadcrum\">Home/ Vendor Review & Rating</div> -->
    <div class=\"vendor-review-dic row\">
        <!--[if lt IE 8]>
            <p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class=\"vendor_reviews_area\">
            <div class=\"vendor_reviews_area_child\">
                <div class=\"vendor_reviews_area_child_img\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper_one.png"), "html", null, true);
        echo "\" alt=\"Our One Page Praper\">
                </div>
                <div class=\"vendor_reviews_area_child_txt\">
                    <h2>Vendor Reviews Directory</h2>
                    <p>Search our directory for ratings and reviews for screened and approved BusinessBid service professionals. You can search for vendors by sorting by Highest Rated, Most Reviewed or by the Best Match vendors for the project. </p>
                    <p>Just select the type of service you are looking for, and you will find a list of rated service professionals for your project.</p>
                </div>
            </div>
        </div>
        <div class=\"position_area\">
            <div class=\"vandor_reviews_directory\">
                <div class=\"vandor_reviews_directory_txt\">
                    <h2>Browse Our Live Directory of Vendors</h2>
                </div>
                <div class=\"vandor_reviews_directory_dropdown\">
                    ";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["formreview"]) ? $context["formreview"] : null), 'form_start', array("attr" => array("id" => "reviewForm")));
        echo "
                        ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formreview"]) ? $context["formreview"] : null), "category", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "


                </div>
                <div class=\"vandor_reviews_directory_btn\">
                            ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formreview"]) ? $context["formreview"] : null), "submit", array()), 'row', array("label" => "BROWSE REVIEWS"));
        echo "
                            ";
        // line 40
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["formreview"]) ? $context["formreview"] : null), 'form_end');
        echo "
                        </div>
            </div>
        </div>
        <div class=\"thework_reviews_area\">
            <div class=\"thework_reviews_area_child\">
                <div class=\"thework_reviews_area_child_img\">
                    <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper_two.png"), "html", null, true);
        echo "\" alt=\"Our two Page Praper\">
                </div>
                <div class=\"thework_reviews_area_child_txt\">
                    <h2>LET US DO THE WORK FOR YOU</h2>
                    <p>Allow us to match you with the most suitable service professionals for your project. </p>
                    <p>Just select the type of job you need done and tell us a little bit about your requirement. We will get 3 of the most suitable screened and approved service professionals’ contact you with quotations.</p>
                </div>
            </div>
        </div>
        <div class=\"position_area_two\">
            <div class=\"thework_reviews_directory\">
                <div class=\"thework_reviews_directory_txt\">
                    <h2>Find Me Vendors</h2>
                </div>
                <div class=\"vandor_reviews_directory_dropdown\">
                    <form name=\"search\" method=\"GET\" action=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
                        <select name=\"category\" id=\"categoryfind\" class=\"form-control\">
                        ";
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoryArray"]) ? $context["categoryArray"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 65
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($context["category"], "category", array()) == (isset($context["chosenCategory"]) ? $context["chosenCategory"] : null))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                        </select>
                        <!-- <select name=\"city\" id=\"city\"></select> -->
                </div>
                <div class=\"thework_reviews_directory_btn\">
                        <input type=\"submit\" class=\"findme_btn\" name=\"submit\" value=\"FIND ME VENDORS\">
                        </form>
                </div>
            </div>
        </div>
       ";
        // line 121
        echo "      </div>
     </div>

        <!-- Footer Script Here -->
        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"js/vendor/jquery-1.11.2.min.js\"><\\/script>')</script>
        <script>
\$(document).ready(function(){
    \$(\"form#reviewForm\").submit(function(){
        var cat = \$('#form_category').val();
        if(cat == ''){
            alert(\"Please select a category...\");
            return false;
        }
        var urlPath = \"";
        // line 135
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => "catid"));
        echo "\";
        urlPath = urlPath.replace(\"catid\", cat);
        window.location = urlPath;
        return false;
    });
});
</script>
        ";
        // line 144
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node4.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 144,  163 => 135,  147 => 121,  136 => 67,  121 => 65,  117 => 64,  112 => 62,  94 => 47,  84 => 40,  80 => 39,  72 => 34,  68 => 33,  50 => 18,  37 => 7,  34 => 6,  29 => 3,  11 => 1,);
    }
}

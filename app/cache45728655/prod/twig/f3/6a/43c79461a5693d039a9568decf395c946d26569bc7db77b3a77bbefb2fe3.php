<?php

/* BBidsBBidsHomeBundle:Home:enquiry.html.twig */
class __TwigTemplate_f36a43c79461a5693d039a9568decf395c946d26569bc7db77b3a77bbefb2fe3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:enquiry.html.twig", 1);
        $this->blocks = array(
            'jquey' => array($this, 'block_jquey'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_jquey($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_banner($context, array $blocks = array())
    {
        // line 8
        echo "
";
    }

    // line 13
    public function block_maincontent($context, array $blocks = array())
    {
        // line 14
        echo "<style>
\t.pac-container:after{
    content:none !important;
}
</style>
<div class=\"container form-horizontal\">
\t<h1>Post your Requirement</h1>
\t";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 22
            echo "
\t\t<div class=\"alert alert-danger\"";
            // line 23
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
\t";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 28
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 29
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 33
            echo "
\t<div class=\"alert alert-success\">";
            // line 34
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t<div class=\"form_wrapper\">
\t
\t\t<div> ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo " </div>
\t\t<div> ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'errors');
        echo " </div>
\t\t<div> ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subj", array()), 'errors');
        echo " </div>
\t\t<div> ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'errors');
        echo " </div>
\t\t<div> ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'errors');
        echo " </div>
\t\t
\t\t<div >
\t\t\t<div >";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'label');
        echo "<span class=\"req\">*</span>: </div> <div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
\t\t<div style=\"display:none\" id=\"div_subcategory\">
\t\t\t<div>";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()), 'label');
        echo "<span  class=\"req\">*</span>: </div> 
\t\t\t<div class=\"control\" >  
\t\t\t\t\t<div id=\"checkbox_subcategory\" >
\t\t\t\t\t\tSelect a category first
\t\t\t\t\t\t";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 54
            echo "\t\t\t\t\t\t\t<div class=\"sub-category\" id=\"checkbox_subcategory\">
\t\t\t\t\t\t\t\t";
            // line 55
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
\t\t\t\t\t\t        </div>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo " 
\t\t\t\t\t\t";
        // line 59
        echo "\t\t\t\t\t</div>\t
\t\t\t</div>
\t\t</div>\t
\t\t\t<div>";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subj", array()), 'label');
        echo "<span class=\"req\">*</span>: </div> <div class=\"control\">  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subj", array()), 'widget');
        echo "</div>
\t\t\t<div>";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label');
        echo "<span class=\"req\">*</span>: </div> <div class=\"control\">  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "<p class=\"help-block2\">(maximum 240 characters)</p></div>
\t\t\t<div>";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
        echo "<span class=\"req\">*</span>: </div> <div class=\"control\">  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
        echo "</div
\t\t\t<div>";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
        echo "<span class=\"req\">*</span>: </div> <div class=\"control\">  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget');
        echo "</div>
\t\t\t<div>";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'label');
        echo ": </div> <div class=\"control\">  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "</div>
\t\t\t";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
\t\t</div>
\t\t";
        // line 69
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t</div>                                                                                                             
\t</div>
\t<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
\tvar input = document.getElementById('form_location');
\tvar options = {componentRestrictions: {country: 'AE'}};
\tnew google.maps.places.Autocomplete(input, options);
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:enquiry.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 69,  209 => 67,  203 => 66,  197 => 65,  191 => 64,  185 => 63,  179 => 62,  174 => 59,  171 => 57,  160 => 55,  157 => 54,  153 => 53,  146 => 49,  139 => 47,  133 => 44,  129 => 43,  125 => 42,  121 => 41,  117 => 40,  110 => 37,  101 => 34,  98 => 33,  93 => 32,  84 => 29,  81 => 28,  77 => 27,  74 => 26,  65 => 23,  62 => 22,  58 => 21,  49 => 14,  46 => 13,  41 => 8,  38 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }
}

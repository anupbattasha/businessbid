<?php

/* BBidsBBidsHomeBundle:Home:contactus.html.twig */
class __TwigTemplate_6d6a0bcdd0ee59cbe395c9da095b8931acb08718ec6a9ab7032dbaf87f7dd96b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:contactus.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "

<div id=\"contact\" class=\"contact-detail\">
\t<div class=\"container\">

\t\t<div class=\"page-title\">
\t\t\t<h1>Contact Us</h1>
\t\t\t<p>We would love to hear from you. You can call us, email us, send us the good old-fashioned letter or even visit us.</p>
\t\t</div>
\t\t\t";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 18
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 19
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
\t";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 24
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 25
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
\t";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 30
            echo "
\t<div class=\"alert alert-success\">";
            // line 31
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
\t\t<div class=\"contact-content\">
\t\t\t<div class=\"column-left\">
\t\t\t\t<div class=\"feedback-form\">


\t\t\t";
        // line 40
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("onsubmit" => "return contactUsValidate();")));
        echo "
\t\t\t\t\t<div class=\"control\">";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'label');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"control\">";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'label');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"control\">";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"control\">";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "usertype", array()), 'label');
        echo "
\t\t\t\t\t";
        // line 46
        echo "\t\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "usertype", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 47
            echo "\t\t\t\t\t\t<div class=\"col-md-3 sub-category\">
\t\t\t\t\t\t\t";
            // line 48
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"control\"> ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "message", array()), 'label');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "message", array()), 'widget');
        echo "<p class=\"help-block\">(maximum 1000 characters)</p></div>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<div class=\"captcha-block\">
\t\t\t\t\t\t\t<div class=\"col-sm-5 captcha-image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vvv/captcha.php"), "html", null, true);
        echo "\" id=\"captcha\" /><br/>
\t\t\t\t\t\t\t\t\t<a href=\"#\" onclick=\"scrollTo();\"
\t\t\t\t\t\t\t\t\t\tid=\"change-image\">Not readable? Change text.</a><br/><br/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-sm-7 captcha-text\">
\t\t\t\t\t\t\t\t\t";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "verification", array()), 'widget');
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"control\">";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "submit", array()), 'widget');
        echo "</div>

\t\t\t";
        // line 67
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"column-right\">
\t\t\t<div class=\"column-map\">
\t\t\t\t<p>If you would like to come and visit us, we are located at the address below: <br><br>Suite 503, Level 5,
Indigo Icon Tower, <br>Cluster F, Jumeirah Lakes Tower - Dubai, UAE<br>Sunday-Thursday 9 am - 6 pm <br/><br><span class=\"glyphicon glyphicon-earphone\"></span> 04 42 13 777<br/><span class=\"glyphicon glyphicon-envelope\"></span> BusinessBid DMCC, PO Box- 393507, Dubai, UAE
</p>

\t\t\t</div>
\t\t\t</div>

\t\t</div>

\t\t<div class=\"align-center\"><img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo " \"> </div>


\t</div>





</div>
<script type=\"text/javascript\">
\tfunction scrollTo() {
\t\tvar pathUrl = \"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vvv/captcha.php"), "html", null, true);
        echo "\"+\"?\";
\t\tdocument.getElementById('captcha').src=pathUrl+Math.random();
        \$('html, body').animate({ scrollTop: \$('.captcha-block').offset().top }, \"slow\");
        document.getElementById('form_verification').focus();
        return false;
    }
    function contactUsValidate () {
\t\tvar enteredText = \$(\"#form_verification\").val(),vFlag = true;

\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vvv/check.php"), "html", null, true);
        echo "\",
\t\t\tdata: {\"code\": enteredText},
\t\t\tsuccess: function (result) {
\t\t\t\tif(result == 'true') {
\t\t\t\t\treturn true;
\t\t\t\t} else {
\t\t\t\t\talert(\"Invalid Captcha\");
\t\t\t\t\tvFlag = false;
\t\t\t\t}
\t\t\t}
\t  \t});
\t  \treturn vFlag;
\t}
</script>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:contactus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 105,  220 => 94,  205 => 82,  187 => 67,  182 => 65,  175 => 61,  167 => 56,  159 => 52,  156 => 51,  145 => 48,  142 => 47,  137 => 46,  133 => 44,  127 => 43,  122 => 42,  117 => 41,  113 => 40,  105 => 34,  96 => 31,  93 => 30,  89 => 29,  86 => 28,  77 => 25,  74 => 24,  70 => 23,  67 => 22,  58 => 19,  55 => 18,  51 => 17,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

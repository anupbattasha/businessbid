<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">


    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 162
        $this->displayBlock('customcss', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('javascripts', $context, $blocks);
        // line 309
        echo "
";
        // line 310
        $this->displayBlock('customjs', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('jquery', $context, $blocks);
        // line 317
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 323
        echo "</head>
";
        // line 324
        ob_start();
        // line 325
        echo "<body>

";
        // line 327
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 328
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 329
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 330
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 331
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 342
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 343
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 345
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 346
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 349
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 350
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 352
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 353
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 359
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 366
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 369
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 372
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 374
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 379
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 384
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 385
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 387
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 390
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 391
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 395
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 402
        $this->displayBlock('maincontent', $context, $blocks);
        // line 633
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 636
        $this->displayBlock('footer', $context, $blocks);
        // line 749
        echo "    <!-- footer block ends -->
</div>";
        // line 752
        $this->displayBlock('footerScript', $context, $blocks);
        // line 756
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 763
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_f8e36c7c5b501f05870b78f637b58e1398753163c7d673536506f02a2774cc92 = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_882e4b0d88a046bfe2a2db057b9edb6eaab37c3089316d8cee15a4c592309928 = "http://www.businessbid.ae/contactus") && ('' === $__internal_882e4b0d88a046bfe2a2db057b9edb6eaab37c3089316d8cee15a4c592309928 || 0 === strpos($__internal_f8e36c7c5b501f05870b78f637b58e1398753163c7d673536506f02a2774cc92, $__internal_882e4b0d88a046bfe2a2db057b9edb6eaab37c3089316d8cee15a4c592309928)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_c8347af4e1486380477556b38d2bb832f00f6216f0872b21cb5fba0a99925ae2 = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0f14dca9a61bf737c5dcc8c8b5a66ad6fadf7c21ca1b44dcf8085c47ca188a92 = "http://www.businessbid.ae/node3") && ('' === $__internal_0f14dca9a61bf737c5dcc8c8b5a66ad6fadf7c21ca1b44dcf8085c47ca188a92 || 0 === strpos($__internal_c8347af4e1486380477556b38d2bb832f00f6216f0872b21cb5fba0a99925ae2, $__internal_0f14dca9a61bf737c5dcc8c8b5a66ad6fadf7c21ca1b44dcf8085c47ca188a92)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_85af28083f641720c82cbc3f4463630260c4258b87599e178029459f591809af = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c7f89c3a50adea80536f631c42a90fadaa31ed72f414734ae3a26b394ebc8623 = "http://www.businessbid.ae/review/login") && ('' === $__internal_c7f89c3a50adea80536f631c42a90fadaa31ed72f414734ae3a26b394ebc8623 || 0 === strpos($__internal_85af28083f641720c82cbc3f4463630260c4258b87599e178029459f591809af, $__internal_c7f89c3a50adea80536f631c42a90fadaa31ed72f414734ae3a26b394ebc8623)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_a92b98e857fe11e969a9f1954023dbaeeef12cd2a081f3bc50dccdcd0ef48638 = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c6617ea6f3db6e06de58fe2d164e818aa0075ce94a6aebdcb19b6b633d47030f = "http://www.businessbid.ae/node1") && ('' === $__internal_c6617ea6f3db6e06de58fe2d164e818aa0075ce94a6aebdcb19b6b633d47030f || 0 === strpos($__internal_a92b98e857fe11e969a9f1954023dbaeeef12cd2a081f3bc50dccdcd0ef48638, $__internal_c6617ea6f3db6e06de58fe2d164e818aa0075ce94a6aebdcb19b6b633d47030f)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_ca359ebc15ce29feeae9a30c4ba1240d19f793b64e5fc6ea264456f66003ce59 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d6291707b6d62953188879eda06b13b6baf42b9323be47da8bb24eb945e6d85f = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_d6291707b6d62953188879eda06b13b6baf42b9323be47da8bb24eb945e6d85f || 0 === strpos($__internal_ca359ebc15ce29feeae9a30c4ba1240d19f793b64e5fc6ea264456f66003ce59, $__internal_d6291707b6d62953188879eda06b13b6baf42b9323be47da8bb24eb945e6d85f)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_baa8f5418fb0d0c3c44ff8d0870273e4b76fc101a319d80d1542e9da86b7ab62 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_cf50eaf58a04f510cc97647f5c90f8d2f65e6c7d67240b519e2e4e78a57a826a = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_cf50eaf58a04f510cc97647f5c90f8d2f65e6c7d67240b519e2e4e78a57a826a || 0 === strpos($__internal_baa8f5418fb0d0c3c44ff8d0870273e4b76fc101a319d80d1542e9da86b7ab62, $__internal_cf50eaf58a04f510cc97647f5c90f8d2f65e6c7d67240b519e2e4e78a57a826a)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_2b6ea4d4ca400c240f7117622eefa12fe5e9c48261105eadc704fc6f406c929b = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f22488378c2abda9a0110be13f317e0d2507f741c4f0916776c9711193e8134e = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_f22488378c2abda9a0110be13f317e0d2507f741c4f0916776c9711193e8134e || 0 === strpos($__internal_2b6ea4d4ca400c240f7117622eefa12fe5e9c48261105eadc704fc6f406c929b, $__internal_f22488378c2abda9a0110be13f317e0d2507f741c4f0916776c9711193e8134e)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_bc1deb602c8e9a99345e97e50170c65528cc6df98fa6183db207d66667444000 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_e16450cea6c0094bc581ea9be65427d881e614c96f76ad0eabf8999bc57a9946 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_e16450cea6c0094bc581ea9be65427d881e614c96f76ad0eabf8999bc57a9946 || 0 === strpos($__internal_bc1deb602c8e9a99345e97e50170c65528cc6df98fa6183db207d66667444000, $__internal_e16450cea6c0094bc581ea9be65427d881e614c96f76ad0eabf8999bc57a9946)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_2860de37f920d182ded3cc7b09db88f8c58f6d93113685904b393f1d39b15cd7 = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ca2ec43ffe19cb1157b057a9f9e0e0aa3fa541acfbc0e648ad05b9a759e67dcb = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_ca2ec43ffe19cb1157b057a9f9e0e0aa3fa541acfbc0e648ad05b9a759e67dcb || 0 === strpos($__internal_2860de37f920d182ded3cc7b09db88f8c58f6d93113685904b393f1d39b15cd7, $__internal_ca2ec43ffe19cb1157b057a9f9e0e0aa3fa541acfbc0e648ad05b9a759e67dcb)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_67b44bb1776a32b17f444582c674600e257cdf2476279aa52305d86345c6cc29 = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8303f8eef800fcbc4494c0fcdd726c66d7280ee7c951bc6c644cb6abb5971aa6 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_8303f8eef800fcbc4494c0fcdd726c66d7280ee7c951bc6c644cb6abb5971aa6 || 0 === strpos($__internal_67b44bb1776a32b17f444582c674600e257cdf2476279aa52305d86345c6cc29, $__internal_8303f8eef800fcbc4494c0fcdd726c66d7280ee7c951bc6c644cb6abb5971aa6)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_61bb62f56c3ec940fe522314825d99d420c667911dd6998b878bbc2ad389715f = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_81687dabd7aba575d5e39bbbc37cd8a47700f1ba08acdaf1496a9efacce78655 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_81687dabd7aba575d5e39bbbc37cd8a47700f1ba08acdaf1496a9efacce78655 || 0 === strpos($__internal_61bb62f56c3ec940fe522314825d99d420c667911dd6998b878bbc2ad389715f, $__internal_81687dabd7aba575d5e39bbbc37cd8a47700f1ba08acdaf1496a9efacce78655)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_8f3b23c35374b372913b764181f4022c5ab81740fcdbdc4d1144845e85e573e0 = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_64bf6caa7b8684c9dd9c7d4a65e5a5bcc375659c7a8f3250ccbfc9987da56c2b = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_64bf6caa7b8684c9dd9c7d4a65e5a5bcc375659c7a8f3250ccbfc9987da56c2b || 0 === strpos($__internal_8f3b23c35374b372913b764181f4022c5ab81740fcdbdc4d1144845e85e573e0, $__internal_64bf6caa7b8684c9dd9c7d4a65e5a5bcc375659c7a8f3250ccbfc9987da56c2b)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_83e9b3b0a0a4e10f292b6c0c5b899389839bca8e60d1877bb0d2554094826458 = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1927bcde2af17d67c30ca2ab1943056715f47f7a088f86e8774ccd4be97fc271 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_1927bcde2af17d67c30ca2ab1943056715f47f7a088f86e8774ccd4be97fc271 || 0 === strpos($__internal_83e9b3b0a0a4e10f292b6c0c5b899389839bca8e60d1877bb0d2554094826458, $__internal_1927bcde2af17d67c30ca2ab1943056715f47f7a088f86e8774ccd4be97fc271)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_c346ba5b1344fa5a23fde38cd6525a91fb08c22f5624e387a4c7fa6fb6b0f229 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0a0f644491816d209b0edc91a86e0f2604d0120fefbf265170c1b42aa0e8430e = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_0a0f644491816d209b0edc91a86e0f2604d0120fefbf265170c1b42aa0e8430e || 0 === strpos($__internal_c346ba5b1344fa5a23fde38cd6525a91fb08c22f5624e387a4c7fa6fb6b0f229, $__internal_0a0f644491816d209b0edc91a86e0f2604d0120fefbf265170c1b42aa0e8430e)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_abb7b81caad03aa701cf8cbf8b019bc06ed27a182dbb6e938862c8ad6b7d3b64 = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_18115a65e9f7d45a388237eaef5127b56ebaad5f51644326bce39482fc0d4a4c = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_18115a65e9f7d45a388237eaef5127b56ebaad5f51644326bce39482fc0d4a4c || 0 === strpos($__internal_abb7b81caad03aa701cf8cbf8b019bc06ed27a182dbb6e938862c8ad6b7d3b64, $__internal_18115a65e9f7d45a388237eaef5127b56ebaad5f51644326bce39482fc0d4a4c)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_7b33a14e29e7b47b7dbfd9ef25387a7b0ba77c3799f575344ffaead50023b7b2 = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_792b70de2efff1be5a9519357821d3d7e2f92cd1b68b3c6613d924d11042e810 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_792b70de2efff1be5a9519357821d3d7e2f92cd1b68b3c6613d924d11042e810 || 0 === strpos($__internal_7b33a14e29e7b47b7dbfd9ef25387a7b0ba77c3799f575344ffaead50023b7b2, $__internal_792b70de2efff1be5a9519357821d3d7e2f92cd1b68b3c6613d924d11042e810)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_a6309a1c367510f63a1988cf9d68a1c7d00a55f8e9b455b837faaea35dd9b2c4 = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_970d80e46ce3192b20472332f642075584d01019648eb7f126e0aa0e57f51963 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_970d80e46ce3192b20472332f642075584d01019648eb7f126e0aa0e57f51963 || 0 === strpos($__internal_a6309a1c367510f63a1988cf9d68a1c7d00a55f8e9b455b837faaea35dd9b2c4, $__internal_970d80e46ce3192b20472332f642075584d01019648eb7f126e0aa0e57f51963)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_be958f2f7f23a2e382debaed478fb1e8a26650905c71799fd41b216ac8503740 = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8cd9aeb33f0ce6d5728c84aefe2c8cfd1ebcf59e1f8c5a298e9d463a43170e21 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_8cd9aeb33f0ce6d5728c84aefe2c8cfd1ebcf59e1f8c5a298e9d463a43170e21 || 0 === strpos($__internal_be958f2f7f23a2e382debaed478fb1e8a26650905c71799fd41b216ac8503740, $__internal_8cd9aeb33f0ce6d5728c84aefe2c8cfd1ebcf59e1f8c5a298e9d463a43170e21)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_9f8d9d2e298566654901f99c1442402426dfa14dee920148b0c48cf358d334b2 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8948a9436b408d2cb84b26992be3a1e8abf5a09ada0a0cc4c27cc77337c19d18 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_8948a9436b408d2cb84b26992be3a1e8abf5a09ada0a0cc4c27cc77337c19d18 || 0 === strpos($__internal_9f8d9d2e298566654901f99c1442402426dfa14dee920148b0c48cf358d334b2, $__internal_8948a9436b408d2cb84b26992be3a1e8abf5a09ada0a0cc4c27cc77337c19d18)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_ef6891ffbf862b3f9139c6dd84f2fbfdba303279f8ac1c1297bbcfc6a508fc9c = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_9a04b9aa747a0eef2b7ed5a6dfd9676c23e0a179316a7f0dd4f7a22abcd58d84 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_9a04b9aa747a0eef2b7ed5a6dfd9676c23e0a179316a7f0dd4f7a22abcd58d84 || 0 === strpos($__internal_ef6891ffbf862b3f9139c6dd84f2fbfdba303279f8ac1c1297bbcfc6a508fc9c, $__internal_9a04b9aa747a0eef2b7ed5a6dfd9676c23e0a179316a7f0dd4f7a22abcd58d84)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_fc7c26b734557516afebe1e878129946e0598ad52c1e359fb552919849499c8b = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_efc836731ee10ecf55f0e3a63704864623fe6488ed3579c17c9ffb8777671937 = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_efc836731ee10ecf55f0e3a63704864623fe6488ed3579c17c9ffb8777671937 || 0 === strpos($__internal_fc7c26b734557516afebe1e878129946e0598ad52c1e359fb552919849499c8b, $__internal_efc836731ee10ecf55f0e3a63704864623fe6488ed3579c17c9ffb8777671937)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_82a874f09d773846bec448eac92baa89ca393c643d8f215c8dd5ef6a077d9e7c = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8b5086832da8ea79d796b2d8d7ad90ef3a21605c5e35f0aa558a01c6703098c5 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_8b5086832da8ea79d796b2d8d7ad90ef3a21605c5e35f0aa558a01c6703098c5 || 0 === strpos($__internal_82a874f09d773846bec448eac92baa89ca393c643d8f215c8dd5ef6a077d9e7c, $__internal_8b5086832da8ea79d796b2d8d7ad90ef3a21605c5e35f0aa558a01c6703098c5)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_537d17e700198a7cce8dc6823792202a44ae4852b7613c70a4802a3ddc226df9 = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6bf0cbd3d1cb96099eba7537466a32c0d7471de68a9097b5301258b1939baedb = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_6bf0cbd3d1cb96099eba7537466a32c0d7471de68a9097b5301258b1939baedb || 0 === strpos($__internal_537d17e700198a7cce8dc6823792202a44ae4852b7613c70a4802a3ddc226df9, $__internal_6bf0cbd3d1cb96099eba7537466a32c0d7471de68a9097b5301258b1939baedb)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_b151d4d102e0ca65420868bf16378eabc6aec9e9d65f743f3dd2ad6846dbd22c = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_4638ab6867b500ec82f13ac53821d0bf1d449888a68197ee0a729de242de3b0b = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_4638ab6867b500ec82f13ac53821d0bf1d449888a68197ee0a729de242de3b0b || 0 === strpos($__internal_b151d4d102e0ca65420868bf16378eabc6aec9e9d65f743f3dd2ad6846dbd22c, $__internal_4638ab6867b500ec82f13ac53821d0bf1d449888a68197ee0a729de242de3b0b)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_6096c373e37bf284a8d366a546c305b8553f1cd56612671aae34d81706caa88a = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_517cbfb2e7b42093e852c225ea50e2289e8b2b8b99bcd58c121bb57cc1ba5275 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_517cbfb2e7b42093e852c225ea50e2289e8b2b8b99bcd58c121bb57cc1ba5275 || 0 === strpos($__internal_6096c373e37bf284a8d366a546c305b8553f1cd56612671aae34d81706caa88a, $__internal_517cbfb2e7b42093e852c225ea50e2289e8b2b8b99bcd58c121bb57cc1ba5275)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_0b7f33370d238c80271d710e04101f600c3746230cbe9e1ee5f7038d03b01332 = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_20762f91ba13030422b70d7d16b7baa0c6a23437978f06849f4cf4d2e6546f1c = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_20762f91ba13030422b70d7d16b7baa0c6a23437978f06849f4cf4d2e6546f1c || 0 === strpos($__internal_0b7f33370d238c80271d710e04101f600c3746230cbe9e1ee5f7038d03b01332, $__internal_20762f91ba13030422b70d7d16b7baa0c6a23437978f06849f4cf4d2e6546f1c)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_a20411818a5686b8e612d28c90dfb90f23dc6ef754872ff2647c94c522ed9970 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_752aefd3104d8f4bac35485e3558541c25a4c0ede02ec943aee28c68098d6c11 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_752aefd3104d8f4bac35485e3558541c25a4c0ede02ec943aee28c68098d6c11 || 0 === strpos($__internal_a20411818a5686b8e612d28c90dfb90f23dc6ef754872ff2647c94c522ed9970, $__internal_752aefd3104d8f4bac35485e3558541c25a4c0ede02ec943aee28c68098d6c11)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_3f0c2692538e488fc96939906cac1786922e0d565ae499adcedbb33e702c6298 = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_037f2390af3ab2d0ea23fde9d1c62c5bcfa6c247cf30be437c51f0314307b1b8 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_037f2390af3ab2d0ea23fde9d1c62c5bcfa6c247cf30be437c51f0314307b1b8 || 0 === strpos($__internal_3f0c2692538e488fc96939906cac1786922e0d565ae499adcedbb33e702c6298, $__internal_037f2390af3ab2d0ea23fde9d1c62c5bcfa6c247cf30be437c51f0314307b1b8)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_6a96deeed0728af22fe06c874cb9f3f5531fd3b49471c1ebd0c998d554292989 = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_7af2f639cf6dd2343c937f204a9cf96c8075b9b4d844aed8cfeed887e9e3ab34 = "http://www.businessbid.ae/node2") && ('' === $__internal_7af2f639cf6dd2343c937f204a9cf96c8075b9b4d844aed8cfeed887e9e3ab34 || 0 === strpos($__internal_6a96deeed0728af22fe06c874cb9f3f5531fd3b49471c1ebd0c998d554292989, $__internal_7af2f639cf6dd2343c937f204a9cf96c8075b9b4d844aed8cfeed887e9e3ab34)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_44e96cedd4f8f4ffb2bc3047983a6eb8a082e47d838dd969d91ea2da595143ef = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2284a0d06655c28f8dc66dbfb81d4903330267f6b67dc433bfc16d2a460c8a7e = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_2284a0d06655c28f8dc66dbfb81d4903330267f6b67dc433bfc16d2a460c8a7e || 0 === strpos($__internal_44e96cedd4f8f4ffb2bc3047983a6eb8a082e47d838dd969d91ea2da595143ef, $__internal_2284a0d06655c28f8dc66dbfb81d4903330267f6b67dc433bfc16d2a460c8a7e)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_787fbf906ee76bf33369ff23a30376ddccb09e96568767ba60a8835b134e3d72 = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8ffb64d591897953a17b9d3ff88db5360c23280cd0a072975ccfd5eca60e8875 = "http://www.businessbid.ae/faq") && ('' === $__internal_8ffb64d591897953a17b9d3ff88db5360c23280cd0a072975ccfd5eca60e8875 || 0 === strpos($__internal_787fbf906ee76bf33369ff23a30376ddccb09e96568767ba60a8835b134e3d72, $__internal_8ffb64d591897953a17b9d3ff88db5360c23280cd0a072975ccfd5eca60e8875)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_149e0363e1ad3d5404a6c44695e2c37b581ca6b63d5c008cbb570687cde4605b = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f303ad96db19c6ca2c069e57d5663a23d180f315f1fc65fc37f037b2c1f28870 = "http://www.businessbid.ae/node4") && ('' === $__internal_f303ad96db19c6ca2c069e57d5663a23d180f315f1fc65fc37f037b2c1f28870 || 0 === strpos($__internal_149e0363e1ad3d5404a6c44695e2c37b581ca6b63d5c008cbb570687cde4605b, $__internal_f303ad96db19c6ca2c069e57d5663a23d180f315f1fc65fc37f037b2c1f28870)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_2f31fd51b88ecff845cd68ac106b570c7bf202ad89443b1b8929506f4bc5f1ba = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_97130af0b3fbe76f1b89adfd230a1fa28c3d14344a8b27876c3cedeaf674d5ba = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_97130af0b3fbe76f1b89adfd230a1fa28c3d14344a8b27876c3cedeaf674d5ba || 0 === strpos($__internal_2f31fd51b88ecff845cd68ac106b570c7bf202ad89443b1b8929506f4bc5f1ba, $__internal_97130af0b3fbe76f1b89adfd230a1fa28c3d14344a8b27876c3cedeaf674d5ba)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_8dfa7475cb9930d8f5e1781313d18623e59811d9c0e699a3d33d603db59c48c2 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0620ed5b7f3898c27e6691f51d5c4b34e1050d141c589c665672ef9cd93b5d2a = "http://www.businessbid.ae/node6") && ('' === $__internal_0620ed5b7f3898c27e6691f51d5c4b34e1050d141c589c665672ef9cd93b5d2a || 0 === strpos($__internal_8dfa7475cb9930d8f5e1781313d18623e59811d9c0e699a3d33d603db59c48c2, $__internal_0620ed5b7f3898c27e6691f51d5c4b34e1050d141c589c665672ef9cd93b5d2a)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_2f9269d431c8897e7a5a03c98d743f572b69cc3344616971cf50393e51a2c39c = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_12d952b89b333b162d5cbabe75b74a5759a9aeb1b4323fabc7ab5e4172d30407 = "http://www.businessbid.ae/feedback") && ('' === $__internal_12d952b89b333b162d5cbabe75b74a5759a9aeb1b4323fabc7ab5e4172d30407 || 0 === strpos($__internal_2f9269d431c8897e7a5a03c98d743f572b69cc3344616971cf50393e51a2c39c, $__internal_12d952b89b333b162d5cbabe75b74a5759a9aeb1b4323fabc7ab5e4172d30407)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_fbbc3945bf47ebf3d15ade8fb50fcb671fe46ece08f5ddfc7d58c60f652b5439 = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c9ae4a9487a7f7962062185a55cf5a3f9a30011bcb3464ad7cb62f486dc81088 = "http://www.businessbid.ae/sitemap") && ('' === $__internal_c9ae4a9487a7f7962062185a55cf5a3f9a30011bcb3464ad7cb62f486dc81088 || 0 === strpos($__internal_fbbc3945bf47ebf3d15ade8fb50fcb671fe46ece08f5ddfc7d58c60f652b5439, $__internal_c9ae4a9487a7f7962062185a55cf5a3f9a30011bcb3464ad7cb62f486dc81088)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_561b46eafabb611a98f73069228ed340afdae815a0c4fbbc08c35763eb0b9be0 = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b68024ef7bf04c360bab15913f0737d6f1f7b672b1b3b148c5d41ba3f169d565 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_b68024ef7bf04c360bab15913f0737d6f1f7b672b1b3b148c5d41ba3f169d565 || 0 === strpos($__internal_561b46eafabb611a98f73069228ed340afdae815a0c4fbbc08c35763eb0b9be0, $__internal_b68024ef7bf04c360bab15913f0737d6f1f7b672b1b3b148c5d41ba3f169d565)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_a6c99b6176c134152bf87011f25856adc4da883fc77d5faa59c3b16cb9467371 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6d276c6f7d412a0dab571249a330f8d70612b7ce514c82e001edfa93e04dba5b = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_6d276c6f7d412a0dab571249a330f8d70612b7ce514c82e001edfa93e04dba5b || 0 === strpos($__internal_a6c99b6176c134152bf87011f25856adc4da883fc77d5faa59c3b16cb9467371, $__internal_6d276c6f7d412a0dab571249a330f8d70612b7ce514c82e001edfa93e04dba5b)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_95389ab08926d13857919a51c5c67e18770f950f19b126fd8128d8b25dcb61c3 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f130474391f6db583586cbbc379d9300e4fbdeab5d340dced4dd3d20341db63c = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_f130474391f6db583586cbbc379d9300e4fbdeab5d340dced4dd3d20341db63c || 0 === strpos($__internal_95389ab08926d13857919a51c5c67e18770f950f19b126fd8128d8b25dcb61c3, $__internal_f130474391f6db583586cbbc379d9300e4fbdeab5d340dced4dd3d20341db63c)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_1db2ff8a78ef6e70c46612fbed54c6fa23c0f2e575c09b03d20ab4bc344b891b = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_14f5af0a1eafaac73f4c2105b540ab33da91548780a47c2bb6a40e8f284c51cb = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_14f5af0a1eafaac73f4c2105b540ab33da91548780a47c2bb6a40e8f284c51cb || 0 === strpos($__internal_1db2ff8a78ef6e70c46612fbed54c6fa23c0f2e575c09b03d20ab4bc344b891b, $__internal_14f5af0a1eafaac73f4c2105b540ab33da91548780a47c2bb6a40e8f284c51cb)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 162
    public function block_customcss($context, array $blocks = array())
    {
        // line 163
        echo "
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "<script>
\$(document).ready(function(){
    ";
        // line 169
        if (array_key_exists("keyword", $context)) {
            // line 170
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 174
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 310
    public function block_customjs($context, array $blocks = array())
    {
        // line 311
        echo "
";
    }

    // line 314
    public function block_jquery($context, array $blocks = array())
    {
        // line 315
        echo "
";
    }

    // line 317
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 318
        echo "
<!-- Google Analytics -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>

";
    }

    // line 402
    public function block_maincontent($context, array $blocks = array())
    {
        // line 403
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 490
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 530
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 560
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 561
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 563
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 565
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 571
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 572
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 574
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 576
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 581
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 582
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 584
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 586
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 593
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 594
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 596
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 598
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 604
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 605
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 607
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 609
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 615
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 616
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 618
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 620
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 490
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 491
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 500
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 501
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 502
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 503
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 504
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 505
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 506
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 508
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 509
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 510
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 512
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 519
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 525
        echo "    </div>
            </div>


    ";
    }

    // line 519
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 520
        echo "    <div class=\"request\">
    <h2>";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 636
    public function block_footer($context, array $blocks = array())
    {
        // line 637
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 666
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 687
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 688
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 740
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 742
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 752
    public function block_footerScript($context, array $blocks = array())
    {
        // line 753
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 754
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1411 => 754,  1406 => 753,  1403 => 752,  1392 => 742,  1387 => 740,  1347 => 703,  1339 => 698,  1335 => 697,  1331 => 696,  1320 => 688,  1317 => 687,  1313 => 685,  1304 => 679,  1300 => 678,  1296 => 677,  1292 => 676,  1288 => 675,  1277 => 667,  1273 => 666,  1264 => 660,  1260 => 659,  1256 => 658,  1252 => 657,  1248 => 656,  1227 => 637,  1224 => 636,  1216 => 521,  1213 => 520,  1210 => 519,  1202 => 525,  1200 => 519,  1193 => 514,  1186 => 512,  1170 => 510,  1167 => 509,  1164 => 508,  1150 => 506,  1147 => 505,  1144 => 504,  1142 => 503,  1138 => 502,  1135 => 501,  1131 => 500,  1121 => 493,  1117 => 491,  1114 => 490,  1098 => 620,  1092 => 618,  1086 => 616,  1084 => 615,  1076 => 609,  1070 => 607,  1064 => 605,  1062 => 604,  1054 => 598,  1048 => 596,  1042 => 594,  1040 => 593,  1031 => 586,  1025 => 584,  1019 => 582,  1017 => 581,  1010 => 576,  1004 => 574,  998 => 572,  996 => 571,  988 => 565,  982 => 563,  976 => 561,  974 => 560,  942 => 530,  940 => 490,  925 => 478,  914 => 470,  903 => 462,  892 => 454,  881 => 446,  871 => 439,  853 => 424,  843 => 417,  833 => 410,  824 => 403,  821 => 402,  813 => 318,  810 => 317,  805 => 315,  802 => 314,  797 => 311,  794 => 310,  775 => 294,  653 => 174,  647 => 170,  645 => 169,  641 => 167,  638 => 166,  633 => 163,  630 => 162,  626 => 127,  623 => 126,  617 => 109,  613 => 107,  611 => 106,  608 => 105,  606 => 104,  603 => 103,  601 => 102,  598 => 101,  596 => 100,  593 => 99,  591 => 98,  588 => 97,  586 => 96,  583 => 95,  581 => 94,  578 => 93,  576 => 92,  573 => 91,  571 => 90,  568 => 89,  566 => 88,  563 => 87,  561 => 86,  558 => 85,  556 => 84,  553 => 83,  551 => 82,  548 => 81,  546 => 80,  543 => 79,  541 => 78,  538 => 77,  536 => 76,  533 => 75,  531 => 74,  528 => 73,  526 => 72,  523 => 71,  521 => 70,  518 => 69,  516 => 68,  513 => 67,  511 => 66,  508 => 65,  506 => 64,  503 => 63,  501 => 62,  498 => 61,  496 => 60,  493 => 59,  491 => 58,  488 => 57,  486 => 56,  483 => 55,  481 => 54,  478 => 53,  476 => 52,  473 => 51,  471 => 50,  468 => 49,  466 => 48,  463 => 47,  461 => 46,  458 => 45,  456 => 44,  453 => 43,  451 => 42,  448 => 41,  446 => 40,  443 => 39,  441 => 38,  438 => 37,  436 => 36,  433 => 35,  431 => 34,  428 => 33,  426 => 32,  423 => 31,  421 => 30,  418 => 29,  416 => 28,  413 => 27,  411 => 26,  408 => 25,  406 => 24,  403 => 23,  401 => 22,  398 => 21,  396 => 20,  393 => 19,  391 => 18,  388 => 17,  386 => 16,  383 => 15,  381 => 14,  378 => 13,  376 => 12,  373 => 11,  368 => 763,  359 => 756,  357 => 752,  354 => 749,  352 => 636,  347 => 633,  345 => 402,  336 => 395,  322 => 392,  319 => 391,  317 => 390,  311 => 387,  307 => 385,  305 => 384,  297 => 379,  289 => 374,  284 => 372,  278 => 369,  273 => 367,  269 => 366,  260 => 359,  244 => 354,  240 => 353,  236 => 352,  232 => 350,  230 => 349,  224 => 346,  218 => 345,  214 => 343,  212 => 342,  201 => 336,  194 => 331,  192 => 330,  190 => 329,  188 => 328,  186 => 327,  182 => 325,  180 => 324,  177 => 323,  175 => 317,  173 => 314,  170 => 313,  168 => 310,  165 => 309,  163 => 166,  160 => 165,  158 => 162,  153 => 160,  149 => 159,  140 => 153,  135 => 151,  131 => 150,  126 => 148,  122 => 147,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

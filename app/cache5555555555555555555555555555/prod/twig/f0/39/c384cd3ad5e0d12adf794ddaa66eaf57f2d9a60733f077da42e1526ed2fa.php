<?php

/* BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig */
class __TwigTemplate_f039c384cd3ad5e0d12adf794ddaa66eaf57f2d9a60733f077da42e1526ed2fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "About us";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/about_style.css"), "html", null, true);
        echo "\">
<!-- Start of about Us Area -->
        <div id=\"top_about_us_area\">
            <div class=\"main_top_about_us_area\">
                <div class=\"inner_top_about_us_img\">
                    <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/about_infography.png"), "html", null, true);
        echo "\" alt=\"BUSINESSBID CUSTOMER BENEFITS\">
                </div>
                <div class=\"inner_top_about_us_text\">
                    <h2>ABOUT US</h2>
                    <h3>IT'S ALL ABOUT CONNECTING CUSTOMERS AND SERVICES</h3>
                    <p>The concept of BusinessBid was introduced into the UAE after detailed and extensive research which highlighted the need of a comprehensive, efficient and a single source of connecting users to vendors within the UAE market. Simply put we are trying to provide a solution.</p>
                    <h3>FOR CUSTOMERS</h3>
                    <p>We understand how important it is to engage a reliable service provider who provides great service at a competitive price and instead of calling around for quotes and waiting for vendors to get back to you; we do the hard work for you.</p>
                    <p id=\"ul\">• Save time and money</p>
                    <p id=\"ul\">• Find qualified vendors and service providers in your area</p>
                    <p id=\"ul\">• Receive competitive quotes from your local professionals</p>
                    <h3>FOR BUSINESSES</h3>
                    <p>We also understand that sustaining a business can be challenging and growing a customer base can be even tougher. Therefore in addition of being customer centric we care about the businesses of our service providers.</p>
                    <p>We notify you when a customer requests a service you can provide and ensure you are receiving quality leads, even while you're on-the-go.</p>
                    <p id=\"ul\">• Promote your business online and get access to a large audience</p>
                    <p id=\"ul\">• Attract new customers who are seeking to engage service professionals</p>
                    <p id=\"ul\">• Get access to more jobs.</p>
                </div>
            </div>
        </div>
        <!-- Ends of about Us Area -->


        <!-- Start of about our team Area -->
        <div class=\"about_our_team\" id=\"team\">
            <div class=\"main_team_area\">
                <div class=\"inner_our_team_area_text\">
                    <h2>Meet the team behind Business Bid</h2>
                    <h1><span class=\"coma\">“</span><span class=\"main\">A successful team </span><span class=\"bold\">beats with one heart.</span><div class=\"rotet\">“</div></h1>
                    <p>From singers to painters to poker players— we at BusinessBid are a diverse and dynamic group of individuals. The things we have in common (apart from our vibrant personalities and great looks!) is a passion to use technology to build a platform that transforms the way people accomplish projects. We aim to build the Middle East’s largest services marketplace.</p>
                    <h3>Join our Team</h3>
                    <p>We make life easier for individuals and businesses across the UAE and relish every moment of it.</p>
                    <a href=\"mailto:careers@businessbid.ae?Subject=careers\" target=\"_top\" id=\"button_about_team\">Apply Now</a>
                </div>
                <div class=\"inner_our_team_area_img\">
                    <div class=\"inner_team_two_line\">
                        <div class=\"child_team\">
                            <img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/salman_image.jpg"), "html", null, true);
        echo "\" alt=\"salman image\">
                            <div class=\"tag\">
                                <h2>Salman</h2>
                                <p>Co-Founder & Director</p>
                            </div>
                        </div>
                        <div class=\"child_team\">
                            <img src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sam_image.jpg"), "html", null, true);
        echo "\" alt=\"Sam image\">
                            <div class=\"tag\">
                                <h2>Sam</h2>
                                <p>Co-Founder & Director</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"inner_team_three_line\">
                        <div class=\"child_team\">
                            <img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sharon_image.jpg"), "html", null, true);
        echo "\" alt=\"Sharon image\">
                            <div class=\"tag\">
                                <h2>Sharon</h2>
                                <p>Client Manager</p>
                            </div>
                        </div>
                        <div class=\"child_team\">
                            <img src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/anthony_image.jpg"), "html", null, true);
        echo "\" alt=\"Anthony image\">
                            <div class=\"tag\">
                                <h2>Anthony</h2>
                                <p>Client Manager</p>
                            </div>
                        </div>
                        <div class=\"child_team\">
                            <img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/rachella_image.jpg"), "html", null, true);
        echo "\" alt=\"Rachelle image\">
                            <div class=\"tag\">
                                <h2>Rachelle </h2>
                                <p>Client Services</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"inner_team_three_line\">
                        <div class=\"child_team\">
                            <img src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/greeth_image.jpg"), "html", null, true);
        echo "\" alt=\"Greeth image\">
                            <div class=\"tag\">
                                <h2>Greeth</h2>
                                <p>Operations Analyst</p>
                            </div>
                        </div>
                        <div class=\"child_team\">
                            <img src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/lucas_image.jpg"), "html", null, true);
        echo "\" alt=\"Lucas image\">
                            <div class=\"tag\">
                                <h2>Lucas</h2>
                                <p>Head Developer</p>
                            </div>
                        </div>
                        <div class=\"child_team\">
                            <img src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/camille_image.jpg"), "html", null, true);
        echo "\" alt=\"Camille image\">
                            <div class=\"tag\">
                                <h2>Camille</h2>
                                <p>Head Designer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Ends of about our team Area -->


        <!-- Start of about your oppourtunity Area -->
        <div class=\"your_opputunity_area\" id=\"oppourtinity\">
            <div class=\"inner_oppournity_area\">
                <div class=\"child_oppournity_img\">
                    <img src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/cv_image.png"), "html", null, true);
        echo "\" alt=\"cv image\">
                </div>
                <div class=\"child_oppournity_text\">
                    <h2>CAREER OPPORTUNITIES</h2>
                    <p>We are always on the look-out for committed and talented and people to join our buzzing team. The people who succeed with us are those who are passionate about their areas, have new ideas and the drive to make things happen!</p>
                    <h3>WHAT DO WE OFFER AS AN ORGANISATION?</h3>
                    <div class=\"child_area\">
                        <div class=\"child_left\">
                            <span>1</span>
                        </div>
                        <div class=\"child_right\">
                            <h4>WE PROVIDE A SOLUTION</h4>
                            <p>We strive our best to create a solution to an existing problem - create simplicity</p>
                        </div>
                    </div>
                    <div class=\"child_area\">
                        <div class=\"child_left\">
                            <span>2</span>
                        </div>
                        <div class=\"child_right\">
                            <h4>GROWTH & DEVELOPMENT</h4>
                            <p>We value our people and if you grow- we grow</p>
                        </div>
                    </div>
                    <div class=\"child_area\">
                        <div class=\"child_left\">
                            <span>3</span>
                        </div>
                        <div class=\"child_right\">
                            <h4>REWARD & RECOGNITION</h4>
                            <p>We know motivation is critical to aim higher</p>
                        </div>
                    </div>
                    <div class=\"child_area\">
                        <div class=\"child_left\">
                            <span>4</span>
                        </div>
                        <div class=\"child_right\">
                            <h4>ENVIRONMENT & CULTURE</h4>
                            <p>We understand creating an excellent atmosphere is essential for work performance</p>
                        </div>
                    </div>
                    <div class=\"child_area\">
                        <div class=\"child_left\">
                            <span>5</span>
                        </div>
                        <div class=\"child_right\">
                            <h4>WORK HARD & PLAY HARD</h4>
                            <p>We know it's not all about work and there is a balancing act to accommodate fun and work.</p>
                        </div>
                    </div>
                    <div class=\"child_area\">
                        <div class=\"child_left\">
                            <span>6</span>
                        </div>
                        <div class=\"child_right\">
                            <h4>CREATIVITY & IDEAS</h4>
                            <p>We listen. So if you have an idea or a suggestion which will help you and us, let us know.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Ends of about your oppourtunity Area -->


        <!-- Start of about valued partner Area -->
        <div class=\"our_valuable_partner\" id=\"partner\">
            <div class=\"inner_partnar_area\">
                <div class=\"inner_partner_text\">
                    <h2>VALUED PARTNERS</h2>
                    <h3>IF YOU ARE INTERESTED IN TEAMING UP WITH US AND BECOMING ONE OF OUR ESTEEMED PARTNERS</h3>
                    <h4>Please reach out to us on the details below</h4>
                    <a href=\"mailto:support@businessbid.ae\" target=\"_top\" id=\"button\">support@businessbid.ae</a>
                    <a href=\"#\" id=\"button\">(04) 42 13 777</a>
                </div>
                <div class=\"inner_partner_img\">
                    <img src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/business_artner.png"), "html", null, true);
        echo "\" alt=\"business Partner\">
                </div>
            </div>
        </div>
        <!-- Ends of about valued partner Area -->


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 199,  190 => 121,  170 => 104,  160 => 97,  150 => 90,  138 => 81,  128 => 74,  118 => 67,  106 => 58,  96 => 51,  56 => 14,  47 => 9,  44 => 8,  39 => 5,  36 => 4,  30 => 2,  11 => 1,);
    }
}

<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">


    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 162
        $this->displayBlock('customcss', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('javascripts', $context, $blocks);
        // line 309
        echo "
";
        // line 310
        $this->displayBlock('customjs', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('jquery', $context, $blocks);
        // line 317
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 323
        echo "</head>
";
        // line 324
        ob_start();
        // line 325
        echo "<body>

";
        // line 327
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 328
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 329
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 330
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 331
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 342
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 343
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 345
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 346
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 349
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 350
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 352
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 353
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 359
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 366
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 369
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 372
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 374
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 379
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 384
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 385
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 387
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 390
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 391
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 395
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 402
        $this->displayBlock('maincontent', $context, $blocks);
        // line 633
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 636
        $this->displayBlock('footer', $context, $blocks);
        // line 749
        echo "    <!-- footer block ends -->
</div>";
        // line 752
        $this->displayBlock('footerScript', $context, $blocks);
        // line 756
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 763
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_45acc86ecc36f60469ad874135b0a055841769cbba2c1c3aeacf44e3eeb02aa4 = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2c5a4f9f3d4433c4da0aa4885b18f61ab0508f1582c6b2c8432b6b6b6ab50782 = "http://www.businessbid.ae/contactus") && ('' === $__internal_2c5a4f9f3d4433c4da0aa4885b18f61ab0508f1582c6b2c8432b6b6b6ab50782 || 0 === strpos($__internal_45acc86ecc36f60469ad874135b0a055841769cbba2c1c3aeacf44e3eeb02aa4, $__internal_2c5a4f9f3d4433c4da0aa4885b18f61ab0508f1582c6b2c8432b6b6b6ab50782)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_288bcb2f20b27471af0dc5d280b87c81a46785ab342bf747d9f378c3887d9cff = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_693a768127801684788c6e3c779b79b3e4fc45fb7485eb2f4e500d18265f5841 = "http://www.businessbid.ae/node3") && ('' === $__internal_693a768127801684788c6e3c779b79b3e4fc45fb7485eb2f4e500d18265f5841 || 0 === strpos($__internal_288bcb2f20b27471af0dc5d280b87c81a46785ab342bf747d9f378c3887d9cff, $__internal_693a768127801684788c6e3c779b79b3e4fc45fb7485eb2f4e500d18265f5841)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_7737244fdd8d4e98d6f344e96e1b341641f15c213306f86cc54cf5c6652ecc8d = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_171c83ad9930438573df3a88ccf26bf84132a074324ecd00b13409d83793b834 = "http://www.businessbid.ae/review/login") && ('' === $__internal_171c83ad9930438573df3a88ccf26bf84132a074324ecd00b13409d83793b834 || 0 === strpos($__internal_7737244fdd8d4e98d6f344e96e1b341641f15c213306f86cc54cf5c6652ecc8d, $__internal_171c83ad9930438573df3a88ccf26bf84132a074324ecd00b13409d83793b834)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_bb70f27f3f261d9aed2fad8e6a9a9c52113819dece7f5973de88160159c6dfb5 = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a8967c308c29b15b2bbb00101579e097d51cd8413fc8bd5ea173eaf5b6037c3f = "http://www.businessbid.ae/node1") && ('' === $__internal_a8967c308c29b15b2bbb00101579e097d51cd8413fc8bd5ea173eaf5b6037c3f || 0 === strpos($__internal_bb70f27f3f261d9aed2fad8e6a9a9c52113819dece7f5973de88160159c6dfb5, $__internal_a8967c308c29b15b2bbb00101579e097d51cd8413fc8bd5ea173eaf5b6037c3f)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_cfd4de07dc3eae3d888d50ec77b5bc469af2cc8ab201805c474a8d9725c2e4d4 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_aceacc9cb9db7dbf83d6b087e205a5404198a64d9175db2526d0aa33d98d2809 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_aceacc9cb9db7dbf83d6b087e205a5404198a64d9175db2526d0aa33d98d2809 || 0 === strpos($__internal_cfd4de07dc3eae3d888d50ec77b5bc469af2cc8ab201805c474a8d9725c2e4d4, $__internal_aceacc9cb9db7dbf83d6b087e205a5404198a64d9175db2526d0aa33d98d2809)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_8a31bc20d49f20e535f2e48d59f575c150c33115654063ee67ba6a2d894492e4 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f710ac406a3c868236cd58b5032ad0554a4ca7fef65a7bc8d1f543a2bdb965c8 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_f710ac406a3c868236cd58b5032ad0554a4ca7fef65a7bc8d1f543a2bdb965c8 || 0 === strpos($__internal_8a31bc20d49f20e535f2e48d59f575c150c33115654063ee67ba6a2d894492e4, $__internal_f710ac406a3c868236cd58b5032ad0554a4ca7fef65a7bc8d1f543a2bdb965c8)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_91531cef55a6328ada1a56b1984e953fa2c7830741081190edffb21200ebda1e = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6b8f92482d845cfbca7e8b7e3b428e73ca6d9765002d914a50934aa9a4c04c8e = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_6b8f92482d845cfbca7e8b7e3b428e73ca6d9765002d914a50934aa9a4c04c8e || 0 === strpos($__internal_91531cef55a6328ada1a56b1984e953fa2c7830741081190edffb21200ebda1e, $__internal_6b8f92482d845cfbca7e8b7e3b428e73ca6d9765002d914a50934aa9a4c04c8e)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_2c2505c20aba98cf8304923467b478b533228964f23ce12e765e33794e776671 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5d82a02ca87e739f52f36f736d9d8320c297599e3a36e677f9183e94fcd2a90a = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_5d82a02ca87e739f52f36f736d9d8320c297599e3a36e677f9183e94fcd2a90a || 0 === strpos($__internal_2c2505c20aba98cf8304923467b478b533228964f23ce12e765e33794e776671, $__internal_5d82a02ca87e739f52f36f736d9d8320c297599e3a36e677f9183e94fcd2a90a)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_bfaf39c358d5dbe432d38b8889d82c0a71f8ee635b627805eb1be833c65d92ee = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5b6d7b790cd2f6bee2ec2dcb9863fb1e56a778d36c440ad484883a1c0fdddcc1 = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_5b6d7b790cd2f6bee2ec2dcb9863fb1e56a778d36c440ad484883a1c0fdddcc1 || 0 === strpos($__internal_bfaf39c358d5dbe432d38b8889d82c0a71f8ee635b627805eb1be833c65d92ee, $__internal_5b6d7b790cd2f6bee2ec2dcb9863fb1e56a778d36c440ad484883a1c0fdddcc1)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_a646874a4125d2d2f26602fe33fdea5b058ef7954abe9255ce04a5996d7a668a = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d3ef9019752a64770ad91a55a42989e9a93702b28c930c08eb297431ad6b1072 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_d3ef9019752a64770ad91a55a42989e9a93702b28c930c08eb297431ad6b1072 || 0 === strpos($__internal_a646874a4125d2d2f26602fe33fdea5b058ef7954abe9255ce04a5996d7a668a, $__internal_d3ef9019752a64770ad91a55a42989e9a93702b28c930c08eb297431ad6b1072)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_eb2fa962729021199686472bb39f159886a25a16a5e4a402cad3bd15ba116c71 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5a6dd3a8595a799db64a9419ceaaa13498d1ac03e23f8207766aa65d83ecc53f = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_5a6dd3a8595a799db64a9419ceaaa13498d1ac03e23f8207766aa65d83ecc53f || 0 === strpos($__internal_eb2fa962729021199686472bb39f159886a25a16a5e4a402cad3bd15ba116c71, $__internal_5a6dd3a8595a799db64a9419ceaaa13498d1ac03e23f8207766aa65d83ecc53f)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_ed2624647b4c292963616a11f7b99fab34b96b67142b9dca850ee315e4cc1aca = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_522519f399b9dc962d143b9f349cbb2670f5378e26db1678f27564524fe0037e = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_522519f399b9dc962d143b9f349cbb2670f5378e26db1678f27564524fe0037e || 0 === strpos($__internal_ed2624647b4c292963616a11f7b99fab34b96b67142b9dca850ee315e4cc1aca, $__internal_522519f399b9dc962d143b9f349cbb2670f5378e26db1678f27564524fe0037e)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_8ebf85e1006121c40c4cea5cf0046170e6ae907a189efb25aaf9479ab15d871a = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_56fe9564a2da440f67dc13cc1d43c28dc9c26db0e525fa95e0623080ecf21759 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_56fe9564a2da440f67dc13cc1d43c28dc9c26db0e525fa95e0623080ecf21759 || 0 === strpos($__internal_8ebf85e1006121c40c4cea5cf0046170e6ae907a189efb25aaf9479ab15d871a, $__internal_56fe9564a2da440f67dc13cc1d43c28dc9c26db0e525fa95e0623080ecf21759)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_208d849a46ea7d061aac8d3a3b676cfb5a41257642ba80d08c3232003838a3e7 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_29da79cc27464f3d31796165f8ada9d8d0c4366a52eeefae83c321514c8cc137 = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_29da79cc27464f3d31796165f8ada9d8d0c4366a52eeefae83c321514c8cc137 || 0 === strpos($__internal_208d849a46ea7d061aac8d3a3b676cfb5a41257642ba80d08c3232003838a3e7, $__internal_29da79cc27464f3d31796165f8ada9d8d0c4366a52eeefae83c321514c8cc137)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_a44b04d86e583ec7e079738048d9e7d5d97587d61d88f25d76889cf534394f02 = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f85f40406216d2a1764670e193d150b1d25edcf192e41b1fe6470a862b4d2d63 = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_f85f40406216d2a1764670e193d150b1d25edcf192e41b1fe6470a862b4d2d63 || 0 === strpos($__internal_a44b04d86e583ec7e079738048d9e7d5d97587d61d88f25d76889cf534394f02, $__internal_f85f40406216d2a1764670e193d150b1d25edcf192e41b1fe6470a862b4d2d63)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_2b48ec2833af43f978fee9a4cd905924f32d498e3329615eca2f3a14bc4afa23 = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0de4604aadbc4993699d4d143494f5f6d5f18963cae032b539c6f0f652647bfd = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_0de4604aadbc4993699d4d143494f5f6d5f18963cae032b539c6f0f652647bfd || 0 === strpos($__internal_2b48ec2833af43f978fee9a4cd905924f32d498e3329615eca2f3a14bc4afa23, $__internal_0de4604aadbc4993699d4d143494f5f6d5f18963cae032b539c6f0f652647bfd)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_497a8ae31147515d77187a39b7bcbd999a8edab60eb1184a2ee8474e32b1a54b = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8cce39d19801b7975fc1f954b353b9e315bb2206eb17f22daa7791a61e6a0b40 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_8cce39d19801b7975fc1f954b353b9e315bb2206eb17f22daa7791a61e6a0b40 || 0 === strpos($__internal_497a8ae31147515d77187a39b7bcbd999a8edab60eb1184a2ee8474e32b1a54b, $__internal_8cce39d19801b7975fc1f954b353b9e315bb2206eb17f22daa7791a61e6a0b40)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_c2972ad8a98eb21520b8dcfbc2774f4241618edbf3d0bb91d63ab1286d81067f = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_373aaffcff9e8cb8133795f4df889a45e5280378985ec2bcd922199d1ee6df56 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_373aaffcff9e8cb8133795f4df889a45e5280378985ec2bcd922199d1ee6df56 || 0 === strpos($__internal_c2972ad8a98eb21520b8dcfbc2774f4241618edbf3d0bb91d63ab1286d81067f, $__internal_373aaffcff9e8cb8133795f4df889a45e5280378985ec2bcd922199d1ee6df56)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_bb2874d87ee930fda7d2c0cf860855e8f9abc1b01e1a91fd5e336a8988960ec4 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ac300c8fb59cb9fac813805d3d3dc456729dea16939b04efe2399c5f2d8eeef3 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_ac300c8fb59cb9fac813805d3d3dc456729dea16939b04efe2399c5f2d8eeef3 || 0 === strpos($__internal_bb2874d87ee930fda7d2c0cf860855e8f9abc1b01e1a91fd5e336a8988960ec4, $__internal_ac300c8fb59cb9fac813805d3d3dc456729dea16939b04efe2399c5f2d8eeef3)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_1746443f5733f19bba24390ed65b1b850af7e7832eb3de6839a8b7eee83e50e1 = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_480a94316f2c1f0a70937a1052e33f028bf51d1f2f67b2183e0e8a7ffea4be05 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_480a94316f2c1f0a70937a1052e33f028bf51d1f2f67b2183e0e8a7ffea4be05 || 0 === strpos($__internal_1746443f5733f19bba24390ed65b1b850af7e7832eb3de6839a8b7eee83e50e1, $__internal_480a94316f2c1f0a70937a1052e33f028bf51d1f2f67b2183e0e8a7ffea4be05)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_53fcfe36ad8207fe75ab7b9fcaeed9ec4d407ed87a5f7e447d9618554f97ac9c = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_362e705ca50a280897a0e28202c3e6aebc9a39a0a38d6d39fb584b706bd811e2 = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_362e705ca50a280897a0e28202c3e6aebc9a39a0a38d6d39fb584b706bd811e2 || 0 === strpos($__internal_53fcfe36ad8207fe75ab7b9fcaeed9ec4d407ed87a5f7e447d9618554f97ac9c, $__internal_362e705ca50a280897a0e28202c3e6aebc9a39a0a38d6d39fb584b706bd811e2)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_a1f82a2842483528d75dcc095932813b0a0eed4131f3ceb733f05e6336322253 = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_69156d20ebc0e8b21a0882f4a7e515beab079b68e1e6ff8828c9e1df2f9c6718 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_69156d20ebc0e8b21a0882f4a7e515beab079b68e1e6ff8828c9e1df2f9c6718 || 0 === strpos($__internal_a1f82a2842483528d75dcc095932813b0a0eed4131f3ceb733f05e6336322253, $__internal_69156d20ebc0e8b21a0882f4a7e515beab079b68e1e6ff8828c9e1df2f9c6718)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_c837ea7d01c8cc105313da1ae2da83e51d650c239470ba3c4d16f36fc93ba1ac = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f6f61659c1a7d8fd196c514dccef8785697ec173a1f871fa2b28a519662aede9 = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_f6f61659c1a7d8fd196c514dccef8785697ec173a1f871fa2b28a519662aede9 || 0 === strpos($__internal_c837ea7d01c8cc105313da1ae2da83e51d650c239470ba3c4d16f36fc93ba1ac, $__internal_f6f61659c1a7d8fd196c514dccef8785697ec173a1f871fa2b28a519662aede9)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_3b72d36355b92314e28a3e5456ccb3c38bb5fa762f65711f124e1418ab1a1bad = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_81698216c08bf88c21d9b49366d3a99902eeae37d162d39a79ee542beef098b3 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_81698216c08bf88c21d9b49366d3a99902eeae37d162d39a79ee542beef098b3 || 0 === strpos($__internal_3b72d36355b92314e28a3e5456ccb3c38bb5fa762f65711f124e1418ab1a1bad, $__internal_81698216c08bf88c21d9b49366d3a99902eeae37d162d39a79ee542beef098b3)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_97bf0a6b6bc1e67233e2186556306f16f314189d565cf2cbbdbb3a4796366884 = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_82e2b8f9d9bac4ed08cc2381efc9f630646d420ed2bfc193b7405938256a4975 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_82e2b8f9d9bac4ed08cc2381efc9f630646d420ed2bfc193b7405938256a4975 || 0 === strpos($__internal_97bf0a6b6bc1e67233e2186556306f16f314189d565cf2cbbdbb3a4796366884, $__internal_82e2b8f9d9bac4ed08cc2381efc9f630646d420ed2bfc193b7405938256a4975)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_4af64c31e70ad90cd59a0971820083ff0a80b141d817953866187c4cb8e6b622 = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5a9f59425eb3a94b14b027dd6ba75fb50ec31732c9487d3d8bf4564d22f12b87 = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_5a9f59425eb3a94b14b027dd6ba75fb50ec31732c9487d3d8bf4564d22f12b87 || 0 === strpos($__internal_4af64c31e70ad90cd59a0971820083ff0a80b141d817953866187c4cb8e6b622, $__internal_5a9f59425eb3a94b14b027dd6ba75fb50ec31732c9487d3d8bf4564d22f12b87)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_78fdbadce10af0480ce55937931a19f76db83253c70a27200df2c01ca0aef48f = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6d48fa812fcaf7ac9e7d1fbd3ac8d2eb1a38f89098314cceab219c94e4e0f12a = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_6d48fa812fcaf7ac9e7d1fbd3ac8d2eb1a38f89098314cceab219c94e4e0f12a || 0 === strpos($__internal_78fdbadce10af0480ce55937931a19f76db83253c70a27200df2c01ca0aef48f, $__internal_6d48fa812fcaf7ac9e7d1fbd3ac8d2eb1a38f89098314cceab219c94e4e0f12a)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_088aeffe6c2dcd4a1bd15a1c7a198a6531ed216625c18351b434a2a46ef7837b = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_4fe4dd44533e51edf2871753d80a1452141d1da55967bb01eb14ebb8309af22d = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_4fe4dd44533e51edf2871753d80a1452141d1da55967bb01eb14ebb8309af22d || 0 === strpos($__internal_088aeffe6c2dcd4a1bd15a1c7a198a6531ed216625c18351b434a2a46ef7837b, $__internal_4fe4dd44533e51edf2871753d80a1452141d1da55967bb01eb14ebb8309af22d)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_fdd4b224008cca6bc7ed7769271e3461ea8835251e5bda165488a6283d117db8 = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_79c517cc756bc4bdbdf1951cb5cee01a90dd03f294dc2acd6346a6e1a7d897c3 = "http://www.businessbid.ae/node2") && ('' === $__internal_79c517cc756bc4bdbdf1951cb5cee01a90dd03f294dc2acd6346a6e1a7d897c3 || 0 === strpos($__internal_fdd4b224008cca6bc7ed7769271e3461ea8835251e5bda165488a6283d117db8, $__internal_79c517cc756bc4bdbdf1951cb5cee01a90dd03f294dc2acd6346a6e1a7d897c3)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_ae88577c2c64551fcd7b829c6aba78d24dbb285796110db21b0122cd07644f4e = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6160496a05c98a630c2e1182cd4206199df2cc507537264a19f3af2d0d5a5638 = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_6160496a05c98a630c2e1182cd4206199df2cc507537264a19f3af2d0d5a5638 || 0 === strpos($__internal_ae88577c2c64551fcd7b829c6aba78d24dbb285796110db21b0122cd07644f4e, $__internal_6160496a05c98a630c2e1182cd4206199df2cc507537264a19f3af2d0d5a5638)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_5153cb7b4910aaaf208703d919f6c43f35d2e4ac37fa656148bcde5094c924d7 = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b5f8181c4021ef6ccfbcc2bdcf44f828a8365dfe1af3f7bef678ed65dfc7b526 = "http://www.businessbid.ae/faq") && ('' === $__internal_b5f8181c4021ef6ccfbcc2bdcf44f828a8365dfe1af3f7bef678ed65dfc7b526 || 0 === strpos($__internal_5153cb7b4910aaaf208703d919f6c43f35d2e4ac37fa656148bcde5094c924d7, $__internal_b5f8181c4021ef6ccfbcc2bdcf44f828a8365dfe1af3f7bef678ed65dfc7b526)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_8ab0ddfba043a31d27878c35108433d98f26e44e83f863b9414f6ed1484deb23 = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_625437e2e6222106aa872a51d646b9f9ad9d92cee67f1abf298eddc2969fd6d6 = "http://www.businessbid.ae/node4") && ('' === $__internal_625437e2e6222106aa872a51d646b9f9ad9d92cee67f1abf298eddc2969fd6d6 || 0 === strpos($__internal_8ab0ddfba043a31d27878c35108433d98f26e44e83f863b9414f6ed1484deb23, $__internal_625437e2e6222106aa872a51d646b9f9ad9d92cee67f1abf298eddc2969fd6d6)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_a573e71ef9a042002223271dcacdd837cf35853d472b7d2942f9218a4d96fb25 = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_f05e63e11de978590dcb0eacbd9becda66822a35cfbaec1e7e6bd7f6749c4a1a = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_f05e63e11de978590dcb0eacbd9becda66822a35cfbaec1e7e6bd7f6749c4a1a || 0 === strpos($__internal_a573e71ef9a042002223271dcacdd837cf35853d472b7d2942f9218a4d96fb25, $__internal_f05e63e11de978590dcb0eacbd9becda66822a35cfbaec1e7e6bd7f6749c4a1a)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_5c989017092332f90fc08df5543e0a180de563465ab0c9981e06ed797d93b9b5 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a18099756c00ac32b67bf3481b64efa8237130e680c5cca2c51fbb82fe5163cf = "http://www.businessbid.ae/node6") && ('' === $__internal_a18099756c00ac32b67bf3481b64efa8237130e680c5cca2c51fbb82fe5163cf || 0 === strpos($__internal_5c989017092332f90fc08df5543e0a180de563465ab0c9981e06ed797d93b9b5, $__internal_a18099756c00ac32b67bf3481b64efa8237130e680c5cca2c51fbb82fe5163cf)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_b3f58a2ed629328c456d75baf9928271022f855cfa052150c3dfaa4419c05e05 = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ed8e03c39d029605ae00e78a7a9d21a49cbfe4bd27e5afe2d4907f5f5fa4dae7 = "http://www.businessbid.ae/feedback") && ('' === $__internal_ed8e03c39d029605ae00e78a7a9d21a49cbfe4bd27e5afe2d4907f5f5fa4dae7 || 0 === strpos($__internal_b3f58a2ed629328c456d75baf9928271022f855cfa052150c3dfaa4419c05e05, $__internal_ed8e03c39d029605ae00e78a7a9d21a49cbfe4bd27e5afe2d4907f5f5fa4dae7)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_5f7b6644337ed797baaba0217d8ca0e01d9c603b535d6e5cc152118d2d39be27 = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_4e11a5fa1ac9916381eb70e9bbccc6fac5c960cc6b864b6d913fa92347876ec8 = "http://www.businessbid.ae/sitemap") && ('' === $__internal_4e11a5fa1ac9916381eb70e9bbccc6fac5c960cc6b864b6d913fa92347876ec8 || 0 === strpos($__internal_5f7b6644337ed797baaba0217d8ca0e01d9c603b535d6e5cc152118d2d39be27, $__internal_4e11a5fa1ac9916381eb70e9bbccc6fac5c960cc6b864b6d913fa92347876ec8)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_48990eb8df704d987da9507108107bea7d833b3257aeda1fc41c55237f9f327e = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0f0aa7091434568df2932bbb36f6833a99f837ca365de97b9522bfee123feb2a = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_0f0aa7091434568df2932bbb36f6833a99f837ca365de97b9522bfee123feb2a || 0 === strpos($__internal_48990eb8df704d987da9507108107bea7d833b3257aeda1fc41c55237f9f327e, $__internal_0f0aa7091434568df2932bbb36f6833a99f837ca365de97b9522bfee123feb2a)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_8fe2f0aa51fa127c3aee36f37bdeed16d6a4c1afb08127707cce886604043025 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_26e5eea877dcb331bbc751b3244bced183ef166f8bab94743bc710828126e9a6 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_26e5eea877dcb331bbc751b3244bced183ef166f8bab94743bc710828126e9a6 || 0 === strpos($__internal_8fe2f0aa51fa127c3aee36f37bdeed16d6a4c1afb08127707cce886604043025, $__internal_26e5eea877dcb331bbc751b3244bced183ef166f8bab94743bc710828126e9a6)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_4a049df1b69a36efddb7db31023f3f958fe3379c961c1042cefaed0e017d8bab = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f0a07863907679c5e56d8923189f8b70aa0318032042687cee945492a539e581 = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_f0a07863907679c5e56d8923189f8b70aa0318032042687cee945492a539e581 || 0 === strpos($__internal_4a049df1b69a36efddb7db31023f3f958fe3379c961c1042cefaed0e017d8bab, $__internal_f0a07863907679c5e56d8923189f8b70aa0318032042687cee945492a539e581)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_6993fc337c3ab50c17275f72eef9086bb9cc8cf8381bc7a5c245931e2738174d = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c32b880eb7e5258dbb218f294f4d6dde2c8cb225b936632cfb1862450c9176d7 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_c32b880eb7e5258dbb218f294f4d6dde2c8cb225b936632cfb1862450c9176d7 || 0 === strpos($__internal_6993fc337c3ab50c17275f72eef9086bb9cc8cf8381bc7a5c245931e2738174d, $__internal_c32b880eb7e5258dbb218f294f4d6dde2c8cb225b936632cfb1862450c9176d7)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 162
    public function block_customcss($context, array $blocks = array())
    {
        // line 163
        echo "
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "<script>
\$(document).ready(function(){
    ";
        // line 169
        if (array_key_exists("keyword", $context)) {
            // line 170
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 174
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 310
    public function block_customjs($context, array $blocks = array())
    {
        // line 311
        echo "
";
    }

    // line 314
    public function block_jquery($context, array $blocks = array())
    {
        // line 315
        echo "
";
    }

    // line 317
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 318
        echo "
<!-- Google Analytics -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>

";
    }

    // line 402
    public function block_maincontent($context, array $blocks = array())
    {
        // line 403
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 490
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 530
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 560
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 561
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 563
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 565
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 571
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 572
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 574
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 576
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 581
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 582
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 584
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 586
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 593
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 594
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 596
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 598
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 604
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 605
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 607
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 609
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 615
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 616
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 618
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 620
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 490
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 491
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 500
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 501
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 502
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 503
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 504
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 505
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 506
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 508
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 509
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 510
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 512
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 519
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 525
        echo "    </div>
            </div>


    ";
    }

    // line 519
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 520
        echo "    <div class=\"request\">
    <h2>";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 636
    public function block_footer($context, array $blocks = array())
    {
        // line 637
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 666
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 687
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 688
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 740
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 742
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 752
    public function block_footerScript($context, array $blocks = array())
    {
        // line 753
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 754
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1411 => 754,  1406 => 753,  1403 => 752,  1392 => 742,  1387 => 740,  1347 => 703,  1339 => 698,  1335 => 697,  1331 => 696,  1320 => 688,  1317 => 687,  1313 => 685,  1304 => 679,  1300 => 678,  1296 => 677,  1292 => 676,  1288 => 675,  1277 => 667,  1273 => 666,  1264 => 660,  1260 => 659,  1256 => 658,  1252 => 657,  1248 => 656,  1227 => 637,  1224 => 636,  1216 => 521,  1213 => 520,  1210 => 519,  1202 => 525,  1200 => 519,  1193 => 514,  1186 => 512,  1170 => 510,  1167 => 509,  1164 => 508,  1150 => 506,  1147 => 505,  1144 => 504,  1142 => 503,  1138 => 502,  1135 => 501,  1131 => 500,  1121 => 493,  1117 => 491,  1114 => 490,  1098 => 620,  1092 => 618,  1086 => 616,  1084 => 615,  1076 => 609,  1070 => 607,  1064 => 605,  1062 => 604,  1054 => 598,  1048 => 596,  1042 => 594,  1040 => 593,  1031 => 586,  1025 => 584,  1019 => 582,  1017 => 581,  1010 => 576,  1004 => 574,  998 => 572,  996 => 571,  988 => 565,  982 => 563,  976 => 561,  974 => 560,  942 => 530,  940 => 490,  925 => 478,  914 => 470,  903 => 462,  892 => 454,  881 => 446,  871 => 439,  853 => 424,  843 => 417,  833 => 410,  824 => 403,  821 => 402,  813 => 318,  810 => 317,  805 => 315,  802 => 314,  797 => 311,  794 => 310,  775 => 294,  653 => 174,  647 => 170,  645 => 169,  641 => 167,  638 => 166,  633 => 163,  630 => 162,  626 => 127,  623 => 126,  617 => 109,  613 => 107,  611 => 106,  608 => 105,  606 => 104,  603 => 103,  601 => 102,  598 => 101,  596 => 100,  593 => 99,  591 => 98,  588 => 97,  586 => 96,  583 => 95,  581 => 94,  578 => 93,  576 => 92,  573 => 91,  571 => 90,  568 => 89,  566 => 88,  563 => 87,  561 => 86,  558 => 85,  556 => 84,  553 => 83,  551 => 82,  548 => 81,  546 => 80,  543 => 79,  541 => 78,  538 => 77,  536 => 76,  533 => 75,  531 => 74,  528 => 73,  526 => 72,  523 => 71,  521 => 70,  518 => 69,  516 => 68,  513 => 67,  511 => 66,  508 => 65,  506 => 64,  503 => 63,  501 => 62,  498 => 61,  496 => 60,  493 => 59,  491 => 58,  488 => 57,  486 => 56,  483 => 55,  481 => 54,  478 => 53,  476 => 52,  473 => 51,  471 => 50,  468 => 49,  466 => 48,  463 => 47,  461 => 46,  458 => 45,  456 => 44,  453 => 43,  451 => 42,  448 => 41,  446 => 40,  443 => 39,  441 => 38,  438 => 37,  436 => 36,  433 => 35,  431 => 34,  428 => 33,  426 => 32,  423 => 31,  421 => 30,  418 => 29,  416 => 28,  413 => 27,  411 => 26,  408 => 25,  406 => 24,  403 => 23,  401 => 22,  398 => 21,  396 => 20,  393 => 19,  391 => 18,  388 => 17,  386 => 16,  383 => 15,  381 => 14,  378 => 13,  376 => 12,  373 => 11,  368 => 763,  359 => 756,  357 => 752,  354 => 749,  352 => 636,  347 => 633,  345 => 402,  336 => 395,  322 => 392,  319 => 391,  317 => 390,  311 => 387,  307 => 385,  305 => 384,  297 => 379,  289 => 374,  284 => 372,  278 => 369,  273 => 367,  269 => 366,  260 => 359,  244 => 354,  240 => 353,  236 => 352,  232 => 350,  230 => 349,  224 => 346,  218 => 345,  214 => 343,  212 => 342,  201 => 336,  194 => 331,  192 => 330,  190 => 329,  188 => 328,  186 => 327,  182 => 325,  180 => 324,  177 => 323,  175 => 317,  173 => 314,  170 => 313,  168 => 310,  165 => 309,  163 => 166,  160 => 165,  158 => 162,  153 => 160,  149 => 159,  140 => 153,  135 => 151,  131 => 150,  126 => 148,  122 => 147,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

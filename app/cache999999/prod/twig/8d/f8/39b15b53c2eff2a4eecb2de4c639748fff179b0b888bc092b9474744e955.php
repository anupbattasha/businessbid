<?php

/* BBidsBBidsHomeBundle:Home:menu.html.twig */
class __TwigTemplate_8df839b15b53c2eff2a4eecb2de4c639748fff179b0b888bc092b9474744e955 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["typeOfMenu"]) ? $context["typeOfMenu"] : null) == "vendorSearch")) {
            // line 2
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 3
                echo "        ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 4
                    echo "            <li><a class=\"all-category\" href=\"";
                    echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
                    echo "\">See All Categories</a></li>
        ";
                } else {
                    // line 6
                    echo "        <li><a class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "catClass", array()), "html", null, true);
                    echo "\" href=\"";
                    if (((isset($context["loginFlag"]) ? $context["loginFlag"] : null) == true)) {
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
                    } else {
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
                    }
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
                    echo " Quotes</a></li>
        ";
                }
                // line 8
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } elseif ((        // line 9
(isset($context["typeOfMenu"]) ? $context["typeOfMenu"] : null) == "vedorReview")) {
            // line 10
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 11
                echo "        ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 12
                    echo "            <li><a class=\"all-reviews\" href=\"";
                    echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
                    echo "\">See All Reviews</a></li>
        ";
                } else {
                    // line 14
                    echo "        <li><a class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "catClass", array()), "html", null, true);
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
                    echo " Reviews</a></li>
        ";
                }
                // line 16
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } elseif ((        // line 17
(isset($context["typeOfMenu"]) ? $context["typeOfMenu"] : null) == "vendorResource")) {
            // line 18
            echo "
    ";
            // line 19
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 20
                echo "        ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 21
                    echo "            <li><a class=\"all-articles\" href=\"http://www.businessbid.ae/resource_centre/home\">See All Articles</a></li>
        ";
                } else {
                    // line 23
                    echo "        <li><a class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "catClass", array()), "html", null, true);
                    echo "\" href=\"http://www.businessbid.ae/resource_centre/";
                    if (twig_test_empty($this->getAttribute($context["category"], "resourceLink", array()))) {
                        echo "home-2";
                    } else {
                        echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "resourceLink", array()), "html", null, true);
                    }
                    echo "\">";
                    echo twig_escape_filter($this->env, trim($this->getAttribute($context["category"], "category", array())), "html", null, true);
                    echo " Articles</a></li>
        ";
                }
                // line 25
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 25,  158 => 23,  154 => 21,  151 => 20,  134 => 19,  131 => 18,  129 => 17,  115 => 16,  105 => 14,  99 => 12,  96 => 11,  78 => 10,  76 => 9,  62 => 8,  48 => 6,  42 => 4,  39 => 3,  21 => 2,  19 => 1,);
    }
}

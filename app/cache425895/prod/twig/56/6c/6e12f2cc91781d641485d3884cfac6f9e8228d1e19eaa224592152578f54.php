<?php

/* BBidsBBidsHomeBundle:User:purchase_leads.html.twig */
class __TwigTemplate_566c6e12f2cc91781d641485d3884cfac6f9e8228d1e19eaa224592152578f54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:purchase_leads.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">

";
        // line 7
        echo "<div>
    ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "
    <div class=\"alert alert-danger\">";
            // line 10
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "
    ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "
    <div class=\"alert alert-success\">";
            // line 16
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "
    <div class=\"alert alert-success\">";
            // line 22
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "</div>

<div class=\"service-category-select col-sm-12 side-clear\">
\t<h2>Select Your Service Category</h2>
\t<div class=\"filter-leadpack\">
\t<div class=\"col-sm-6 side-clear service-category-type\">
\t\t<div class=\"col-sm-9 side-clear\">
\t\t\t<input type=\"search\" name=\"parentCat\" id=\"parentCat\" placeholder=\"Type service category, e.g - Accountant, Catering\" title=\"Start typing a service category name\" />
\t\t</div>
\t\t<div class=\"col-sm-3\">
\t\t\t<input class=\"submit-pack\" type=\"submit\" value=\"Add\" id=\"autoCompleteAdd\" />
\t\t</div>
\t</div>
\t<div class=\"col-sm-1\"><span class=\"ORval\">OR</span></div>
\t<div class=\"col-sm-5 side-clear service-category-option\">
\t\t<div class=\"col-sm-9 side-clear\">
\t\t\t<select name=\"selectBoxParentCat\" id=\"selectBoxParentCat\">
\t\t\t\t<option selected=\"selected\">Choose Category from List</option>
\t\t\t\t";
        // line 43
        echo (isset($context["categoryOptions"]) ? $context["categoryOptions"] : null);
        echo "
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-sm-3 clear-right\">
\t\t\t<input class=\"submit-pack\" type=\"submit\" value=\"Add\" id=\"selectBoxAdd\" />
\t\t</div>
\t</div>
\t</div>
\t<div class=\"service-category-select col-sm-12\">
\t\t<table width=\"660\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" id=\"selectedCatTable\">
\t\t\t<thead>
\t\t\t  <tr>
\t\t\t\t<td width=\"216\">Service Category </td>
\t\t\t\t<td width=\"197\">Price Per Lead (AED)</td>
\t\t\t\t<td width=\"189\">Action</td>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t  <tr id=\"nocat\"><td colspan=\"3\">No Service Category Selected.</td></tr>
\t\t\t</tbody>
\t\t</table>
\t</div>
\t<div class=\"row text-right update-account\">
\t\t\t<a href=\"#\" class=\"btn btn-success\"> Proceed to View Packages </a>
\t</div>
</div>
</div>
<script type=\"text/javascript\">
\tvar availableCat     = ";
        // line 71
        echo twig_jsonencode_filter((isset($context["catArray"]) ? $context["catArray"] : null));
        echo ";
\tvar availableNumbers = ";
        // line 72
        echo twig_jsonencode_filter((isset($context["catPrice"]) ? $context["catPrice"] : null));
        echo ";
\tvar purchaseID       = '";
        // line 73
        echo twig_escape_filter($this->env, (isset($context["purchaseID"]) ? $context["purchaseID"] : null), "html", null, true);
        echo "';
\tvar redeemUrl        = \"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_store_purchase_leads");
        echo "\";
\tvar planUrl          = \"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_lead_plan", array("purchaseID" => (isset($context["purchaseID"]) ? $context["purchaseID"] : null))), "html", null, true);
        echo "\";
\tvar selectedCats     = new Array();
</script>
<script type=\"text/javascript\" src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/purchase.leads.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:purchase_leads.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 78,  159 => 75,  155 => 74,  151 => 73,  147 => 72,  143 => 71,  112 => 43,  92 => 25,  83 => 22,  80 => 21,  76 => 20,  73 => 19,  64 => 16,  61 => 15,  57 => 14,  54 => 13,  45 => 10,  42 => 9,  38 => 8,  35 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:vendorenquiries.html.twig */
class __TwigTemplate_9941ee94636812bfd7b860c6a26851035701bea63f569d37796437e9f9084918 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:vendorenquiries.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">

 function confirmEnq(enqcat,enqid) {

 \tvar con = confirm(1 +\" lead will be deducted upon accepting this enquiry. Are you sure to Accept this enquiry? This action cannot be reverted!\");
 \tif(con) {
\t\t";
        // line 11
        echo "\t\t\$.ajax({
\t\t\turl: \"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_checkvendor_leads_available");
        echo "\",
\t\t\tmethod: \"POST\",
\t\t\tdata:{'enqcat':enqcat,'enqid':enqid},
\t\t\tsuccess: function(result){
\t\t\t\t";
        // line 17
        echo "\t\t\t\tvar obj = jQuery.parseJSON( result ) ;
\t\t\t\tif(obj.status == 'Successful') {
\t\t\t\t\tif(obj.leadCount != 0) {
\t\t\t\t\t\tvar acceptanceUrl = \"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_accept_enquiry", array("category" => "enqcat", "enquiryid" => "enqid")), "html", null, true);
        echo "\";
\t\t\t\t\t\tvar urlset = acceptanceUrl.replace('enqcat', enqcat);
\t\t\t\t\t\tvar urlset = urlset.replace('enqid', enqid);
\t\t\t\t\t\twindow.open(urlset,\"_self\");
\t\t\t\t\t\treturn true;
\t\t\t\t\t} else {
\t\t\t\t\t\talert('You do not have sufficient lead credit to accept this enquiry. Please purcace more leads');
\t\t\t\t\t\treturn false;
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\talert(obj.status);
\t\t\t\t\treturn false;
\t\t\t\t}
    \t}});
\t}
\telse {
\t\talert(\"Action canceled to accept the requested enquiry\");
\t\treturn false;
\t}
 }
";
        // line 65
        echo "</script>
<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\">

<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
<script>
\$(function() {
\t\$(\"#datepicker3\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true,
\t\tmaxDate: '+2y',
\t\tyearRange: '2014:2024',
\t\tonSelect: function(date){
\t\t\tvar selectedDate = new Date(date);
\t\t\tvar msecsInADay = 86400000;
\t\t\tvar endDate = new Date(selectedDate.getTime() + msecsInADay);

\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"minDate\", endDate );
\t\t\t\$(\"#datepicker4\").datepicker( \"option\", \"maxDate\", '+2y' );
\t\t}
\t});

\t\$(\"#datepicker4\").datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true
\t});


});
</script>
<div class=\"col-md-9 dashboard-rightpanel\">
<div class=\"page-title\"><h1>My Job Requests</h1></div>

<div>
\t";
        // line 98
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 99
            echo "
\t<div class=\"alert alert-danger\">";
            // line 100
            echo $context["flashMessage"];
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "
\t";
        // line 104
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 105
            echo "
\t<div class=\"alert alert-success\">";
            // line 106
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 109
        echo "
\t";
        // line 110
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 111
            echo "
\t<div class=\"alert alert-success\">";
            // line 112
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "</div>
<div class=\"admin_filter\">
\t";
        // line 117
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t<div class=\"col-md-3 clear-right\"> ";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
\t\t<div class=\"col-md-3 clear-right\"> ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
        echo "</div>
\t\t<div class=\"col-md-2 clear-right\"> ";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fromdate", array()), 'widget', array("id" => "datepicker3"));
        echo "</div>
\t    <div class=\"col-md-2 clear-right\"> ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "todate", array()), 'widget', array("id" => "datepicker4"));
        echo " </div>
\t\t<div class=\"col-md-2 top-space\"> ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "filter", array()), 'widget', array("label" => "Search"));
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo " </div>
</div>
<div class=\"table-wrapper\">
<table class=\"vendor-enquiries\">
<thead><tr>
<th>Date Recieved</th>
<th>Job Request Number</th>
<th>Category</th><th>Subject</th><th>City</th><th>Job Status</th><th>Description</th><th>Actions</th></tr></thead>
       ";
        // line 130
        if ( !twig_test_empty((isset($context["enquiries"]) ? $context["enquiries"] : null))) {
            // line 131
            echo "
\t";
            // line 132
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["enquiry"]) {
                // line 133
                echo "\t<tr>
\t\t\t<td>";
                // line 134
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo " </td>
\t\t\t<td><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"myModalEnq\" data-enq=\"";
                // line 135
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "id", array()), "html", null, true);
                echo "</a></td>
\t\t\t<td>";
                // line 136
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "category", array()), "html", null, true);
                echo " </td>
\t\t\t<td>";
                // line 137
                if (($this->getAttribute($context["enquiry"], "acceptstatus", array()) == 1)) {
                    echo " <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_enquiry_view", array("enquiryid" => $this->getAttribute($context["enquiry"], "id", array()))), "html", null, true);
                    echo "\">";
                    if ((twig_length_filter($this->env, $this->getAttribute($context["enquiry"], "subj", array())) > 20)) {
                        echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enquiry"], "subj", array()), 0, 20), "html", null, true);
                        echo "... ";
                    } else {
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "subj", array()), "html", null, true);
                        echo " ";
                    }
                    echo "</a>";
                } else {
                    echo " ";
                    if ((twig_length_filter($this->env, $this->getAttribute($context["enquiry"], "subj", array())) > 20)) {
                        echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enquiry"], "subj", array()), 0, 20), "html", null, true);
                        echo "... ";
                    } else {
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "subj", array()), "html", null, true);
                        echo " ";
                    }
                }
                echo "</td>
\t\t\t<td>";
                // line 138
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "city", array()), "html", null, true);
                echo " </td>
\t\t\t<td>";
                // line 139
                if (($this->getAttribute($context["enquiry"], "acceptstatus", array()) == 1)) {
                    echo " Accepted  ";
                } else {
                    echo " Not Accept</a> ";
                }
                echo " </td>
\t</td>
\t\t\t<td> ";
                // line 141
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "description", array()), "html", null, true);
                echo " </td>
\t\t\t<td>";
                // line 142
                if (($this->getAttribute($context["enquiry"], "acceptstatus", array()) == 1)) {
                    echo " Accepted | <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_view_consumer", array("id" => $this->getAttribute($context["enquiry"], "authorid", array()), "eid" => $this->getAttribute($context["enquiry"], "id", array()))), "html", null, true);
                    echo "\" > View Customer Information</a> ";
                } else {
                    echo " <a href=\"#\" onclick=\"confirmEnq('";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "category", array()), "html", null, true);
                    echo "','";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "id", array()), "html", null, true);
                    echo "')\";>Accept Lead</a> ";
                }
                echo " </td>
\t</tr>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquiry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 145
            echo "\t\t</table>
</div>
";
            // line 147
            if ( !twig_test_empty((isset($context["count"]) ? $context["count"] : null))) {
                // line 148
                echo "<div class=\"row text-right\">
<div class=\"row text-right page-counter\"><div class=\"counter-box\"><span class=\"now-show\">Now showing : ";
                // line 149
                echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
                echo " to ";
                echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
                echo " of ";
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo " records</span></div></div>
\t\t<div class=\"counter-box\">";
                // line 150
                $context["Epage_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 4)));
                // line 151
                echo "\t\t<ul class=\"pagination\">
\t\t  <li><a href=\"";
                // line 152
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_enquiries", array("offset" => 1));
                echo "\">&laquo;</a></li>
\t\t";
                // line 153
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 154
                    echo "
\t\t  <li><a href=\"";
                    // line 155
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_enquiries", array("offset" => $context["i"])), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "</a></li>

\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 158
                echo "
\t\t  <li><a href=\"";
                // line 159
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_enquiries", array("offset" =>  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null))), "html", null, true);
                echo "\">&raquo;</a></li>
\t\t</ul>
\t\t</div>

\t</div>
\t";
            }
            // line 165
            echo "\t";
        } else {
            // line 166
            echo "\t<tr><td colspan=\"2\" align=\"center\">No Enquiries found</td></tr>
\t";
        }
        // line 168
        echo "\t</table>
</div>

<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Additional Information</h4>
      </div>
      <div class=\"modal-body\">
      </div>
    </div>
  </div>
</div>

</div>
<script>
\t\$(document).ready(function(){\$(\".myModalEnq\").on(\"click\",function(){\$(\".modal-body\").html(\"Loading Data..... Please wait.\");var a=\$(this).attr(\"data-enq\"),t=\"";
        // line 187
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_form_extra_fields", array("id" => "enq"));
        echo "\",d=t.replace(\"enq\",a);\$.ajax({url:d,success:function(a){\$(\".modal-body\").html(a)}})})});
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:vendorenquiries.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  383 => 187,  362 => 168,  358 => 166,  355 => 165,  346 => 159,  343 => 158,  332 => 155,  329 => 154,  325 => 153,  321 => 152,  318 => 151,  316 => 150,  308 => 149,  305 => 148,  303 => 147,  299 => 145,  280 => 142,  276 => 141,  267 => 139,  263 => 138,  236 => 137,  232 => 136,  226 => 135,  222 => 134,  219 => 133,  215 => 132,  212 => 131,  210 => 130,  198 => 122,  192 => 121,  186 => 120,  180 => 119,  174 => 118,  170 => 117,  166 => 115,  157 => 112,  154 => 111,  150 => 110,  147 => 109,  138 => 106,  135 => 105,  131 => 104,  128 => 103,  119 => 100,  116 => 99,  112 => 98,  77 => 65,  54 => 20,  49 => 17,  42 => 12,  39 => 11,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:leadhistory.html.twig */
class __TwigTemplate_31b32b3a297274a3addfa44decd64f1b5fd564ab87ff0f60e50176a705f9c7a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leadhistory.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>Leads Management History</h1></div>
<div>
        ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "
        <div class=\"alert alert-danger\">";
            // line 10
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "
        ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "
        <div class=\"alert alert-success\">";
            // line 16
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
        ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "
        <div class=\"alert alert-success\">";
            // line 22
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "</div>
<div class=\"row\">
<table class=\"col-md-6 vendor-enquiries\">
<thead><tr><th>Category</th><th>Total Leads Purchased Till Date</th><th>Total Available Leads</th>
<!--<th>Deducted Leads History
</th><th>Package</th>-->
<th>Action</th></tr></thead>
";
        // line 32
        if ( !twig_test_empty((isset($context["leads"]) ? $context["leads"] : null))) {
            // line 33
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["leads"]) ? $context["leads"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
                // line 34
                echo "
<tr>
<td>";
                // line 36
                if (($this->getAttribute($context["l"], "flag", array()) == 2)) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_view_leads_freetrailhistory", array("uid" => $this->getAttribute($context["l"], "vendorid", array()), "catid" => $this->getAttribute($context["l"], "categoryid", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "category", array()), "html", null, true);
                    echo "</a>";
                } else {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_view_leads_history", array("uid" => $this->getAttribute($context["l"], "vendorid", array()), "catid" => $this->getAttribute($context["l"], "categoryid", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "category", array()), "html", null, true);
                    echo "</a>";
                }
                echo "</td>
<td>";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "pur", array()), "html", null, true);
                echo "</td>
<td>";
                // line 38
                if (twig_test_empty($this->getAttribute($context["l"], "leadpack", array()))) {
                    echo " 0 ";
                } else {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "leadpack", array()), "html", null, true);
                    echo " ";
                }
                echo "</td>
<!--<td>
\t";
                // line 40
                if ((twig_test_empty($this->getAttribute($context["l"], "deductedlead", array())) && twig_test_empty($this->getAttribute($context["l"], "deductedfreelead", array())))) {
                    echo " N/A 
\t";
                } elseif (twig_test_empty($this->getAttribute(                // line 41
$context["l"], "deductedlead", array()))) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "deductedfreelead", array()), "html", null, true);
                    echo "  Freetrail 
\t";
                } elseif (twig_test_empty($this->getAttribute(                // line 42
$context["l"], "deductedfreelead", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "deductedlead", array()), "html", null, true);
                    echo " Purchased leads
\t";
                } else {
                    // line 43
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "deductedlead", array()), "html", null, true);
                    echo " Purchased leads<br/> and ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "deductedfreelead", array()), "html", null, true);
                    echo " Freetrail ";
                }
                echo "</td>
\t
<td>";
                // line 45
                if (($this->getAttribute($context["l"], "flag", array()) == 2)) {
                    echo "Freetrail ";
                } elseif (twig_test_empty($this->getAttribute($context["l"], "flag", array()))) {
                    echo "Freetrail ";
                } else {
                    echo " Lead Purchased ";
                }
                echo "</td>-->
<td> <a href=\"";
                // line 46
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_setsessioncategory", array("catid" => $this->getAttribute($context["l"], "category", array()))), "html", null, true);
                echo "\" class=\"btn btn-success\">Buy leads </a> 
 

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 51
            echo "<tr><td colspan=\"2\" style=\"text-align:enter;\">No Data Available</td></tr>
";
        }
        // line 53
        echo "</table>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leadhistory.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 53,  188 => 51,  177 => 46,  167 => 45,  157 => 43,  151 => 42,  145 => 41,  141 => 40,  130 => 38,  126 => 37,  110 => 36,  106 => 34,  102 => 33,  100 => 32,  91 => 25,  82 => 22,  79 => 21,  75 => 20,  72 => 19,  63 => 16,  60 => 15,  56 => 14,  53 => 13,  44 => 10,  41 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}

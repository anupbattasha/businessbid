<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // ticketing_ticket_ticket_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ticketing_ticket_ticket_homepage')), array (  '_controller' => 'TicketingTicketTicketBundle:Default:index',));
        }

        if (0 === strpos($pathinfo, '/_errors')) {
            // b_bids_b_bids_home_homepage
            if (rtrim($pathinfo, '/') === '/_errors') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_home_homepage');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::indexAction',  '_route' => 'b_bids_b_bids_home_homepage',);
            }

            if (0 === strpos($pathinfo, '/_errors/search')) {
                // b_bids_b_bids_home_search
                if (preg_match('#^/_errors/search(?:/(?P<vendorid>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_home_search')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::searchAction',  'vendorid' => 1,));
                }

                // b_bids_b_bids_home_search_resource
                if (0 === strpos($pathinfo, '/_errors/search2') && preg_match('#^/_errors/search2(?:/(?P<vendorid>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_home_search_resource')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::searchResourceAction',  'vendorid' => 1,));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/a')) {
                if (0 === strpos($pathinfo, '/_errors/admin')) {
                    // b_bids_b_bids_admin_home
                    if ($pathinfo === '/_errors/admin') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::indexAction',  '_route' => 'b_bids_b_bids_admin_home',);
                    }

                    // b_bids_b_bids_admin_login
                    if (rtrim($pathinfo, '/') === '/_errors/admin/login') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'b_bids_b_bids_admin_login');
                        }

                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::loginAction',  '_route' => 'b_bids_b_bids_admin_login',);
                    }

                }

                // b_bids_b_bids_aboutbusinessbid
                if ($pathinfo === '/_errors/aboutbusinessbid') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::aboutbusinessbidAction',  '_route' => 'b_bids_b_bids_aboutbusinessbid',);
                }

                if (0 === strpos($pathinfo, '/_errors/admin/categor')) {
                    // b_bids_b_bids_admin_categories
                    if ($pathinfo === '/_errors/admin/categories') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::categoriesAction',  '_route' => 'b_bids_b_bids_admin_categories',);
                    }

                    // b_bids_b_bids_admin_category_add
                    if ($pathinfo === '/_errors/admin/category/add') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addcategoryAction',  '_route' => 'b_bids_b_bids_admin_category_add',);
                    }

                }

            }

            // b_bids_b_bids_consumer_register
            if ($pathinfo === '/_errors/customer/register') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerregisterAction',  '_route' => 'b_bids_b_bids_consumer_register',);
            }

            if (0 === strpos($pathinfo, '/_errors/user')) {
                // b_bids_b_bids_user_sms_verify
                if (0 === strpos($pathinfo, '/_errors/user/smsverify') && preg_match('#^/_errors/user/smsverify/(?P<smscode>[^/]++)/(?P<userid>[^/]++)/(?P<path>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_sms_verify')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::smsverifyAction',));
                }

                // b_bids_b_bids_user_email_verify
                if (0 === strpos($pathinfo, '/_errors/user/email/verify') && preg_match('#^/_errors/user/email/verify/(?P<userid>[^/]++)/(?P<userhash>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_email_verify')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::emailverifyAction',));
                }

                // b_bids_b_bids_user_sms_verified
                if ($pathinfo === '/_errors/user/sms/verified') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::smsverifiedAction',  '_route' => 'b_bids_b_bids_user_sms_verified',);
                }

                // b_bids_b_bids_user_email_verified
                if ($pathinfo === '/_errors/user/email/verified') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::emailverifiedAction',  '_route' => 'b_bids_b_bids_user_email_verified',);
                }

            }

            if (0 === strpos($pathinfo, '/_errors/post')) {
                // b_bids_b_bids_post_job
                if ($pathinfo === '/_errors/post/job') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postjobAction',  '_route' => 'b_bids_b_bids_post_job',);
                }

                // b_bids_b_bids_post_quote
                if ($pathinfo === '/_errors/post/quote') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postquoteAction',  '_route' => 'b_bids_b_bids_post_quote',);
                }

            }

            // b_choosesubcategoriesbycategory
            if ($pathinfo === '/_errors/chooseSubcategoriesByCategory') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::chooseSubcategoriesByCategoryAction',  '_route' => 'b_choosesubcategoriesbycategory',);
            }

            // b_bids_b_bids_enquiry_processing
            if ($pathinfo === '/_errors/enquiry/process') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enquiryprocessAction',  '_route' => 'b_bids_b_bids_enquiry_processing',);
            }

            // b_bids_b_bids_vendor_register
            if ($pathinfo === '/_errors/vendor/register') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorregisterAction',  '_route' => 'b_bids_b_bids_vendor_register',);
            }

            if (0 === strpos($pathinfo, '/_errors/post/quote/bysearch')) {
                // b_bids_b_bids_post_by_search
                if (preg_match('#^/_errors/post/quote/bysearch/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_post_by_search')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postbysearchAction',));
                }

                // b_bids_b_bids_post_by_search_inline
                if (0 === strpos($pathinfo, '/_errors/post/quote/bysearch/inline') && preg_match('#^/_errors/post/quote/bysearch/inline/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_post_by_search_inline')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postbysearchinlineAction',));
                }

            }

            // b_bids_b_bids_vendor_register_step_2
            if ($pathinfo === '/_errors/vendor/choosecategories') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorcategoriesAction',  '_route' => 'b_bids_b_bids_vendor_register_step_2',);
            }

            if (0 === strpos($pathinfo, '/_errors/a')) {
                // b_bids_b_bids_admin_master
                if ($pathinfo === '/_errors/admin/master') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::loadmasterAction',  '_route' => 'b_bids_b_bids_admin_master',);
                }

                // b_bids_b_bids_account
                if ($pathinfo === '/_errors/account') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::accountAction',  '_route' => 'b_bids_b_bids_account',);
                }

            }

            if (0 === strpos($pathinfo, '/_errors/log')) {
                // b_bids_b_bids_logout
                if ($pathinfo === '/_errors/logout') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::logoutAction',  '_route' => 'b_bids_b_bids_logout',);
                }

                // b_bids_b_bids_login
                if (0 === strpos($pathinfo, '/_errors/login') && preg_match('#^/_errors/login(?:/(?P<returnurl>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_login')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::loginAction',  'returnurl' => 1,));
                }

            }

            // b_bids_b_bids_vendor_home
            if ($pathinfo === '/_errors/vendor/home') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorhomeAction',  '_route' => 'b_bids_b_bids_vendor_home',);
            }

            // b_bids_b_bids_home
            if ($pathinfo === '/_errors/home') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::homeAction',  '_route' => 'b_bids_b_bids_home',);
            }

            if (0 === strpos($pathinfo, '/_errors/vendor')) {
                // b_bids_b_bids_vendor_buy_leads
                if ($pathinfo === '/_errors/vendor/buy/leads') {
                    return array (  '_controller' => 'BBidsBBidsHomeBundle:Vendor:buyleads',  '_route' => 'b_bids_b_bids_vendor_buy_leads',);
                }

                // b_bids_b_bids_vendor_purchase_leads
                if ($pathinfo === '/_errors/vendor/purchase/leads') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::purchaseLeadsAction',  '_route' => 'b_bids_b_bids_vendor_purchase_leads',);
                }

                // b_bids_b_bids_store_purchase_leads
                if ($pathinfo === '/_errors/vendor/store/purchase/leads') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::storePurchaseLeadsAction',  '_route' => 'b_bids_b_bids_store_purchase_leads',);
                }

                // b_bids_b_bids_vendor_lead_plan
                if (0 === strpos($pathinfo, '/_errors/vendor/leads/plan') && preg_match('#^/_errors/vendor/leads/plan(?:/(?P<purchaseID>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_lead_plan')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsPlanAction',  'purchaseID' => 1,));
                }

                // b_bids_b_bids_store_lead_plan
                if ($pathinfo === '/_errors/vendor/store/leads/plan') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::storeLeadsPlanAction',  '_route' => 'b_bids_b_bids_store_lead_plan',);
                }

                // b_bids_b_bids_pymt_form
                if (0 === strpos($pathinfo, '/_errors/vendor/leads/payment') && preg_match('#^/_errors/vendor/leads/payment/(?P<purchaseID>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_pymt_form')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsPaymentFormAction',));
                }

                // b_bids_b_bids_pymt_success
                if (0 === strpos($pathinfo, '/_errors/vendor/payment/confirmation') && preg_match('#^/_errors/vendor/payment/confirmation/(?P<ordernumber>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_pymt_success')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsPaymentConfirmationAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/leads')) {
                // b_bids_b_bids_leads_history
                if ($pathinfo === '/_errors/leads/history') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadhistoryAction',  '_route' => 'b_bids_b_bids_leads_history',);
                }

                if (0 === strpos($pathinfo, '/_errors/leads/view')) {
                    // b_bids_b_bids_view_leads
                    if ($pathinfo === '/_errors/leads/view') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsviewAction',  '_route' => 'b_bids_b_bids_view_leads',);
                    }

                    // b_bids_b_bids_view_leads_history
                    if (0 === strpos($pathinfo, '/_errors/leads/view/history') && preg_match('#^/_errors/leads/view/history/(?P<uid>[^/]++)/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_view_leads_history')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsviewhistoryAction',));
                    }

                }

            }

            // b_bids_b_bids_vendor_choose_lead_pack
            if ($pathinfo === '/_errors/vendor/choosepack') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::choosepackAction',  '_route' => 'b_bids_b_bids_vendor_choose_lead_pack',);
            }

            // b_bids_b_bids_consumer_home
            if (0 === strpos($pathinfo, '/_errors/customer/home') && preg_match('#^/_errors/customer/home(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumer_home')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerhomeAction',  'offset' => 1,));
            }

            // b_bids_b_bids_vendor_enquiries
            if (0 === strpos($pathinfo, '/_errors/vendor/enquiries') && preg_match('#^/_errors/vendor/enquiries(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_enquiries')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorenquiriesAction',  'offset' => 1,));
            }

            // b_bids_b_bids_enquiries_my
            if ($pathinfo === '/_errors/enquiries/my') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::myenquiriesAction',  '_route' => 'b_bids_b_bids_enquiries_my',);
            }

            // b_bids_b_bids_leads_my
            if ($pathinfo === '/_errors/leads/my') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::buyleadsAction',  '_route' => 'b_bids_b_bids_leads_my',);
            }

            // b_bids_b_bids_accept_enquiry
            if (0 === strpos($pathinfo, '/_errors/enquiry/accept') && preg_match('#^/_errors/enquiry/accept/(?P<category>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_accept_enquiry')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::acceptenquiryAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/admin')) {
                if (0 === strpos($pathinfo, '/_errors/admin/user')) {
                    if (0 === strpos($pathinfo, '/_errors/admin/users')) {
                        // b_bids_b_bids_admin_users
                        if (preg_match('#^/_errors/admin/users(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_users')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::usersAction',  'offset' => 1,));
                        }

                        // b_bids_b_bids_admin_users_new
                        if ($pathinfo === '/_errors/admin/usersnew') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::usersNewAction',  '_route' => 'b_bids_b_bids_admin_users_new',);
                        }

                        // b_bids_b_bids_admin_users_grid
                        if ($pathinfo === '/_errors/admin/usersgrid') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::usersGridAction',  '_route' => 'b_bids_b_bids_admin_users_grid',);
                        }

                    }

                    // b_bids_b_bids_admin_user_edit
                    if (0 === strpos($pathinfo, '/_errors/admin/user/edit') && preg_match('#^/_errors/admin/user/edit/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_user_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::edituserAction',));
                    }

                    // b_bids_b_bids_admin_user_delete
                    if (0 === strpos($pathinfo, '/_errors/admin/user/delete') && preg_match('#^/_errors/admin/user/delete/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_user_delete')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::deleteuserAction',));
                    }

                    // b_bids_b_bids_admin_user_view
                    if (0 === strpos($pathinfo, '/_errors/admin/user/view') && preg_match('#^/_errors/admin/user/view/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_user_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::viewuserAction',));
                    }

                }

                // b_bids_b_bids_freetrails
                if (0 === strpos($pathinfo, '/_errors/admin/freetrail') && preg_match('#^/_errors/admin/freetrail(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_freetrails')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::freetrailAction',  'offset' => 1,));
                }

                if (0 === strpos($pathinfo, '/_errors/admin/enquir')) {
                    // b_bids_b_bids_admin_enquiries
                    if (0 === strpos($pathinfo, '/_errors/admin/enquiries') && preg_match('#^/_errors/admin/enquiries(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enquiries')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiriesAction',  'offset' => 1,));
                    }

                    if (0 === strpos($pathinfo, '/_errors/admin/enquiry')) {
                        // b_bids_b_bids_admin_enquiry_delete
                        if ($pathinfo === '/_errors/admin/enquiry/delete') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquirydeleteAction',  '_route' => 'b_bids_b_bids_admin_enquiry_delete',);
                        }

                        // b_bids_b_bids_admin_enquiry_edit
                        if (0 === strpos($pathinfo, '/_errors/admin/enquiry/edit') && preg_match('#^/_errors/admin/enquiry/edit/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enquiry_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiryeditAction',));
                        }

                        // b_bids_b_bids_admin_enquiry_view
                        if (0 === strpos($pathinfo, '/_errors/admin/enquiry/view') && preg_match('#^/_errors/admin/enquiry/view/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enquiry_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiryviewAction',));
                        }

                    }

                }

                // b_bids_b_bids_admin_mapped_vendors
                if (0 === strpos($pathinfo, '/_errors/admin/mapped/vendors') && preg_match('#^/_errors/admin/mapped/vendors/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_mapped_vendors')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::mappedvendorsAction',));
                }

                // b_bids_b_bids_admin_vendor_response
                if (0 === strpos($pathinfo, '/_errors/admin/vendor/response') && preg_match('#^/_errors/admin/vendor/response/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_vendor_response')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorresponseAction',));
                }

                if (0 === strpos($pathinfo, '/_errors/admin/add')) {
                    // b_bids_b_bids_admin_add_subcategory
                    if (0 === strpos($pathinfo, '/_errors/admin/add/subcategory') && preg_match('#^/_errors/admin/add/subcategory/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_add_subcategory')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addsubcategoryAction',));
                    }

                    // b_bids_b_bids_admin_add_keyword
                    if (0 === strpos($pathinfo, '/_errors/admin/add/keyword') && preg_match('#^/_errors/admin/add/keyword/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_add_keyword')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addkeywordAction',));
                    }

                }

                // b_bids_b_bids_admin_subcategories
                if (0 === strpos($pathinfo, '/_errors/admin/subcategories') && preg_match('#^/_errors/admin/subcategories/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_subcategories')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::subcategoriesAction',));
                }

                // b_bids_b_bids_admin_keywords
                if (0 === strpos($pathinfo, '/_errors/admin/keywords') && preg_match('#^/_errors/admin/keywords/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_keywords')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::keywordsAction',));
                }

                // b_bids_b_bids_admin_add_category
                if ($pathinfo === '/_errors/admin/add/category') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addcategoryAction',  '_route' => 'b_bids_b_bids_admin_add_category',);
                }

            }

            // b_bids_b_bids_forgot_password
            if ($pathinfo === '/_errors/forgotpassword') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::forgotpasswordAction',  '_route' => 'b_bids_b_bids_forgot_password',);
            }

            // b_bids_b_bids_dashboard
            if ($pathinfo === '/_errors/dashboard') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::dashboardAction',  '_route' => 'b_bids_b_bids_dashboard',);
            }

            if (0 === strpos($pathinfo, '/_errors/admin')) {
                // b_bids_b_bids_admin_edit_category
                if (0 === strpos($pathinfo, '/_errors/admin/edit/category') && preg_match('#^/_errors/admin/edit/category/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit_category')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editcategoryAction',));
                }

                // b_bids_b_bids_admin_delete_category
                if (0 === strpos($pathinfo, '/_errors/admin/delete/category') && preg_match('#^/_errors/admin/delete/category/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_delete_category')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::deletecategoryAction',));
                }

                if (0 === strpos($pathinfo, '/_errors/admin/e')) {
                    // b_bids_b_bids_admin_enable_category
                    if (0 === strpos($pathinfo, '/_errors/admin/enable/category') && preg_match('#^/_errors/admin/enable/category/(?P<catid>[^/]++)/(?P<stat>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enable_category')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enablecategoryAction',));
                    }

                    if (0 === strpos($pathinfo, '/_errors/admin/edit')) {
                        // b_bids_b_bids_admin_edit_subcategory
                        if (0 === strpos($pathinfo, '/_errors/admin/edit/subcategory') && preg_match('#^/_errors/admin/edit/subcategory/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit_subcategory')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editsubcategoryAction',));
                        }

                        // b_bids_b_bids_admin_edit_keyword
                        if (0 === strpos($pathinfo, '/_errors/admin/edit/keyword') && preg_match('#^/_errors/admin/edit/keyword/(?P<keyid>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit_keyword')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editkeywordAction',));
                        }

                    }

                }

                // b_bids_b_bids_admin_delete_keyword
                if (0 === strpos($pathinfo, '/_errors/admin/delete/keyword') && preg_match('#^/_errors/admin/delete/keyword/(?P<keyid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_delete_keyword')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::deletekeywordAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/user')) {
                // b_bids_b_bids_user_authenticate
                if (0 === strpos($pathinfo, '/_errors/user/authenticate') && preg_match('#^/_errors/user/authenticate/(?P<email>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_authenticate')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::authenticateAction',));
                }

                // b_bids_b_bids_user_email_activate
                if (0 === strpos($pathinfo, '/_errors/user/email/activate') && preg_match('#^/_errors/user/email/activate/(?P<email>[^/]++)/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_email_activate')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::emailactivateAction',));
                }

            }

            // b_bids_b_bids_how_it_works_2
            if ($pathinfo === '/_errors/node3') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node3Action',  '_route' => 'b_bids_b_bids_how_it_works_2',);
            }

            // b_bids_b_bids_forgot_password_auto
            if (0 === strpos($pathinfo, '/_errors/user/forgotpassword/auto') && preg_match('#^/_errors/user/forgotpassword/auto/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_forgot_password_auto')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::forgotpasswordautoAction',));
            }

            // b_bids_b_bids_node4
            if ($pathinfo === '/_errors/node4') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node4Action',  '_route' => 'b_bids_b_bids_node4',);
            }

            if (0 === strpos($pathinfo, '/_errors/review')) {
                // b_bids_b_bids_reviewform_post
                if (0 === strpos($pathinfo, '/_errors/reviews/list') && preg_match('#^/_errors/reviews/list/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviewform_post')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewsListByCategoryAction',));
                }

                // b_bids_b_bids_reviews_quote
                if ($pathinfo === '/_errors/review/quote/vendor') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::storeReviewVendorsAction',  '_route' => 'b_bids_b_bids_reviews_quote',);
                }

            }

            // b_bids_b_bids_jobform_post
            if ($pathinfo === '/_errors/jobformpost') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::jobformpostAction',  '_route' => 'b_bids_b_bids_jobform_post',);
            }

            // b_bids_b_bids_node5
            if ($pathinfo === '/_errors/node5') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node5Action',  '_route' => 'b_bids_b_bids_node5',);
            }

            if (0 === strpos($pathinfo, '/_errors/vendor')) {
                // b_bids_b_bids_vendor
                if (0 === strpos($pathinfo, '/_errors/vendor/profile') && preg_match('#^/_errors/vendor/profile/(?P<userid>[^/]++)/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vendorProfileAction',));
                }

                // b_bids_b_bids_vendor_update
                if (0 === strpos($pathinfo, '/_errors/vendor/update') && preg_match('#^/_errors/vendor/update/(?P<vendorid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_update')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorupdateAction',));
                }

            }

            // b_bids_b_bids_consumer_update
            if (0 === strpos($pathinfo, '/_errors/customer/update') && preg_match('#^/_errors/customer/update/(?P<consumerid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumer_update')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerupdateAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/vendor')) {
                if (0 === strpos($pathinfo, '/_errors/vendor/save')) {
                    // b_bids_b_bids_vendor_save_account
                    if ($pathinfo === '/_errors/vendor/save/account') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::saveaccountAction',  '_route' => 'b_bids_b_bids_vendor_save_account',);
                    }

                    if (0 === strpos($pathinfo, '/_errors/vendor/save/c')) {
                        // b_bids_b_bids_vendor_save_city
                        if ($pathinfo === '/_errors/vendor/save/city') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savecityAction',  '_route' => 'b_bids_b_bids_vendor_save_city',);
                        }

                        // b_bids_b_bids_vendor_save_certification
                        if ($pathinfo === '/_errors/vendor/save/certification') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savecertificationAction',  '_route' => 'b_bids_b_bids_vendor_save_certification',);
                        }

                    }

                    // b_bids_b_bids_vendor_save_product
                    if ($pathinfo === '/_errors/vendor/save/product') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::saveproductAction',  '_route' => 'b_bids_b_bids_vendor_save_product',);
                    }

                    // b_bids_b_bids_vendor_save_album
                    if ($pathinfo === '/_errors/vendor/save/album') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savealbumAction',  '_route' => 'b_bids_b_bids_vendor_save_album',);
                    }

                }

                // b_bids_b_bids_unlink_album
                if (0 === strpos($pathinfo, '/_errors/vendor/unlink/album') && preg_match('#^/_errors/vendor/unlink/album/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_album')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removeAlbumAction',));
                }

                if (0 === strpos($pathinfo, '/_errors/vendor/save')) {
                    // b_bids_b_bids_vendor_save_photo
                    if ($pathinfo === '/_errors/vendor/save/photo') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savephotoAction',  '_route' => 'b_bids_b_bids_vendor_save_photo',);
                    }

                    // b_bids_b_bids_vendor_save_logo
                    if ($pathinfo === '/_errors/vendor/save/logo') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::savelogoAction',  '_route' => 'b_bids_b_bids_vendor_save_logo',);
                    }

                }

            }

            // b_bids_b_bids_album_view
            if (0 === strpos($pathinfo, '/_errors/album/view') && preg_match('#^/_errors/album/view/(?P<albumid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_album_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::albumviewAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/p')) {
                // b_bids_b_bids_add_photo
                if (0 === strpos($pathinfo, '/_errors/photo/add') && preg_match('#^/_errors/photo/add/(?P<albumid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_add_photo')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::addphotoAction',));
                }

                // b_bids_b_bids_post_review
                if (0 === strpos($pathinfo, '/_errors/post/review') && preg_match('#^/_errors/post/review/(?P<authorid>[^/]++)/(?P<vendorid>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_post_review')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::postreviewAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/admin/review')) {
                // b_bids_b_bids_reviews
                if (0 === strpos($pathinfo, '/_errors/admin/reviews') && preg_match('#^/_errors/admin/reviews/(?P<offset>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::reviewsAction',));
                }

                // b_bids_b_bids_review
                if (preg_match('#^/_errors/admin/review/(?P<reviewid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_review')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::reviewAction',));
                }

                // b_bids_b_bids_admin_publish_review
                if (0 === strpos($pathinfo, '/_errors/admin/review/publish') && preg_match('#^/_errors/admin/review/publish/(?P<reviewid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_publish_review')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::publishreviewAction',));
                }

            }

            // b_bids_b_bids_node6
            if ($pathinfo === '/_errors/node6') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node6Action',  '_route' => 'b_bids_b_bids_node6',);
            }

            if (0 === strpos($pathinfo, '/_errors/vendor/unlink')) {
                // b_bids_b_bids_unlink_certification
                if (0 === strpos($pathinfo, '/_errors/vendor/unlink/certification') && preg_match('#^/_errors/vendor/unlink/certification/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_certification')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removecertificationAction',));
                }

                // b_bids_b_bids_unlink_product
                if (0 === strpos($pathinfo, '/_errors/vendor/unlink/product') && preg_match('#^/_errors/vendor/unlink/product/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_product')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removeproductsAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/admin/order')) {
                // b_bids_b_bids_admin_orders
                if (0 === strpos($pathinfo, '/_errors/admin/orders') && preg_match('#^/_errors/admin/orders/(?P<offset>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_orders')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::ordersAction',));
                }

                // b_bids_b_bids_admin_approve_order
                if (0 === strpos($pathinfo, '/_errors/admin/order/approve') && preg_match('#^/_errors/admin/order/approve/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_approve_order')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::orderapproveAction',));
                }

                // b_bids_b_bids_admin_order_view
                if (0 === strpos($pathinfo, '/_errors/admin/order/view') && preg_match('#^/_errors/admin/order/view/(?P<id>[^/]++)/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_order_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vieworderAction',));
                }

            }

            // b_bids_b_bids_change_password
            if ($pathinfo === '/_errors/password/change') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::changepasswordAction',  '_route' => 'b_bids_b_bids_change_password',);
            }

            // b_bids_b_bids_view_consumer
            if (0 === strpos($pathinfo, '/_errors/customer/view') && preg_match('#^/_errors/customer/view/(?P<id>[^/]++)/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_view_consumer')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerviewAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/vendorview')) {
                // b_bids_b_bids_vendororders
                if (0 === strpos($pathinfo, '/_errors/vendorview/orders') && preg_match('#^/_errors/vendorview/orders(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendororders')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorordersAction',  'offset' => 1,));
                }

                // b_bids_b_bids_vendorreviews
                if (0 === strpos($pathinfo, '/_errors/vendorview/reviews') && preg_match('#^/_errors/vendorview/reviews(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendorreviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorreviewsAction',  'offset' => 1,));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/customer')) {
                // b_bids_b_bids_consumerreviews
                if (0 === strpos($pathinfo, '/_errors/customerview/reviews') && preg_match('#^/_errors/customerview/reviews/(?P<offset>[^/\\:]++)\\:$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumerreviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerreviewsAction',  'offset' => 1,));
                }

                // b_bids_b_bids_customerorder
                if ($pathinfo === '/_errors/customerorder:') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerordersAction',  '_route' => 'b_bids_b_bids_customerorder',);
                }

            }

            // b_bids_b_bids_admin_report
            if ($pathinfo === '/_errors/admin/report') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::adminReportAction',  '_route' => 'b_bids_b_bids_admin_report',);
            }

            // b_bids_b_bids_try_it_pack
            if (0 === strpos($pathinfo, '/_errors/vendor/tryitnoworder') && preg_match('#^/_errors/vendor/tryitnoworder/(?P<categoryArray>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_try_it_pack')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::tryitnoworderAction',));
            }

            // b_bids_b_bids_admin_tryleads
            if (0 === strpos($pathinfo, '/_errors/admin/tryitnowleads') && preg_match('#^/_errors/admin/tryitnowleads(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_tryleads')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::viewtryleadsAction',  'offset' => 1,));
            }

            // b_bids_b_bids_facebook_login
            if (0 === strpos($pathinfo, '/_errors/facebook/signin') && preg_match('#^/_errors/facebook/signin/(?P<userid>[^/]++)/(?P<email>[^/]++)/(?P<fname>[^/]++)/(?P<pid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_facebook_login')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::facebookloginAction',));
            }

            // b_bids_b_bids_setsessioncategory
            if (0 === strpos($pathinfo, '/_errors/setsessioncategory') && preg_match('#^/_errors/setsessioncategory/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_setsessioncategory')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::setsessioncategoryAction',));
            }

            // b_bids_b_bids_my_reviews
            if (0 === strpos($pathinfo, '/_errors/reviews') && preg_match('#^/_errors/reviews(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_my_reviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::myreviewsAction',  'offset' => 1,));
            }

            // b_bids_b_bids_enquiry_view
            if (0 === strpos($pathinfo, '/_errors/en/view') && preg_match('#^/_errors/en/view/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_enquiry_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enviewAction',));
            }

            // b_bids_b_bids_reviewsmy
            if (0 === strpos($pathinfo, '/_errors/my/reviews') && preg_match('#^/_errors/my/reviews(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviewsmy')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::conreviewsAction',  'offset' => 1,));
            }

            // b_bids_b_bids_faq
            if ($pathinfo === '/_errors/faq') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::faqAction',  '_route' => 'b_bids_b_bids_faq',);
            }

            if (0 === strpos($pathinfo, '/_errors/support')) {
                // b_bids_b_bids_support
                if ($pathinfo === '/_errors/support') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportAction',  '_route' => 'b_bids_b_bids_support',);
                }

                // b_bids_b_bids_supportview
                if ($pathinfo === '/_errors/support/view') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportviewAction',  '_route' => 'b_bids_b_bids_supportview',);
                }

            }

            // b_bids_b_bids_consumer_postenquiries
            if (0 === strpos($pathinfo, '/_errors/customer/enquiries') && preg_match('#^/_errors/customer/enquiries(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumer_postenquiries')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerenquiriesAction',  'offset' => 1,));
            }

            // b_bids_b_bids_view_vendor
            if (0 === strpos($pathinfo, '/_errors/vendor/view') && preg_match('#^/_errors/vendor/view/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_view_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorviewAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/customer')) {
                // b_bids_b_bids_consumeraccount
                if ($pathinfo === '/_errors/customer/account') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumeraccountAction',  '_route' => 'b_bids_b_bids_consumeraccount',);
                }

                if (0 === strpos($pathinfo, '/_errors/customer/s')) {
                    // b_bids_b_bids_consumer_save_account
                    if ($pathinfo === '/_errors/customer/save/account') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumersaveaccountAction',  '_route' => 'b_bids_b_bids_consumer_save_account',);
                    }

                    if (0 === strpos($pathinfo, '/_errors/customer/support')) {
                        // b_bids_b_bids_support_consumer
                        if ($pathinfo === '/_errors/customer/support') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportconsumerAction',  '_route' => 'b_bids_b_bids_support_consumer',);
                        }

                        // b_bids_b_bids_supportview_consumer
                        if ($pathinfo === '/_errors/customer/support/view') {
                            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportviewconsumerAction',  '_route' => 'b_bids_b_bids_supportview_consumer',);
                        }

                    }

                }

                // b_bids_b_bids_ratings
                if (0 === strpos($pathinfo, '/_errors/customer/ratings') && preg_match('#^/_errors/customer/ratings(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ratings')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::consumerratingsAction',  'offset' => 1,));
                }

            }

            // b_bids_b_bids_mapped_vendor
            if (0 === strpos($pathinfo, '/_errors/mapped/vendor') && preg_match('#^/_errors/mapped/vendor/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::mappedvendortocustAction',));
            }

            // b_bids_b_bids_feedback
            if ($pathinfo === '/_errors/feedback') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::feedbackAction',  '_route' => 'b_bids_b_bids_feedback',);
            }

            // b_bids_b_bids_vendor_order_view
            if (0 === strpos($pathinfo, '/_errors/vendor/order/view') && preg_match('#^/_errors/vendor/order/view/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_order_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vieworderAction',));
            }

            // b_bids_b_bids_admin_review_edit
            if (0 === strpos($pathinfo, '/_errors/admin/edit/review') && preg_match('#^/_errors/admin/edit/review/(?P<reviewid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_review_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editreviewAction',));
            }

            // b_bids_b_bids_reviewlogin
            if (0 === strpos($pathinfo, '/_errors/review/login') && preg_match('#^/_errors/review/login(?:/(?P<returnurl>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviewlogin')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewloginAction',  'returnurl' => 1,));
            }

            // b_bids_b_bids_admin_edit
            if (0 === strpos($pathinfo, '/_errors/admin/profile/edit') && preg_match('#^/_errors/admin/profile/edit/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editadminAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/mapped')) {
                // b_bids_b_bids_mapped_enquiry
                if (0 === strpos($pathinfo, '/_errors/mapped/enquiry') && preg_match('#^/_errors/mapped/enquiry/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_enquiry')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enquirydetailsAction',));
                }

                // b_bids_b_bids_mapped_reviews
                if (0 === strpos($pathinfo, '/_errors/mapped/review') && preg_match('#^/_errors/mapped/review/(?P<rid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_reviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewdetailsAction',));
                }

                // b_bids_b_bids_mapped_enquiry_vendor
                if (0 === strpos($pathinfo, '/_errors/mapped/enquiry/vendor') && preg_match('#^/_errors/mapped/enquiry/vendor/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_enquiry_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enquirydetailsvendorAction',));
                }

                // b_bids_b_bids_mapped_reviews_vendor
                if (0 === strpos($pathinfo, '/_errors/mapped/review/vendor') && preg_match('#^/_errors/mapped/review/vendor/(?P<rid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_reviews_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewdetailsvendorAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_errors/vendors')) {
                if (0 === strpos($pathinfo, '/_errors/vendors/datatable')) {
                    // b_bids_b_bids_ticket_index
                    if ($pathinfo === '/_errors/vendors/datatable') {
                        return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::gridAction',  '_route' => 'b_bids_b_bids_ticket_index',);
                    }

                    // b_bids_b_bids_ticket_datatable
                    if ($pathinfo === '/_errors/vendors/datatableindex') {
                        return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::indexAction',  '_route' => 'b_bids_b_bids_ticket_datatable',);
                    }

                }

                // b_bids_b_bids_ticket_home
                if ($pathinfo === '/_errors/vendors/helpdesk') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::vendorTicketHomeAction',  '_route' => 'b_bids_b_bids_ticket_home',);
                }

                if (0 === strpos($pathinfo, '/_errors/vendors/ticket')) {
                    // b_bids_b_bids_ticket_list
                    if ($pathinfo === '/_errors/vendors/ticketlist') {
                        return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::vendorTicketListAction',  '_route' => 'b_bids_b_bids_ticket_list',);
                    }

                    // b_bids_b_bids_ticket_view
                    if (0 === strpos($pathinfo, '/_errors/vendors/ticketview/no') && preg_match('#^/_errors/vendors/ticketview/no\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ticket_view')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::vendorViewTicketAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/_errors/admin')) {
                if (0 === strpos($pathinfo, '/_errors/admin/helpdesk')) {
                    // b_bids_b_bids_ticket_adminhome
                    if ($pathinfo === '/_errors/admin/helpdesk/dashboard') {
                        return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\AdminticketController::adminTicketHomeAction',  '_route' => 'b_bids_b_bids_ticket_adminhome',);
                    }

                    // b_bids_b_bids_ticket_adminlist
                    if (0 === strpos($pathinfo, '/_errors/admin/helpdesk/list') && preg_match('#^/_errors/admin/helpdesk/list/(?P<usertype>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ticket_adminlist')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\AdminticketController::adminListTicketsAction',));
                    }

                }

                // b_bids_b_bids_ticket_adminview
                if (0 === strpos($pathinfo, '/_errors/admin/ticketview/no') && preg_match('#^/_errors/admin/ticketview/no\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ticket_adminview')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\AdminticketController::adminViewTicketAction',));
                }

            }

            // b_bids_b_bids_mymessages
            if ($pathinfo === '/_errors/mymessages') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::getemailmessagesAction',  '_route' => 'b_bids_b_bids_mymessages',);
            }

            // b_bids_b_bids_customer_messages
            if ($pathinfo === '/_errors/customer/messages') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::getcustomeremailmessagesAction',  '_route' => 'b_bids_b_bids_customer_messages',);
            }

            if (0 === strpos($pathinfo, '/_errors/user')) {
                // b_bids_b_bids_customer_ticket_home
                if ($pathinfo === '/_errors/user/helpdesk') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\CustomerticketController::customerTicketHomeAction',  '_route' => 'b_bids_b_bids_customer_ticket_home',);
                }

                if (0 === strpos($pathinfo, '/_errors/user/ticket')) {
                    // b_bids_b_bids_customer_ticket_list
                    if ($pathinfo === '/_errors/user/ticketlist') {
                        return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\CustomerticketController::customerTicketListAction',  '_route' => 'b_bids_b_bids_customer_ticket_list',);
                    }

                    // b_bids_b_bids_customer_ticket_view
                    if (0 === strpos($pathinfo, '/_errors/user/ticketview/no') && preg_match('#^/_errors/user/ticketview/no\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_customer_ticket_view')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\CustomerticketController::customerViewTicketAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/_errors/user/custtovendor')) {
                    // b_bids_b_bids_user_custtovendorupgration_success
                    if ($pathinfo === '/_errors/user/custtovendor/success') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::custovendorupgrationAction',  '_route' => 'b_bids_b_bids_user_custtovendorupgration_success',);
                    }

                    // b_bids_b_bids_user_custtovendorupgration_check
                    if (0 === strpos($pathinfo, '/_errors/user/custtovendor/check') && preg_match('#^/_errors/user/custtovendor/check/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_custtovendorupgration_check')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::custovendorupgrationcheckAction',));
                    }

                }

            }

            // b_bids_b_bids_sitemap
            if ($pathinfo === '/_errors/sitemap') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::sitemapAction',  '_route' => 'b_bids_b_bids_sitemap',);
            }

            // b_bids_b_bids_contactus
            if ($pathinfo === '/_errors/contactus') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::contactusAction',  '_route' => 'b_bids_b_bids_contactus',);
            }

            // b_bids_b_bids_vendorlicense
            if ($pathinfo === '/_errors/vendors/licenseupdate') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorLicenseAction',  '_route' => 'b_bids_b_bids_vendorlicense',);
            }

            // b_bids_b_bids_globalsearch
            if ($pathinfo === '/_errors/site/search') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::globalSearchAction',  '_route' => 'b_bids_b_bids_globalsearch',);
            }

            // b_bids_b_bids_admin_lead_management
            if (0 === strpos($pathinfo, '/_errors/admin/lead/credits') && preg_match('#^/_errors/admin/lead/credits(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_lead_management')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::leadrefundAction',  'offset' => 1,));
            }

            // b_bids_b_bids_unlink_lisence
            if (0 === strpos($pathinfo, '/_errors/vendor/unlink/lisence') && preg_match('#^/_errors/vendor/unlink/lisence/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_lisence')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removelisenceAction',));
            }

            // b_bids_b_bids_admin_dashboard
            if (rtrim($pathinfo, '/') === '/_errors/admin/dashboard') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_admin_dashboard');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::homeAction',  '_route' => 'b_bids_b_bids_admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/_errors/customer/con')) {
                // b_bids_b_bids_consumerupdatepasssword
                if (rtrim($pathinfo, '/') === '/_errors/customer/conchangepassword') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_consumerupdatepasssword');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::conchangepasswordAction',  '_route' => 'b_bids_b_bids_consumerupdatepasssword',);
                }

                // b_bids_b_bids_consumerupdatemail
                if (rtrim($pathinfo, '/') === '/_errors/customer/conupdateemail') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_consumerupdatemail');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::conupdateemailAction',  '_route' => 'b_bids_b_bids_consumerupdatemail',);
                }

                // b_bids_b_bids_consumerdeleteAccount
                if (rtrim($pathinfo, '/') === '/_errors/customer/condeleteAccount') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_consumerdeleteAccount');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::condeleteAccountAction',  '_route' => 'b_bids_b_bids_consumerdeleteAccount',);
                }

            }

            if (0 === strpos($pathinfo, '/_errors/vendor')) {
                if (0 === strpos($pathinfo, '/_errors/vendor/ven')) {
                    // b_bids_b_bids_vendorupdatepasssword
                    if (rtrim($pathinfo, '/') === '/_errors/vendor/venchangepassword') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendorupdatepasssword');
                        }

                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::venchangepasswordAction',  '_route' => 'b_bids_b_bids_vendorupdatepasssword',);
                    }

                    // b_bids_b_bids_vendorupdatemail
                    if (rtrim($pathinfo, '/') === '/_errors/vendor/venupdateemail') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendorupdatemail');
                        }

                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::venupdateemailAction',  '_route' => 'b_bids_b_bids_vendorupdatemail',);
                    }

                    // b_bids_b_bids_vendordeleteAccount
                    if (rtrim($pathinfo, '/') === '/_errors/vendor/vendeleteAccount') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendordeleteAccount');
                        }

                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendeleteAccountAction',  '_route' => 'b_bids_b_bids_vendordeleteAccount',);
                    }

                }

                // b_bids_b_bids_vendor_reports
                if (rtrim($pathinfo, '/') === '/_errors/vendor/reports') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendor_reports');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorReportsAction',  '_route' => 'b_bids_b_bids_vendor_reports',);
                }

            }

            // b_bids_b_bids_customer_reports
            if (rtrim($pathinfo, '/') === '/_errors/customer/reports') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_customer_reports');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::customerReportsAction',  '_route' => 'b_bids_b_bids_customer_reports',);
            }

            if (0 === strpos($pathinfo, '/_errors/orders')) {
                // b_bids_b_bids_orders_reports
                if (rtrim($pathinfo, '/') === '/_errors/orders/reports') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_orders_reports');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::orderReportsAction',  '_route' => 'b_bids_b_bids_orders_reports',);
                }

                // b_bids_b_bids_exportall_orders
                if ($pathinfo === '/_errors/orders/export/all') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportOrdersToExcelAction',  '_route' => 'b_bids_b_bids_exportall_orders',);
                }

            }

            // b_bids_b_bids_categories_reports
            if (rtrim($pathinfo, '/') === '/_errors/categories/reports') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_categories_reports');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::categoriesReportsAction',  '_route' => 'b_bids_b_bids_categories_reports',);
            }

            // b_bids_b_bids_enquiries_reports
            if (rtrim($pathinfo, '/') === '/_errors/enquiries/reports') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_enquiries_reports');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiriesReportsAction',  '_route' => 'b_bids_b_bids_enquiries_reports',);
            }

            // b_bids_b_bids_exportall_customer
            if ($pathinfo === '/_errors/customer/export/all') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportCustomersToExcelAction',  '_route' => 'b_bids_b_bids_exportall_customer',);
            }

            // b_bids_b_bids_exportall_vendors
            if ($pathinfo === '/_errors/vendor/export/all') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportVendorToExcelAction',  '_route' => 'b_bids_b_bids_exportall_vendors',);
            }

            // b_bids_b_bids_exportall_jobrequest
            if ($pathinfo === '/_errors/job/request/export/all') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportJobrequestToExcelAction',  '_route' => 'b_bids_b_bids_exportall_jobrequest',);
            }

            // b_bids_b_bids_exportall_categories
            if ($pathinfo === '/_errors/categories/export/all') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportCategoriesToExcelAction',  '_route' => 'b_bids_b_bids_exportall_categories',);
            }

            if (0 === strpos($pathinfo, '/_errors/sms')) {
                // b_bids_b_bids_sms_replies
                if ($pathinfo === '/_errors/sms/replies/all') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\SmsController::smsRepliesAction',  '_route' => 'b_bids_b_bids_sms_replies',);
                }

                // b_bids_b_bids_sms_job_req
                if ($pathinfo === '/_errors/sms/job/request') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\SmsController::sendJobReqSmsToVendorsAction',  '_route' => 'b_bids_b_bids_sms_job_req',);
                }

            }

            // b_bids_b_bids_international_user__verified
            if ($pathinfo === '/_errors/international/user/verified') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::internationaluserverifiedAction',  '_route' => 'b_bids_b_bids_international_user__verified',);
            }

            // b_bids_b_bids_pgateway_paypal
            if (0 === strpos($pathinfo, '/_errors/paymentgateway/paypal') && preg_match('#^/_errors/paymentgateway/paypal/(?P<purchaseID>[^/]++)/(?P<actiontype>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_pgateway_paypal')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::paypalPaymentgatewayAction',));
            }

            // b_bids_b_bids_accouting_auditing_ajaxaction
            if ($pathinfo === '/_errors/accouting/auditing/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::accountingAuditingAction',  '_route' => 'b_bids_b_bids_accouting_auditing_ajaxaction',);
            }

            // b_bids_b_bids_catering_services_ajaxaction
            if ($pathinfo === '/_errors/catering/service/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::cateringServicesAction',  '_route' => 'b_bids_b_bids_catering_services_ajaxaction',);
            }

            if (0 === strpos($pathinfo, '/_errors/i')) {
                // b_bids_b_bids_interior_designers_ajaxaction
                if ($pathinfo === '/_errors/interior/designers/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'b_bids_b_bids_interior_designers_ajaxaction',);
                }

                // b_bids_b_bids_it_support_ajaxaction
                if ($pathinfo === '/_errors/it/support/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'b_bids_b_bids_it_support_ajaxaction',);
                }

            }

            // b_bids_b_bids_landscape_gardens_ajaxaction
            if ($pathinfo === '/_errors/landscape/gardens/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::landscapeGardensAction',  '_route' => 'b_bids_b_bids_landscape_gardens_ajaxaction',);
            }

            // b_bids_b_bids_signage_signboards_ajaxaction
            if ($pathinfo === '/_errors/signage/signboards/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::signageSignboardsAction',  '_route' => 'b_bids_b_bids_signage_signboards_ajaxaction',);
            }

            // b_bids_b_bids_photography_video_ajaxaction
            if ($pathinfo === '/_errors/photography/video/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::photographyVideoAction',  '_route' => 'b_bids_b_bids_photography_video_ajaxaction',);
            }

            // b_bids_b_bids_offset_printing_ajaxaction
            if ($pathinfo === '/_errors/ofset/printing/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::offsetPrintingAction',  '_route' => 'b_bids_b_bids_offset_printing_ajaxaction',);
            }

            // b_bids_b_bids_commercial_cleaning_ajaxaction
            if ($pathinfo === '/_errors/commercial/cleaning/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::commercialCleaningAction',  '_route' => 'b_bids_b_bids_commercial_cleaning_ajaxaction',);
            }

            // biz_bids_florists_ajaxaction
            if ($pathinfo === '/_errors/florists/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::floristsAction',  '_route' => 'biz_bids_florists_ajaxaction',);
            }

            // biz_bids_event_managment_ajaxaction
            if ($pathinfo === '/_errors/event/managment/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::eventManagmentAction',  '_route' => 'biz_bids_event_managment_ajaxaction',);
            }

            // biz_bids_weeding_planners_ajaxaction
            if ($pathinfo === '/_errors/weeding/planners/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::weedingPlannersAction',  '_route' => 'biz_bids_weeding_planners_ajaxaction',);
            }

            // biz_bids_exhibitions_ajaxaction
            if ($pathinfo === '/_errors/exhibitions/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::exhibitionsAction',  '_route' => 'biz_bids_exhibitions_ajaxaction',);
            }

            // biz_bids_gifts_promotional_ajaxaction
            if ($pathinfo === '/_errors/gifts/promotional/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::giftsPromotionalAction',  '_route' => 'biz_bids_gifts_promotional_ajaxaction',);
            }

            // biz_bids_sound_lighting_ajaxaction
            if ($pathinfo === '/_errors/sound/lighting/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::soundLightingAction',  '_route' => 'biz_bids_sound_lighting_ajaxaction',);
            }

            // b_bids_b_bids_maintenance_ajaxaction
            if ($pathinfo === '/_errors/maintenance/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::maintenanceAction',  '_route' => 'b_bids_b_bids_maintenance_ajaxaction',);
            }

            // b_bids_b_bids_pest_control_ajaxaction
            if ($pathinfo === '/_errors/pest/control/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::pestControlAction',  '_route' => 'b_bids_b_bids_pest_control_ajaxaction',);
            }

            if (0 === strpos($pathinfo, '/_errors/a')) {
                // biz_bids_air_conditioning_cooling_ajaxaction
                if ($pathinfo === '/_errors/air-conditioning/air-cooling/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::airConditioningCoolingAction',  '_route' => 'biz_bids_air_conditioning_cooling_ajaxaction',);
                }

                // biz_bids_attestation_translation_ajaxaction
                if ($pathinfo === '/_errors/attestation/translation/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::attestationTranslationAction',  '_route' => 'biz_bids_attestation_translation_ajaxaction',);
                }

            }

            if (0 === strpos($pathinfo, '/_errors/b')) {
                // biz_bids_babysitter_nanny_ajaxaction
                if ($pathinfo === '/_errors/babysitter/nanny/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::babysitterNannyAction',  '_route' => 'biz_bids_babysitter_nanny_ajaxaction',);
                }

                // biz_bids_beauty_styling_ajaxaction
                if ($pathinfo === '/_errors/beauty/styling/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::beautyStylingAction',  '_route' => 'biz_bids_beauty_styling_ajaxaction',);
                }

            }

            if (0 === strpos($pathinfo, '/_errors/co')) {
                // biz_bids_computer_phone_repair_ajaxaction
                if ($pathinfo === '/_errors/computer/phone/repair/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::computerPhoneRepairAction',  '_route' => 'biz_bids_computer_phone_repair_ajaxaction',);
                }

                // biz_bids_concierge_errands_ajaxaction
                if ($pathinfo === '/_errors/concierge/errands/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::conciergeErrandsAction',  '_route' => 'biz_bids_concierge_errands_ajaxaction',);
                }

            }

            // biz_bids_education_learning_ajaxaction
            if ($pathinfo === '/_errors/education/learning/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'biz_bids_education_learning_ajaxaction',);
            }

            // biz_bids_moving_storage_ajaxaction
            if ($pathinfo === '/_errors/moving/storage/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'biz_bids_moving_storage_ajaxaction',);
            }

            if (0 === strpos($pathinfo, '/_errors/pe')) {
                // biz_bids_personal_fitness_trainer_ajaxaction
                if ($pathinfo === '/_errors/personal/fitness/trainer/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'biz_bids_personal_fitness_trainer_ajaxaction',);
                }

                // biz_bids_pet_service_ajaxaction
                if ($pathinfo === '/_errors/pet/service/ajaxformaction') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'biz_bids_pet_service_ajaxaction',);
                }

            }

            // biz_bids_sports_swimming_ajaxaction
            if ($pathinfo === '/_errors/sports/swimming/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\BaseCategoryController::categoriesAction',  '_route' => 'biz_bids_sports_swimming_ajaxaction',);
            }

            // b_bids_b_bids_invoice_reciept_pdf
            if (0 === strpos($pathinfo, '/_errors/invoice/reciept/pdf') && preg_match('#^/_errors/invoice/reciept/pdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_invoice_reciept_pdf')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::printOrderAction',));
            }

            // b_bids_b_bids_checkvendor_leads_available
            if ($pathinfo === '/_errors/vendor/leads/available') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::checkLeadAvailableAction',  '_route' => 'b_bids_b_bids_checkvendor_leads_available',);
            }

            // b_bids_b_bids_form_extra_fields
            if (0 === strpos($pathinfo, '/_errors/get/form/extrafields') && preg_match('#^/_errors/get/form/extrafields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_form_extra_fields')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::getExtraFormFieldsAction',));
            }

            // b_bids_b_bids_vendor_leads_list
            if ($pathinfo === '/_errors/vendor/leads/list') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorLeadsListAction',  '_route' => 'b_bids_b_bids_vendor_leads_list',);
            }

            // b_bids_b_bids_exportall_leadhistory
            if ($pathinfo === '/_errors/export/all/leadhistory') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportAllLeadsListAction',  '_route' => 'b_bids_b_bids_exportall_leadhistory',);
            }

            // b_bids_b_bids_vendor_category_mapping
            if ($pathinfo === '/_errors/vendor/category/mapping') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorCatgoryMappingAction',  '_route' => 'b_bids_b_bids_vendor_category_mapping',);
            }

            // b_bids_b_bids_category_form_elements
            if ($pathinfo === '/_errors/category/form/elements') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::categoryFormElementsAction',  '_route' => 'b_bids_b_bids_category_form_elements',);
            }

            // b_bids_b_bids_deactivate_category_mapping
            if (0 === strpos($pathinfo, '/_errors/vendor/toggle/mapping') && preg_match('#^/_errors/vendor/toggle/mapping/(?P<vid>[^/]++)/(?P<cid>[^/]++)/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_deactivate_category_mapping')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::toggleCategoryMappingAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/bbids-')) {
                // bizbids_are_you_a_vendor
                if ($pathinfo === '/_errors/bbids-leads') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::areyouaVendorAction',  '_route' => 'bizbids_are_you_a_vendor',);
                }

                // bizbids_template2
                if ($pathinfo === '/_errors/bbids-work') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::bbidsWorkAction',  '_route' => 'bizbids_template2',);
                }

            }

            // bizbids_template3
            if ($pathinfo === '/_errors/get-hired-on-bbids') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::getHiredAction',  '_route' => 'bizbids_template3',);
            }

            // bizbids_template4
            if ($pathinfo === '/_errors/why-join-bbids') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::whyJoinBbidsAction',  '_route' => 'bizbids_template4',);
            }

            if (0 === strpos($pathinfo, '/_errors/a')) {
                // bizbids_template5
                if ($pathinfo === '/_errors/are-you-a-vendor') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::template5Action',  '_route' => 'bizbids_template5',);
                }

                // bizbids_addcat
                if ($pathinfo === '/_errors/account/categorysuccess') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::categorysuccessAction',  '_route' => 'bizbids_addcat',);
                }

            }

            // bizbids_addcat_port
            if ($pathinfo === '/_errors/profile/categorysuccess') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::portcategorysuccessAction',  '_route' => 'bizbids_addcat_port',);
            }

            // bizbids_deletecat
            if (0 === strpos($pathinfo, '/_errors/account/categorydelete') && preg_match('#^/_errors/account/categorydelete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_deletecat')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::categorydeleteAction',));
            }

            // bizbids_editcat_port
            if ($pathinfo === '/_errors/profile/categoryedit') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::portcategoryeditAction',  '_route' => 'bizbids_editcat_port',);
            }

            // bizbids_editcat
            if ($pathinfo === '/_errors/account/categoryedit') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::categoryeditAction',  '_route' => 'bizbids_editcat',);
            }

            // bizbids_deletecat_port
            if (0 === strpos($pathinfo, '/_errors/profile/categorydelete') && preg_match('#^/_errors/profile/categorydelete/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_deletecat_port')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::portcategorydeleteAction',));
            }

            if (0 === strpos($pathinfo, '/_errors/vendor')) {
                // bizbids_vendor_search
                if ($pathinfo === '/_errors/vendor/search/home') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vendorSearchHomeAction',  '_route' => 'bizbids_vendor_search',);
                }

                // bizbids_vendor_direct_enquiry
                if ($pathinfo === '/_errors/vendor/direct/enquiry') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vendorDirectEnqAction',  '_route' => 'bizbids_vendor_direct_enquiry',);
                }

                if (0 === strpos($pathinfo, '/_errors/vendor/activation')) {
                    // bizbids_vendor_on_off_service
                    if ($pathinfo === '/_errors/vendor/activation/service') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vedorOnOffServiceAction',  '_route' => 'bizbids_vendor_on_off_service',);
                    }

                    // bizbids_vendor_on_off_ajax
                    if ($pathinfo === '/_errors/vendor/activation/request') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorOnOffAjaxAction',  '_route' => 'bizbids_vendor_on_off_ajax',);
                    }

                }

                // bizbids_vendor_test_account_toggle
                if (0 === strpos($pathinfo, '/_errors/vendor/test/account/toggle') && preg_match('#^/_errors/vendor/test/account/toggle/(?P<vid>[^/]++)/(?P<status>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_vendor_test_account_toggle')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::toggleVendTestAccountAction',));
                }

                if (0 === strpos($pathinfo, '/_errors/vendor/reviews')) {
                    // bizbids_vendor_reviews_upload
                    if ($pathinfo === '/_errors/vendor/reviews/upload') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendReviewsUploadAction',  '_route' => 'bizbids_vendor_reviews_upload',);
                    }

                    // bizbids_reviews_ajax_call_upload
                    if ($pathinfo === '/_errors/vendor/reviews/ajax') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::reviewsUploadAjaxAction',  '_route' => 'bizbids_reviews_ajax_call_upload',);
                    }

                }

                // bizbids_lead_dedcution_return_upload
                if ($pathinfo === '/_errors/vendorlead/deduction/return') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::manualLeadDeductionAction',  '_route' => 'bizbids_lead_dedcution_return_upload',);
                }

            }

            // bizbids_anonymous_reviews_upload
            if ($pathinfo === '/_errors/anonymous/reviews/upload') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::anonymousReviewsUploadAction',  '_route' => 'bizbids_anonymous_reviews_upload',);
            }

            // bizbids_job_post_sucessful
            if ($pathinfo === '/_errors/job/posted/successful') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::jobPostSucessfulAction',  '_route' => 'bizbids_job_post_sucessful',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:vendor.html.twig */
class __TwigTemplate_cc60efb9f78d734afa440e6975c2dfe6bd4af85c054f4af355738b2e82586d3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:vendor.html.twig", 1);
        $this->blocks = array(
            'chartscript' => array($this, 'block_chartscript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_chartscript($context, array $blocks = array())
    {
        // line 5
        echo "<script type=\"text/javascript\">
    ";
        // line 6
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart"]) ? $context["chart"] : null));
        echo "
</script>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:vendor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 6,  31 => 5,  28 => 4,  11 => 1,);
    }
}

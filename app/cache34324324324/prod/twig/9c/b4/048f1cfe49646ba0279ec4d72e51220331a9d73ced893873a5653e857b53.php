<?php

/* ::home.html.twig */
class __TwigTemplate_9cb4048f1cfe49646ba0279ec4d72e51220331a9d73ced893873a5653e857b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'googleanalatics' => array($this, 'block_googleanalatics'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html class=\"no-js\" lang=\"\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\"/>
        <title>Hire a Quality Service Professional at a Fair Price</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>

        <link rel=\"canonical\" href=\"http://www.businessbid.ae/\" />
        <link rel=\"apple-touch-icon\" href=\"apple-touch-icon.png\">
        <!-- Place favicon.ico in the root directory -->
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/home-style.css"), "html", null, true);
        echo "\">
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">

        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/modernizr-2.6.2.min.js"), "html", null, true);
        echo "\"></script>


        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
        ";
        // line 22
        echo "
  <style>
    #sticker {
    visibility:visible
      margin: 0 auto;
      text-shadow: 0 1px 1px rgba(0,0,0,.2);
    }
  </style>


        ";
        // line 32
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 36
        echo "    </head>
    ";
        // line 37
        ob_start();
        // line 38
        echo "    <body>
        ";
        // line 39
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 40
        echo "        ";
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 41
        echo "        ";
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 42
        echo "        ";
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 43
        echo "        <!--[if lt IE 8]>
            <p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Business Bid Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 57
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 58
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 64
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 65
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 67
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        ";
            // line 69
            echo "

       <li><a class='button-login naked' href=\"";
            // line 71
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
      <li class=\"my-account\"><img src=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>

                        </ul>
                    </div>
                    ";
        }
        // line 78
        echo "                </div>
            </div>
        </div>
        ";
        // line 81
        list($context["serviceCatLinks"], $context["reviewCatLinks"], $context["resourceCatLinks"], $context["resource"]) =         array("", "", "", "");
        // line 82
        echo "        ";
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 83
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 84
                echo "                ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 85
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : null) . "<li><a class=\"all-category\" href=\"") . $this->env->getExtension('routing')->getPath("bizbids_vendor_search")) . "\">See All Categories</a></li>");
                    // line 86
                    echo "
                    ";
                    // line 87
                    $context["reviewCatLinks"] = ((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : null) . "<li><a class=\"all-category\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4")) . "\">See All Reviews</a></li>");
                    // line 88
                    echo "                    ";
                    $context["resourceCatLinks"] = ((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : null) . "<li><a class=\"all-category\" href=\"http://www.businessbid.ae/resource-centre/home\">See All Articles</a></li>");
                    // line 89
                    echo "                ";
                } else {
                    // line 90
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : null) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Quotes</a></li>");
                    // line 91
                    echo "
                    ";
                    // line 92
                    $context["reviewCatLinks"] = ((((((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : null) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Reviews</a></li>");
                    // line 93
                    echo "                    ";
                    if (twig_test_empty($this->getAttribute($context["category"], "resourceLink", array()))) {
                        // line 94
                        echo "                        ";
                        $context["resource"] = "home-2";
                        // line 95
                        echo "                    ";
                    } else {
                        // line 96
                        echo "                        ";
                        $context["resource"] = $this->getAttribute($context["category"], "resourceLink", array());
                        // line 97
                        echo "                    ";
                    }
                    // line 98
                    echo "                    ";
                    $context["resourceCatLinks"] = ((((((((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : null) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"http://www.businessbid.ae/resource-centre/") . (isset($context["resource"]) ? $context["resource"] : null)) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Articles</a></li>");
                    // line 99
                    echo "                ";
                }
                // line 100
                echo "            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 101
            echo "        ";
        } else {
            // line 102
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 103
                echo "            ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    // line 104
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : null) . "<li><a class=\"all-category\" href=\"") . $this->env->getExtension('routing')->getPath("bizbids_vendor_search")) . "\">See All Categories</a></li>");
                    // line 105
                    echo "
                    ";
                    // line 106
                    $context["reviewCatLinks"] = ((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : null) . "<li><a class=\"all-reviews\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4")) . "\">See All Reviews</a></li>");
                    // line 107
                    echo "                    ";
                    $context["resourceCatLinks"] = ((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : null) . "<li><a class=\"all-articles\" href=\"http://www.businessbid.ae/resource-centre/home\">See All Articles</a></li>");
                    // line 108
                    echo "                ";
                } else {
                    // line 109
                    echo "                    ";
                    $context["serviceCatLinks"] = ((((((((isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : null) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Quotes</a></li>");
                    // line 110
                    echo "
                    ";
                    // line 111
                    $context["reviewCatLinks"] = ((((((((isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : null) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"") . $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("catid" => $this->getAttribute($context["category"], "id", array())))) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Reviews</a></li>");
                    // line 112
                    echo "                    ";
                    if (twig_test_empty($this->getAttribute($context["category"], "resourceLink", array()))) {
                        // line 113
                        echo "                        ";
                        $context["resource"] = "home-2";
                        // line 114
                        echo "                    ";
                    } else {
                        // line 115
                        echo "                        ";
                        $context["resource"] = $this->getAttribute($context["category"], "resourceLink", array());
                        // line 116
                        echo "                    ";
                    }
                    // line 117
                    echo "                    ";
                    $context["resourceCatLinks"] = ((((((((isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : null) . "<li><a class=\"") . $this->getAttribute($context["category"], "catClass", array())) . "\" href=\"http://www.businessbid.ae/resource-centre/") . (isset($context["resource"]) ? $context["resource"] : null)) . "\">") . $this->getAttribute($context["category"], "category", array())) . " Articles</a></li>");
                    // line 118
                    echo "                ";
                }
                // line 119
                echo "            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 120
            echo "        ";
        }
        // line 121
        echo "        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 128
        echo (isset($context["serviceCatLinks"]) ? $context["serviceCatLinks"] : null);
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 131
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 133
        echo (isset($context["reviewCatLinks"]) ? $context["reviewCatLinks"] : null);
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 138
        echo (isset($context["resourceCatLinks"]) ? $context["resourceCatLinks"] : null);
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 143
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 144
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 146
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 149
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 150
            echo "                <div class=\"register_menu1\">
     <img src=\"";
            // line 151
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 154
        echo "            </div>
        </div>
        <div class=\"one_area\">
            <div class=\"one_child_area\">
                <div class=\"inner_one_area_opacity\"></div>
                <div class=\"inner_one_area\">
                    <h3>Need to hire a quality service professional at a fair price? Ask us.</h3>
                    <h4>What service do you need? </h4>
                    <div class=\"home_top_sear_btn_n_plac\">
                        <form method=\"GET\" action=\"";
        // line 163
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
                        <input id='category' type=\"text\" name=\"category\" placeholder=\"eg. cleaner, photographer, handyman etc.\">
                        ";
        // line 166
        echo "                        <button type=\"submit\">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"two_area\">
            <div class=\"two_child_area\">
                <div class=\"inner_two_area_coain\">
                    <h4>COMPLETELY FREE TO USE</h4>
                </div>
                <div class=\"inner_two_area_praper\">
                    <h4>COMPARE COMPETITIVE QUOTES</h4>
                </div>
                <div class=\"inner_two_area_star\">
                    <h4>CHOOSE RATED PROFESSIONALS</h4>
                </div>
            </div>
        </div>
        <div class=\"stick_bg_test\">
            <div id=\"sticker\" class=\"sticky_top_menu_area\">
                <div class=\"sticky_top_menu_area_inner\">
                   <div class=\"sticky_heading\">
                       <h2>COMPARE UP TO 3 QUOTES INSTANTLY</h2>
                   </div>
                    <div class=\"sticky_table_menu\">
                        <select name=\"categories\">
                            ";
        // line 193
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 194
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 196
        echo "                        </select>
                    </div>
                    <div class=\"sticky_table_menu_btn\"><a href=\"#\" id=\"get_quotes\">GET QUOTES</a></div>
                </div>
            </div>
        </div>
        <div class=\"three_area\">
            <div class=\"three_child_area\">
                <h2>How does BusinessBid work?</h2>
                <div class=\"inner_three_area\">
                    <img src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/step_one.png"), "html", null, true);
        echo "\" alt=\"Step One\">
                    <h3>Tell us what you need?</h3>
                    <p>Fill out a quote request form.</p>
                </div>
                <div class=\"three_arrow\"><img src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo "\" alt=\"Arrow\"></div>
                <div class=\"inner_three_area\">
                    <img src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/step_two.png"), "html", null, true);
        echo "\" alt=\"Step Two\">
                    <h3>Receive multiple quotes</h3>
                    <p>Compare prices and reviews.</p>
                </div>
                <div class=\"three_arrow\"><img src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo "\" alt=\"Arrow\"></div>
                <div class=\"inner_three_area\">
                    <img src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/stpe_three.png"), "html", null, true);
        echo "\" alt=\"Step Three\">
                    <h3>Hire the best professional</h3>
                    <p>Once done, rate your experience</p>
                </div>
            </div>
        </div>
        <div class=\"fore_area\">
            <div class=\"fore_child_area\">
                <h2>Popular Categories</h2>
                <div class=\"inner_four_area\">
                    <ul>
                        <li class=\"coain\"><a href=\"/resource-centre/accounting-audit-firms/\">Accounting & Auditing Services</a></li>
                        <li class=\"interior\"><a href=\"/resource-centre/interior\">Interior Design & Fit Out</a></li>
                        <li class=\"signage\"><a href=\"/resource-centre/signage-graphics-companies/\">Signage and Signboards</a></li>
                        <li class=\"catering\"><a href=\"/resource-centre/catering-4/\">Catering Services</a></li>
                    </ul>
                </div>
                <div class=\"inner_four_area middle\">
                    <ul>
                        <li class=\"support\"><a href=\"/resource-centre/it-support\">IT Support</a></li>
                        <li class=\"landscaping\"><a href=\"/resource-centre/landscaping-gardening-companies/\">Landscaping and Gardens</a></li>
                        <li class=\"photography\"><a href=\"/resource-centre/photography\">Photography and Video</a></li>
                        <li class=\"offset\"><a href=\"/resource-centre/printing-press/\">Offset Printing</a></li>
                    </ul>
                </div>
                <div class=\"inner_four_area right\">
                    <ul>
                        <li class=\"residential\"><a href=\"/resource-centre/cleaning\">Residential Cleaning</a></li>
                        <li class=\"commercial\"><a href=\"/resource-centre/cleaning\">Commercial Cleaning</a></li>
                        <li class=\"maintenance\"><a href=\"/resource-centre/maintenance\">Maintenance</a></li>
                        <li class=\"control\"><a href=\"/resource-centre/pest-control-companies/\">Pest Control</a></li>
                    </ul>
                </div>
                <div class=\"fore_btn\">
                    <a href=\"";
        // line 252
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">VIEW ALL CATEGORIES</a>
                </div>
            </div>
        </div>
        <div class=\"five_area\">
            <div class=\"five_child_area\">
                <div class=\"inner_five_text\">
                    <h1>HIRE RATED PROFESSIONALS</h1>
                    <p>BusinessBid screens all service professionals to ensure they are licensed and certified to carry out their activity and have a good reputation in their industry. In addition, customers who have previously hired a vendor are able to rate the overall experience. As a BusinessBid user you have access to thousands of vendor reviews and ratings you can view before hiring a service professional.</p>               <p>Whether you need access to <a href=\"http://www.businessbid.ae/resource-centre/accounting-audit-firms/\">audit firms</a>, <a href=\"http://www.businessbid.ae/resource-centre/interior/\">interior design companies</a>, <a href=\"http://www.businessbid.ae/resource-centre/catering-4/\">catering businesses</a>, or <a href=\"http://www.businessbid.ae/resource-centre/it-support/\">IT services</a>, we can connect you to screened and approved professionals who you can trust to get things done.</p>
                    <div class=\"five_btn\">
                        <a href=\"";
        // line 262
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Check Reviews & Ratings</a>
                    </div>
                </div>
                <div class=\"inner_five_img\">
                    <img src=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/window.png"), "html", null, true);
        echo "\" alt=\"Browser Window\">
                </div>
            </div>
        </div>
        <div class=\"six_area\">
            <div class=\"six_child_area\">
                <div class=\"inner_six_area_opaciry\"></div>
                <div class=\"inner_six_area\">
                    <h2>QUICK QUOTES THAT SAVE YOU MONEY</h2>
                    <p>All you need to do is fill out a simple form telling us what you need and we will have the most suitable professionals quote you for your project. You will usually have 3 service professionals contact you so you can compare quotes and ensure you get a great deal. Typically vendors will contact you within 30 minutes or less!</p>
                    <p>At BusinessBid we pride ourselves in being able to provide customers with multiple quotes at fast turnaround times.</p>
                    <form name=\"search\" method=\"GET\" action=\"";
        // line 277
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
                    <div class=\"six_tabil\">
                        <select name=\"category\">
                            ";
        // line 280
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoriesList"]) ? $context["categoriesList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 281
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 283
        echo "                        </select>
                    </div>
                    <input type=\"submit\" class=\"six_btn\" name=\"submit\" value=\"START FREE QUOTES\">
                    </form>
                </div>
            </div>
        </div>
        <div class=\"seven_area\">
            <div class=\"seven_child_area\">
                <h2>ARE YOU A SERVICE PROFESSIONAL?</h2>
                <p>Learn more on how joining BusinessBid can benefit your business.</p>
                <div class=\"seven_btn\"><a href=\"";
        // line 294
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">FIND OUT MORE</a></div>
            </div>
        </div>
        <div class=\"main_footer_area\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>

                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 316
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 317
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 318
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 319
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 320
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 326
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 327
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 335
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 337
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 338
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 339
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 345
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 347
        echo "                            <li><a href=\"/resource-centre/home/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 348
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 356
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 357
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 363
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 400
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 402
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>
               <div class=\"copy_rightt\">
                <p>© <span>2015 BusinessBid DMCC</span> </p>
            </div>
        </div>

        ";
        // line 412
        echo "        ";
        // line 438
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
        <script>
        \$(window).load(function(){
          \$(\"#sticker\").sticky({ topSpacing: 0, center:true, className:\"hey\" });
        });
      </script>
        <script type=\"text/javascript\">
        \$(document).ready(function(){
            \$('select[name=\"categories\"]').bind('change',function () {
            ";
        // line 447
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 448
            echo "                var cat = \$(this).val(), urlPath = \"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => "cid"));
            echo "\";
            ";
        } else {
            // line 450
            echo "                var cat = \$(this).val(), urlPath = \"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => "cid"));
            echo "\";
            ";
        }
        // line 452
        echo "                urlPath = urlPath.replace(\"cid\", cat);
                \$(\"#get_quotes\").attr('href', urlPath);
            });

            ";
        // line 456
        if (array_key_exists("keyword", $context)) {
            // line 457
            echo "            window.onload = function(){
                document.getElementById(\"submitButton\").click();
            }
            ";
        }
        // line 461
        echo "            var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

            \$('#category').autocomplete({
                source: function( request, response ) {
                \$.ajax({
                    url : realpath+\"ajax-info.php\",
                    dataType: \"json\",
                    data: {
                       name_startsWith: request.term,
                       type: 'category'
                    },
                    success: function( data ) {
                        response( \$.map( data, function( item ) {
                            return {
                                label: item,
                                value: item
                            }
                        }));
                    },
                    select: function(event, ui) {
                        alert( \$(event.target).val() );
                        console.log( \$(event).val() );
                    }
                });
                },autoFocus: true,minLength: 2
            });
        });
        </script>
        <div id=\"fb-root\"></div>
        <script type=\"text/javascript\">
        window.fbAsyncInit = function() {
            FB.init({
            appId      : '754756437905943', // replace your app id here
            status     : true,
            cookie     : true,
            xfbml      : true
            });
        };
        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = \"//connect.facebook.net/en_US/all.js\";
            ref.parentNode.insertBefore(js, ref);
        }(document));

        function FBLogin(){
            FB.login(function(response){
                if(response.authResponse){
                    window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
                }
            }, {scope: 'email,user_likes'});
        }
        </script>
        <script src=\"";
        // line 515
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/jquery.sticky.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 516
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 517
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>

        <!-- Pure Chat Snippet -->
        <script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();
        </script>
        <!-- End Pure Chat Snippet -->
    </body>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 525
        echo "</html>
";
    }

    // line 32
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 33
        echo "        <!-- Google Analytics -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>
        ";
    }

    public function getTemplateName()
    {
        return "::home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  914 => 33,  911 => 32,  906 => 525,  895 => 517,  891 => 516,  887 => 515,  831 => 461,  825 => 457,  823 => 456,  817 => 452,  811 => 450,  805 => 448,  803 => 447,  790 => 438,  788 => 412,  776 => 402,  771 => 400,  731 => 363,  723 => 358,  719 => 357,  715 => 356,  704 => 348,  701 => 347,  697 => 345,  688 => 339,  684 => 338,  680 => 337,  676 => 336,  672 => 335,  661 => 327,  657 => 326,  648 => 320,  644 => 319,  640 => 318,  636 => 317,  632 => 316,  607 => 294,  594 => 283,  583 => 281,  579 => 280,  573 => 277,  559 => 266,  552 => 262,  539 => 252,  502 => 218,  497 => 216,  490 => 212,  485 => 210,  478 => 206,  466 => 196,  455 => 194,  451 => 193,  422 => 166,  417 => 163,  406 => 154,  392 => 151,  389 => 150,  387 => 149,  381 => 146,  377 => 144,  375 => 143,  367 => 138,  359 => 133,  354 => 131,  348 => 128,  343 => 126,  339 => 125,  333 => 121,  330 => 120,  316 => 119,  313 => 118,  310 => 117,  307 => 116,  304 => 115,  301 => 114,  298 => 113,  295 => 112,  293 => 111,  290 => 110,  287 => 109,  284 => 108,  281 => 107,  279 => 106,  276 => 105,  273 => 104,  270 => 103,  252 => 102,  249 => 101,  235 => 100,  232 => 99,  229 => 98,  226 => 97,  223 => 96,  220 => 95,  217 => 94,  214 => 93,  212 => 92,  209 => 91,  206 => 90,  203 => 89,  200 => 88,  198 => 87,  195 => 86,  192 => 85,  189 => 84,  171 => 83,  168 => 82,  166 => 81,  161 => 78,  144 => 72,  140 => 71,  136 => 69,  132 => 67,  128 => 65,  126 => 64,  120 => 61,  114 => 60,  110 => 58,  108 => 57,  97 => 51,  87 => 43,  84 => 42,  81 => 41,  78 => 40,  76 => 39,  73 => 38,  71 => 37,  68 => 36,  66 => 32,  54 => 22,  47 => 17,  42 => 15,  38 => 14,  34 => 13,  20 => 1,);
    }
}

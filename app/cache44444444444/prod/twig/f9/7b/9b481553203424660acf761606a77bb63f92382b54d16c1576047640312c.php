<?php

/* BBidsBBidsHomeBundle:User:leads_paymentform.html.twig */
class __TwigTemplate_f97b9b481553203424660acf761606a77bb63f92382b54d16c1576047640312c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "
\t<style type=\"text/css\">
\t.camouflage{
\t\tdisplay: none;
\t}
\t.exposed{
\t\tdisplay: block;
\t}
\t.errorinput{ content: attr(title); color: red; margin-left: 0.6rem; }
\t</style>
\t<script>
\t

\t\$(document).ready(function(){
\t
\$('.bbspop').popover();
\$('.bbspop').popover({ trigger: \"hover\" });
    \$('[data-toggle=\"popover\"]').popover();
    \$('#form_ccExpDate_month,#form_ccExpDate_year').width(60);
    \$('#form_ccSubmit').hide();
    \$(\"#form_ccNumber\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCNum').html('Enter card number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorCCNum').html('Numbers only !');
        } else {
            \$('small#errorCCNum').html('');
            var result = getCreditCardType(ccVal);
            if(result == 'unknown'){
                \$('small#errorCCNum').html('Only Master and Visa card allowed');
            } else {
                \$('small#errorCCNum').html('');
            }

            var result = isValidCreditCard(type, ccnum);
            if(result) {
                \$('small#errorCCNum').html('');
            } else {
                \$('small#errorCCNum').html('Invalid card number');
            }
        }
    })
    \$(\"#form_ccName\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCName').html('Enter card holder\\'s name.');
        } else if(!isNaN(ccVal)) {
            \$('small#errorCCName').html('Numbers not allowed !');
        } else {
            \$('small#errorCCName').html('');
        }
    })
    \$(\"#form_ccCVV\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCVV').html('Enter cvv number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorCCVV').html('Numbers only!');
        } else if (ccVal.length > 3){
            \$('small#errorCCVV').html('Only 3 digits allowed.');
        } else {
            \$('small#errorCCVV').html('');
        }
    })
    \$(\"input[name='payType'],div#editAdress\").click(function (){
        var payType = \$(\"input[name='payType']:checked\").val();
        if(payType == 'check pickup') {
            \$( 'div#checkFillForm' ).toggleClass( \"camouflage\" );
            \$( 'div#checkAddShow' ).toggleClass( \"camouflage\" );
        }
        else if(payType == 'cash pickup') {
            \$( 'div#cashFillForm' ).toggleClass( \"camouflage\" );
            \$( 'div#cashAddShow' ).toggleClass( \"camouflage\" );
        }
    });
    \$('.btn-success').click(function () {
        var payType = \$(\"input[name='payType']:checked\").val();//\$(\"input:radio[name='payType']\").val();
        alert('Your '+payType+' payment is being processed. Please click OK and do not leave this page');
        if(payType == 'cc') {
            \$( \"#ccForm\" ).submit();
        } else if (payType == 'paypal') {
             \$( \"#payPalForm\" ).submit();
        } else if (payType == 'check pickup') {
            \$( \"#checkForm\" ).submit();
        } else if (payType == 'cash pickup') {
            \$( \"#cashForm\" ).submit();
        }
    })
    \$('#form_ccExpDate_month').change(function () {
        var ccmonth = \$(this).val();
        if(ccmonth == '') {
            \$('small#errorCCExp').html('Select expiry month.');
        } else {
            \$('small#errorCCExp').html('');
        }
    })
    \$('#form_ccExpDate_year').change(function () {
        var ccmonth = \$(this).val();
        if(ccmonth == '') {
            \$('small#errorCCExp').html('Select expiry year.');
        } else {
            \$('small#errorCCExp').html('');
        }
    })

    \$(\"#form_contactnumber\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorConNum').html('Enter Mobile Contact Number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorConNum').html('Numbers only!');
        } else {
            \$('small#errorConNum').html('');
        }
    })
});
function getCreditCardType(accountNumber)
{

  //start without knowing the credit card type
  var result = \"unknown\";

  //first check for MasterCard

  if (/^5[1-5]/.test(accountNumber))
  {
    result = \"mastercard\";
  }

  //then check for Visa
  else if (/^4/.test(accountNumber))
  {
    result = \"visa\";
  }

  //then check for AmEx
  /*else if (/^3[47]/.test(accountNumber))
  {
    result = \"amex\";
  }*/

  return result;
}

function isValidCreditCard(type, ccnum)
{
/* Visa: length 16, prefix 4, dashes optional.
Mastercard: length 16, prefix 51-55, dashes optional.
Discover: length 16, prefix 6011, dashes optional.
American Express: length 15, prefix 34 or 37.
Diners: length 14, prefix 30, 36, or 38. */

  var re = new Regex({ \"visa\": \"/^4\\d{3}-?\\d{4}-?\\d{4}-?\\d\",
                       \"mc\": \"/^5[1-5]\\d{2}-?\\d{4}-?\\d{4}-?\\d{4}\$/\",
                       \"disc\": \"/^6011-?\\d{4}-?\\d{4}-?\\d{4}\$/\",
                       \"amex\": \"/^3[47]\\d{13}\$/\",
                       \"diners\": \"/^3[068]\\d{12}\$/\"}[type.toLowerCase()])

   if (!re.test(ccnum)) return false;
   // Remove all dashes for the checksum checks to eliminate negative numbers
   ccnum = ccnum.split(\"-\").join(\"\");
   // Checksum (\"Mod 10\")
   // Add even digits in even length strings or odd digits in odd length strings.
   var checksum = 0;
   for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
      checksum += parseInt(ccnum.charAt(i-1));
   }
   // Analyze odd digits in even length strings or even digits in odd length strings.
   for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
      var digit = parseInt(ccnum.charAt(i-1)) * 2;
      if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
   }
   if ((checksum % 10) == 0) return true; else return false;
}
\t
\t</script>
\t
<div class=\"container\" >
<div>
    ";
        // line 184
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 185
            echo "
    <div class=\"alert alert-danger\">";
            // line 186
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 189
        echo "
    ";
        // line 190
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 191
            echo "
    <div class=\"alert alert-success\">";
            // line 192
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 195
        echo "
    ";
        // line 196
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 197
            echo "
    <div class=\"alert alert-success\">";
            // line 198
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 201
        echo "</div>

<div class=\"col-sm-12\">
\t<div class=\"package-details\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" id=\"selectpackagedetails\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Package Details </td>
\t\t\t\t\t<td width=\"197\">Price</td>
\t\t\t\t\t<td width=\"189\">Discount</td>
\t\t\t\t\t<td width=\"189\">Total</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 215
        $context["OTC"] = 0;
        // line 216
        echo "\t\t\t\t";
        $context["grandTotal"] = 0;
        // line 217
        echo "\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["purchasingLeads"]) ? $context["purchasingLeads"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lead"]) {
            // line 218
            echo "\t\t\t\t\t";
            $context["priceArray"] = twig_split_filter($this->env, $this->getAttribute($context["lead"], "price", array()), " - ");
            // line 219
            echo "
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">";
            // line 221
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "plan", array()), "html", null, true);
            echo " Package: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "category", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t";
            // line 222
            $context["multiplier"] = $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
            // line 223
            echo "\t\t\t\t\t<td width=\"197\">AED ";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">
\t\t\t\t\t\t";
            // line 225
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                // line 226
                echo "\t\t\t\t\t\t\t";
                $context["selPlan"] = $this->getAttribute((isset($context["leadsPack"]) ? $context["leadsPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
                // line 227
                echo "\t\t\t\t\t\t\tAED ";
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")) - ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array"))), "html", null, true);
                echo "
\t\t\t\t\t\t";
            } else {
                // line 229
                echo "\t\t\t\t\t\t\tAED 0
\t\t\t\t\t\t";
            }
            // line 231
            echo "\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 232
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")));
            } else {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)));
            }
            echo "</td>

\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lead'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 236
        echo "\t\t\t\t";
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            // line 237
            echo "\t\t\t\t\t";
            $context["OTC"] = (isset($context["registerFee"]) ? $context["registerFee"] : null);
            // line 238
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Account Setup Fee <span><a tabindex=\"0\" class=\"popover-icon\" role=\"button\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"bottom\" data-trigger=\"hover\" title=\"The Account Setup fee is one time charge and includes:\" data-content=\"<ul><li>Creation of dedicated webpage with a full business profie.</li> <li>Access to 24/7 online dashboard with reporting features.</li> <li>Compilation and publication of 5 recent customer reviews.</li></ul>\"></a></span>
\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"197\">AED ";
            // line 241
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">AED 0</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 243
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t</tbody>
\t\t\t\t";
        }
        // line 247
        echo "\t\t\t\t<tfoot>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Grand Total</td>
\t\t\t\t\t<td width=\"197\"></td>
\t\t\t\t\t<td width=\"189\"></td>
\t\t\t\t\t<td width=\"189\"><strong>AED ";
        // line 252
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</strong></td>
\t\t\t\t</tr>
\t\t\t\t</tfoot>
\t\t</table>
\t</div>
\t<div class=\"paymentmethod\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\">Payment Method</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\"><strong>How would you like to pay <span class=\"payment_value\">";
        // line 266
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</span> AED ?</td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t</table>
\t\t<div class=\"credit-card-option col-sm-12 position-in-opt left-clear\" style=\"width:100%; float:left;\">
\t\t\t<div class=\"col-sm-12 position-in-opt\"><input type=\"radio\" name=\"payType\" checked=\"checked\" value=\"cc\" />Credit Card</div>

\t\t\t<div class=\"col-sm-9\">

        <script src=\"https://checkout.com/cdn/js/checkout.js\"></script>

            <form class=\"payment-form\">
                <script>
                  var publickey = 'pk_04d5b10a-845e-4267-866b-7a16910dd048'; //PublicKey provided by Checkout

                  Checkout.render({
                      debugMode: true,
                      namespace: 'CheckoutIntegration',
                      publicKey: publickey,
                      paymentToken: \"";
        // line 285
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\",
                      customerEmail: '";
        // line 286
        echo twig_escape_filter($this->env, (isset($context["usermail"]) ? $context["usermail"] : null), "html", null, true);
        echo "',
                      customerName: '";
        // line 287
        echo twig_escape_filter($this->env, (isset($context["custome"]) ? $context["custome"] : null), "html", null, true);
        echo "',
                      paymentMode: 'card',
                      value: ";
        // line 289
        echo twig_escape_filter($this->env, (((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)) * 100), "html", null, true);
        echo ",
                      currency: 'AED',
                      forceMobileRedirect: true,
                      payButtonSelector: '#payButtonId',
                      widgetContainerSelector: '.payment-form',
                      useCurrencyCode: false,
                      
                       styling: {
                            widgetColor: 'rgba(204, 204, 204, 0.16)',
                            themeColor: '',
                            buttonColor: '',
                            buttonLabelColor: '',
                            logoUrl: '',
                            formButtonColor: '',
                            formButtonLabelColor: '',
                            iconColor: '',
                            overlayShade: 'dark',
                            overlayOpacity: 0.8,
                            buttonLabel: 'Visa Checkout'
                        },

                 
                      cardCharged: function (event) {
\t\t\t\t\t  console.log(event);
                          document.getElementById('cko-cc-paymenToken').value = event.data.paymentToken;
                          document.getElementById('payment-tokenform').submit();
                      }

                  });
\t\t\t\t  
\t\t\t\t  
                </script>

            </form>
\t\t\t<form name=\"payment-tokenform\" method=\"post\" id=\"payment-tokenform\" action=\"http://www.businessbid.ae/vendor/home\">
                <input type=\"hidden\" id =\"cko-cc-paymenToken\" name =\"cko-cc-paymenToken\" />
            </form>
\t\t\t</div>
\t\t\t<div class=\"col-sm-3\"><img src=\"";
        // line 327
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/payment-icon.jpg"), "html", null, true);
        echo "\" /></div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"check pickup\" />Cheque Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"checkAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 337
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"checkFillForm\">
\t\t\t\t";
        // line 343
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_start', array("attr" => array("id" => "checkForm")));
        echo "
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\"><label>Company Name:</label></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 348
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 351
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 352
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 355
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 356
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 359
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 360
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 363
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 364
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 367
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 368
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 371
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 372
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 375
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 376
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr class=\"camouflage\">
\t\t\t\t\t\t\t<td width=\"20%\"></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 380
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "Submit", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t\t";
        // line 384
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_end');
        echo "
\t\t\t</div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"cash pickup\" />Cash Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"cashAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 395
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"cashFillForm\">
                ";
        // line 401
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_start', array("attr" => array("id" => "cashForm")));
        echo "
                <table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
                    <tbody>
                        <tr>
                            <td width=\"20%\"><label>Company Name:</label></td>
                            <td width=\"80%\">";
        // line 406
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 409
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
                            <td width=\"80%\">";
        // line 410
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 413
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
                            <td width=\"80%\">";
        // line 414
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 417
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 418
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 421
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 422
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 425
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 426
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr class=\"camouflage\">
                            <td width=\"20%\"></td>
                            <td width=\"80%\">";
        // line 430
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "Submit", array()), 'widget');
        echo "</td>
                        </tr>
                    </tbody>
                </table>
                ";
        // line 434
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_end');
        echo "
            </div>
\t\t</div>

\t\t";
        // line 453
        echo "\t</div>
</div>
\t<div class=\"row text-right update-account\">
\t\t<a href=\"#\" class=\"btn btn-success\"> Place Order </a>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  687 => 453,  680 => 434,  673 => 430,  666 => 426,  662 => 425,  656 => 422,  652 => 421,  646 => 418,  642 => 417,  636 => 414,  632 => 413,  626 => 410,  622 => 409,  616 => 406,  608 => 401,  599 => 395,  585 => 384,  578 => 380,  571 => 376,  567 => 375,  561 => 372,  557 => 371,  551 => 368,  547 => 367,  541 => 364,  537 => 363,  531 => 360,  527 => 359,  521 => 356,  517 => 355,  511 => 352,  507 => 351,  501 => 348,  493 => 343,  484 => 337,  471 => 327,  430 => 289,  425 => 287,  421 => 286,  417 => 285,  395 => 266,  378 => 252,  371 => 247,  364 => 243,  359 => 241,  354 => 238,  351 => 237,  348 => 236,  332 => 232,  329 => 231,  325 => 229,  319 => 227,  316 => 226,  314 => 225,  308 => 223,  306 => 222,  300 => 221,  296 => 219,  293 => 218,  288 => 217,  285 => 216,  283 => 215,  267 => 201,  258 => 198,  255 => 197,  251 => 196,  248 => 195,  239 => 192,  236 => 191,  232 => 190,  229 => 189,  220 => 186,  217 => 185,  213 => 184,  31 => 4,  28 => 3,  11 => 1,);
    }
}

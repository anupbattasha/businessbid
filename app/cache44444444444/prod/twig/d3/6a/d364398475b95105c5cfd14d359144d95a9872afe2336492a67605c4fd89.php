<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">


    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 162
        $this->displayBlock('customcss', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('javascripts', $context, $blocks);
        // line 309
        echo "
";
        // line 310
        $this->displayBlock('customjs', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('jquery', $context, $blocks);
        // line 317
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 323
        echo "</head>
";
        // line 324
        ob_start();
        // line 325
        echo "<body>

";
        // line 327
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 328
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 329
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 330
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 331
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 342
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 343
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 345
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 346
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 349
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 350
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 352
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 353
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 359
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 366
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 369
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 372
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 374
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 379
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 384
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 385
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 387
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 390
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 391
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 395
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 402
        $this->displayBlock('maincontent', $context, $blocks);
        // line 633
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 636
        $this->displayBlock('footer', $context, $blocks);
        // line 749
        echo "    <!-- footer block ends -->
</div>";
        // line 752
        $this->displayBlock('footerScript', $context, $blocks);
        // line 756
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 763
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_0e16be1bd7f72b9c2876f78dea84107543fc6e50a2600dc8cecc579b47e8a72c = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_160b23dc14804641ee8a0de2be318f4d351565d629073011eadbd2df9ad6357c = "http://www.businessbid.ae/contactus") && ('' === $__internal_160b23dc14804641ee8a0de2be318f4d351565d629073011eadbd2df9ad6357c || 0 === strpos($__internal_0e16be1bd7f72b9c2876f78dea84107543fc6e50a2600dc8cecc579b47e8a72c, $__internal_160b23dc14804641ee8a0de2be318f4d351565d629073011eadbd2df9ad6357c)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_5a8634cf1526239eda766b2268fdcdf4707452004e77f9f186b9ad928dda1acb = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_190b6b3c086f7ced33ae9fcee367cdeab15c3f7abe52e349e922779d0e2fed42 = "http://www.businessbid.ae/node3") && ('' === $__internal_190b6b3c086f7ced33ae9fcee367cdeab15c3f7abe52e349e922779d0e2fed42 || 0 === strpos($__internal_5a8634cf1526239eda766b2268fdcdf4707452004e77f9f186b9ad928dda1acb, $__internal_190b6b3c086f7ced33ae9fcee367cdeab15c3f7abe52e349e922779d0e2fed42)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_d76985f6b3ff3597d1633647b14d97bcad6cf73dd8047bb8fd8824ecf956e9dd = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_06c626eba86a9cbfd41d9781c731e7294cb0e89289ac7ee299f7e9d8663c340b = "http://www.businessbid.ae/review/login") && ('' === $__internal_06c626eba86a9cbfd41d9781c731e7294cb0e89289ac7ee299f7e9d8663c340b || 0 === strpos($__internal_d76985f6b3ff3597d1633647b14d97bcad6cf73dd8047bb8fd8824ecf956e9dd, $__internal_06c626eba86a9cbfd41d9781c731e7294cb0e89289ac7ee299f7e9d8663c340b)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_ebef3d66c1c7b32ac854c933e636aa3bb194da0cd5011a1140313237aa2af15b = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a7c130fce21039e4235287b9cd55874cbe66556cab5e3eb222dce7a3f8ac596d = "http://www.businessbid.ae/node1") && ('' === $__internal_a7c130fce21039e4235287b9cd55874cbe66556cab5e3eb222dce7a3f8ac596d || 0 === strpos($__internal_ebef3d66c1c7b32ac854c933e636aa3bb194da0cd5011a1140313237aa2af15b, $__internal_a7c130fce21039e4235287b9cd55874cbe66556cab5e3eb222dce7a3f8ac596d)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_a8fd30ff28b183b20bfc35ab6be536174ccf0592ba819589285e67cf6abe7a31 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_49d8aded2b680d5c3bf7175a884766d76003be6aa54231abb35954cfec46636b = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_49d8aded2b680d5c3bf7175a884766d76003be6aa54231abb35954cfec46636b || 0 === strpos($__internal_a8fd30ff28b183b20bfc35ab6be536174ccf0592ba819589285e67cf6abe7a31, $__internal_49d8aded2b680d5c3bf7175a884766d76003be6aa54231abb35954cfec46636b)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_11e98293bc874e3307fff604104261848f7a0774576c4ff7c0f272f18cdb7409 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c47568408ad7672eb2c060f5096eba104e37b869dc0f1e3e612c7841cb5b0632 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_c47568408ad7672eb2c060f5096eba104e37b869dc0f1e3e612c7841cb5b0632 || 0 === strpos($__internal_11e98293bc874e3307fff604104261848f7a0774576c4ff7c0f272f18cdb7409, $__internal_c47568408ad7672eb2c060f5096eba104e37b869dc0f1e3e612c7841cb5b0632)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_4eb395b695a843c783b4ee7cbea7ad7d11e56c7dfa40f12f2b4eb0fbe06b835c = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b73e5b2239b0ebfe5eed79ad4d4b375ab00524d3316f55998d643da424082d6e = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_b73e5b2239b0ebfe5eed79ad4d4b375ab00524d3316f55998d643da424082d6e || 0 === strpos($__internal_4eb395b695a843c783b4ee7cbea7ad7d11e56c7dfa40f12f2b4eb0fbe06b835c, $__internal_b73e5b2239b0ebfe5eed79ad4d4b375ab00524d3316f55998d643da424082d6e)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_eab363ed18af26a352bfd25e1d50da1c71d323a924d288472f6d676bd863f2ae = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_afe6af2ac7d860a4f653d3f9d93dc851279fb019fb71a2a75abdecf650de1621 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_afe6af2ac7d860a4f653d3f9d93dc851279fb019fb71a2a75abdecf650de1621 || 0 === strpos($__internal_eab363ed18af26a352bfd25e1d50da1c71d323a924d288472f6d676bd863f2ae, $__internal_afe6af2ac7d860a4f653d3f9d93dc851279fb019fb71a2a75abdecf650de1621)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_5f2c0ec8067897144f515d317e795288e6d281e220d5a74c0b1a89fe4af9b2c3 = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f944eaea5f45f463bdfed4b0d5c6f795d80a92ae395cd51df6e8b39b5acfd221 = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_f944eaea5f45f463bdfed4b0d5c6f795d80a92ae395cd51df6e8b39b5acfd221 || 0 === strpos($__internal_5f2c0ec8067897144f515d317e795288e6d281e220d5a74c0b1a89fe4af9b2c3, $__internal_f944eaea5f45f463bdfed4b0d5c6f795d80a92ae395cd51df6e8b39b5acfd221)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_3c0a5e06a068d199fd6878ab504d0e22972243b28842b207c239e1cbb6787099 = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3a4d0ca07d77679a2213941f017d9e528e295286c92c07be213e0e2b56824c74 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_3a4d0ca07d77679a2213941f017d9e528e295286c92c07be213e0e2b56824c74 || 0 === strpos($__internal_3c0a5e06a068d199fd6878ab504d0e22972243b28842b207c239e1cbb6787099, $__internal_3a4d0ca07d77679a2213941f017d9e528e295286c92c07be213e0e2b56824c74)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_7766dabd001b87c7ed6fd2f03f3b190d51a35524fab3772db9de4ec1b2fe7f76 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2b591a1dcfc1b0567cbd7dec188e1d1abaeecd224c523a7b2543317d46d1914a = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_2b591a1dcfc1b0567cbd7dec188e1d1abaeecd224c523a7b2543317d46d1914a || 0 === strpos($__internal_7766dabd001b87c7ed6fd2f03f3b190d51a35524fab3772db9de4ec1b2fe7f76, $__internal_2b591a1dcfc1b0567cbd7dec188e1d1abaeecd224c523a7b2543317d46d1914a)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_0dc14a078507799c1c76ec88ba672ed8f9044833e3cf3de4fa2a7110958e7bae = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_caf837614f3dde5e1a24650194168c656d8ef2d9c71c21ea37839a94c407d2bc = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_caf837614f3dde5e1a24650194168c656d8ef2d9c71c21ea37839a94c407d2bc || 0 === strpos($__internal_0dc14a078507799c1c76ec88ba672ed8f9044833e3cf3de4fa2a7110958e7bae, $__internal_caf837614f3dde5e1a24650194168c656d8ef2d9c71c21ea37839a94c407d2bc)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_ab85f9406829dfeab92cb402dc29238fdb98acf4ed82fd60eb0b8e9aaf1c38b9 = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2c0e74ba27b1b8e9303698c967bb78daab052103cb2e3d1487186a054d2d9807 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_2c0e74ba27b1b8e9303698c967bb78daab052103cb2e3d1487186a054d2d9807 || 0 === strpos($__internal_ab85f9406829dfeab92cb402dc29238fdb98acf4ed82fd60eb0b8e9aaf1c38b9, $__internal_2c0e74ba27b1b8e9303698c967bb78daab052103cb2e3d1487186a054d2d9807)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_bc4e73c86637bd62d78b02bff41704ed7865bd20beb1475013d3d334f44e7801 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b1c9516282580595259f83f689730f8cbcb5ad9e496f08ba35859ca1b3f5235c = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_b1c9516282580595259f83f689730f8cbcb5ad9e496f08ba35859ca1b3f5235c || 0 === strpos($__internal_bc4e73c86637bd62d78b02bff41704ed7865bd20beb1475013d3d334f44e7801, $__internal_b1c9516282580595259f83f689730f8cbcb5ad9e496f08ba35859ca1b3f5235c)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_2b7328b66f569d44e0bf4c902f7adb64214f602d02bbfefaa08493e4732cdc0d = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c68f190cace69b733a71b228a77e882c1600beb9888228d7d5e6c46e56109fdc = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_c68f190cace69b733a71b228a77e882c1600beb9888228d7d5e6c46e56109fdc || 0 === strpos($__internal_2b7328b66f569d44e0bf4c902f7adb64214f602d02bbfefaa08493e4732cdc0d, $__internal_c68f190cace69b733a71b228a77e882c1600beb9888228d7d5e6c46e56109fdc)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_8f3966d76e964ca9f52429d898395b99f104465ae3c1a65a15859fbdb6454df8 = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_92619153409511d1d0358c6adfb5c0c05f6517c50450573386e0c53df9bc3775 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_92619153409511d1d0358c6adfb5c0c05f6517c50450573386e0c53df9bc3775 || 0 === strpos($__internal_8f3966d76e964ca9f52429d898395b99f104465ae3c1a65a15859fbdb6454df8, $__internal_92619153409511d1d0358c6adfb5c0c05f6517c50450573386e0c53df9bc3775)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_b53bcd77e913ff6bb6808a77e3935b8c8128badc79ae7d5010796b0de2170123 = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_eef15337e282fb31d4dfe4f8ebd2b34cb0c43f391d701749c359ef8b5e873a6a = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_eef15337e282fb31d4dfe4f8ebd2b34cb0c43f391d701749c359ef8b5e873a6a || 0 === strpos($__internal_b53bcd77e913ff6bb6808a77e3935b8c8128badc79ae7d5010796b0de2170123, $__internal_eef15337e282fb31d4dfe4f8ebd2b34cb0c43f391d701749c359ef8b5e873a6a)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_2decc26d0d8e4f99396a3c60c278d32facacb1c72f221779d024e86d6026be4c = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_7ba5c93bdd8e508639f2b1af9ce0404bb03e6e90d82e37eccec6f374ee14519a = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_7ba5c93bdd8e508639f2b1af9ce0404bb03e6e90d82e37eccec6f374ee14519a || 0 === strpos($__internal_2decc26d0d8e4f99396a3c60c278d32facacb1c72f221779d024e86d6026be4c, $__internal_7ba5c93bdd8e508639f2b1af9ce0404bb03e6e90d82e37eccec6f374ee14519a)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_165198fe93cd25a33095660c3c0e56564d8194da88ab01136ad802a6737a5903 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_680cb60c30e4e25b2d94a9f126746f545dce14b2a8fe1a0ee51ca6ec149b1d06 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_680cb60c30e4e25b2d94a9f126746f545dce14b2a8fe1a0ee51ca6ec149b1d06 || 0 === strpos($__internal_165198fe93cd25a33095660c3c0e56564d8194da88ab01136ad802a6737a5903, $__internal_680cb60c30e4e25b2d94a9f126746f545dce14b2a8fe1a0ee51ca6ec149b1d06)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_d777307edc7ceb0712b1a9ce303de7440a8334f74cc02347c81f1c042051946f = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ff8ab60819d61c2a95aea36bb02dd02ae07fd3355b889fee195bdb9ef0bf60f2 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_ff8ab60819d61c2a95aea36bb02dd02ae07fd3355b889fee195bdb9ef0bf60f2 || 0 === strpos($__internal_d777307edc7ceb0712b1a9ce303de7440a8334f74cc02347c81f1c042051946f, $__internal_ff8ab60819d61c2a95aea36bb02dd02ae07fd3355b889fee195bdb9ef0bf60f2)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_b83092e037f4c60c1a84da65d55f6917a92b279cd6bff5c6fdca6e6476efe433 = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_24b7082d33c87ef3de779d0ca70a4dfe6c2390300bc371b5cc0855ca07caaf6b = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_24b7082d33c87ef3de779d0ca70a4dfe6c2390300bc371b5cc0855ca07caaf6b || 0 === strpos($__internal_b83092e037f4c60c1a84da65d55f6917a92b279cd6bff5c6fdca6e6476efe433, $__internal_24b7082d33c87ef3de779d0ca70a4dfe6c2390300bc371b5cc0855ca07caaf6b)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_3f22ae8e639280b9c51e92c9e5255d131a6b84e59be806b8041e4a3d4846a47a = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2ab89252a4604eaef79289b830d5412442dd4742be08b01b87ef437f5924cec9 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_2ab89252a4604eaef79289b830d5412442dd4742be08b01b87ef437f5924cec9 || 0 === strpos($__internal_3f22ae8e639280b9c51e92c9e5255d131a6b84e59be806b8041e4a3d4846a47a, $__internal_2ab89252a4604eaef79289b830d5412442dd4742be08b01b87ef437f5924cec9)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_5c121d24a6570a3b061494dd3b27b3fc62a09636eb7a8fc1a254c5736ddb7ee8 = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_83ac3255eee91c26680be94981521361b2f95d18f183a506a09b5cdc23a40633 = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_83ac3255eee91c26680be94981521361b2f95d18f183a506a09b5cdc23a40633 || 0 === strpos($__internal_5c121d24a6570a3b061494dd3b27b3fc62a09636eb7a8fc1a254c5736ddb7ee8, $__internal_83ac3255eee91c26680be94981521361b2f95d18f183a506a09b5cdc23a40633)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_e08f0536e606792dc7ae6d2fc9c6dbc1b06c3430cb5b07f1c68b6bc4f7fd09ff = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_790dab1c4884d1dc7bc3b1fcc9b0c4e4eb2729d0f2dfb411a4a67a91c72e8b18 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_790dab1c4884d1dc7bc3b1fcc9b0c4e4eb2729d0f2dfb411a4a67a91c72e8b18 || 0 === strpos($__internal_e08f0536e606792dc7ae6d2fc9c6dbc1b06c3430cb5b07f1c68b6bc4f7fd09ff, $__internal_790dab1c4884d1dc7bc3b1fcc9b0c4e4eb2729d0f2dfb411a4a67a91c72e8b18)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_0f08460750c29d04ccec3e09dcb0e02bb3781f97802a1150e6ed29f684b28b70 = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f69ac5ed403c076396536c00764b23003c92e7361a998898eb4abe55f6b562aa = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_f69ac5ed403c076396536c00764b23003c92e7361a998898eb4abe55f6b562aa || 0 === strpos($__internal_0f08460750c29d04ccec3e09dcb0e02bb3781f97802a1150e6ed29f684b28b70, $__internal_f69ac5ed403c076396536c00764b23003c92e7361a998898eb4abe55f6b562aa)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_19d6e45b31a901b81aef85e57c2221bcd9c57f8f0d811cca4a1b17d40ae9aaae = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c5936ff6cfe6b384529ff0ed8950f4f541aa67a370463dae798431ed9aabf3d3 = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_c5936ff6cfe6b384529ff0ed8950f4f541aa67a370463dae798431ed9aabf3d3 || 0 === strpos($__internal_19d6e45b31a901b81aef85e57c2221bcd9c57f8f0d811cca4a1b17d40ae9aaae, $__internal_c5936ff6cfe6b384529ff0ed8950f4f541aa67a370463dae798431ed9aabf3d3)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_584d3d3182a1b463e9b8a11b2d0709179d2b4000f48b11616dc0daf2495e4735 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_67fd2ef4f6d29b79ce9d1a601e3acc77b54892ee176722bec49186051b9424d8 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_67fd2ef4f6d29b79ce9d1a601e3acc77b54892ee176722bec49186051b9424d8 || 0 === strpos($__internal_584d3d3182a1b463e9b8a11b2d0709179d2b4000f48b11616dc0daf2495e4735, $__internal_67fd2ef4f6d29b79ce9d1a601e3acc77b54892ee176722bec49186051b9424d8)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_236f40d65f785806f1e9192a518746b790dda76056bdb061e344977ad3d0fc79 = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_060eb6b2119f59db0658f7e520428fad48a74d934d59d3af0229a5ba998d9b24 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_060eb6b2119f59db0658f7e520428fad48a74d934d59d3af0229a5ba998d9b24 || 0 === strpos($__internal_236f40d65f785806f1e9192a518746b790dda76056bdb061e344977ad3d0fc79, $__internal_060eb6b2119f59db0658f7e520428fad48a74d934d59d3af0229a5ba998d9b24)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_e3829d9d12858a438f1a69a06c23f0e9134efa04f622d122de0daf125cbdc675 = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b0d37167860c985bad45eac4d9491a354485fdebb17812fc9e059e0e8fb6e3f6 = "http://www.businessbid.ae/node2") && ('' === $__internal_b0d37167860c985bad45eac4d9491a354485fdebb17812fc9e059e0e8fb6e3f6 || 0 === strpos($__internal_e3829d9d12858a438f1a69a06c23f0e9134efa04f622d122de0daf125cbdc675, $__internal_b0d37167860c985bad45eac4d9491a354485fdebb17812fc9e059e0e8fb6e3f6)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_bd43402a6ec621fe9308f1a068b26f2435563f00aa8680ee07935de7845fa9e8 = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f9e12ca6ceb478ae8f3d3c5bb0d244f96cd03404ec00e82c1521edbf3965eb9e = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_f9e12ca6ceb478ae8f3d3c5bb0d244f96cd03404ec00e82c1521edbf3965eb9e || 0 === strpos($__internal_bd43402a6ec621fe9308f1a068b26f2435563f00aa8680ee07935de7845fa9e8, $__internal_f9e12ca6ceb478ae8f3d3c5bb0d244f96cd03404ec00e82c1521edbf3965eb9e)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_a6eb5d311bbc08804c6ed13281de4671144dd589f6f82095105b1292d137a55d = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a6fcd57daff0fb1f753fbd380f2a9670f228532aa7f974303824706b37c5b86f = "http://www.businessbid.ae/faq") && ('' === $__internal_a6fcd57daff0fb1f753fbd380f2a9670f228532aa7f974303824706b37c5b86f || 0 === strpos($__internal_a6eb5d311bbc08804c6ed13281de4671144dd589f6f82095105b1292d137a55d, $__internal_a6fcd57daff0fb1f753fbd380f2a9670f228532aa7f974303824706b37c5b86f)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_253ac6e9e94f54d590effd8f7d7e324e9d520b59147d367c4315a680b371badb = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a18cd6aa641f5a92f0dc3e2b45b8ea513a32510840c7945c0baa8c9a72804a8b = "http://www.businessbid.ae/node4") && ('' === $__internal_a18cd6aa641f5a92f0dc3e2b45b8ea513a32510840c7945c0baa8c9a72804a8b || 0 === strpos($__internal_253ac6e9e94f54d590effd8f7d7e324e9d520b59147d367c4315a680b371badb, $__internal_a18cd6aa641f5a92f0dc3e2b45b8ea513a32510840c7945c0baa8c9a72804a8b)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_57f82b4b054929fba2bc4b6a51d932d71cf761209aee8581af741d0d96869aca = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_470602b771a282fcfdc90e569aab18bffa2fc0d2698561987b60827c347051b9 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_470602b771a282fcfdc90e569aab18bffa2fc0d2698561987b60827c347051b9 || 0 === strpos($__internal_57f82b4b054929fba2bc4b6a51d932d71cf761209aee8581af741d0d96869aca, $__internal_470602b771a282fcfdc90e569aab18bffa2fc0d2698561987b60827c347051b9)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_942e24d8811c96997c0a26c5826756cf576a45c5fb046c7049b71e851fc93166 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3f8babc1aa8dac6d9196b01d09e472f02e8d37be8ba1a6963975a1fbbd00d897 = "http://www.businessbid.ae/node6") && ('' === $__internal_3f8babc1aa8dac6d9196b01d09e472f02e8d37be8ba1a6963975a1fbbd00d897 || 0 === strpos($__internal_942e24d8811c96997c0a26c5826756cf576a45c5fb046c7049b71e851fc93166, $__internal_3f8babc1aa8dac6d9196b01d09e472f02e8d37be8ba1a6963975a1fbbd00d897)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_0475154e410b6e6b2778ecce108941c4f9b7d4d0c597bf308a9fca11601c7649 = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0c129d0ca34a4a4c4da1352c955610d46300760a4a005cf2c9f08ea015f1ffa8 = "http://www.businessbid.ae/feedback") && ('' === $__internal_0c129d0ca34a4a4c4da1352c955610d46300760a4a005cf2c9f08ea015f1ffa8 || 0 === strpos($__internal_0475154e410b6e6b2778ecce108941c4f9b7d4d0c597bf308a9fca11601c7649, $__internal_0c129d0ca34a4a4c4da1352c955610d46300760a4a005cf2c9f08ea015f1ffa8)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_a69c9aeeae085abb0afdf10c4728c82cf9102b0bdf1e64a68a788e6254670b79 = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f09fb5e15ca3ca6fe7b832e171f6c9453aeaa884116b87aae63f505293bdba70 = "http://www.businessbid.ae/sitemap") && ('' === $__internal_f09fb5e15ca3ca6fe7b832e171f6c9453aeaa884116b87aae63f505293bdba70 || 0 === strpos($__internal_a69c9aeeae085abb0afdf10c4728c82cf9102b0bdf1e64a68a788e6254670b79, $__internal_f09fb5e15ca3ca6fe7b832e171f6c9453aeaa884116b87aae63f505293bdba70)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_ee62f0d65b08ab70a54506800326da0752d3b5eec8f4c7bd82ad479a6052a9da = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c960d5e002386aa7606fb47519b1494bdafa3f7cd80e18737d08495f249f6f50 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_c960d5e002386aa7606fb47519b1494bdafa3f7cd80e18737d08495f249f6f50 || 0 === strpos($__internal_ee62f0d65b08ab70a54506800326da0752d3b5eec8f4c7bd82ad479a6052a9da, $__internal_c960d5e002386aa7606fb47519b1494bdafa3f7cd80e18737d08495f249f6f50)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_97db472efaac835cd4a607a27c37970dc4a10e5f81d76f92979aed65273740be = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1491690d2253bfcdbfd2a8ed2fe851be1878a7daa6408c01884a3dc86398eac7 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_1491690d2253bfcdbfd2a8ed2fe851be1878a7daa6408c01884a3dc86398eac7 || 0 === strpos($__internal_97db472efaac835cd4a607a27c37970dc4a10e5f81d76f92979aed65273740be, $__internal_1491690d2253bfcdbfd2a8ed2fe851be1878a7daa6408c01884a3dc86398eac7)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_b6e8840d8bf5f9a0fa222a3be6545710d494c9124004f18a184a0935d26b37de = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_eec081778a260058c5703cb98808e4465e7eb1232cca24f1e5651bc5efe0d8a7 = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_eec081778a260058c5703cb98808e4465e7eb1232cca24f1e5651bc5efe0d8a7 || 0 === strpos($__internal_b6e8840d8bf5f9a0fa222a3be6545710d494c9124004f18a184a0935d26b37de, $__internal_eec081778a260058c5703cb98808e4465e7eb1232cca24f1e5651bc5efe0d8a7)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_e1d64becb8c1a1a3e17bb786b1a78b21f6ef7ec4e50635c6897fd1c3e05e6c2c = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_4a68f31bcf36ac0b079abf35ec3e4084fc4b666e40a6679029fbd58834e5f365 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_4a68f31bcf36ac0b079abf35ec3e4084fc4b666e40a6679029fbd58834e5f365 || 0 === strpos($__internal_e1d64becb8c1a1a3e17bb786b1a78b21f6ef7ec4e50635c6897fd1c3e05e6c2c, $__internal_4a68f31bcf36ac0b079abf35ec3e4084fc4b666e40a6679029fbd58834e5f365)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 162
    public function block_customcss($context, array $blocks = array())
    {
        // line 163
        echo "
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "<script>
\$(document).ready(function(){
    ";
        // line 169
        if (array_key_exists("keyword", $context)) {
            // line 170
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 174
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 310
    public function block_customjs($context, array $blocks = array())
    {
        // line 311
        echo "
";
    }

    // line 314
    public function block_jquery($context, array $blocks = array())
    {
        // line 315
        echo "
";
    }

    // line 317
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 318
        echo "
<!-- Google Analytics -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>

";
    }

    // line 402
    public function block_maincontent($context, array $blocks = array())
    {
        // line 403
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 490
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 530
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 560
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 561
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 563
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 565
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 571
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 572
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 574
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 576
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 581
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 582
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 584
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 586
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 593
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 594
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 596
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 598
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 604
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 605
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 607
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 609
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 615
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 616
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 618
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 620
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 490
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 491
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 500
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 501
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 502
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 503
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 504
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 505
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 506
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 508
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 509
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 510
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 512
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 519
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 525
        echo "    </div>
            </div>


    ";
    }

    // line 519
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 520
        echo "    <div class=\"request\">
    <h2>";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 636
    public function block_footer($context, array $blocks = array())
    {
        // line 637
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 666
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 687
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 688
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 740
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 742
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 752
    public function block_footerScript($context, array $blocks = array())
    {
        // line 753
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 754
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1411 => 754,  1406 => 753,  1403 => 752,  1392 => 742,  1387 => 740,  1347 => 703,  1339 => 698,  1335 => 697,  1331 => 696,  1320 => 688,  1317 => 687,  1313 => 685,  1304 => 679,  1300 => 678,  1296 => 677,  1292 => 676,  1288 => 675,  1277 => 667,  1273 => 666,  1264 => 660,  1260 => 659,  1256 => 658,  1252 => 657,  1248 => 656,  1227 => 637,  1224 => 636,  1216 => 521,  1213 => 520,  1210 => 519,  1202 => 525,  1200 => 519,  1193 => 514,  1186 => 512,  1170 => 510,  1167 => 509,  1164 => 508,  1150 => 506,  1147 => 505,  1144 => 504,  1142 => 503,  1138 => 502,  1135 => 501,  1131 => 500,  1121 => 493,  1117 => 491,  1114 => 490,  1098 => 620,  1092 => 618,  1086 => 616,  1084 => 615,  1076 => 609,  1070 => 607,  1064 => 605,  1062 => 604,  1054 => 598,  1048 => 596,  1042 => 594,  1040 => 593,  1031 => 586,  1025 => 584,  1019 => 582,  1017 => 581,  1010 => 576,  1004 => 574,  998 => 572,  996 => 571,  988 => 565,  982 => 563,  976 => 561,  974 => 560,  942 => 530,  940 => 490,  925 => 478,  914 => 470,  903 => 462,  892 => 454,  881 => 446,  871 => 439,  853 => 424,  843 => 417,  833 => 410,  824 => 403,  821 => 402,  813 => 318,  810 => 317,  805 => 315,  802 => 314,  797 => 311,  794 => 310,  775 => 294,  653 => 174,  647 => 170,  645 => 169,  641 => 167,  638 => 166,  633 => 163,  630 => 162,  626 => 127,  623 => 126,  617 => 109,  613 => 107,  611 => 106,  608 => 105,  606 => 104,  603 => 103,  601 => 102,  598 => 101,  596 => 100,  593 => 99,  591 => 98,  588 => 97,  586 => 96,  583 => 95,  581 => 94,  578 => 93,  576 => 92,  573 => 91,  571 => 90,  568 => 89,  566 => 88,  563 => 87,  561 => 86,  558 => 85,  556 => 84,  553 => 83,  551 => 82,  548 => 81,  546 => 80,  543 => 79,  541 => 78,  538 => 77,  536 => 76,  533 => 75,  531 => 74,  528 => 73,  526 => 72,  523 => 71,  521 => 70,  518 => 69,  516 => 68,  513 => 67,  511 => 66,  508 => 65,  506 => 64,  503 => 63,  501 => 62,  498 => 61,  496 => 60,  493 => 59,  491 => 58,  488 => 57,  486 => 56,  483 => 55,  481 => 54,  478 => 53,  476 => 52,  473 => 51,  471 => 50,  468 => 49,  466 => 48,  463 => 47,  461 => 46,  458 => 45,  456 => 44,  453 => 43,  451 => 42,  448 => 41,  446 => 40,  443 => 39,  441 => 38,  438 => 37,  436 => 36,  433 => 35,  431 => 34,  428 => 33,  426 => 32,  423 => 31,  421 => 30,  418 => 29,  416 => 28,  413 => 27,  411 => 26,  408 => 25,  406 => 24,  403 => 23,  401 => 22,  398 => 21,  396 => 20,  393 => 19,  391 => 18,  388 => 17,  386 => 16,  383 => 15,  381 => 14,  378 => 13,  376 => 12,  373 => 11,  368 => 763,  359 => 756,  357 => 752,  354 => 749,  352 => 636,  347 => 633,  345 => 402,  336 => 395,  322 => 392,  319 => 391,  317 => 390,  311 => 387,  307 => 385,  305 => 384,  297 => 379,  289 => 374,  284 => 372,  278 => 369,  273 => 367,  269 => 366,  260 => 359,  244 => 354,  240 => 353,  236 => 352,  232 => 350,  230 => 349,  224 => 346,  218 => 345,  214 => 343,  212 => 342,  201 => 336,  194 => 331,  192 => 330,  190 => 329,  188 => 328,  186 => 327,  182 => 325,  180 => 324,  177 => 323,  175 => 317,  173 => 314,  170 => 313,  168 => 310,  165 => 309,  163 => 166,  160 => 165,  158 => 162,  153 => 160,  149 => 159,  140 => 153,  135 => 151,  131 => 150,  126 => 148,  122 => 147,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:leads_paymentform.html.twig */
class __TwigTemplate_f97b9b481553203424660acf761606a77bb63f92382b54d16c1576047640312c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "
\t<style type=\"text/css\">
\t.camouflage{
\t\tdisplay: none;
\t}
\t.exposed{
\t\tdisplay: block;
\t}
\t.errorinput{ content: attr(title); color: red; margin-left: 0.6rem; }
\t</style>
\t<script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t
\t\$(document).ready(function(){
\$('.bbspop').popover();
\$('.bbspop').popover({ trigger: \"hover\" });
    \$('[data-toggle=\"popover\"]').popover();
    \$('#form_ccExpDate_month,#form_ccExpDate_year').width(60);
    \$('#form_ccSubmit').hide();
    \$(\"#form_ccNumber\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCNum').html('Enter card number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorCCNum').html('Numbers only !');
        } else {
            \$('small#errorCCNum').html('');
            var result = getCreditCardType(ccVal);
            if(result == 'unknown'){
                \$('small#errorCCNum').html('Only Master and Visa card allowed');
            } else {
                \$('small#errorCCNum').html('');
            }

            var result = isValidCreditCard(type, ccnum);
            if(result) {
                \$('small#errorCCNum').html('');
            } else {
                \$('small#errorCCNum').html('Invalid card number');
            }
        }
    })
    \$(\"#form_ccName\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCName').html('Enter card holder\\'s name.');
        } else if(!isNaN(ccVal)) {
            \$('small#errorCCName').html('Numbers not allowed !');
        } else {
            \$('small#errorCCName').html('');
        }
    })
    \$(\"#form_ccCVV\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorCCVV').html('Enter cvv number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorCCVV').html('Numbers only!');
        } else if (ccVal.length > 3){
            \$('small#errorCCVV').html('Only 3 digits allowed.');
        } else {
            \$('small#errorCCVV').html('');
        }
    })
    \$(\"input[name='payType'],div#editAdress\").click(function (){
        var payType = \$(\"input[name='payType']:checked\").val();
        if(payType == 'check pickup') {
            \$( 'div#checkFillForm' ).toggleClass( \"camouflage\" );
            \$( 'div#checkAddShow' ).toggleClass( \"camouflage\" );
        }
        else if(payType == 'cash pickup') {
            \$( 'div#cashFillForm' ).toggleClass( \"camouflage\" );
            \$( 'div#cashAddShow' ).toggleClass( \"camouflage\" );
        }
    });
    \$('.btn-success').click(function () {
        var payType = \$(\"input[name='payType']:checked\").val();//\$(\"input:radio[name='payType']\").val();
        alert('Your '+payType+' payment is being processed. Please click OK and do not leave this page');
        if(payType == 'cc') {
            \$( \"#ccForm\" ).submit();
        } else if (payType == 'paypal') {
             \$( \"#payPalForm\" ).submit();
        } else if (payType == 'check pickup') {
            \$( \"#checkForm\" ).submit();
        } else if (payType == 'cash pickup') {
            \$( \"#cashForm\" ).submit();
        }
    })
    \$('#form_ccExpDate_month').change(function () {
        var ccmonth = \$(this).val();
        if(ccmonth == '') {
            \$('small#errorCCExp').html('Select expiry month.');
        } else {
            \$('small#errorCCExp').html('');
        }
    })
    \$('#form_ccExpDate_year').change(function () {
        var ccmonth = \$(this).val();
        if(ccmonth == '') {
            \$('small#errorCCExp').html('Select expiry year.');
        } else {
            \$('small#errorCCExp').html('');
        }
    })

    \$(\"#form_contactnumber\").keyup(function(){
        var ccVal = \$(this).val();
        if(ccVal == '') {
            \$('small#errorConNum').html('Enter Mobile Contact Number.');
        } else if(isNaN(ccVal)) {
            \$('small#errorConNum').html('Numbers only!');
        } else {
            \$('small#errorConNum').html('');
        }
    })
});
function getCreditCardType(accountNumber)
{

  //start without knowing the credit card type
  var result = \"unknown\";

  //first check for MasterCard

  if (/^5[1-5]/.test(accountNumber))
  {
    result = \"mastercard\";
  }

  //then check for Visa
  else if (/^4/.test(accountNumber))
  {
    result = \"visa\";
  }

  //then check for AmEx
  /*else if (/^3[47]/.test(accountNumber))
  {
    result = \"amex\";
  }*/

  return result;
}

function isValidCreditCard(type, ccnum)
{
/* Visa: length 16, prefix 4, dashes optional.
Mastercard: length 16, prefix 51-55, dashes optional.
Discover: length 16, prefix 6011, dashes optional.
American Express: length 15, prefix 34 or 37.
Diners: length 14, prefix 30, 36, or 38. */

  var re = new Regex({ \"visa\": \"/^4\\d{3}-?\\d{4}-?\\d{4}-?\\d\",
                       \"mc\": \"/^5[1-5]\\d{2}-?\\d{4}-?\\d{4}-?\\d{4}\$/\",
                       \"disc\": \"/^6011-?\\d{4}-?\\d{4}-?\\d{4}\$/\",
                       \"amex\": \"/^3[47]\\d{13}\$/\",
                       \"diners\": \"/^3[068]\\d{12}\$/\"}[type.toLowerCase()])

   if (!re.test(ccnum)) return false;
   // Remove all dashes for the checksum checks to eliminate negative numbers
   ccnum = ccnum.split(\"-\").join(\"\");
   // Checksum (\"Mod 10\")
   // Add even digits in even length strings or odd digits in odd length strings.
   var checksum = 0;
   for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
      checksum += parseInt(ccnum.charAt(i-1));
   }
   // Analyze odd digits in even length strings or even digits in odd length strings.
   for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
      var digit = parseInt(ccnum.charAt(i-1)) * 2;
      if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
   }
   if ((checksum % 10) == 0) return true; else return false;
}
\t
\t</script>
\t
<div class=\"container\">
<div>
    ";
        // line 183
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 184
            echo "
    <div class=\"alert alert-danger\">";
            // line 185
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 188
        echo "
    ";
        // line 189
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 190
            echo "
    <div class=\"alert alert-success\">";
            // line 191
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 194
        echo "
    ";
        // line 195
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 196
            echo "
    <div class=\"alert alert-success\">";
            // line 197
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 200
        echo "</div>

<div class=\"col-sm-12\">
\t<div class=\"package-details\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" id=\"selectpackagedetails\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Package Details </td>
\t\t\t\t\t<td width=\"197\">Price</td>
\t\t\t\t\t<td width=\"189\">Discount</td>
\t\t\t\t\t<td width=\"189\">Total</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 214
        $context["OTC"] = 0;
        // line 215
        echo "\t\t\t\t";
        $context["grandTotal"] = 0;
        // line 216
        echo "\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["purchasingLeads"]) ? $context["purchasingLeads"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lead"]) {
            // line 217
            echo "\t\t\t\t\t";
            $context["priceArray"] = twig_split_filter($this->env, $this->getAttribute($context["lead"], "price", array()), " - ");
            // line 218
            echo "
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">";
            // line 220
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "plan", array()), "html", null, true);
            echo " Package: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lead"], "category", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t";
            // line 221
            $context["multiplier"] = $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
            // line 222
            echo "\t\t\t\t\t<td width=\"197\">AED ";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">
\t\t\t\t\t\t";
            // line 224
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                // line 225
                echo "\t\t\t\t\t\t\t";
                $context["selPlan"] = $this->getAttribute((isset($context["leadsPack"]) ? $context["leadsPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array");
                // line 226
                echo "\t\t\t\t\t\t\tAED ";
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")) - ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array"))), "html", null, true);
                echo "
\t\t\t\t\t\t";
            } else {
                // line 228
                echo "\t\t\t\t\t\t\tAED 0
\t\t\t\t\t\t";
            }
            // line 230
            echo "\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 231
            if (($this->getAttribute($context["lead"], "plan", array()) != "Bronze")) {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), (isset($context["selPlan"]) ? $context["selPlan"] : null), array(), "array") * $this->getAttribute((isset($context["leadsPerPack"]) ? $context["leadsPerPack"] : null), $this->getAttribute($context["lead"], "plan", array()), array(), "array")));
            } else {
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)), "html", null, true);
                $context["grandTotal"] = ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + ($this->getAttribute((isset($context["priceArray"]) ? $context["priceArray"] : null), 0, array(), "array") * (isset($context["multiplier"]) ? $context["multiplier"] : null)));
            }
            echo "</td>

\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lead'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 235
        echo "\t\t\t\t";
        if (((isset($context["otcStatus"]) ? $context["otcStatus"] : null) == 0)) {
            // line 236
            echo "\t\t\t\t\t";
            $context["OTC"] = (isset($context["registerFee"]) ? $context["registerFee"] : null);
            // line 237
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Account Setup Fee <span><a tabindex=\"0\" class=\"popover-icon\" role=\"button\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"bottom\" data-trigger=\"hover\" title=\"The Account Setup fee is one time charge and includes:\" data-content=\"<ul><li>Creation of dedicated webpage with a full business profie.</li> <li>Access to 24/7 online dashboard with reporting features.</li> <li>Compilation and publication of 5 recent customer reviews.</li></ul>\"></a></span>
\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"197\">AED ";
            // line 240
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t\t<td width=\"189\">AED 0</td>
\t\t\t\t\t<td width=\"189\">AED ";
            // line 242
            echo twig_escape_filter($this->env, (isset($context["registerFee"]) ? $context["registerFee"] : null), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t</tbody>
\t\t\t\t";
        }
        // line 246
        echo "\t\t\t\t<tfoot>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"216\">Grand Total</td>
\t\t\t\t\t<td width=\"197\"></td>
\t\t\t\t\t<td width=\"189\"></td>
\t\t\t\t\t<td width=\"189\"><strong>AED ";
        // line 251
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</strong></td>
\t\t\t\t</tr>
\t\t\t\t</tfoot>
\t\t</table>
\t</div>
\t<script>
\t\$(document).ready(function(){
\tvar tt=(grandTotal + OTC)* 100;
\talert(tt);
\t\$.cookie(\"totalnew\",  tt,{ path: '/' });
\t});
\t</script>
\t<div class=\"paymentmethod\">
\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\">Payment Method</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t<tr>
\t\t\t\t\t<td width=\"100%\"><strong>How would you like to pay <span class=\"payment_value\">";
        // line 272
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)), 2, ".", ","), "html", null, true);
        echo "</span> AED ?</td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t</table>
\t\t<div class=\"credit-card-option col-sm-12 position-in-opt left-clear\" style=\"width:100%; float:left;\">
\t\t\t<div class=\"col-sm-12 position-in-opt\"><input type=\"radio\" name=\"payType\" checked=\"checked\" value=\"cc\" />Credit Card</div>

\t\t\t<div class=\"col-sm-9\">

        <script src=\"https://checkout.com/cdn/js/checkout.js\"></script>

            <form class=\"payment-form\">
                <script>
                  var publickey = 'pk_04d5b10a-845e-4267-866b-7a16910dd048'; //PublicKey provided by Checkout

                  Checkout.render({
                      debugMode: true,
                      namespace: 'CheckoutIntegration',
                      publicKey: publickey,
                      paymentToken: \"";
        // line 291
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\",
                      customerEmail: '";
        // line 292
        echo twig_escape_filter($this->env, (isset($context["usermail"]) ? $context["usermail"] : null), "html", null, true);
        echo "',
                      customerName: '";
        // line 293
        echo twig_escape_filter($this->env, (isset($context["custome"]) ? $context["custome"] : null), "html", null, true);
        echo "',
                      paymentMode: 'card',
                      value: ";
        // line 295
        echo twig_escape_filter($this->env, (((isset($context["grandTotal"]) ? $context["grandTotal"] : null) + (isset($context["OTC"]) ? $context["OTC"] : null)) * 100), "html", null, true);
        echo ",
                      currency: 'AED',
                      forceMobileRedirect: true,
                      payButtonSelector: '#payButtonId',
                      widgetContainerSelector: '.payment-form',
                      useCurrencyCode: false,
                      
                       styling: {
                            widgetColor: 'rgba(204, 204, 204, 0.16)',
                            themeColor: '',
                            buttonColor: '',
                            buttonLabelColor: '',
                            logoUrl: '',
                            formButtonColor: '',
                            formButtonLabelColor: '',
                            iconColor: '',
                            overlayShade: 'dark',
                            overlayOpacity: 0.8,
                            buttonLabel: 'Visa Checkout'
                        },

                 
                      cardCharged: function (event) {
                          document.getElementById('cko-cc-paymenToken').value = event.data.paymentToken;
                          document.getElementById('payment-tokenform').submit();
                      }

                  });
\t\t\t\t  
\t\t\t\t  
                </script>

            </form>
\t\t\t<form name=\"payment-tokenform\" method=\"post\" id=\"payment-tokenform\" action=\"http://www.businessbid.ae/vendor/home\">
                <input type=\"hidden\" id =\"cko-cc-paymenToken\" name =\"cko-cc-paymenToken\" />
            </form>
\t\t\t</div>
\t\t\t<div class=\"col-sm-3\"><img src=\"";
        // line 332
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/payment-icon.jpg"), "html", null, true);
        echo "\" /></div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"check pickup\" />Cheque Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"checkAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 342
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"checkFillForm\">
\t\t\t\t";
        // line 348
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_start', array("attr" => array("id" => "checkForm")));
        echo "
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\"><label>Company Name:</label></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 353
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 356
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 357
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "address", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 360
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 361
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 364
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 365
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "city", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 368
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 369
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 372
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 373
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 376
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 377
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "bankname", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"20%\">";
        // line 380
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 381
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "checknumber", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr class=\"camouflage\">
\t\t\t\t\t\t\t<td width=\"20%\"></td>
\t\t\t\t\t\t\t<td width=\"80%\">";
        // line 385
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["checkForm"]) ? $context["checkForm"] : null), "Submit", array()), 'widget');
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t\t";
        // line 389
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["checkForm"]) ? $context["checkForm"] : null), 'form_end');
        echo "
\t\t\t</div>
\t\t</div>

\t\t <div class=\"check-pickup-option col-sm-12 position-in-opt\">
\t\t\t<div class=\"\"><input type=\"radio\" name=\"payType\" value=\"cash pickup\" />Cash Pick Up</div>
\t\t\t<!--<div class=\"col-sm-2\" id=\"editAdress\"><a>Edit Address</a></div>-->
\t\t\t<div class=\"col-sm-12\" id=\"cashAddShow\">
\t\t\t\t<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
\t\t\t\t\t<tbody>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td width=\"100%\">";
        // line 400
        echo twig_escape_filter($this->env, (isset($context["vendorAddress"]) ? $context["vendorAddress"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t\t<div class=\"col-sm-12 camouflage\" id=\"cashFillForm\">
                ";
        // line 406
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_start', array("attr" => array("id" => "cashForm")));
        echo "
                <table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\" class=\"paymentmethod\">
                    <tbody>
                        <tr>
                            <td width=\"20%\"><label>Company Name:</label></td>
                            <td width=\"80%\">";
        // line 411
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 414
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'label');
        echo "<span class=\"credit\">*</span>:</td>
                            <td width=\"80%\">";
        // line 415
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "address", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 418
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'label');
        echo ":</td>
                            <td width=\"80%\">";
        // line 419
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "addresssecond", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 422
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 423
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "city", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 426
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 427
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactnumber", array()), 'widget');
        echo "<small class=\"errorinput\" id=\"errorConNum\"></small></td>
                        </tr>
                        <tr>
                            <td width=\"20%\">";
        // line 430
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'label');
        echo ":<span class=\"credit\">*</span></td>
                            <td width=\"80%\">";
        // line 431
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "contactperson", array()), 'widget');
        echo "</td>
                        </tr>
                        <tr class=\"camouflage\">
                            <td width=\"20%\"></td>
                            <td width=\"80%\">";
        // line 435
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cashForm"]) ? $context["cashForm"] : null), "Submit", array()), 'widget');
        echo "</td>
                        </tr>
                    </tbody>
                </table>
                ";
        // line 439
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cashForm"]) ? $context["cashForm"] : null), 'form_end');
        echo "
            </div>
\t\t</div>

\t\t";
        // line 458
        echo "\t</div>
</div>
\t<div class=\"row text-right update-account\">
\t\t<a href=\"#\" class=\"btn btn-success\"> Place Order </a>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leads_paymentform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  695 => 458,  688 => 439,  681 => 435,  674 => 431,  670 => 430,  664 => 427,  660 => 426,  654 => 423,  650 => 422,  644 => 419,  640 => 418,  634 => 415,  630 => 414,  624 => 411,  616 => 406,  607 => 400,  593 => 389,  586 => 385,  579 => 381,  575 => 380,  569 => 377,  565 => 376,  559 => 373,  555 => 372,  549 => 369,  545 => 368,  539 => 365,  535 => 364,  529 => 361,  525 => 360,  519 => 357,  515 => 356,  509 => 353,  501 => 348,  492 => 342,  479 => 332,  439 => 295,  434 => 293,  430 => 292,  426 => 291,  404 => 272,  380 => 251,  373 => 246,  366 => 242,  361 => 240,  356 => 237,  353 => 236,  350 => 235,  334 => 231,  331 => 230,  327 => 228,  321 => 226,  318 => 225,  316 => 224,  310 => 222,  308 => 221,  302 => 220,  298 => 218,  295 => 217,  290 => 216,  287 => 215,  285 => 214,  269 => 200,  260 => 197,  257 => 196,  253 => 195,  250 => 194,  241 => 191,  238 => 190,  234 => 189,  231 => 188,  222 => 185,  219 => 184,  215 => 183,  43 => 14,  31 => 4,  28 => 3,  11 => 1,);
    }
}

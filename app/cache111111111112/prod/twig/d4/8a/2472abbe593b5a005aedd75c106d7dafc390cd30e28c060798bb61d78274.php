<?php

/* BBidsBBidsHomeBundle:Admin:vendor_leads_list.html.twig */
class __TwigTemplate_d48a2472abbe593b5a005aedd75c106d7dafc390cd30e28c060798bb61d78274 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:vendor_leads_list.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).ready(function() {

    \$('#example').dataTable({
        \"sPaginationType\": \"full_numbers\",
        \"aaSortingFixed\": [[4,'asc']],
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sEmptyTable\": '',
            \"sInfoEmpty\": '',
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
        \"sEmptyTable\": \"There are no records\",
        \"sAjaxSource\": \"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/vendor_leads_list.php"), "html", null, true);
        echo "\",
        \"fnServerParams\": function(aoData) {

        }
    }).columnFilter({
        aoColumns: [
                    { sSelector: \"#companyNameFilter\"},
                    { sSelector: \"#vendorFilter\"},
                    null,
                    { sSelector: \"#categoryFilter\", type:\"select\", values : ";
        // line 47
        echo twig_jsonencode_filter((isset($context["catArray"]) ? $context["catArray"] : null));
        echo " },
                    null,null
                ]
    });


    \$(\"#btnExport\").click(function() {
        \$(\"#example\").btechco_excelexport({
            containerid: \"example\",
            datatype: \$datatype.Table
        });
    });
});
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Vendor Leads List</h1></div>

<div>
    ";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 67
            echo "
    <div class=\"alert alert-danger\">";
            // line 68
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "
    ";
        // line 72
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 73
            echo "
    <div class=\"alert alert-success\">";
            // line 74
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "
    ";
        // line 78
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 79
            echo "
    <div class=\"alert alert-success\">";
            // line 80
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "<div>";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "</div>
<div class=\"admin_filter report-filter\">
    <div class=\"col-md-3 \" id=\"companyNameFilter\"></div>
    <div class=\"col-md-3\" id=\"vendorFilter\"></div>
    <div class=\"col-md-3\" id=\"categoryFilter\"></div>";
        // line 91
        echo "    <div class=\"col-md-5 clear-right action-links\">
    <span class=\"export-all\"><a href=\"";
        // line 92
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_exportall_leadhistory");
        echo "\" target=\"_blank\" class=\"form_submit\">Export All</a></span>
    <span class=\"export-selected\"><a href=\"#\" class=\"form_submit\" onclick=\"getSearchInputs();\">Export Filtered Data</a></span>
    <span class=\"reset\"><a href=\"#\" class=\"form_submit\" onclick=\"fnResetFilters();\">Reset</a></span>
    </div>
</div>

<div class=\"latest-orders\">
<table id=\"example\">
    <thead>
        <tr>
            <th>Company Name</th>
            <th>Vendor Name</th>
            <th>Contact Number</th>
            <th>Category</th>
            <th>Lead Count</th>
            <th>Total Leads till Date</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan=\"8\" class=\"dataTables_empty\">Loading data from server... Please wait</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th>Company Name</th>
            <th>Vendor Name</th>
            <th>Contact Number</th>
            <th>Category</th>
            <th>Lead Count</th>
            <th>Total Leads till Date</th>
        </tr>
    </tfoot>
</table>
</div>
</div>

</div>
<script type=\"text/javascript\">
    function getSearchInputs () {
        var oTable = \$('#example').dataTable();
        var oSettings = oTable.fnSettings();
        var \$form = document.forms[\"form\"];
        \$form.reset();
        var oForm = document.forms[\"form\"].getElementsByTagName(\"input\");
        var dateRange = '';

        oForm[0].value = dateRange;
        oForm[2].value = oSettings.aoPreSearchCols[2].sSearch;
        oForm[3].value = oSettings.aoPreSearchCols[3].sSearch;
        oForm[4].value = oSettings.aoPreSearchCols[4].sSearch;
        if(oSettings.aoPreSearchCols[5].sSearch != '')
            oForm[5].value = (oSettings.aoPreSearchCols[5].sSearch == 'Active') ? 1 : 0;

        if((oForm[0].value =='~') && (oForm[2].value =='') && (oForm[3].value =='') && (oForm[4].value =='') && (oForm[5].value ==''))
            alert('No filtered results');
        else
           \$form.submit();
    }

    function fnResetFilters() {
        var table = \$('#example').dataTable();
        table.fnFilterClear();
        \$('input').val('');
        \$('div#statusFilter').find(\"select option:eq(0)\").prop(\"selected\",true);
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:vendor_leads_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 92,  173 => 91,  165 => 83,  156 => 80,  153 => 79,  149 => 78,  146 => 77,  137 => 74,  134 => 73,  130 => 72,  127 => 71,  118 => 68,  115 => 67,  111 => 66,  89 => 47,  77 => 38,  44 => 8,  40 => 7,  36 => 6,  31 => 5,  28 => 3,  11 => 1,);
    }
}

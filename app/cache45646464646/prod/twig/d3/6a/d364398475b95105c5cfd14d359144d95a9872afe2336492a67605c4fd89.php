<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">


    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 162
        $this->displayBlock('customcss', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('javascripts', $context, $blocks);
        // line 309
        echo "
";
        // line 310
        $this->displayBlock('customjs', $context, $blocks);
        // line 313
        echo "
";
        // line 314
        $this->displayBlock('jquery', $context, $blocks);
        // line 317
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 323
        echo "</head>
";
        // line 324
        ob_start();
        // line 325
        echo "<body>

";
        // line 327
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 328
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 329
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 330
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 331
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"";
        // line 336
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 342
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 343
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 345
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 346
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 349
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 350
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 352
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 353
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 359
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 366
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 369
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 372
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 374
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 379
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 384
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 385
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 387
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 390
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 391
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 395
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 402
        $this->displayBlock('maincontent', $context, $blocks);
        // line 633
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 636
        $this->displayBlock('footer', $context, $blocks);
        // line 749
        echo "    <!-- footer block ends -->
</div>";
        // line 752
        $this->displayBlock('footerScript', $context, $blocks);
        // line 756
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 763
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_498555f2a5bfe6f5caf6655bd6b5a0e9d33616928c1cae4af29203cc73039471 = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_74f7135810255b3d3ff960631952cd3e6a04571818f3e74c5d7e81e7bdd01f96 = "http://www.businessbid.ae/contactus") && ('' === $__internal_74f7135810255b3d3ff960631952cd3e6a04571818f3e74c5d7e81e7bdd01f96 || 0 === strpos($__internal_498555f2a5bfe6f5caf6655bd6b5a0e9d33616928c1cae4af29203cc73039471, $__internal_74f7135810255b3d3ff960631952cd3e6a04571818f3e74c5d7e81e7bdd01f96)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_996581ffa3aad045b62633060b9d8487db591f80f9c7c8c0c17de3b112481812 = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_84fcc1ba6e830386e03a54b4348f37baffaff9e9718fba29c1ae40c2e62e5e7a = "http://www.businessbid.ae/node3") && ('' === $__internal_84fcc1ba6e830386e03a54b4348f37baffaff9e9718fba29c1ae40c2e62e5e7a || 0 === strpos($__internal_996581ffa3aad045b62633060b9d8487db591f80f9c7c8c0c17de3b112481812, $__internal_84fcc1ba6e830386e03a54b4348f37baffaff9e9718fba29c1ae40c2e62e5e7a)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_bd13106aea54dfcbebd10c12fefdd3ccea51f6a2a5be369da7adf267e6ffa949 = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1a6cca7d3c5a416c3e8ed3bfa25d458c0b8ac7923ae5cad78c3e0d7a661192b2 = "http://www.businessbid.ae/review/login") && ('' === $__internal_1a6cca7d3c5a416c3e8ed3bfa25d458c0b8ac7923ae5cad78c3e0d7a661192b2 || 0 === strpos($__internal_bd13106aea54dfcbebd10c12fefdd3ccea51f6a2a5be369da7adf267e6ffa949, $__internal_1a6cca7d3c5a416c3e8ed3bfa25d458c0b8ac7923ae5cad78c3e0d7a661192b2)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_2325eb12727e8ce801659f120edeafc67cd669d804c5de31e6a16cab8d5b39ed = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6378db7a3d69cd88d3fb690892a6f8ed8b5d1437d2ecc5c35d77750bfc0d6024 = "http://www.businessbid.ae/node1") && ('' === $__internal_6378db7a3d69cd88d3fb690892a6f8ed8b5d1437d2ecc5c35d77750bfc0d6024 || 0 === strpos($__internal_2325eb12727e8ce801659f120edeafc67cd669d804c5de31e6a16cab8d5b39ed, $__internal_6378db7a3d69cd88d3fb690892a6f8ed8b5d1437d2ecc5c35d77750bfc0d6024)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_b09d154c89ff9fa8efb09684507664ba18d5a2e2e2027f34b6a495c750f1cbf4 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ddaf586f33ba0bc50d8232d161702adf774b4ba111c8849d49051fdc5c008732 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_ddaf586f33ba0bc50d8232d161702adf774b4ba111c8849d49051fdc5c008732 || 0 === strpos($__internal_b09d154c89ff9fa8efb09684507664ba18d5a2e2e2027f34b6a495c750f1cbf4, $__internal_ddaf586f33ba0bc50d8232d161702adf774b4ba111c8849d49051fdc5c008732)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_a4a0bb6c442b343c702303fa08717777e22b95f743c1cd2e285a8f729ea63da3 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_098999b96b0d8105b7b4e937d2028101378ab5db35d5d6c0137ac8778e5e1689 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_098999b96b0d8105b7b4e937d2028101378ab5db35d5d6c0137ac8778e5e1689 || 0 === strpos($__internal_a4a0bb6c442b343c702303fa08717777e22b95f743c1cd2e285a8f729ea63da3, $__internal_098999b96b0d8105b7b4e937d2028101378ab5db35d5d6c0137ac8778e5e1689)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_6db58c34f0db48ebc853d053b7c21ae6f4975c2e5e6717bd7a1ac2af06e535b6 = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0f5cb9ff5ce47910234ec5958dc12ddfcc73279a5862cc4dce98766c7eb12a3f = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_0f5cb9ff5ce47910234ec5958dc12ddfcc73279a5862cc4dce98766c7eb12a3f || 0 === strpos($__internal_6db58c34f0db48ebc853d053b7c21ae6f4975c2e5e6717bd7a1ac2af06e535b6, $__internal_0f5cb9ff5ce47910234ec5958dc12ddfcc73279a5862cc4dce98766c7eb12a3f)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_f5e192f4d4af7c5d0835aea3c0f799fa5a80e1a10ae4d7f3323839b92b21b209 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5b1ed69d17c9cccabeeadaec574dcfd4141785d2b503130d3b465bc09401106e = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_5b1ed69d17c9cccabeeadaec574dcfd4141785d2b503130d3b465bc09401106e || 0 === strpos($__internal_f5e192f4d4af7c5d0835aea3c0f799fa5a80e1a10ae4d7f3323839b92b21b209, $__internal_5b1ed69d17c9cccabeeadaec574dcfd4141785d2b503130d3b465bc09401106e)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_45316e8c80cab30c5ab8d781f9c31d0d4a7648e13e6b549b36a4cf4dae8498de = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_7c090efc0d0e56d97ca765a2ad8687899a347814f9a0f4a090bfbfccbcfd847d = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_7c090efc0d0e56d97ca765a2ad8687899a347814f9a0f4a090bfbfccbcfd847d || 0 === strpos($__internal_45316e8c80cab30c5ab8d781f9c31d0d4a7648e13e6b549b36a4cf4dae8498de, $__internal_7c090efc0d0e56d97ca765a2ad8687899a347814f9a0f4a090bfbfccbcfd847d)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_21fee3563ae41c9b928dfae2b83349b6338431e80c0e64f1247a5e7616627da3 = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b405bbf51313f14290b65991907537e70ea4e068d0a1cab633f4b736a6061a8b = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_b405bbf51313f14290b65991907537e70ea4e068d0a1cab633f4b736a6061a8b || 0 === strpos($__internal_21fee3563ae41c9b928dfae2b83349b6338431e80c0e64f1247a5e7616627da3, $__internal_b405bbf51313f14290b65991907537e70ea4e068d0a1cab633f4b736a6061a8b)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_8b54b00bac32bd8498e5a91b959d4d536c4ca67b9ff341a90cf3cfef1de6cb12 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c7f3547e428396d845d5a2eb816f65d495bd556320b203e801eb8b995ac43a9e = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_c7f3547e428396d845d5a2eb816f65d495bd556320b203e801eb8b995ac43a9e || 0 === strpos($__internal_8b54b00bac32bd8498e5a91b959d4d536c4ca67b9ff341a90cf3cfef1de6cb12, $__internal_c7f3547e428396d845d5a2eb816f65d495bd556320b203e801eb8b995ac43a9e)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_a516bf496561450ada9ac8530473eff3670e64f3aa94e2dfa66a5b36cef462c3 = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_52f2a437572ec8f0e1d3fe0d3326d1d26d726ca8e568a90dc6e195e032bd3405 = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_52f2a437572ec8f0e1d3fe0d3326d1d26d726ca8e568a90dc6e195e032bd3405 || 0 === strpos($__internal_a516bf496561450ada9ac8530473eff3670e64f3aa94e2dfa66a5b36cef462c3, $__internal_52f2a437572ec8f0e1d3fe0d3326d1d26d726ca8e568a90dc6e195e032bd3405)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_d3bb796cef1c76aeec557b7eebeba714dea9b433f74a24496a80a63654f1a3eb = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_94addf0d7603f198fa408d2a8b7797f99d64254455f1d850c4a5cafcb4825980 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_94addf0d7603f198fa408d2a8b7797f99d64254455f1d850c4a5cafcb4825980 || 0 === strpos($__internal_d3bb796cef1c76aeec557b7eebeba714dea9b433f74a24496a80a63654f1a3eb, $__internal_94addf0d7603f198fa408d2a8b7797f99d64254455f1d850c4a5cafcb4825980)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_5d20686b889734e66086309d3ba086eb419a2cd055b7eb4b79c4b4407a53f821 = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8abf46505fb86a59fa87e1019f52c25a3b45d6a79ea65d3d5687c306f8c96409 = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_8abf46505fb86a59fa87e1019f52c25a3b45d6a79ea65d3d5687c306f8c96409 || 0 === strpos($__internal_5d20686b889734e66086309d3ba086eb419a2cd055b7eb4b79c4b4407a53f821, $__internal_8abf46505fb86a59fa87e1019f52c25a3b45d6a79ea65d3d5687c306f8c96409)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_71a40365586a40545d0654d0847d0ded8a09b92d7ef010b353acd191cd284a5b = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8862a5656f27d2d62570b9b2065030b00cb19eeb8363528b55dc9c4eafa1b6c1 = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_8862a5656f27d2d62570b9b2065030b00cb19eeb8363528b55dc9c4eafa1b6c1 || 0 === strpos($__internal_71a40365586a40545d0654d0847d0ded8a09b92d7ef010b353acd191cd284a5b, $__internal_8862a5656f27d2d62570b9b2065030b00cb19eeb8363528b55dc9c4eafa1b6c1)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_ff5834f9e2310f61d9b7c2b83f3df4970bff25670dfa6dbbc15e96f90324e52d = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_008ca387645ecae190a9fe7f8d33b0174a6e02aaffc3642305ac879d820da505 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_008ca387645ecae190a9fe7f8d33b0174a6e02aaffc3642305ac879d820da505 || 0 === strpos($__internal_ff5834f9e2310f61d9b7c2b83f3df4970bff25670dfa6dbbc15e96f90324e52d, $__internal_008ca387645ecae190a9fe7f8d33b0174a6e02aaffc3642305ac879d820da505)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_7eaef645bff1d8f058f3a42cce12289eb6e3f5365dbf7b875710b5c79329feee = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2c856312e30a1db2af2c2c454dfe0e04fb0f92866d09337e6ca1ee287d7d114c = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_2c856312e30a1db2af2c2c454dfe0e04fb0f92866d09337e6ca1ee287d7d114c || 0 === strpos($__internal_7eaef645bff1d8f058f3a42cce12289eb6e3f5365dbf7b875710b5c79329feee, $__internal_2c856312e30a1db2af2c2c454dfe0e04fb0f92866d09337e6ca1ee287d7d114c)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_e61b92de46391f3125572acc704529a0e562bd704d4b185e67db54c1e288d6df = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_9406bc658429cbf82f44cea9474976ed806ec505e9b8bdab12c3b9742cb4da1d = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_9406bc658429cbf82f44cea9474976ed806ec505e9b8bdab12c3b9742cb4da1d || 0 === strpos($__internal_e61b92de46391f3125572acc704529a0e562bd704d4b185e67db54c1e288d6df, $__internal_9406bc658429cbf82f44cea9474976ed806ec505e9b8bdab12c3b9742cb4da1d)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_bd5218cc78408046070a78fc4ff41066c6d5c9ca31b4655449e45e10d6d6c649 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_480424b01cdfa38517c90e52820f96e395a5b2433f17d78241bd54a8bc06a2d8 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_480424b01cdfa38517c90e52820f96e395a5b2433f17d78241bd54a8bc06a2d8 || 0 === strpos($__internal_bd5218cc78408046070a78fc4ff41066c6d5c9ca31b4655449e45e10d6d6c649, $__internal_480424b01cdfa38517c90e52820f96e395a5b2433f17d78241bd54a8bc06a2d8)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_b691497082f29248958b052d1ef7972b6aa430348bf19b59e3326577e281e742 = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_49503f51d9b64b513c29434286d836c5b3469d6b9bd3334d0c79ececc43199c9 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_49503f51d9b64b513c29434286d836c5b3469d6b9bd3334d0c79ececc43199c9 || 0 === strpos($__internal_b691497082f29248958b052d1ef7972b6aa430348bf19b59e3326577e281e742, $__internal_49503f51d9b64b513c29434286d836c5b3469d6b9bd3334d0c79ececc43199c9)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_22c4afb861506e07ff077d6078459fe8d2ab7cd307041f3f22ba8fa854d8cecb = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ed6472883ff1b26186b2a8fdbb2d99983a7087fd7bc1dd0287759d820e3dfd9f = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_ed6472883ff1b26186b2a8fdbb2d99983a7087fd7bc1dd0287759d820e3dfd9f || 0 === strpos($__internal_22c4afb861506e07ff077d6078459fe8d2ab7cd307041f3f22ba8fa854d8cecb, $__internal_ed6472883ff1b26186b2a8fdbb2d99983a7087fd7bc1dd0287759d820e3dfd9f)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_d69917ea2ef6310ac6fb21727b141e975f740f3ac26370ca0e9dff278ba66e1d = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_99995d87aa835983f0dc395d72dbe1090fe16bce89297d58796f0eb8591e1297 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_99995d87aa835983f0dc395d72dbe1090fe16bce89297d58796f0eb8591e1297 || 0 === strpos($__internal_d69917ea2ef6310ac6fb21727b141e975f740f3ac26370ca0e9dff278ba66e1d, $__internal_99995d87aa835983f0dc395d72dbe1090fe16bce89297d58796f0eb8591e1297)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_2781b30e046740c625af850bbedf2db3d3d953c01f51c2dfe624a7eceaf6c42b = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_fb50e1361db6ea1952c79c5f22984fa5cb1a90431cf80f956f14337d337a7896 = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_fb50e1361db6ea1952c79c5f22984fa5cb1a90431cf80f956f14337d337a7896 || 0 === strpos($__internal_2781b30e046740c625af850bbedf2db3d3d953c01f51c2dfe624a7eceaf6c42b, $__internal_fb50e1361db6ea1952c79c5f22984fa5cb1a90431cf80f956f14337d337a7896)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_aa7b7174e53ba08db94728bbfc1b706a3b3c762b156444a79066ffda367cbdc7 = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_659b019f53abb43c1e9ff901553f056e5c752c981f4c37d9536f92a6774aedb5 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_659b019f53abb43c1e9ff901553f056e5c752c981f4c37d9536f92a6774aedb5 || 0 === strpos($__internal_aa7b7174e53ba08db94728bbfc1b706a3b3c762b156444a79066ffda367cbdc7, $__internal_659b019f53abb43c1e9ff901553f056e5c752c981f4c37d9536f92a6774aedb5)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_19058561653e1fe843b4c1c17aae845a9b9777b3d279c2be180afedb17a69742 = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2af48b04b3dca7d5896601a1eee601bc95998483d2bf0e9d134de15c54a42520 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_2af48b04b3dca7d5896601a1eee601bc95998483d2bf0e9d134de15c54a42520 || 0 === strpos($__internal_19058561653e1fe843b4c1c17aae845a9b9777b3d279c2be180afedb17a69742, $__internal_2af48b04b3dca7d5896601a1eee601bc95998483d2bf0e9d134de15c54a42520)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_2984d1f35f12fb32f2736d8f8ccdb48a09e46f8eb4d44e00e5d78a13c33dadb6 = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b1c760bf0dce86c766ae8a08ac6596c208fe09cb3b56e66fe1f1d10e974aad2e = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_b1c760bf0dce86c766ae8a08ac6596c208fe09cb3b56e66fe1f1d10e974aad2e || 0 === strpos($__internal_2984d1f35f12fb32f2736d8f8ccdb48a09e46f8eb4d44e00e5d78a13c33dadb6, $__internal_b1c760bf0dce86c766ae8a08ac6596c208fe09cb3b56e66fe1f1d10e974aad2e)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_f7d43cb4b659e7473fa8833ee70a57a19a1babc039865241f828c25a1503753b = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b03e281541c2b879ea197ab30361d9d586f98499989b633e5c69360ae82a3fa9 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_b03e281541c2b879ea197ab30361d9d586f98499989b633e5c69360ae82a3fa9 || 0 === strpos($__internal_f7d43cb4b659e7473fa8833ee70a57a19a1babc039865241f828c25a1503753b, $__internal_b03e281541c2b879ea197ab30361d9d586f98499989b633e5c69360ae82a3fa9)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_4f4a5cb25264d146f3427c62a7a16d0f045fcc43987bed56eb62f028beccd75d = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b3a90c1e809bea8ca6da829603af2481b5d729a11f2a68bee09bbd1ccfe90855 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_b3a90c1e809bea8ca6da829603af2481b5d729a11f2a68bee09bbd1ccfe90855 || 0 === strpos($__internal_4f4a5cb25264d146f3427c62a7a16d0f045fcc43987bed56eb62f028beccd75d, $__internal_b3a90c1e809bea8ca6da829603af2481b5d729a11f2a68bee09bbd1ccfe90855)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_9be8cde0920ed64a2f31fb27edb3916f62c10a4c01f20f7b01a0c3e24e6c2c9a = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_23fc92bb19d2aacee7288ef4869129b600040ab30240b111a56af3723e1ec09c = "http://www.businessbid.ae/node2") && ('' === $__internal_23fc92bb19d2aacee7288ef4869129b600040ab30240b111a56af3723e1ec09c || 0 === strpos($__internal_9be8cde0920ed64a2f31fb27edb3916f62c10a4c01f20f7b01a0c3e24e6c2c9a, $__internal_23fc92bb19d2aacee7288ef4869129b600040ab30240b111a56af3723e1ec09c)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_bc4d98f82dfa1c8ee1538048ea17d81b85557824d14238936a6cde94b43a3bb3 = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_208d2d05772357ad98dbacb131e92953b24bfe34af1ba922f59d0a48062cf8a0 = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_208d2d05772357ad98dbacb131e92953b24bfe34af1ba922f59d0a48062cf8a0 || 0 === strpos($__internal_bc4d98f82dfa1c8ee1538048ea17d81b85557824d14238936a6cde94b43a3bb3, $__internal_208d2d05772357ad98dbacb131e92953b24bfe34af1ba922f59d0a48062cf8a0)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_a46cce628bac2bf07dbbd0e3f8a71da73861a74716461f1e6d1c817388fbc59b = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_513f29bc262fa55ac7585f36c7a4356cb28b2544dcf169a5bf2a4190b99404df = "http://www.businessbid.ae/faq") && ('' === $__internal_513f29bc262fa55ac7585f36c7a4356cb28b2544dcf169a5bf2a4190b99404df || 0 === strpos($__internal_a46cce628bac2bf07dbbd0e3f8a71da73861a74716461f1e6d1c817388fbc59b, $__internal_513f29bc262fa55ac7585f36c7a4356cb28b2544dcf169a5bf2a4190b99404df)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_99c52c80c2fbadb49fe898072f4a728fdbfab1ff3651cf5d6974fb4a5e187f2a = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6ea925f62dde6e048f0bcb9965802fd34858c84cc7a0be2d4516f6bba8ad2ac9 = "http://www.businessbid.ae/node4") && ('' === $__internal_6ea925f62dde6e048f0bcb9965802fd34858c84cc7a0be2d4516f6bba8ad2ac9 || 0 === strpos($__internal_99c52c80c2fbadb49fe898072f4a728fdbfab1ff3651cf5d6974fb4a5e187f2a, $__internal_6ea925f62dde6e048f0bcb9965802fd34858c84cc7a0be2d4516f6bba8ad2ac9)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_bda6daa73dda4d2da28b28d3bc2d01a7018ce83e1010ea70287100fc4af8afc3 = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_3e893759d01bbe255ba2defa956f419e9c4a568ae923c46101b34884b4e5e069 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_3e893759d01bbe255ba2defa956f419e9c4a568ae923c46101b34884b4e5e069 || 0 === strpos($__internal_bda6daa73dda4d2da28b28d3bc2d01a7018ce83e1010ea70287100fc4af8afc3, $__internal_3e893759d01bbe255ba2defa956f419e9c4a568ae923c46101b34884b4e5e069)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_e9ed2cad77b7166458ef126f7b712925522e1a851074d2b552f119a179c78c23 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_ad3b9b0043c9cf65f4d33b91d78cff5ab00904da65bfd652f6c15097e6c5b78e = "http://www.businessbid.ae/node6") && ('' === $__internal_ad3b9b0043c9cf65f4d33b91d78cff5ab00904da65bfd652f6c15097e6c5b78e || 0 === strpos($__internal_e9ed2cad77b7166458ef126f7b712925522e1a851074d2b552f119a179c78c23, $__internal_ad3b9b0043c9cf65f4d33b91d78cff5ab00904da65bfd652f6c15097e6c5b78e)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_98f936ec9f3f15654a7bd1a261bfb501e3bdf1cf9c29796cb922c7fca54e9943 = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_903bebdf0c8755fb193860c10f1c76ec7634ed9c5adcd0c931cd1f19e69c4b14 = "http://www.businessbid.ae/feedback") && ('' === $__internal_903bebdf0c8755fb193860c10f1c76ec7634ed9c5adcd0c931cd1f19e69c4b14 || 0 === strpos($__internal_98f936ec9f3f15654a7bd1a261bfb501e3bdf1cf9c29796cb922c7fca54e9943, $__internal_903bebdf0c8755fb193860c10f1c76ec7634ed9c5adcd0c931cd1f19e69c4b14)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_583c1e531804e5f9b712c7f3906ad979a2dcba1f38b86c0ce8a153334da00a19 = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_efc0b35d82e7b1447aecfe8c6a6cf4cb07c059a0a90af9c8b0e270f9381a6dce = "http://www.businessbid.ae/sitemap") && ('' === $__internal_efc0b35d82e7b1447aecfe8c6a6cf4cb07c059a0a90af9c8b0e270f9381a6dce || 0 === strpos($__internal_583c1e531804e5f9b712c7f3906ad979a2dcba1f38b86c0ce8a153334da00a19, $__internal_efc0b35d82e7b1447aecfe8c6a6cf4cb07c059a0a90af9c8b0e270f9381a6dce)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_af941d3bb6b846a7218b0b6565920e0729b88857f018ada07601bb9c57e1d2ca = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1b6519a738d2aab2782ee39c40b796a2ac106981386272ee08ae638120050a48 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_1b6519a738d2aab2782ee39c40b796a2ac106981386272ee08ae638120050a48 || 0 === strpos($__internal_af941d3bb6b846a7218b0b6565920e0729b88857f018ada07601bb9c57e1d2ca, $__internal_1b6519a738d2aab2782ee39c40b796a2ac106981386272ee08ae638120050a48)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_6b2f0e3de9b2ec1fdb4b604fe272944f31d4200b46d1157f44c7b987094c4b01 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0ed87848c688b6bc54d0a368544601fd7648188a695e02192d351be213f63bed = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_0ed87848c688b6bc54d0a368544601fd7648188a695e02192d351be213f63bed || 0 === strpos($__internal_6b2f0e3de9b2ec1fdb4b604fe272944f31d4200b46d1157f44c7b987094c4b01, $__internal_0ed87848c688b6bc54d0a368544601fd7648188a695e02192d351be213f63bed)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_31a3866a6302eee70fb3d4a21afb11a5bad663e9cf870e9a4f95ffbda7971d96 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_47f846eadfd5ebe84601cc881655db814ccf88146259cb29f6edaf308ab8934a = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_47f846eadfd5ebe84601cc881655db814ccf88146259cb29f6edaf308ab8934a || 0 === strpos($__internal_31a3866a6302eee70fb3d4a21afb11a5bad663e9cf870e9a4f95ffbda7971d96, $__internal_47f846eadfd5ebe84601cc881655db814ccf88146259cb29f6edaf308ab8934a)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_f247700ba7358cc292c45866f1899c78075fce70bbc72bae3fc7d5fd848df4bd = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f69b452de6b082b31815282adbbcecd3f1be3e68cc34beecb958d2f1e4b4971c = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_f69b452de6b082b31815282adbbcecd3f1be3e68cc34beecb958d2f1e4b4971c || 0 === strpos($__internal_f247700ba7358cc292c45866f1899c78075fce70bbc72bae3fc7d5fd848df4bd, $__internal_f69b452de6b082b31815282adbbcecd3f1be3e68cc34beecb958d2f1e4b4971c)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 162
    public function block_customcss($context, array $blocks = array())
    {
        // line 163
        echo "
";
    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        // line 167
        echo "<script>
\$(document).ready(function(){
    ";
        // line 169
        if (array_key_exists("keyword", $context)) {
            // line 170
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 174
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 310
    public function block_customjs($context, array $blocks = array())
    {
        // line 311
        echo "
";
    }

    // line 314
    public function block_jquery($context, array $blocks = array())
    {
        // line 315
        echo "
";
    }

    // line 317
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 318
        echo "
<!-- Google Analytics -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PZXQ9P');</script>

";
    }

    // line 402
    public function block_maincontent($context, array $blocks = array())
    {
        // line 403
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 439
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 490
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 530
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 560
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 561
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 563
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 565
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 571
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 572
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 574
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 576
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 581
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 582
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 584
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 586
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 593
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 594
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 596
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 598
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 604
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 605
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 607
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 609
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 615
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 616
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 618
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 620
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 490
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 491
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 500
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 501
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 502
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 503
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 504
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 505
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 506
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 508
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 509
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 510
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 512
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 519
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 525
        echo "    </div>
            </div>


    ";
    }

    // line 519
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 520
        echo "    <div class=\"request\">
    <h2>";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 636
    public function block_footer($context, array $blocks = array())
    {
        // line 637
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 656
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 666
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 675
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 687
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 688
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 696
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 740
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 742
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 752
    public function block_footerScript($context, array $blocks = array())
    {
        // line 753
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 754
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1411 => 754,  1406 => 753,  1403 => 752,  1392 => 742,  1387 => 740,  1347 => 703,  1339 => 698,  1335 => 697,  1331 => 696,  1320 => 688,  1317 => 687,  1313 => 685,  1304 => 679,  1300 => 678,  1296 => 677,  1292 => 676,  1288 => 675,  1277 => 667,  1273 => 666,  1264 => 660,  1260 => 659,  1256 => 658,  1252 => 657,  1248 => 656,  1227 => 637,  1224 => 636,  1216 => 521,  1213 => 520,  1210 => 519,  1202 => 525,  1200 => 519,  1193 => 514,  1186 => 512,  1170 => 510,  1167 => 509,  1164 => 508,  1150 => 506,  1147 => 505,  1144 => 504,  1142 => 503,  1138 => 502,  1135 => 501,  1131 => 500,  1121 => 493,  1117 => 491,  1114 => 490,  1098 => 620,  1092 => 618,  1086 => 616,  1084 => 615,  1076 => 609,  1070 => 607,  1064 => 605,  1062 => 604,  1054 => 598,  1048 => 596,  1042 => 594,  1040 => 593,  1031 => 586,  1025 => 584,  1019 => 582,  1017 => 581,  1010 => 576,  1004 => 574,  998 => 572,  996 => 571,  988 => 565,  982 => 563,  976 => 561,  974 => 560,  942 => 530,  940 => 490,  925 => 478,  914 => 470,  903 => 462,  892 => 454,  881 => 446,  871 => 439,  853 => 424,  843 => 417,  833 => 410,  824 => 403,  821 => 402,  813 => 318,  810 => 317,  805 => 315,  802 => 314,  797 => 311,  794 => 310,  775 => 294,  653 => 174,  647 => 170,  645 => 169,  641 => 167,  638 => 166,  633 => 163,  630 => 162,  626 => 127,  623 => 126,  617 => 109,  613 => 107,  611 => 106,  608 => 105,  606 => 104,  603 => 103,  601 => 102,  598 => 101,  596 => 100,  593 => 99,  591 => 98,  588 => 97,  586 => 96,  583 => 95,  581 => 94,  578 => 93,  576 => 92,  573 => 91,  571 => 90,  568 => 89,  566 => 88,  563 => 87,  561 => 86,  558 => 85,  556 => 84,  553 => 83,  551 => 82,  548 => 81,  546 => 80,  543 => 79,  541 => 78,  538 => 77,  536 => 76,  533 => 75,  531 => 74,  528 => 73,  526 => 72,  523 => 71,  521 => 70,  518 => 69,  516 => 68,  513 => 67,  511 => 66,  508 => 65,  506 => 64,  503 => 63,  501 => 62,  498 => 61,  496 => 60,  493 => 59,  491 => 58,  488 => 57,  486 => 56,  483 => 55,  481 => 54,  478 => 53,  476 => 52,  473 => 51,  471 => 50,  468 => 49,  466 => 48,  463 => 47,  461 => 46,  458 => 45,  456 => 44,  453 => 43,  451 => 42,  448 => 41,  446 => 40,  443 => 39,  441 => 38,  438 => 37,  436 => 36,  433 => 35,  431 => 34,  428 => 33,  426 => 32,  423 => 31,  421 => 30,  418 => 29,  416 => 28,  413 => 27,  411 => 26,  408 => 25,  406 => 24,  403 => 23,  401 => 22,  398 => 21,  396 => 20,  393 => 19,  391 => 18,  388 => 17,  386 => 16,  383 => 15,  381 => 14,  378 => 13,  376 => 12,  373 => 11,  368 => 763,  359 => 756,  357 => 752,  354 => 749,  352 => 636,  347 => 633,  345 => 402,  336 => 395,  322 => 392,  319 => 391,  317 => 390,  311 => 387,  307 => 385,  305 => 384,  297 => 379,  289 => 374,  284 => 372,  278 => 369,  273 => 367,  269 => 366,  260 => 359,  244 => 354,  240 => 353,  236 => 352,  232 => 350,  230 => 349,  224 => 346,  218 => 345,  214 => 343,  212 => 342,  201 => 336,  194 => 331,  192 => 330,  190 => 329,  188 => 328,  186 => 327,  182 => 325,  180 => 324,  177 => 323,  175 => 317,  173 => 314,  170 => 313,  168 => 310,  165 => 309,  163 => 166,  160 => 165,  158 => 162,  153 => 160,  149 => 159,  140 => 153,  135 => 151,  131 => 150,  126 => 148,  122 => 147,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

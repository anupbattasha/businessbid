<?php

/* BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig */
class __TwigTemplate_cd0a37f6f4cf154529367f319116eec10f296081e2dbdf6a93c323996cab3d5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "

<script>
\$(document).ready(function(){
\$('#logo-form').hide();
\$('.albumform').hide();
\t\$('#changeimage').click(function(){

\t\t\$('#logo-form').show(); 
\t});
\t\$('#newalbum').click(function(){
\t\t\$('.albumform').show(); });

\t});

</script>
<div class=\"container inner_container\">
<div class=\"col-md-9 dashboard-rightpanel user-profile-basic\">
\t<div class=\"page-title\"><h1>Update Password</h1></div>
\t<div class=\"row user-profile-block\">
\t\t<div class=\"user-profile-basic\">
\t\t\t
\t\t\t\t
\t\t\t<div class=\"row\">
\t\t\t ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 30
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 31
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div> 
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
\t";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 36
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 37
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>\t
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "\t
\t";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 42
            echo "
\t<div class=\"alert alert-success\">";
            // line 43
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "\t\t\t\t<div class=\"tab-content\">
\t\t\t\t  <div class=\"tab-pane active profile-inform\" id=\"profile\">
\t\t\t\t<div class=\"col-md-8 profile-info\">
\t\t\t\t";
        // line 49
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["passwordform"]) ? $context["passwordform"] : null), 'form_start');
        echo "
\t\t\t\t";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["passwordform"]) ? $context["passwordform"] : null), 'errors');
        echo "
\t\t\t\t\t<div class=\"form-item\">";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "password", array()), 'label');
        echo "<span class=\"req\">*</span> :<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "password", array()), 'widget');
        echo "</div></div>
\t\t\t\t\t<div class=\"form-item\">";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "npassword", array()), 'label');
        echo "<span class=\"req\">*</span> :<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "npassword", array()), 'widget');
        echo " </div></div>
\t\t\t\t\t<div class=\"form-item\">";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "cpassword", array()), 'label');
        echo "<span class=\"req\">*</span> :<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "cpassword", array()), 'widget');
        echo " </div></div>
\t\t\t\t\t<div class=\"form-item pull-right text-right\">";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["passwordform"]) ? $context["passwordform"] : null), "Submit", array()), 'widget');
        echo "</div>
\t\t\t\t\t
\t\t\t\t";
        // line 56
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["passwordform"]) ? $context["passwordform"] : null), 'form_end');
        echo "
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>
\t\t</div>
\t\t
\t

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:venpasswordcheck.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 56,  142 => 54,  136 => 53,  130 => 52,  124 => 51,  120 => 50,  116 => 49,  111 => 46,  102 => 43,  99 => 42,  95 => 41,  92 => 40,  83 => 37,  80 => 36,  76 => 35,  73 => 34,  64 => 31,  61 => 30,  57 => 29,  31 => 5,  28 => 4,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Admin:cheque_pickup.html.twig */
class __TwigTemplate_f3793f18e1f6b50c0bc5b97fecf579ce9557ece77bd595f73813ba6edf46db0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " <style type='text/css'>
.tg
{
    border-collapse:collapse;border-spacing:0;border-color:#ccc
}
.tg td
{
    font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;
    border-width:1px;
    overflow:hidden;
    word-break:normal;
    border-color:#ccc;
    color:#333;
    background-color:#fff
}
.tg th
{
    font-family:Arial,sans-serif;
    font-size:14px;
    font-weight:400;
    padding:10px 5px;
    border-style:solid;
    border-width:1px;
    overflow:hidden;
    word-break:normal;
    border-color:#ccc;
    color:#333;
    background-color:#f0f0f0
}
.tg .tg-4eph
{
    background-color:#f9f9f9
}
</style>
<table class='tg'>
    <tr>
        <th class='tg-031e' colspan='2'>
            Cheque Pickup Details
        </th>
    </tr>
    <tr>
        <td class='tg-4eph'>
            Company Name :
        </td>
        <td class='tg-4eph'>
            ";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["vendorCompName"]) ? $context["vendorCompName"] : null), "html", null, true);
        echo "
        </td>
    </tr>
    <tr>
        <td class='tg-031e'>
            Address :
        </td>
        <td class='tg-031e'>
            ";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "address", array(), "array"), "html", null, true);
        echo "
        </td>
    </tr>
    <tr>
        <td class='tg-4eph'>
            City :
        </td>
        <td class='tg-4eph'>
            ";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "city", array(), "array"), "html", null, true);
        echo "
        </td>
    </tr>
    <tr>
        <td class='tg-031e'>
            Contact Number :
        </td>
        <td class='tg-031e'>
            ";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "contactnumber", array(), "array"), "html", null, true);
        echo "
        </td>
    </tr>
    <tr>
        <td class='tg-4eph'>
            Contact Person :
        </td>
        <td class='tg-4eph'>
            ";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "contactperson", array(), "array"), "html", null, true);
        echo "
        </td>
    </tr>
    ";
        // line 81
        if ($this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "bankname", array(), "array", true, true)) {
            // line 82
            echo "    <tr>
        <td class='tg-031e'>
            Bank Name :
        </td>
        <td class='tg-031e'>
            ";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "bankname", array(), "array"), "html", null, true);
            echo "
        </td>
    </tr>
    <tr>
        <td class='tg-031e'>
            Check Number :
        </td>
        <td class='tg-031e'>
            ";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formData"]) ? $context["formData"] : null), "checknumber", array(), "array"), "html", null, true);
            echo "
        </td>
    </tr>
    ";
        }
        // line 99
        echo "</table>";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Admin:cheque_pickup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 99,  136 => 95,  125 => 87,  118 => 82,  116 => 81,  110 => 78,  99 => 70,  88 => 62,  77 => 54,  66 => 46,  19 => 1,);
    }
}

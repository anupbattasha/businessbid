<?php

/* BBidsBBidsHomeBundle:Home:node5.html.twig */
class __TwigTemplate_c352a90ddedfd23b5e912d0f0acfec9c2a035c8957c6cf846ca22a92aa1afc3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node5.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 5
    public function block_maincontent($context, array $blocks = array())
    {
        // line 6
        echo "
<script>
\t\$(document).ready(function(){
\t\$('span[id^=\"contactdetailsdiv\"]').hide();
\t\$('input[id^=\"contactdetails-\"]').click(function(){
\t\t\tvar btid = \$(this).attr('id');
\t\t\tvar arr = btid.split('-');


\t\t\$('#contactdetails-'+arr[1]).hide();
\t\t\$('#contactdetailsdiv'+arr[1]).show();

\t});

\t\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();
});


</script>
<div class=\"container\">
\t<div class=\"breadcrum\">Home / Vendor Review & Rating / ";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo "</div>
\t<div class=\"review-list-filter row\">
\t\t<div class=\"col-md-4 review-cat\"><input type=\"text\" readonly  name=\"\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo "\"></div>
\t\t<div class=\"col-md-4 review-city\"><input type=\"text\" readonly  name=\"\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["city"]) ? $context["city"] : $this->getContext($context, "city")), "html", null, true);
        echo "\"></div>
\t\t<div class=\"col-md-4 review-sort form-horizontal\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"col-sm-4 control-label\">Sort By</div>
\t\t\t\t\t<div class=\"col-sm-8\"> ";
        // line 47
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "filter", array()), 'widget');
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo " </div>
\t\t\t\t</div>
\t\t </div>
\t</div>



\t<div class=\"review-list-container\">
\t\t<div class=\"review-list-title\">";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo "</div>
\t\t";
        // line 56
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["vendors"]) ? $context["vendors"] : $this->getContext($context, "vendors")));
        foreach ($context['_seq'] as $context["i"] => $context["vendor"]) {
            // line 57
            echo "\t\t<div class=\"review-list-block\">
\t\t\t<div class=\"list-block row\">
\t\t\t\t<h2><a href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => $this->getAttribute($context["vendor"], "userid", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["vendor"], "bizname", array()), "html", null, true);
            echo "</a></h2>
\t\t\t\t<div class=\"col-md-2 verified-block\">

\t\t\t\t\t\t<span class=\"stars\">";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["vendor"], "rating", array()), "html", null, true);
            echo "</span>";
            echo twig_escape_filter($this->env, twig_round($this->getAttribute($context["vendor"], "rating", array()), 1, "floor"), "html", null, true);
            echo " ";
            if (($this->getAttribute($context["vendor"], "rating", array()) == 1)) {
                echo " star ";
            } else {
                echo " stars ";
            }
            // line 63
            echo "\t\t\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => $this->getAttribute($context["vendor"], "userid", array()))), "html", null, true);
            echo "#rating\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["vendor"], 1, array()), "html", null, true);
            echo " Verified Reviews</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-7 description-block\">
\t\t\t\t\t<p>";
            // line 66
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["vendor"], "description", array()), 0, 200), "html", null, true);
            echo "...<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor", array("userid" => $this->getAttribute($context["vendor"], "userid", array()))), "html", null, true);
            echo "\">See more</a></p>

\t\t\t\t</div>


\t\t\t\t<div class=\"col-md-3 get-quote-block\">
\t\t\t\t\t<input class=\"btn btn-success btn-block getQuotes\" type=\"submit\" onclick=\"window.open('";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => (isset($context["categoryid"]) ? $context["categoryid"] : $this->getContext($context, "categoryid")))), "html", null, true);
            echo "', '_self');\" value=\"Get a Quotes\">
\t\t\t\t\t<span id=\"contactdetailsdiv";
            // line 73
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\"><p><label>Address</label> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["vendor"], "address", array()), "html", null, true);
            echo "<p><label>SMS Phone</label> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["vendor"], "smsphone", array()), "html", null, true);
            echo "</p></span><input class=\"btn btn-success btn-block getContact\" type=\"submit\" id=\"contactdetails-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" value=\"Get Contact Details\">
\t\t\t\t</div>

\t\t\t</div>

\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['vendor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "\t</div>
\t";
        // line 81
        if ((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count"))) {
            // line 82
            echo "<div class=\"row text-right \">
\t<div class=\"counter-box top-space\">
\t";
            // line 84
            $context["page_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 10)));
            // line 85
            echo "<ul class=\"pagination\">

  <li><a href=\"";
            // line 87
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("offset" => 1, "cid" => (isset($context["categoryid"]) ? $context["categoryid"] : $this->getContext($context, "categoryid")), "city" => (isset($context["city"]) ? $context["city"] : $this->getContext($context, "city")))), "html", null, true);
            echo "\">&laquo;</a></li>
";
            // line 88
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["page_count"]) ? $context["page_count"] : $this->getContext($context, "page_count"))));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 89
                echo "
  <li><a href=\"";
                // line 90
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("offset" => $context["i"], "cid" => (isset($context["categoryid"]) ? $context["categoryid"] : $this->getContext($context, "categoryid")), "city" => (isset($context["city"]) ? $context["city"] : $this->getContext($context, "city")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</a></li>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "
  <li><a href=\"";
            // line 94
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewform_post", array("offset" =>  -(isset($context["page_count"]) ? $context["page_count"] : $this->getContext($context, "page_count")), "cid" => (isset($context["categoryid"]) ? $context["categoryid"] : $this->getContext($context, "categoryid")), "city" => (isset($context["city"]) ? $context["city"] : $this->getContext($context, "city")))), "html", null, true);
            echo "\">&raquo;</a></li>
</ul>
</div>
</div>
";
        }
        // line 99
        echo "
\t<div class=\"row get-matched-block\">
\t\t<div class=\"col-md-3 get-matched-img\"></div>
\t\t<div class=\"col-md-6 get-matched-cont\">
\t\t\t<h3>Get matched Right Away</h3>
\t\t\t<p>Use our vendor match technology to receive quotations from the most appropriate vendors immediately.</p>
\t\t</div>
\t\t<div class=\"col-md-3 get-matched-but\"><input class=\"btn btn-success btn-block\" type=\"submit\" onclick=\"window.open('";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["categoryid"]) ? $context["categoryid"] : $this->getContext($context, "categoryid")))), "html", null, true);
        echo "', '_self');\" value=\"Get Matched to Pros\"></div>
\t</div>
</div>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node5.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 106,  224 => 99,  216 => 94,  213 => 93,  202 => 90,  199 => 89,  195 => 88,  191 => 87,  187 => 85,  185 => 84,  181 => 82,  179 => 81,  176 => 80,  157 => 73,  153 => 72,  142 => 66,  133 => 63,  123 => 62,  115 => 59,  111 => 57,  107 => 56,  103 => 55,  89 => 47,  82 => 43,  78 => 42,  73 => 40,  37 => 6,  34 => 5,  29 => 2,  11 => 1,);
    }
}

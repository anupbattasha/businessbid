<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'toprightmenu' => array($this, 'block_toprightmenu'),
            'search' => array($this, 'block_search'),
            'topleftmenu' => array($this, 'block_topleftmenu'),
            'megamenu' => array($this, 'block_megamenu'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<head>
";
        // line 3
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#aboutus");
        // line 4
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#meetteam");
        // line 5
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#career");
        // line 6
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#valuedpartner");
        // line 7
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#vendor");
        // line 8
        echo "
";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 10
            echo "\t<title>Main Home Page</title>
";
        } elseif ((is_string($__internal_c6877f4d85bcf223879f711841ead110c15438e78dcad62fde6a0938c34a0d0a = $this->getAttribute($this->getAttribute(        // line 11
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_13a3973eee11ef611133624c7d30b671e07591438ca8e8037cdad3347bc848f5 = "http://www.businessbid.ae/contactus") && ('' === $__internal_13a3973eee11ef611133624c7d30b671e07591438ca8e8037cdad3347bc848f5 || 0 === strpos($__internal_c6877f4d85bcf223879f711841ead110c15438e78dcad62fde6a0938c34a0d0a, $__internal_13a3973eee11ef611133624c7d30b671e07591438ca8e8037cdad3347bc848f5)))) {
            // line 12
            echo "\t<title>Contact Us Form </title>
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 13
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 14
            echo "\t<title>Account Login</title>
";
        } elseif ((is_string($__internal_80382b6c94a6d14c0e9f11c68aba23e3fb1dd9bba7a5db98bb6ff2038fe7f7ee = $this->getAttribute($this->getAttribute(        // line 15
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_20bf925adb2d6c920f494d36558b8ddaacfbb25c853e52fc203d25aa503775fc = "http://www.businessbid.ae/node3") && ('' === $__internal_20bf925adb2d6c920f494d36558b8ddaacfbb25c853e52fc203d25aa503775fc || 0 === strpos($__internal_80382b6c94a6d14c0e9f11c68aba23e3fb1dd9bba7a5db98bb6ff2038fe7f7ee, $__internal_20bf925adb2d6c920f494d36558b8ddaacfbb25c853e52fc203d25aa503775fc)))) {
            // line 16
            echo "\t<title>How BusinessBid Works for Customers</title>
";
        } elseif ((is_string($__internal_ca182465159cedf348fac15e4366b5b306e76b553c41b835f553477bf6ba47b0 = $this->getAttribute($this->getAttribute(        // line 17
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_4819be8c20a16c4202a0bf90e4569f0f8e2eb282aac25bc88d6657ad33bb1b27 = "http://www.businessbid.ae/review/login") && ('' === $__internal_4819be8c20a16c4202a0bf90e4569f0f8e2eb282aac25bc88d6657ad33bb1b27 || 0 === strpos($__internal_ca182465159cedf348fac15e4366b5b306e76b553c41b835f553477bf6ba47b0, $__internal_4819be8c20a16c4202a0bf90e4569f0f8e2eb282aac25bc88d6657ad33bb1b27)))) {
            // line 18
            echo "\t<title>Write A Review Login</title>
";
        } elseif ((is_string($__internal_5fe37696ac2ff60c51e6c03effa06d4bb06586afab2fde28c329181ae1070c4a = $this->getAttribute($this->getAttribute(        // line 19
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2dd9f4ff90b8ce6e68a4806493f698949ec41573400b10c8c891f74e292b2a20 = "http://www.businessbid.ae/node1") && ('' === $__internal_2dd9f4ff90b8ce6e68a4806493f698949ec41573400b10c8c891f74e292b2a20 || 0 === strpos($__internal_5fe37696ac2ff60c51e6c03effa06d4bb06586afab2fde28c329181ae1070c4a, $__internal_2dd9f4ff90b8ce6e68a4806493f698949ec41573400b10c8c891f74e292b2a20)))) {
            // line 20
            echo "\t<title>Are you A Vendor</title>
";
        } elseif ((is_string($__internal_aa83b2b7c5c0c403903aa31de77ad8e00ac10dd4927f7871980f8e581cecab55 = $this->getAttribute($this->getAttribute(        // line 21
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_41cec0a68920858766267b3fc6b2fc0b34fb319f32daf5d1a015bd89d52c4268 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_41cec0a68920858766267b3fc6b2fc0b34fb319f32daf5d1a015bd89d52c4268 || 0 === strpos($__internal_aa83b2b7c5c0c403903aa31de77ad8e00ac10dd4927f7871980f8e581cecab55, $__internal_41cec0a68920858766267b3fc6b2fc0b34fb319f32daf5d1a015bd89d52c4268)))) {
            // line 22
            echo "\t<title>Accounting & Auditing Quote Form</title>
";
        } elseif ((is_string($__internal_c134fd4557553f9ceb00df14ef9cae54922202e00fe0739837db8f4e13f17718 = $this->getAttribute($this->getAttribute(        // line 23
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_54b0d5a74373fa9211ed067fe70356cc2a3dd15c81ff739b692459f724cdf094 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_54b0d5a74373fa9211ed067fe70356cc2a3dd15c81ff739b692459f724cdf094 || 0 === strpos($__internal_c134fd4557553f9ceb00df14ef9cae54922202e00fe0739837db8f4e13f17718, $__internal_54b0d5a74373fa9211ed067fe70356cc2a3dd15c81ff739b692459f724cdf094)))) {
            // line 24
            echo "\t<title>Get Signage & Signboard Quotes</title>
";
        } elseif ((is_string($__internal_71ba519283f9ab9c632f552bdc12bc0955d919c1980dccc7fe2ae9d55c6bd650 = $this->getAttribute($this->getAttribute(        // line 25
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_5a8f54da62a678dd351e61502e0c10c3c8c44260517cc21a8fc975abbad89835 = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_5a8f54da62a678dd351e61502e0c10c3c8c44260517cc21a8fc975abbad89835 || 0 === strpos($__internal_71ba519283f9ab9c632f552bdc12bc0955d919c1980dccc7fe2ae9d55c6bd650, $__internal_5a8f54da62a678dd351e61502e0c10c3c8c44260517cc21a8fc975abbad89835)))) {
            // line 26
            echo "\t<title>Obtain IT Support Quotes</title>
";
        } elseif ((is_string($__internal_19b6024f768f0ec024574a31efd898394a46c49a7e5b58674d9f3e95c0a56cb1 = $this->getAttribute($this->getAttribute(        // line 27
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_cc11920ea9bc19dc0ad44ac2c8980406e01e3a0ef319a5885f85bbe7d644fc38 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_cc11920ea9bc19dc0ad44ac2c8980406e01e3a0ef319a5885f85bbe7d644fc38 || 0 === strpos($__internal_19b6024f768f0ec024574a31efd898394a46c49a7e5b58674d9f3e95c0a56cb1, $__internal_cc11920ea9bc19dc0ad44ac2c8980406e01e3a0ef319a5885f85bbe7d644fc38)))) {
            // line 28
            echo "\t<title>Easy Way to get Photography & Videos Quotes </title>
";
        } elseif ((is_string($__internal_742fd5e4d264856ca30a288a14f889330efad2db2c1a8af650719b40038eadf1 = $this->getAttribute($this->getAttribute(        // line 29
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_8cbc0087515d9fbad16104f6fea220ec12ade41851d6a8493b356502582429ee = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_8cbc0087515d9fbad16104f6fea220ec12ade41851d6a8493b356502582429ee || 0 === strpos($__internal_742fd5e4d264856ca30a288a14f889330efad2db2c1a8af650719b40038eadf1, $__internal_8cbc0087515d9fbad16104f6fea220ec12ade41851d6a8493b356502582429ee)))) {
            // line 30
            echo "\t<title>Procure Residential Cleaning Quotes Right Here</title>
";
        } elseif ((is_string($__internal_f2e8dc1c02fcc9b70f6a7003d305c428e4a93e9c30fc51891ed309ef29d299c1 = $this->getAttribute($this->getAttribute(        // line 31
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_25c302376318a7bcd7b2221cf275c9a41299b02a67c5ab7cd0a9bf1ae202bd97 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_25c302376318a7bcd7b2221cf275c9a41299b02a67c5ab7cd0a9bf1ae202bd97 || 0 === strpos($__internal_f2e8dc1c02fcc9b70f6a7003d305c428e4a93e9c30fc51891ed309ef29d299c1, $__internal_25c302376318a7bcd7b2221cf275c9a41299b02a67c5ab7cd0a9bf1ae202bd97)))) {
            // line 32
            echo "\t<title>Receive Maintenance Quotes for Free</title>
";
        } elseif ((is_string($__internal_c75c92d49f3ead8ebca182009abbe6c74aeb7e1237701ab8afeb35b10cb45b6e = $this->getAttribute($this->getAttribute(        // line 33
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_dc80f0f9e12fa16fc802fe3885896c53a1bbe3c57b0cb074b39d3896189a7a62 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_dc80f0f9e12fa16fc802fe3885896c53a1bbe3c57b0cb074b39d3896189a7a62 || 0 === strpos($__internal_c75c92d49f3ead8ebca182009abbe6c74aeb7e1237701ab8afeb35b10cb45b6e, $__internal_dc80f0f9e12fa16fc802fe3885896c53a1bbe3c57b0cb074b39d3896189a7a62)))) {
            // line 34
            echo "\t<title>Acquire Interior Design & FitOut Quotes </title>
";
        } elseif ((is_string($__internal_0e08f700fabaadece431bc576c2d08854984b0904835a6e352dd5ce68604b3f3 = $this->getAttribute($this->getAttribute(        // line 35
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_0868a14a6accad910ebe60102b4ffda340189d3d59c305c5229b322dcdf97a3e = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_0868a14a6accad910ebe60102b4ffda340189d3d59c305c5229b322dcdf97a3e || 0 === strpos($__internal_0e08f700fabaadece431bc576c2d08854984b0904835a6e352dd5ce68604b3f3, $__internal_0868a14a6accad910ebe60102b4ffda340189d3d59c305c5229b322dcdf97a3e)))) {
            // line 36
            echo "\t<title>Get Hold of Free Catering Quotes</title>
";
        } elseif ((is_string($__internal_7d432efd014528425520e5b20646a928162c973133235027526293d2129f95ec = $this->getAttribute($this->getAttribute(        // line 37
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ab81a22f68e3f5ffb687fd7d844b937e4c442c4c5b6581196fed261a5fac8d0e = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_ab81a22f68e3f5ffb687fd7d844b937e4c442c4c5b6581196fed261a5fac8d0e || 0 === strpos($__internal_7d432efd014528425520e5b20646a928162c973133235027526293d2129f95ec, $__internal_ab81a22f68e3f5ffb687fd7d844b937e4c442c4c5b6581196fed261a5fac8d0e)))) {
            // line 38
            echo "\t<title>Find Free Landscaping & Gardens Quotes </title>
";
        } elseif ((is_string($__internal_061cde21edd7b7addb5644f6923b2c5ac2c26cf98a4d7445b610d0943cbea8d5 = $this->getAttribute($this->getAttribute(        // line 39
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ddd36a2343bb0658a5307c22c8bc335a20c80105852295994721d295bcf98f07 = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_ddd36a2343bb0658a5307c22c8bc335a20c80105852295994721d295bcf98f07 || 0 === strpos($__internal_061cde21edd7b7addb5644f6923b2c5ac2c26cf98a4d7445b610d0943cbea8d5, $__internal_ddd36a2343bb0658a5307c22c8bc335a20c80105852295994721d295bcf98f07)))) {
            // line 40
            echo "\t<title>Attain Free Offset Printing Quotes from Companies</title>
";
        } elseif ((is_string($__internal_d138df53d58975eb605a237d9add61fb1558a33ef9ff52ed21138061b14a9d8a = $this->getAttribute($this->getAttribute(        // line 41
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_b68f58f49ca2a371195235fc6e83d53bb15b252f4fb55db7b0434a1451f2b7e2 = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_b68f58f49ca2a371195235fc6e83d53bb15b252f4fb55db7b0434a1451f2b7e2 || 0 === strpos($__internal_d138df53d58975eb605a237d9add61fb1558a33ef9ff52ed21138061b14a9d8a, $__internal_b68f58f49ca2a371195235fc6e83d53bb15b252f4fb55db7b0434a1451f2b7e2)))) {
            // line 42
            echo "\t<title>Get Free Commercial Cleaning Quotes Now</title>
";
        } elseif ((is_string($__internal_98cc8e743aa2f60057d48cc6f3ce633c6c403512fb2c70530f2006851325a451 = $this->getAttribute($this->getAttribute(        // line 43
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_55ae828c559d3e8f74c7b3cce28543f7d3bcfa0941210bebfdad25309c6ca817 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_55ae828c559d3e8f74c7b3cce28543f7d3bcfa0941210bebfdad25309c6ca817 || 0 === strpos($__internal_98cc8e743aa2f60057d48cc6f3ce633c6c403512fb2c70530f2006851325a451, $__internal_55ae828c559d3e8f74c7b3cce28543f7d3bcfa0941210bebfdad25309c6ca817)))) {
            // line 44
            echo "\t<title>Procure Free Pest Control Quotes Easily</title>
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 45
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 46
            echo "\t<title>Vendor Reviews Directory Home Page</title>
";
        } elseif ((is_string($__internal_da30577dab6a9b84de3fa077e05beeff69d95724a62e7fbb0136c05e6f0fed89 = $this->getAttribute($this->getAttribute(        // line 47
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_531c5792dee3b1283e672bc98d396919ce87219ca2607ef9708dba46f9a9e378 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_531c5792dee3b1283e672bc98d396919ce87219ca2607ef9708dba46f9a9e378 || 0 === strpos($__internal_da30577dab6a9b84de3fa077e05beeff69d95724a62e7fbb0136c05e6f0fed89, $__internal_531c5792dee3b1283e672bc98d396919ce87219ca2607ef9708dba46f9a9e378)))) {
            // line 48
            echo "\t<title>Accounting & Auditing Reviews Directory Page</title>
";
        } elseif ((is_string($__internal_f4b86833c77ba40e8e93a496750d7d59ddfcf14f8056effca92fdc410bbeda7e = $this->getAttribute($this->getAttribute(        // line 49
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_94304752a011ee5a7c5457ad1642c4b79ba125d53795885cc58724efc512c3b2 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_94304752a011ee5a7c5457ad1642c4b79ba125d53795885cc58724efc512c3b2 || 0 === strpos($__internal_f4b86833c77ba40e8e93a496750d7d59ddfcf14f8056effca92fdc410bbeda7e, $__internal_94304752a011ee5a7c5457ad1642c4b79ba125d53795885cc58724efc512c3b2)))) {
            // line 50
            echo "\t<title>Find Signage & Signboard Reviews </title>
";
        } elseif ((is_string($__internal_6d89585263625bcd006e2116e377635284103d87fb1ed3b410f09f64c8cd7eff = $this->getAttribute($this->getAttribute(        // line 51
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_5876e2eb30ea1c0f1fd1da9b3db83344e796958d290d681b0d397c9a76ea1bf3 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_5876e2eb30ea1c0f1fd1da9b3db83344e796958d290d681b0d397c9a76ea1bf3 || 0 === strpos($__internal_6d89585263625bcd006e2116e377635284103d87fb1ed3b410f09f64c8cd7eff, $__internal_5876e2eb30ea1c0f1fd1da9b3db83344e796958d290d681b0d397c9a76ea1bf3)))) {
            // line 52
            echo "\t<title>Have a Look at IT Support Reviews Directory</title>
";
        } elseif ((is_string($__internal_7d31de074f83d607dd3c8346b3c55f74637d95250fbdabfb2d0259bf39b39d1e = $this->getAttribute($this->getAttribute(        // line 53
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_a6aece40378dbf499646fb8d0bdb78a9e7f3dff0238ea31985695f91a85e72b9 = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_a6aece40378dbf499646fb8d0bdb78a9e7f3dff0238ea31985695f91a85e72b9 || 0 === strpos($__internal_7d31de074f83d607dd3c8346b3c55f74637d95250fbdabfb2d0259bf39b39d1e, $__internal_a6aece40378dbf499646fb8d0bdb78a9e7f3dff0238ea31985695f91a85e72b9)))) {
            // line 54
            echo "\t<title>Check Out Photography & Videos Reviews Here</title>
";
        } elseif ((is_string($__internal_0d366e75f60412401cab97adba09f2ba2859943efbbb71496a0493fdc7afbffe = $this->getAttribute($this->getAttribute(        // line 55
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_7dae2c050f7d5385eed296eceab3594d7067dbb3840e7998703f6dd27ccd472a = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_7dae2c050f7d5385eed296eceab3594d7067dbb3840e7998703f6dd27ccd472a || 0 === strpos($__internal_0d366e75f60412401cab97adba09f2ba2859943efbbb71496a0493fdc7afbffe, $__internal_7dae2c050f7d5385eed296eceab3594d7067dbb3840e7998703f6dd27ccd472a)))) {
            // line 56
            echo "\t<title>View Residential Cleaning Reviews From Here</title>
";
        } elseif ((is_string($__internal_5cd6fd8d2b5b136ebd049af5200008e649aad1a036c2dd036e6af18e82a917ff = $this->getAttribute($this->getAttribute(        // line 57
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_666b9b2f195bc39f83ccf19112e6ffb71eebadb144936d3d645f7a85512b087a = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_666b9b2f195bc39f83ccf19112e6ffb71eebadb144936d3d645f7a85512b087a || 0 === strpos($__internal_5cd6fd8d2b5b136ebd049af5200008e649aad1a036c2dd036e6af18e82a917ff, $__internal_666b9b2f195bc39f83ccf19112e6ffb71eebadb144936d3d645f7a85512b087a)))) {
            // line 58
            echo "\t<title>Find Genuine Maintenance Reviews</title>
";
        } elseif ((is_string($__internal_26a9511053479ca17116e12f6fb43b5785eed8442997ce3cd0d60d8ebac80011 = $this->getAttribute($this->getAttribute(        // line 59
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_6e976b59b2cb8787daee58ca89daee165c498b74457315b6a94340e3460558bb = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_6e976b59b2cb8787daee58ca89daee165c498b74457315b6a94340e3460558bb || 0 === strpos($__internal_26a9511053479ca17116e12f6fb43b5785eed8442997ce3cd0d60d8ebac80011, $__internal_6e976b59b2cb8787daee58ca89daee165c498b74457315b6a94340e3460558bb)))) {
            // line 60
            echo "\t<title>Locate Interior Design & FitOut Reviews</title>
";
        } elseif ((is_string($__internal_a713daf57b479b228d553a9ff286a07b03b5da8b1cd8c603a5ac8daab6a0c16a = $this->getAttribute($this->getAttribute(        // line 61
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_479292af6a8d8422c38060113307228eee8982f5a64a5995f621589a9f252bf1 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_479292af6a8d8422c38060113307228eee8982f5a64a5995f621589a9f252bf1 || 0 === strpos($__internal_a713daf57b479b228d553a9ff286a07b03b5da8b1cd8c603a5ac8daab6a0c16a, $__internal_479292af6a8d8422c38060113307228eee8982f5a64a5995f621589a9f252bf1)))) {
            // line 62
            echo "\t<title>See All Catering Reviews Logged</title>
";
        } elseif ((is_string($__internal_441c6ec37b1a5b337900a492022f61c5c2b2084442eb7b6803763ca2808a7d99 = $this->getAttribute($this->getAttribute(        // line 63
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_e20f53a8feef82ca08c04cedbc6baa4b9d82a65e130489ade5ad584eb548c924 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_e20f53a8feef82ca08c04cedbc6baa4b9d82a65e130489ade5ad584eb548c924 || 0 === strpos($__internal_441c6ec37b1a5b337900a492022f61c5c2b2084442eb7b6803763ca2808a7d99, $__internal_e20f53a8feef82ca08c04cedbc6baa4b9d82a65e130489ade5ad584eb548c924)))) {
            // line 64
            echo "\t<title>Listings of all Landscaping & Gardens Reviews </title>
";
        } elseif ((is_string($__internal_b4ef0d9e4c91d770771ae0a8476014b464106c79f2b9f3e0fa44eea8f69de22b = $this->getAttribute($this->getAttribute(        // line 65
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_8af08dbb2f333dffe7a8d86c81c5cc7863526885a00bce036809c24c3c78dfac = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_8af08dbb2f333dffe7a8d86c81c5cc7863526885a00bce036809c24c3c78dfac || 0 === strpos($__internal_b4ef0d9e4c91d770771ae0a8476014b464106c79f2b9f3e0fa44eea8f69de22b, $__internal_8af08dbb2f333dffe7a8d86c81c5cc7863526885a00bce036809c24c3c78dfac)))) {
            // line 66
            echo "\t<title>Get Access to Offset Printing Reviews</title>
";
        } elseif ((is_string($__internal_07abac9babb53fd1a652304ccd80ec9774d20316a0c219b4885d94f816783413 = $this->getAttribute($this->getAttribute(        // line 67
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_4bc817fdba55f8497a49bb5fce2be5dac7abc99261f2cadd6f342a528da98ff9 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_4bc817fdba55f8497a49bb5fce2be5dac7abc99261f2cadd6f342a528da98ff9 || 0 === strpos($__internal_07abac9babb53fd1a652304ccd80ec9774d20316a0c219b4885d94f816783413, $__internal_4bc817fdba55f8497a49bb5fce2be5dac7abc99261f2cadd6f342a528da98ff9)))) {
            // line 68
            echo "\t<title>Have a Look at various Commercial Cleaning Reviews</title>
";
        } elseif ((is_string($__internal_d99188a83586932aca4e2bde2b7e713117e54d8b0b6fc8ab03896ad56a02c38e = $this->getAttribute($this->getAttribute(        // line 69
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_e037db58152097bc0b1cb87880d0de15c961216f7f7af87f636eb3b46c833437 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_e037db58152097bc0b1cb87880d0de15c961216f7f7af87f636eb3b46c833437 || 0 === strpos($__internal_d99188a83586932aca4e2bde2b7e713117e54d8b0b6fc8ab03896ad56a02c38e, $__internal_e037db58152097bc0b1cb87880d0de15c961216f7f7af87f636eb3b46c833437)))) {
            // line 70
            echo "\t<title>Read the Pest Control Reviews Listed Here</title>
";
        } elseif ((        // line 71
(isset($context["aboutus"]) ? $context["aboutus"] : $this->getContext($context, "aboutus")) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 72
            echo "\t<title>Find information About BusinessBid</title>
";
        } elseif ((        // line 73
(isset($context["meetteam"]) ? $context["meetteam"] : $this->getContext($context, "meetteam")) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 74
            echo "\t<title>Let us Introduce the BusinessBid Team</title>
";
        } elseif ((        // line 75
(isset($context["career"]) ? $context["career"] : $this->getContext($context, "career")) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 76
            echo "\t<title>Have a Look at BusinessBid Career Opportunities</title>
";
        } elseif ((        // line 77
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : $this->getContext($context, "valuedpartner")) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 78
            echo "\t<title>Want to become BusinessBid Valued Partners</title>
";
        } elseif ((        // line 79
(isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")) == "http://www.businessbid.ae/login#vendor")) {
            // line 80
            echo "\t<title>Vendor Account Login Access Page</title>
";
        } elseif ((is_string($__internal_2a167c2dddae5f59ea991d24ee69357e47edab22e76442dd0ddd6d41901a3b75 = $this->getAttribute($this->getAttribute(        // line 81
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2e988341166926017db4ca2beccdfa5f625d7250e1ba327f6787010e7750a7d8 = "http://www.businessbid.ae/node2") && ('' === $__internal_2e988341166926017db4ca2beccdfa5f625d7250e1ba327f6787010e7750a7d8 || 0 === strpos($__internal_2a167c2dddae5f59ea991d24ee69357e47edab22e76442dd0ddd6d41901a3b75, $__internal_2e988341166926017db4ca2beccdfa5f625d7250e1ba327f6787010e7750a7d8)))) {
            // line 82
            echo "\t<title>How BusinessBid Works for Vendors</title>
";
        } elseif ((is_string($__internal_aa82bb5e6918df9c71c73bb33eb812dd9c54dbccca578a6a24142ab218905b0c = $this->getAttribute($this->getAttribute(        // line 83
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_7380762874d24feb81890c3ce98a7bf6ee8e21042b17f84316ca7794176c428d = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_7380762874d24feb81890c3ce98a7bf6ee8e21042b17f84316ca7794176c428d || 0 === strpos($__internal_aa82bb5e6918df9c71c73bb33eb812dd9c54dbccca578a6a24142ab218905b0c, $__internal_7380762874d24feb81890c3ce98a7bf6ee8e21042b17f84316ca7794176c428d)))) {
            // line 84
            echo "\t<title>BusinessBid Vendor Registration Page</title>
";
        } elseif ((is_string($__internal_33adb5ab06dc6ff946ff3bfd406ce177fd5bfbeaa07ef1465523fb5e9f7d80f6 = $this->getAttribute($this->getAttribute(        // line 85
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_e213c9ada318781c6c9a3a64f4c1a191eae970b744cb4fe792eb750c8cb4af37 = "http://www.businessbid.ae/faq") && ('' === $__internal_e213c9ada318781c6c9a3a64f4c1a191eae970b744cb4fe792eb750c8cb4af37 || 0 === strpos($__internal_33adb5ab06dc6ff946ff3bfd406ce177fd5bfbeaa07ef1465523fb5e9f7d80f6, $__internal_e213c9ada318781c6c9a3a64f4c1a191eae970b744cb4fe792eb750c8cb4af37)))) {
            // line 86
            echo "\t<title>Find answers to common Vendor FAQ’s</title>
";
        } elseif ((is_string($__internal_2a4df97843c77ea2105cd3dfd9106ad26dc64fbb28ad2a9fdf27e35c90fdfa24 = $this->getAttribute($this->getAttribute(        // line 87
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_66f8f3c67d48134ffe08aa281b3233ebfa94dcf5611732b103ac144fa383c4d5 = "http://www.businessbid.ae/node4") && ('' === $__internal_66f8f3c67d48134ffe08aa281b3233ebfa94dcf5611732b103ac144fa383c4d5 || 0 === strpos($__internal_2a4df97843c77ea2105cd3dfd9106ad26dc64fbb28ad2a9fdf27e35c90fdfa24, $__internal_66f8f3c67d48134ffe08aa281b3233ebfa94dcf5611732b103ac144fa383c4d5)))) {
            // line 88
            echo "\t<title>BusinessBid Vendor Reviews Directory Home</title>
";
        } elseif ((is_string($__internal_e3e2729bc52828e104e802015114a772512632be2cbbe35f9ec34dd5b437e583 = ($this->getAttribute($this->getAttribute(        // line 89
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_e5e498a508f8db558df2657fe6e91a36d016eea06fc5494f0f63235d79f85bcc = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_e5e498a508f8db558df2657fe6e91a36d016eea06fc5494f0f63235d79f85bcc || 0 === strpos($__internal_e3e2729bc52828e104e802015114a772512632be2cbbe35f9ec34dd5b437e583, $__internal_e5e498a508f8db558df2657fe6e91a36d016eea06fc5494f0f63235d79f85bcc)))) {
            // line 90
            echo "\t<title>Customer Account Login and Dashboard Access</title>
";
        } elseif ((is_string($__internal_3bf4023fd0d8eaa65d900d483ee48077544e4c95cca09496a2e6936ab2b3982b = $this->getAttribute($this->getAttribute(        // line 91
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_c184bcd32b583f05edc9e7e8b0070f07917afb874b56fc379afe6ba15076ae71 = "http://www.businessbid.ae/node6") && ('' === $__internal_c184bcd32b583f05edc9e7e8b0070f07917afb874b56fc379afe6ba15076ae71 || 0 === strpos($__internal_3bf4023fd0d8eaa65d900d483ee48077544e4c95cca09496a2e6936ab2b3982b, $__internal_c184bcd32b583f05edc9e7e8b0070f07917afb874b56fc379afe6ba15076ae71)))) {
            // line 92
            echo "\t<title>Find helpful answers to common Customer FAQ’s</title>
";
        } elseif ((is_string($__internal_9dfd2daebcfbf17c16410ae7070dfcbd022d4202680a3d01fff9dc4a5b8d856d = $this->getAttribute($this->getAttribute(        // line 93
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_7126b218752a40c5c50a8907e84157dbf70c89c31365c2978431f3e538f7ecac = "http://www.businessbid.ae/feedback") && ('' === $__internal_7126b218752a40c5c50a8907e84157dbf70c89c31365c2978431f3e538f7ecac || 0 === strpos($__internal_9dfd2daebcfbf17c16410ae7070dfcbd022d4202680a3d01fff9dc4a5b8d856d, $__internal_7126b218752a40c5c50a8907e84157dbf70c89c31365c2978431f3e538f7ecac)))) {
            // line 94
            echo "\t<title>Give us Your Feedback</title>
";
        } elseif ((is_string($__internal_728c75815e36cbbddb48b47f3d2e05ab7e8b2f5bb42094d0b289c4eeba1cb46d = $this->getAttribute($this->getAttribute(        // line 95
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_15e3f8baebf4a0128d8f31d8b39a756b9f09444606a3ba67c55d5694ddf2a54a = "http://www.businessbid.ae/sitemap") && ('' === $__internal_15e3f8baebf4a0128d8f31d8b39a756b9f09444606a3ba67c55d5694ddf2a54a || 0 === strpos($__internal_728c75815e36cbbddb48b47f3d2e05ab7e8b2f5bb42094d0b289c4eeba1cb46d, $__internal_15e3f8baebf4a0128d8f31d8b39a756b9f09444606a3ba67c55d5694ddf2a54a)))) {
            // line 96
            echo "\t<title>Site Map Page</title>
";
        } elseif ((is_string($__internal_834a5ab431cdaa52d15e0aa5c71d59239b936093a834027c521665b6b9331c04 = $this->getAttribute($this->getAttribute(        // line 97
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_02b5bf7c72b08ae463e9153dde58069a5dd3c981809df07ab9b91ede8a434805 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_02b5bf7c72b08ae463e9153dde58069a5dd3c981809df07ab9b91ede8a434805 || 0 === strpos($__internal_834a5ab431cdaa52d15e0aa5c71d59239b936093a834027c521665b6b9331c04, $__internal_02b5bf7c72b08ae463e9153dde58069a5dd3c981809df07ab9b91ede8a434805)))) {
            // line 98
            echo "\t<title>Did you Forget Forgot Your Password</title>
";
        } elseif ((is_string($__internal_fa1d47ffda538c6552efe0c523cb112da331c6379cc480a1aa19eab2f0dd44fa = $this->getAttribute($this->getAttribute(        // line 99
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_c0378ddf2af9b90e39138f09f9e63788fafa2ae651753aa1e3c088f6b2ddc798 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_c0378ddf2af9b90e39138f09f9e63788fafa2ae651753aa1e3c088f6b2ddc798 || 0 === strpos($__internal_fa1d47ffda538c6552efe0c523cb112da331c6379cc480a1aa19eab2f0dd44fa, $__internal_c0378ddf2af9b90e39138f09f9e63788fafa2ae651753aa1e3c088f6b2ddc798)))) {
            // line 100
            echo "\t<title>Do You Wish to Reset Your Password</title>
";
        } elseif ((is_string($__internal_6d99fd6205fc1ab1f1e5d43d04eb5d663b556024107979ffc793ad796ca11eb0 = $this->getAttribute($this->getAttribute(        // line 101
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_abf9e5379908ad319230689093fd8c0b583edbdb6d86f7b548c84ee5968bfe11 = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_abf9e5379908ad319230689093fd8c0b583edbdb6d86f7b548c84ee5968bfe11 || 0 === strpos($__internal_6d99fd6205fc1ab1f1e5d43d04eb5d663b556024107979ffc793ad796ca11eb0, $__internal_abf9e5379908ad319230689093fd8c0b583edbdb6d86f7b548c84ee5968bfe11)))) {
            // line 102
            echo "\t<title>All you wanted to Know About Us</title>
";
        } elseif ((is_string($__internal_3bb0416437e2fb503d0286ead9571dd256ffed38279a3d3d3604cc01bba44bbf = $this->getAttribute($this->getAttribute(        // line 103
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ea30199b4a9c2a537e1535b89021cefa4b8bfe5eef9f85b855df3fe3b7081132 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_ea30199b4a9c2a537e1535b89021cefa4b8bfe5eef9f85b855df3fe3b7081132 || 0 === strpos($__internal_3bb0416437e2fb503d0286ead9571dd256ffed38279a3d3d3604cc01bba44bbf, $__internal_ea30199b4a9c2a537e1535b89021cefa4b8bfe5eef9f85b855df3fe3b7081132)))) {
            // line 104
            echo "\t<title>Information for All Prospective Vendors</title>
";
        } else {
            // line 106
            echo "\t<title>Business BID</title>
";
        }
        // line 108
        echo "


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta charset=\"utf-8\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
\t<!--[if lt IE 8]>
\t<script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
\t<script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
\t";
        // line 137
        echo "\t<link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">


\t<script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
\t<script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
";
        // line 144
        echo "\t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.scrollbox.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\$('#demo2').scrollbox({
\t  linear: true,
\t  step: 1,
\t  delay: 0,
\t  speed: 100
\t});
</script>
<script>
\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t
\t\$.ajax({url:realpath+\"/pullCategoryForFindMeVendor.php\",success:function(result){
\t\t\$(\"#categoryfind\").html(result);
\t}});
\t\$.ajax({url:realpath+\"/pullCityForFindMeVendor.php\",success:function(result){
\t\t\$(\"#city\").html(result);
\t}});
\t
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 169
        $this->displayBlock('customcss', $context, $blocks);
        // line 172
        echo "
";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 606
        echo "
";
        // line 607
        $this->displayBlock('customjs', $context, $blocks);
        // line 610
        echo "
";
        // line 611
        $this->displayBlock('jquery', $context, $blocks);
        // line 614
        echo "</head>
<body>

";
        // line 617
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "uid"), "method");
        // line 618
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "pid"), "method");
        // line 619
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "email"), "method");
        // line 620
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "name"), "method");
        // line 621
        echo "
<div class=\"main_container\">
\t<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t<div class=\"row\">
\t<div class=\"col-sm-3 contact-info\">
\t<div class=\"col-sm-6 clear-right\">
\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>

\t<span>(04) 42 13 777</span>
\t</div>
\t<div class=\"col-sm-6 left-clear\">

\t<span class=\"contact-no\"><a href=\"";
        // line 634
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></span>
\t</div>

\t</div>
\t<div class=\"col-sm-5 flash-message\"><!--<marquee behavior=\"scroll\" height=\"20\" direction=\"left\" scrollamount=\"2\">LAUNCHING SOON</marquee> --></div>
\t<div class=\"col-sm-4 clear-right \">

\t<div class=\"right-top-links\">
\t";
        // line 642
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 643
            echo "\t\t<ul class=\"nav navbar-nav\">
\t\t\t<li><a class=\"fb\" href=\"";
            // line 644
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\" ><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/my-login.png"), "html", null, true);
            echo "\"></a></li>
\t\t\t<li><img src=\"";
            // line 645
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fbsign.png"), "html", null, true);
            echo "\" onclick=\"FBLogin();\"></li>

\t\t\t<li id=\"flip\"><span class=\"glyphicon glyphicon-search search-block-top\"></span></li>
\t\t\t<div id=\"panel\">
\t\t\t<form action=\"";
            // line 649
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_globalsearch");
            echo "\" method=\"GET\" target=\"_blank\">
\t\t\t\t<input type=\"search\" name=\"keyword\" id=\"keyword\" value=\"\" placeholder=\"Search here\" style=\"width: 173px\"/>
\t\t\t\t<input class=\"search-btn form_submit\" type=\"submit\" value=\"Search\" />
\t\t\t</form>
\t\t\t\t";
            // line 655
            echo "\t\t\t</div>
\t\t</ul>
\t\t";
        }
        // line 658
        echo "\t</div>

\t";
        // line 660
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 661
            echo "\t<div class=\"col-sm-12 clear-right dash-right-top-links\">
\t<ul>
\t<li>Welcome, <span class=\"profile-name\">";
            // line 663
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
            echo "</span></li>
\t<li><a href=\"";
            // line 664
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t</ul>
\t</div>

\t";
        }
        // line 669
        echo "
\t</div>
\t</div>
\t</div>
\t</div>
\t<!-- top header Starts -->
<div class=\"header\">
\t<div class=\"container\">
    \t<div class=\"row\">
            <div class=\"col-md-5 logo\"><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\" ></a></div>
            <div class=\"col-md-7 right_nav\">
\t";
        // line 680
        $this->displayBlock('toprightmenu', $context, $blocks);
        // line 707
        echo "            </div>
        </div>
    </div>
</div>
    <!-- top header Ends -->
    <!-- Search block Starts -->
    <div class=\"search_block\">
\t";
        // line 714
        $this->displayBlock('search', $context, $blocks);
        // line 772
        echo "
    </div>
   \t<!-- Search block Ends -->
    <!-- Banner block Starts -->
    ";
        // line 776
        $this->displayBlock('banner', $context, $blocks);
        // line 834
        echo "    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 838
        $this->displayBlock('maincontent', $context, $blocks);
        // line 1097
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
\t";
        // line 1100
        $this->displayBlock('footer', $context, $blocks);
        // line 1279
        echo "    <!-- footer block ends -->
</div>

<div class=\"modal fade\" id=\"privacy-policy\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Privacy Policy</h4>
      </div>
\t<iframe src=\"/web/htm-files/privacy-policy.html\" width=\"100%\" height=\"500\" frameborder=\"0\">
\t</iframe>

    </div>
  </div>
</div>

<!--<div id=\"sitemap\">

    <li class=\"leaf\"><a href=\"";
        // line 1298
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\" title=\"\">Site map</a></li>

</div>-->

<div class=\"modal fade\" id=\"terms-conditions\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Terms &amp; Conditions</h4>
      </div>
\t<iframe src=\"/web/htm-files/terms-conditions.html\" width=\"100%\" height=\"500\" frameborder=\"0\">
\t</iframe>
    </div>
  </div>
</div>

</body>
</html>

<script type=\"text/javascript\">
window.onload = jsfunction();
//window.onload = getcharactercount();
//window.onload = megamenuAjax();
</script>
";
    }

    // line 169
    public function block_customcss($context, array $blocks = array())
    {
        // line 170
        echo "
";
    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        // line 174
        echo "<script>
\$(document).ready(function(){
\t";
        // line 176
        if (array_key_exists("keyword", $context)) {
            // line 177
            echo "\t   \twindow.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 181
        echo "\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\t\$('#category').autocomplete({
      \tsource: function( request, response ) {
      \t\$.ajax({
\t      \turl : realpath+\"ajax-info.php\",
\t      \tdataType: \"json\",
\t\t\tdata: {
\t\t\t   name_startsWith: request.term,
\t\t\t   type: 'category'
\t\t\t},
\t\t\tsuccess: function( data ) {
\t\t\t\tresponse( \$.map( data, function( item ) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item,
\t\t\t\t\t\tvalue: item
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t},
\t\t\tselect: function(event, ui) {
               \talert( \$(event.target).val() );
            }
      \t});
  \t\t},autoFocus: true,minLength: 2
  \t});
});
\t\$(document).ready(function(){
\t\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\t\t\$(\"#form_subcategory\").html('<option value=\"0\">Select</option>');
\t\t\$(\"#form_category\").on('change',function (){
\t\t\tvar ax = \t\$(\"#form_category\").val();
\t\t\t\$.ajax({url:realpath+\"pullSubCategoryByCategory.php?categoryid=\"+ax,success:function(result){
\t\t\t\t\$(\"#checkbox_subcategory\").html(result);
\t\t\t\t\$(\"#div_subcategory\").show();
\t\t\t}});
\t\t});
\t});

\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_email\").on('input',function (){
\tvar ax = \t\$(\"#form_email\").val();
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}

\telse{

\tvar filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
\tvar \$th = \$(this);
\tif (filter.test(ax)) {
\t\t\t\t\$th.css('border', '3px solid green');
\t\t\t}
\t\t\telse {
\t\t\t\t\$th.css('border', '3px solid red');
\t\t\t\te.preventDefault();
\t\t\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}

\t}
\t});




\t\$(\"#form_vemail\").on('input',function (){
\tvar ax = \t\$(\"#form_vemail\").val();
\tvar filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
\tvar \$th = \$(this);
\tif (filter.test(ax))
\t{
\t\t\$th.css('border', '3px solid green');
\t}
\telse {
\t\t\$th.css('border', '3px solid red');
\t\te.preventDefault();
\t\t}
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}
\t});




});

\t\$(document).ready(function(){
\t\$('#form_description').attr(\"maxlength\",\"240\");
\t\$('#form_location').attr(\"maxlength\",\"30\");

\t});

</script>
\t<script type='text/javascript' >

\tfunction jsfunction(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);

\tdocument.getElementById(\"searchval\").innerHTML=xmlhttp.responseText;
\t megamenublogAjax();
\t megamenubycatAjax();
\tmegamenubycatVendorSearchAjax();
\tmegamenubycatVendorReviewAjax();
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"ajax-info.php\",true);
\txmlhttp.send();



\t}

\tfunction megamenublogAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t\t\t\t\t\t\t\t\t//document.getElementById(\"megamenu1\").innerHTML=xmlhttp.responseText;
\t\tvar mvsa = document.getElementById(\"megamenu1Anch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Resourcesegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menublog.php\",true);
\t//xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);

\txmlhttp.send();



\t}
\tfunction megamenubycatAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\tdocument.getElementById(\"megamenubycatUl\").innerHTML=xmlhttp.responseText;
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menubycat.php\",true);
\txmlhttp.send();



\t}

\tfunction megamenubycatVendorSearchAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t//console.log(xmlhttp.responseText);
\t\tvar mvsa = document.getElementById(\"megamenuVenSearchAnch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Searchsegment-drop \") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t//document.getElementById(\"megamenuVenSearchAnch\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorSearch\",true);
\txmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorReviewAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();

\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t\t//alert(xmlhttp.responseText);
\t\t\tvar mvsa = document.getElementById(\"megamenuVenReviewAnch\");
\t\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Reviewsegment-drop\") {
\t\t\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t\t\t  break;
\t\t\t\t\t}
\t\t\t}

\t\t//document.getElementById(\"megamenuVenReviewUl\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t  xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorReview\",true);
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorReview\",true);
\txmlhttp.send();



\t}
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
\tFB.init({
\tappId      : '754756437905943', // replace your app id here
\tstatus     : true,
\tcookie     : true,
\txfbml      : true
\t});
};
(function(d){
\tvar js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
\tif (d.getElementById(id)) {return;}
\tjs = d.createElement('script'); js.id = id; js.async = true;
\tjs.src = \"//connect.facebook.net/en_US/all.js\";
\tref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
\tFB.login(function(response){
\t\tif(response.authResponse){
\t\t\twindow.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
\t\t}
\t}, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
function show_submenu(b, menuid)
{
\tdocument.getElementById(b).style.display=\"block\";
\tif(menuid!=\"megaanchorbycat\"){
\t\tdocument.getElementById(menuid).className = \"setbg\";
\t}
}

function hide_submenu(b, menuid)
{
\tsetTimeout(dispminus(b,menuid), 200);
}

function dispminus(subid,menuid) {
\tif(menuid!=\"megaanchorbycat\") {
\t\tdocument.getElementById(menuid).className = \"unsetbg\";
\t}
\tdocument.getElementById(subid).style.display=\"none\";
}


\$(document).ready(function(){
\t\$('#flip').click(function(){
\t\t\$('#panel').slideToggle('slow');
\t\t\$('#keyword').val('');
\t})

\t\$(document).click(function(event) {
\t\tif (!\$(event.target).closest(\"#flip, #panel\").length) {
\t\t\t\$(\"#panel\").slideUp('slow');
\t\t}
\t})
});



 \$(document).ready(function(){
      \$(\".Searchsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Searchsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });
 \$(document).ready(function(){
      \$(\".Reviewsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Reviewsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });

\$(document).ready(function(){
\t\$('#ab1').click(function(){
\t\tdocument.title='Find information About BusinessBid';
\t});
\t\$('#mt1').click(function(){
\t\tdocument.title='Let us Introduce the BusinessBid Team';
\t});
\t\$('#cr1').click(function(){
\t\tdocument.title='Have a Look at BusinessBid Career Opportunities';
\t});
\t\$('#vp1').click(function(){
\t\tdocument.title='Want to become BusinessBid Valued Partners';
\t});
\t\$('#ct1').click(function(){
\t\tdocument.title='All you wanted to Know About Us';
\t});

\t\$('#customertab').click(function(){
\t\tdocument.title='Customer Account Login and Dashboard Access';
\t});

\t\$('#vendortab').click(function(){
\t\tdocument.title='Vendor Account Login Access Page';
\t});

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
\t\ttype : \"POST\",
\t\tdata : {\"email\":email},
\t\turl:\"";
        // line 587
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
\t\tsuccess:function(result){
\t\t\t\$(\"#succ\").css(\"display\",\"block\");
\t\t},
\t\terror: function (error) {
\t\t\t\$(\"#err\").css(\"display\",\"block\");
      \t}
\t});

  });

  \$('#rc').click(function(){
\t\twindow.location.href = \"http://www.businessbid.ae/resource_centre/\";
  });
});
</script>


";
    }

    // line 607
    public function block_customjs($context, array $blocks = array())
    {
        // line 608
        echo "
";
    }

    // line 611
    public function block_jquery($context, array $blocks = array())
    {
        // line 612
        echo "
";
    }

    // line 680
    public function block_toprightmenu($context, array $blocks = array())
    {
        // line 681
        echo "            \t<nav class=\"top_menu\">
                \t<ul class=\"nav navbar-nav\">
                    \t<li><a href=\"";
        // line 683
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
                     <li><a href=\"";
        // line 684
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
\t<!--<li> <a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_job");
        echo "\" title=\"\">Post a Job</a></li> -->
\t";
        // line 686
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 687
            echo "                     \t";
        } else {
            // line 688
            echo "\t";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                // line 689
                echo "\t<li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_home");
                echo "\"> Dashboard </a></li>


\t";
            } else {
                // line 693
                echo "\t<li><a class=\"my_account\" href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home");
                echo "\"> My Account </a></li>
\t";
            }
            // line 695
            echo "\t";
        }
        // line 696
        echo "\t";
        if ((null === (isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 697
            echo "                     <li>  <a class=\"fb\" href=\"";
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/ruvendor.jpg"), "html", null, true);
            echo "\"></a> </li>
                 ";
        }
        // line 699
        echo "                  </ul>
                </nav>
\t<nav class=\"top_menu_bottom\">
\t<ul>

\t</ul>
\t</nav>
\t";
    }

    // line 714
    public function block_search($context, array $blocks = array())
    {
        // line 715
        echo "    \t<div class=\"container\">
        \t<div class=\"row\">
\t<div class=\"col-md-7 left_nav\">
\t";
        // line 718
        $this->displayBlock('topleftmenu', $context, $blocks);
        // line 739
        echo "            </div>
\t<div class=\"col-md-5 search_block\">
\t <form name=\"search\" method=\"request\" action=\"";
        // line 741
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
            \t \t<input class=\"col-sm-8 search-box\" type=\"text\" list=\"browsers\" placeholder=\"Type the service you require\" id='category' name='category' ><datalist id=\"searchval\"></datalist>
<input type=\"submit\" value=\"Search\" class=\"search-btn form_submit col-sm-3\" />
                 </form>
\t </div>
    <!-- mega menu div block starts -->
\t";
        // line 747
        $this->displayBlock('megamenu', $context, $blocks);
        // line 767
        echo "    <!-- mega menu div block ends -->
            </div>
\t    </div>

\t    ";
    }

    // line 718
    public function block_topleftmenu($context, array $blocks = array())
    {
        // line 719
        echo "            \t<nav class=\"user_menu_top\">
                \t<ul class=\"nav navbar-nav\">
                    \t<li><a href=\"";
        // line 721
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
\t\t\t\t\t\t<li id = \"megamenuVenSearchAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Search<b class=\"caret\"></b></a>
\t\t\t\t\t\t<ul class=\"dropdown-menu Searchsegment-drop \"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li id = \"megamenuVenReviewAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Review<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Reviewsegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li id = \"megamenu1Anch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a id=\"rc\" href=\"http://www.businessbid.ae/resource_centre/\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" >Resource Centre<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Resourcesegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
                        <!--<li id = \"megamenu1Anch\"  onClick=\"javascript:show_submenu('megamenu1', this.id);\" onMouseOut=\"javascript:hide_submenu12('megamenu1', this.id);\"><a href=\"#\" id=\"megaanchor\" >Resource Centre</a></li>-->
                    </ul>
                </nav>

\t";
    }

    // line 747
    public function block_megamenu($context, array $blocks = array())
    {
        // line 748
        echo "\t<div id=\"megamenuVenSearch\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" ><div><ul id=\"megamenuVenSearchUl\"></ul></div></div>
\t<div id=\"megamenuVenReview\" class=\"submenu menu-mega megamenubg-1 megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');\"><div class=\"review-menu-left\"><ul id=\"megamenuVenReviewUl\"></ul></div><div  class=\"review-menu-right\"><button class=\"btn btn-success write-button\" type=\"button\" >Write a Review</button></div></div>
\t<ul id=\"megamenu1\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenu1','megamenu1Anch');\" onMouseOut=\"javascript:hide_submenu12('megamenu1','megamenu1Anch');\" ></ul>
\t<div id=\"megamenubycat\" class=\"submenu menu-megabycat megamenubg\" onMouseOver=\"javascript:show_submenu('megamenubycat','megaanchorbycat');\" onMouseOut=\"javascript:hide_submenu('megamenubycat','megaanchorbycat');\"><div><ul id=\"megamenubycatUl\"></ul></div><div></div></div>

\t";
        // line 753
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 754
            echo "
\t<script type=\"text/javascript\">

\tdocument.getElementById(\"megamenuVenSearch\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenuVenReview\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenu1\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenubycat\").setAttribute(\"class\", \"submenu menu-megabycat-login megamenubg\");
\t</script>


\t";
        }
        // line 765
        echo "
\t";
    }

    // line 776
    public function block_banner($context, array $blocks = array())
    {
        // line 777
        echo "
    <div class=\"jumbotron\">
    <div id=\"carousel-example-generic\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\"  data-interval=\"3000\">
              <!-- Indicators -->
              <ol class=\"carousel-indicators\">
                <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>
\t<li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>
\t<li data-target=\"#carousel-example-generic\" data-slide-to=\"3\"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class=\"carousel-inner\">
\t<div class=\"item active\">
                  \t<img src=\"";
        // line 791
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner1.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
\t<div class=\"item\">
                  \t<img src=\"";
        // line 794
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner2.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
\t  \t<div class=\"item\">
                  \t<img src=\"";
        // line 797
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner3.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
\t<div class=\"item\">
                  \t<img src=\"";
        // line 800
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner4.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
              </div>


            </div>

\t<div class=\"category-right-block\">
\t";
        // line 808
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
\t<div class=\"row\">
\t<h2>Get Quick Quotes from Local Vendors for Free!</h2>
\t<div class=\"col-md-6 side-clear right-med\">

\t<div class=\"form-group\">
\t";
        // line 814
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "category", array()), 'widget');
        echo "
\t</div>
\t</div>
\t<div class=\"col-md-6 side-clear left-med\">
\t<div class=\"form-group\">
\t";
        // line 819
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city", array()), 'widget');
        echo "
\t</div>
\t</div>
\t<div class=\"row\">
\t<div class=\"form-group col-sm-12\">
\t";
        // line 824
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
\t</div>
\t</div>

\t</div>
\t";
        // line 829
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
\t</div>

    </div>
    ";
    }

    // line 838
    public function block_maincontent($context, array $blocks = array())
    {
        // line 839
        echo "    <div class=\"main_content\">
\t<div class=\"container content-top-one\">
\t<h1>How does it work? </h1>
\t<h3>Find the perfect vendor for your project in just three simple steps</h3>
\t<div class=\"content-top-one-block\">
\t<div class=\"col-sm-4 float-shadow\">
\t<div class=\"how-work\">
\t<img src=\"";
        // line 846
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
\t<h4>TELL US ONCE</h4>
\t<h5>Simply fill out a short form or give us a call</h5>
\t</div>
\t</div>
\t<div class=\"col-sm-4 float-shadow\">
\t<div class=\"how-work\">
\t<img src=\"";
        // line 853
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
\t<h4>VENDORS CONTACT YOU</h4>
                                   <h5>Receive three quotes from screened vendors in minutes</h5>
\t</div>
\t</div>
\t<div class=\"col-sm-4 float-shadow\">
\t<div class=\"how-work\">
\t<img src=\"";
        // line 860
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
\t<h4>CHOOSE THE BEST VENDOR</h4>
\t<h5>Use quotes and reviews to select the perfect vendors</h5>
\t</div>
\t</div>
\t</div>
\t</div>

<div class=\"content-top-two\">
\t<div class=\"container\">
\t<h1>Why choose Business Bid?</h1>
\t<h3>The most effective way to find vendors for your work</h3>
\t<div class=\"content-top-two-block\">
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 875
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
\t<h4>Convenience</h4>
\t</div>
\t<h5>Multiple quotations with a single, simple form</h5>
\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 882
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
\t<h4>Competitive Pricing</h4>
\t</div>
\t<h5>Compare quotes before picking the most suitable</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 890
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
\t<h4>Speed</h4>
\t</div>
\t<h5>Quick responses to all your job requests</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 898
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
\t<h4>Reputation</h4>
\t</div>
\t<h5>Check reviews and ratings for the inside scoop</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 906
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
\t<h4>Free To Use</h4>
\t</div>
\t<h5>No usage fees. Ever!</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 914
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
\t<h4>Amazing Support</h4>
\t</div>
\t<h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

\t</div>
\t</div>
\t</div>

</div>
    \t<div class=\"container content-top-three\">
        \t<div class=\"row\">
\t";
        // line 926
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 966
        echo "            </div>

\t     </div>
        </div>

    </div>

\t\t<!--<div class=\"col-sm-12\">
\t\t\t<div id=\"demo2\" class=\"scroll-text\">
\t\t\t  <ul>
\t\t\t\t<li><a href=\"#\">DEA Mines \"National Security\" Data To Spy On Americans, Now Concealing Program</a></li>
\t\t\t\t<li><a href=\"#\">Sergey Brin invests in synthetic beef</a></li>
\t\t\t\t<li><a href=\"#\">OS X emulation layer for Linux</a></li>
\t\t\t\t<li><a href=\"#\">Fast android emulator using Virtualbox</a></li>
\t\t\t\t<li><a href=\"#\">Latvia blocking extradition of Gozi writer due to disproportionate US sentencing</a></li>
\t\t\t\t<li><a href=\"#\">Please let me know if I should Stop developing apps for Google</a></li>
\t\t\t  </ul>
\t\t\t</div>
\t\t</div>-->


\t <div class=\"certified_block\">
    \t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t<h1>We do work for you!</h1>
\t\t\t<div class=\"content-block\">
\t\t\t<p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
\t\t\t<p>Your satisfication matters to us!</p>
\t\t\t<p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
\t\t\t<p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
\t\tservice experience.</p>
\t\t\t</div>
\t\t\t</div>
        </div>
\t</div>
\t <div class=\"popular_category_block\">
    \t<div class=\"container\">
\t<div class=\"row\">
\t<h1>Popular Categories</h1>
\t<h3>Check out some of the hottest services in the UAE</h3>
\t<div class=\"content-block\">
\t<div class=\"col-sm-12\">
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1010
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 1011
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
\t";
        } else {
            // line 1013
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
\t";
        }
        // line 1015
        echo "
\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>

\t";
        // line 1021
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 1022
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
\t";
        } else {
            // line 1024
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
\t";
        }
        // line 1026
        echo "
\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1031
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 1032
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
\t";
        } else {
            // line 1034
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
\t";
        }
        // line 1036
        echo "


\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1043
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 1044
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
\t";
        } else {
            // line 1046
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
\t";
        }
        // line 1048
        echo "
\t</ul>
\t</div>

\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1054
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 1055
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
\t";
        } else {
            // line 1057
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
\t";
        }
        // line 1059
        echo "

\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1065
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 1066
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
\t";
        } else {
            // line 1068
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
\t";
        }
        // line 1070
        echo "

\t</ul>
\t</div>
\t</div>
\t</div>
\t<div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource_centre/home-2\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
\t</div>
\t</div>
       </div>
<!--
<div class=\"people_business_block\">
    <div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"content-block\">
\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t<img src=\"";
        // line 1086
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/businessbid-logo-botom.png"), "html", null, true);
        echo "\" /> <h1>is the most efficient way for Customers
\t\t\t\t\t\t\tto find Vendors in the UAE.<br/> Why not give it a try?</h1>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-12 see-all-cat-block \"><a class=\"btn btn-success see-all-cat blue wobble-horizontal\" href=\"";
        // line 1089
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_quote_inline");
        echo "\">Request Free Quotes</a><a class=\"btn btn-success see-all-cat peach wobble-horizontal\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works");
        echo "\">Register Your Business</a></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
-->
\t";
    }

    // line 926
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 927
        echo "\t<h1>We operate all across the UAE</h1>
            <div class=\"col-md-6 grow\">
\t<img src=\"";
        // line 929
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

\t<div class=\"col-md-6\">
              \t<h2>Recent Jobs</h2>
\t\t\t\t<div id=\"demo2\" class=\"scroll-text\">
\t\t\t\t\t<ul class=\"recent_block\">
\t\t\t\t\t\t";
        // line 936
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : $this->getContext($context, "activities")));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 937
            echo "\t\t\t\t\t\t<li class=\"activities-block\">
\t\t\t\t\t\t<div class=\"act-date\">";
            // line 938
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
\t\t\t\t\t\t";
            // line 939
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 940
                echo "\t\t\t\t\t\t\t";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 941
                echo "\t\t\t\t\t\t\t";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : $this->getContext($context, "fullname")), " ");
                // line 942
                echo "\t\t\t\t\t\t<span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : $this->getContext($context, "firstName")), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
\t\t\t\t\t\t";
            } else {
                // line 944
                echo "\t\t\t\t\t\t\t";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 945
                echo "\t\t\t\t\t\t\t";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : $this->getContext($context, "fullname")), " ");
                // line 946
                echo "\t\t\t\t\t\t<span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : $this->getContext($context, "firstName")), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
\t\t\t\t\t\t";
            }
            // line 948
            echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 950
        echo "\t\t\t\t\t</ul>
\t\t\t\t</div>


\t<div class=\"col-md-12 side-clear\">
\t";
        // line 955
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 961
        echo "\t</div>
            </div>


\t";
    }

    // line 955
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 956
        echo "\t<div class=\"request\">
\t<h2>";
        // line 957
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : $this->getContext($context, "enquiries")), "html", null, true);
        echo "</h2>
\t<span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
\t</div>
\t";
    }

    // line 1100
    public function block_footer($context, array $blocks = array())
    {
        // line 1101
        echo "<footer class=\"footer_wrapper\">
\t<div class=\"container\">
\t<div class=\"row\">
\t<div class=\"customer-benefits-block\">
\t <div class=\"region region-footer-links-one footer-menu\">
\t<section id=\"block-menu-menu-footer-menu-one\" class=\"block block-menu clearfix customer-benefits\">
\t<h2 class=\"block-title\">Customer Benefits</h2>
\t<ul class=\"menu nav\">
\t<li class=\"first leaf convenience\"><a>Convenience</a>
\t<span>Multiple quotations with a single, simple form</span>
\t</li>
\t<li class=\"leaf active competitive\"><a>Competitive Pricing</a>
\t<span>Compare quotes before choosing the most suitable.</span>
\t</li>
\t<li class=\"leaf active speed\"><a>Speed</a>
\t<span>Quick responses to all your job requests.</span>
\t</li>
\t<li class=\"leaf active reputation\"><a>Reputation</a>
\t<span>Check reviews and ratings for the inside scoop.</span>
\t</li>
\t<li class=\"leaf active free-use\"><a>Free to Use</a>
\t<span>No usage fees. Ever!</span>
\t</li>
\t<li class=\"last leaf active amazing\"><a>Amazing Support</a>
\t<span>Phone. Live Chat. Facebook. Twitter.</span>
\t</li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\"footer-right-menu-block\">
\t<div class=\"footer-right-menu-block-top\">
\t<div class=\"customer-benefits-block-first\">
\t <div class=\"region region-footer-links-two footer-menu\">
\t<section id=\"block-menu-menu-footer-menu-two\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">About</h2>
\t<ul class=\"menu nav\">
\t<li class=\"first leaf\"><a href=\"";
        // line 1138
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\" title=\"\" id=\"ab1\">About Us</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1139
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\" title=\"\" class=\"active\" id=\"mt1\">Meet the Team</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1140
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\" title=\"\" class=\"active\" id=\"cr1\">Career Opportunities</a></li>
\t<li class=\"last leaf active\"><a href=\"";
        // line 1141
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\" title=\"\" class=\"active\" id=\"vp1\">Valued Partners</a></li>

\t<li class=\"leaf active\"><a href=\"";
        // line 1143
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\" class=\"active\" id=\"ct1\">Contact Us</a></li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-three footer-menu\">
\t<section id=\"block-menu-menu-for-services-professionals\" class=\"block block-menu clearfix \">
\t<h2 class=\"block-title\">Vendor Resources</h2>
\t<ul class=\"menu nav\">
\t<li class=\"leaf\"><a href=\"";
        // line 1153
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\" title=\"\">Login</a></li>
\t<li class=\"first leaf active\"><a href=\"";
        // line 1154
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_grow_your_business");
        echo "\" title=\"\" class=\"active\">Grow Your Business</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1155
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works");
        echo "\" title=\"\">How it Works</a></li>
\t<li class=\"first leaf\"><a href=\"";
        // line 1156
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" title=\"\">Become a Vendor</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1157
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\" title=\"\" class=\"active\">FAQ's &amp; Support</a></li>
\t</ul>
\t</section>
\t<!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-four footer-menu social-links\">
\t<section id=\"block-menu-menu-for-services-professionals\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">Stay Connected</h2>
\t<ul class=\"menu nav\">
\t<li class=\"leaf\"><a href=\"https://www.facebook.com/businessbid?ref=hl\" class=\"facebook\" target=\"_blank\">Like us on Facebook</a></li>
\t<li class=\"first leaf active\"><a href=\"https://twitter.com/BusinessBid\" title=\"\" class=\"twitter\" target=\"_blank\">Follow us on Twitter</a></li>
\t<li class=\"leaf\"><a href=\"#\" title=\"\" class=\"gplus\" target=\"_blank\">Follow Us on Google+</a></li>
\t</ul>
\t</section>
\t<!-- /.block -->
\t</div>
\t</div>

\t<div class=\"col-xs-12 customer-benefits-block-last footer_nav_liks\">
\t<div class=\"region region-footer-links-five\">
\t<section id=\"block-block-8\" class=\"block block-block clearfix\">
\t<h2 class=\"block-title\">Contact Us</h2>
\t<address class=\"addr-map\">Suite 503, Level 5, Indigo Icon Tower, Cluster F, Jumeirah Lakes Tower - Dubai, UAE</address>
\t<span class=\"work-day-time\">Sunday-Thursday</span>
\t<span class=\"work-day-time\">9 am - 6 pm</span>

\t<span class=\"mobile\">(04) 42 13 777</span>
\t<span class=\"addr\">BusinessBid DMCC, <br>PO Box- 393507, Dubai, UAE</span>
\t";
        // line 1188
        echo "\t</section> <!-- /.block -->
\t</div>

\t</div>
\t</div>
\t<div class=\"footer-right-menu-block-top\">
\t<div class=\"customer-benefits-block-first\">
\t <div class=\"region region-footer-links-two footer-menu\">
\t<section id=\"block-menu-menu-footer-menu-two\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">Client Services</h2>
\t<ul class=\"menu nav\">
\t<!-- <li class=\"first leaf\"><a href=\"";
        // line 1199
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_job");
        echo "\" title=\"\">Post a Job</a></li> -->
\t";
        // line 1201
        echo "\t<li class=\"leaf\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\" title=\"\">Search Reviews</a></li>
\t<li class=\"last leaf\"><a href=\"";
        // line 1202
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\" title=\"\">Write a Review</a></li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-three footer-menu\">
\t<section id=\"block-menu-menu-client-resources\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">Client Resources</h2>
\t<ul class=\"menu nav\">
\t<li class=\"leaf\"><a href=\"";
        // line 1212
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\" title=\"\">Login</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1213
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
\t<!--<li class=\"first leaf\"><a href=\"";
        // line 1214
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\" title=\"\">Become a Consumer</a></li> -->
\t<li class=\"last leaf active\"><a href=\"http:../../../resource_centre\" title=\"\" class=\"active\">Resource Center</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1216
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\" title=\"\" class=\"active\">FAQ's &amp; Support</a></li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-four footer-menu\">
\t<section id=\"block-menu-menu-for-services-professionals\" class=\"block block-menu clearfix payment-accept\">
\t<h2 class=\"block-title\">Accepted Payment</h2>
\t<ul class=\"menu nav col-sm-8\">
\t<li class=\"leaf\"><a><img src=\"";
        // line 1226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/visa-icon.jpg"), "html", null, true);
        echo " \" /></a></li>
\t<li class=\"leaf\"><a><img src=\"";
        // line 1227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/masercard-icon.jpg"), "html", null, true);
        echo " \" /></a></li>
\t<!-- <li class=\"leaf\"><a><img src=\"";
        // line 1228
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/paypal-icon.jpg"), "html", null, true);
        echo " \" /></a></li> -->
\t<li class=\"leaf\"><a><img src=\"";
        // line 1229
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/securecode-icon.jpg"), "html", null, true);
        echo "\" /></a></li>
\t</ul>
\t</section>
\t<!-- /.block -->
\t</div>
\t</div>

\t<div class=\"col-xs-12 customer-benefits-block-last footer_nav_liks\">
\t<div class=\"region region-footer-links-five\">
\t<section id=\"block-block-8\" class=\"block block-block clearfix subscribe_news\">
\t<div id=\"succ\" style=\"display:none;color:#FFFFFF\">You have successfully Subscribed Newsletter</div>
\t<div id=\"err\" style=\"display:none;color:#FFFFFF\">You have already Subscribed</div>
\t<h2 class=\"block-title\">Subscribe to Newsletter</h2>
\t<div class=\"subscribe-text\">Sign up with your e-mail to get tips, resources
and amazing deals</div> <div class=\"mail-box\">
<form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource_centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
<input type=\"hidden\" name=\"nr\" value=\"widget\">
<input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email here\">
<input class=\"submit\" type=\"submit\" value=\"Subscribe\" /></div>
</form>\t</section> <!-- /.block -->
\t</div>

\t</div>
\t</div>

\t<div class=\"footer-right-menu-block-top\">
\t<div class=\"region region-footer-quick-links\">
\t<section id=\"block-menu-menu-footer-quick-links\" class=\"block block-menu clearfix\">
\t  <ul class=\"menu nav\">
\t  \t<li class=\"first leaf\"><a href=\"";
        // line 1258
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1259
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\" title=\"\">Careers</a></li>
\t<li class=\"leaf\"><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1261
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
\t<li class=\"last leaf\"><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms &amp; Conditions</a></li>
\t  </ul>
\t</section> <!-- /.block -->
  \t</div>
               \t</div>


\t</div>
\t</div>
\t</div>
\t</div>
\t";
        // line 1274
        echo "\t<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</footer>
\t";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2012 => 1274,  1997 => 1261,  1992 => 1259,  1988 => 1258,  1956 => 1229,  1952 => 1228,  1948 => 1227,  1944 => 1226,  1931 => 1216,  1926 => 1214,  1922 => 1213,  1918 => 1212,  1905 => 1202,  1900 => 1201,  1896 => 1199,  1883 => 1188,  1850 => 1157,  1846 => 1156,  1842 => 1155,  1838 => 1154,  1834 => 1153,  1821 => 1143,  1816 => 1141,  1812 => 1140,  1808 => 1139,  1804 => 1138,  1765 => 1101,  1762 => 1100,  1754 => 957,  1751 => 956,  1748 => 955,  1740 => 961,  1738 => 955,  1731 => 950,  1724 => 948,  1708 => 946,  1705 => 945,  1702 => 944,  1688 => 942,  1685 => 941,  1682 => 940,  1680 => 939,  1676 => 938,  1673 => 937,  1669 => 936,  1659 => 929,  1655 => 927,  1652 => 926,  1638 => 1089,  1632 => 1086,  1614 => 1070,  1608 => 1068,  1602 => 1066,  1600 => 1065,  1592 => 1059,  1586 => 1057,  1580 => 1055,  1578 => 1054,  1570 => 1048,  1564 => 1046,  1558 => 1044,  1556 => 1043,  1547 => 1036,  1541 => 1034,  1535 => 1032,  1533 => 1031,  1526 => 1026,  1520 => 1024,  1514 => 1022,  1512 => 1021,  1504 => 1015,  1498 => 1013,  1492 => 1011,  1490 => 1010,  1444 => 966,  1442 => 926,  1427 => 914,  1416 => 906,  1405 => 898,  1394 => 890,  1383 => 882,  1373 => 875,  1355 => 860,  1345 => 853,  1335 => 846,  1326 => 839,  1323 => 838,  1314 => 829,  1306 => 824,  1298 => 819,  1290 => 814,  1281 => 808,  1270 => 800,  1264 => 797,  1258 => 794,  1252 => 791,  1236 => 777,  1233 => 776,  1228 => 765,  1215 => 754,  1213 => 753,  1206 => 748,  1203 => 747,  1181 => 721,  1177 => 719,  1174 => 718,  1166 => 767,  1164 => 747,  1155 => 741,  1151 => 739,  1149 => 718,  1144 => 715,  1141 => 714,  1130 => 699,  1122 => 697,  1119 => 696,  1116 => 695,  1110 => 693,  1102 => 689,  1099 => 688,  1096 => 687,  1094 => 686,  1090 => 685,  1086 => 684,  1082 => 683,  1078 => 681,  1075 => 680,  1070 => 612,  1067 => 611,  1062 => 608,  1059 => 607,  1036 => 587,  628 => 181,  622 => 177,  620 => 176,  616 => 174,  613 => 173,  608 => 170,  605 => 169,  575 => 1298,  554 => 1279,  552 => 1100,  547 => 1097,  545 => 838,  539 => 834,  537 => 776,  531 => 772,  529 => 714,  520 => 707,  518 => 680,  511 => 678,  500 => 669,  492 => 664,  488 => 663,  484 => 661,  482 => 660,  478 => 658,  473 => 655,  466 => 649,  459 => 645,  453 => 644,  450 => 643,  448 => 642,  437 => 634,  422 => 621,  420 => 620,  418 => 619,  416 => 618,  414 => 617,  409 => 614,  407 => 611,  404 => 610,  402 => 607,  399 => 606,  397 => 173,  394 => 172,  392 => 169,  387 => 167,  383 => 166,  357 => 144,  353 => 141,  347 => 137,  343 => 134,  339 => 133,  335 => 132,  331 => 131,  327 => 130,  316 => 122,  312 => 121,  308 => 120,  294 => 108,  290 => 106,  286 => 104,  284 => 103,  281 => 102,  279 => 101,  276 => 100,  274 => 99,  271 => 98,  269 => 97,  266 => 96,  264 => 95,  261 => 94,  259 => 93,  256 => 92,  254 => 91,  251 => 90,  249 => 89,  246 => 88,  244 => 87,  241 => 86,  239 => 85,  236 => 84,  234 => 83,  231 => 82,  229 => 81,  226 => 80,  224 => 79,  221 => 78,  219 => 77,  216 => 76,  214 => 75,  211 => 74,  209 => 73,  206 => 72,  204 => 71,  201 => 70,  199 => 69,  196 => 68,  194 => 67,  191 => 66,  189 => 65,  186 => 64,  184 => 63,  181 => 62,  179 => 61,  176 => 60,  174 => 59,  171 => 58,  169 => 57,  166 => 56,  164 => 55,  161 => 54,  159 => 53,  156 => 52,  154 => 51,  151 => 50,  149 => 49,  146 => 48,  144 => 47,  141 => 46,  139 => 45,  136 => 44,  134 => 43,  131 => 42,  129 => 41,  126 => 40,  124 => 39,  121 => 38,  119 => 37,  116 => 36,  114 => 35,  111 => 34,  109 => 33,  106 => 32,  104 => 31,  101 => 30,  99 => 29,  96 => 28,  94 => 27,  91 => 26,  89 => 25,  86 => 24,  84 => 23,  81 => 22,  79 => 21,  76 => 20,  74 => 19,  71 => 18,  69 => 17,  66 => 16,  64 => 15,  61 => 14,  59 => 13,  56 => 12,  54 => 11,  51 => 10,  49 => 9,  46 => 8,  44 => 7,  42 => 6,  40 => 5,  38 => 4,  36 => 3,  32 => 1,);
    }
}

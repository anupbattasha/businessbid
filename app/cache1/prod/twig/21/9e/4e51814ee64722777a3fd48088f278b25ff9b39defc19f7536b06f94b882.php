<?php

/* BBidsBBidsHomeBundle:Home:template5.html.twig */
class __TwigTemplate_219e4e51814ee64722777a3fd48088f278b25ff9b39defc19f7536b06f94b882 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:template5.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "       <div class=\"welcome_header_area\">
           <div class=\"inner_wolcome_area\">
               <div class=\"wolcome_video_area\">
                   <div class=\"youtube_video_welcome\"><img src=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/macbook.png"), "html", null, true);
        echo " alt=\"Mac Book Pro\"></div>
                   <div class=\"ifream_wolcome\">
\t\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/La-gaNJQ64Q\" frameborder=\"0\" allowfullscreen></iframe>
\t\t\t\t\t</div>
               </div>
               <div class=\"wolcome_content_area\">
                   <h4>Welcome to BusinessBid</h4>
                   <p>BusinessBid is UAE’s largest one-stop services portal with over 700 services across multiple trades. We reach out to thousands of customers everyday to help them find quality service professionals like you with the expertise and reliability to complete their projects.</p>
                   <p>It’s your business – but let us do the hard work for you. Watch our short video to learn how we can help grow your business.</p>
                   <a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" class=\"sign_up_btn\">join NOW</a> 
               </div>
           </div>
       </div>
       <div class=\"join_why_area\">
           <div class=\"inner_join\">
               <div class=\"inner_join_text\">
                   <h1>Why join BusinessBid?</h1>
                   <p>Customers know that logging their project requirements with BusinessBid will mean they’ll receive quotations from
companies that have been screened and approved to be the best in the industry. Joining BusinessBid’s elite panel of
vendors will allow you to meet new customers and grow your business.</p>
                    <a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("bizbids_template4");
        echo "\" class=\"find_out_btn\">find out more</a>
               </div>
               <div class=\"inner_join_img\">
                   <img src=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/member.png"), "html", null, true);
        echo " alt=\"Why join BusinessBid?\">
               </div>
           </div>
       </div>
       <div class=\"how_does_work_main_area\">
           <div class=\"inner_how_does_work\">
               <div class=\"inner_how_does_img\">
                   <img src=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobilepc.png"), "html", null, true);
        echo " alt=\"How does BusinessBid work?\">
               </div>
               <div class=\"inner_how_does_txt\">
                   <h1>How does BusinessBid work?</h1>
                   <p>Our unique lead management system matches customer leads with service providers like you. We forward the
project details via text message and email so you can instantly view the job and respond if you are interested. </p>
                  <p>Tell us about the services you offer and leave the rest to BusinessBid.</p>
                  <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\" class=\"find_out_btn\">Find Out How it Works in 3 easy steps</a>
               </div>
           </div>
       </div>
       <div class=\"how_does_hire_main_area\">
           <div class=\"inner_how_does_hire\">
               <div class=\"inner_how_hire_txt\">
                   <h1>How do I get hired on BusinessBid?</h1>
                   <p>Once you have registered as a service provider on BusinessBid, you can be assured that you have
access to qualified customer leads thereby generating a reliable source of new business.</p>
                  <p>To help you improve your success rate at finalizing these projects, we have compiled some useful tips
and advice that is sure to give you an edge. </p>
                  <a href=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("bizbids_template3");
        echo "\" class=\"sign_up_btn\">Learn more</a>
               </div>
               <div class=\"inner_how_hire_img\">
                   <img src=";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/hend.png"), "html", null, true);
        echo " alt=\"How do I get hired on BusinessBid?\">
               </div>               
           </div>
       </div>
       <div class=\"how_does_work_main_area\">
           <div class=\"inner_how_does_work\">
               <div class=\"inner_how_does_img leads_igm\">
                   <img src=";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/leads.png"), "html", null, true);
        echo " alt=\"BusinessBid Leads\">
               </div>
               <div class=\"inner_how_does_txt leads_txts\">
                   <h1>BusinessBid Leads</h1>
                   <p>The BusinessBid fee structure is extremely simple. Once you receive a customer inquiry you are only charged if you are interested in quoting for the project. A lead credit is then deducted from your account and you are able to purchase more anytime you would like to top-up.</p>
                   <p>It’s indeed that simple!</p>
                  <a href=\"";
        // line 72
        echo $this->env->getExtension('routing')->getPath("bizbids_are_you_a_vendor");
        echo "\" class=\"sign_up_btn\">Click Here For More Information</a>
               </div>
           </div>
       </div> 
       <div class=\"how_does_hire_main_area\">
           <div class=\"inner_how_does_hire\">
               <div class=\"inner_how_hire_txt supp_vandor_text\">
                   <h1>Vendor Support</h1>
                   <p>At BusinessBid our aim is to ensure you get the best help and support. No matter how big or small your
concern we are there to hold your hand and assist you. All our business partners are allocated a dedicated
and experienced client manager who is committed to helping your business grow. </p>
                  <div class=\"support_area_vendor\">
                      <div class=\"supp_img\">
                          <img src=";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone_icon.png"), "html", null, true);
        echo " alt=\"Phone Icon\">
                      </div>
                      <div class=\"supp_text\">
                          <h4>VENDOR SUPPORT LINE:</h4>
                          <h3>(04) 421 3777</h3>
                      </div>
                  </div>
               </div>
               <div class=\"support_img_inner\">
                   <img src=";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/man.png"), "html", null, true);
        echo " alt=\"Vendor Support\">
               </div>               
           </div>
       </div>             
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    ";
    }

    // line 108
    public function block_maincontent($context, array $blocks = array())
    {
        // line 109
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:template5.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 109,  175 => 108,  157 => 94,  145 => 85,  129 => 72,  120 => 66,  110 => 59,  104 => 56,  89 => 44,  79 => 37,  69 => 30,  63 => 27,  49 => 16,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

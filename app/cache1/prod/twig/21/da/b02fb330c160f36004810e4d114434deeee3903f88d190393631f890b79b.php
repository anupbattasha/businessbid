<?php

/* ::consumer_dashboard.html.twig */
class __TwigTemplate_21dab02fb330c160f36004810e4d114434deeee3903f88d190393631f890b79b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'jquery' => array($this, 'block_jquery'),
            'customcss' => array($this, 'block_customcss'),
            'header' => array($this, 'block_header'),
            'menu' => array($this, 'block_menu'),
            'topmenu' => array($this, 'block_topmenu'),
            'pageblocknew' => array($this, 'block_pageblocknew'),
            'megamenu' => array($this, 'block_megamenu'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'pageblock' => array($this, 'block_pageblock'),
            'dashboard' => array($this, 'block_dashboard'),
            'statistics' => array($this, 'block_statistics'),
            'latestorders' => array($this, 'block_latestorders'),
            'enquiries' => array($this, 'block_enquiries'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<head>
<title>Business BID</title>
<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/eticket.css"), "html", null, true);
        echo "\">
<!--[if lt IE 9]>
\t<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/html5shiv.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.min.js"), "html", null, true);
        echo "\"></script>
<![endif]-->
\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.10.2.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
";
        // line 27
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/highcharts.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/modules/exporting.js"), "html", null, true);
        echo "\"></script>
<!--
<script>


</script>
-->
<!-- - main menu start- -->

<script>
\t
\$(document).ready(function(){
\t if (\$('input[name=\"form[city][]\"]:checked').length == \$('input[name=\"form[city][]\"]').length) {
       //do something
       \$('#checkcity').prop('checked',true);
    }
    else
    {
\t\t \$('#checkcity').prop('checked',false);
\t}
});
\$(document).ready(function(){
\t\$('#addform').click(function(){
\t\tvar setid = \$('#setdata').val();
\t\tvar data = (parseInt(setid) + 1)-1;
\t\tvar ff = parseInt(data)+ parseInt(1); 
\t\t\$('#setdata').val(ff);
\t\tif(ff < 9)
\t\t\t\$(\"#moreproducts\").append('<div class=\"morecertificates subcatadd\"><input  type=\"text\" id=\"form_cat\" name=\"cat[]\" placeholder=\"Enter your category\"/><div id=\"cat-add-error'+ff+'\" style=\"color:red;\"></div>');
\t\t/*
\t\tvar setid = \$('#setdata').val();
\t\tvar data = (parseInt(setid) + 1)-1;
\t\tvar ff = parseInt(data)+ parseInt(1); 
\t\tvar test = \"form[subcategory\"+ff+\"]\";
\t\t\$('#setdata').val(ff);
\t\t
\t\tvar form = document.createElement('input');
\t\tform.setAttribute('type','text');
\t\t//form.setAttribute('name','cat[]');
\t\t//form.setAttribute('id','form_cat');
\t\tform.setAttribute('placeholder','Enter the subcategory'); 
\t\t
\t\tvar anc = document.createElement('a');
\t\tanc.setAttribute('href','#');
\t\tanc.setAttribute('class','remove_field');
\t\tanc.innerHTML = \"Remove\";
\t\t
\t\tvar div  = document.createElement('div');
\t\t//div.setAttribute('class','morecertificates');
\t\t
\t\tdiv.append(form);
\t\t//div.append(anc);
\t\t
\t\t//\$(\"#moreproducts\").append(anc);  
\t\t*/
\t});
\t
\t\$('#addform1').click(function(){
\t\tvar setid = \$('#setdata').val();
\t\tvar data = (parseInt(setid) + 1)-1;
\t\tvar ff = parseInt(data)+ parseInt(1); 
\t\t\$('#setdata').val(ff);
\t\tif(ff < 9)
\t\t\t\$(\"#moreproducts1\").append('<div class=\"morecertificates subcatadd\"><input  type=\"text\" id=\"form_cat\" name=\"cat[]\" placeholder=\"Enter your category\"/><div id=\"cat-add-error'+ff+'\" style=\"color:red;\"></div>');
\t});
\t
\t \$('.remove_field').click(function(){ //user click on remove text
        \$(this).closest('div').remove();
    });
\t\$('[id^=\"editcat\"]').click(function(){
\t\tvar id = \$(this).attr('id').split('editcat')[1];
\t\t\$(\"#\"+id).append('<div class=\"editblock'+id+'\"><form action=\"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("bizbids_editcat");
        echo "\"><input type=\"text\" id=\"editdataa\" name=\"edited\"/><input type=\"hidden\" value= \"'+id+'\" id=\"idval\" name=\"idval\"/><input type=\"submit\" name=\"submit\" value=\"submit\" id=\"edit-submit\"/><a href=\"#\" class=\"can'+id+'\" id=\"cancel-edit\">Cancel</a></form></div>');
\t\t\$(\"#editcat\"+id).hide();
\t\t});
\t\t
\t\$('[id^=\"editcat-port\"]').click(function(){
\t\tvar id = \$(this).attr('id').split('editcat-port')[1];
\t\t\$(\"#\"+id).append('<div class=\"editblock-port'+id+'\"><form action=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("bizbids_editcat_port");
        echo "\"><input type=\"text\" id=\"editdataa1\" name=\"edited\"/><input type=\"hidden\" value= \"'+id+'\" id=\"idval\" name=\"idval\"/><input type=\"submit\" name=\"submit\" value=\"submit\" id=\"edit-submit\"/><a href=\"#\" class=\"can1'+id+'\" id=\"cancel-edit1\">Cancel</a></form></div>');
\t\t\$(\"#editcat\"+id).hide();
\t\t});
\t
\t\$(document).on(\"click\", '#cancel-edit', function() {
\t\tvar id = \$(this).attr('class').split('can')[1];
\t\t\$(\"#editcat\"+id).show();
\t\t\$('.editblock'+id).hide();\t
\t\t\$('html,body').animate({
\t\tscrollTop: \$(\"#editdt\").offset().top},
\t\t'slow');
\t\t
\t});
\t
\t\$(document).on(\"click\", '#cancel-edit1', function() {
\t\tvar id = \$(this).attr('class').split('can1')[1];
\t\t\$(\"#editcat-port\"+id).show();
\t\t\$('.editblock-port'+id).hide();\t
\t\t\$('html,body').animate({
\t\tscrollTop: \$(\"#editdt-port\").offset().top},
\t\t'slow');
\t\t
\t});
\t
\t
    \$('#checkcity').click(function(event) {  //on click 
        if(this.checked) { // check select status
            \$('input[name=\"form[city][]\"]').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class \"checkbox1\"               
            });
        }else{
            \$('input[name=\"form[city][]\"]').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class \"checkbox1\"                       
            });         
        }
    });
 
\t
\t
\t/*
\t\$('#savecat').click(function(){
\t\t\t\t
\t\tif(\$('[id^=\"cat-add-error\"]').val() == '')
\t\t{
\t\t\tvar id = \$(this).attr('id').split('cat-add-error')[1];
\t\t\tconsole.log(id);
\t\t\t\$('#cat-add-error').text(\"Category cannot be left blank\");
\t\t\treturn false;
\t\t}
\t\telse
\t\t{
\t\t\treturn true;
\t\t}
\t\t});
\t*/
\t
\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

});
</script>

<script>
  \$(function() {
    \$( \"#datepicker1\" ).datepicker();
  });
\$(function() {
    \$( \"#datepicker2\" ).datepicker();
  });

</script>

<script>
\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$('#category').autocomplete({
\t      \tsource: function( request, response ) {
\t\t      \t\$.ajax({
\t\t\t      \turl : realpath+\"ajax-info.php\",
\t\t\t      \tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t   name_startsWith: request.term,
\t\t\t\t\t   type: 'category'
\t\t\t\t\t},
\t \t\t\t\tsuccess: function( data ) {
\t\t \t\t\t\tresponse( \$.map( data, function( item ) {
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\tlabel: item,
\t\t\t\t\t\t\t\tvalue: item
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}));
\t\t\t\t\t},
\t\t\t\t\tselect: function(event, ui) {
                       alert( \$(event.target).val() );
                    }
      \t\t\t});
\t      \t},
\t      \tautoFocus: true,
\t      \tminLength: 2
      \t});
    });
\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_subcategory\").html('<option value=\"0\">Select</option>');
\t\$(\"#form_category\").on('change',function (){
\tvar ax = \t\$(\"#form_category\").val();

\t\$.ajax({url:realpath+\"pullSubCategoryByCategory.php?categoryid=\"+ax,success:function(result){

\t\$(\"#form_subcategory\").html(result);
\t}});
\t});

});

\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_email\").on('input',function (){
\tvar ax = \t\$(\"#form_email\").val();
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}
\t});

});

\t\$(document).ready(function(){
\t\$('#form_description').attr(\"maxlength\",\"300\");
\t\$('#form_location').attr(\"maxlength\",\"30\");

\t});

</script>

\t<script>
\t\t\$(document).ready(function() {
\t\t\tsetTimeout(function() {
\t\t\t\t// Slide

\t\t\t\t\$('#menu > div > a.expanded + ul').slideToggle('medium');
\t\t\t\t\$('#menu > div > a').click(function() {
\t\t\t\t\t\$('#menu > div > a.expanded').not(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
\t\t\t\t\t\$(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
\t\t\t\t});
\t\t\t}, 250);
\t\t});
\t</script>

<script type='text/javascript' >

\tfunction jsfunction(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//~ alert(xmlhttp.responseText);
\t//alert('fghf');
\tdocument.getElementById(\"searchval\").innerHTML=xmlhttp.responseText;
\tmegamenublogAjax();
\tmegamenubycatAjax();
\tmegamenubycatVendorSearchAjax();
\tmegamenubycatVendorReviewAjax();
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"ajax-info.php\",true);
\txmlhttp.send();



\t}

function megamenublogAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t\t\t\t\t\t\t\t\t//document.getElementById(\"megamenu1\").innerHTML=xmlhttp.responseText;
\t\t/*var mvsa = document.getElementById(\"megamenu1Anch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Resourcesegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}*/
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menublog.php\",true);
\t//xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);

\txmlhttp.send();



\t}
\tfunction megamenubycatAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\tdocument.getElementById(\"megamenubycatUl\").innerHTML=xmlhttp.responseText;
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menubycat.php\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorSearchAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t//console.log(xmlhttp.responseText);
\t\tvar mvsa = document.getElementById(\"megamenuVenSearchAnch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Searchsegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t//document.getElementById(\"megamenuVenSearchAnch\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorSearch\",true);
\txmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorReviewAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();

\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t\t//alert(xmlhttp.responseText);
\t\t\tvar mvsa = document.getElementById(\"megamenuVenReviewAnch\");
\t\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Reviewsegment-drop\") {
\t\t\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t\t\t  break;
\t\t\t\t\t}
\t\t\t}

\t\t//document.getElementById(\"megamenuVenReviewUl\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t  xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorReview\",true);
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorReview\",true);
\txmlhttp.send();



\t}

</script>

<script type=\"text/javascript\">
function show_submenu(b, menuid)
{
\talert(menuid);

\tdocument.getElementById(b).style.display=\"block\";
\tif(menuid!=\"megaanchorbycat\"){
\tdocument.getElementById(menuid).className = \"setbg\";
\t}
}

function hide_submenu(b, menuid)
{


\tsetTimeout(dispminus(b,menuid), 200);



\t//document.getElementById(b).onmouseover = function() { document.getElementById(b).style.display=\"block\"; }
\t//document.getElementById(b).onmouseout = function() { document.getElementById(b).style.display=\"none\"; }
\t//document.getElementById(b).style.display=\"none\";
}

function dispminus(subid,menuid){
\t//if(menuid!=\"megaanchorbycat\"){
\t//document.getElementById(menuid).className = \"unsetbg\";
\t//}
\t//document.getElementById(subid).style.display=\"none\";


\t}


\$(document).ready(function(){
  \$(\"#flip\").click(function(){
    \$(\"#panel\").slideToggle(\"slow\");
  });

});

 \$(document).ready(function(){
      \$(\".Searchsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Searchsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });
 \$(document).ready(function(){
      \$(\".Reviewsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Reviewsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });


</script>
<!---main menu ends --->
";
        // line 518
        $this->displayBlock('jquery', $context, $blocks);
        // line 521
        echo "
";
        // line 522
        $this->displayBlock('customcss', $context, $blocks);
        // line 525
        echo "

</head>
<body>

";
        // line 530
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 531
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 532
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 533
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 534
        echo "
";
        // line 535
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 536
            echo "<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 contact-info\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>
\t\t\t\t\t<span>(04) 42 13 777</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3 left-clear\">
\t\t\t\t\t<span class=\"contact-no\"><a href=\"";
            // line 545
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
            echo "\">Contact Us</a></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6 clear-right dash-right-top-links\">
\t\t\t\t<ul>
\t\t\t\t\t<li>Welcome, <span class=\"profile-name\">";
            // line 550
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
\t\t\t\t\t<li><a href=\"";
            // line 551
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

";
        }
        // line 559
        echo "
<div class=\"main_container\">
\t";
        // line 561
        $this->displayBlock('header', $context, $blocks);
        // line 570
        echo "\t";
        $this->displayBlock('menu', $context, $blocks);
        // line 607
        echo "

\t";
        // line 609
        $this->displayBlock('pageblocknew', $context, $blocks);
        // line 675
        echo "
\t";
        // line 676
        $this->displayBlock('pageblock', $context, $blocks);
        // line 785
        echo "
</div>
    \t</div>
</div>
\t";
        // line 789
        $this->displayBlock('footer', $context, $blocks);
        // line 798
        echo "<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>

<script type=\"text/javascript\">
window.onload = jsfunction();
</script>

</html>
";
    }

    // line 518
    public function block_jquery($context, array $blocks = array())
    {
        // line 519
        echo "
";
    }

    // line 522
    public function block_customcss($context, array $blocks = array())
    {
        // line 523
        echo "
";
    }

    // line 561
    public function block_header($context, array $blocks = array())
    {
        // line 562
        echo "\t<div class=\"header container\">
    \t<div class=\"col-md-6 logo\">
        \t<a href=\"";
        // line 564
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" > <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"> </a>
        </div>
        <div class=\"col-md-6\">
        </div>
    \t</div>
\t";
    }

    // line 570
    public function block_menu($context, array $blocks = array())
    {
        // line 571
        echo "\t<div class=\"menu_block\">
\t\t\t<div class=\"container\">


\t\t\t\t";
        // line 575
        $this->displayBlock('topmenu', $context, $blocks);
        // line 604
        echo "        \t</div>
  \t\t</div>
\t";
    }

    // line 575
    public function block_topmenu($context, array $blocks = array())
    {
        // line 576
        echo "\t\t\t\t\t<div class=\"navHeaderCollapse col-sm-7\">
\t\t\t\t\t\t<ul class=\"nav navbar-nav navbar-left\">
\t\t\t\t\t\t\t<!--<li><a href=\"";
        // line 578
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
        echo "\">Dashboard</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 579
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_enquiries");
        echo "\">View All Enquires</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 580
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders");
        echo "\">View All Orders</a></li>-->

\t\t\t\t\t\t\t<li><a href=\"";
        // line 582
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
\t\t\t\t\t\t\t<li id = \"megamenuVenSearchAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Search<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Searchsegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li id = \"megamenuVenReviewAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Review<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Reviewsegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t\t</li>


\t\t\t\t\t\t\t";
        // line 593
        if ((((isset($context["useridexistsfreeleads"]) ? $context["useridexistsfreeleads"] : null) == 0) && ((isset($context["useridexists"]) ? $context["useridexists"] : null) == 0))) {
            // line 594
            echo "                            <li><a class=\"free-trial\" href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_free_trial");
            echo "\">Free Trial</a></li>
\t\t\t\t\t\t\t\t";
        }
        // line 596
        echo "\t\t\t\t\t\t</ul></div>
\t\t\t\t\t\t<div class=\"col-md-5 search_block\">
\t\t\t\t\t\t<form name=\"search\" method=\"request\" action=\"";
        // line 598
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
\t\t\t\t\t\t\t<input class=\"col-sm-8 search-box\" type=\"text\" list=\"browsers\" placeholder=\"Type the service you require\" id='category' name='category' ><datalist id=\"searchval\"></datalist>
\t\t\t\t\t\t\t<input type=\"submit\" value=\"Search\" class=\"search-btn form_submit col-sm-3\" />
\t\t\t\t\t\t\t</form></div>
\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 609
    public function block_pageblocknew($context, array $blocks = array())
    {
        // line 610
        echo "\t<div class=\"page-bg\">
    \t<div class=\"container inner_container\">
\t\t<div class=\"row \">
\t\t\t<div class=\"dashboard-panel \">

\t\t\t\t\t";
        // line 615
        $this->displayBlock('megamenu', $context, $blocks);
        // line 633
        echo "\t\t\t\t";
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 672
        echo "

\t";
    }

    // line 615
    public function block_megamenu($context, array $blocks = array())
    {
        // line 616
        echo "\t<div id=\"megamenuVenSearch\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" ><div><ul id=\"megamenuVenSearchUl\"></ul></div></div>
\t<div id=\"megamenuVenReview\" class=\"submenu menu-mega megamenubg-1 megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');\"><div class=\"review-menu-left\"><ul id=\"megamenuVenReviewUl\"></ul></div><div  class=\"review-menu-right\"><button class=\"btn btn-success write-button\" type=\"button\" >Write a Review</button></div></div>
\t<ul id=\"megamenu1\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenu1','megamenu1Anch');\" onMouseOut=\"javascript:hide_submenu12('megamenu1','megamenu1Anch');\" ></ul>
\t<div id=\"megamenubycat\" class=\"submenu menu-megabycat megamenubg\" onMouseOver=\"javascript:show_submenu('megamenubycat','megaanchorbycat');\" onMouseOut=\"javascript:hide_submenu('megamenubycat','megaanchorbycat');\"><div><ul id=\"megamenubycatUl\"></ul></div><div></div></div>

\t";
        // line 621
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 622
            echo "
\t<script type=\"text/javascript\">

\tdocument.getElementById(\"megamenuVenSearch\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenuVenReview\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenu1\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenubycat\").setAttribute(\"class\", \"submenu menu-megabycat-login megamenubg\");
\t</script>


\t";
        }
        // line 632
        echo "\t";
    }

    // line 633
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 634
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 637
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
        echo "\">Vendor Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"account\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">My Account <span class=\"caret\"></span></a>
\t\t\t\t\t\t\t\t<ul id=\"collapseOne\" class=\"panel-collapse collapse menu-col\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 640
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_account");
        echo "\">My Profile</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 641
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendorupdatepasssword");
        echo "\">Change Password</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 642
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendorupdatemail");
        echo "\">Update Email</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 643
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendordeleteAccount");
        echo "\">Delete Account</a>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t";
        // line 647
        if ((array_key_exists("emailCount", $context) && ((isset($context["emailCount"]) ? $context["emailCount"] : null) != 0))) {
            // line 648
            echo "                            <li>
                            \t<a class=\"messages1\" href=\"";
            // line 649
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_mymessages");
            echo "\">Messages<span class=\"mess-read\">NEW</span></a>
                            </li>
                            ";
        } else {
            // line 652
            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a class=\"messages\" href=\"";
            // line 653
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_mymessages");
            echo "\">Messages</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        }
        // line 656
        echo "\t\t\t\t\t\t\t<li><a class=\"reviews\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_enquiries");
        echo "\">My Job Requests</a></li>
\t\t\t\t\t      \t";
        // line 658
        echo "\t\t\t\t\t      \t<li><a class=\"lead-purchases\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_purchase_leads");
        echo "\">Purchase Leads</a></li>
\t\t\t\t        \t<li><a class=\"orders\" href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders");
        echo "\">View All Orders</a></li>
\t\t\t\t        \t<li><a class=\"enquiry\" href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_my_reviews");
        echo "\">Reviews and Ratings</a></li>
\t\t\t\t        \t<li><a class=\"lead-management\" href=\"";
        // line 661
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_leads_history");
        echo "\">Lead Management</a></li>
\t\t\t\t\t\t\t<li><a class=\"support\" href=\"";
        // line 662
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_home");
        echo "\">Support</a>
\t\t\t\t\t\t\t\t<!--<ul class=\"ticket-links\">
\t\t\t\t\t\t\t\t<li><a class=\"creat-ticket\" href=\"";
        // line 664
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_support");
        echo "\">Create Ticket</a></li>
\t\t\t\t\t\t\t\t<li><a class=\"view-ticket\" href=\"";
        // line 665
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_supportview");
        echo "\">View Ticket</a></li>
\t\t\t\t\t\t\t\t</ul>-->
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t";
    }

    // line 676
    public function block_pageblock($context, array $blocks = array())
    {
        // line 677
        echo "    \t<div class=\"container inner_container\">
    \t\t<h1>Dashboard</h1>
\t\t";
        // line 679
        $this->displayBlock('dashboard', $context, $blocks);
        // line 722
        echo "
\t\t";
        // line 723
        $this->displayBlock('statistics', $context, $blocks);
        // line 731
        echo "
\t\t";
        // line 732
        $this->displayBlock('latestorders', $context, $blocks);
        // line 757
        echo "
\t\t";
        // line 758
        $this->displayBlock('enquiries', $context, $blocks);
        // line 782
        echo "
    \t</div>
\t";
    }

    // line 679
    public function block_dashboard($context, array $blocks = array())
    {
        // line 680
        echo "        \t<div class=\"col-md-6\">i
        \t\t<div class=\"overview_block block\">
            \t\t\t\t<h2>Overview</h2>
            \t\t\t\t\t<table>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>Total Job Requests</td>
\t\t\t\t\t\t\t<td>";
        // line 686
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["enquiriesvendorcount"]) ? $context["enquiriesvendorcount"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["enqvendor"]) {
            // line 687
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "noofenqueries", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enqvendor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 689
        echo "\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>Total Job Requests Accepted</td>
\t\t\t\t\t\t\t<td>1111</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>Total Orders</td>
\t\t\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 697
        echo twig_escape_filter($this->env, (isset($context["totalordercount"]) ? $context["totalordercount"] : null), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Overall Ratings (Highest is 5) </td>
\t\t\t\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 701
        if (((isset($context["enquiriesvendorcount"]) ? $context["enquiriesvendorcount"] : null) == 1)) {
            echo twig_escape_filter($this->env, (isset($context["enquiriesvendorcount"]) ? $context["enquiriesvendorcount"] : null), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, (isset($context["enquiriesvendorcount"]) ? $context["enquiriesvendorcount"] : null), "html", null, true);
        }
        echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Service Categories</td>
\t\t\t\t\t\t\t\t\t<td><span class=\"pull-left overview-catgories\">
\t\t\t\t\t\t\t\t\t\t";
        // line 706
        if (((isset($context["orders"]) ? $context["orders"] : null) == "")) {
            // line 707
            echo "\t\t\t\t\t\t\t\t\t\t<p>N/A</p>
\t\t\t\t\t\t\t\t\t\t";
        } else {
            // line 709
            echo "\t\t\t\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
                // line 710
                echo "\t\t\t\t\t\t\t\t\t\t<li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "category", array()), "html", null, true);
                echo "</li>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 712
            echo "\t\t\t\t\t\t\t\t\t\t";
        }
        // line 713
        echo "\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t</table>

            \t\t</div>
        \t</div>
\t\t";
    }

    // line 723
    public function block_statistics($context, array $blocks = array())
    {
        // line 724
        echo "        \t<div class=\"col-md-6  last\">
        \t<div class=\"overview_block block\">
        \t    \t<h2>Statistics</h2>

            \t</div>
        \t</div>
\t\t";
    }

    // line 732
    public function block_latestorders($context, array $blocks = array())
    {
        // line 733
        echo "        \t<div class=\"col-md-12 mid\">
        \t<div class=\"overview_block block\">
            \t\t<h2>Latest 10 Orders</h2>
            \t\t<table border=\"1\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Orderid</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t<th>Date Added</th>
\t\t\t\t\t\t\t<th>Amount</th></tr>
\t\t\t\t\t\t\t\t\t";
        // line 742
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["enquiry"]) {
            // line 743
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>";
            // line 744
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "ordernumber", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 745
            if (($this->getAttribute($context["enquiry"], "status", array()) == 1)) {
                echo " processed ... ";
            } else {
                echo " processing ";
            }
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 746
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 747
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "amount", array()), "html", null, true);
            echo "</td>

\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquiry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 751
        echo "            \t\t</table>

            \t</div>
        \t</div>
\t\t\t</div>
\t\t";
    }

    // line 758
    public function block_enquiries($context, array $blocks = array())
    {
        // line 759
        echo "\t\t\t\t\t\t<table border=\"1\">
\t\t\t\t\t\t\t\t<thead><tr>
\t\t\t\t\t\t\t\t\t<th>Enquiry Name</th>
\t\t\t\t\t\t\t\t\t<th>Category</th>
\t\t\t\t\t\t\t\t\t<th>Enquiry Date</th>
\t\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t\t</tr></thead>
                                     ";
        // line 766
        if (twig_test_empty((isset($context["enquiriesvendor"]) ? $context["enquiriesvendor"] : null))) {
            // line 767
            echo "                                      <tr><td>No Records Found<td></tr>
                                     ";
        } else {
            // line 769
            echo "
\t\t\t\t\t\t\t\t\t";
            // line 770
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiriesvendor"]) ? $context["enquiriesvendor"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["enqvendor"]) {
                // line 771
                echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 772
                if (twig_test_empty($this->getAttribute($context["enqvendor"], "acceptstatus", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "subj", array()), "html", null, true);
                } else {
                    echo "<a href=\" ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_view_consumer", array("id" => $this->getAttribute($context["enqvendor"], "authorid", array()), "eid" => $this->getAttribute($context["enqvendor"], "id", array()))), "html", null, true);
                    echo " \">";
                    if ((twig_length_filter($this->env, $this->getAttribute($context["enqvendor"], "subj", array())) > 20)) {
                        echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enqvendor"], "subj", array()), 0, 20), "html", null, true);
                        echo "... ";
                    } else {
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "subj", array()), "html", null, true);
                        echo " ";
                    }
                    echo "</a>";
                }
                echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 773
                echo twig_escape_filter($this->env, $this->getAttribute($context["enqvendor"], "category", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 774
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enqvendor"], "created", array()), "Y-m-d"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 775
                if (twig_test_empty($this->getAttribute($context["enqvendor"], "acceptstatus", array()))) {
                    echo " Not Accepted ";
                } else {
                    echo " Accepted ";
                }
                echo "</td>

\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enqvendor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 779
            echo "                                                                ";
        }
        // line 780
        echo "                        \t\t\t\t\t\t</table>
\t\t\t\t\t\t";
    }

    // line 789
    public function block_footer($context, array $blocks = array())
    {
        // line 790
        echo "    \t<div class=\"footer-dashboard text-center\">
\t\t<div class=\"container\">
\t\t<div class=\"col-sm-6 footer-messages\">Copyright 2013 - 2014 BusinessBid</div>
    \t<div class=\"col-sm-6 clear-right\"></div>

    \t</div>
\t\t</div>
    \t";
    }

    public function getTemplateName()
    {
        return "::consumer_dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1235 => 790,  1232 => 789,  1227 => 780,  1224 => 779,  1210 => 775,  1206 => 774,  1202 => 773,  1183 => 772,  1180 => 771,  1176 => 770,  1173 => 769,  1169 => 767,  1167 => 766,  1158 => 759,  1155 => 758,  1146 => 751,  1136 => 747,  1132 => 746,  1124 => 745,  1120 => 744,  1117 => 743,  1113 => 742,  1102 => 733,  1099 => 732,  1089 => 724,  1086 => 723,  1074 => 713,  1071 => 712,  1062 => 710,  1057 => 709,  1053 => 707,  1051 => 706,  1039 => 701,  1032 => 697,  1022 => 689,  1013 => 687,  1009 => 686,  1001 => 680,  998 => 679,  992 => 782,  990 => 758,  987 => 757,  985 => 732,  982 => 731,  980 => 723,  977 => 722,  975 => 679,  971 => 677,  968 => 676,  957 => 665,  953 => 664,  948 => 662,  944 => 661,  940 => 660,  936 => 659,  931 => 658,  926 => 656,  920 => 653,  917 => 652,  911 => 649,  908 => 648,  906 => 647,  899 => 643,  895 => 642,  891 => 641,  887 => 640,  881 => 637,  876 => 634,  873 => 633,  869 => 632,  856 => 622,  854 => 621,  847 => 616,  844 => 615,  838 => 672,  835 => 633,  833 => 615,  826 => 610,  823 => 609,  813 => 598,  809 => 596,  803 => 594,  801 => 593,  787 => 582,  782 => 580,  778 => 579,  774 => 578,  770 => 576,  767 => 575,  761 => 604,  759 => 575,  753 => 571,  750 => 570,  738 => 564,  734 => 562,  731 => 561,  726 => 523,  723 => 522,  718 => 519,  715 => 518,  701 => 798,  699 => 789,  693 => 785,  691 => 676,  688 => 675,  686 => 609,  682 => 607,  679 => 570,  677 => 561,  673 => 559,  662 => 551,  658 => 550,  650 => 545,  639 => 536,  637 => 535,  634 => 534,  632 => 533,  630 => 532,  628 => 531,  626 => 530,  619 => 525,  617 => 522,  614 => 521,  612 => 518,  199 => 108,  190 => 102,  116 => 31,  112 => 30,  108 => 29,  104 => 28,  99 => 27,  95 => 25,  91 => 24,  87 => 23,  79 => 18,  75 => 17,  70 => 15,  66 => 14,  62 => 13,  58 => 12,  54 => 11,  50 => 10,  45 => 8,  40 => 6,  33 => 1,);
    }
}

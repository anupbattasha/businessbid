<?php

/* BBidsBBidsHomeBundle:Categoriesform:signage_signboards_form.html.twig */
class __TwigTemplate_802f974bea478928b55932370454b24859e26f886b302a95f352a9ac60f2befd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:signage_signboards_form.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<style>
    .pac-container:after{
    content:none !important;
}
.mask{
    display: none;
}
</style>
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
        <div class=\"alert alert-danger\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
        ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "
        <div class=\"alert alert-success\">";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 30
            echo "
        <div class=\"alert alert-success\">";
            // line 31
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "    </div>
    <div class=\"cat-form\">
        <div class=\"page-title\"><h1>GET FREE QUOTES FROM SIGNAGE & SIGNBOARD COMPANIES IN THE UAE</h1></div>
        <div class=\"cat-form-block\">
            <div class=\"category-steps-block\">
                <div class=\"col-md-4 steps\"><span>1</span><p>Choose what services you require</p></div>
                <div class=\"col-md-4 steps\"><span>2</span><p>Vendors contact you & offer you quotes</p></div>
                <div class=\"col-md-4 steps\"><span>3</span><p>You engage the best Vendor</p></div>
            </div>
            ";
        // line 43
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
            <div class=\"cat-list-cont\">
                <div class=\"cat-list-block\" id=\"signage_service\">
                    <div class=\"col-md-1 icon-block\">
                        <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sin1.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>Which of the below signage service do you require?*</h2>
                        ";
        // line 51
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 52
            echo "                            <div class=\"col-md-4 sub-category\">
                                ";
            // line 53
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "signage")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                        <div class=\"error\" id=\"sign_service\"></div>
                    </div>

                </div>

                <div class=\"cat-list-block\" id=\"width\">
                    <div class=\"col-md-1 icon-block\">
                        <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sin2.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>What is the approximate width?*</h2>
                        <div class=\"row1\">
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "swidth")));
        echo "
                                ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "swidth")));
        echo "
                                ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "swidth")));
        echo "
                                    ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "swidth")));
        echo "
                                ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 4, array(), "array"), 'widget', array("attr" => array("class" => "swidth")));
        echo "
                                ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 4, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 5, array(), "array"), 'widget', array("attr" => array("class" => "swidth")));
        echo "
                                ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_width", array()), "children", array()), 5, array(), "array"), 'label');
        echo "
                            </div>
                        </div>
                        <div class=\"error\" id=\"sign_width\"></div>
                    </div>

                </div>

                <div class=\"cat-list-block\" id=\"height\">
                    <div class=\"col-md-1 icon-block\">
                        <img src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sin3.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>What is the approximate height?*</h2>
                        <div class=\"row1\">
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "sheight")));
        echo "
                                ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "sheight")));
        echo "
                                ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "sheight")));
        echo "
                                    ";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "sheight")));
        echo "
                                ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 4, array(), "array"), 'widget', array("attr" => array("class" => "sheight")));
        echo "
                                ";
        // line 123
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 4, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 5, array(), "array"), 'widget', array("attr" => array("class" => "sheight")));
        echo "
                                ";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "appox_height", array()), "children", array()), 5, array(), "array"), 'label');
        echo "
                            </div>
                        </div>
                        <div class=\"error\" id=\"sign_heigth\"></div>
                    </div>

                </div>


                <div class=\"cat-list-block\" id=\"installation\">
                    <div class=\"col-md-1 icon-block\">
                        <img src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sin4.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>Does the signage require installation?</h2>
                        <div class=\"row1\">
                            <div class=\"form-group col-md-6 clear-left\">
                                ";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "installation", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "sinstall")));
        echo "
                                ";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "installation", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-6 clear-left\">
                                ";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "installation", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "sinstall")));
        echo "
                                ";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "installation", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                            </div>
                        </div>
                        <div class=\"error\" id=\"sign_installation\"></div>
                    </div>

                </div>


                 ";
        // line 158
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 159
            echo "            <div class=\"cat-list-block\" id=\"name\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 160
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon6.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 162
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 164
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "name")));
            echo "
                    </div>
                    <div class=\"error\" id=\"sign_name\"></div>
                </div>
            </div>
            <div class=\"cat-list-block\" id=\"email_id\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 170
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon7.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 172
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 174
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "email_id")));
            echo "
                    </div>
                    <div class=\"error\" id=\"sign_email\"></div>
                </div>
            </div>
            ";
        }
        // line 180
        echo "
             <div class=\"cat-list-block\" id=\"location\">
                <div class=\"col-md-1 icon-block space-top \"><img src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon8.png"), "html", null, true);
        echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>Select your Location*</h2>
                    <div class=\"row1\">
                        <div class=\"form-control cus-location\">
                            <div class=\"col-md-6 align-right\">
                                ";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "location")));
        echo "
                                ";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"col-md-6\">
                                ";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "location")));
        echo "
                                ";
        // line 193
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"error\" id=\"sign_loc\"></div>
                </div>
            </div>


            ";
        // line 202
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            // line 203
            echo "            <div class=\"cat-list-block mask\" id=\"domCity\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 204
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-city.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 206
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 208
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_city\"></div>
                </div>
            </div>
            ";
        }
        // line 214
        echo "
            ";
        // line 215
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 216
            echo "            <div class=\"cat-list-block mask\" id=\"domArea\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 217
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/area-icon.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 219
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 221
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_area\"></div>
                </div>
            </div>
            ";
        }
        // line 227
        echo "
            ";
        // line 228
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 229
            echo "            <div class=\"row1 mask\" id=\"domContact\">
                <div class=\"col-md-6 clear-left\">
                    <div class=\"cat-list-block\" id=\"uae_mobileno\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 232
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-mobile.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 234
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left int-country\"><label><b>Area Code</b> </label>";
            // line 236
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
            echo "</div> <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget');
            echo "</div>
                            </div>
                        <div class=\"error\" id=\"acc_mobile\"></div>
                        </div>
                    </div>
                </div>
                ";
            // line 254
            echo "                <div class=\"col-md-6 clear-right\">
                    <div class=\"cat-list-block\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 256
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-phone.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 258
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left int-country\"> <label><b>Area Code</b> </label>";
            // line 260
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
            echo "</div>  <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget');
            echo " </div>
                            </div>
                        <div class=\"error\"></div>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }
        // line 268
        echo "
            ";
        // line 269
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 270
            echo "            <div class=\"cat-list-block mask\" id=\"intCountry\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 271
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-country.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 273
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 275
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_intcountry\"></div>
                </div>
            </div>

            <div class=\"cat-list-block mask\" id=\"intCity\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 282
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-city.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 284
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 286
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_intcity\"></div>
                </div>
            </div>
            ";
        }
        // line 292
        echo "
            ";
        // line 293
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 294
            echo "            <div class=\"row clear-left clear-right mask\" id=\"intContact\">
                <div class=\"col-md-6 clear-left\">
                    <div class=\"cat-list-block\" id=\"int_mobileno\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 297
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-mobile.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 299
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left  int-country\"><label><b>Country Code</b> <i>+</i></label>";
            // line 301
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget');
            echo "</div> <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget');
            echo "</div>
                            </div>
                        <div class=\"error\" id=\"acc_intmobile\"></div>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6 clear-left\">
                    <div class=\"cat-list-block\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 309
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-phone.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 311
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left  int-country\"><label><b>Country Code</b> <i>+</i></label> ";
            // line 313
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget');
            echo "</div>  <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget');
            echo " </div>
                            </div>
                        <div class=\"error\" ></div>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }
        // line 321
        echo "
            <div class=\"cat-list-block\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
        // line 323
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon4.png"), "html", null, true);
        echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
        // line 325
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'label');
        echo "</h2>
                    <div class=\"row1\">
                        ";
        // line 327
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                <div class=\"error\" id=\"acc_hire\"></div>
                </div>
            </div>

            <div class=\"cat-list-block\">
                <div class=\"col-md-1 icon-block space-top-more\"> <img src=\"";
        // line 334
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon9.png"), "html", null, true);
        echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
        // line 336
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label');
        echo "</h2>
                    <div class=\"row1\">
                        ";
        // line 338
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "
                        <span>(max 120 characters)</span>
                    </div>
                <div class=\"error\" id=\"acc_desc\"></div>
                </div>
            </div>



                <script type=\"text/javascript\" src=\"https://www.google.com/recaptcha/api/challenge?k=6LcCrQMTAAAAAA3zTbRoCk0tmgghhTt_onX5-gMz\"></script>
                <noscript>
               <iframe src=\"https://www.google.com/recaptcha/api/noscript?k=6LcCrQMTAAAAAA3zTbRoCk0tmgghhTt_onX5-gMz\"
                   height=\"300\" width=\"500\" frameborder=\"0\"></iframe><br>
             </noscript>
             <div class=\"extra-text\">
                 <div class=\"extra-block\">
                     <ul>
                        <li>Enter text then click on button below to get quotes</li>
                        <li>Your privacy is important, we do not provide information to third parties</li>
                     </ul>
                 </div>
             </div>
        <div class=\"cat-submit\">";
        // line 360
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "GET QUOTES NOW"));
        echo "</div>
            </div>
            ";
        // line 362
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
        </div>
    </div>
</div>


<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>

<script type=\"text/javascript\" src=\"";
        // line 375
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/signage_signboard.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 376
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/categoriesform.general.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:signage_signboards_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  722 => 376,  718 => 375,  702 => 362,  697 => 360,  672 => 338,  667 => 336,  662 => 334,  652 => 327,  647 => 325,  642 => 323,  638 => 321,  625 => 313,  620 => 311,  615 => 309,  602 => 301,  597 => 299,  592 => 297,  587 => 294,  585 => 293,  582 => 292,  573 => 286,  568 => 284,  563 => 282,  553 => 275,  548 => 273,  543 => 271,  540 => 270,  538 => 269,  535 => 268,  522 => 260,  517 => 258,  512 => 256,  508 => 254,  497 => 236,  492 => 234,  487 => 232,  482 => 229,  480 => 228,  477 => 227,  468 => 221,  463 => 219,  458 => 217,  455 => 216,  453 => 215,  450 => 214,  441 => 208,  436 => 206,  431 => 204,  428 => 203,  426 => 202,  414 => 193,  410 => 192,  404 => 189,  400 => 188,  391 => 182,  387 => 180,  378 => 174,  373 => 172,  368 => 170,  359 => 164,  354 => 162,  349 => 160,  346 => 159,  344 => 158,  332 => 149,  328 => 148,  322 => 145,  318 => 144,  309 => 138,  295 => 127,  291 => 126,  285 => 123,  281 => 122,  275 => 119,  271 => 118,  265 => 115,  261 => 114,  255 => 111,  251 => 110,  245 => 107,  241 => 106,  232 => 100,  219 => 90,  215 => 89,  209 => 86,  205 => 85,  199 => 82,  195 => 81,  189 => 78,  185 => 77,  179 => 74,  175 => 73,  169 => 70,  165 => 69,  156 => 63,  147 => 56,  136 => 53,  133 => 52,  129 => 51,  122 => 47,  115 => 43,  104 => 34,  95 => 31,  92 => 30,  87 => 29,  78 => 26,  75 => 25,  71 => 24,  68 => 23,  59 => 20,  56 => 19,  52 => 18,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:categorylist.html.twig */
class __TwigTemplate_7698c0a61ab47e3edc5678646c19b4f5c2d82428148f2e5d8ba565ffa095ddf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:categorylist.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<script type=\"text/javascript\">
function getpath(cat){
\tvar catCounter = ";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["catcount"]) ? $context["catcount"] : null), "html", null, true);
        echo ";
var hostname = window.location.hostname;
var uricat = encodeURIComponent(cat);
var uri = \"http://\"+hostname+\"/web/app.php/search?category=\"+uricat;


  window.open(uri, \"_self\");

}
</script>
<div class=\"container\">
<div class=\"review-block row\">
<h1>All Categories</h1>
\t\t";
        // line 31
        echo "
";
        // line 32
        $context["diff"] = 4;
        // line 33
        $context["diff2"] = 4;
        // line 34
        $context["diff3"] = 3;
        // line 35
        echo "
";
        // line 36
        $context["i"] = 0;
        // line 37
        $context["classcount"] = 0;
        // line 38
        echo "\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["segments"]) ? $context["segments"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
            // line 39
            echo "
\t\t\t";
            // line 40
            if ( !twig_test_iterable($context["value"])) {
                // line 41
                echo "\t\t\t\t";
                $context["classcount"] = ((isset($context["classcount"]) ? $context["classcount"] : null) + 1);
                // line 42
                echo "\t\t\t\t";
                if (((isset($context["i"]) ? $context["i"] : null) == 0)) {
                    // line 43
                    echo "\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t";
                }
                // line 45
                echo "\t\t\t\t<h3> ";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo " </h3>
\t\t\t";
            } else {
                // line 47
                echo "\t\t\t<ul class=\"block-one";
                echo twig_escape_filter($this->env, (isset($context["classcount"]) ? $context["classcount"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                echo "\">
\t\t\t\t";
                // line 48
                if ( !twig_test_empty($context["value"])) {
                    // line 49
                    echo "\t\t\t\t\t";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($context["value"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                        // line 50
                        echo "\t\t\t\t\t\t";
                        $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                        // line 51
                        echo "\t\t\t\t\t\t\t<li><a class=\"";
                        echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                        echo "\" href=\"/web/app.php/search?category=";
                        echo twig_escape_filter($this->env, $context["cat"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["cat"], "html", null, true);
                        echo " </a></li>
\t\t\t\t\t\t";
                        // line 52
                        if (((isset($context["i"]) ? $context["i"] : null) == twig_round(((isset($context["diff"]) ? $context["diff"] : null) + 3)))) {
                            // line 53
                            echo "\t\t\t\t\t\t\t</ul></div><div class=\"col-md-4\"><ul>
\t\t\t\t\t\t";
                        } elseif ((                        // line 54
(isset($context["i"]) ? $context["i"] : null) == ((isset($context["diff2"]) ? $context["diff2"] : null) + 4))) {
                            // line 55
                            echo "\t\t\t\t\t\t\t</ul></div><div class=\"col-md-4\"><ul>
\t\t\t\t\t\t";
                        } elseif ((                        // line 56
(isset($context["i"]) ? $context["i"] : null) == ((isset($context["catcount"]) ? $context["catcount"] : null) + 1))) {
                            // line 57
                            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t\t";
                        }
                        // line 60
                        echo "\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 61
                    echo "\t\t\t\t\t";
                    if ((twig_length_filter($this->env, $context["value"]) < 2)) {
                        // line 62
                        echo "\t\t\t\t\t\t<li>&nbsp;</li>
\t\t\t\t\t\t<li>&nbsp;</li>
\t\t\t\t\t";
                    }
                    // line 65
                    echo "\t\t\t\t";
                } else {
                    // line 66
                    echo "\t\t\t\t\t";
                    $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                    // line 67
                    echo "\t\t\t\t\t<li>Coming Soon</li>
\t\t\t\t\t<li>&nbsp;</li>
\t\t\t\t\t<li>&nbsp;</li>
\t\t\t\t\t";
                    // line 70
                    if (((isset($context["i"]) ? $context["i"] : null) == twig_round(((isset($context["diff"]) ? $context["diff"] : null) + 3)))) {
                        // line 71
                        echo "\t\t\t\t\t\t</ul></div><div class=\"col-md-4\"><ul>
\t\t\t\t\t";
                    } elseif ((                    // line 72
(isset($context["i"]) ? $context["i"] : null) == ((isset($context["diff2"]) ? $context["diff2"] : null) + 4))) {
                        // line 73
                        echo "\t\t\t\t\t\t</ul></div><div class=\"col-md-4\"><ul>
\t\t\t\t\t";
                    } elseif ((                    // line 74
(isset($context["i"]) ? $context["i"] : null) == ((isset($context["catcount"]) ? $context["catcount"] : null) + 1))) {
                        // line 75
                        echo "\t\t\t\t\t\t</ul></div>
\t\t\t\t\t";
                    }
                    // line 77
                    echo "\t\t\t\t";
                }
                // line 78
                echo "\t\t\t</ul>
\t\t\t";
            }
            // line 80
            echo "
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:categorylist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 82,  193 => 80,  189 => 78,  186 => 77,  182 => 75,  180 => 74,  177 => 73,  175 => 72,  172 => 71,  170 => 70,  165 => 67,  162 => 66,  159 => 65,  154 => 62,  151 => 61,  145 => 60,  140 => 57,  138 => 56,  135 => 55,  133 => 54,  130 => 53,  128 => 52,  119 => 51,  116 => 50,  111 => 49,  109 => 48,  102 => 47,  96 => 45,  92 => 43,  89 => 42,  86 => 41,  84 => 40,  81 => 39,  76 => 38,  74 => 37,  72 => 36,  69 => 35,  67 => 34,  65 => 33,  63 => 32,  60 => 31,  44 => 10,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

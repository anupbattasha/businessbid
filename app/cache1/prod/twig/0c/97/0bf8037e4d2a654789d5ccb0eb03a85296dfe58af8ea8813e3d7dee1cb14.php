<?php

/* BBidsBBidsHomeBundle:Home:mymessages.html.twig */
class __TwigTemplate_0c970bf8037e4d2a654789d5ccb0eb03a85296dfe58af8ea8813e3d7dee1cb14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:mymessages.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
            'dashboard' => array($this, 'block_dashboard'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayBlock('dashboard', $context, $blocks);
        // line 40
        echo "
";
    }

    // line 4
    public function block_dashboard($context, array $blocks = array())
    {
        // line 5
        echo "\t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t<div class=\"page-title\"><h1>My Messages</h1></div>
\t\t\t<!--<div class=\"row text-right page-counter\"><div class=\"counter-box\"><span class=\"now-show\">1 of 20</span><span class=\"Prev btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-chevron-left\"></span></span><span class=\"next btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-chevron-right\"></span></span></div></div>-->
\t\t\t<div class=\"message-reviews\">
\t\t\t\t";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 10
            echo "    \t\t\t\t";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "\t\t\t\t
\t\t\t\t";
        // line 13
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "\t
\t\t\t\t<div class=\"message-reviews-filter admin_filter\">
\t\t\t\t\t<div class=\"col-md-5 side-clear\"><div class=\"label\">From</div>";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from", array()), 'errors');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from", array()), 'widget', array("id" => "datepicker1"));
        echo "</div>
\t\t\t\t\t<div class=\"col-md-5 side-clear\"><div class=\"label\">To</div>";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to", array()), 'errors');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to", array()), 'widget', array("id" => "datepicker2"));
        echo "</div>
\t\t\t\t\t<!--<div class=\"col-md-4 search\">";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "</div>-->
\t\t\t\t\t<div class=\"col-md-2 search-button\">";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "search", array()), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
\t\t\t\t</div>
\t\t\t\t";
        // line 20
        if (twig_test_empty((isset($context["emails"]) ? $context["emails"] : null))) {
            // line 21
            echo "\t\t\t\t\t<div class=\"message-alert alert alert-success\">You have not received any emails.</div>
\t\t\t\t";
        } else {
            // line 23
            echo "\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["emails"]) ? $context["emails"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
                // line 24
                echo "\t\t\t\t\t<div class=\"form-item\">
\t\t\t\t\t\t<div class=\"subject\">";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["email"], "fromemail", array()), "html", null, true);
                echo "</div>
\t\t\t\t\t\t<div class=\"date\">";
                // line 26
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["email"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</div>
\t\t\t\t\t\t<div id=\"menu\" class=\"example_menu\">
\t\t\t\t\t\t\t<div class=\"review\"><a class=\"collapsed\">";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["email"], "emailsubj", array()), "html", null, true);
                echo "</a><ul><li>";
                echo $this->getAttribute($context["email"], "emailmessage", array());
                echo "</li></ul></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "\t\t\t\t";
        }
        // line 33
        echo "\t\t\t\t";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t</div>
\t\t
\t";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:mymessages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 33,  130 => 32,  118 => 28,  113 => 26,  109 => 25,  106 => 24,  101 => 23,  97 => 21,  95 => 20,  88 => 18,  82 => 17,  76 => 16,  70 => 15,  65 => 13,  62 => 12,  53 => 10,  49 => 9,  43 => 5,  40 => 4,  35 => 40,  32 => 4,  29 => 3,  11 => 1,);
    }
}

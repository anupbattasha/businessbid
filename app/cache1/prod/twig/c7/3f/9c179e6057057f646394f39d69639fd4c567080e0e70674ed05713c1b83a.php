<?php

/* BBidsBBidsHomeBundle:Home:template1.html.twig */
class __TwigTemplate_c73f9c179e6057057f646394f39d69639fd4c567080e0e70674ed05713c1b83a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:template1.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "       <div class=\"leads_area\">
           <div class=\"inner_leads\">
               <div class=\"leads_inner_img\">
                   <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/atm_card.png"), "html", null, true);
        echo "\" alt=\"Our ATM CARD IMAGES\">
               </div>
               <div class=\"leads_inner_content\">
                   <h1>BUSINESSBID LEADS</h1>
                   <p>BusinessBid uses a pay-as-you-go model where you only pay for leads you are interested in. You get a detailed job requirement and pay only if you would like to contact the customer for a meeting and a quote. With the BusinessBid model you have complete freedom and choice to take up jobs that work best for you.</p>
              <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" class=\"sign_up_btn\">SIGN UP NOW</a>
               </div>
           </div>
       </div>
       <div class=\"purchase_area\">
           <div class=\"inner_purchase\">
               <h1>How do I purchase BusinessBid Leads?</h1>
               <div class=\"inner_bid_lids\">
                   <img src=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/home.jpg"), "html", null, true);
        echo " alt=\"Home Icon\">
                   <h5>Choose your</h5>
                   <h5>service categories</h5>
               </div>
               <div class=\"bid_arrows\">
                   <img src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo " alt=\"arrow\">
               </div>
               <div class=\"inner_bid_lids\">
                   <img src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/round_tir.png"), "html", null, true);
        echo " alt=\"Rounded Icon\">
                   <h5>Choose your</h5>
                   <h5>lead package</h5>
               </div>
               <div class=\"bid_arrows\">
                   <img src=";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/arrow.png"), "html", null, true);
        echo " alt=\"arrow\">
               </div>               
               <div class=\"inner_bid_lids\">
                   <img src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper.png"), "html", null, true);
        echo " alt=\"Preper Icon\">
                   <h5>Convenient</h5>
                   <h5>payment options</h5>
               </div>
           </div>
       </div>
       <div class=\"service_categories_area\">
           <div class=\"inner-service-categories\">
               <div class=\"inner-service-categories_content\">
                   <h1>01</h1>
                   <h3>Choose your service categories</h3>
                   <p>As UAE’s largest service provisioning portal, BusinessBid operates in a multitude of different industries. You are able to receive jobs in any of these service categories provided that you or your company are qualified to take on these projects. Just select your categories from a list and add them to your portfolio.</p>
               </div>
               <div class=\"inner-service-categories_img\">
                   <img src=";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/service-caregory.png"), "html", null, true);
        echo " alt=\"service categories\">
               </div>
           </div>
       </div>
       <div class=\"lead_package_area\">
           <div class=\"inner_lead_package\">
               <div class=\"lead_package_area_img\">
                   <img src=";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/packeges.png"), "html", null, true);
        echo " alt=\"Lead Package Area\">
               </div>
               <div class=\"lead_package_area_content\">
                   <h1>02</h1>
                   <h3>Choose your lead package</h3>
                   <p>The price of each lead varies depending on the industry and the number of leads purchased. Packages of 10, 25, 50 and 100 Leads can be purchased, each with incremental levels of discount.  As an example, the silver package of 25 leads comes with a 5% discount, while the Platinum package of 100 leads gets you a 15% discount.</p>                   
               </div>
           </div>
       </div>
       <div class=\"payment_categories_area\">
           <div class=\"inner-payment-categories\">
               <div class=\"inner-payment-categories_content\">
                   <h1>03</h1>
                   <h3>Convenient payment options</h3>
                   <p>Once you have chosen your service categories and lead packages, you are able to pay for them either online through credit card or Paypal, or offline with a cheque. Once your payment is received your account is topped up and you can begin receiving and accepting customer leads.</p>
               </div>
               <div class=\"inner-payment-categories_img\">
                   <img src=";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tream.png"), "html", null, true);
        echo " alt=\"payment categories\">
               </div>
           </div>
       </div>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
   ";
    }

    // line 88
    public function block_maincontent($context, array $blocks = array())
    {
        // line 89
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:template1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 89,  149 => 88,  131 => 74,  111 => 57,  101 => 50,  84 => 36,  78 => 33,  70 => 28,  64 => 25,  56 => 20,  45 => 12,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:loginreview.html.twig */
class __TwigTemplate_4da234f8a74d1acbb2bdc5100ff12d6a8f7a4a5d6cf07cdcb0a2a902d1c1a13a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:User:loginreview.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        echo " 
<script src = \"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src = \"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
\$('#vendor').css('display','none');

\$('#customertab').click(function(){
    
\t\t\$('#vendor').css('display','none');
\t\t\$('#customer').css('display','block');
\t\t\$('#vendor-help').css('display','none');
\t\t\$('#consumer-help').css('display','block');
\t\t\$(\"#register\").attr(\"href\", \"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\")

});
\$('#vendortab').click(function(){
\t\t\$('#customer').css('display','none');
\t\t\$('#vendor').css('display','block');
\t\t\$('#vendor-help').css('display','block');
\t\t\$('#consumer-help').css('display','none');
\t\t\$(\"#register\").attr(\"href\", \"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\")
\t\t
\t});


});

</script>
<div class=\"container\">

\t";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 38
            echo "
\t<div class=\"alert alert-danger\">";
            // line 39
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "
\t";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 44
            echo "
\t<div class=\"alert alert-success\">";
            // line 45
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 49
            echo "
\t<div class=\"alert alert-success\">";
            // line 50
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "\t
<div class=\"login-wrapper col-md-12\">
<div class=\"page-title\">
\t\t\t<h1>Write a Review</h1>
\t\t</div>
<div class=\"login_inner_wrapper\">
<div class=\"login-block col-md-6 review-login\">
\t\t<h5>Please login using your credentials below.</h5>
\t
\t\t";
        // line 62
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
\t
\t\t<div class=\"login-fb\">
\t\t\t<a href=\"#\" class=\"fb-but\"></a>
\t\t\t<p>We will never post anything on your wall without your permission</p>
\t\t</div>\t
\t\t<h2> <span>OR</span> </h2>
\t
\t\t<div>

\t\t\t<h4>LOGIN</h4>
\t\t\t<div> ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "</div>

\t\t\t<div> ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'widget');
        echo "</div>
\t\t</div>
\t\t<div class=\"forgot-passord-block\">
\t\t<div class=\"col-sm-6\">";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
\t\t<div class=\"col-sm-6\"><a href=\"";
        // line 80
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_forgot_password");
        echo "\" >Forgot your password?</a></div>
\t\t</div>
\t\t<div class=\"new-user col-sm-12\">Are you a new user? <a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\" id=\"register\">Register here</a></div>\t\t
\t\t<div id=\"consumer-help\">
\t\t\t<h6>Need Help?</h6>
\t\t\t<p>Call Customer Support Line: <br>1800 555 323</p>
\t\t\t<p>Get help via Live Chat</p>
\t\t</div>
\t\t<div id=\"vendor-help\" style=\"display:none\">
\t\t\t<h6>Need Help?</h6>
\t\t\t<p>Call Vendor Support Line:<br>1800 555 111</p>
\t\t\t<p>Get help via Live Chat</p>
\t\t</div>

\t\t
</div>
<div class=\"col-md-6 login-content review-block-bg\" id=\"customer\">
\t
</div>


</div>
</div>

</div>
<script>
\$(document).ready(function(){
var url = window.location.href;
var arr = url.split(\"#\");
if(arr[1]=='vendor'){\t
\t\$('#myTab a[href=\"#profile\"]').tab('show');
\t\t\$('#customer').css('display','none');
\t\t\$('#vendor').css('display','block');
\t\t\$(\"#register\").attr(\"href\", \"";
        // line 113
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\");
}
});
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:loginreview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 113,  185 => 82,  180 => 80,  176 => 79,  170 => 76,  165 => 74,  151 => 63,  147 => 62,  136 => 53,  127 => 50,  124 => 49,  119 => 48,  110 => 45,  107 => 44,  103 => 43,  100 => 42,  91 => 39,  88 => 38,  84 => 37,  71 => 27,  60 => 19,  46 => 8,  42 => 7,  37 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:vendorview.html.twig */
class __TwigTemplate_35202e00684129b50f63ea7e05dfff9f610d8c97206f3fd82f132fce581902cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:vendorview.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        // line 7
        echo "<script src = \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.timers-1.2.js"), "html", null, true);
        echo "\"></script>
<script src = \"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easing.1.3.js"), "html", null, true);
        echo "\"></script>
<script src = \"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.galleryview-3.0-dev.js"), "html", null, true);
        echo "\"></script>
<script src = \"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src = \"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<link type=\"text/css\" href = \"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.galleryview-3.0-dev.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<script>
\$(document).ready(function(){
\t\$.fn.stars = function() {
    return \$(this).each(function() {
        // Get the value
        var val = parseFloat(\$(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;

        // Create stars holder
        var \$span = \$('<span />').width(size);

        // Replace the numerical value with stars
        \$(this).html(\$span);
    });
}
\$('span.stars').stars();

\t\$(\"button[data-target^='.bs-example-modal-lg_']\").click(function(){
\tvar clss = \$(this).attr('data-target');

\t\tvar cs = clss.split(\"_\");

\t\t\t\$('#myGallery'+cs[1]).galleryView();


\t});

\$('#getcontvalue').hide();
\t\$('#getcontbutton').click(function(){
\t\$('#getcontvalue').show();
\t\$('#getcontbutton').hide();
});

\$('div[id^=\"morediv\"]').hide();
\t\$('span[id^=\"seemore\"]').click(function(){
\t\tvar idd = \$(this).attr('id');
\t\tvar arr = idd.split(\"_\");

\t\t\$('#lessdiv_'+arr[1]).hide();
\t\t\$('#morediv_'+arr[1]).show();
\t});
\t\t\$('span[id^=\"seeless\"]').click(function(){
\t\t\tvar idd = \$(this).attr('id');
\t\t\tvar arr = idd.split(\"_\");
\t\t\t\$('#morediv_'+arr[1]).hide();
\t\t\t\$('#lessdiv_'+arr[1]).show();
\t\t});



});
</script>

<div class=\"container vendor-view\">
\t<div class=\"breadcrum\">Home / Vendor Review / ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "bizname", array()), "html", null, true);
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</div>
\t\t<div class=\"row vendor-detail-block\">
\t\t\t<div class=\"col-md-3 vendor-img\">
\t\t\t\t";
        // line 71
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 72
            echo "\t\t\t\t\t<div class=\"thumbnail\">
\t\t\t\t\t\t<img src=\"http://115.124.120.36/web/logo/";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "logopath", array()), "html", null, true);
            echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "\t\t\t</div>
\t\t\t<div class=\"col-md-6 vendor-title\">
\t\t\t\t<h2>";
        // line 78
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "bizname", array()), "html", null, true);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</h2>
\t\t\t\t<div class=\"share\">
\t\t\t\t<!-- AddThis Button BEGIN -->
\t\t\t\t\t<div class=\"addthis_toolbox addthis_default_style \">
\t\t\t\t\t<a class=\"addthis_button_preferred_1\"></a>
\t\t\t\t\t<a class=\"addthis_button_preferred_2\"></a>
\t\t\t\t\t<a class=\"addthis_button_preferred_3\"></a>
\t\t\t\t\t<a class=\"addthis_button_preferred_4\"></a>
\t\t\t\t\t<a class=\"addthis_button_compact\"></a>
\t\t\t\t\t<a class=\"addthis_counter addthis_bubble_style\"></a>
\t\t\t\t\t</div>
\t\t\t\t<script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-53799bdd524f2e06\"></script>
\t\t\t\t<!-- AddThis Button END --></div>
\t\t\t</div>
\t\t\t<div class=\"col-md-3 button-block\">
\t\t\t\t\t";
        // line 94
        echo "\t\t\t\t\t<input class=\"btn btn-success btn-block\" type=\"submit\" id=\"getcontbutton\"  value=\"Get Contact Details\">
\t\t\t\t\t";
        // line 95
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 96
            echo "\t\t\t\t\t<div id=\"getcontvalue\" class=\"contactdetails\"><h2>Contact details</h2> <p><label>SMS Phone</label> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "smsphone", array()), "html", null, true);
            echo " </p><p><label>Home Phone</label> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "homephone", array()), "html", null, true);
            echo "</p>  <p><label>Address</label> :";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "address", array()), "html", null, true);
            echo "</p></div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo "\t\t\t</div>
\t\t</div>
\t\t<div class=\"row vendor-content-block\">


\t\t<ul class=\"nav nav-tabs\" id=\"myTab\">
\t\t  <li id=\"profileli\" class='active'><a href=\"#profile\" data-toggle=\"tab\">Profile</a></li>
\t\t  <li id=\"photosli\"><a href=\"#photos\" data-toggle=\"tab\">Photos</a></li>
\t\t  <li id=\"ratingreviews\"><a href=\"#rating\" data-toggle=\"tab\">Rating & Reviews</a></li>
\t\t</ul>

\t\t<div class=\"tab-content vendor-panel\">
\t\t  <div class=\"tab-pane active\" id=\"profile\">
\t\t\t\t\t";
        // line 111
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 112
            echo "\t\t\t\t\t\t\t<h1>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "bizname", array()), "html", null, true);
            echo "</h1>
\t\t\t\t\t\t\t<p>";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "description", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<h2>Services Offered</h2>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
        // line 119
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 120
            echo "\t\t\t\t\t\t\t\t<li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array(), "array"), "html", null, true);
            echo "</li>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "\t\t\t\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subs"]) ? $context["subs"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 123
            echo "\t\t\t\t\t\t\t\t<li>";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</li>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 125
        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<h2>Area Serviced</h2>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
        // line 130
        if ( !twig_test_empty((isset($context["cities"]) ? $context["cities"] : null))) {
            // line 131
            echo "\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cities"]) ? $context["cities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
                // line 132
                echo "\t\t\t\t\t\t\t\t<li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "city", array()), "html", null, true);
                echo "</li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 134
            echo "\t\t\t\t\t\t\t";
        } else {
            // line 135
            echo "\t\t\t\t\t\t\t\t<li>No service area</li>
\t\t\t\t\t\t\t";
        }
        // line 137
        echo "\t\t\t\t\t\t</ul>

\t\t\t\t\t\t<h2>Brands & Products</h2>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
        // line 141
        if ( !twig_test_empty((isset($context["products"]) ? $context["products"] : null))) {
            // line 142
            echo "\t\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["pr"]) {
                // line 143
                echo "\t\t\t\t\t\t\t\t<li> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pr"], "product", array()), "html", null, true);
                echo " </li>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pr'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 145
            echo "\t\t\t\t\t\t\t";
        } else {
            // line 146
            echo "\t\t\t\t\t\t\t\t<li>No products available</li>
\t\t\t\t\t\t\t";
        }
        // line 148
        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<h2>Certifications</h2>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
        // line 153
        if ( !twig_test_empty((isset($context["certifications"]) ? $context["certifications"] : null))) {
            // line 154
            echo "\t\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["certifications"]) ? $context["certifications"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 155
                echo "\t\t\t\t\t\t\t\t<li> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "certification", array()), "html", null, true);
                echo " </li>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 157
            echo "\t\t\t\t\t\t\t";
        } else {
            // line 158
            echo "\t\t\t\t\t\t\t\t<li>No certifications available</li>
\t\t\t\t\t\t\t";
        }
        // line 160
        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t";
        // line 174
        echo "\t\t\t\t\t";
        // line 182
        echo "\t\t\t\t\t<div class=\"col-md-4\">

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t  </div>
\t\t  <div class=\"tab-pane\" id=\"photos\">

\t\t\t";
        // line 189
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 190
            echo "\t\t\t\t<h1>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "bizname", array()), "html", null, true);
            echo "</h1>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 192
        echo "

\t\t\t <div class=\"row vendor-album\">
\t\t\t\t";
        // line 195
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["i"] => $context["album"]) {
            // line 196
            echo "\t\t\t\t\t";
            $context["albumid"] = $this->getAttribute($context["album"], "albumid", array());
            // line 197
            echo "\t                ";
            $context["defaultphotoid"] = $this->getAttribute($context["album"], "defaultphotopath", array());
            // line 198
            echo "            \t\t";
            $context["imagepath"] = (((("http://115.124.120.36/stagging/web/gallery/" . (isset($context["albumid"]) ? $context["albumid"] : null)) . "/") . (isset($context["albumid"]) ? $context["albumid"] : null)) . $this->getAttribute($context["loop"], "index", array()));
            // line 199
            echo "\t\t\t\t\t";
            $context["i"] = ($context["i"] + 1);
            // line 200
            echo "\t\t\t\t\t";
            $context["photocount"] = $this->getAttribute($context["album"], "photocount", array());
            // line 201
            echo "

\t\t\t\t<div class=\"col-md-4 album-block\">
\t\t\t\t\t<div class=\"image-block\">
\t\t\t\t\t\t<img src=\"";
            // line 205
            echo twig_escape_filter($this->env, (isset($context["imagepath"]) ? $context["imagepath"] : null), "html", null, true);
            echo "\" width=\"90px\" ></div>
\t\t\t\t\t<div class=\"content-block\">
\t\t\t\t\t\t<div><strong>";
            // line 207
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumname", array()), "html", null, true);
            echo "</strong></div>
\t\t\t\t\t\t<button class=\"photobutton\" data-target=\".bs-example-modal-lg_";
            // line 208
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" data-toggle=\"modal\">
\t\t\t\t\t\t\t";
            // line 209
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "photocount", array()), "html", null, true);
            echo " Photo(s)</button>
\t\t\t\t\t\t<div class=\"photodes\">
\t\t\t\t\t\t\t<div id=\"lessdiv_";
            // line 211
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" >";
            if ((twig_length_filter($this->env, $this->getAttribute($context["album"], "summary", array())) > 50)) {
                echo " ";
                echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["album"], "summary", array()), 0, 50), "html", null, true);
                echo "...<span id=\"seemore_";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\"> see more </span> ";
            } else {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "summary", array()), "html", null, true);
                echo " ";
            }
            echo "</div>
\t\t\t\t\t\t\t<div id=\"morediv_";
            // line 212
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">  ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "summary", array()), "html", null, true);
            echo " <span id=\"seeless_";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\"> less </span> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            // line 215
            echo "\t\t\t\t\t\t\t<div class=\"modal fade bs-example-modal-lg_";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
\t\t\t\t\t\t\t  <div class=\"modal-dialog modal-lg";
            // line 216
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t<div class=\"modal-content\" id=\"myModal";
            // line 217
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t<ul id=\"myGallery";
            // line 218
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">

\t\t\t\t\t\t\t\t\t\t";
            // line 220
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["photocount"]) ? $context["photocount"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["pic"]) {
                // line 221
                echo "\t\t\t\t\t\t\t\t\t\t<li><img src=\"http://115.124.120.36/stagging/web/gallery/";
                echo twig_escape_filter($this->env, (isset($context["albumid"]) ? $context["albumid"] : null), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, ((isset($context["albumid"]) ? $context["albumid"] : null) . $context["pic"]), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $context["pic"], "html", null, true);
                echo "\" /></li>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 223
            echo "
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 231
        echo "
\t\t\t </div>
\t\t\t</div>
\t\t  <div class=\"tab-pane\" id=\"rating\">
\t\t\t<div class=\"row vendor-review-header\">
\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t<h2>Ratings & Reviews</h2>
\t\t\t\t\t";
        // line 238
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 239
            echo "\t\t\t\t\t\t\t<h3>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "bizname", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 241
        echo "\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 getcontactdetails\">

\t\t\t\t";
        // line 244
        if (((isset($context["war"]) ? $context["war"] : null) == 3)) {
            // line 245
            echo "\t\t\t\t\t<p> You are logged in as ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["profile"]) ? $context["profile"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                echo "<h3>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "bizname", array()), "html", null, true);
                echo "</h3>\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " </p>
\t\t\t\t";
        } elseif ((        // line 246
(isset($context["war"]) ? $context["war"] : null) == 2)) {
            // line 247
            echo "\t\t\t\t\t";
            $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
            // line 248
            echo "\t\t\t\t\t";
            $context["returnurl"] = ("b_bids_b_bids_vendor+" . (isset($context["vendorid"]) ? $context["vendorid"] : null));
            // line 249
            echo "\t\t\t\t\t";
            $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
            // line 250
            echo "\t\t\t\t\t<input class=\"btn btn-success btn-block\" type=\"submit\" onclick=\"window.open('";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_login", array("returnurl" => (isset($context["returnurl"]) ? $context["returnurl"] : null))), "html", null, true);
            echo "', '_self');\" value=\"Please login to Write a Review\">
\t\t\t\t\tReal feedback from real Business Bids.
\t\t\t\t";
        } elseif ((        // line 252
(isset($context["war"]) ? $context["war"] : null) == 1)) {
            // line 253
            echo "\t\t\t\t\t";
            $context["userid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
            // line 254
            echo "\t\t\t\t\t";
            $context["returnurl"] = ((((("/post/review/" . (isset($context["userid"]) ? $context["userid"] : null)) . "/") . (isset($context["vendorid"]) ? $context["vendorid"] : null)) . "/") . (isset($context["enqid"]) ? $context["enqid"] : null));
            // line 255
            echo "\t\t\t\t\t<input class=\"btn btn-success btn-block\" type=\"submit\" onclick=\"window.open('";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_review", array("authorid" => (isset($context["userid"]) ? $context["userid"] : null), "vendorid" => (isset($context["vendorid"]) ? $context["vendorid"] : null), "enquiryid" => (isset($context["enqid"]) ? $context["enqid"] : null))), "html", null, true);
            echo "', '_self');\" value=\"Write a Review\">
\t\t\t\t\tReal feedback from real Business Bids.
\t\t\t\t";
        } elseif ((        // line 257
(isset($context["war"]) ? $context["war"] : null) == 0)) {
            // line 258
            echo "\t\t\t\t\t <p> You do not have any enquiry mapped against this vendor to write a review </p>
\t\t\t\t";
        }
        // line 260
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t";
        // line 264
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 265
            echo "\t\t\t\t<div>
\t\t\t\t\t<h4>";
            // line 266
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "author", array()), "html", null, true);
            echo "</h4>
\t\t\t\t\t<p>";
            // line 267
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "review", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t<span class=\"stars\">";
            // line 268
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "rating", array()), "html", null, true);
            echo "</span>| ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["r"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "
\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 271
        echo "\t\t\t</div>
\t\t</div>
\t\t</div>

\t\t\t<script>
\t\t\t\t\$(function(){
\t\t\t\tvar loc = window.location.href;
\t\t\t\tvar arr = loc.split('#');
\t\t\t\tif(arr[1]==\"rating\"){
\t\t\t\t\t\t\$('#myTab a[href=\"#rating\"]').tab('show');
\t\t\t\t}
\t\t\t\t})
\t\t\t</script>

\t\t</div>

</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:vendorview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  634 => 271,  623 => 268,  619 => 267,  615 => 266,  612 => 265,  608 => 264,  602 => 260,  598 => 258,  596 => 257,  590 => 255,  587 => 254,  584 => 253,  582 => 252,  576 => 250,  573 => 249,  570 => 248,  567 => 247,  565 => 246,  551 => 245,  549 => 244,  544 => 241,  535 => 239,  531 => 238,  522 => 231,  501 => 223,  488 => 221,  484 => 220,  479 => 218,  475 => 217,  471 => 216,  466 => 215,  457 => 212,  441 => 211,  436 => 209,  432 => 208,  428 => 207,  423 => 205,  417 => 201,  414 => 200,  411 => 199,  408 => 198,  405 => 197,  402 => 196,  385 => 195,  380 => 192,  371 => 190,  367 => 189,  358 => 182,  356 => 174,  350 => 160,  346 => 158,  343 => 157,  334 => 155,  329 => 154,  327 => 153,  320 => 148,  316 => 146,  313 => 145,  304 => 143,  299 => 142,  297 => 141,  291 => 137,  287 => 135,  284 => 134,  275 => 132,  270 => 131,  268 => 130,  261 => 125,  252 => 123,  247 => 122,  238 => 120,  234 => 119,  228 => 115,  220 => 113,  215 => 112,  211 => 111,  196 => 98,  183 => 96,  179 => 95,  176 => 94,  151 => 78,  147 => 76,  138 => 73,  135 => 72,  131 => 71,  117 => 68,  58 => 12,  54 => 11,  50 => 10,  46 => 9,  42 => 8,  37 => 7,  34 => 6,  29 => 3,  11 => 1,);
    }
}

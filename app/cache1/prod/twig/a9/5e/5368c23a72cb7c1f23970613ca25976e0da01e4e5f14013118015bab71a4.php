<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Customer:enquiry_confirmation_email.html.twig */
class __TwigTemplate_a95e5368c23a72cb7c1f23970613ca25976e0da01e4e5f14013118015bab71a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; margin-top:0px; width:100%\">Thank you for submitting your request. Your job request number is ";
        echo twig_escape_filter($this->env, (isset($context["enquiryid"]) ? $context["enquiryid"] : null), "html", null, true);
        echo "</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; margin-top:0px; width:100%\">
\tWhat's next?
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; margin-top:0px; width:100%\">
\tPlease ensure that you have your phone handy as vendors will soon start contacting you to discuss your project requirements and pricing.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; margin-top:0px; width:100%\">
\tAfter you vendor engagement, please remember to write a review on your experience. This will help us to serve you better.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; margin-top:0px; width:100%\">
\tAnd of course, if you have any questions or need help call us on 04 42 13 777.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; margin-top:0px; width:100%\">
\tGood Luck with your project!
</p>   
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">Regards<br>BusinessBid Team</p>
 
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Customer:enquiry_confirmation_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}

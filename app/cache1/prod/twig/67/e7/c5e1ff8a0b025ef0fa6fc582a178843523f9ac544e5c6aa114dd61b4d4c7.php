<?php

/* BBidsBBidsHomeBundle:Home:freetrailemailvendor.html.twig */
class __TwigTemplate_67e7c5e1ff8a0b025ef0fa6fc582a178843523f9ac544e5c6aa114dd61b4d4c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table>
  <tr>
    <td colspan=\"2\" align=\"center\" valign=\"middle\">
    <table>
<p class=\"MsoNormal\">
<span style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left;\">
";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 8
            echo "<b>Dear ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "contactname", array()), "html", null, true);
            echo ",</b>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "<br>
<br>
</span>
</p>
<p class=\"MsoNormal\">
<span style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left\">
Thank you for choosing the Free Trial feature from Businessbids Networks
<span class=\"im\">
Once this feature has been activated you will be notified by our team and then you will start receiving new business leads.
<br>
<br>
</span>
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left\"><strong>If you have any questions please feel free to contact our support center on <a style=\"color: #0099FF;\"> (04) 42 13 777</a>.</strong></P>
<br>
<br>
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left\">Regards
<br>
BusinessBid</p>
<br>
<br>
</span>
<b>
<u>
<span style=\"font-size:11.0pt;font-family:\"Calibri\",\"sans-serif\";color:#1f497d\">
<u></u>
<u></u>
</span>
</u>
</b>
</p>
    </table>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:freetrailemailvendor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 10,  31 => 8,  27 => 7,  19 => 1,);
    }
}

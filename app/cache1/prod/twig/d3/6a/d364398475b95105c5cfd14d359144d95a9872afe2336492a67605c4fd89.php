<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'toprightmenu' => array($this, 'block_toprightmenu'),
            'search' => array($this, 'block_search'),
            'topleftmenu' => array($this, 'block_topleftmenu'),
            'megamenu' => array($this, 'block_megamenu'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<head>
";
        // line 3
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 4
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 5
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 6
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 7
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 8
        echo "
";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 10
            echo "\t<title>Main Home Page</title>
";
        } elseif ((is_string($__internal_3bd4b30c37e923c6ba0557822148903c77ca745339fe397fc339fea55cbdc8ec = $this->getAttribute($this->getAttribute(        // line 11
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_fd357441b75a4d36091726e9d68780876822e82f7e14618db7db4d97bfb4345a = "http://www.businessbid.ae/contactus") && ('' === $__internal_fd357441b75a4d36091726e9d68780876822e82f7e14618db7db4d97bfb4345a || 0 === strpos($__internal_3bd4b30c37e923c6ba0557822148903c77ca745339fe397fc339fea55cbdc8ec, $__internal_fd357441b75a4d36091726e9d68780876822e82f7e14618db7db4d97bfb4345a)))) {
            // line 12
            echo "\t<title>Contact Us Form </title>
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 13
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 14
            echo "\t<title>Account Login</title>
";
        } elseif ((is_string($__internal_c2c98d5c3058d045b626af5949a6a087c87bff02a9213f890b267239cd1da64c = $this->getAttribute($this->getAttribute(        // line 15
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5e9a11add5212ce8098af7c43f4dfae3121b68e65c3d2ef32a60dd538d47c891 = "http://www.businessbid.ae/node3") && ('' === $__internal_5e9a11add5212ce8098af7c43f4dfae3121b68e65c3d2ef32a60dd538d47c891 || 0 === strpos($__internal_c2c98d5c3058d045b626af5949a6a087c87bff02a9213f890b267239cd1da64c, $__internal_5e9a11add5212ce8098af7c43f4dfae3121b68e65c3d2ef32a60dd538d47c891)))) {
            // line 16
            echo "\t<title>How BusinessBid Works for Customers</title>
";
        } elseif ((is_string($__internal_72b624da7743347e2b997b2a9a3234cc43bd7a174f6dac590668dfc57a031b3c = $this->getAttribute($this->getAttribute(        // line 17
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a6817a8aee3984e45eca4de4e14e79f4b41a7b1d58111517752370df2326aa60 = "http://www.businessbid.ae/review/login") && ('' === $__internal_a6817a8aee3984e45eca4de4e14e79f4b41a7b1d58111517752370df2326aa60 || 0 === strpos($__internal_72b624da7743347e2b997b2a9a3234cc43bd7a174f6dac590668dfc57a031b3c, $__internal_a6817a8aee3984e45eca4de4e14e79f4b41a7b1d58111517752370df2326aa60)))) {
            // line 18
            echo "\t<title>Write A Review Login</title>
";
        } elseif ((is_string($__internal_e7cca5011bcf4f09123624bb3b6645c1ff35d91af23b1f1ec3c4736fd22539c7 = $this->getAttribute($this->getAttribute(        // line 19
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_611e26ba17bbfa670e512b55cfab7e70cb595bed4fb387177f6425c71d544cd4 = "http://www.businessbid.ae/node1") && ('' === $__internal_611e26ba17bbfa670e512b55cfab7e70cb595bed4fb387177f6425c71d544cd4 || 0 === strpos($__internal_e7cca5011bcf4f09123624bb3b6645c1ff35d91af23b1f1ec3c4736fd22539c7, $__internal_611e26ba17bbfa670e512b55cfab7e70cb595bed4fb387177f6425c71d544cd4)))) {
            // line 20
            echo "\t<title>Are you A Vendor</title>
";
        } elseif ((is_string($__internal_898ffae7fc7dee5367073a9adc704ee1a5a81423663b4d4c76d9afb4c27360ce = $this->getAttribute($this->getAttribute(        // line 21
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_899539917c8c0b6eddc48bebfcf7ada8c04f511ca3a2ace8af56749f197c6500 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_899539917c8c0b6eddc48bebfcf7ada8c04f511ca3a2ace8af56749f197c6500 || 0 === strpos($__internal_898ffae7fc7dee5367073a9adc704ee1a5a81423663b4d4c76d9afb4c27360ce, $__internal_899539917c8c0b6eddc48bebfcf7ada8c04f511ca3a2ace8af56749f197c6500)))) {
            // line 22
            echo "\t<title>Accounting & Auditing Quote Form</title>
";
        } elseif ((is_string($__internal_c35db33228d4fa43a89f01d85074c4944d239192e7850d2645fa07f4b9261d9b = $this->getAttribute($this->getAttribute(        // line 23
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_9d62e2ba54a3382f7e7bfdfa12164674c8950c84aac304b9bdceddbab40c203a = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_9d62e2ba54a3382f7e7bfdfa12164674c8950c84aac304b9bdceddbab40c203a || 0 === strpos($__internal_c35db33228d4fa43a89f01d85074c4944d239192e7850d2645fa07f4b9261d9b, $__internal_9d62e2ba54a3382f7e7bfdfa12164674c8950c84aac304b9bdceddbab40c203a)))) {
            // line 24
            echo "\t<title>Get Signage & Signboard Quotes</title>
";
        } elseif ((is_string($__internal_d4c240d32f62ed98da19508d0455c4f34c5c9bc15aad6eb7128331ae6f1d08d3 = $this->getAttribute($this->getAttribute(        // line 25
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_01ad35c0d37819715c2ac49d9328c00cb98b125404f79ad67538a4114c893337 = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_01ad35c0d37819715c2ac49d9328c00cb98b125404f79ad67538a4114c893337 || 0 === strpos($__internal_d4c240d32f62ed98da19508d0455c4f34c5c9bc15aad6eb7128331ae6f1d08d3, $__internal_01ad35c0d37819715c2ac49d9328c00cb98b125404f79ad67538a4114c893337)))) {
            // line 26
            echo "\t<title>Obtain IT Support Quotes</title>
";
        } elseif ((is_string($__internal_75a3fc87307f34496e826889f16af348ddc2f90d48ba9c6c3410054053f8ef97 = $this->getAttribute($this->getAttribute(        // line 27
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2de94318f5278e2a8d7404f17267bd5d0bf913bda27a910cc70907ee414efa13 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_2de94318f5278e2a8d7404f17267bd5d0bf913bda27a910cc70907ee414efa13 || 0 === strpos($__internal_75a3fc87307f34496e826889f16af348ddc2f90d48ba9c6c3410054053f8ef97, $__internal_2de94318f5278e2a8d7404f17267bd5d0bf913bda27a910cc70907ee414efa13)))) {
            // line 28
            echo "\t<title>Easy Way to get Photography & Videos Quotes </title>
";
        } elseif ((is_string($__internal_d9c0e6e20364bf55c81b40f7fcaa5be811867cd7fa135e1df61f687c6cf092b2 = $this->getAttribute($this->getAttribute(        // line 29
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_be0ad98181a21fa2236c245062b66f1de379dce8bc06db1a89345fc7d1f57ec4 = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_be0ad98181a21fa2236c245062b66f1de379dce8bc06db1a89345fc7d1f57ec4 || 0 === strpos($__internal_d9c0e6e20364bf55c81b40f7fcaa5be811867cd7fa135e1df61f687c6cf092b2, $__internal_be0ad98181a21fa2236c245062b66f1de379dce8bc06db1a89345fc7d1f57ec4)))) {
            // line 30
            echo "\t<title>Procure Residential Cleaning Quotes Right Here</title>
";
        } elseif ((is_string($__internal_f5a5146a66173f720eadd7e2cdfc8ac68dc2c7ecd622d1358d107417456bb122 = $this->getAttribute($this->getAttribute(        // line 31
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_96ca48564989e263c4ddfb3579953b004ab05cb75d6d7a303f4fd3d028f48c43 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_96ca48564989e263c4ddfb3579953b004ab05cb75d6d7a303f4fd3d028f48c43 || 0 === strpos($__internal_f5a5146a66173f720eadd7e2cdfc8ac68dc2c7ecd622d1358d107417456bb122, $__internal_96ca48564989e263c4ddfb3579953b004ab05cb75d6d7a303f4fd3d028f48c43)))) {
            // line 32
            echo "\t<title>Receive Maintenance Quotes for Free</title>
";
        } elseif ((is_string($__internal_8b7c098d80a949eb713b7e2e6a62c8a9243aab315c1ba8fd1383262bd1166d8f = $this->getAttribute($this->getAttribute(        // line 33
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3b69a897cbedd9066390a404f74153e17f2621d10a35bd2066b3fee0370739a5 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_3b69a897cbedd9066390a404f74153e17f2621d10a35bd2066b3fee0370739a5 || 0 === strpos($__internal_8b7c098d80a949eb713b7e2e6a62c8a9243aab315c1ba8fd1383262bd1166d8f, $__internal_3b69a897cbedd9066390a404f74153e17f2621d10a35bd2066b3fee0370739a5)))) {
            // line 34
            echo "\t<title>Acquire Interior Design & FitOut Quotes </title>
";
        } elseif ((is_string($__internal_f11dc24d8502770bf32c65506f81618cf3c58c32366b50222b43d9583f2f6b02 = $this->getAttribute($this->getAttribute(        // line 35
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a7379dbd0e62938b02bde9857dc0b16a714b536d7cf4099f99823be52b7e1c5e = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_a7379dbd0e62938b02bde9857dc0b16a714b536d7cf4099f99823be52b7e1c5e || 0 === strpos($__internal_f11dc24d8502770bf32c65506f81618cf3c58c32366b50222b43d9583f2f6b02, $__internal_a7379dbd0e62938b02bde9857dc0b16a714b536d7cf4099f99823be52b7e1c5e)))) {
            // line 36
            echo "\t<title>Get Hold of Free Catering Quotes</title>
";
        } elseif ((is_string($__internal_44f3a75ab763ef56e6ac30cafc5ac92c328a457b5112929118c6040616d4b36d = $this->getAttribute($this->getAttribute(        // line 37
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d5bbeeddfb90d6387f4eb3f5a8fc5c80eeabea88cbf77003434b272301b2cdba = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_d5bbeeddfb90d6387f4eb3f5a8fc5c80eeabea88cbf77003434b272301b2cdba || 0 === strpos($__internal_44f3a75ab763ef56e6ac30cafc5ac92c328a457b5112929118c6040616d4b36d, $__internal_d5bbeeddfb90d6387f4eb3f5a8fc5c80eeabea88cbf77003434b272301b2cdba)))) {
            // line 38
            echo "\t<title>Find Free Landscaping & Gardens Quotes </title>
";
        } elseif ((is_string($__internal_ff425c96bb0b6f4161746d208c34f7bae536dc65c8b6ce1c7521965ba1e0e8a2 = $this->getAttribute($this->getAttribute(        // line 39
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3008c515c5c73118c679a6c1d36610f632c03d921ba55a9177b607e0f35099cd = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_3008c515c5c73118c679a6c1d36610f632c03d921ba55a9177b607e0f35099cd || 0 === strpos($__internal_ff425c96bb0b6f4161746d208c34f7bae536dc65c8b6ce1c7521965ba1e0e8a2, $__internal_3008c515c5c73118c679a6c1d36610f632c03d921ba55a9177b607e0f35099cd)))) {
            // line 40
            echo "\t<title>Attain Free Offset Printing Quotes from Companies</title>
";
        } elseif ((is_string($__internal_d8f72847f7eacf5cfa3b08375d449d391d9d63efe16793823d46a38c20ef1f92 = $this->getAttribute($this->getAttribute(        // line 41
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c117e31a8e4fc5e707a40d37181b4aba1c07b20d57767fe9af07baf80b3aea9e = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_c117e31a8e4fc5e707a40d37181b4aba1c07b20d57767fe9af07baf80b3aea9e || 0 === strpos($__internal_d8f72847f7eacf5cfa3b08375d449d391d9d63efe16793823d46a38c20ef1f92, $__internal_c117e31a8e4fc5e707a40d37181b4aba1c07b20d57767fe9af07baf80b3aea9e)))) {
            // line 42
            echo "\t<title>Get Free Commercial Cleaning Quotes Now</title>
";
        } elseif ((is_string($__internal_06758599941cc4e26d60b8a45c0d8966d122d790d6593cae27a290bda95b5dd1 = $this->getAttribute($this->getAttribute(        // line 43
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b5f134d492c149963af56ed5fc0b68d05b45f22bcb743d740fa106e2c6de1705 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_b5f134d492c149963af56ed5fc0b68d05b45f22bcb743d740fa106e2c6de1705 || 0 === strpos($__internal_06758599941cc4e26d60b8a45c0d8966d122d790d6593cae27a290bda95b5dd1, $__internal_b5f134d492c149963af56ed5fc0b68d05b45f22bcb743d740fa106e2c6de1705)))) {
            // line 44
            echo "\t<title>Procure Free Pest Control Quotes Easily</title>
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 45
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 46
            echo "\t<title>Vendor Reviews Directory Home Page</title>
";
        } elseif ((is_string($__internal_1cf9d0dcd96a3ba0ee2a10f36cb565be096ccdd2f8d941b85dc590364706d360 = $this->getAttribute($this->getAttribute(        // line 47
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_603a6cf10931f1cab643756e2210553887fc0c066bb67b26dbb05d94b9829f31 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_603a6cf10931f1cab643756e2210553887fc0c066bb67b26dbb05d94b9829f31 || 0 === strpos($__internal_1cf9d0dcd96a3ba0ee2a10f36cb565be096ccdd2f8d941b85dc590364706d360, $__internal_603a6cf10931f1cab643756e2210553887fc0c066bb67b26dbb05d94b9829f31)))) {
            // line 48
            echo "\t<title>Accounting & Auditing Reviews Directory Page</title>
";
        } elseif ((is_string($__internal_9419aad72db09679718e023ae4e3dbe3a8e22338005bc3d4d33bc2420cbf25a8 = $this->getAttribute($this->getAttribute(        // line 49
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_98b790cc6b6be617f3aad1c9d2c3c1a32df755c6ec1bdff599ec49ac27211269 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_98b790cc6b6be617f3aad1c9d2c3c1a32df755c6ec1bdff599ec49ac27211269 || 0 === strpos($__internal_9419aad72db09679718e023ae4e3dbe3a8e22338005bc3d4d33bc2420cbf25a8, $__internal_98b790cc6b6be617f3aad1c9d2c3c1a32df755c6ec1bdff599ec49ac27211269)))) {
            // line 50
            echo "\t<title>Find Signage & Signboard Reviews </title>
";
        } elseif ((is_string($__internal_c8958b7081c3089aa5e395e00a148a56d33de008831b1549901fdcd4b269ff92 = $this->getAttribute($this->getAttribute(        // line 51
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_26466604feb1126adb5c5577fba0812418697eb923e43a830b58134d68e9aefd = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_26466604feb1126adb5c5577fba0812418697eb923e43a830b58134d68e9aefd || 0 === strpos($__internal_c8958b7081c3089aa5e395e00a148a56d33de008831b1549901fdcd4b269ff92, $__internal_26466604feb1126adb5c5577fba0812418697eb923e43a830b58134d68e9aefd)))) {
            // line 52
            echo "\t<title>Have a Look at IT Support Reviews Directory</title>
";
        } elseif ((is_string($__internal_af11a0a63e4705708863abe84812369eb3927988371fd00257f514ba7ffa0597 = $this->getAttribute($this->getAttribute(        // line 53
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_a144df073a27716fae2f7dc030277d26ffb3da59f20955df85ace5352fa75dac = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_a144df073a27716fae2f7dc030277d26ffb3da59f20955df85ace5352fa75dac || 0 === strpos($__internal_af11a0a63e4705708863abe84812369eb3927988371fd00257f514ba7ffa0597, $__internal_a144df073a27716fae2f7dc030277d26ffb3da59f20955df85ace5352fa75dac)))) {
            // line 54
            echo "\t<title>Check Out Photography & Videos Reviews Here</title>
";
        } elseif ((is_string($__internal_2f17741a7f86ec9614a281abdf43a6e68f16c1735acdd4505f8774d864692440 = $this->getAttribute($this->getAttribute(        // line 55
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f1f763d3ef88b95e366ba771c7948e3f95697fbd1060eb074626c0e7fca12a7d = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_f1f763d3ef88b95e366ba771c7948e3f95697fbd1060eb074626c0e7fca12a7d || 0 === strpos($__internal_2f17741a7f86ec9614a281abdf43a6e68f16c1735acdd4505f8774d864692440, $__internal_f1f763d3ef88b95e366ba771c7948e3f95697fbd1060eb074626c0e7fca12a7d)))) {
            // line 56
            echo "\t<title>View Residential Cleaning Reviews From Here</title>
";
        } elseif ((is_string($__internal_57e036d410e4131426173fa1e0c814db4c47ffe4c6818006e8d95abf5f162e4e = $this->getAttribute($this->getAttribute(        // line 57
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_29e07f3afd083c3b5a82c71ab7d1e30cd14432c118a1bc6e88de923855a62ac0 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_29e07f3afd083c3b5a82c71ab7d1e30cd14432c118a1bc6e88de923855a62ac0 || 0 === strpos($__internal_57e036d410e4131426173fa1e0c814db4c47ffe4c6818006e8d95abf5f162e4e, $__internal_29e07f3afd083c3b5a82c71ab7d1e30cd14432c118a1bc6e88de923855a62ac0)))) {
            // line 58
            echo "\t<title>Find Genuine Maintenance Reviews</title>
";
        } elseif ((is_string($__internal_aeb149d9a92cc90077f13630d79afeb2adf394045590cb8bed6f47d2c67af14c = $this->getAttribute($this->getAttribute(        // line 59
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b1db901b542c12634c1e4554e9361e1ea834e8bf501770cbddbe81029db94f95 = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_b1db901b542c12634c1e4554e9361e1ea834e8bf501770cbddbe81029db94f95 || 0 === strpos($__internal_aeb149d9a92cc90077f13630d79afeb2adf394045590cb8bed6f47d2c67af14c, $__internal_b1db901b542c12634c1e4554e9361e1ea834e8bf501770cbddbe81029db94f95)))) {
            // line 60
            echo "\t<title>Locate Interior Design & FitOut Reviews</title>
";
        } elseif ((is_string($__internal_6730b6a4de825f72af47b9a42e637bf820e1a319e05068cb7dbbb32d3bf37a16 = $this->getAttribute($this->getAttribute(        // line 61
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3914e6d05e661a9b77c35b914b3e582bac2789812e4cc0fdf6728986da87972e = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_3914e6d05e661a9b77c35b914b3e582bac2789812e4cc0fdf6728986da87972e || 0 === strpos($__internal_6730b6a4de825f72af47b9a42e637bf820e1a319e05068cb7dbbb32d3bf37a16, $__internal_3914e6d05e661a9b77c35b914b3e582bac2789812e4cc0fdf6728986da87972e)))) {
            // line 62
            echo "\t<title>See All Catering Reviews Logged</title>
";
        } elseif ((is_string($__internal_77d8534f5c235ec31acbe56b92c3d7522a2e1bc9ec3da3cded76d74c45275c8f = $this->getAttribute($this->getAttribute(        // line 63
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5fb55d827958a488db1372eb13a72c2e870b6a7fd9fdc4d042f29578e020341a = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_5fb55d827958a488db1372eb13a72c2e870b6a7fd9fdc4d042f29578e020341a || 0 === strpos($__internal_77d8534f5c235ec31acbe56b92c3d7522a2e1bc9ec3da3cded76d74c45275c8f, $__internal_5fb55d827958a488db1372eb13a72c2e870b6a7fd9fdc4d042f29578e020341a)))) {
            // line 64
            echo "\t<title>Listings of all Landscaping & Gardens Reviews </title>
";
        } elseif ((is_string($__internal_baa1dd1ac31cd13f947593a8f1d685732cc1cbf1313159182a22ffabe327ae51 = $this->getAttribute($this->getAttribute(        // line 65
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_7af7745e8e1e17f5de48f09c5e4e89844b1584411583d0d08d2469586617d2ab = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_7af7745e8e1e17f5de48f09c5e4e89844b1584411583d0d08d2469586617d2ab || 0 === strpos($__internal_baa1dd1ac31cd13f947593a8f1d685732cc1cbf1313159182a22ffabe327ae51, $__internal_7af7745e8e1e17f5de48f09c5e4e89844b1584411583d0d08d2469586617d2ab)))) {
            // line 66
            echo "\t<title>Get Access to Offset Printing Reviews</title>
";
        } elseif ((is_string($__internal_da1b6baf489c9b7c244ffd9e208f63f3e97b5aa3a64b4a6da00c1a62ad1bc031 = $this->getAttribute($this->getAttribute(        // line 67
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b1fdb6f9f19054699efb45f6ebcdff3ad19370d1b7e75d4ff064e58595032d9d = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_b1fdb6f9f19054699efb45f6ebcdff3ad19370d1b7e75d4ff064e58595032d9d || 0 === strpos($__internal_da1b6baf489c9b7c244ffd9e208f63f3e97b5aa3a64b4a6da00c1a62ad1bc031, $__internal_b1fdb6f9f19054699efb45f6ebcdff3ad19370d1b7e75d4ff064e58595032d9d)))) {
            // line 68
            echo "\t<title>Have a Look at various Commercial Cleaning Reviews</title>
";
        } elseif ((is_string($__internal_2c2b827820c893b10fb30a659214d67bd888f7051e92f80c9327c23db7a03c29 = $this->getAttribute($this->getAttribute(        // line 69
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c8fdb6d7f087e6f14b7ad2d5cf6e9b69378c0d69011c68eb2985d7ea6dd4171b = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_c8fdb6d7f087e6f14b7ad2d5cf6e9b69378c0d69011c68eb2985d7ea6dd4171b || 0 === strpos($__internal_2c2b827820c893b10fb30a659214d67bd888f7051e92f80c9327c23db7a03c29, $__internal_c8fdb6d7f087e6f14b7ad2d5cf6e9b69378c0d69011c68eb2985d7ea6dd4171b)))) {
            // line 70
            echo "\t<title>Read the Pest Control Reviews Listed Here</title>
";
        } elseif ((        // line 71
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 72
            echo "\t<title>Find information About BusinessBid</title>
";
        } elseif ((        // line 73
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 74
            echo "\t<title>Let us Introduce the BusinessBid Team</title>
";
        } elseif ((        // line 75
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 76
            echo "\t<title>Have a Look at BusinessBid Career Opportunities</title>
";
        } elseif ((        // line 77
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 78
            echo "\t<title>Want to become BusinessBid Valued Partners</title>
";
        } elseif ((        // line 79
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 80
            echo "\t<title>Vendor Account Login Access Page</title>
";
        } elseif ((is_string($__internal_0644fb74415414a941f5f14a3c8bfbf6b8435d35d5e5f0b3822ce09f22b333ff = $this->getAttribute($this->getAttribute(        // line 81
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_cdeef4d6743a5f4a309cf7d6bcfbcf68e6f3a84b47f701c32711790724dad243 = "http://www.businessbid.ae/node2") && ('' === $__internal_cdeef4d6743a5f4a309cf7d6bcfbcf68e6f3a84b47f701c32711790724dad243 || 0 === strpos($__internal_0644fb74415414a941f5f14a3c8bfbf6b8435d35d5e5f0b3822ce09f22b333ff, $__internal_cdeef4d6743a5f4a309cf7d6bcfbcf68e6f3a84b47f701c32711790724dad243)))) {
            // line 82
            echo "\t<title>How BusinessBid Works for Vendors</title>
";
        } elseif ((is_string($__internal_a77c0cdd01b15e16539635ae65a7b1790e6d36bd638d37f1e3934f726d371f3b = $this->getAttribute($this->getAttribute(        // line 83
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_218f1d98761978c680c027703921afa9809c135f83418944cbac104574683489 = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_218f1d98761978c680c027703921afa9809c135f83418944cbac104574683489 || 0 === strpos($__internal_a77c0cdd01b15e16539635ae65a7b1790e6d36bd638d37f1e3934f726d371f3b, $__internal_218f1d98761978c680c027703921afa9809c135f83418944cbac104574683489)))) {
            // line 84
            echo "\t<title>BusinessBid Vendor Registration Page</title>
";
        } elseif ((is_string($__internal_7370ee805941bc60b423e61076342ef61b5dea52b7c29cc1b61f729f93e93d77 = $this->getAttribute($this->getAttribute(        // line 85
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_11b62a53aaaad7c7378f0c62b8255b5c57a165518a2d8c40564f56e2ce7f3328 = "http://www.businessbid.ae/faq") && ('' === $__internal_11b62a53aaaad7c7378f0c62b8255b5c57a165518a2d8c40564f56e2ce7f3328 || 0 === strpos($__internal_7370ee805941bc60b423e61076342ef61b5dea52b7c29cc1b61f729f93e93d77, $__internal_11b62a53aaaad7c7378f0c62b8255b5c57a165518a2d8c40564f56e2ce7f3328)))) {
            // line 86
            echo "\t<title>Find answers to common Vendor FAQ’s</title>
";
        } elseif ((is_string($__internal_f6cfa5772f66a302e5331f9a4f94efbbd6db225fbf0c1e7919159011004bbfaf = $this->getAttribute($this->getAttribute(        // line 87
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_91981e19a2d3026fdb7dd3fb4f23d79e0e2ecd3083393b7244f3eea89de79860 = "http://www.businessbid.ae/node4") && ('' === $__internal_91981e19a2d3026fdb7dd3fb4f23d79e0e2ecd3083393b7244f3eea89de79860 || 0 === strpos($__internal_f6cfa5772f66a302e5331f9a4f94efbbd6db225fbf0c1e7919159011004bbfaf, $__internal_91981e19a2d3026fdb7dd3fb4f23d79e0e2ecd3083393b7244f3eea89de79860)))) {
            // line 88
            echo "\t<title>BusinessBid Vendor Reviews Directory Home</title>
";
        } elseif ((is_string($__internal_8a455f271e9e791c1f9ede8afc279f02126ae1493ffcf6f6a4ed30f708541854 = ($this->getAttribute($this->getAttribute(        // line 89
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_eea741996e947189e9b9e39a9abbb33753179579181eaaa340a4d42f1f150677 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_eea741996e947189e9b9e39a9abbb33753179579181eaaa340a4d42f1f150677 || 0 === strpos($__internal_8a455f271e9e791c1f9ede8afc279f02126ae1493ffcf6f6a4ed30f708541854, $__internal_eea741996e947189e9b9e39a9abbb33753179579181eaaa340a4d42f1f150677)))) {
            // line 90
            echo "\t<title>Customer Account Login and Dashboard Access</title>
";
        } elseif ((is_string($__internal_b91cb9aebba7869aa456bf3610aa56a8ded57795c98b27cf587e0d3c2bffb1af = $this->getAttribute($this->getAttribute(        // line 91
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8235c56787bb32d9f032031fd193c393dfa691510eb37320cee342de8a079053 = "http://www.businessbid.ae/node6") && ('' === $__internal_8235c56787bb32d9f032031fd193c393dfa691510eb37320cee342de8a079053 || 0 === strpos($__internal_b91cb9aebba7869aa456bf3610aa56a8ded57795c98b27cf587e0d3c2bffb1af, $__internal_8235c56787bb32d9f032031fd193c393dfa691510eb37320cee342de8a079053)))) {
            // line 92
            echo "\t<title>Find helpful answers to common Customer FAQ’s</title>
";
        } elseif ((is_string($__internal_ce0378bd46a59aaabe2d0865562d876f9d45c02ca8ff9a703cad9c31dfc3d5e7 = $this->getAttribute($this->getAttribute(        // line 93
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8983a3505b87fa76edef44715f557547e7ac0509b82594637123c3e1b4023b01 = "http://www.businessbid.ae/feedback") && ('' === $__internal_8983a3505b87fa76edef44715f557547e7ac0509b82594637123c3e1b4023b01 || 0 === strpos($__internal_ce0378bd46a59aaabe2d0865562d876f9d45c02ca8ff9a703cad9c31dfc3d5e7, $__internal_8983a3505b87fa76edef44715f557547e7ac0509b82594637123c3e1b4023b01)))) {
            // line 94
            echo "\t<title>Give us Your Feedback</title>
";
        } elseif ((is_string($__internal_e8790845cb3cde3b0a5e3c8e3b5cec65fae20a6d11bc595b357833157fe5f24a = $this->getAttribute($this->getAttribute(        // line 95
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_284314d17581ed7c5115d52a1dcab67dbb36fb62e002569a051f28e08da9d88b = "http://www.businessbid.ae/sitemap") && ('' === $__internal_284314d17581ed7c5115d52a1dcab67dbb36fb62e002569a051f28e08da9d88b || 0 === strpos($__internal_e8790845cb3cde3b0a5e3c8e3b5cec65fae20a6d11bc595b357833157fe5f24a, $__internal_284314d17581ed7c5115d52a1dcab67dbb36fb62e002569a051f28e08da9d88b)))) {
            // line 96
            echo "\t<title>Site Map Page</title>
";
        } elseif ((is_string($__internal_bbbc8b6bcac11402f1787286a651dc2bb4221ca163417178cb5609d7cce393d8 = $this->getAttribute($this->getAttribute(        // line 97
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2b805b713e6edcf3e154adab9c7b11359d95410d45ee82a2ca990e81c5f82886 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_2b805b713e6edcf3e154adab9c7b11359d95410d45ee82a2ca990e81c5f82886 || 0 === strpos($__internal_bbbc8b6bcac11402f1787286a651dc2bb4221ca163417178cb5609d7cce393d8, $__internal_2b805b713e6edcf3e154adab9c7b11359d95410d45ee82a2ca990e81c5f82886)))) {
            // line 98
            echo "\t<title>Did you Forget Forgot Your Password</title>
";
        } elseif ((is_string($__internal_609cc6285caea16497299b1655ab6cfd27de91778288fa900476c8006b09e8d8 = $this->getAttribute($this->getAttribute(        // line 99
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d04a8aa418af3e01618f121c7d551a21e2c3652a9a02b851ab49f8f24af1a8c0 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_d04a8aa418af3e01618f121c7d551a21e2c3652a9a02b851ab49f8f24af1a8c0 || 0 === strpos($__internal_609cc6285caea16497299b1655ab6cfd27de91778288fa900476c8006b09e8d8, $__internal_d04a8aa418af3e01618f121c7d551a21e2c3652a9a02b851ab49f8f24af1a8c0)))) {
            // line 100
            echo "\t<title>Do You Wish to Reset Your Password</title>
";
        } elseif ((is_string($__internal_55c9ca80a9439320668d4f7e1d9d74dcc80aa6f745fe4df546147a916b18fee0 = $this->getAttribute($this->getAttribute(        // line 101
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1ebc6113d54b814db620a783a351e55435f2ceea9b15836107314469a61cd8a0 = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_1ebc6113d54b814db620a783a351e55435f2ceea9b15836107314469a61cd8a0 || 0 === strpos($__internal_55c9ca80a9439320668d4f7e1d9d74dcc80aa6f745fe4df546147a916b18fee0, $__internal_1ebc6113d54b814db620a783a351e55435f2ceea9b15836107314469a61cd8a0)))) {
            // line 102
            echo "\t<title>All you wanted to Know About Us</title>
";
        } elseif ((is_string($__internal_d32638b27fe2721c3d8a6d56fd8038975cabb148abf6c1ea026ed21b5745f743 = $this->getAttribute($this->getAttribute(        // line 103
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_0588afb5b79e5db5383ee3bfebfbaca4903e734082f2165eaa114fb63a6e8e6e = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_0588afb5b79e5db5383ee3bfebfbaca4903e734082f2165eaa114fb63a6e8e6e || 0 === strpos($__internal_d32638b27fe2721c3d8a6d56fd8038975cabb148abf6c1ea026ed21b5745f743, $__internal_0588afb5b79e5db5383ee3bfebfbaca4903e734082f2165eaa114fb63a6e8e6e)))) {
            // line 104
            echo "\t<title>Information for All Prospective Vendors</title>
";
        } else {
            // line 106
            echo "\t<title>Business BID</title>
";
        }
        // line 108
        echo "


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta charset=\"utf-8\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
\t<!--[if lt IE 8]>
\t<script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
\t<script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
\t";
        // line 137
        echo "\t<link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">


\t<script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
\t<script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
";
        // line 144
        echo "\t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.scrollbox.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\$('#demo2').scrollbox({
\t  linear: true,
\t  step: 1,
\t  delay: 0,
\t  speed: 100
\t});
</script>
<script>
\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t";
        // line 164
        echo "});
</script>
<script type=\"text/javascript\" src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 169
        $this->displayBlock('customcss', $context, $blocks);
        // line 172
        echo "
";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 606
        echo "
";
        // line 607
        $this->displayBlock('customjs', $context, $blocks);
        // line 610
        echo "
";
        // line 611
        $this->displayBlock('jquery', $context, $blocks);
        // line 614
        echo "</head>
<body>

";
        // line 617
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 618
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 619
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 620
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 621
        echo "
<div class=\"main_container\">
\t<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t<div class=\"row\">
\t<div class=\"col-sm-3 contact-info\">
\t<div class=\"col-sm-6 clear-right\">
\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>

\t<span>(04) 42 13 777</span>
\t</div>
\t<div class=\"col-sm-6 left-clear\">

\t<span class=\"contact-no\"><a href=\"";
        // line 634
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></span>
\t</div>

\t</div>
\t<div class=\"col-sm-5 flash-message\"><!--<marquee behavior=\"scroll\" height=\"20\" direction=\"left\" scrollamount=\"2\">LAUNCHING SOON</marquee> --></div>
\t<div class=\"col-sm-4 clear-right \">

\t<div class=\"right-top-links\">
\t";
        // line 642
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 643
            echo "\t\t<ul class=\"nav navbar-nav\">
\t\t\t<li><a class=\"fb\" href=\"";
            // line 644
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\" ><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/my-login.png"), "html", null, true);
            echo "\"></a></li>
\t\t\t<li><img src=\"";
            // line 645
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fbsign.png"), "html", null, true);
            echo "\" onclick=\"FBLogin();\"></li>

\t\t\t<li id=\"flip\"><span class=\"glyphicon glyphicon-search search-block-top\"></span></li>
\t\t\t<div id=\"panel\">
\t\t\t<form action=\"";
            // line 649
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_globalsearch");
            echo "\" method=\"GET\" target=\"_blank\">
\t\t\t\t<input type=\"search\" name=\"keyword\" id=\"keyword\" value=\"\" placeholder=\"Search here\" style=\"width: 173px\"/>
\t\t\t\t<input class=\"search-btn form_submit\" type=\"submit\" value=\"Search\" />
\t\t\t</form>
\t\t\t\t";
            // line 655
            echo "\t\t\t</div>
\t\t</ul>
\t\t";
        }
        // line 658
        echo "\t</div>

\t";
        // line 660
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 661
            echo "\t<div class=\"col-sm-12 clear-right dash-right-top-links\">
\t<ul>
\t<li>Welcome, <span class=\"profile-name\">";
            // line 663
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
\t<li><a href=\"";
            // line 664
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t</ul>
\t</div>

\t";
        }
        // line 669
        echo "
\t</div>
\t</div>
\t</div>
\t</div>
\t<!-- top header Starts -->
<div class=\"header\">
\t<div class=\"container\">
    \t<div class=\"row\">
            <div class=\"col-md-5 logo\"><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\" ></a></div>
            <div class=\"col-md-7 right_nav\">
\t";
        // line 680
        $this->displayBlock('toprightmenu', $context, $blocks);
        // line 707
        echo "            </div>
        </div>
    </div>
</div>
    <!-- top header Ends -->
    <!-- Search block Starts -->
    <div class=\"search_block\">
\t";
        // line 714
        $this->displayBlock('search', $context, $blocks);
        // line 772
        echo "
    </div>
   \t<!-- Search block Ends -->
    <!-- Banner block Starts -->
    ";
        // line 776
        $this->displayBlock('banner', $context, $blocks);
        // line 834
        echo "    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 838
        $this->displayBlock('maincontent', $context, $blocks);
        // line 1097
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
\t";
        // line 1100
        $this->displayBlock('footer', $context, $blocks);
        // line 1279
        echo "    <!-- footer block ends -->
</div>

<div class=\"modal fade\" id=\"privacy-policy\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Privacy Policy</h4>
      </div>
\t<iframe src=\"/web/htm-files/privacy-policy.html\" width=\"100%\" height=\"500\" frameborder=\"0\">
\t</iframe>

    </div>
  </div>
</div>

<!--<div id=\"sitemap\">

    <li class=\"leaf\"><a href=\"";
        // line 1298
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\" title=\"\">Site map</a></li>

</div>-->

<div class=\"modal fade\" id=\"terms-conditions\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Terms &amp; Conditions</h4>
      </div>
\t<iframe src=\"/web/htm-files/terms-conditions.html\" width=\"100%\" height=\"500\" frameborder=\"0\">
\t</iframe>
    </div>
  </div>
</div>

</body>
</html>

<script type=\"text/javascript\">
window.onload = jsfunction();
//window.onload = getcharactercount();
//window.onload = megamenuAjax();
</script>
";
    }

    // line 169
    public function block_customcss($context, array $blocks = array())
    {
        // line 170
        echo "
";
    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        // line 174
        echo "<script>
\$(document).ready(function(){
\t";
        // line 176
        if (array_key_exists("keyword", $context)) {
            // line 177
            echo "\t   \twindow.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 181
        echo "\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\t\$('#category').autocomplete({
      \tsource: function( request, response ) {
      \t\$.ajax({
\t      \turl : realpath+\"ajax-info.php\",
\t      \tdataType: \"json\",
\t\t\tdata: {
\t\t\t   name_startsWith: request.term,
\t\t\t   type: 'category'
\t\t\t},
\t\t\tsuccess: function( data ) {
\t\t\t\tresponse( \$.map( data, function( item ) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item,
\t\t\t\t\t\tvalue: item
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t},
\t\t\tselect: function(event, ui) {
               \talert( \$(event.target).val() );
            }
      \t});
  \t\t},autoFocus: true,minLength: 2
  \t});
});
\t\$(document).ready(function(){
\t\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\t\t\$(\"#form_subcategory\").html('<option value=\"0\">Select</option>');
\t\t\$(\"#form_category\").on('change',function (){
\t\t\tvar ax = \t\$(\"#form_category\").val();
\t\t\t\$.ajax({url:realpath+\"pullSubCategoryByCategory.php?categoryid=\"+ax,success:function(result){
\t\t\t\t\$(\"#checkbox_subcategory\").html(result);
\t\t\t\t\$(\"#div_subcategory\").show();
\t\t\t}});
\t\t});
\t});

\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_email\").on('input',function (){
\tvar ax = \t\$(\"#form_email\").val();
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}

\telse{

\tvar filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
\tvar \$th = \$(this);
\tif (filter.test(ax)) {
\t\t\t\t\$th.css('border', '3px solid green');
\t\t\t}
\t\t\telse {
\t\t\t\t\$th.css('border', '3px solid red');
\t\t\t\te.preventDefault();
\t\t\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}

\t}
\t});




\t\$(\"#form_vemail\").on('input',function (){
\tvar ax = \t\$(\"#form_vemail\").val();
\tvar filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
\tvar \$th = \$(this);
\tif (filter.test(ax))
\t{
\t\t\$th.css('border', '3px solid green');
\t}
\telse {
\t\t\$th.css('border', '3px solid red');
\t\te.preventDefault();
\t\t}
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}
\t});




});

\t\$(document).ready(function(){
\t\$('#form_description').attr(\"maxlength\",\"240\");
\t\$('#form_location').attr(\"maxlength\",\"30\");

\t});

</script>
\t<script type='text/javascript' >

\tfunction jsfunction(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);

\tdocument.getElementById(\"searchval\").innerHTML=xmlhttp.responseText;
\t// megamenublogAjax();
\t// megamenubycatAjax();
\tmegamenubycatVendorSearchAjax();
\tmegamenubycatVendorReviewAjax();
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"ajax-info.php\",true);
\txmlhttp.send();



\t}

\tfunction megamenublogAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t\t\t\t\t\t\t\t\t//document.getElementById(\"megamenu1\").innerHTML=xmlhttp.responseText;
\t\tvar mvsa = document.getElementById(\"megamenu1Anch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Resourcesegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menublog.php\",true);
\t//xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);

\txmlhttp.send();



\t}
\tfunction megamenubycatAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\tdocument.getElementById(\"megamenubycatUl\").innerHTML=xmlhttp.responseText;
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menubycat.php\",true);
\txmlhttp.send();



\t}

\tfunction megamenubycatVendorSearchAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t//console.log(xmlhttp.responseText);
\t\tvar mvsa = document.getElementById(\"megamenuVenSearchAnch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Searchsegment-drop \") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t//document.getElementById(\"megamenuVenSearchAnch\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorSearch\",true);
\txmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorReviewAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();

\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t\t//alert(xmlhttp.responseText);
\t\t\tvar mvsa = document.getElementById(\"megamenuVenReviewAnch\");
\t\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Reviewsegment-drop\") {
\t\t\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t\t\t  break;
\t\t\t\t\t}
\t\t\t}

\t\t//document.getElementById(\"megamenuVenReviewUl\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t  xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorReview\",true);
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorReview\",true);
\txmlhttp.send();



\t}
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
\tFB.init({
\tappId      : '754756437905943', // replace your app id here
\tstatus     : true,
\tcookie     : true,
\txfbml      : true
\t});
};
(function(d){
\tvar js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
\tif (d.getElementById(id)) {return;}
\tjs = d.createElement('script'); js.id = id; js.async = true;
\tjs.src = \"//connect.facebook.net/en_US/all.js\";
\tref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
\tFB.login(function(response){
\t\tif(response.authResponse){
\t\t\twindow.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
\t\t}
\t}, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
function show_submenu(b, menuid)
{
\tdocument.getElementById(b).style.display=\"block\";
\tif(menuid!=\"megaanchorbycat\"){
\t\tdocument.getElementById(menuid).className = \"setbg\";
\t}
}

function hide_submenu(b, menuid)
{
\tsetTimeout(dispminus(b,menuid), 200);
}

function dispminus(subid,menuid) {
\tif(menuid!=\"megaanchorbycat\") {
\t\tdocument.getElementById(menuid).className = \"unsetbg\";
\t}
\tdocument.getElementById(subid).style.display=\"none\";
}


\$(document).ready(function(){
\t\$('#flip').click(function(){
\t\t\$('#panel').slideToggle('slow');
\t\t\$('#keyword').val('');
\t})

\t\$(document).click(function(event) {
\t\tif (!\$(event.target).closest(\"#flip, #panel\").length) {
\t\t\t\$(\"#panel\").slideUp('slow');
\t\t}
\t})
});



 \$(document).ready(function(){
      \$(\".Searchsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Searchsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });
 \$(document).ready(function(){
      \$(\".Reviewsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Reviewsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });

\$(document).ready(function(){
\t\$('#ab1').click(function(){
\t\tdocument.title='Find information About BusinessBid';
\t});
\t\$('#mt1').click(function(){
\t\tdocument.title='Let us Introduce the BusinessBid Team';
\t});
\t\$('#cr1').click(function(){
\t\tdocument.title='Have a Look at BusinessBid Career Opportunities';
\t});
\t\$('#vp1').click(function(){
\t\tdocument.title='Want to become BusinessBid Valued Partners';
\t});
\t\$('#ct1').click(function(){
\t\tdocument.title='All you wanted to Know About Us';
\t});

\t\$('#customertab').click(function(){
\t\tdocument.title='Customer Account Login and Dashboard Access';
\t});

\t\$('#vendortab').click(function(){
\t\tdocument.title='Vendor Account Login Access Page';
\t});

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
\t\ttype : \"POST\",
\t\tdata : {\"email\":email},
\t\turl:\"";
        // line 587
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
\t\tsuccess:function(result){
\t\t\t\$(\"#succ\").css(\"display\",\"block\");
\t\t},
\t\terror: function (error) {
\t\t\t\$(\"#err\").css(\"display\",\"block\");
      \t}
\t});

  });

  \$('#rc').click(function(){
\t\twindow.location.href = \"http://www.businessbid.ae/resource_centre/\";
  });
});
</script>


";
    }

    // line 607
    public function block_customjs($context, array $blocks = array())
    {
        // line 608
        echo "
";
    }

    // line 611
    public function block_jquery($context, array $blocks = array())
    {
        // line 612
        echo "
";
    }

    // line 680
    public function block_toprightmenu($context, array $blocks = array())
    {
        // line 681
        echo "            \t<nav class=\"top_menu\">
                \t<ul class=\"nav navbar-nav\">
                    \t<li><a href=\"";
        // line 683
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
                     <li><a href=\"";
        // line 684
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
\t<!--<li> <a href=\"";
        // line 685
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_job");
        echo "\" title=\"\">Post a Job</a></li> -->
\t";
        // line 686
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 687
            echo "                     \t";
        } else {
            // line 688
            echo "\t";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                // line 689
                echo "\t<li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_home");
                echo "\"> Dashboard </a></li>


\t";
            } else {
                // line 693
                echo "\t<li><a class=\"my_account\" href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home");
                echo "\"> My Account </a></li>
\t";
            }
            // line 695
            echo "\t";
        }
        // line 696
        echo "\t";
        if ((null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 697
            echo "                     <li>  <a class=\"fb\" href=\"";
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/ruvendor.jpg"), "html", null, true);
            echo "\"></a> </li>
                 ";
        }
        // line 699
        echo "                  </ul>
                </nav>
\t<nav class=\"top_menu_bottom\">
\t<ul>

\t</ul>
\t</nav>
\t";
    }

    // line 714
    public function block_search($context, array $blocks = array())
    {
        // line 715
        echo "    \t<div class=\"container\">
        \t<div class=\"row\">
\t<div class=\"col-md-7 left_nav\">
\t";
        // line 718
        $this->displayBlock('topleftmenu', $context, $blocks);
        // line 739
        echo "            </div>
\t<div class=\"col-md-5 search_block\">
\t <form name=\"search\" method=\"request\" action=\"";
        // line 741
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
            \t \t<input class=\"col-sm-8 search-box\" type=\"text\" list=\"browsers\" placeholder=\"Type the service you require\" id='category' name='category' ><datalist id=\"searchval\"></datalist>
<input type=\"submit\" value=\"Search\" class=\"search-btn form_submit col-sm-3\" />
                 </form>
\t </div>
    <!-- mega menu div block starts -->
\t";
        // line 747
        $this->displayBlock('megamenu', $context, $blocks);
        // line 767
        echo "    <!-- mega menu div block ends -->
            </div>
\t    </div>

\t    ";
    }

    // line 718
    public function block_topleftmenu($context, array $blocks = array())
    {
        // line 719
        echo "            \t<nav class=\"user_menu_top\">
                \t<ul class=\"nav navbar-nav\">
                    \t<li><a href=\"";
        // line 721
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
\t\t\t\t\t\t<li id = \"megamenuVenSearchAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Search<b class=\"caret\"></b></a>
\t\t\t\t\t\t<ul class=\"dropdown-menu Searchsegment-drop \"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li id = \"megamenuVenReviewAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Review<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Reviewsegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li id = \"megamenu1Anch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a id=\"rc\" href=\"http://www.businessbid.ae/resource_centre/\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" >Resource Centre<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Resourcesegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
                        <!--<li id = \"megamenu1Anch\"  onClick=\"javascript:show_submenu('megamenu1', this.id);\" onMouseOut=\"javascript:hide_submenu12('megamenu1', this.id);\"><a href=\"#\" id=\"megaanchor\" >Resource Centre</a></li>-->
                    </ul>
                </nav>

\t";
    }

    // line 747
    public function block_megamenu($context, array $blocks = array())
    {
        // line 748
        echo "\t<div id=\"megamenuVenSearch\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" ><div><ul id=\"megamenuVenSearchUl\"></ul></div></div>
\t<div id=\"megamenuVenReview\" class=\"submenu menu-mega megamenubg-1 megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');\"><div class=\"review-menu-left\"><ul id=\"megamenuVenReviewUl\"></ul></div><div  class=\"review-menu-right\"><button class=\"btn btn-success write-button\" type=\"button\" >Write a Review</button></div></div>
\t<ul id=\"megamenu1\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenu1','megamenu1Anch');\" onMouseOut=\"javascript:hide_submenu12('megamenu1','megamenu1Anch');\" ></ul>
\t<div id=\"megamenubycat\" class=\"submenu menu-megabycat megamenubg\" onMouseOver=\"javascript:show_submenu('megamenubycat','megaanchorbycat');\" onMouseOut=\"javascript:hide_submenu('megamenubycat','megaanchorbycat');\"><div><ul id=\"megamenubycatUl\"></ul></div><div></div></div>

\t";
        // line 753
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 754
            echo "
\t<script type=\"text/javascript\">

\tdocument.getElementById(\"megamenuVenSearch\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenuVenReview\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenu1\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\tdocument.getElementById(\"megamenubycat\").setAttribute(\"class\", \"submenu menu-megabycat-login megamenubg\");
\t</script>


\t";
        }
        // line 765
        echo "
\t";
    }

    // line 776
    public function block_banner($context, array $blocks = array())
    {
        // line 777
        echo "
    <div class=\"jumbotron\">
    <div id=\"carousel-example-generic\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\"  data-interval=\"3000\">
              <!-- Indicators -->
              <ol class=\"carousel-indicators\">
                <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>
\t<li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>
\t<li data-target=\"#carousel-example-generic\" data-slide-to=\"3\"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class=\"carousel-inner\">
\t<div class=\"item active\">
                  \t<img src=\"";
        // line 791
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner1.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
\t<div class=\"item\">
                  \t<img src=\"";
        // line 794
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner2.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
\t  \t<div class=\"item\">
                  \t<img src=\"";
        // line 797
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner3.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
\t<div class=\"item\">
                  \t<img src=\"";
        // line 800
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner4.jpg"), "html", null, true);
        echo "\" alt=\"...\">
                \t</div>
              </div>


            </div>

\t<div class=\"category-right-block\">
\t";
        // line 808
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t<div class=\"row\">
\t<h2>Get Quick Quotes from Local Vendors for Free!</h2>
\t<div class=\"col-md-6 side-clear right-med\">

\t<div class=\"form-group\">
\t";
        // line 814
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "
\t</div>
\t</div>
\t<div class=\"col-md-6 side-clear left-med\">
\t<div class=\"form-group\">
\t";
        // line 819
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
        echo "
\t</div>
\t</div>
\t<div class=\"row\">
\t<div class=\"form-group col-sm-12\">
\t";
        // line 824
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
\t</div>
\t</div>

\t</div>
\t";
        // line 829
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
\t</div>

    </div>
    ";
    }

    // line 838
    public function block_maincontent($context, array $blocks = array())
    {
        // line 839
        echo "    <div class=\"main_content\">
\t<div class=\"container content-top-one\">
\t<h1>How does it work? </h1>
\t<h3>Find the perfect vendor for your project in just three simple steps</h3>
\t<div class=\"content-top-one-block\">
\t<div class=\"col-sm-4 float-shadow\">
\t<div class=\"how-work\">
\t<img src=\"";
        // line 846
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
\t<h4>TELL US ONCE</h4>
\t<h5>Simply fill out a short form or give us a call</h5>
\t</div>
\t</div>
\t<div class=\"col-sm-4 float-shadow\">
\t<div class=\"how-work\">
\t<img src=\"";
        // line 853
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
\t<h4>VENDORS CONTACT YOU</h4>
                                   <h5>Receive three quotes from screened vendors in minutes</h5>
\t</div>
\t</div>
\t<div class=\"col-sm-4 float-shadow\">
\t<div class=\"how-work\">
\t<img src=\"";
        // line 860
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
\t<h4>CHOOSE THE BEST VENDOR</h4>
\t<h5>Use quotes and reviews to select the perfect vendors</h5>
\t</div>
\t</div>
\t</div>
\t</div>

<div class=\"content-top-two\">
\t<div class=\"container\">
\t<h1>Why choose Business Bid?</h1>
\t<h3>The most effective way to find vendors for your work</h3>
\t<div class=\"content-top-two-block\">
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 875
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
\t<h4>Convenience</h4>
\t</div>
\t<h5>Multiple quotations with a single, simple form</h5>
\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 882
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
\t<h4>Competitive Pricing</h4>
\t</div>
\t<h5>Compare quotes before picking the most suitable</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 890
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
\t<h4>Speed</h4>
\t</div>
\t<h5>Quick responses to all your job requests</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 898
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
\t<h4>Reputation</h4>
\t</div>
\t<h5>Check reviews and ratings for the inside scoop</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 906
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
\t<h4>Free To Use</h4>
\t</div>
\t<h5>No usage fees. Ever!</h5>

\t</div>
\t<div class=\"col-sm-4 wobble-vertical\">
\t<div class=\"choose-vendor\">
\t<img src=\"";
        // line 914
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
\t<h4>Amazing Support</h4>
\t</div>
\t<h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

\t</div>
\t</div>
\t</div>

</div>
    \t<div class=\"container content-top-three\">
        \t<div class=\"row\">
\t";
        // line 926
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 966
        echo "            </div>

\t     </div>
        </div>

    </div>

\t\t<!--<div class=\"col-sm-12\">
\t\t\t<div id=\"demo2\" class=\"scroll-text\">
\t\t\t  <ul>
\t\t\t\t<li><a href=\"#\">DEA Mines \"National Security\" Data To Spy On Americans, Now Concealing Program</a></li>
\t\t\t\t<li><a href=\"#\">Sergey Brin invests in synthetic beef</a></li>
\t\t\t\t<li><a href=\"#\">OS X emulation layer for Linux</a></li>
\t\t\t\t<li><a href=\"#\">Fast android emulator using Virtualbox</a></li>
\t\t\t\t<li><a href=\"#\">Latvia blocking extradition of Gozi writer due to disproportionate US sentencing</a></li>
\t\t\t\t<li><a href=\"#\">Please let me know if I should Stop developing apps for Google</a></li>
\t\t\t  </ul>
\t\t\t</div>
\t\t</div>-->


\t <div class=\"certified_block\">
    \t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t<h1>We do work for you!</h1>
\t\t\t<div class=\"content-block\">
\t\t\t<p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
\t\t\t<p>Your satisfication matters to us!</p>
\t\t\t<p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
\t\t\t<p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
\t\tservice experience.</p>
\t\t\t</div>
\t\t\t</div>
        </div>
\t</div>
\t <div class=\"popular_category_block\">
    \t<div class=\"container\">
\t<div class=\"row\">
\t<h1>Popular Categories</h1>
\t<h3>Check out some of the hottest services in the UAE</h3>
\t<div class=\"content-block\">
\t<div class=\"col-sm-12\">
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1010
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 1011
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
\t";
        } else {
            // line 1013
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
\t";
        }
        // line 1015
        echo "
\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>

\t";
        // line 1021
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 1022
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
\t";
        } else {
            // line 1024
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
\t";
        }
        // line 1026
        echo "
\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1031
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 1032
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
\t";
        } else {
            // line 1034
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
\t";
        }
        // line 1036
        echo "


\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1043
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 1044
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
\t";
        } else {
            // line 1046
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
\t";
        }
        // line 1048
        echo "
\t</ul>
\t</div>

\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1054
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 1055
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
\t";
        } else {
            // line 1057
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
\t";
        }
        // line 1059
        echo "

\t</ul>
\t</div>
\t<div class=\"col-sm-2\">
\t<ul>
\t";
        // line 1065
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 1066
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
\t";
        } else {
            // line 1068
            echo "\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
\t";
        }
        // line 1070
        echo "

\t</ul>
\t</div>
\t</div>
\t</div>
\t<div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource_centre/home-2\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
\t</div>
\t</div>
       </div>
<!--
<div class=\"people_business_block\">
    <div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"content-block\">
\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t<img src=\"";
        // line 1086
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/businessbid-logo-botom.png"), "html", null, true);
        echo "\" /> <h1>is the most efficient way for Customers
\t\t\t\t\t\t\tto find Vendors in the UAE.<br/> Why not give it a try?</h1>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-12 see-all-cat-block \"><a class=\"btn btn-success see-all-cat blue wobble-horizontal\" href=\"";
        // line 1089
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_quote_inline");
        echo "\">Request Free Quotes</a><a class=\"btn btn-success see-all-cat peach wobble-horizontal\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works");
        echo "\">Register Your Business</a></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
-->
\t";
    }

    // line 926
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 927
        echo "\t<h1>We operate all across the UAE</h1>
            <div class=\"col-md-6 grow\">
\t<img src=\"";
        // line 929
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

\t<div class=\"col-md-6\">
              \t<h2>Recent Jobs</h2>
\t\t\t\t<div id=\"demo2\" class=\"scroll-text\">
\t\t\t\t\t<ul class=\"recent_block\">
\t\t\t\t\t\t";
        // line 936
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 937
            echo "\t\t\t\t\t\t<li class=\"activities-block\">
\t\t\t\t\t\t<div class=\"act-date\">";
            // line 938
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
\t\t\t\t\t\t";
            // line 939
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 940
                echo "\t\t\t\t\t\t\t";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 941
                echo "\t\t\t\t\t\t\t";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 942
                echo "\t\t\t\t\t\t<span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
\t\t\t\t\t\t";
            } else {
                // line 944
                echo "\t\t\t\t\t\t\t";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 945
                echo "\t\t\t\t\t\t\t";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 946
                echo "\t\t\t\t\t\t<span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
\t\t\t\t\t\t";
            }
            // line 948
            echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 950
        echo "\t\t\t\t\t</ul>
\t\t\t\t</div>


\t<div class=\"col-md-12 side-clear\">
\t";
        // line 955
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 961
        echo "\t</div>
            </div>


\t";
    }

    // line 955
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 956
        echo "\t<div class=\"request\">
\t<h2>";
        // line 957
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
\t<span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
\t</div>
\t";
    }

    // line 1100
    public function block_footer($context, array $blocks = array())
    {
        // line 1101
        echo "<footer class=\"footer_wrapper\">
\t<div class=\"container\">
\t<div class=\"row\">
\t<div class=\"customer-benefits-block\">
\t <div class=\"region region-footer-links-one footer-menu\">
\t<section id=\"block-menu-menu-footer-menu-one\" class=\"block block-menu clearfix customer-benefits\">
\t<h2 class=\"block-title\">Customer Benefits</h2>
\t<ul class=\"menu nav\">
\t<li class=\"first leaf convenience\"><a>Convenience</a>
\t<span>Multiple quotations with a single, simple form</span>
\t</li>
\t<li class=\"leaf active competitive\"><a>Competitive Pricing</a>
\t<span>Compare quotes before choosing the most suitable.</span>
\t</li>
\t<li class=\"leaf active speed\"><a>Speed</a>
\t<span>Quick responses to all your job requests.</span>
\t</li>
\t<li class=\"leaf active reputation\"><a>Reputation</a>
\t<span>Check reviews and ratings for the inside scoop.</span>
\t</li>
\t<li class=\"leaf active free-use\"><a>Free to Use</a>
\t<span>No usage fees. Ever!</span>
\t</li>
\t<li class=\"last leaf active amazing\"><a>Amazing Support</a>
\t<span>Phone. Live Chat. Facebook. Twitter.</span>
\t</li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\"footer-right-menu-block\">
\t<div class=\"footer-right-menu-block-top\">
\t<div class=\"customer-benefits-block-first\">
\t <div class=\"region region-footer-links-two footer-menu\">
\t<section id=\"block-menu-menu-footer-menu-two\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">About</h2>
\t<ul class=\"menu nav\">
\t<li class=\"first leaf\"><a href=\"";
        // line 1138
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\" title=\"\" id=\"ab1\">About Us</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1139
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\" title=\"\" class=\"active\" id=\"mt1\">Meet the Team</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1140
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\" title=\"\" class=\"active\" id=\"cr1\">Career Opportunities</a></li>
\t<li class=\"last leaf active\"><a href=\"";
        // line 1141
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\" title=\"\" class=\"active\" id=\"vp1\">Valued Partners</a></li>

\t<li class=\"leaf active\"><a href=\"";
        // line 1143
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\" class=\"active\" id=\"ct1\">Contact Us</a></li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-three footer-menu\">
\t<section id=\"block-menu-menu-for-services-professionals\" class=\"block block-menu clearfix \">
\t<h2 class=\"block-title\">Vendor Resources</h2>
\t<ul class=\"menu nav\">
\t<li class=\"leaf\"><a href=\"";
        // line 1153
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\" title=\"\">Login</a></li>
\t<li class=\"first leaf active\"><a href=\"";
        // line 1154
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_grow_your_business");
        echo "\" title=\"\" class=\"active\">Grow Your Business</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1155
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works");
        echo "\" title=\"\">How it Works</a></li>
\t<li class=\"first leaf\"><a href=\"";
        // line 1156
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" title=\"\">Become a Vendor</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1157
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\" title=\"\" class=\"active\">FAQ's &amp; Support</a></li>
\t</ul>
\t</section>
\t<!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-four footer-menu social-links\">
\t<section id=\"block-menu-menu-for-services-professionals\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">Stay Connected</h2>
\t<ul class=\"menu nav\">
\t<li class=\"leaf\"><a href=\"https://www.facebook.com/businessbid?ref=hl\" class=\"facebook\" target=\"_blank\">Like us on Facebook</a></li>
\t<li class=\"first leaf active\"><a href=\"https://twitter.com/BusinessBid\" title=\"\" class=\"twitter\" target=\"_blank\">Follow us on Twitter</a></li>
\t<li class=\"leaf\"><a href=\"#\" title=\"\" class=\"gplus\" target=\"_blank\">Follow Us on Google+</a></li>
\t</ul>
\t</section>
\t<!-- /.block -->
\t</div>
\t</div>

\t<div class=\"col-xs-12 customer-benefits-block-last footer_nav_liks\">
\t<div class=\"region region-footer-links-five\">
\t<section id=\"block-block-8\" class=\"block block-block clearfix\">
\t<h2 class=\"block-title\">Contact Us</h2>
\t<address class=\"addr-map\">Suite 503, Level 5, Indigo Icon Tower, Cluster F, Jumeirah Lakes Tower - Dubai, UAE</address>
\t<span class=\"work-day-time\">Sunday-Thursday</span>
\t<span class=\"work-day-time\">9 am - 6 pm</span>

\t<span class=\"mobile\">(04) 42 13 777</span>
\t<span class=\"addr\">BusinessBid DMCC, <br>PO Box- 393507, Dubai, UAE</span>
\t";
        // line 1188
        echo "\t</section> <!-- /.block -->
\t</div>

\t</div>
\t</div>
\t<div class=\"footer-right-menu-block-top\">
\t<div class=\"customer-benefits-block-first\">
\t <div class=\"region region-footer-links-two footer-menu\">
\t<section id=\"block-menu-menu-footer-menu-two\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">Client Services</h2>
\t<ul class=\"menu nav\">
\t<!-- <li class=\"first leaf\"><a href=\"";
        // line 1199
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_job");
        echo "\" title=\"\">Post a Job</a></li> -->
\t";
        // line 1201
        echo "\t<li class=\"leaf\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\" title=\"\">Search Reviews</a></li>
\t<li class=\"last leaf\"><a href=\"";
        // line 1202
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\" title=\"\">Write a Review</a></li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-three footer-menu\">
\t<section id=\"block-menu-menu-client-resources\" class=\"block block-menu clearfix\">
\t<h2 class=\"block-title\">Client Resources</h2>
\t<ul class=\"menu nav\">
\t<li class=\"leaf\"><a href=\"";
        // line 1212
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\" title=\"\">Login</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1213
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works_2");
        echo "\">How it Works</a></li>
\t<!--<li class=\"first leaf\"><a href=\"";
        // line 1214
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\" title=\"\">Become a Consumer</a></li> -->
\t<li class=\"last leaf active\"><a href=\"http:../../../resource_centre\" title=\"\" class=\"active\">Resource Center</a></li>
\t<li class=\"leaf active\"><a href=\"";
        // line 1216
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\" title=\"\" class=\"active\">FAQ's &amp; Support</a></li>
\t</ul>
\t</section> <!-- /.block -->
\t</div>
\t</div>
\t<div class=\" col-sm-3\">
\t<div class=\"region region-footer-links-four footer-menu\">
\t<section id=\"block-menu-menu-for-services-professionals\" class=\"block block-menu clearfix payment-accept\">
\t<h2 class=\"block-title\">Accepted Payment</h2>
\t<ul class=\"menu nav col-sm-8\">
\t<li class=\"leaf\"><a><img src=\"";
        // line 1226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/visa-icon.jpg"), "html", null, true);
        echo " \" /></a></li>
\t<li class=\"leaf\"><a><img src=\"";
        // line 1227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/masercard-icon.jpg"), "html", null, true);
        echo " \" /></a></li>
\t<!-- <li class=\"leaf\"><a><img src=\"";
        // line 1228
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/paypal-icon.jpg"), "html", null, true);
        echo " \" /></a></li> -->
\t<li class=\"leaf\"><a><img src=\"";
        // line 1229
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/securecode-icon.jpg"), "html", null, true);
        echo "\" /></a></li>
\t</ul>
\t</section>
\t<!-- /.block -->
\t</div>
\t</div>

\t<div class=\"col-xs-12 customer-benefits-block-last footer_nav_liks\">
\t<div class=\"region region-footer-links-five\">
\t<section id=\"block-block-8\" class=\"block block-block clearfix subscribe_news\">
\t<div id=\"succ\" style=\"display:none;color:#FFFFFF\">You have successfully Subscribed Newsletter</div>
\t<div id=\"err\" style=\"display:none;color:#FFFFFF\">You have already Subscribed</div>
\t<h2 class=\"block-title\">Subscribe to Newsletter</h2>
\t<div class=\"subscribe-text\">Sign up with your e-mail to get tips, resources
and amazing deals</div> <div class=\"mail-box\">
<form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource_centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
<input type=\"hidden\" name=\"nr\" value=\"widget\">
<input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email here\">
<input class=\"submit\" type=\"submit\" value=\"Subscribe\" /></div>
</form>\t</section> <!-- /.block -->
\t</div>

\t</div>
\t</div>

\t<div class=\"footer-right-menu-block-top\">
\t<div class=\"region region-footer-quick-links\">
\t<section id=\"block-menu-menu-footer-quick-links\" class=\"block block-menu clearfix\">
\t  <ul class=\"menu nav\">
\t  \t<li class=\"first leaf\"><a href=\"";
        // line 1258
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1259
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\" title=\"\">Careers</a></li>
\t<li class=\"leaf\"><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
\t<li class=\"leaf\"><a href=\"";
        // line 1261
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
\t<li class=\"last leaf\"><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms &amp; Conditions</a></li>
\t  </ul>
\t</section> <!-- /.block -->
  \t</div>
               \t</div>


\t</div>
\t</div>
\t</div>
\t</div>
\t";
        // line 1274
        echo "\t<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</footer>
\t";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2006 => 1274,  1991 => 1261,  1986 => 1259,  1982 => 1258,  1950 => 1229,  1946 => 1228,  1942 => 1227,  1938 => 1226,  1925 => 1216,  1920 => 1214,  1916 => 1213,  1912 => 1212,  1899 => 1202,  1894 => 1201,  1890 => 1199,  1877 => 1188,  1844 => 1157,  1840 => 1156,  1836 => 1155,  1832 => 1154,  1828 => 1153,  1815 => 1143,  1810 => 1141,  1806 => 1140,  1802 => 1139,  1798 => 1138,  1759 => 1101,  1756 => 1100,  1748 => 957,  1745 => 956,  1742 => 955,  1734 => 961,  1732 => 955,  1725 => 950,  1718 => 948,  1702 => 946,  1699 => 945,  1696 => 944,  1682 => 942,  1679 => 941,  1676 => 940,  1674 => 939,  1670 => 938,  1667 => 937,  1663 => 936,  1653 => 929,  1649 => 927,  1646 => 926,  1632 => 1089,  1626 => 1086,  1608 => 1070,  1602 => 1068,  1596 => 1066,  1594 => 1065,  1586 => 1059,  1580 => 1057,  1574 => 1055,  1572 => 1054,  1564 => 1048,  1558 => 1046,  1552 => 1044,  1550 => 1043,  1541 => 1036,  1535 => 1034,  1529 => 1032,  1527 => 1031,  1520 => 1026,  1514 => 1024,  1508 => 1022,  1506 => 1021,  1498 => 1015,  1492 => 1013,  1486 => 1011,  1484 => 1010,  1438 => 966,  1436 => 926,  1421 => 914,  1410 => 906,  1399 => 898,  1388 => 890,  1377 => 882,  1367 => 875,  1349 => 860,  1339 => 853,  1329 => 846,  1320 => 839,  1317 => 838,  1308 => 829,  1300 => 824,  1292 => 819,  1284 => 814,  1275 => 808,  1264 => 800,  1258 => 797,  1252 => 794,  1246 => 791,  1230 => 777,  1227 => 776,  1222 => 765,  1209 => 754,  1207 => 753,  1200 => 748,  1197 => 747,  1175 => 721,  1171 => 719,  1168 => 718,  1160 => 767,  1158 => 747,  1149 => 741,  1145 => 739,  1143 => 718,  1138 => 715,  1135 => 714,  1124 => 699,  1116 => 697,  1113 => 696,  1110 => 695,  1104 => 693,  1096 => 689,  1093 => 688,  1090 => 687,  1088 => 686,  1084 => 685,  1080 => 684,  1076 => 683,  1072 => 681,  1069 => 680,  1064 => 612,  1061 => 611,  1056 => 608,  1053 => 607,  1030 => 587,  622 => 181,  616 => 177,  614 => 176,  610 => 174,  607 => 173,  602 => 170,  599 => 169,  569 => 1298,  548 => 1279,  546 => 1100,  541 => 1097,  539 => 838,  533 => 834,  531 => 776,  525 => 772,  523 => 714,  514 => 707,  512 => 680,  505 => 678,  494 => 669,  486 => 664,  482 => 663,  478 => 661,  476 => 660,  472 => 658,  467 => 655,  460 => 649,  453 => 645,  447 => 644,  444 => 643,  442 => 642,  431 => 634,  416 => 621,  414 => 620,  412 => 619,  410 => 618,  408 => 617,  403 => 614,  401 => 611,  398 => 610,  396 => 607,  393 => 606,  391 => 173,  388 => 172,  386 => 169,  381 => 167,  377 => 166,  373 => 164,  357 => 144,  353 => 141,  347 => 137,  343 => 134,  339 => 133,  335 => 132,  331 => 131,  327 => 130,  316 => 122,  312 => 121,  308 => 120,  294 => 108,  290 => 106,  286 => 104,  284 => 103,  281 => 102,  279 => 101,  276 => 100,  274 => 99,  271 => 98,  269 => 97,  266 => 96,  264 => 95,  261 => 94,  259 => 93,  256 => 92,  254 => 91,  251 => 90,  249 => 89,  246 => 88,  244 => 87,  241 => 86,  239 => 85,  236 => 84,  234 => 83,  231 => 82,  229 => 81,  226 => 80,  224 => 79,  221 => 78,  219 => 77,  216 => 76,  214 => 75,  211 => 74,  209 => 73,  206 => 72,  204 => 71,  201 => 70,  199 => 69,  196 => 68,  194 => 67,  191 => 66,  189 => 65,  186 => 64,  184 => 63,  181 => 62,  179 => 61,  176 => 60,  174 => 59,  171 => 58,  169 => 57,  166 => 56,  164 => 55,  161 => 54,  159 => 53,  156 => 52,  154 => 51,  151 => 50,  149 => 49,  146 => 48,  144 => 47,  141 => 46,  139 => 45,  136 => 44,  134 => 43,  131 => 42,  129 => 41,  126 => 40,  124 => 39,  121 => 38,  119 => 37,  116 => 36,  114 => 35,  111 => 34,  109 => 33,  106 => 32,  104 => 31,  101 => 30,  99 => 29,  96 => 28,  94 => 27,  91 => 26,  89 => 25,  86 => 24,  84 => 23,  81 => 22,  79 => 21,  76 => 20,  74 => 19,  71 => 18,  69 => 17,  66 => 16,  64 => 15,  61 => 14,  59 => 13,  56 => 12,  54 => 11,  51 => 10,  49 => 9,  46 => 8,  44 => 7,  42 => 6,  40 => 5,  38 => 4,  36 => 3,  32 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:login.html.twig */
class __TwigTemplate_1027ae6cbbba3946daaf8b76ec346dd95c02605bb23679ad2a0b15961c15544d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:User:login.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        echo " 
<script src = \"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src = \"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
\$('#vendor').css('display','none');

\$('#customertab').click(function(){
    
\t\t\$('#vendor').css('display','none');
\t\t\$('#customer').css('display','block');
\t\t\$('#vendor-help').css('display','none');
\t\t\$('#consumer-help').css('display','block');
\t\t\$(\"#register\").attr(\"href\", \"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\")

});
\$('#vendortab').click(function(){
\t\t\$('#customer').css('display','none');
\t\t\$('#vendor').css('display','block');
\t\t\$('#vendor-help').css('display','block');
\t\t\$('#consumer-help').css('display','none');
\t\t\$(\"#register\").attr(\"href\", \"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\")
\t\t
\t});


});

</script>
<div class=\"container\">

\t";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 38
            echo "
\t<div class=\"alert alert-danger\">";
            // line 39
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "
\t";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 44
            echo "
\t<div class=\"alert alert-success\">";
            // line 45
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 49
            echo "
\t<div class=\"alert alert-success\">";
            // line 50
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "\t
<div class=\"login-wrapper col-md-12\">
<ul class=\"nav nav-tabs tabs\" id=\"myTab\">
\t  <li class=\"active\"><a href=\"#home\" data-toggle=\"tab\" id=\"customertab\">Customer</a></li>
\t  <li><a href=\"#profile\" data-toggle=\"tab\" id=\"vendortab\">Vendor</a></li>
</ul>
<div class=\"login_inner_wrapper\">
<div class=\"login-block col-md-6\">
\t
\t
\t\t";
        // line 63
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
\t
\t\t<div class=\"login-fb\">
\t\t\t<a href=\"#\" class=\"fb-but\"></a>
\t\t\t<p>We will never post anything on your wall without your permission</p>
\t\t</div>\t
\t\t<h2> <span>OR</span> </h2>
\t
\t\t<div>

\t\t\t<h4>LOGIN</h4>
\t\t\t<div> ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "</div>

\t\t\t<div> ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'widget');
        echo "</div>
\t\t</div>
\t";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo " 


\t\t<h6><a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_forgot_password");
        echo "\" >Forgot your password?</a></h6>
\t\t<div class=\"new-user\">Are you a new user? <a href=\"";
        // line 83
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_register");
        echo "\" id=\"register\">Register here</a></div>\t\t
\t\t<div id=\"consumer-help\">
\t\t\t<h6>Need Help?</h6>
\t\t\t<p>Call Customer Support Line: <br>(04) 42 13 777</p>
\t\t\t<p>Get help via Live Chat</p>
\t\t</div>
\t\t<div id=\"vendor-help\" style=\"display:none\">
\t\t\t<h6>Need Help?</h6>
\t\t\t<p>Call Vendor Support Line:<br>(04) 42 13 777</p>
\t\t\t<p>Get help via Live Chat</p>
\t\t</div>

\t\t
</div>
<div class=\"col-md-6 login-content\" id=\"customer\">
\t<h4>Why create a customer account?</h4>
\t<div class=\"login-icon icon-1\">
\t\t<h3>Convenience</h3>
\t\t<p>Allows you to receive multiple quotes from vendors with a single form and eliminate the hassle of multiple searches</p> 
\t</div>
\t
\t<div class=\"login-icon icon-2\">
\t\t<h3>Competitive Pricing</h3>
\t\t<p>Receive at least three quotations from vendors so you know you have a choice to get the most competitive pricing!</p>
\t</div>
\t
\t<div class=\"login-icon icon-3\">
\t\t<h3>Speed</h3>
\t\t<p>As vendors are competing with each other you can be assured that you will get a swift response. No more waiting around!</p>
\t</div>
\t
\t<div class=\"login-icon icon-4\">
\t\t<h3>Reputation</h3>
\t\t<p>All vendors are pre-qualified and verified to ensure they are reputable and skilled</p>
\t</div>
\t
\t<div class=\"login-icon icon-5\">
\t\t<h3>Quality</h3>
\t\t<p>Our vendor rating system focusses on vendors delivering high quality service and a positive user experience</p>
\t</div>
\t
\t<div class=\"login-icon icon-6\">
\t\t<h3>Free to Use</h3>
\t\t<p>The website is completely free for users with no search or membership fees charged ever! </p>
\t</div>
\t
\t<div class=\"login-icon icon-7\">
\t\t<h3>Portal</h3>
\t\t<p>Get access to your personal services dashboard where you can view and manage your jobs from anywhere, anytime</p>
\t</div>
\t
\t

</div>
<div class=\"col-md-6 login-content\" id=\"vendor\">
\t<h4>Why create a vendor account?</h4>
\t
\t<div class=\"login-icon icon-8\">
\t\t<h3>Competitive Edge</h3>
\t\t<p>Get qualified leads provided to you for your business. Stay ahead of your competition!</p> 
\t</div>
\t
\t<div class=\"login-icon icon-9\">
\t\t<h3>Choice of Jobs</h3>
\t\t<p>You choose if you want to accept the lead or not. Pay only for leads accepted!</p>
\t</div>
\t
\t<div class=\"login-icon icon-10\">
\t\t<h3>Extend your reach</h3>
\t\t<p>Let us do the hard work and provide leads from areas outside your immediate business reach. Extend your services further!</p>
\t</div>
\t
\t<div class=\"login-icon icon-11\">
\t\t<h3>Dashboard</h3>
\t\t<p>Get access to your personal services dashboard with information on leads offered, leads accepted and outstanding jobs and much more and manage your jobs 
\tfrom anywhere, anytime</p>
\t</div>
\t
</div>

</div>
</div>

</div>
<script>
\$(document).ready(function(){
var url = window.location.href;
var arr = url.split(\"#\");
if(arr[1]=='vendor'){\t
\t\$('#myTab a[href=\"#profile\"]').tab('show');
\t\t\$('#customer').css('display','none');
\t\t\$('#vendor').css('display','block');
\t\t\$(\"#register\").attr(\"href\", \"";
        // line 175
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\");
}
});
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 175,  186 => 83,  182 => 82,  176 => 79,  171 => 77,  166 => 75,  152 => 64,  148 => 63,  136 => 53,  127 => 50,  124 => 49,  119 => 48,  110 => 45,  107 => 44,  103 => 43,  100 => 42,  91 => 39,  88 => 38,  84 => 37,  71 => 27,  60 => 19,  46 => 8,  42 => 7,  37 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:node4.html.twig */
class __TwigTemplate_2c911ae851d18a414283fc9cf3267a158acbee0146c919f3993c5077fbf0377a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node4.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 6
    public function block_maincontent($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"container\">
\t<!-- <div class=\"breadcrum\">Home/ Vendor Review & Rating</div> -->
\t<div class=\"vendor-review-dic row\">
        <!--[if lt IE 8]>
            <p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class=\"vendor_reviews_area\">
            <div class=\"vendor_reviews_area_child\">
                <div class=\"vendor_reviews_area_child_img\">
                    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper_one.png"), "html", null, true);
        echo "\" alt=\"Our One Page Praper\">
                </div>
                <div class=\"vendor_reviews_area_child_txt\">
                    <h2>Vendor Reviews Directory</h2>
                    <p>Search our directory for ratings and reviews for screened and approved BusinessBid service professionals. You can search for vendors by sorting by Highest Rated, Most Reviewed or by the Best Match vendors for the project. </p>
                    <p>Just select the type of service you are looking for, and you will find a list of rated service professionals for your project.</p>
                </div>               
            </div>           
        </div>
        <div class=\"position_area\">
\t\t\t<div class=\"vandor_reviews_directory\">
\t\t\t\t<div class=\"vandor_reviews_directory_txt\">
\t\t\t\t\t<h2>Browse Our Live Directory of Vendors</h2>
\t\t\t\t</div>
\t\t\t\t<div class=\"vandor_reviews_directory_dropdown\">
\t\t\t\t\t";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["formreview"]) ? $context["formreview"] : null), 'form_start');
        echo "
\t\t\t\t\t\t";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formreview"]) ? $context["formreview"] : null), "category", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t\t
\t\t\t\t\t\t  
\t\t\t\t</div>     
\t\t\t\t<div class=\"vandor_reviews_directory_btn\">
\t\t\t\t\t\t\t";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formreview"]) ? $context["formreview"] : null), "submit", array()), 'row', array("label" => "BROWSE REVIEWS"));
        echo "
\t\t\t\t\t\t\t";
        // line 40
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["formreview"]) ? $context["formreview"] : null), 'form_end');
        echo "    
\t\t\t\t\t\t</div>      
\t\t\t</div>             
        </div>
        <div class=\"thework_reviews_area\">
            <div class=\"thework_reviews_area_child\">
\t\t\t\t<div class=\"thework_reviews_area_child_img\">
                    <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/praper_two.png"), "html", null, true);
        echo "\" alt=\"Our two Page Praper\">
                </div>
                <div class=\"thework_reviews_area_child_txt\">
                    <h2>LET US DO THE WORK FOR YOU</h2>
                    <p>Allow us to match you with the most suitable service professionals for your project. </p>
                    <p>Just select the type of job you need done and tell us a little bit about your requirement. We will get 3 of the most suitable screened and approved service professionals’ contact you with quotations.</p>
                </div>
            </div>
        </div>
        <div class=\"position_area_two\">
            <div class=\"thework_reviews_directory\">
                <div class=\"thework_reviews_directory_txt\">
                    <h2>Find Me Vendors</h2>
                </div>
                <div class=\"vandor_reviews_directory_dropdown\">
\t\t\t\t\t<form name=\"search\" method=\"request\" action=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
\t\t\t\t\t\t<select name=\"category\" id=\"categoryfind\" class=\"form-control\"></select>
\t\t\t\t\t\t<!-- <select name=\"city\" id=\"city\"></select> -->
\t\t\t\t</div>
\t\t\t\t<div class=\"thework_reviews_directory_btn\">
\t\t\t\t\t\t<input type=\"submit\" class=\"findme_btn\" name=\"submit\" value=\"FIND ME VENDORS\"> 
\t\t\t\t\t\t</form>
\t\t\t\t</div>
            </div>     
        </div>       
       ";
        // line 117
        echo "      </div>
     </div>
        
        <!-- Footer Script Here -->
        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"js/vendor/jquery-1.11.2.min.js\"><\\/script>')</script>
        <script src=\"js/bootstrap.min.js\"></script>
        <script src=\"js/main.js\"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
 
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node4.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 117,  112 => 62,  94 => 47,  84 => 40,  80 => 39,  72 => 34,  68 => 33,  50 => 18,  37 => 7,  34 => 6,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:template3.html.twig */
class __TwigTemplate_2c21bad8c058b111e0931c059d545d7bd9976635a98d454209fa0ed2be8a32a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:template3.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "        <div class=\"hiring_area_main\">
            <div class=\"inner_hiring_area\">
                <div class=\"hiring_area_img\">
                    <img src=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/peoples.png"), "html", null, true);
        echo " alt=\"peopleas\">
                </div>
                <div class=\"hiring_area_content\">
                    <h1>How do I get hired on BusinessBid?</h1>
                    <p>Need help getting hired? Getting qualified leads sent to you is a starting point – the goal is to convert those leads into customers. Here are some tips that willensure you are able to successfully close more deals.</p>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" class=\"join_up_btn\">JOIN NOW</a>
                </div>
            </div>
        </div>
        <div class=\"impression_main_area\">
            <div class=\"inner_impression_area\">
                <div class=\"inner_impression_business\">
                    <div class=\"inner_impression_business_img\">
                        <img src=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/bis_icon.png"), "html", null, true);
        echo " alt=\"Business Icon\">
                    </div>
                    <div class=\"inner_impression_business_content\">
                        <h4>Create an Impressive Business Profile</h4>
                        <p>Your profile page is the customer's first impression of you. Make sure it has clear and concise information about your company and the services you provide. Upload your trade license and business certifications, list your services and your contact details.. Demonstrate what you can do by including images and videos of your past work, testimonials and products. We know that customers are looking for experienced professionals who care so it is in your best interest to make a good connection. Be genuine and believable!</p>
                    </div>
                </div>
                <div class=\"inner_impression_business\">
                    <div class=\"inner_impression_business_img\">
                        <img src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mes_icon.png"), "html", null, true);
        echo " alt=\"Communication & Commitment\">
                    </div>
                    <div class=\"inner_impression_business_content\">
                        <h4>Communication & Commitment</h4>
                        <p>Always attempt to address any issues the customer has raised and respond to their specific needs. It helps to be competitive on pricing and have clarity on what your price includes and most importantly what you can or cannot do. Stick to your commitment and pay attention to detail - address the customer by name, be punctual and deliver to what you agree. Customer service is key in the services business.</p>
                    </div>
                </div>
                <div class=\"inner_impression_business\">
                    <div class=\"inner_impression_business_img\">
                        <img src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/follow_ionc.png"), "html", null, true);
        echo " alt=\"Follow Up\">
                    </div>
                    <div class=\"inner_impression_business_content\">
                        <h4>Follow Up</h4>
                        <p>Most businesses secure customer's jobs not purely based on pricing but how you engage with them and what value you provide. Your timely follow up, punctuality, attention to detail and excellent customer service all matter when trying to win the business. Most customers are more than happy to pay the right amount for the right service.</p>
                    </div>
                </div>
                <div class=\"inner_impression_business\">
                    <div class=\"inner_impression_business_img\">
                        <img src=";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/book_icon.png"), "html", null, true);
        echo " alt=\"Receiving Feedback and Reviews\">
                    </div>
                    <div class=\"inner_impression_business_content\">
                        <h4>Receiving Feedback and Reviews</h4>
                        <p>It is important to get reviews and ratings of jobs done in the past as this builds confidence amongst prospective customers. We notice that service professionals that have at least 1-2 reviews are far more likely to be engaged than companies that have no reviews. Always take the time to ask your customers to provide feedback and write reviews.</p>
<p>Just remember, no company is perfect and we all strive to improve all the time. In fact, we often see that a few negative reviews makes your company profile seem more realistic and leads to more customer inquiries. Of course the more reviews you have shows potential customers how popular and active you are!
</p>
                    </div>
                </div>
                <div class=\"inner_impression_business\">
                    <div class=\"inner_impression_business_img\">
                        <img src=";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/hand_icon.png"), "html", null, true);
        echo " alt=\"Stay Persistent! \">
                    </div>
                    <div class=\"inner_impression_business_content\">
                        <h4>Stay Persistent! </h4>
                        <p>Our aim is to provide you a high volume of quality customer leads. Many service professionals usually double or even triple the size of their business by simply responding consistently to customer requests. However, you need to be persistent in experimenting to see what works for you as statistics suggest that it could take more than 5 attempts to secure your first job. </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    ";
    }

    // line 77
    public function block_maincontent($context, array $blocks = array())
    {
        // line 78
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:template3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 78,  129 => 77,  106 => 58,  92 => 47,  80 => 38,  68 => 29,  56 => 20,  45 => 12,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

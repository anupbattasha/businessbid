<?php

/* BBidsBBidsHomeBundle:Home:template2.html.twig */
class __TwigTemplate_cf5df481d30f927fba352ccb9c7dc4b64768c431decaac7ca3ab910d4880e3ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:template2.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "       <div class=\"leads_area\">
           <div class=\"inner_leads\">
               <div class=\"business_bid_work_inner_img\">
                   <img src=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile.png"), "html", null, true);
        echo " alt=\"HOW DOES BUSINESS BID WORK?\">
               </div>
               <div class=\"business_con\">
                   <h1>HOW DOES BUSINESS BID WORK?</h1>
                   <p>Simple, our objective is to help your business grow.  Just tell us about the services you offer and we’ll send you detailed customer job requests by text and email. Let us know which leads you are interested in and we’ll provide you with client contact information so you can get in touch and finalize the job. You will be able to track your success with detailed reports on the leads you have received and quoted for through your vendor account dashboard. </p> 
                   <p>At BusinessBid, we provide all the tools you need to succeed.</p>
              <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\" class=\"join_up_btn\">Join NOW</a>
               </div>
           </div>
       </div>
       <div class=\"customers_categories_area\">
           <div class=\"inner-customers-categories\">
              <div class=\"inner_customar_headding\"><h1>01</h1></div>
               <div class=\"inner-customers-categories_content\">
                   <h3>Customers tell us what they require</h3>
                    <ul>
                        <li>Customers across the UAE come to BusinessBid to find the most suitable service professionals for their project</li>
                        <li>They fill a detailed brief outlining their exact requirements</li>
                        <li>Through our vendor matching technology, the project details are sent to the most suitable service professionals by text message and e-mail</li>
                        <li>Through our vendor matching technology, the project details are sent to the most suitable service professionals by text message and e-mail</li>
                        <li>All projects are verified to make sure they are genuine.</li>
                    </ul>
               </div>
               <div class=\"inner-customers-categories_img\">
                   <img src=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/laptop.png"), "html", null, true);
        echo " alt=\"Customers tell us what they require\">
               </div>
           </div>
       </div>
       <div class=\"job_interested_area\">
           <div class=\"inner_job_interested\">
               <div class=\"job_interested_area_img\">
                   <img src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile_icon.png"), "html", null, true);
        echo " alt=\"Choose which jobs interest you\">
               </div>
               <div class=\"inner_customar_headding\"><h1>02</h1></div>
               <div class=\"job_interested_area_content\">
                   <h3>Choose which jobs interest you</h3>
                       <ul>
                        <li>Once the job brief is received, you evaluate the details to determine whether you would like to take on the project</li>
                        <li>Let us know you are interested and you will instantly receive client contact information</li>
                        <li>You only pay for leads that you are interested in. A lead is deducted from your BusinessBid lead account when you choose to accept a project</li>
                        <li>You can top up your lead account through your vendor account dashboard anytime you are running low</li>
                    </ul>                  
               </div>
           </div>
       </div>
       <div class=\"customers_categories_area\">
           <div class=\"inner-customers-categories\">
              <div class=\"inner_customar_headding\"><h1>03</h1></div>
               <div class=\"inner-customers-categories_content\">
                   <h3>Contact client and get hired</h3>
                    <ul>
                        <li>Once client contact information is received, you can call the client to understand the requirement in more detail and set an appointment if necessary</li>
                        <li>Usually, 1 or 2 other service professionals also contact the customer who chooses which quotation to finally go ahead with</li>
                        <li>You can login to your vendor account dashboard and view analytics about your performance and success with BusinessBid</li>
                    </ul>
               </div>
               <div class=\"inner_clients_categories_img\">
                   <img src=";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/pcmobile.png"), "html", null, true);
        echo " alt=\"Contact client and get hired\">
               </div>
           </div>
       </div>  
       
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
   ";
    }

    // line 79
    public function block_maincontent($context, array $blocks = array())
    {
        // line 80
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:template2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 80,  125 => 79,  106 => 64,  77 => 38,  67 => 31,  46 => 13,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

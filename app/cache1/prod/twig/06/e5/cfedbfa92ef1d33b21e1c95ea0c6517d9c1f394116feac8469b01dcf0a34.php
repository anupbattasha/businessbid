<?php

/* BBidsBBidsHomeBundle:Home:freetrailemail.html.twig */
class __TwigTemplate_06e5cfedbfa92ef1d33b21e1c95ea0c6517d9c1f394116feac8469b01dcf0a34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table>
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left;\"><b>Dear Admin,</b></p><br/>
 ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            echo " 
<p style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left;\">New Free Trial request has been initiated by  ";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "contactname", array()), "html", null, true);
            echo " </p>
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 5
        echo " 
<p class=\"MsoNormal\">
<span style=\"family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3em;text-align:left;\">BusinessBid</span>
<span style=\"font-size:11.0pt;font-family:\"Calibri\",\"sans-serif\";color:red\">
<u></u>
<u></u>
</span>
</p>
<p class=\"MsoNormal\">
<span style=\"font-size:11.0pt;font-family:\"Calibri\",\"sans-serif\";color:red\">
<u></u>
<u></u>
</span>
</p>
<p class=\"MsoNormal\">
<span style=\"font-size:11.0pt;font-family:\"Calibri\",\"sans-serif\";color:#1f497d\">
----------------------------------------------------------------------------------------------------------------------------------------
</span>
</p>
<span>
<p>
<span style=\"font-family:Calibri,sans-serif; color:red;\">
Once the BusinessBid admin initiates the Free Trial Feature then the vendor should also receive the following email.
<br>
<br>
</span>
<span style=\"font-size:11.0pt;font-family:\"Calibri\",\"sans-serif\";color:red\">
<u></u>
<u></u>
</span>
</p>
</span>
</table>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:freetrailemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 5,  29 => 4,  23 => 3,  19 => 1,);
    }
}

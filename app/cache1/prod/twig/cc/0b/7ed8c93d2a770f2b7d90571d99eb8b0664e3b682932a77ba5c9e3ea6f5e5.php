<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Others:contactusemail.html.twig */
class __TwigTemplate_cc0b7ed8c93d2a770f2b7d90571d99eb8b0664e3b682932a77ba5c9e3ea6f5e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">Dear ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ",</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\tThank you for contacting BusinessBid.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%; \">
\tOur team is looking into your request and you can expect to be contacted with either an answer to your question or a solution to the issue raised.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%;  \">
\tOur expected time-frames to respond will vary according to the severity or complexity of the matter being addressed and volume of contacts reaching us but we will usually respond within 48 hours.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%;  \">
            However we assure you that we are working to get back to you as fast as possible and to properly address your questions and concern.
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%;  \">
\tIf the matter is urgent or need help call us on 04 42 13 777.   
</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">Regards<br>BusinessBid Team</p>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Others:contactusemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}

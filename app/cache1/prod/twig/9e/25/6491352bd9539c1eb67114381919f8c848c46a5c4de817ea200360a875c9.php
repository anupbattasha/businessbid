<?php

/* BBidsBBidsHomeBundle:User:vendororders.html.twig */
class __TwigTemplate_9e256491352bd9539c1eb67114381919f8c848c46a5c4de817ea200360a875c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:vendororders.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>Orders</h1></div>
";
        // line 7
        if (((isset($context["count"]) ? $context["count"] : null) != 0)) {
            // line 8
            echo "<div class=\"row text-right page-counter\">
\t\t<div class=\"counter-box\"><span class=\"now-show\">Now showing : ";
            // line 9
            echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
            echo " records</span></div>
\t</div>
\t";
        }
        // line 12
        echo "<div>
\t";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 14
            echo "
\t<div class=\"alert alert-danger\">";
            // line 15
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
\t";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 20
            echo "
\t<div class=\"alert alert-success\">";
            // line 21
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
\t";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
\t<div class=\"alert alert-success\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "</div>

<div class=\"table-wrapper\">
<table class=\"vendor-enquiries\">
<thead><tr><th>Date Purchased</th><th>Order Number</th>
<!--<th>Leads</th>-->
<th>Amount(AED)</th><th>Payment Type</th><th>Status</th></tr></thead>
";
        // line 37
        if ( !twig_test_empty((isset($context["orders"]) ? $context["orders"] : null))) {
            // line 38
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
                // line 39
                echo "
        <tr>
        <td>";
                // line 41
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["order"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td>
        <td><a href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_order_view", array("id" => $this->getAttribute($context["order"], "id", array()))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "ordernumber", array()), "html", null, true);
                echo "</a></td>


        <td>";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "amount", array()), "html", null, true);
                echo "</td>
        <td>";
                // line 46
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "payoption", array()), "html", null, true);
                echo "</td>
        <td>";
                // line 47
                if (($this->getAttribute($context["order"], "status", array()) == 0)) {
                    echo " Not Approved ";
                } elseif (($this->getAttribute($context["order"], "status", array()) == 1)) {
                    echo " Approved  ";
                } elseif (($this->getAttribute($context["order"], "status", array()) == 3)) {
                    echo " Pending ";
                }
                echo "</td>
        </tr>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "
";
        } else {
            // line 53
            echo "<tr align=\"center\"><td colspan=\"3\" style=\"text-align:center; width:100%;\">Orders Not Available</td></tr>
";
        }
        // line 55
        echo "</table>
";
        // line 56
        if (((isset($context["count"]) ? $context["count"] : null) != 0)) {
            // line 57
            echo "<div class=\"row text-right top-spce\">
\t\t<div class=\"counter-box\">";
            // line 58
            $context["Epage_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 4)));
            // line 59
            echo "\t\t<ul class=\"pagination\">
\t\t  <li><a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders", array("offset" => 1));
            echo "\">&laquo;</a></li>
\t\t";
            // line 61
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 62
                echo "\t\t  <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders", array("offset" => $context["i"])), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "</a></li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "\t\t  <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders", array("offset" =>  -(isset($context["Epage_count"]) ? $context["Epage_count"] : null))), "html", null, true);
            echo "\">&raquo;</a></li>
\t\t</ul>
\t\t</div>
\t</div>
</div>
";
        }
        // line 70
        echo "<div class=\"table-wrapper\">
<div class=\"panel-title\"><h2>FreeTrail Orders</h2></div>
<table class=\"vendor-enquiries\">
<thead><tr><th>Order Date</th><th>Order Number</th><th>No:Leads</th><th>Duration</th><th>Status</th><th>Order Accepted date</th></tr></thead>
";
        // line 74
        if ( !twig_test_empty((isset($context["freepack"]) ? $context["freepack"] : null))) {
            // line 75
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["freepack"]) ? $context["freepack"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
                // line 76
                echo "
        <tr><td>";
                // line 77
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["f"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td><td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["f"], "orderno", array()), "html", null, true);
                echo "</a></td><td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["f"], "leads", array()), "html", null, true);
                echo "</td><td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["f"], "duration", array()), "html", null, true);
                echo " days</td><td>";
                if (($this->getAttribute($context["f"], "status", array()) == 0)) {
                    echo " Not Approved ";
                } elseif (($this->getAttribute($context["f"], "status", array()) == 1)) {
                    echo " Approved </a>";
                }
                echo "</td><td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["f"], "updated", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td></tr>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 81
            echo "<tr><td align=\"center\">Orders Not Available</td></tr>
";
        }
        // line 83
        echo "
</table>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:vendororders.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 83,  250 => 81,  226 => 77,  223 => 76,  219 => 75,  217 => 74,  211 => 70,  201 => 64,  190 => 62,  186 => 61,  182 => 60,  179 => 59,  177 => 58,  174 => 57,  172 => 56,  169 => 55,  165 => 53,  161 => 51,  145 => 47,  141 => 46,  137 => 45,  129 => 42,  125 => 41,  121 => 39,  117 => 38,  115 => 37,  106 => 30,  97 => 27,  94 => 26,  90 => 25,  87 => 24,  78 => 21,  75 => 20,  71 => 19,  68 => 18,  59 => 15,  56 => 14,  52 => 13,  49 => 12,  41 => 9,  38 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:global_search.html.twig */
class __TwigTemplate_533e37f26cb01b326c3c746fd2fe290581370606e3513eca774d9e06eef61a6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:global_search.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/globalsearch/styles.css"), "html", null, true);
        echo "\" />


<div id=\"contact\" class=\"contact-detail\">
\t<div class=\"page-bg\">
\t<div class=\"container\">
\t\t<div class=\"Create-ticket-form\">
\t\t<div class=\"page-title\"><h1>Site Search</h1></div>
\t\t<form id=\"searchForm\" method=\"post\">
\t\t\t\t<fieldset>
\t\t\t\t\t<input id=\"s\" type=\"text\" value=\"";
        // line 18
        if ( !twig_test_empty((isset($context["keyword"]) ? $context["keyword"] : null))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["keyword"]) ? $context["keyword"] : null), "html", null, true);
            echo " ";
        }
        echo "\"/>
\t\t\t\t\t<input type=\"submit\" value=\"Submit\" id=\"submitButton\" />
\t\t\t\t\t
\t\t\t\t\t\t<!-- <div id=\"searchInContainer\">
\t\t\t\t\t\t\t<input type=\"radio\" name=\"check\" value=\"site\" id=\"searchSite\" checked />
\t\t\t\t\t\t\t<label for=\"searchSite\" id=\"siteNameLabel\">Search</label>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- <input type=\"radio\" name=\"check\" value=\"web\" id=\"searchWeb\" style=\"display:none;\" /> -->
\t\t\t\t\t\t\t<!-- <label for=\"searchWeb\">Search The Web</label> -->
\t\t\t\t\t<ul class=\"icons\" style=\"display:none;\">
\t\t\t\t\t\t<li class=\"web\" title=\"Web Search\" data-searchType=\"web\">Web</li>
\t\t\t\t\t\t<li class=\"images\" title=\"Image Search\" data-searchType=\"images\">Images</li>
\t\t\t\t\t\t<li class=\"news\" title=\"News Search\" data-searchType=\"news\">News</li>
\t\t\t\t\t\t<li class=\"videos\" title=\"Video Search\" data-searchType=\"video\">Videos</li>
\t\t\t\t\t</ul>
\t\t\t\t</fieldset>
\t\t</form>
    \t<div id=\"resultsDiv\"></div>

    </div>
\t</div>
\t</div>  
</div>
    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js\"></script>
\t<script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/globalsearch/script.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:global_search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 42,  54 => 18,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Vendor:activateemailstatus.html.twig */
class __TwigTemplate_ac1dc5825603d0ddbc8c8ac85def70cff37df8a79f3acc01a420cab2290a5080 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
\t\t";
        // line 3
        if (((isset($context["useraccount"]) ? $context["useraccount"] : null) == 2)) {
            // line 4
            echo "\t\t\t<table width=\"100%\" bgcolor=\"#939598\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
\t\t\t\t<tr>
\t\t\t\t\t<td colspan=\"2\" align=\"center\" valign=\"middle\" bgcolor=\"#939598\">
\t\t\t\t\t<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>&nbsp;</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td height=\"23\" bgcolor=\"#f36e23\">&nbsp;</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td align=\"center\" bgcolor=\"#FFFFFF\"><table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td align=\"center\" style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; \">
\t\t\t\t\t\t\t\t<p style=\"font-size:24px; margin-bottom:10px; font-weight:600; color:#1e7dc0;\">
\t\t\t\t\t\t\t\t\tWelcome to the BusinessBid Network.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
\t\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\t\t\t\t\t\t\t\t\tBusinessBid is UAE’s most extensive service provisioning portal visited by thousands of customer’s every day. 
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\t\t\t\t\t\t\t\t\tBusinessBid has access to customers who are looking to engage vendors all over the UAE for their project needs.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\t\t\t\t\t\t\t\t\tWith your own BusinessBid account now, you can be assured that you have access to qualified customer leads thus generating a reliable and easy source of new business generation.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<!-- <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">With your own BusinessBid account now, you can be assured that you have access to qualified customer leads thus generating a reliable and easy source of new business generation.BusinessBid has access to customers who are looking to engage vendors all over the UAE for their project needs.</p> -->
\t\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\t\t\t\t\t\t\t\t\tPlease ensure you set up your dashboard and profile to reflect your preferences and business needs.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\t\t\t\t\t\t\t\t\tAnd of course, if you have any questions or need help call us on 04 42 13 777.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t
\t\t\t\t\t\t";
        } else {
            // line 44
            echo "\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; float:left; margin-bottom:10px; width:100%\">
\t\t\t\t\t\t\t\tOur records indicate that you have recently changed your password. 
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
\t\t\t\t\t\t\t\tIf you believe that it was not you, please report this suspicious activity to BusinessBid on<a>support@businessbid.ae</a>or call us on 04 42 13 777.
\t\t\t\t\t\t\t</p>

\t\t\t\t\t\t";
        }
        // line 52
        echo "\t\t\t\t\t</tr>
\t\t\t\t   
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td align=\"center\">
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">
\t\t\t\t\t\t\t\tRegards<br>BusinessBid Team
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>&nbsp;</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td bgcolor=\"#eeeeee\">&nbsp;</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td bgcolor=\"#eeeeee\">&nbsp;</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td bgcolor=\"#939598\">&nbsp;</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td bgcolor=\"#939598\">&nbsp;</td>
\t\t</tr>
</table>

    </td>
  </tr>
  </table>
</span>
</p>
</p>
</html>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Vendor:activateemailstatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 52,  67 => 44,  25 => 4,  23 => 3,  19 => 1,);
    }
}

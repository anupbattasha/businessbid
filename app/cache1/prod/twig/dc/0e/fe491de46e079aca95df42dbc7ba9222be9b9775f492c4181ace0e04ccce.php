<?php

/* BBidsBBidsHomeBundle:User:vendorupdate.html.twig */
class __TwigTemplate_dc0efe491de46e079aca95df42dbc7ba9222be9b9775f492c4181ace0e04ccce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:vendorupdate.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script>
\$(document).ready(function(){

    \$('#form_albumimages').change(function(e,data){
        var inp = \$(this).get(0);
        var uploadErrors = [];
        var acceptFileTypes = /^image\\/(gif|jpe?g|png)\$/i;
        for (var i = 0; i < inp.files.length; ++i) {
          var name = inp.files.item(i).name;
          var fileType = inp.files.item(i).type;
          var fileSize = inp.files.item(i).size;
            if(!acceptFileTypes.test(fileType)) {
                uploadErrors.push('File No.'+ (i+1) +' is not an accepted file type');
            }
            if(fileSize > 5000000) {
                uploadErrors.push('Filesize is too big');
            }
          // console.log(\"here is a file name: \" + name);
        }
        if(uploadErrors.length > 0) {
            alert(uploadErrors.join(\"\\n\"));
            \$(this).val('');
            return false;
        }
        return true;
    });

\tvar max_fields      = 10; //maximum input boxes allowed
    var wrapper         = \$(\".products\"); //Fields wrapper
    var add_button      = \$(\".add_products_button\"); //Add button ID

    var x = 1; //initlal text box count
    \$(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            \$(\"#moreproducts\").append('<div class=\"morecertificates\"><input type=\"text\" name=\"product[]\"/><a href=\"#\" class=\"remove_field\">Remove</a></div>'); //add input box
        }
    });

    \$(wrapper).on(\"click\",\".remove_field\", function(e){ //user click on remove text
        e.preventDefault(); \$(this).parent('div').remove(); x--;
    });

    var max_fields_cerificates      = 5; //maximum input boxes allowed
    var wrapper_cerificates         = \$(\".certificates\"); //Fields wrapper
    var add_button_cerificates      = \$(\".add_certificate_button\"); //Add button ID

    var x_cerificates = 1; //initlal text box count
    \$(add_button_cerificates).click(function(e){ //on add input button click
        e.preventDefault();
        if(x_cerificates < max_fields_cerificates){ //max input box allowed
            x_cerificates++; //text box increment
            \$(\"#morecertificates\").append('<div class=\"morecertificates\"><input type=\"text\" name=\"certification[]\"/><div class=\"upload-button\"><input type=\"file\" name=\"certificationfile[]\"/></div><a href=\"#\" class=\"remove_field\">Remove</a></div>'); //add input box
        }
    });

    \$(wrapper_cerificates).on(\"click\",\".remove_field\", function(e){ //user click on remove text
        e.preventDefault(); \$(this).parent('div').remove(); x_cerificates--;
    });


\$('#logo-form').hide();
\$('.albumform').hide();
\t\$('#changeimage').click(function(){

\t\t\$('#logo-form').show();
\t});
\t\$('#newalbum').click(function(){
\t\t\$('.albumform').show(); });

\t});

</script>
<script type=\"text/javascript\">
function deleteEntity(entity,id,vid)
{
    if(entity==\"certification\")
    {
        var con = confirm(\"Are You sure to delete the Certification\");
        if(con)
        {
            var url = \"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_certification", array("id" => "cid", "vid" => "vid")), "html", null, true);
        echo "\";
            var urlset = url.replace('cid', id);
            urlset = urlset.replace('vid', vid);
            window.open(urlset,\"_self\");
            return true;
        }
        else
        {
            return false;
        }
    }
    else if(entity==\"product\")
    {
        var con = confirm(\"Are You sure to delete the Product\");
        if(con)
        {
            var url = \"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_product", array("id" => "pid", "vid" => "vid")), "html", null, true);
        echo "\";
            var urlset = url.replace('pid', id);
            urlset = urlset.replace('vid', vid);
            window.open(urlset,\"_self\");
            return true;
        }
        else
        {
            return false;

        }
    }

    else if(entity==\"license\")
    {
        var con = confirm(\"Are You sure to delete the License\");
        if(con)
        {
            var url = \"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_lisence", array("id" => "eid", "vid" => "vid")), "html", null, true);
        echo "\";
            var urlset = url.replace('eid', id);
            urlset = urlset.replace('vid', vid);
            window.open(urlset,\"_self\");
            return true;
        }
        else
        {
            return false;
        }
    }

    else if(entity==\"album\")
    {
        var con = confirm(\"Are You sure to delete the Album\");
        if(con)
        {
            var url = \"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_unlink_album", array("id" => "eid", "vid" => "vid")), "html", null, true);
        echo "\";
            var urlset = url.replace('eid', id);
            urlset = urlset.replace('vid', vid);
            window.open(urlset,\"_self\");
            return true;
        }
        else
        {
            return false;
        }
    }
}
</script>
<script>
  \$(function() {
    \$( \"#datepicker3\" ).datepicker({
\t\tdateFormat: 'yy-mm-dd',
\t\tchangeMonth: true,
\t\tmaxDate: '+2y',
\t\tyearRange: '2014:2024' });
  });
  </script>
<div class=\"col-md-9 dashboard-rightpanel user-profile-basic\">
\t<div class=\"page-title\"><h1>My Profile</h1>\t</div>
\t<div class=\"row user-profile-block\">
\t\t<div class=\"user-profile-basic\">
\t\t\t<div class=\"profile-inform-image\">
\t\t\t";
        // line 164
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["accountArray"]) ? $context["accountArray"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 165
            echo "\t\t\t\t<div class=\"profile-image\">";
            $context["image"] = $this->getAttribute($context["a"], "logopath", array());
            // line 166
            echo "\t\t\t\t";
            $context["logopath"] = ("http://115.124.120.36/web/logo/" . (isset($context["image"]) ? $context["image"] : null));
            // line 167
            echo "\t\t\t\t</div>
\t\t\t\t";
            // line 168
            if ( !twig_test_empty((isset($context["image"]) ? $context["image"] : null))) {
                // line 169
                echo "\t\t\t\t<div class=\"profile-picture\"><img src=\"";
                echo twig_escape_filter($this->env, (isset($context["logopath"]) ? $context["logopath"] : null), "html", null, true);
                echo "\" width=\"250px\"><a href=\"#\" id=\"changeimage\">Change Profile Image</a></div>
\t\t\t\t";
            } else {
                // line 171
                echo "\t\t\t\t<div class=\"profile-picture\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/no-image.png"), "html", null, true);
                echo "\" ><a href=\"#\" id=\"changeimage\">Add Profile Image</a>
\t\t\t\t</div>
\t\t\t\t";
            }
            // line 174
            echo "
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 176
        echo "\t\t\t<div class=\"profile-name\">
\t\t\t\t<h1>";
        // line 177
        echo twig_escape_filter($this->env, (isset($context["bizname"]) ? $context["bizname"] : null), "html", null, true);
        echo "</h1>
\t\t\t\t<div id=\"logo-form\">

\t\t\t\t\t<!--<form name=\"logoform\" method=\"post\" enctype=\"multipart/form-data\" action=\"/vendorlogo.php\">

\t\t\t\t\t\t<div class=\"col-md-7\"><input type=\"file\" name=\"vendorlogo\"/><input type=\"hidden\" name=\"vendorid\" value=\"";
        // line 182
        echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
        echo "\"></div>

\t\t\t\t\t\t<div class=\"col-md-5\"><input type=\"submit\" name=\"submit\" class='btn btn-success btn-getquotes text-center' value=\"Upload\"></div>
\t\t\t\t\t</form>-->
\t\t\t\t\t";
        // line 186
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["logoform"]) ? $context["logoform"] : null), 'form_start');
        echo "
\t\t\t\t\t<div class=\"col-md-7\">";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["logoform"]) ? $context["logoform"] : null), "image", array()), 'widget');
        echo "</div>
\t\t\t\t\t<div class=\"col-md-5\">";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["logoform"]) ? $context["logoform"] : null), "Upload", array()), 'widget');
        echo "</div>
\t\t\t\t\t";
        // line 189
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["logoform"]) ? $context["logoform"] : null), 'form_end');
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t\t</div>

\t\t\t<ul class=\"nav nav-tabs\" role=\"tablist\">
\t\t\t  <li class=\"active profile-icon\"><a href=\"#profile\" role=\"tab\" data-toggle=\"tab\">Information</a></li>
\t\t\t  <li class=\"certificate-icon\"><a href=\"#certificates\" role=\"tab\" data-toggle=\"tab\">Certifications</a></li>
\t\t\t  <li class=\"products-icon\"><a href=\"#products\" role=\"tab\" data-toggle=\"tab\">Products</a></li>
\t\t\t  <li class=\"album-icon\"><a href=\"#license\" role=\"tab\" data-toggle=\"tab\">Business License</a></li>
\t\t\t  <li class=\"album-icon\"><a href=\"#album\" role=\"tab\" data-toggle=\"tab\">Album</a></li>
\t\t\t   <li class=\"address-icon\"><a href=\"#address\" role=\"tab\" data-toggle=\"tab\">Service Area</a></li>
\t\t\t   <li class=\"address-icon\"><a href=\"#portfolio\" role=\"tab\" data-toggle=\"tab\">Service Portfolio</a></li>
\t\t\t</ul>

<!-- Tab panes -->
<div class=\"tab-content\">
  <div class=\"tab-pane active profile-inform\" id=\"profile\">

  ";
        // line 208
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 209
            echo "
\t\t\t<div class=\"alert alert-danger\">";
            // line 210
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 213
        echo "
\t";
        // line 214
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 215
            echo "
\t\t<div class=\"alert alert-success\">";
            // line 216
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 219
        echo "
\t";
        // line 220
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 221
            echo "
\t<div class=\"alert alert-success\">";
            // line 222
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 225
        echo "  \t";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["profileform"]) ? $context["profileform"] : null), 'form_start');
        echo "
\t";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["profileform"]) ? $context["profileform"] : null), 'errors');
        echo "
\t\t<div class=\"field-item col-sm-6\">";
        // line 227
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "bizname", array()), 'label');
        echo "<span class=\"req\">*</span> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "bizname", array()), 'widget');
        echo "</div>
\t\t<div class=\"field-item col-sm-6\">";
        // line 228
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "contactname", array()), 'label');
        echo "<span class=\"req\">*</span> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "contactname", array()), 'widget');
        echo "</div>
\t\t<div class=\"field-item col-sm-6\">";
        // line 229
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "mobilecode", array()), 'label');
        echo "<span class=\"req\">*</span> <div class=\"control\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "mobilecode", array()), 'widget');
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "smsphone", array()), 'widget');
        echo "</div></div>
\t\t<div class=\"field-item col-sm-6\">";
        // line 230
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "homecode", array()), 'label');
        echo "<div class=\"control\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "homecode", array()), 'widget');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "homephone", array()), 'widget');
        echo "</div></div>
\t\t<!--<div class=\"field-item col-sm-6\">";
        // line 231
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "address", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "address", array()), 'widget');
        echo "</div>-->
\t\t<div class=\"field-item col-sm-6\">";
        // line 232
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "fax", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "fax", array()), 'widget');
        echo "</div>
\t\t<div class=\"field-item-full col-sm-6\">";
        // line 233
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "description", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "description", array()), 'widget');
        echo "</div>
\t\t<div class=\"field-item-save\">";
        // line 234
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["profileform"]) ? $context["profileform"] : null), "save", array()), 'widget');
        echo "</div>
\t";
        // line 235
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["profileform"]) ? $context["profileform"] : null), 'form_end');
        echo "
  </div>
  <div class=\"tab-pane\" id=\"address\">
  \t";
        // line 238
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cityform"]) ? $context["cityform"] : null), 'form_start', array("attr" => array("class" => "city_form")));
        echo "
  \t<ul>
\t\t";
        // line 240
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["cityform"]) ? $context["cityform"] : null), "city", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 241
            echo "\t\t
\t\t\t<li class=\"city-field-item\">
\t\t\t\t";
            // line 243
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["a"], 'widget');
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["a"], 'label');
            echo "
\t\t\t</li>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 246
        echo "\t\t\t<li class=\"city-field-item\">
\t\t\t\t<input type=\"checkbox\" id=\"checkcity\" name=\"checkcity\" value=\"all\"><label>All of UAE</label>
\t\t\t</li>
\t\t\t";
        // line 249
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["cityform"]) ? $context["cityform"] : null), "save", array()), 'widget', array("attr" => array("class" => "save_city")));
        echo "
\t\t</ul>
\t\t
\t\t\t

\t";
        // line 254
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["cityform"]) ? $context["cityform"] : null), 'form_end');
        echo "
  </div>
\t<div class=\"tab-pane certificate\" id=\"certificates\">
\t\t<div class=\"list-certificate col-sm-7\">
\t\t\t";
        // line 258
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["certifications"]) ? $context["certifications"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 259
            echo "\t\t\t\t<p><button type=\"button\" class=\"close\" title=\"Delete\"  aria-hidden=\"true\" onclick = \"return deleteEntity('certification','";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\" >&times;</button> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "certification", array()), "html", null, true);
            echo "</p>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 261
        echo "\t\t</div>
\t\t<div class=\"add-certificate col-sm-5\">
\t\t\t<h2>Add Your Certifications</h2>
\t\t\t<div class=\"add-certificate-block certificates\">
\t\t\t\t<form name=\"form\" method=\"post\" action=\"";
        // line 265
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_save_certification");
        echo "\" onsubmit=\"return certificateValidate();\" enctype=\"multipart/form-data\">
\t\t\t\t\t<span class=\"file-upload-wrapper\">
\t\t\t\t\t\t<input type=\"text\" id=\"form_certification\" name=\"certification[]\">
\t\t\t\t\t\t<div class=\"upload-button\"><input type=\"file\" name=\"certificationfile[]\"></div>
\t\t\t\t\t</span>
\t\t\t\t\t<span>

\t\t\t\t\t</span>
\t\t\t\t\t<div id=\"morecertificates\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"pull-right\"><a class=\"add_certificate_button\"> add more certificates <span class=\"glyphicon glyphicon-plus\"></span></a></div>
\t\t\t\t\t<h5>Industry certifications are always an advantage for:</h5>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li>Proving you are genuine.</li>
\t\t\t\t\t\t<li>Being recognised as a specialist vendor within your industry</li>
\t\t\t\t\t\t<li>Attracting more customers.</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<button type=\"submit\" id=\"form_save\" name=\"form[save]\">Add Certificate</button>
\t\t\t\t</form>
\t\t\t\t";
        // line 290
        echo "\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"tab-pane\" id=\"products\">
\t\t<div class=\"list-product\">
\t\t\t<div class=\"list-certificate col-sm-7\">
\t\t\t\t";
        // line 296
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 297
            echo "\t\t\t\t\t<p><button type=\"button\" title=\"Delete\" class=\"close\"  aria-hidden=\"true\" onclick = \"return deleteEntity('product','";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\" >&times;</button>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "product", array()), "html", null, true);
            echo " </p>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 299
        echo "\t\t\t</div>
\t\t\t<div class=\"add-certificate col-sm-5\">
\t\t\t\t<h2>Add Your Products</h2>
\t\t\t\t<form name=\"form\" method=\"post\" action=\"";
        // line 302
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_save_product");
        echo "\" onsubmit=\"return productValidate();\">
\t\t\t\t\t<div class=\"add-certificate-block products\">
\t\t\t\t\t\t<span class=\"file-upload-wrapper\">
\t\t\t\t\t\t<input type=\"text\" id=\"form_product\" name=\"product[]\">
\t\t\t\t\t\t</span>
\t\t\t\t\t\t<span>

\t\t\t\t\t\t</span>
\t\t\t\t\t\t<div id=\"moreproducts\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"pull-right\"><a class=\"add_certificate_button add_products_button\"> add more products <span class=\"glyphicon glyphicon-plus\"></span></a></div>
\t\t\t\t\t\t<h5>Upload any in-house products or recommended products you work with.</h5><br>

\t\t\t\t\t\t<button type=\"submit\" id=\"form_save\" name=\"form[save]\" >Product &amp; Services</button>
\t\t\t\t\t\t";
        // line 326
        echo "\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>

\t\t</div>
\t</div>
  <div class=\"tab-pane\" id=\"license\">
  \t\t<div class=\"list-certificate col-sm-7\">
\t\t\t\t";
        // line 334
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["expLicense"]) ? $context["expLicense"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 335
            echo "\t\t\t\t\t<div> License Expiry Date<p>
                        <button type=\"button\" title=\"Delete\" class=\"close\"  aria-hidden=\"true\" onclick = \"return deleteEntity('license','";
            // line 336
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\" >&times;</button>";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["e"], "licenseName", array()) . " - ") . twig_date_format_filter($this->env, $this->getAttribute($context["e"], "expdate", array()), "Y-m-d")), "html", null, true);
            echo "<div>License:<span><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/licenses"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "file", array()), "html", null, true);
            echo "\">Click here to download the license</a></span></div></p></div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 338
        echo "\t\t</div>
\t\t<div class=\"add-certificate col-sm-5\">
\t\t\t\t<h2>Add Your Business License</h2>
\t\t\t\t<div class=\"newalbum add-certificate-block\">
\t\t\t\t\t";
        // line 342
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["licenseform"]) ? $context["licenseform"] : null), 'form_start');
        echo "
                            ";
        // line 343
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "name", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "name", array()), 'widget');
        echo "
\t\t\t\t\t\t\t";
        // line 344
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "expirydate", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "expirydate", array()), 'widget', array("id" => "datepicker3"));
        echo "
\t\t\t\t\t\t\t";
        // line 345
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "licensefile", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "licensefile", array()), 'widget');
        echo "
\t\t\t\t\t\t\t<h5>\"Your license provides credibility and authenticity of your business.  Upon verification we will display your license on your profile.\"</h5>
\t\t\t\t\t\t\t";
        // line 347
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["licenseform"]) ? $context["licenseform"] : null), "save", array()), 'widget');
        echo "
\t\t\t\t\t\t";
        // line 348
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["licenseform"]) ? $context["licenseform"] : null), 'form_end');
        echo "
\t\t\t\t</div>

\t\t</div>
  </div>
  <div class=\"tab-pane\" id=\"album\">
  \t";
        // line 354
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 355
            echo "\t\t\t";
            $context["albumid"] = $this->getAttribute($context["a"], "albumid", array());
            // line 356
            echo "\t\t\t";
            $context["defaultphotoid"] = $this->getAttribute($context["a"], "defaultphotopath", array());
            // line 357
            echo "\t\t\t";
            $context["imagepath"] = (((("http://115.124.120.36/stagging/web/gallery/" . (isset($context["albumid"]) ? $context["albumid"] : null)) . "/") . (isset($context["albumid"]) ? $context["albumid"] : null)) . $this->getAttribute($context["loop"], "index", array()));
            // line 358
            echo "\t\t\t<div class=\"photo\">
                <button type=\"button\" title=\"Delete Album\" class=\"close\" onclick = \"return deleteEntity('album','";
            // line 359
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
            echo "');\">&times;</button>
                <a target=\"_blank\" href=\"";
            // line 360
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_album_view", array("albumid" => (isset($context["albumid"]) ? $context["albumid"] : null))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["imagepath"]) ? $context["imagepath"] : null), "html", null, true);
            echo "\" width=\"90px\"></a>
\t\t\t\t<p>";
            // line 361
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "albumname", array()), "html", null, true);
            echo " </p><p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "photocount", array()), "html", null, true);
            echo " Photo(s) </p><p>";
            echo "</p>
\t\t\t</div>
\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 364
        echo "\t\t<div class=\"newalbum\">
\t\t\t<div><button id=\"newalbum\">New Album</button></div>
\t\t\t<div class=\"albumform\">
\t\t\t";
        // line 367
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["albumform"]) ? $context["albumform"] : null), 'form_start', array("attr" => array("onsubmit" => "return albumValidate();")));
        echo "
                    ";
        // line 368
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumname", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumname", array()), 'widget');
        echo "
                    ";
        // line 369
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "description", array()), 'label');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "description", array()), 'widget');
        echo "
\t\t\t\t\t";
        // line 370
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumimages", array()), 'widget', array("full_name" => ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "albumimages", array()), "vars", array()), "full_name", array()) . "[]")));
        echo "
\t\t\t\t\t";
        // line 371
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["albumform"]) ? $context["albumform"] : null), "save", array()), 'widget');
        echo "
\t\t\t\t";
        // line 372
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["albumform"]) ? $context["albumform"] : null), 'form_end');
        echo "
\t\t\t</div>
\t\t</div>
  </div>
  
  <div class=\"tab-pane\" id=\"portfolio\">
\t\t<div class=\"pane-title\"><label>Services Provided</label></div>
\t\t<div class=\"sub-pane-title\"><label>List the services you provide</label></div>
\t\t<div class=\"\">
\t\t\t<form action=\"";
        // line 381
        echo $this->env->getExtension('routing')->getPath("bizbids_addcat_port");
        echo "\" method=\"post\">
\t\t\t\t<div class=\"morecertificates subcatadd\">
\t\t\t\t\t<input type=\"text\" id=\"form_cat\" name=\"cat[]\" placeholder=\"Enter your category\">\t
\t\t\t\t</div>
\t\t\t\t<a id=\"addform1\">Add</a>
\t\t\t\t<div id=\"moreproducts1\">
\t\t\t\t</div>
\t\t\t\t<input type=\"hidden\" id=\"vendorid\" name=\"vendorid\" value=\"";
        // line 388
        echo twig_escape_filter($this->env, (isset($context["vendorid"]) ? $context["vendorid"] : null), "html", null, true);
        echo "\">
\t\t\t\t<input type=\"hidden\" id=\"setdata\" value=\"0\">
\t\t\t\t<input type=\"submit\" id=\"savecat\" name=\"form[save]\" value=\"Save\">
\t\t\t</form>
\t\t</div>
\t\t";
        // line 393
        if ( !twig_test_empty((isset($context["maincat"]) ? $context["maincat"] : null))) {
            // line 394
            echo "\t\t<div class=\"field-item1\">
\t\t\t<label style=\"margin: 10px 10px;  width: 100%;  float: left;\">Service Category</label>
\t\t\t";
            // line 396
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["maincat"]) ? $context["maincat"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                // line 397
                echo "\t\t\t\t<div class=\"editcategory-port\">
\t\t\t\t\t";
                // line 398
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "category", array()), "html", null, true);
                echo "
\t\t\t\t</div>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 400
            echo " 
\t\t</div>     
\t\t";
        }
        // line 403
        echo "\t\t";
        if ( !twig_test_empty((isset($context["displaycat"]) ? $context["displaycat"] : null))) {
            // line 404
            echo "\t\t\t<div class=\"field-item1\"><label>Service Portfolio</label>
\t\t\t\t";
            // line 405
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["displaycat"]) ? $context["displaycat"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                // line 406
                echo "\t\t\t\t<div class=\"main-cat\">
\t\t\t\t\t<div class=\"field-value editcategory-port\" id=\"";
                // line 407
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t<span class=\"catname\">";
                // line 408
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "category", array()), "html", null, true);
                echo " </span>
\t\t\t\t\t</div>
\t\t\t\t\t<span id=\"editdt\" >
\t\t\t\t\t\t<input id=\"vendid\" type=\"hidden\" value=\"";
                // line 411
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t<span class=\"editcatname-port\"><a href=\"#editdt-port\" id=\"editcat-port";
                // line 412
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
                echo "\" class=\"editcat-port\">Edit</a>  </span>
\t\t\t\t\t\t<span class=\"deletecatname-port\"><a href=\"";
                // line 413
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("bizbids_deletecat_port", array("id" => $this->getAttribute($context["a"], "id", array()))), "html", null, true);
                echo "\">Delete</a> </span> 
\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 417
            echo "\t\t\t</div>
\t\t";
        }
        // line 419
        echo "\t</div>

  </div>
</div>
<!-- Tab panes ending -->
\t\t</div>
\t</div>
</div>
<script type=\"text/javascript\">
function albumValidate()
{
    var isValid = true;
    var albumName = \$('#form_albumname');
    if(albumName.val() == '') {
        albumName.css('border', '3px solid red');
        albumName.focus();
        isValid = false;
    } else {
        albumName.css('border', '3px solid green');
    }
    return isValid;
}

function productValidate() {
\tvar isValid = true;
\t\$('input[name=\"product[]\"]').each(function() {
\t\tvar aValue = \$(this).val();
\t\tconsole.log(aValue);
\t\tif(aValue == '') {
\t\t\talert('All product fields should be filled.');
\t\t\t\$(this).focus();
\t\t\tisValid = false;
\t\t}
\t});
\treturn isValid;
};

function certificateValidate() {
\tvar isValid = true;
\t\$('input[name=\"certification[]\"]').each(function() {
\t\tvar aValue = \$(this).val();
\t\tif(aValue == '') {
\t\t\t\$(this).css('border', '3px solid red');
\t\t\t\$(this).focus();
\t\t\tisValid = false;
\t\t} else {
\t\t\t\$(this).css('border', '3px solid green');
\t\t}
\t});
\tif(isValid == true)
\t{
\t\t\$('input[name=\"certificationfile[]\"]').each(function() {
\t\t\tvar aValue    = \$(this).val();
\t\t\tvar extension = [\"jpg\",\"jpeg\",\"gif\",\"png\"];
\t\t\tvar ext       =aValue.substring(aValue.lastIndexOf('.')+1);
\t\t\tvar flag=0;
\t\t\tfor(i=0;i<extension.length;i++)
\t\t\t{
\t\t\t\tif(ext==extension[i])
\t\t\t\t{
\t\t\t\t\tflag=0;
\t\t\t\t\tbreak;
\t\t\t\t}
\t\t\t\telse
\t\t\t\t{
\t\t\t\t\tflag=1;
\t\t\t\t}
\t\t\t}
\t\t\tif(flag!=0) {
\t\t\t\talert('File type not allowed');
\t\t\t\t\$(this).focus();
\t\t\t\t\$(this).css('border', '3px solid red');
\t\t\t\tisValid = false;
\t\t\t\treturn isValid;
\t\t\t}
\t\t\telse if(aValue == '') {
\t\t\t\t\$(this).focus();
\t\t\t\t\$(this).css('border', '3px solid red');
\t\t\t\tisValid = false;
\t\t\t\treturn isValid;
\t\t\t} else {
\t\t\t\t\$(this).css('border', '3px solid green');
\t\t\t}
\t\t});
\t}
\treturn isValid;
};
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:vendorupdate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  812 => 419,  808 => 417,  798 => 413,  794 => 412,  790 => 411,  784 => 408,  780 => 407,  777 => 406,  773 => 405,  770 => 404,  767 => 403,  762 => 400,  753 => 398,  750 => 397,  746 => 396,  742 => 394,  740 => 393,  732 => 388,  722 => 381,  710 => 372,  706 => 371,  702 => 370,  696 => 369,  690 => 368,  686 => 367,  681 => 364,  661 => 361,  655 => 360,  649 => 359,  646 => 358,  643 => 357,  640 => 356,  637 => 355,  620 => 354,  611 => 348,  607 => 347,  600 => 345,  594 => 344,  588 => 343,  584 => 342,  578 => 338,  562 => 336,  559 => 335,  555 => 334,  545 => 326,  528 => 302,  523 => 299,  510 => 297,  506 => 296,  498 => 290,  476 => 265,  470 => 261,  457 => 259,  453 => 258,  446 => 254,  438 => 249,  433 => 246,  423 => 243,  419 => 241,  415 => 240,  410 => 238,  404 => 235,  400 => 234,  394 => 233,  388 => 232,  382 => 231,  374 => 230,  367 => 229,  361 => 228,  355 => 227,  351 => 226,  346 => 225,  337 => 222,  334 => 221,  330 => 220,  327 => 219,  318 => 216,  315 => 215,  311 => 214,  308 => 213,  299 => 210,  296 => 209,  292 => 208,  270 => 189,  266 => 188,  262 => 187,  258 => 186,  251 => 182,  243 => 177,  240 => 176,  233 => 174,  226 => 171,  220 => 169,  218 => 168,  215 => 167,  212 => 166,  209 => 165,  205 => 164,  175 => 137,  155 => 120,  134 => 102,  115 => 86,  31 => 4,  28 => 3,  11 => 1,);
    }
}

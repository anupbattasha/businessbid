<?php

/* TicketingTicketTicketBundle:Admin:customer_ticket_list.html.twig */
class __TwigTemplate_7f859bf50067560a885149bf0d57c7c9dc99c131ab18e4f0d610b0a9852f380b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "TicketingTicketTicketBundle:Admin:customer_ticket_list.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
            'dashboard' => array($this, 'block_dashboard'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\">
<link rel=\"stylesheet\" href=\"//jquery-datatables-column-filter.googlecode.com/svn/trunk/media/css/demo_page.css\">
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-1.4.4.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilter.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" charset=\"utf-8\">
\t\$(document).ready( function () {
\t\t\$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
        \$.datepicker.regional[\"\"].changeMonth = true;
        \$.datepicker.regional[\"\"].changeYear = true;
        \$.datepicker.setDefaults(\$.datepicker.regional['']);
\t\t\$('#consumerTicketList').dataTable({
\t\t\t\"bLengthChange\": false,
\t     \t\"iDisplayLength\": 10,
\t     \t\"language\": {
\t     \t\t\"search\": \"_INPUT_\",
\t\t        \"searchPlaceholder\": \"Search records\"
\t\t    },
\t\t    \"sZeroRecords\": \"No Records to dispaly\",
\t\t\t\"sPaginationType\" : 'full_numbers',
\t\t\t\"sDom\": '<\"top\"l>rt<\"bottom\"ip><\"clear\">'\t
\t\t}).columnFilter({aoColumns:[
\t\t\t\t\t\tnull,
\t\t\t\t\t\t{ type:\"date-range\", sSelector: \"#dateFilter\" },
\t\t\t\t\t\t{ sSelector: \"#ticketidFilter\" },
\t\t\t\t\t\t{ sSelector: \"#subjectFilter\" },
\t\t\t\t\t\t{ type:\"select\", values : [\"Open\", \"Close\", \"Pending\"], sSelector: \"#statusFilter\" }
\t\t\t\t\t\t]}
\t\t\t\t\t);
\t\t
\t} );
</script>
<div class=\"page-bg\">
<div class=\"container\">
\t<div class=\"page-title\"><h1>Consumer Tickets</h1></div>
\t<div class=\"search_filter consumer-tickets-list-filter\">
\t\t\t<p id=\"ticketidFilter\"></p>
\t\t\t<p id=\"dateFilter\"></p>
\t\t\t<p id=\"subjectFilter\"></p>
\t\t\t<p id=\"statusFilter\"></p>
\t\t</div>
\t\t<div class=\"Create-ticket-form ticket-lists\">
\t\t\t
\t\t\t<div class=\"latest-orders\">
\t\t
\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\"consumerTicketList\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>Sl. No</th>
\t\t\t\t\t<th>Date Received</th>
\t\t\t\t\t<th>Ticket ID</th>
\t\t\t\t\t<th>Subject</th>
\t\t\t\t\t<th>Status</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tfoot>
\t\t\t<tr>
\t\t\t\t<th>Sl. No</th>
\t\t\t\t<th>Date Received</th>
\t\t\t\t<th>Ticket Id</th>
\t\t\t\t<th>Subjects</th>
\t\t\t\t<th>Status</th>
\t\t\t</tr>
\t\t\t</tfoot>
\t\t\t<tbody>
\t\t\t\t";
        // line 69
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ticketsList"]) ? $context["ticketsList"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 70
            echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"sl-no\">";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td class=\"date-rcvd\">";
            // line 72
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($context["list"], "ticketDatetime", array()), "date", array()), 0, 10), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td class=\"ticket-id\"><a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminview", array("id" => $this->getAttribute($context["list"], "id", array()))), "html", null, true);
            echo "\">#C";
            echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "ticketNo", array()), "html", null, true);
            echo "</a></td>
\t\t\t\t\t\t<td class=\"subject\">";
            // line 74
            echo twig_escape_filter($this->env, (((twig_length_filter($this->env, $this->getAttribute($context["list"], "ticketSubject", array())) > 50)) ? ((twig_slice($this->env, $this->getAttribute($context["list"], "ticketSubject", array()), 0, 50) . "...")) : ($this->getAttribute($context["list"], "ticketSubject", array()))), "html", null, true);
            echo "</td>
\t\t\t\t\t\t<td class=\"status\"><span>";
            // line 75
            if (($this->getAttribute($context["list"], "ticketStatus", array()) == 1)) {
                echo "<span class=\"open-status\"> Open </span>";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 2)) {
                echo "<span class=\"close-status\"> Close </span>";
            } elseif (($this->getAttribute($context["list"], "ticketStatus", array()) == 3)) {
                echo "<span class=\"pending-status\"> Pending </span>";
            }
            echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "\t\t\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
\t<div style=\"clear:both; height:10px\"></div>
\t</div>
\t</div> <!-- container div ends -->
<div class=\"container inner_container admin-dashboard\">
\t";
        // line 86
        $this->displayBlock('dashboard', $context, $blocks);
        // line 88
        echo "</div>
";
    }

    // line 86
    public function block_dashboard($context, array $blocks = array())
    {
        // line 87
        echo "\t";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:Admin:customer_ticket_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 87,  190 => 86,  185 => 88,  183 => 86,  173 => 78,  150 => 75,  146 => 74,  140 => 73,  136 => 72,  132 => 71,  129 => 70,  112 => 69,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  32 => 3,  29 => 2,  11 => 1,);
    }
}

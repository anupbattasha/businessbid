<?php

/* ::base_admin.html.twig */
class __TwigTemplate_8d86d8155b338a8197ca4e60c6d7e7006015023bfe433a974fba7ce64455376f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'chartscript' => array($this, 'block_chartscript'),
            'maincontent' => array($this, 'block_maincontent'),
            'header' => array($this, 'block_header'),
            'menu' => array($this, 'block_menu'),
            'topmenu' => array($this, 'block_topmenu'),
            'pageblock' => array($this, 'block_pageblock'),
            'dashboard' => array($this, 'block_dashboard'),
            'statistics' => array($this, 'block_statistics'),
            'latestorders' => array($this, 'block_latestorders'),
            'enquiries' => array($this, 'block_enquiries'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<head>
<title>Business BID Administrator</title>
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">

<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<!--[if lt IE 9]>
  \t<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/html5shiv.min.js"), "html", null, true);
        echo "\"></script>
  \t<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.min.js"), "html", null, true);
        echo "\"></script>
\t<![endif]-->
\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<!--[if IE 8]>
\t\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style-ie.css"), "html", null, true);
        echo "\">
\t<![endif]-->
\t<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/eticket.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.min.css"), "html", null, true);
        echo "\">
";
        // line 23
        echo "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>
";
        // line 25
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/highcharts.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/obhighcharts/js/highcharts/modules/exporting.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function() {




\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_updateemailid\").on('input',function (){
\tvar ax = \t\$(\"#form_updateemailid\").val();
\t//alert(ax);
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}

\telse{

\tvar filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
\tvar \$th = \$(this);
\tif (filter.test(ax)) {
\t\t\t\t\$th.css('border', '3px solid green');
\t\t\t}
\t\t\telse {
\t\t\t\t\$th.css('border', '3px solid red');
\t\t\t\te.preventDefault();
\t\t\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}

\t}
\t});

\t});
</script>
";
        // line 71
        $this->displayBlock('chartscript', $context, $blocks);
        // line 75
        echo "</head>
<body>
";
        // line 77
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 78
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 79
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 80
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 81
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 82
            echo "<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 contact-info\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>
\t\t\t\t\t<span>(04) 42 13 777</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3 left-clear\">
\t\t\t\t\t<!--<span class=\"contact-no\"><a href=\"/contact\">Contact Us</a></span>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6 clear-right dash-right-top-links\">
\t\t\t\t<ul>
\t\t\t\t\t<li>Welcome, <span class=\"profile-name\"><a href=\"";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_edit", array("userid" => (isset($context["uid"]) ? $context["uid"] : null))), "html", null, true);
            echo " \">";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</a></span></li>
\t\t\t\t\t<li><a href=\"";
            // line 97
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
        }
        // line 104
        $this->displayBlock('maincontent', $context, $blocks);
        // line 303
        echo "</body>
</html>
";
    }

    // line 71
    public function block_chartscript($context, array $blocks = array())
    {
        // line 72
        echo "

";
    }

    // line 104
    public function block_maincontent($context, array $blocks = array())
    {
        // line 105
        echo "<div class=\"main_container\" >
\t";
        // line 106
        $this->displayBlock('header', $context, $blocks);
        // line 115
        echo "\t";
        $this->displayBlock('menu', $context, $blocks);
        // line 161
        echo "    \t";
        $this->displayBlock('pageblock', $context, $blocks);
        // line 292
        echo "   ";
        $this->displayBlock('footer', $context, $blocks);
        // line 301
        echo "
";
    }

    // line 106
    public function block_header($context, array $blocks = array())
    {
        // line 107
        echo "\t<div class=\"header container\">
    \t<div class=\"col-md-6\">
\t\t<a href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_home");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"> </a>
        </div>
        <div class=\"col-md-6\">
        </div>
    </div>
\t";
    }

    // line 115
    public function block_menu($context, array $blocks = array())
    {
        // line 116
        echo "    <div class=\"menu_block menu_block-admin\">
    \t<div class=\"container side-clear\">
        \t\t<div class=\"navbar-header col-sm-2 side-clear\">
\t\t\t\t\t<ul class=\"page-title\"><li><a href=\"";
        // line 119
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_home");
        echo "\">DASHBOARD</a></li></ul>
\t\t\t\t</div>
\t\t\t\t";
        // line 121
        $this->displayBlock('topmenu', $context, $blocks);
        // line 158
        echo "        </div>
    </div>
\t";
    }

    // line 121
    public function block_topmenu($context, array $blocks = array())
    {
        // line 122
        echo "\t\t\t\t\t<div class=\"collapse navbar-collapse navHeaderCollapse col-sm-10 side-clear pull-right\">
\t\t\t\t\t\t<ul class=\"nav navbar-nav\">
\t\t\t\t\t\t\t<li><a href=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_enquiries", array("offset" => 1));
        echo "\">Job Requests</a></li>
\t\t\t\t\t\t\t<li class=\"active\"><a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_freetrails", array("offset" => 1));
        echo "\">Free Trial</a></li>
\t\t\t\t\t\t\t<li class=\"active\"><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_users", array("offset" => 1));
        echo "\">Users</a></li>
\t\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Vendor Orders</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\"  aria-labelledby=\"dropdownMenu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 130
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_orders", array("offset" => 1));
        echo "\"> Vendor Orders</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 131
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_category_mapping");
        echo "\">Vendor Category Mapping</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t\t";
        // line 136
        echo "\t\t\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Report</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\"  aria-labelledby=\"dropdownMenu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 138
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_reports");
        echo "\">Vendor Reports</a>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 139
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_orders_reports");
        echo "\">Vendor Orders</a>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 140
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_reports");
        echo "\">Customer Reports</a>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 141
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_enquiries_reports");
        echo "\">Job Requests</a>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 142
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_categories_reports");
        echo "\">Categories</a>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 143
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_leads_list");
        echo "\">Vendor Leads List</a>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 146
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_categories");
        echo "\"> Categories</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 147
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviews", array("offset" => 1));
        echo "\">Reviews</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 148
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_lead_management");
        echo "\">Lead Refund</a></li>
\t\t\t\t\t\t\t<li class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"";
        // line 149
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminhome");
        echo "\">Support<span class=\"caret\"></span></a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 151
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminlist", array("usertype" => "Vendor"));
        echo "\">Vendor Ticket List</a>
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 152
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ticket_adminlist", array("usertype" => 1));
        echo "\">Customer Ticket List</a>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 161
    public function block_pageblock($context, array $blocks = array())
    {
        // line 162
        echo "\t<div class=\"page-bg\">
    <div class=\"container inner_container admin-dashboard\">
\t";
        // line 164
        $this->displayBlock('dashboard', $context, $blocks);
        // line 200
        echo "
\t";
        // line 201
        $this->displayBlock('statistics', $context, $blocks);
        // line 208
        echo "
\t";
        // line 209
        $this->displayBlock('latestorders', $context, $blocks);
        // line 251
        echo "
\t";
        // line 252
        $this->displayBlock('enquiries', $context, $blocks);
        // line 288
        echo "
    </div>
    </div>
\t";
    }

    // line 164
    public function block_dashboard($context, array $blocks = array())
    {
        // line 165
        echo "        <div class=\"col-md-6\">
        \t<div class=\"overview_block block  overview-cotainer\">
\t\t\t\t<table>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Total Job Requests</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 170
        echo twig_escape_filter($this->env, (isset($context["totalenquirycount"]) ? $context["totalenquirycount"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Total  Job Requests This Year</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 174
        echo twig_escape_filter($this->env, (isset($context["totalenquirycount2"]) ? $context["totalenquirycount2"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Vendor Orders</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 178
        echo twig_escape_filter($this->env, (isset($context["totalordercount"]) ? $context["totalordercount"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>No. of Customers</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 182
        echo twig_escape_filter($this->env, (isset($context["totalcustcount"]) ? $context["totalcustcount"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>No. of Vendors</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 186
        echo twig_escape_filter($this->env, (isset($context["totalusercountforapproval"]) ? $context["totalusercountforapproval"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Job Requests Not Accepted</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 190
        echo twig_escape_filter($this->env, (isset($context["totalenquiryawaitingcount"]) ? $context["totalenquiryawaitingcount"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>Reviews Awaiting Approval</td>
\t\t\t\t\t\t<td><span class=\"pull-right\">";
        // line 194
        echo twig_escape_filter($this->env, (isset($context["totalreviewsawaitingcount"]) ? $context["totalreviewsawaitingcount"] : null), "html", null, true);
        echo "</span></td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
            </div>
        </div>
\t";
    }

    // line 201
    public function block_statistics($context, array $blocks = array())
    {
        // line 202
        echo "        <div class=\"col-md-6 last statistics\">
        \t<div class=\"overview_block block\">
         <div id=\"linechart\" style=\"min-width: 400px; margin: 0 auto\"></div>
            </div>
        </div>
\t";
    }

    // line 209
    public function block_latestorders($context, array $blocks = array())
    {
        // line 210
        echo "        <div class=\"latest-orders top-spce\">
        \t<div class=\"overview_block block\">
            \t<h2>Last 10 Vendor Orders</h2>
                <table>
\t\t\t\t<thead><tr>
\t\t\t\t<th>Date Received</th>
\t\t\t\t<th>Order Number</th>
\t\t\t\t<th>Vendor Name</th>
\t\t\t\t<th>Business Name</th>
\t\t\t\t<th>Vendor Email-Id</th>
\t\t\t\t<th>Category</th>
\t\t\t\t<th>Lead Package</th>
\t\t\t\t<th>Payment Type</th>
\t\t\t\t<th>Status</th>
\t\t\t\t<th>Amount</th>
\t\t\t\t</tr></thead>
\t\t\t\t<tbody>
\t\t\t\t";
        // line 227
        if ((twig_length_filter($this->env, (isset($context["orders"]) ? $context["orders"] : null)) > 0)) {
            // line 228
            echo "
\t\t\t\t\t";
            // line 229
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
                // line 230
                echo "\t\t\t\t\t\t<tr><td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["order"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo "</td><td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "ordernumber", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 231
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "contactname", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 232
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "bizname", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 233
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "email", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 234
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "category", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 235
                if (($this->getAttribute($context["order"], "leadpack", array()) == 25)) {
                    echo " Bronze ";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 50)) {
                    echo " Silver
\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 236
$context["order"], "leadpack", array()) == 100)) {
                    echo " Gold ";
                } elseif (($this->getAttribute($context["order"], "leadpack", array()) == 250)) {
                    echo " Platinum ";
                }
                // line 237
                echo "\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>";
                // line 238
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "payoption", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 239
                if (($this->getAttribute($context["order"], "status", array()) == 1)) {
                    echo " Approved ";
                } else {
                    echo " Pending ";
                }
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 240
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "amount", array()), "html", null, true);
                echo "</td></tr>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 242
            echo "
\t\t\t\t";
        } else {
            // line 244
            echo "\t\t\t\t\t<tr> <td colspan=\"6\" class=\"no-record\"> No Records found  </td></tr>
\t\t\t\t";
        }
        // line 246
        echo "\t\t\t\t</tbody>
\t\t\t\t</table>
            </div>
        </div>
\t";
    }

    // line 252
    public function block_enquiries($context, array $blocks = array())
    {
        // line 253
        echo "        <div class=\"mid latest-enquires assasa\">
        \t<div class=\"overview_block block\">
            \t<h2>Last 10 Job Requests</h2>
                <table>
\t\t\t\t<thead><tr>
\t\t\t\t<th>Date Received</th>
\t\t\t\t<th>Job Request Number</th>
\t\t\t\t<th>Subject</th>
\t\t\t\t<th>Category</th>
\t\t\t\t<th>City</th>
\t\t\t\t<th>Name</th>
\t\t\t\t<th>Email</th>
\t\t\t\t<th>Contact Number</th>
\t\t\t\t<th>Status</th></tr></thead>
\t\t\t\t<tbody>
\t\t\t\t";
        // line 268
        if ((twig_length_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null)) > 0)) {
            // line 269
            echo "\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["enquiry"]) {
                // line 270
                echo "\t\t\t\t\t\t<tr><td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 271
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "id", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 272
                echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enquiry"], "subj", array()), 0, 20), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 273
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "category", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t\t<td>";
                // line 274
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "location", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 275
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "contactname", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 276
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "email", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 277
                echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "smsphone", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t<td>";
                // line 278
                if (($this->getAttribute($context["enquiry"], "status", array()) == 1)) {
                    echo " Accepted ";
                }
                echo "</td></tr>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquiry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 280
            echo "\t\t\t\t";
        } else {
            // line 281
            echo "\t\t\t\t<tr> <td colspan=\"6\" class=\"no-record\"> No Records found  <td>
\t\t\t\t";
        }
        // line 283
        echo "\t\t\t\t</tbody>
\t\t\t\t</table>
            </div>
        </div>
\t";
    }

    // line 292
    public function block_footer($context, array $blocks = array())
    {
        // line 293
        echo "\t<div class=\"footer-dashboard text-center\">

\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"col-sm-6 footer-messages\">Copyright 2013 - 2014 BusinessBid</div>
    \t\t\t<div class=\"col-sm-6 clear-right\"></div>
\t\t\t<div>
    \t</div>
\t";
    }

    public function getTemplateName()
    {
        return "::base_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  658 => 293,  655 => 292,  647 => 283,  643 => 281,  640 => 280,  630 => 278,  626 => 277,  622 => 276,  618 => 275,  614 => 274,  610 => 273,  606 => 272,  602 => 271,  597 => 270,  592 => 269,  590 => 268,  573 => 253,  570 => 252,  562 => 246,  558 => 244,  554 => 242,  546 => 240,  538 => 239,  534 => 238,  531 => 237,  525 => 236,  519 => 235,  515 => 234,  511 => 233,  507 => 232,  503 => 231,  496 => 230,  492 => 229,  489 => 228,  487 => 227,  468 => 210,  465 => 209,  456 => 202,  453 => 201,  443 => 194,  436 => 190,  429 => 186,  422 => 182,  415 => 178,  408 => 174,  401 => 170,  394 => 165,  391 => 164,  384 => 288,  382 => 252,  379 => 251,  377 => 209,  374 => 208,  372 => 201,  369 => 200,  367 => 164,  363 => 162,  360 => 161,  350 => 152,  346 => 151,  341 => 149,  337 => 148,  333 => 147,  329 => 146,  323 => 143,  319 => 142,  315 => 141,  311 => 140,  307 => 139,  303 => 138,  299 => 136,  292 => 131,  288 => 130,  281 => 126,  277 => 125,  273 => 124,  269 => 122,  266 => 121,  260 => 158,  258 => 121,  253 => 119,  248 => 116,  245 => 115,  233 => 109,  229 => 107,  226 => 106,  221 => 301,  218 => 292,  215 => 161,  212 => 115,  210 => 106,  207 => 105,  204 => 104,  198 => 72,  195 => 71,  189 => 303,  187 => 104,  177 => 97,  171 => 96,  155 => 82,  153 => 81,  151 => 80,  149 => 79,  147 => 78,  145 => 77,  141 => 75,  139 => 71,  93 => 28,  89 => 27,  83 => 25,  80 => 23,  76 => 21,  72 => 20,  68 => 19,  64 => 18,  60 => 17,  55 => 15,  46 => 9,  42 => 8,  35 => 4,  30 => 1,);
    }
}

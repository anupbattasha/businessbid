<?php

/* BBidsBBidsHomeBundle:User:freetrail.html.twig */
class __TwigTemplate_22c332893f61be1a61d00e885358bbee09583d642e2b3568b84c21fb01affde4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:freetrail.html.twig", 1);
        $this->blocks = array(
            'jquery' => array($this, 'block_jquery'),
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_jquery($context, array $blocks = array())
    {
        // line 5
        echo "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script>
 <script src=\"http://code.jquery.com/jquery-latest.js\"></script>
 <script type=\"text/javascript\">
 \$(document).ready(function () {
 
 
    \$('input[id^=\"form_category\"]').click(function () {
\t\t\$('input[id^=\"form_category\"]').each(function(){
\t\t\t\$(this).parent().find('.subcategory').remove();
\t\t\t
\t\t
\t\t\t
\t\t});
\t\t
\t\t\tvar idarr = \$(this).attr('id').split('_');
\t\t\tvar s=\$(this).val();
\t\t\tvar ids= \$(this).attr('id');
\t\t   loadjsajax(ids,s);   
\t\t\t\$('#cityvalidate').css('display','none');
\t\t   \t\$('#city').css('display','block');
\t\t   

    });
   
});


function loadjsajax(id,value)
{

var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
\t\t\t
\t\t\tvar newNode = document.createElement('div');  
\t\t\tnewNode.id =  \"subcat\"+value;
\t\t\tnewNode.setAttribute(\"class\",\"subcategory\");
\t\t\tnewNode.innerHTML =xmlhttp.responseText;
\t\t\tvar abc = document.getElementById(id).parentNode;
\t\t\t
\t\t\tabc.appendChild(newNode);\t
\t\t\t
\t\t\t
    }
  }
xmlhttp.open(\"GET\",realpath+\"/pullcategoriesBySegmentstovendor.php?id=\"+value+\"&mode=selectsubcat\",true);
xmlhttp.send();
}

</script>
             
";
        // line 68
        echo "
";
    }

    // line 71
    public function block_pageblock($context, array $blocks = array())
    {
        // line 72
        echo "<div class=\"col-md-9 dashboard-rightpanel\">
\t<div  class=\"page-title\"> <h1>Freetrail Lead  Pack</h1></div>
\t";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 75
            echo "\t\t<div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 78
            echo "\t\t<div class=\"alert alert-error\">";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "\t<div class=\"category-block\"><h4>CHOOSE YOUR BUSINESS OFFERING </h4>
\t\t<div class=\"category-content\">
\t\t\t
\t\t\t<div class=\"field-item\">
\t\t\t\t";
        // line 84
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
\t\t\t\t";
        // line 85
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 86
            echo "\t\t\t\t\t<div class=\"field-item\">";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo " </div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "\t\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"category-block\"><h4>CHOOSE YOUR CITY <span data-toggle='tooltip'  title=\"Please select at least one City\">i</span></h4>
\t\t<div class=\"category-content\"><p>
\t\t\t<p class=\"message\" id=\"cityvalidate\" >Please select the category</p>
\t\t\t<div class=\"city\" id=\"city\" style=\"display:none\">
\t\t\t\t";
        // line 97
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 98
            echo "\t\t\t\t\t<div class=\"field-item\">";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "\t\t\t</div>
\t\t</div>
\t</div>
\t\t<div class=\"col-sm-12 side-clear proceed-freetrial\"><div class=\"row\"> ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "free", array()), 'widget', array("label" => "Proceed to your Free Trial now"));
        echo " </div></div>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:freetrail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 103,  186 => 100,  175 => 98,  171 => 97,  160 => 88,  149 => 86,  145 => 85,  141 => 84,  135 => 80,  126 => 78,  121 => 77,  112 => 75,  108 => 74,  104 => 72,  101 => 71,  96 => 68,  32 => 5,  29 => 3,  11 => 1,);
    }
}

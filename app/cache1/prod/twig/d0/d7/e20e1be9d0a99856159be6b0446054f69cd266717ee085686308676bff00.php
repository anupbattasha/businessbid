<?php

/* BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig */
class __TwigTemplate_d0d7e20e1be9d0a99856159be6b0446054f69cd266717ee085686308676bff00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "
<div class=\"container\">
<div class=\"page-title\"><h1>Vendor Category Mapping</h1></div>
<div class=\"edit-user-block\">

<div class=\"col-md-12 side-clear\">
    ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 10
            echo "
    <div class=\"alert alert-danger\">";
            // line 11
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "
    ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 16
            echo "
    <div class=\"alert alert-success\">";
            // line 17
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
    ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 22
            echo "
    <div class=\"alert alert-success\">";
            // line 23
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "

    ";
        // line 28
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
        ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
    <div>
        <div>";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'errors');
        echo " </div>
        <div>";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendor_name", array()), 'errors');
        echo " </div>
        <div>";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from_date", array()), 'errors');
        echo " </div>
        <div>";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to_date", array()), 'errors');
        echo " </div>
        <div>";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'errors');
        echo " </div>
    </div>

    <div class=\"container side-clear\">
        <div class=\"col-sm-8 edit-form-vendor clear-left\">

            <div class=\"col-sm-6 med-top-space\"><div class=\"field-label\">";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'label');
        echo " <span class=\"req\" id=\"emailexists\"></span></div>";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "category", array()), 'widget');
        echo "</div>
            <div class=\"col-sm-6 med-top-space\"><div class=\"field-label\">";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendor_name", array()), 'label');
        echo "</div> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vendor_name", array()), 'widget');
        echo "</div>
            <div class=\"col-sm-6 med-top-space\">";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from_date", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "from_date", array()), 'widget');
        echo "</div>
            <div class=\"col-sm-6 med-top-space\">";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to_date", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "to_date", array()), 'widget');
        echo " </div>
            <div class=\"col-sm-6 med-top-space\">";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label');
        echo " : ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "</div>
            <div class=\"col-sm-10 med-top-space\">";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
            ";
        // line 47
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
        </div>
    </div>
</div>

<div class=\"latest-orders\">
    <table id=\"list\" class=\"display\" cellspacing=\"0\" width=\"100%\">
        <thead>
            <tr>
                <th>Category</th>
                <th>Vendor</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Description</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 66
        if ( !twig_test_empty((isset($context["mappingList"]) ? $context["mappingList"] : null))) {
            // line 67
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["mappingList"]) ? $context["mappingList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                // line 68
                echo "            <tr>
                <td>";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "category", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "contactname", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 71
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["m"], "startDate", array()), "d-m-Y"), "html", null, true);
                echo "</td>
                <td>";
                // line 72
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["m"], "endDate", array()), "d-m-Y"), "html", null, true);
                echo "</td>
                <td>";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "description", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 74
                if (($this->getAttribute($context["m"], "status", array()) == 1)) {
                    echo " Active ";
                } else {
                    echo " Inactive ";
                }
                echo "</td>
                <td>";
                // line 75
                if (($this->getAttribute($context["m"], "status", array()) == 1)) {
                    echo " <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_deactivate_category_mapping", array("vid" => $this->getAttribute($context["m"], "vid", array()), "cid" => $this->getAttribute($context["m"], "cid", array()), "type" => 2)), "html", null, true);
                    echo "\">Deactivate</a> ";
                } else {
                    echo " <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_deactivate_category_mapping", array("vid" => $this->getAttribute($context["m"], "vid", array()), "cid" => $this->getAttribute($context["m"], "cid", array()), "type" => 1)), "html", null, true);
                    echo "\">Activate</a> ";
                }
                echo "</td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "            ";
        } else {
            // line 79
            echo "            <tr>
                <td colspan=\"7\">No Results found</td>
            </tr>
            ";
        }
        // line 83
        echo "        </tbody>
    </table>
</div>
<script src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
";
        // line 88
        echo "<script>
  \$(function() {
    // \$('#list').dataTable({});
    \$( \"#form_vendor_name\" ).autocomplete({
      source: \"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("autocomplete/vendor_names.php"), "html", null, true);
        echo "\",
      minLength: 2,
      select: function( event, ui ) {
        \$('#form_vid').val(ui.item.id);
      },
      change: function(event,ui)
        {
            if (ui.item==null)
            {
                \$( this ).val('');
                \$( this ).focus();
                \$('#form_vid').val('');
            }
        }
    });

    var dates = \$(\"#form_from_date, #form_to_date\").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        minDate: new Date(),
        maxDate: '+5m',
        onSelect: function(selectedDate) {
            var option = this.id == \"form_from_date\" ? \"minDate\" : \"maxDate\",
            instance = \$(this).data(\"datepicker\"),
            date = \$.datepicker.parseDate(
            instance.settings.dateFormat ||
            \$.datepicker._defaults.dateFormat,
            selectedDate, instance.settings);
            dates.not(this).datepicker(\"option\", option, date);
        }
    }).attr('readonly', 'readonly');

  });
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:vendor_category_mapping.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 92,  260 => 88,  256 => 86,  251 => 83,  245 => 79,  242 => 78,  225 => 75,  217 => 74,  213 => 73,  209 => 72,  205 => 71,  201 => 70,  197 => 69,  194 => 68,  189 => 67,  187 => 66,  165 => 47,  161 => 46,  155 => 45,  149 => 44,  143 => 43,  137 => 42,  131 => 41,  122 => 35,  118 => 34,  114 => 33,  110 => 32,  106 => 31,  101 => 29,  97 => 28,  93 => 26,  84 => 23,  81 => 22,  77 => 21,  74 => 20,  65 => 17,  62 => 16,  58 => 15,  55 => 14,  46 => 11,  43 => 10,  39 => 9,  31 => 3,  28 => 2,  11 => 1,);
    }
}

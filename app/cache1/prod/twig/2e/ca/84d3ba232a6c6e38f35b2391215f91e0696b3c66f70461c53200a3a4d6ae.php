<?php

/* BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig */
class __TwigTemplate_2eca84d3ba232a6c6e38f35b2391215f91e0696b3c66f70461c53200a3a4d6ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig", 1);
        $this->blocks = array(
            'customjs' => array($this, 'block_customjs'),
            'customcss' => array($this, 'block_customcss'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_customjs($context, array $blocks = array())
    {
        // line 4
        echo "<script type=\"text/javascript\">
\t
\t
\tfunction timer(i){
\t\tvar arr = new Array('1d','2d','3d','4d');
\t\t//alert(i);

\t
\t
\t\ti = parseInt(i)+1;
\t\tif(i!=4)
\t\t{      if(i==1){
\t\t\t document.getElementById('loader1').style.width=\"33%\";
\t\t\t document.getElementById('loader2').style.width=\"33%\";
\t\t\t document.getElementById('form-succ').innerHTML = \"Your request is being processed...\";
\t\t\t}
\t\t\tif(i==2){
                         document.getElementById('loader1').style.width=\"66%\";
                         document.getElementById('loader2').style.width=\"66%\";
                         document.getElementById('form-succ').innerHTML = \"Your request is being processed...\";
                        }
\t\t\tif(i==3){
                         document.getElementById('loader1').style.width=\"100%\";
                         document.getElementById('loader2').style.width=\"100%\";
                         document.getElementById('form-succ').innerHTML = \"Your job request has been logged successfully. Please note your Job Request Number is ";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["jobNo"]) ? $context["jobNo"] : null), "html", null, true);
        echo "\";
                        }
\t\t\t
\t\t\tsetTimeout(function() { timer([i]); }, 5000);
\t\t} else {
\t\t\t";
        // line 33
        if (array_key_exists("jobNo", $context)) {
            // line 34
            echo "\t\t\t\tdocument.getElementById('form-succ').innerHTML = \"Your job request has been logged successfully. Please note your Job Request Number is ";
            echo twig_escape_filter($this->env, (isset($context["jobNo"]) ? $context["jobNo"] : null), "html", null, true);
            echo "\";
\t\t\t";
        }
        // line 36
        echo "\t\t\tsetTimeout(function() { window.open('";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "', '_self'); }, 7000);\t\t\t
\t\t}
\t}
</script>
";
    }

    // line 42
    public function block_customcss($context, array $blocks = array())
    {
        // line 43
        echo "<style>
. des{
\twidth:100%;
\talign-text:center;
}
</style>

";
    }

    // line 52
    public function block_banner($context, array $blocks = array())
    {
        // line 53
        echo "
";
    }

    // line 56
    public function block_maincontent($context, array $blocks = array())
    {
        // line 57
        echo "<div class=\"container loader-cont\">

\t\t<div class=\"bg-success alert-success\" id=\"form-succ\"></div>
\t\t<div class=\"loader\">
\t\t\t<div class=\"load-arrow-left\">
\t\t\t\t<div class=\"load-arrow blue-arrow\" id=\"loader1\" style=\"width: 33%\">
\t\t\t\t\t<span></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"arrow-logo\">
\t\t\t</div>
\t\t\t<div class=\"load-arrow-right\">
\t\t\t\t<div class=\"load-arrow orange-arrow\" id=\"loader2\" style=\"width: 33%\">
\t\t\t\t\t<span></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<!-- <div class=\"alert-success\" id=\"request\">
\t\t</div> -->

\t\t<div id=\"pstatus\"></div>
\t

</div>

\t
</div>


<script type=\"text/javascript\">

window.onload = timer(0);

</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:enquiryprocessing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 57,  108 => 56,  103 => 53,  100 => 52,  89 => 43,  86 => 42,  76 => 36,  70 => 34,  68 => 33,  60 => 28,  34 => 4,  31 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:success.html.twig */
class __TwigTemplate_eb84f384b97d7e172491be1bb7153db762d0cdfc0cff1e266936f2b50bc2da92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:success.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"col-md-9 dashboard-rightpanel thank-you-block\"><h2>Thank you for choosing free trial feature.</h3>
<p>Once this feature has been activated you will be notified by our team and then you will start receiving new business leads. 
<br>
<br>
<strong>If you have any questions please feel free to contact our support centre on <a>1800 525 666</a>.</strong></p>
<br>
<h5>Regards,</h5>
<h5>BusinessBid</h5>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }
}

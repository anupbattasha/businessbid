<?php

/* BBidsBBidsHomeBundle:Admin:getleadcredits.html.twig */
class __TwigTemplate_4916b81b8df96796954cfd3a84c0297da5790bdc08d31157d969d0a58c26a17e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:getleadcredits.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"page-bg\">\t
    <div class=\"container inner_container admin-dashboard\">
\t\t<div class=\"page-title\"><h1>Lead Refund History</h1></div>
\t\t<div class=\"row text-right page-counter-history\"><div class=\"counter-box\">Now showing : ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : null), "html", null, true);
        echo " to ";
        echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : null), "html", null, true);
        echo " of ";
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
        echo " records</div></div>
\t\t<div class=\"admin_filter\">\t
\t\t\t<form name=\"form\" method=\"post\" action=\"\">
\t\t\t\t<div class=\"col-md-3 side-clear\">";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("label" => "Contact Name"));
        echo " </div><div class=\"col-md-1 clear-right\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "</div>
\t\t\t</form>
\t\t</div>
\t\t
\t\t<div class=\"latest-orders top-spce\">
\t\t\t<table>
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>Contact Name</th>
\t\t\t\t\t<th>Business Name</th>
\t\t\t\t\t<th>Category</th>
\t\t\t\t\t<th>Leadcount</th>
\t\t\t\t\t<th>New Leadcount</th>
\t\t\t\t\t<th> Leads Credited </th>
\t\t\t\t\t<th>Refund Date</th>
\t\t\t\t\t<th>Status</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["credits"]) ? $context["credits"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 29
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "contactname", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "bizname", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "leadcount", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "newleadcount", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "leadcredited", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 36
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["c"], "created", array()), "Y-m-d"), "html", null, true);
            echo " </td>
\t\t\t\t\t<td>";
            // line 37
            if (($this->getAttribute($context["c"], "status", array()) == 1)) {
                echo "Approved ";
            } else {
                echo " Disapprove ";
            }
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t\t
\t
\t\t<div class=\"row text-right\">
\t\t\t<div class=\"counter-box\">
\t\t\t\t";
        // line 47
        $context["page_count"] = intval(floor(( -(isset($context["count"]) ? $context["count"] : null) / 10)));
        // line 48
        echo "\t\t\t\t<ul class=\"pagination\">
\t\t\t\t\t<li><a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_lead_management", array("offset" => 1));
        echo "\"> &laquo; </a></li>
\t\t\t\t
\t\t\t\t\t";
        // line 51
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1,  -(isset($context["page_count"]) ? $context["page_count"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 52
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_lead_management", array("offset" => $context["i"])), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</a></li>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:getleadcredits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 54,  143 => 52,  139 => 51,  134 => 49,  131 => 48,  129 => 47,  120 => 40,  107 => 37,  103 => 36,  99 => 35,  95 => 34,  91 => 33,  87 => 32,  83 => 31,  79 => 30,  76 => 29,  72 => 28,  46 => 9,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}

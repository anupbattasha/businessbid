<?php

/* BBidsBBidsHomeBundle:Home:node1.html.twig */
class __TwigTemplate_9024d305e2d9454c19d2e5afbb14c76c633d15aa85922aea8eedbf42381c72f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node1.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"inner-banner-block\">
\t<div class=\"container inner-banner-content\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-8\">
\t\t\t\t<h1>Are you a service professional?</br> <span>Here is how to grow your business</span></h1>
\t\t\t\t<div class=\"banner-conntent\">
\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t<h2>Get access to qualified leads</h2>
\t\t\t\t\t<p>Tell us what your business is about and we will match customer projects & leads to meet your exact needs.</p>
\t\t\t\t\t</span>
\t\t\t\t\t
\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t<h2>Covering all Sectors </h2>
\t\t\t\t\t<p>Now get access to leads across Residential, Commercial, Corporate and Lifestyle related projects as we cover all sectors.</p>
\t\t\t\t\t</span>
\t\t\t\t\t
\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t<h2>Choice and Control</h2>
\t\t\t\t\t<p>You only pay for the projects you wish to undertake and thereby you control your budget.</p>
\t\t\t\t\t</span>
\t\t\t\t\t
\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t<h2>Outsmart your competition </h2>
\t\t\t\t\t<p>Our unique lead management tools keep you notified of all customer jobs and projects at all times.</p>
\t\t\t\t\t</span>
\t\t\t\t\t
\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t<h2>Extend your reach</h2>
\t\t\t\t\t<p>Access Leads from areas outside your immediate business zone with no marketing costs and extend your services further.</p>
\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<div class=\"container grow-business-block\">
\t<div class=\"row\">
\t\t<div class=\"col-md-4\">
\t\t\t<div class=\"business-block\">
\t\t\t\t<h1>Jobs Logged Today</h1>
\t\t\t\t<p>Request Received:</p>
\t\t\t\t<div class=\"request-digit\">0</div>
\t\t\t\t<div class=\"request-digit\">0</div>
\t\t\t\t<div class=\"request-digit\">0</div>
\t\t\t\t<div class=\"request-digit\">0</div>
\t\t\t\t<div class=\"request-digit\">4</div>
\t\t\t\t<div class=\"request-digit\">8</div>
\t\t\t\t<div class=\"request-digit\">5</div>
\t\t\t\t
\t\t\t\t<p>See how you can get access to customer jobs being logged</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-4\">
\t\t\t<div class=\"business-block\">
\t\t\t<div>
\t\t\t\t<h1>Success Stories</h1>
\t\t\t\t<p>\"Bussiness Bids is an integral part of our business that has helped us keep our job pipeline full at a fair price.\"</p>
\t\t\t\t<a href=\"#\">Read More</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"row1\">
\t\t\t\t\t<div class=\"col-md-4\"><a class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#success-story\">Success Stories</a></div>
\t\t\t\t\t<div class=\"col-md-8\"><span>See how BusinessBid is helping other vendors grow their business</span></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-4\">
\t\t\t<div class=\"business-block\">
\t\t\t\t<h1>How It Works For Vendors</h1>
\t\t\t\t<div>
\t\t\t\t\t<div class=\"col-md-6 video-icon\"></div>
\t\t\t\t\t<div class=\"col-md-6 video-cont\">
\t\t\t\t\t\tLearn how BusinessBid can help you connect with customers looking for vendors like you in your area. 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t\t<div class=\"row1\">
\t\t\t\t\t\t<div class=\"col-md-4\"><a class=\"btn btn-success\" href=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_how_it_works");
        echo "\">Watch Tutorial</a></div>
\t\t\t\t\t\t<div class=\"col-md-8\"><span>Watch a 2 minute tutorial and find out how easy it is</span></div>
\t\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<div class=\"container signup\">
\t<a class=\"btn btn-success signup-btn\" href=\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">SIGN UP TODAY</a>
</div>
<div class=\"modal fade\" id=\"success-story\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
        <h4 class=\"modal-title\" id=\"myModalLabel\">Success Stories</h4>
      </div>
      <div class=\"modal-body modal-body-story\">
\t\t\t<div class=\"quote-box\"><div class=\"content\">BusinessBid has helped me get new customers in a short time and I have my pipeline busy for the next 2 months</div> <span class=\"profile-addrs\">H20 Landscape, Dubai</span></div>
\t\t\t<div class=\"quote-box\"><div class=\"content\">BusinessBid takes care of my business development � I don�t even have to worry about a marketing budget now</div> <span class=\"profile-addrs\">Age Films, Sharjah</span></div>
\t\t\t<div class=\"quote-box\"><div class=\"content\">I didn�t know easy it was dealing with BusinessBid. I am connected to more customers now  than before</div> <span class=\"profile-addrs\">InCreative Interiors & Fit-Outs, Dubai</span></div>
\t\t\t<div class=\"quote-box\"><div class=\"content\">I have access to quality leads and the best part is I only pay for leads which I choose to accept</div> <span class=\"profile-addrs\">M Square Studio,  Sharjah</span></div>
\t\t\t<div class=\"quote-box\"><div class=\"content\">We have started getting a lot more orders now. Thank you BusinessBid !</div> <span class=\"profile-addrs\">Melange Cake Pops, Dubai</span></div>
      </div>
    </div>
  </div>
</div>



";
    }

    // line 110
    public function block_maincontent($context, array $blocks = array())
    {
        // line 111
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 111,  147 => 110,  120 => 87,  109 => 79,  32 => 4,  29 => 3,  11 => 1,);
    }
}

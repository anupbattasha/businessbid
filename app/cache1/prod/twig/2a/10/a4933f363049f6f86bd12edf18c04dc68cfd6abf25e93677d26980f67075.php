<?php

/* ::newconsumer_dashboard.html.twig */
class __TwigTemplate_2a10a4933f363049f6f86bd12edf18c04dc68cfd6abf25e93677d26980f67075 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'jquery' => array($this, 'block_jquery'),
            'customcss' => array($this, 'block_customcss'),
            'header' => array($this, 'block_header'),
            'menu' => array($this, 'block_menu'),
            'topmenu' => array($this, 'block_topmenu'),
            'pageblocknew' => array($this, 'block_pageblocknew'),
            'megamenu' => array($this, 'block_megamenu'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'enquiries' => array($this, 'block_enquiries'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<head>
\t<title>Business BID</title>
\t<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">
\t<script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
\t<!--<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>-->
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/eticket.css"), "html", null, true);
        echo "\">
\t<!--<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>-->
\t<!--[if lt IE 9]>
  \t<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/html5shiv.min.js"), "html", null, true);
        echo "\"></script>
  \t<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.min.js"), "html", null, true);
        echo "\"></script>
\t<![endif]-->
\t\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>

\t<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js\"></script>
\t<script>
\t\$(document).ready(function(){

\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$('#category').autocomplete({
\t      \tsource: function( request, response ) {
\t      \t\$.ajax({
\t      \turl : realpath+\"ajax-info.php\",
\t      \tdataType: \"json\",
\tdata: {
\t   name_startsWith: request.term,
\t   type: 'category'
\t},
\t success: function( data ) {
\t response( \$.map( data, function( item ) {

\treturn {
\tlabel: item,
\tvalue: item

\t}

\t}));
\t},
\tselect: function(event, ui) {
                               alert( \$(event.target).val() );
                            }
\t      \t});
\t      \t},
\t      \tautoFocus: true,
\t      \tminLength: 2

\t      });

});
\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_subcategory\").html('<option value=\"0\">Select</option>');
\t\$(\"#form_category\").on('change',function (){
\tvar ax = \t\$(\"#form_category\").val();

\t\$.ajax({url:realpath+\"pullSubCategoryByCategory.php?categoryid=\"+ax,success:function(result){

\t\$(\"#form_subcategory\").html(result);
\t}});
\t});

});

\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_email\").on('input',function (){
\tvar ax = \t\$(\"#form_email\").val();
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}
\t});

});

\t\$(document).ready(function(){
\t\$('#form_description').attr(\"maxlength\",\"300\");
\t\$('#form_location').attr(\"maxlength\",\"30\");

\t});

</script>
\t<script>
\t\t\$(document).ready(function() {
\t\t\tsetTimeout(function() {
\t\t\t\t// Slide

\t\t\t\t\$('div[id=^\"menu\"] > div > a.expanded + ul').slideToggle('medium');
\t\t\t\t\$('div[id=^\"menu\"] > div > a').click(function() {
\t\t\t\t\t\$('div[id=^\"menu\"] > div > a.expanded').not(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
\t\t\t\t\t\$(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
\t\t\t\t});
\t\t\t}, 250);
\t\t});
\t</script>


\t<script type='text/javascript' >

\tfunction jsfunction(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//~ alert(xmlhttp.responseText);
\tdocument.getElementById(\"searchval\").innerHTML=xmlhttp.responseText;
\tmegamenublogAjax();
\tmegamenubycatAjax();
\tmegamenubycatVendorSearchAjax();
\tmegamenubycatVendorReviewAjax();
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"ajax-info.php\",true);
\txmlhttp.send();



\t}
\tfunction megamenublogAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t\t\t\t\t\t\t\t\t//document.getElementById(\"megamenu1\").innerHTML=xmlhttp.responseText;
\t\t//~ var mvsa = document.getElementById(\"megamenu1Anch\");
\t\t//~ for (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t//~ if (mvsa.childNodes[i].className == \"dropdown-menu Resourcesegment-drop\") {
\t\t\t\t//~ mvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  //~ break;
\t\t\t//~ }
\t\t//~ }
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menublog.php\",true);
\t//xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);

\txmlhttp.send();



\t}
\tfunction megamenubycatAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\tdocument.getElementById(\"megamenubycatUl\").innerHTML=xmlhttp.responseText;
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menubycat.php\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorSearchAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t//console.log(xmlhttp.responseText);
\t\tvar mvsa = document.getElementById(\"megamenuVenSearchAnch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Searchsegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t//document.getElementById(\"megamenuVenSearchAnch\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorSearch\",true);
\txmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorReviewAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();

\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t\t//alert(xmlhttp.responseText);
\t\t\tvar mvsa = document.getElementById(\"megamenuVenReviewAnch\");
\t\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Reviewsegment-drop\") {
\t\t\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t\t\t  break;
\t\t\t\t\t}
\t\t\t}

\t\t//document.getElementById(\"megamenuVenReviewUl\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t  xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorReview\",true);
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorReview\",true);
\txmlhttp.send();
\t}


</script>

<script type=\"text/javascript\">
function show_submenu(b, menuid)
{


\tdocument.getElementById(b).style.display=\"block\";
\tif(menuid!=\"megaanchorbycat\"){
\tdocument.getElementById(menuid).className = \"setbg\";
\t}
}

function hide_submenu(b, menuid)
{


\tsetTimeout(dispminus(b,menuid), 200);



\t//document.getElementById(b).onmouseover = function() { document.getElementById(b).style.display=\"block\"; }
\t//document.getElementById(b).onmouseout = function() { document.getElementById(b).style.display=\"none\"; }
\t//document.getElementById(b).style.display=\"none\";
}

function dispminus(subid,menuid){
\t//~ if(menuid!=\"megaanchorbycat\"){
\t//~ document.getElementById(menuid).className = \"unsetbg\";
\t//~ }
\t//~ document.getElementById(subid).style.display=\"none\";


\t}


\$(document).ready(function(){
  \$(\"#flip\").click(function(){
    \$(\"#panel\").slideToggle(\"slow\");
  });

});

 \$(document).ready(function(){
      \$(\".Searchsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Searchsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });
 \$(document).ready(function(){
      \$(\".Reviewsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Reviewsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });


</script>
";
        // line 356
        $this->displayBlock('jquery', $context, $blocks);
        // line 359
        echo "
";
        // line 360
        $this->displayBlock('customcss', $context, $blocks);
        // line 363
        echo "

</head>
<body>

";
        // line 368
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 369
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 370
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 371
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 372
        echo "
";
        // line 373
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 374
            echo "<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 contact-info\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>
\t\t\t\t\t<span>(04) 42 13 777</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3 left-clear\">
\t\t\t\t\t<span class=\"contact-no\"><a href=\"";
            // line 383
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
            echo "\">Contact Us</a></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6 clear-right dash-right-top-links\">
\t\t\t\t<ul>
\t\t\t\t\t<li>Welcome, <span class=\"profile-name\">";
            // line 388
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
\t\t\t\t\t<li><a href=\"";
            // line 389
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

";
        }
        // line 397
        echo "
<div class=\"main_container\">
\t";
        // line 399
        $this->displayBlock('header', $context, $blocks);
        // line 408
        echo "\t";
        $this->displayBlock('menu', $context, $blocks);
        // line 445
        echo "

\t";
        // line 447
        $this->displayBlock('pageblocknew', $context, $blocks);
        // line 513
        echo "


\t\t";
        // line 516
        $this->displayBlock('enquiries', $context, $blocks);
        // line 541
        echo "</div>
    \t</div>
</div></div>
\t";
        // line 544
        $this->displayBlock('footer', $context, $blocks);
        // line 553
        echo "<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
<script type=\"text/javascript\">
window.onload = jsfunction();
</script>
</html>
";
    }

    // line 356
    public function block_jquery($context, array $blocks = array())
    {
        // line 357
        echo "
";
    }

    // line 360
    public function block_customcss($context, array $blocks = array())
    {
        // line 361
        echo "
";
    }

    // line 399
    public function block_header($context, array $blocks = array())
    {
        // line 400
        echo "\t<div class=\"header container\">
    \t<div class=\"col-md-6 logo\">
        \t<a href=\"";
        // line 402
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" > <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"> </a>
        </div>
        <div class=\"col-md-6\">
        </div>
    \t</div>
\t";
    }

    // line 408
    public function block_menu($context, array $blocks = array())
    {
        // line 409
        echo "\t<div class=\"menu_block\">
\t\t\t<div class=\"container\">
\t\t\t\t";
        // line 411
        $this->displayBlock('topmenu', $context, $blocks);
        // line 442
        echo "        \t</div>
  \t\t</div>
\t";
    }

    // line 411
    public function block_topmenu($context, array $blocks = array())
    {
        // line 412
        echo "\t\t\t\t\t<div class=\"navHeaderCollapse col-sm-7\">
\t\t\t\t\t\t<ul class=\"nav navbar-nav navbar-left\">
\t\t\t\t\t\t<!--<li><a href=\"";
        // line 414
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
        echo "\">Dashboard</a></li>-->
\t\t\t\t\t\t\t<!--\t<li><a href=\"";
        // line 415
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_postenquiries");
        echo "\">View All Enquires</a></li>-->
\t\t\t\t\t\t\t<li><a href=\"";
        // line 416
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
\t\t\t\t\t\t\t<li id = \"megamenuVenSearchAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Search<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Searchsegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li id = \"megamenuVenReviewAnch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Review<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Reviewsegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t<!--
\t\t\t\t\t\t<li id = \"megamenu1Anch\" class=\"dropdown\" onClick=\"javascript:hide_submenu('megamenu1', 'megamenu1Anch')\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Resource Centre<b class=\"caret\"></b></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu Resourcesegment-drop\"  role=\"menu\" aria-labelledby=\"dropdownMenu\"></ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t-->
\t\t\t\t\t\t</ul>

\t\t\t\t\t\t</div>
\t\t\t<div class=\"col-md-5 search_block\">
\t\t\t<form name=\"search\" method=\"request\" action=\"";
        // line 435
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
            \t \t<input class=\"col-sm-8 search-box\" type=\"text\" list=\"browsers\" placeholder=\"Type the service you require\" id='category' name='category' ><datalist id=\"searchval\"></datalist>
\t\t\t<input type=\"submit\" value=\"Search\" class=\"search-btn form_submit col-sm-3\" />
                 </form>
\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 447
    public function block_pageblocknew($context, array $blocks = array())
    {
        // line 448
        echo "\t\t<div class=\"page-bg\">
    \t<div class=\"container inner_container\">
\t\t<div class=\"row \">
\t\t\t<div class=\"dashboard-panel\">


    <!-- mega menu div block starts -->
\t\t";
        // line 455
        $this->displayBlock('megamenu', $context, $blocks);
        // line 472
        echo "
\t";
        // line 473
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 510
        echo "

\t";
    }

    // line 455
    public function block_megamenu($context, array $blocks = array())
    {
        // line 456
        echo "\t\t<div id=\"megamenuVenSearch\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" ><div><ul id=\"megamenuVenSearchUl\"></ul></div></div>
\t\t<div id=\"megamenuVenReview\" class=\"submenu menu-mega megamenubg-1 megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');\"><div class=\"review-menu-left\"><ul id=\"megamenuVenReviewUl\"></ul></div><div  class=\"review-menu-right\"><button class=\"btn btn-success write-button\" type=\"button\" >Write a Review</button></div></div>
\t\t<ul id=\"megamenu1\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenu1','megamenu1Anch');\" onMouseOut=\"javascript:hide_submenu12('megamenu1','megamenu1Anch');\" ></ul>
\t\t<div id=\"megamenubycat\" class=\"submenu menu-megabycat megamenubg\" onMouseOver=\"javascript:show_submenu('megamenubycat','megaanchorbycat');\" onMouseOut=\"javascript:hide_submenu('megamenubycat','megaanchorbycat');\"><div><ul id=\"megamenubycatUl\"></ul></div><div></div></div>

\t\t";
        // line 461
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 462
            echo "
\t\t<script type=\"text/javascript\">

\t\tdocument.getElementById(\"megamenuVenSearch\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\t\tdocument.getElementById(\"megamenuVenReview\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\t\tdocument.getElementById(\"megamenu1\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\t\tdocument.getElementById(\"megamenubycat\").setAttribute(\"class\", \"submenu menu-megabycat-login megamenubg\");
\t\t</script>
\t";
        }
        // line 471
        echo "\t\t";
    }

    // line 473
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 474
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 477
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
        echo "\">Customer Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"enquiries reviews\" href=\"";
        // line 478
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_postenquiries");
        echo "\">My Job Requests</a></li>
\t\t\t\t\t\t\t<li><a class=\"enquiry\" href=\"";
        // line 479
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ratings");
        echo "\">Rating & Reviews</a></li>
\t\t\t\t\t\t\t<li><a class=\"account\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">My Account <span class=\"caret\"></span></a>
\t\t\t\t\t\t\t\t<ul id=\"collapseOne\" class=\"panel-collapse collapse menu-col\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 482
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumeraccount");
        echo "\">My Profile</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 483
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumerupdatepasssword");
        echo "\">Change Password</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 484
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumerupdatemail");
        echo "\">Update Email</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 485
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumerdeleteAccount");
        echo "\">Delete Account</a>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        // line 488
        if ((array_key_exists("emailCount", $context) && ((isset($context["emailCount"]) ? $context["emailCount"] : null) != 0))) {
            // line 489
            echo "                            <li>
                            \t<a class=\"messages1\" href=\"";
            // line 490
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_messages");
            echo "\">Messages<span class=\"mess-read\">NEW</span></a>
                            </li>
                            ";
        } else {
            // line 493
            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a class=\"messages\" href=\"";
            // line 494
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_messages");
            echo "\">Messages</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        }
        // line 497
        echo "\t\t\t\t\t\t\t<li><a class=\"support\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_home");
        echo "\">Support</a>

\t\t\t\t\t\t\t\t<!--<ul class=\"ticket-links\">
\t\t\t\t\t\t\t\t<li><a class=\"creat-ticket\" href=\"";
        // line 500
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_support_consumer");
        echo "\">Creat Ticket</a></li>
\t\t\t\t\t\t\t\t<li><a class=\"view-ticket\" href=\"";
        // line 501
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_supportview_consumer");
        echo "\">View Ticket</a></li>
\t\t\t\t\t\t\t\t</ul>-->
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"get-quote-link\"><a href=\"";
        // line 504
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_quote");
        echo "\">GET QUOTES NOW</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t";
    }

    // line 516
    public function block_enquiries($context, array $blocks = array())
    {
        // line 517
        echo "        \t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t\t<div class=\"overview_block\">
\t\t\t\t\t";
        // line 519
        if (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
            // line 520
            echo "\t\t\t\t<div class=\"page-title\">
\t\t\t\t\t<h1>My Customer Account</h1>

\t\t\t\t\t\t\t<a class=\"switch-user-vendor\" href=\"";
            // line 523
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            echo "\">Switch to My Vendor Profile</a>

\t\t\t\t</div>
\t\t\t\t";
        }
        // line 527
        echo "\t\t\t\t<div>
        \t<div class=\"dashboardmain_block\">
\t\t\t\t<h3>Welcome ";
        // line 529
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ", to your personalised BusinessBid Dashboard where you can:</h3>
\t\t\t\t<ul>
\t\t\t\t\t<li>View all the job requests you have logged</li>
\t\t\t\t\t<li>View all your posted vendor reviews</strong></li>
\t\t\t\t\t<li>Update your personal profile</li>
\t\t\t\t\t<li>View all communications and messages</li>
\t\t\t\t\t<li>Log any support issues</li>
\t\t\t\t\t<li>Log new job requests</li>
\t\t\t\t</ul>
        \t</div>
        \t</div>
\t\t";
    }

    // line 544
    public function block_footer($context, array $blocks = array())
    {
        // line 545
        echo "    \t<div class=\"footer-dashboard text-center\">
\t\t<div class=\"container\">
\t\t<div class=\"col-sm-6 footer-messages\">Copyright 2013 - 2014 BBusinessBid</div>
    \t<div class=\"col-sm-6 clear-right\"></div>

    \t</div>
\t\t</div>
    \t";
    }

    public function getTemplateName()
    {
        return "::newconsumer_dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  805 => 545,  802 => 544,  786 => 529,  782 => 527,  775 => 523,  770 => 520,  768 => 519,  764 => 517,  761 => 516,  751 => 504,  745 => 501,  741 => 500,  734 => 497,  728 => 494,  725 => 493,  719 => 490,  716 => 489,  714 => 488,  708 => 485,  704 => 484,  700 => 483,  696 => 482,  690 => 479,  686 => 478,  682 => 477,  677 => 474,  674 => 473,  670 => 471,  659 => 462,  657 => 461,  650 => 456,  647 => 455,  641 => 510,  639 => 473,  636 => 472,  634 => 455,  625 => 448,  622 => 447,  611 => 435,  589 => 416,  585 => 415,  581 => 414,  577 => 412,  574 => 411,  568 => 442,  566 => 411,  562 => 409,  559 => 408,  547 => 402,  543 => 400,  540 => 399,  535 => 361,  532 => 360,  527 => 357,  524 => 356,  512 => 553,  510 => 544,  505 => 541,  503 => 516,  498 => 513,  496 => 447,  492 => 445,  489 => 408,  487 => 399,  483 => 397,  472 => 389,  468 => 388,  460 => 383,  449 => 374,  447 => 373,  444 => 372,  442 => 371,  440 => 370,  438 => 369,  436 => 368,  429 => 363,  427 => 360,  424 => 359,  422 => 356,  89 => 26,  85 => 25,  77 => 20,  73 => 19,  68 => 17,  64 => 16,  60 => 15,  56 => 14,  52 => 13,  45 => 9,  41 => 8,  36 => 6,  29 => 1,);
    }
}

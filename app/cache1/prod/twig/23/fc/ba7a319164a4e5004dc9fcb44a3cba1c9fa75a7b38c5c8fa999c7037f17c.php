<?php

/* BBidsBBidsHomeBundle:Home:node2.html.twig */
class __TwigTemplate_23fcba7a319164a4e5004dc9fcb44a3cba1c9fa75a7b38c5c8fa999c7037f17c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:node2.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container works-container\">
\t<div class=\"row\">
\t\t<div class=\"col-md-6 works-content\">
\t\t\t<h1>How It Works For Vendors</h1>
\t\t\t<p>BusinessBid is UAE's largest and most extensive one-stop portal with over 700 services which reaches out to thousands of customers every day. We help them find quality service professional and vendors like you to complete their projects.</p>
\t\t\t<p>Stay ahead of the competition and join the BusinessBid network to get access to highly targeted prospects and qualified leads that will grow your business.</p>
\t\t\t<p>Getting started is easy. Sign up today and let us help you grow your business, one homeowner at a time.</p>\t
\t\t</div>
\t\t<div class=\"col-md-6 works-video\">
\t\t\t<iframe width=\"100%\" height=\"300\" src=\"//www.youtube.com/embed/La-gaNJQ64Q?rel=0\" frameborder=\"0\" allowfullscreen></iframe>
\t\t</div>
\t\t
\t</div>
</div>

<div class=\"container works-control-block\">
\t<div class=\"works-title\">It's your business – but let us do all the hard work for you</div>
\t<div class=\"col-md-4\">
\t\t<div class=\"works-block\">
\t\t\t<h1><span>Get Noticed with Online Presence:</span></h1>
\t\t\t<p>We create web pages of your business on our portal for the customers to view your services therefore increasing your business profile </p>
\t\t</div>
\t</div>
\t<div class=\"col-md-4\">
\t\t<div class=\"works-block\">
\t\t\t<h1><span>Pay per Leads:</span></h1>
\t\t\t<p>Get access to all prospective and qualified leads but pay only for the ones you wish to undertake </p>
\t\t</div>
\t</div>
\t<div class=\"col-md-4\">
\t\t<div class=\"works-block\">
\t\t\t<h1><span>Be busy, always:</span></h1>
\t\t\t<p>Our unique lead and review management systems ensure you have access to all new projects in addition to generating repeat business</p>
\t\t</div>
\t</div>
</div>

<div class=\"container signup\">
\t<a class=\"btn btn-success signup-btn\" href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">SIGN UP TODAY</a>
</div>

";
    }

    // line 48
    public function block_maincontent($context, array $blocks = array())
    {
        // line 49
        echo "
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:node2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 49,  81 => 48,  73 => 43,  32 => 4,  29 => 3,  11 => 1,);
    }
}

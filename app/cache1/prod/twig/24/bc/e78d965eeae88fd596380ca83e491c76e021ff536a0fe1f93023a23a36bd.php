<?php

/* BBidsBBidsHomeBundle:Categoriesform:interior_designers_fitout_form.html.twig */
class __TwigTemplate_24bce78d965eeae88fd596380ca83e491c76e021ff536a0fe1f93023a23a36bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:interior_designers_fitout_form.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<style>
    .pac-container:after{
    content:none !important;
}
.mask{
    display: none;
}
</style>
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 19
            echo "
        <div class=\"alert alert-danger\">";
            // line 20
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
        ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "
        <div class=\"alert alert-success\">";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 30
            echo "
        <div class=\"alert alert-success\">";
            // line 31
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "    </div>
    <div class=\"cat-form\">
        <div class=\"page-title\"><h1>GET FREE QUOTES FROM ";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
        echo " COMPANIES IN THE UAE</h1></div>
        ";
        // line 37
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
        <div class=\"cat-form-block\">
            <div class=\"category-steps-block\">
                <div class=\"col-md-4 steps \"><span>1</span><p>Choose what services you require</p></div>
                <div class=\"col-md-4 steps\"><span>2</span><p>Vendors contact you & offer you quotes</p></div>
                <div class=\"col-md-4 steps\"><span>3</span><p>You engage the best Vendor</p></div>
            </div>
            <div class=\"cat-list-cont\">
                <div class=\"cat-list-block\" id=\"services\">
                    <div class=\"col-md-1 icon-block space-top-more\">
                        <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/int1.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>What kind of interior design and fit out services do you require?*</h2>
                        ";
        // line 51
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 52
            echo "                            <div class=\"col-md-4 sub-category\">
                                ";
            // line 53
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "int_design")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "\t\t\t\t\t\t<div class=\"error\" id=\"int_design\"></div>
                    </div>

                </div>

                <div class=\"cat-list-block\" id=\"property\">
                    <div class=\"col-md-1 icon-block\">
                        <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/int2.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>What kind of property is the service required for?*</h2>
                        <div class=\"row1 sup-top\">
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "int_property")));
        echo "
                                ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 1, array()), 'widget', array("attr" => array("class" => "int_property")));
        echo "
                                ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 1, array()), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 2, array()), 'widget', array("attr" => array("class" => "int_property")));
        echo "
                                ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 2, array()), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 3, array()), 'widget', array("attr" => array("class" => "int_property")));
        echo "
                                ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 3, array()), 'label');
        echo "
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 4, array()), 'widget', array("attr" => array("class" => "int_property")));
        echo "
                                ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type", array()), 4, array()), 'label');
        echo "
                                <div class=\"other\">";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "property_type_other", array()), 'widget', array("attr" => array("class" => "int_property")));
        echo "<span>(max 30 characters)</span> </div>
                            </div>
                        </div>
                         <div class=\"error\" id=\"int_property\"></div>
                    </div>

                </div>

                <div class=\"cat-list-block\" id=\"dimension\">
                    <div class=\"col-md-1 icon-block\">
                        <img src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/int3.png"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"col-md-11\">
                        <h2>What is the dimension of this property?*</h2>
                        <div class=\"row1 sup-top\">
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "int_dimension")));
        echo "
                                ";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 0, array(), "array"), 'label');
        echo "<sup>2</sup>
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "int_dimension")));
        echo "
                                ";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 1, array(), "array"), 'label');
        echo "<sup>2</sup>
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "int_dimension")));
        echo "
                                ";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 2, array(), "array"), 'label');
        echo "<sup>2</sup>
                            </div>
                            <div class=\"form-group col-md-4 clear-left\">
                                ";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "int_dimension")));
        echo "
                                ";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 3, array(), "array"), 'label');
        echo "<sup>2</sup>
                            </div>
                            <div class=\"form-group col-md-4 clear-left color-blue\">
                                ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 4, array(), "array"), 'widget', array("attr" => array("class" => "int_dimension")));
        echo "
                                ";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dimensions", array()), "children", array()), 4, array(), "array"), 'label');
        echo "<sup>2</sup>+
                            </div>
                        </div>
                        <div class=\"error\" id=\"int_dimension\"></div>
                    </div>

                </div>


            ";
        // line 129
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 130
            echo "            <div class=\"cat-list-block\" id=\"name\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 131
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon6.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 133
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 135
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "name")));
            echo "
                    </div>
                    <div class=\"error\" id=\"int_name\"></div>
                </div>
            </div>
            <div class=\"cat-list-block\" id=\"email_id\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 141
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon7.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 143
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 145
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "email_id")));
            echo "
                    </div>
                    <div class=\"error\" id=\"int_email\"></div>
                </div>
            </div>
            ";
        }
        // line 151
        echo "
           <div class=\"cat-list-block\" id=\"location\">
                <div class=\"col-md-1 icon-block space-top \"><img src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon8.png"), "html", null, true);
        echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>Select your Location*</h2>
                    <div class=\"row1\">
                        <div class=\"form-control cus-location\">
                            <div class=\"col-md-6 align-right\">
                                ";
        // line 159
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "int_location")));
        echo "
                                ";
        // line 160
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                            </div>
                            <div class=\"col-md-6\">
                                ";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "int_location")));
        echo "
                                ";
        // line 164
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"error\" id=\"int_location\"></div>
                </div>
            </div>


            ";
        // line 173
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            // line 174
            echo "            <div class=\"cat-list-block mask\" id=\"domCity\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 175
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-city.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 177
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 179
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_city\"></div>
                </div>
            </div>
            ";
        }
        // line 185
        echo "
            ";
        // line 186
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 187
            echo "            <div class=\"cat-list-block mask\" id=\"domArea\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 188
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/area-icon.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 190
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 192
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_area\"></div>
                </div>
            </div>
            ";
        }
        // line 198
        echo "
            ";
        // line 199
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 200
            echo "            <div class=\"row1 mask\" id=\"domContact\">
                <div class=\"col-md-6 clear-left\">
                    <div class=\"cat-list-block\" id=\"uae_mobileno\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 203
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-mobile.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 205
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left int-country\"><label><b>Area Code</b> </label>";
            // line 207
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
            echo "</div> <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget');
            echo "</div>
                            </div>
                        <div class=\"error\" id=\"acc_mobile\"></div>
                        </div>
                    </div>
                </div>
                ";
            // line 225
            echo "                <div class=\"col-md-6 clear-right\">
                    <div class=\"cat-list-block\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 227
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-phone.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 229
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left int-country\"><label><b>Area Code</b> </label> ";
            // line 231
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
            echo "</div>  <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget');
            echo " </div>
                            </div>
                        <div class=\"error\"></div>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }
        // line 239
        echo "
            ";
        // line 240
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 241
            echo "            <div class=\"cat-list-block mask\" id=\"intCountry\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 242
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-country.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 244
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 246
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_intcountry\"></div>
                </div>
            </div>

            <div class=\"cat-list-block mask\" id=\"intCity\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
            // line 253
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-city.png"), "html", null, true);
            echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
            // line 255
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h2>
                    <div class=\"row1\">
                        ";
            // line 257
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget');
            echo "
                    </div>
                <div class=\"error\" id=\"acc_intcity\"></div>
                </div>
            </div>
            ";
        }
        // line 263
        echo "
            ";
        // line 264
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 265
            echo "            <div class=\"row clear-left clear-right mask\" id=\"intContact\">
                <div class=\"col-md-6 clear-left\">
                    <div class=\"cat-list-block\" id=\"int_mobileno\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 268
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-mobile.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 270
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left  int-country\"><label><b>Country Code</b> <i>+</i></label>";
            // line 272
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget');
            echo "</div> <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget');
            echo "</div>
                            </div>
                        <div class=\"error\" id=\"acc_intmobile\"></div>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6 clear-left\">
                    <div class=\"cat-list-block\">
                        <div class=\"col-md-2 icon-block space-top1\"> <img src=\"";
            // line 280
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon-phone.png"), "html", null, true);
            echo "\"></div>
                        <div class=\"col-md-10\">
                            <h2>";
            // line 282
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h2>
                            <div class=\"row1\">
                                <div class=\"col-md-6 clear-left  int-country\"><label><b>Country Code</b> <i>+</i></label> ";
            // line 284
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget');
            echo "</div>  <div class=\"col-md-6 clear-right\"> ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget');
            echo " </div>
                            </div>
                        <div class=\"error\" ></div>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }
        // line 292
        echo "
            <div class=\"cat-list-block\">
                <div class=\"col-md-1 icon-block space-top\"> <img src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon4.png"), "html", null, true);
        echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
        // line 296
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'label');
        echo "</h2>
                    <div class=\"row1\">
                        ";
        // line 298
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                <div class=\"error\" id=\"acc_hire\"></div>
                </div>
            </div>

            <div class=\"cat-list-block\">
                <div class=\"col-md-1 icon-block space-top-more\"> <img src=\"";
        // line 305
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/icon9.png"), "html", null, true);
        echo "\"></div>
                <div class=\"col-md-11\">
                    <h2>";
        // line 307
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label');
        echo "</h2>
                    <div class=\"row1\">
                        ";
        // line 309
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "
                        <span>(max 120 characters)</span>
                    </div>
                <div class=\"error\" id=\"acc_desc\"></div>
                </div>
            </div>




                <script type=\"text/javascript\" src=\"https://www.google.com/recaptcha/api/challenge?k=6LcCrQMTAAAAAA3zTbRoCk0tmgghhTt_onX5-gMz\"></script>
                <noscript>
               <iframe src=\"https://www.google.com/recaptcha/api/noscript?k=6LcCrQMTAAAAAA3zTbRoCk0tmgghhTt_onX5-gMz\"
                   height=\"300\" width=\"500\" frameborder=\"0\"></iframe><br>
             </noscript>
             <div class=\"extra-text\">
                 <div class=\"extra-block\">
                     <ul>
                        <li>Enter text then click on button below to get quotes</li>
                        <li>Your privacy is important, we do not provide information to third parties</li>
                     </ul>
                 </div>
             </div>
        <div class=\"cat-submit\">";
        // line 332
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "GET QUOTES NOW"));
        echo "</div>
            </div>
            ";
        // line 334
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
        </div>
    </div>


</div>


<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>

<script type=\"text/javascript\" src=\"";
        // line 349
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/interior_design.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 350
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/categoriesform.general.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:interior_designers_fitout_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  675 => 350,  671 => 349,  653 => 334,  648 => 332,  622 => 309,  617 => 307,  612 => 305,  602 => 298,  597 => 296,  592 => 294,  588 => 292,  575 => 284,  570 => 282,  565 => 280,  552 => 272,  547 => 270,  542 => 268,  537 => 265,  535 => 264,  532 => 263,  523 => 257,  518 => 255,  513 => 253,  503 => 246,  498 => 244,  493 => 242,  490 => 241,  488 => 240,  485 => 239,  472 => 231,  467 => 229,  462 => 227,  458 => 225,  447 => 207,  442 => 205,  437 => 203,  432 => 200,  430 => 199,  427 => 198,  418 => 192,  413 => 190,  408 => 188,  405 => 187,  403 => 186,  400 => 185,  391 => 179,  386 => 177,  381 => 175,  378 => 174,  376 => 173,  364 => 164,  360 => 163,  354 => 160,  350 => 159,  341 => 153,  337 => 151,  328 => 145,  323 => 143,  318 => 141,  309 => 135,  304 => 133,  299 => 131,  296 => 130,  294 => 129,  282 => 120,  278 => 119,  272 => 116,  268 => 115,  262 => 112,  258 => 111,  252 => 108,  248 => 107,  242 => 104,  238 => 103,  229 => 97,  216 => 87,  212 => 86,  208 => 85,  202 => 82,  198 => 81,  192 => 78,  188 => 77,  182 => 74,  178 => 73,  172 => 70,  168 => 69,  159 => 63,  150 => 56,  139 => 53,  136 => 52,  132 => 51,  125 => 47,  112 => 37,  108 => 36,  104 => 34,  95 => 31,  92 => 30,  87 => 29,  78 => 26,  75 => 25,  71 => 24,  68 => 23,  59 => 20,  56 => 19,  52 => 18,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

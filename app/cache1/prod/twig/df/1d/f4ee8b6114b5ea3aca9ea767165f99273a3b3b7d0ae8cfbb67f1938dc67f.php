<?php

/* BBidsBBidsHomeBundle:Admin:enquiryview.html.twig */
class __TwigTemplate_df1df4ee8b6114b5ea3aca9ea767165f99273a3b3b7d0ae8cfbb67f1938dc67f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:enquiryview.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
<div class=\"page-title\"><h1>Job Request Information<br>";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            echo " Job Request Number ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</h1></div>

<table class=\"col-sm-6 vendor-user gray-bg vendor-enquiries\" width=\"100%\">
    ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["enquiry"]) {
            // line 9
            echo "    <tr><td><strong>Date Recieved</strong></td><td> ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "   </td></tr>
    <tr><td width=\"30%\"><strong>Subject</strong></td><td width=\"70%\"> ";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "subj", array()), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Category</strong></td><td> ";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : null), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Sub Category</strong></td><td> ";
            // line 12
            echo twig_escape_filter($this->env, (isset($context["subCategoryString"]) ? $context["subCategoryString"] : null), "html", null, true);
            echo " </td></tr>

    <tr><td><strong>City</strong></td><td>  ";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "city", array()), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Location</strong></td><td>\t";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "location", array()), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Name</strong></td><td>  ";
            // line 16
            echo twig_escape_filter($this->env, (isset($context["name2"]) ? $context["name2"] : null), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Updated Date</strong></td><td>  ";
            // line 17
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquiry"], "updated", array()), "Y-m-d h:i:s"), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Email</strong></td><td> ";
            // line 18
            echo twig_escape_filter($this->env, (isset($context["email2"]) ? $context["email2"] : null), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Contact Number</strong></td><td>    ";
            // line 19
            echo twig_escape_filter($this->env, (isset($context["mobile"]) ? $context["mobile"] : null), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Status</strong></td><td>    ";
            // line 20
            if (($this->getAttribute($context["enquiry"], "status", array()) == 1)) {
                echo " Active ";
            } elseif (($this->getAttribute($context["enquiry"], "status", array()) == 2)) {
                echo " Inactive ";
            } elseif (($this->getAttribute($context["enquiry"], "status", array()) == 3)) {
                echo " Blocked ";
            }
            echo "</td></tr>
    <tr><td><strong>Description</strong></td><td>   ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquiry"], "description", array()), "html", null, true);
            echo " </td></tr>
    <tr><td><strong>Matched vendors</strong></td><td> <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_mapped_vendors", array("enquiryid" => $this->getAttribute($context["enquiry"], "id", array()))), "html", null, true);
            echo "\" > View matched Vendors </a> </td></tr>
\t<tr><td><strong>When are you looking to hire?</strong></td><td>";
            // line 23
            if (($this->getAttribute($context["enquiry"], "hirePriority", array()) == 1)) {
                echo " Urgent ";
            } elseif (($this->getAttribute($context["enquiry"], "hirePriority", array()) == 2)) {
                echo "1-3 Days";
            } elseif (($this->getAttribute($context["enquiry"], "hirePriority", array()) == 3)) {
                echo "4-7 Days";
            } elseif (($this->getAttribute($context["enquiry"], "hirePriority", array()) == 4)) {
                echo "7-14 Days";
            } elseif (($this->getAttribute($context["enquiry"], "hirePriority", array()) == 5)) {
                echo "Just Planning";
            }
            echo "</td> </tr>
    <!--<tr><td><strong>Vendor Response</strong></td><td> <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_vendor_response", array("enquiryid" => $this->getAttribute($context["enquiry"], "id", array()))), "html", null, true);
            echo "\"> View Vendor Response </a> </td></tr>-->

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquiry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "
</table>
</div>
<div class=\"container\">
    <div class=\"page-title\"><h1>Additional Information</h1></div>
<style type=\"text/css\">
.tg{border-collapse:collapse;border-spacing:0;border-color:#ccc}.tg td{font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff}.tg th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0}.tg .tg-4eph{background-color:#f9f9f9}
</style>
<table class=\"tg\">
  <tr>
    <th class=\"tg-031e\">Tag</th>
    <th class=\"tg-031e\">Value</th>
  </tr>
  ";
        // line 40
        $context["rowColor"] = "";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["formExtraFields"]) ? $context["formExtraFields"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 42
            echo "  ";
            if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                echo " ";
                $context["rowColor"] = "tg-4eph";
                echo " ";
            } else {
                echo " ";
                $context["rowColor"] = "tg-031e";
                echo " ";
            }
            // line 43
            echo "  <tr>
    <td class=\"";
            // line 44
            echo twig_escape_filter($this->env, (isset($context["rowColor"]) ? $context["rowColor"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fields"], "keyName", array()), "html", null, true);
            echo "</td>
    <td class=\"";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["rowColor"]) ? $context["rowColor"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fields"], "keyValue", array()), "html", null, true);
            echo "</td>
  </tr>
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "</table>
</div>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:enquiryview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 48,  190 => 45,  184 => 44,  181 => 43,  170 => 42,  153 => 41,  151 => 40,  136 => 27,  127 => 24,  113 => 23,  109 => 22,  105 => 21,  95 => 20,  91 => 19,  87 => 18,  83 => 17,  79 => 16,  75 => 15,  71 => 14,  66 => 12,  62 => 11,  58 => 10,  53 => 9,  49 => 8,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}

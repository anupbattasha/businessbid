<?php

/* BBidsBBidsHomeBundle:Admin:userlist.html.twig */
class __TwigTemplate_ea1e691619d8b6a7e0bef4948b922df33261988696496d77d9c1e6f382d98396 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:userlist.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).ready(function() {

    \$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
    \$.datepicker.regional[\"\"].changeMonth = true;
    \$.datepicker.regional[\"\"].changeYear = true;
    \$.datepicker.setDefaults(\$.datepicker.regional['']);

    \$('#example').dataTable({
        \"sPaginationType\": \"full_numbers\",
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sEmptyTable\": '',
            \"sInfoEmpty\": '',
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
        \"sEmptyTable\": \"There are no records\",
        \"sAjaxSource\": \"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/userlist.php"), "html", null, true);
        echo "\",
        \"fnServerParams\": function(aoData) {
            if (aoData[31].value != '') {
                aoData[31].value = (aoData[31].value == 'Active') ? 1 : 0;
            }
            if (aoData[19].value != '') {
                aoData[19].value = (aoData[19].value == 'Customer') ? 2 : 3;
            }
        },
        \"fnRowCallback\": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            \$(nRow).children().each(function(index, td) {
            \tif(index == 7){
            \t\tif (\$(td).html() == 0) {
            \t\t\t\$(td).css(\"background-color\", \"#FFDE00\");
            \t\t}
            \t}
            })

        }
    }).columnFilter({
        aoColumns: [{
            type: \"date-range\",
            sSelector: \"#dateFilter\",
            sRangeFormat: \"<label>Date</label> {from} {to}\"
        }, {
            type: 'select',
            values: [\"Customer\", \"Vendor\"],
            sSelector: '#typeFilter'
        }, null, null, null, {
            type: 'select',
            values: [\"Active\", \"Inactive\"],
            sSelector: '#statusFilter'
        }, null, {
            type: 'select',
            values: [\"All\", \"Zero\"],
            sSelector: '#loginFilter'
        },null]
    });
});
</script>
<script type=\"text/javascript\">

 function deletefunc(userid, username){


\tvar con = confirm('Are you sure to delete '+username+' ? The record once deleted cannot be retrived back');
\tif(con){
\t\tvar url = \"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_user_delete", array("userid" => "USERID"));
        echo "\";
\t\tvar urlset = url.replace('USERID', userid);
\t\t\twindow.open(urlset,\"_self\");
\t\t\t//alert(urlset);
\t\treturn true;
\t}
\telse
\t{
\t\treturn false;
\t}
 }
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Users List</h1></div>
";
        // line 105
        echo "<div>
\t";
        // line 106
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 107
            echo "
\t<div class=\"alert alert-danger\">";
            // line 108
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "
\t";
        // line 112
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 113
            echo "
\t<div class=\"alert alert-success\">";
            // line 114
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "
\t";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 119
            echo "
\t<div class=\"alert alert-success\">";
            // line 120
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "
    <div class=\"admin_filter report-filter\">
        <div class=\"col-md-3 side-clear\" id=\"dateFilter\"></div>
        <div class=\"col-md-3\" id=\"typeFilter\"></div>
        <div class=\"col-md-3\" id=\"statusFilter\"></div>
        <div class=\"col-md-3 clear-right\" id=\"loginFilter\"></div>
    </div>

";
        // line 141
        echo "<div class=\"latest-orders\">
<table id=\"example\">
\t<thead>
\t<tr>
\t\t<th>Date Created</th>
        <th>Profile</th>
        <th>Email</th>
        <th>Action</th>
        <th>Contact Number</th>
        <th>Status</th>
        <th>Name</th>
        <th>Login Count</th>
        <th>Last Access Date</th>
\t</tr>
\t</thead>
\t<tbody><tr><td colspan=\"9\" class=\"dataTables_empty\">Loading data from server... Please wait</td></tr></tbody>
\t<tfoot>
\t<tr>
\t\t<th>Date Created</th>
        <th>Profile</th>
        <th>Email</th>
        <th>Action</th>
        <th>Contact Number</th>
        <th>Status</th>
        <th>Name</th>
        <th>Login Count</th>
        <th>Last Access Date</th>
\t</tr>
\t</tfoot>
</table>
</div>
</div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:userlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 141,  205 => 123,  196 => 120,  193 => 119,  189 => 118,  186 => 117,  177 => 114,  174 => 113,  170 => 112,  167 => 111,  158 => 108,  155 => 107,  151 => 106,  148 => 105,  130 => 87,  80 => 40,  44 => 7,  40 => 6,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}

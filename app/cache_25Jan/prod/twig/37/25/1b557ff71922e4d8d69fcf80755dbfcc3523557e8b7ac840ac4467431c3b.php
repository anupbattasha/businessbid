<?php

/* BBidsBBidsHomeBundle:Home:consumer.html.twig */
class __TwigTemplate_37251b557ff71922e4d8d69fcf80755dbfcc3523557e8b7ac840ac4467431c3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:consumer.html.twig", 1);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:consumer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:User:consumerview.html.twig */
class __TwigTemplate_02468b19e2f885665ee9a9cc0a1cc64cc09febae52987021a4a294f8f27b2320 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:consumerview.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>Mapped Customer</h1></div>
<div>
\t";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "
\t<div class=\"alert alert-danger\">";
            // line 10
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "\t
\t";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "
\t<div class=\"alert alert-success\">";
            // line 16
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "\t
\t";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 21
            echo "
\t<div class=\"alert alert-success\">";
            // line 22
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "</div>
<div class=\"col-sm-6 vendor-user gray-bg\">
\t";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) ? $context["user"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 28
            echo "\t\t
\t\t<div class=\"field-item\"><label>Email Address</label><div class=\"field-value\"> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "email", array()), "html", null, true);
            echo "</div></div>


\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
\t";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["account"]) ? $context["account"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 35
            echo "\t\t<div class=\"field-item\"><label>Name</label><div class=\"field-value\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "contactname", array()), "html", null, true);
            echo "</div></div>
\t\t<div class=\"field-item\"><label>Address</label><div class=\"field-value\">";
            // line 36
            if (twig_test_empty($this->getAttribute($context["a"], "address", array()))) {
                echo " N/A ";
            } else {
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "address", array()), "html", null, true);
            }
            echo "</div></div>
\t\t<div class=\"field-item\"><label>Mobile Number</label><div class=\"field-value\">";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "smsphone", array()), "html", null, true);
            echo "</div></div>
\t\t<div class=\"field-item\"><label>Home Phone</label><div class=\"field-value\"> ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "homephone", array()), "html", null, true);
            echo "</div></div>
\t\t<div class=\"field-item\"><label>Description</label><div class=\"field-value\">";
            // line 39
            if (twig_test_empty($this->getAttribute($context["a"], "description", array()))) {
                echo " N/A ";
            } else {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "description", array()), "html", null, true);
                echo " ";
            }
            echo "</div></div>
\t\t
\t\t

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["enquiry"]) ? $context["enquiry"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 45
            echo "\t<div class=\"field-item\"><label>Job Request</label><div class=\"field-value\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "subj", array()), "html", null, true);
            echo "</div></div>
\t
\t\t\t<div class=\"field-item\"><label>Location</label><div class=\"field-value\"> ";
            // line 47
            if (twig_test_empty($this->getAttribute($context["e"], "location", array()))) {
                echo " N/A ";
            } else {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "location", array()), "html", null, true);
            }
            echo "</div></div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:consumerview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 49,  168 => 47,  162 => 45,  157 => 44,  140 => 39,  136 => 38,  132 => 37,  124 => 36,  119 => 35,  115 => 34,  112 => 33,  102 => 29,  99 => 28,  95 => 27,  91 => 25,  82 => 22,  79 => 21,  75 => 20,  72 => 19,  63 => 16,  60 => 15,  56 => 14,  53 => 13,  44 => 10,  41 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}

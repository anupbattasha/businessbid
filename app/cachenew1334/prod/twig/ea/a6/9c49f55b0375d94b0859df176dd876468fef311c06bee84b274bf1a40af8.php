<?php

/* BBidsBBidsHomeBundle:Categoriesform:event_managment_form.html.twig */
class __TwigTemplate_eaa69c49f55b0375d94b0859df176dd876468fef311c06bee84b274bf1a40af8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:event_managment_form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Solicit Free Event Management Quotes Now";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "
<style>
    .pac-container:after{
        content:none !important;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 20
            echo "
        <div class=\"alert alert-danger\">";
            // line 21
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
        ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
        <div class=\"alert alert-success\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "
        <div class=\"alert alert-success\">";
            // line 32
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "    </div>
<div class=\"cat-form\">
  <div class=\"photo_form_one_area modal1\">
    <h1>Looking for Event Management in the UAE</h1>
    <p>Tell us bit about your requirement and get free quotes from experienced event management companies in under a minute. Compare quote and hire the best one.
      <br>OR<br><br><span class=\"call-us\">CALL (04) 4213777</span></p>
  </div>

        ";
        // line 43
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "category_form")));
        echo "
        <div id=\"step1\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 1 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 77
        $context["oddoreven"] = "even";
        // line 78
        echo "          ";
        $context["eventLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_services", array())) - 1);
        // line 79
        echo "          ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_services", array())) % 2 == 1)) {
            // line 80
            echo "            ";
            $context["oddoreven"] = "odd";
            // line 81
            echo "          ";
        }
        // line 82
        echo "          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form eventService\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/event.png"), "html", null, true);
        echo "\" alt=\"Event\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                    <h3>What event is the services required for?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_services", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 91
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 92
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"child_pho_table_left_inner_text\" >
                                ";
                // line 94
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo "
                                ";
                // line 95
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_other", array()), 'widget', array("attr" => array("class" => "event_other")));
                echo "
                                <h5>(max 30 characters)</h5>
                            </div>
                          </div>
                        ";
            } else {
                // line 100
                echo "                          <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                            <div class=\"";
                // line 101
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["eventLength"]) ? $context["eventLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                ";
                // line 102
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                            </div>
                          </div>
                        ";
            }
            // line 106
            echo "                      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "                      ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 108
            echo "                        <div class=\"child_pho_table_right\">
                           <div class=\"child_pho_table_left_inner_text\" ></div>
                       </div>
                      ";
        }
        // line 112
        echo "                      ";
        // line 133
        echo "                      <div class=\"error\" id=\"event_services\"></div>
                  </div>
                </div>
          </div>
        </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/what.png"), "html", null, true);
        echo "\" alt=\"What\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>What services do you require?</h3>
                      <div class=\"child_pro_tabil_main kindofService\">
                          ";
        // line 146
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 147
            echo "                            <div class=\"child_pho_table_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                              <div class=\"child_pho_table_left_inner\" >
                                  ";
            // line 149
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
                              </div>
                            </div>
                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 153
        echo "                          ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory", array())) % 2 == 1)) {
            // line 154
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner\" ></div>
                           </div>
                          ";
        }
        // line 158
        echo "                          <div class=\"error\" id=\"kind_service\"></div>
                      </div>
                  <div class=\"photo_contunue_btn proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue1\">CONTINUE</a>
                  </div>
                  </div>
              </div>
          </div>
          </div>


        ";
        // line 170
        echo "        <div id=\"step2\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 2 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form eventPlaned\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/scheduled.png"), "html", null, true);
        echo "\" alt=\"Scheduled\">
                  </div>
                  <div class=\"inner_photo_one_form_table software\">
                      <h3>What date is this event scheduled for?</h3>
                      ";
        // line 210
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "event_planed", array()), 'widget', array("attr" => array("class" => "event")));
        echo "
                  </div>
                  <div class=\"error\" id=\"event_planed\"></div>
              </div>
          </div>

          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/duration.png"), "html", null, true);
        echo "\" alt=\"Duration\">
                  </div>
                  <div class=\"inner_photo_one_form_table eventDuration\">
                      <h3>Is this event duration over?</h3>
                      <div class=\"child_pro_tabil_main\">
                          <div class=\"child_pho_table_left\">
                             <div class=\"child_pho_table_left_inner\">
                                 ";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "duration", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "live1")));
        echo "
                                    ";
        // line 227
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "duration", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                             </div>
                          </div>
                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_right_inner \">
                                 ";
        // line 232
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "duration", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "live1")));
        echo "
                                    ";
        // line 233
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "duration", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                             </div>
                          </div>
                          <div class=\"error\" id=\"event_duration\"></div>
                      </div>
                  </div>
              </div>
          </div>

          <div class=\"photo_one_form\" id=\"musicServicesBlock\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/music.png"), "html", null, true);
        echo "\" alt=\"Music\">
              </div>
              <div class=\"inner_photo_one_form_table musicServices\">
                  <h3>Which of the following Music Services do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 252
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "musics")));
        echo "
                            ";
        // line 253
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 256
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "musics")));
        echo "
                            ";
        // line 257
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 262
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "musics")));
        echo "
                            ";
        // line 263
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 266
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "musics")));
        echo "
                            ";
        // line 267
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_services", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"error\" id=\"music_services\"></div>
                  </div>
              </div>
            </div>
          </div>
          <div class=\"photo_one_form\" id=\"entertainersBlock\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 278
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/entertainers.png"), "html", null, true);
        echo "\" alt=\"Entertainers\">
              </div>
              <div class=\"inner_photo_one_form_table entertainers\">
                  <h3>What type of Entertainers would you like?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 285
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 286
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 289
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 290
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 293
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 4, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 294
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 4, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 297
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 6, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 298
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 6, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 303
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 304
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 307
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 308
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 311
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 5, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 312
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 5, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 315
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 7, array(), "array"), 'widget', array("attr" => array("class" => "entertainers")));
        echo "
                            ";
        // line 316
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entertainers", array()), "children", array()), 7, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"error\" id=\"event_entertainers\"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class=\"photo_one_form\" id=\"modelsBlock\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 328
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/models.png"), "html", null, true);
        echo "\" alt=\"Models\">
              </div>
              <div class=\"inner_photo_one_form_table models\">
                  <h3>How many Models do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 333
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "models", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["model"]) {
            // line 334
            echo "                      <div class=\"child_accounting_three1_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                        ";
            // line 335
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["model"], 'widget', array("attr" => array("class" => "model")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["model"], 'label');
            echo "
                      </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['model'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 338
        echo "                    <div class=\"child_accounting_three1_left\">
                      ";
        // line 339
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "models_none", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "models_noneed")));
        echo "
                      ";
        // line 340
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "models_none", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                    </div>
                    <div class=\"child_accounting_three1_right innertext2\">
                    </div>
                      ";
        // line 373
        echo "                      <div class=\"error\" id=\"event_models\"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class=\"photo_one_form\" id=\"hostsBlock\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 382
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/hosts.png"), "html", null, true);
        echo "\" alt=\"Hosts\">
              </div>
              <div class=\"inner_photo_one_form_table hosts\">
                  <h3>How many Hosts & Hostesses do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 387
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hosts", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["host"]) {
            // line 388
            echo "                      <div class=\"child_accounting_three1_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                        ";
            // line 389
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["host"], 'widget', array("attr" => array("class" => "host")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["host"], 'label');
            echo "
                      </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['host'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 392
        echo "                    <div class=\"child_accounting_three1_left\">
                    ";
        // line 393
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hosts_none", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "hosts_noneed")));
        echo "
                    ";
        // line 394
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hosts_none", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                    </div>
                    <div class=\"child_accounting_three1_right innertext2\">
                    </div>
                      ";
        // line 424
        echo "                      <div class=\"error\" id=\"event_host\"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class=\"photo_one_form\" id=\"staffBlock\">
            <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 433
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/staff.png"), "html", null, true);
        echo "\" alt=\"Staff\">
              </div>
              <div class=\"inner_photo_one_form_table staff\">
                  <h3>How many Waiting Staff do you require?</h3>
                  <div class=\"child_pro_tabil_main\">
                      <div class=\"child_pho_table_left\">
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 440
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 441
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 0, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_left_inner\">
                             ";
        // line 444
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 2, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 445
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 2, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 450
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 451
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 1, array(), "array"), 'label');
        echo "
                         </div>
                         <div class=\"child_pho_table_right_inner\">
                             ";
        // line 454
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 3, array(), "array"), 'widget', array("attr" => array("class" => "meal")));
        echo "
                            ";
        // line 455
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "staff", array()), "children", array()), 3, array(), "array"), 'label');
        echo "
                         </div>
                      </div>
                      <div class=\"error\" id=\"event_staff\"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 467
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/guests.png"), "html", null, true);
        echo "\" alt=\"Guests\">
                  </div>
                  <div class=\"inner_photo_one_form_table guests\">
                      <h3>How many guests will be attending the event?</h3>
                       <div class=\"child_pro_tabil_main\">
                        ";
        // line 472
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "guests", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["guest"]) {
            // line 473
            echo "                          <div class=\"child_accounting_three1_";
            echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "\">
                            ";
            // line 474
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["guest"], 'widget', array("attr" => array("class" => "guest")));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["guest"], 'label');
            echo "
                          </div>
                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['guest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 477
        echo "                      ";
        // line 510
        echo "                          <div class=\"error\" id=\"event_guests\"></div>
                      </div>
                      <div class=\"inner_photo_one_form_table\">
                        <div class=\"proceed\">
                            <a href=\"javascript:void(0)\" id=\"continue2\" class=\"button3\"><button type=\"button\">CONTINUE</button></a>
                        </div>
                        <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback1\">BACK</a></div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      ";
        // line 523
        echo "      ";
        // line 524
        echo "         <div id=\"step3\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                  <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 3 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 557
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 558
            echo "          <div class=\"photo_form_main\">
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form name\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 562
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/name.png"), "html", null, true);
            echo "\" alt=\"Name\">
                  </div>
                  <div class=\"inner_photo_one_form_table\" >
                      <h3>";
            // line 565
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h3>
                          ";
            // line 566
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "form-input name")));
            echo "
                      <div class=\"error\" id=\"name\" ></div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form  email\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 574
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/email.png"), "html", null, true);
            echo "\" alt=\"Email\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 577
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo " </h3>
                     ";
            // line 578
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-input email_id")));
            echo "
                     <div class=\"error\" id=\"email\" ></div>
                  </div>
              </div>
          </div>
          ";
        }
        // line 584
        echo "          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form location\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 587
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location.png"), "html", null, true);
        echo "\" alt=\"Location\">
                  </div>
                  <div class=\"inner_photo_one_form_table res2\" >
                      <h3>Select your Location*</h3>
                      <div class=\"child_pro_tabil_main res21\">
                          <div class=\"child_accounting_three_left\">
                              ";
        // line 593
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => " land_location")));
        echo "
                                  <p>";
        // line 594
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"child_accounting_three_right\">
                             ";
        // line 597
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "land_location")));
        echo "
                                  <p>";
        // line 598
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"error\" id=\"location\"></div>
                      </div>
                  </div>
              </div>
          </div>


              ";
        // line 607
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            echo " <div class=\"photo_one_form for-uae \">
              <div class=\"inner_photo_one_form mask domcity_cont\" id=\"domCity\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
            // line 611
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <h3>";
            // line 614
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h3>
                    <div class=\"child_pro_tabil_main city_cont\">
                      ";
            // line 616
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "")));
            echo "
                    </div>
                  </div>
                </div>
                <div class=\"error\" id=\"acc_city\"></div>
              </div>
              ";
        }
        // line 623
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 624
            echo "              <div class=\"inner_photo_one_form mask\" id=\"domArea\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 626
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location icon.png"), "html", null, true);
            echo "\" alt=\"Location Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 629
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h3>
                     ";
            // line 630
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_area\"></div>
              </div>
              ";
        }
        // line 635
        echo "
              ";
        // line 636
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 637
            echo "              <div class=\"mask\" id=\"domContact\" >
                <div class=\"inner_photo_one_form\" style=\"margin-top: 40px;\" id=\"uae_mobileno\">
                    <div class=\"inner_photo_one_form_img\">
                        <img src=\"";
            // line 640
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                    </div>
                    <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                        <h3>";
            // line 643
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h3>


                        <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                        ";
            // line 647
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget');
            echo "
                        ";
            // line 648
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                    </div>
                    <div class=\"error\" id=\"acc_mobile\"></div>
                </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top: 40px;\" >
                        <div class=\"inner_photo_one_form_img\">
                            <img src=\"";
            // line 655
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                        </div>
                        <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                            <h3>";
            // line 658
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h3>


                             <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                            ";
            // line 662
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget');
            echo "
                            ";
            // line 663
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "

                        </div>
                    </div>
              </div></div>
                    ";
        }
        // line 669
        echo "

            <div class=\"photo_one_form for-foreign\">
              ";
        // line 672
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 673
            echo "              <div class=\"inner_photo_one_form mask intCountry_cont\" id=\"intCountry\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 675
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/international icon.png"), "html", null, true);
            echo "\" alt=\"international Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 678
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h3>
                     ";
            // line 679
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcountry\"></div>
              </div>
              <div class=\"inner_photo_one_form mask\" id=\"intCity\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 685
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 688
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h3>
                      ";
            // line 689
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget', array("attr" => array("class" => "form-input")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcity\"></div>
              </div>
              ";
        }
        // line 694
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 695
            echo "              <div class=\"mask\" id=\"intContact\">
                <div class=\"inner_photo_one_form\" style=\"margin-top:40px;\" id=\"int_mobileno\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 698
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 701
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 703
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget');
            echo "
                      ";
            // line 704
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intmobile\"></div>
              </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 710
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 713
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 715
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget');
            echo "
                      ";
            // line 716
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget', array("attr" => array("class" => "form-input1")));
            echo "
                  </div>
              </div>
             </div>
              ";
        }
        // line 721
        echo "            </div>

            <div class=\"photo_one_form\" id=\"hireBlock\">
              <div class=\"inner_photo_one_form hire_cont\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 727
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/looking to hire.png"), "html", null, true);
        echo "\" alt=\"Looking To Hire\">
                  </div>
                  <div class=\"inner_photo_one_form_table resp1\">
                    <h3>When are you looking to hire?*</h3>
                    <div class=\"child_pro_tabil_main  \">
                      ";
        // line 732
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                  </div>
                </div>
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 737
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/information.png"), "html", null, true);
        echo "\" alt=\"Information\">
                </div>
                <div class=\"inner_photo_one_form_table resp1\">
                    <h3>Is there any other additional information you would like to provide?</h3>
                    ";
        // line 741
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget');
        echo "
                     <br> <h5 class=\"char\">(max 120 characters)</h5>
                </div>
                ";
        // line 751
        echo "                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue3\" class=\"button3\">";
        // line 753
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "CONTINUE", "attr" => array("class" => "")));
        echo "</a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback2\">BACK</a></div>
                </div>
              </div>
          </div>
        </div>
        </script></div>
        ";
        // line 762
        echo "        ";
        // line 763
        echo "        <div id=\"step4\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <p>Page 4 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <h3 class=\"thankyou-heading\"><b>Thank you, you’re done!</b></h3>
              <p class=\"thankyou-content\" id=\"enquiryResult\">Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!</p>
              <p class=\"thankyou-content\">If you thought BusinessBid was useful to you, why not share the love? We’d greatly appreciate it. Like us on
              Facebook or follow us on Twitter to to keep updated on all the news and best deals.</p>
              <div class=\"social-icon\">
                <a href=\"https://twitter.com/\"><img src=\"";
        // line 802
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/twitter.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Twitter Bird\"/></a>
                <a href=\"https://www.facebook.com/\"><img src=\"";
        // line 803
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Facebook Sign\" /></a>
              </div>
              <div id=\"thankyou-btn\"  class=\"inner_photo_one_form\">
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 807
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">LOG ANOTHER REQUEST</a>
              </div>
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 810
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" >CONTINUE TO HOMEPAGE</a>
              </div>
              </div>
          </div>
        </div>
        ";
        // line 816
        echo "           ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
</div>
</div>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script type=\"text/javascript\">
    function scrollBox (tag) {
        \$('html,body').animate({scrollTop: tag.position().top -10},'slow');
    }
    \$(document).ready(function () {
        var forms = [
            '[ name=\"";
        // line 831
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
          ];
        var validationFlag = true;
          \$( forms.join(',') ).submit( function( e ){
            e.preventDefault();
            console.log(\"tried submitting\")
            if(validationFlag) {
                postForm( \$(this), function( response ){
                    if(response.hasOwnProperty('error')) {
                        alert(response.error);
                        var i = 1;
                        for (i = 1; i < 5; i++) {
                            \$(\"div#step\"+i).hide();
                        };
                        \$(\"div#step\"+response.step).show();
                    } else {
                        var url = '";
        // line 847
        echo $this->env->getExtension('routing')->getPath("bizbids_job_post_sucessful", array("eid" => "jobNo"));
        echo "';
url = url.replace(\"jobNo\", response.jobNo);
window.location.href = url;
                        \$(\"#category_form\")[0].reset();
                    }
                });
                ga('send', 'event', 'Button', 'Click', 'Form Sent');
            }
            return false;
          });
        function postForm( \$form, callback ) {
            NProgress.start();
              \$.ajax({
                type        : \$form.attr( 'method' ),
                beforeSend  : function() { NProgress.inc() },
                url         : \$form.attr( 'action' ),
                data        : \$form.serialize(),
                success     : function(data) {
                  callback( data );
                  NProgress.done();
                },
                error : function (xhr) {
                    alert(\"Error occured.please try again\");
                    NProgress.done();
                }
              });
        }
    })
</script>
<script type=\"text/javascript\" src=\"";
        // line 876
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 877
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script type=\"text/javascript\" src=\"";
        // line 878
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("js/reviewsformsvalidation/" . (isset($context["twigFileName"]) ? $context["twigFileName"] : null)) . ".min.js")), "html", null, true);
        echo "\" ></script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:event_managment_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1443 => 878,  1439 => 877,  1435 => 876,  1403 => 847,  1384 => 831,  1365 => 816,  1357 => 810,  1351 => 807,  1344 => 803,  1340 => 802,  1299 => 763,  1297 => 762,  1286 => 753,  1282 => 751,  1276 => 741,  1269 => 737,  1261 => 732,  1253 => 727,  1245 => 721,  1237 => 716,  1233 => 715,  1228 => 713,  1222 => 710,  1213 => 704,  1209 => 703,  1204 => 701,  1198 => 698,  1193 => 695,  1190 => 694,  1182 => 689,  1178 => 688,  1172 => 685,  1163 => 679,  1159 => 678,  1153 => 675,  1149 => 673,  1147 => 672,  1142 => 669,  1133 => 663,  1129 => 662,  1122 => 658,  1116 => 655,  1106 => 648,  1102 => 647,  1095 => 643,  1089 => 640,  1084 => 637,  1082 => 636,  1079 => 635,  1071 => 630,  1067 => 629,  1061 => 626,  1057 => 624,  1054 => 623,  1044 => 616,  1039 => 614,  1033 => 611,  1026 => 607,  1014 => 598,  1010 => 597,  1004 => 594,  1000 => 593,  991 => 587,  986 => 584,  977 => 578,  973 => 577,  967 => 574,  956 => 566,  952 => 565,  946 => 562,  940 => 558,  938 => 557,  903 => 524,  901 => 523,  887 => 510,  885 => 477,  866 => 474,  861 => 473,  844 => 472,  836 => 467,  821 => 455,  817 => 454,  811 => 451,  807 => 450,  799 => 445,  795 => 444,  789 => 441,  785 => 440,  775 => 433,  764 => 424,  757 => 394,  753 => 393,  750 => 392,  731 => 389,  726 => 388,  709 => 387,  701 => 382,  690 => 373,  683 => 340,  679 => 339,  676 => 338,  657 => 335,  652 => 334,  635 => 333,  627 => 328,  612 => 316,  608 => 315,  602 => 312,  598 => 311,  592 => 308,  588 => 307,  582 => 304,  578 => 303,  570 => 298,  566 => 297,  560 => 294,  556 => 293,  550 => 290,  546 => 289,  540 => 286,  536 => 285,  526 => 278,  512 => 267,  508 => 266,  502 => 263,  498 => 262,  490 => 257,  486 => 256,  480 => 253,  476 => 252,  466 => 245,  451 => 233,  447 => 232,  439 => 227,  435 => 226,  425 => 219,  413 => 210,  406 => 206,  368 => 170,  355 => 158,  349 => 154,  346 => 153,  326 => 149,  320 => 147,  303 => 146,  295 => 141,  285 => 133,  283 => 112,  277 => 108,  274 => 107,  260 => 106,  251 => 102,  243 => 101,  238 => 100,  230 => 95,  226 => 94,  220 => 92,  217 => 91,  200 => 90,  192 => 85,  187 => 82,  184 => 81,  181 => 80,  178 => 79,  175 => 78,  173 => 77,  136 => 43,  126 => 35,  117 => 32,  114 => 31,  109 => 30,  100 => 27,  97 => 26,  93 => 25,  90 => 24,  81 => 21,  78 => 20,  74 => 19,  68 => 16,  64 => 15,  56 => 9,  53 => 8,  48 => 5,  45 => 4,  37 => 3,  31 => 2,  11 => 1,);
    }
}

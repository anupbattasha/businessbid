<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">

    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 161
        $this->displayBlock('customcss', $context, $blocks);
        // line 164
        echo "
";
        // line 165
        $this->displayBlock('javascripts', $context, $blocks);
        // line 308
        echo "
";
        // line 309
        $this->displayBlock('customjs', $context, $blocks);
        // line 312
        echo "
";
        // line 313
        $this->displayBlock('jquery', $context, $blocks);
        // line 316
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 324
        echo "</head>
";
        // line 325
        ob_start();
        // line 326
        echo "<body>

";
        // line 328
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 329
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 330
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 331
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 332
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"\"><img src=\"";
        // line 337
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 343
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 344
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 346
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 347
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 350
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 351
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 353
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 354
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 355
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 360
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 368
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 370
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 373
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 375
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 380
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 385
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 386
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 388
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 391
(isset($context["uid"]) ? $context["uid"] : null))) {
            // line 392
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 393
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : null) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 396
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 403
        $this->displayBlock('maincontent', $context, $blocks);
        // line 634
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 637
        $this->displayBlock('footer', $context, $blocks);
        // line 750
        echo "    <!-- footer block ends -->
</div>";
        // line 753
        $this->displayBlock('footerScript', $context, $blocks);
        // line 757
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 764
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_af0f7093c8dde03b39309c57a5ec11b3a3666953651502ceeadcb37578b96769 = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_71bb9706ff976755369643d85e19e267f51f8ca477af3ce312513369363ffb0f = "http://www.businessbid.ae/contactus") && ('' === $__internal_71bb9706ff976755369643d85e19e267f51f8ca477af3ce312513369363ffb0f || 0 === strpos($__internal_af0f7093c8dde03b39309c57a5ec11b3a3666953651502ceeadcb37578b96769, $__internal_71bb9706ff976755369643d85e19e267f51f8ca477af3ce312513369363ffb0f)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_9b5acabc111fd2a7c958e93de5af48faab0c68e61177b72af3200cd8332bc1bc = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_bd982f9f710a25c921a9ff7e115cfdd8e77a758935d78f2230145fbdf3f9a7e1 = "http://www.businessbid.ae/node3") && ('' === $__internal_bd982f9f710a25c921a9ff7e115cfdd8e77a758935d78f2230145fbdf3f9a7e1 || 0 === strpos($__internal_9b5acabc111fd2a7c958e93de5af48faab0c68e61177b72af3200cd8332bc1bc, $__internal_bd982f9f710a25c921a9ff7e115cfdd8e77a758935d78f2230145fbdf3f9a7e1)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_f9bee1b420fb1993809eaa8c9f62a8965881cdfc8d802d0cd24ebb731710c91a = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_fed00fa0576db710202e8710dbdaddeda2d101bd026d9d884c85a3f226971af2 = "http://www.businessbid.ae/review/login") && ('' === $__internal_fed00fa0576db710202e8710dbdaddeda2d101bd026d9d884c85a3f226971af2 || 0 === strpos($__internal_f9bee1b420fb1993809eaa8c9f62a8965881cdfc8d802d0cd24ebb731710c91a, $__internal_fed00fa0576db710202e8710dbdaddeda2d101bd026d9d884c85a3f226971af2)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_08922deedf01b32ffad931f971c4adc01a4fb8da6def3bc1b1403a7aceaa4da0 = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3836ec83646270f58c968b4eb8481ca63e7b4e31c837e2e641069890ff89fad9 = "http://www.businessbid.ae/node1") && ('' === $__internal_3836ec83646270f58c968b4eb8481ca63e7b4e31c837e2e641069890ff89fad9 || 0 === strpos($__internal_08922deedf01b32ffad931f971c4adc01a4fb8da6def3bc1b1403a7aceaa4da0, $__internal_3836ec83646270f58c968b4eb8481ca63e7b4e31c837e2e641069890ff89fad9)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_ef9d715b2c8f071a63c3a29e98a2e6b73082cae11a635dbad012ec5bd5777c07 = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1a3f0aff2a690225cb4564333a790ed724d2fdb4d7a79089ccc318d0a3724b5e = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_1a3f0aff2a690225cb4564333a790ed724d2fdb4d7a79089ccc318d0a3724b5e || 0 === strpos($__internal_ef9d715b2c8f071a63c3a29e98a2e6b73082cae11a635dbad012ec5bd5777c07, $__internal_1a3f0aff2a690225cb4564333a790ed724d2fdb4d7a79089ccc318d0a3724b5e)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_f8b249f7e58926224b503bba8802fdd4f0e3ec40cb3a1a8053c660d3a07908e7 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_fb9d4b375fae284aa2136b738d4d7d436e24700e0ea3a9c07d7248e15f6da055 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_fb9d4b375fae284aa2136b738d4d7d436e24700e0ea3a9c07d7248e15f6da055 || 0 === strpos($__internal_f8b249f7e58926224b503bba8802fdd4f0e3ec40cb3a1a8053c660d3a07908e7, $__internal_fb9d4b375fae284aa2136b738d4d7d436e24700e0ea3a9c07d7248e15f6da055)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_38df67b7ccc91d6c851c6e1e28c8440f6337f82afcead21629d534a82ca272fe = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6fef33c02f7e41bba6f5638b0f702b1bbb9864db2b3fba2d5eb60da870693e13 = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_6fef33c02f7e41bba6f5638b0f702b1bbb9864db2b3fba2d5eb60da870693e13 || 0 === strpos($__internal_38df67b7ccc91d6c851c6e1e28c8440f6337f82afcead21629d534a82ca272fe, $__internal_6fef33c02f7e41bba6f5638b0f702b1bbb9864db2b3fba2d5eb60da870693e13)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_35b82bf0c4df5113f8b1f6a4a99f14500edaec5011bcd0d01b7eb45be7be9692 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d37ef75d92334f59eadf2bfdff3ab1c9368273346ed4ec60214bab304bb504cf = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_d37ef75d92334f59eadf2bfdff3ab1c9368273346ed4ec60214bab304bb504cf || 0 === strpos($__internal_35b82bf0c4df5113f8b1f6a4a99f14500edaec5011bcd0d01b7eb45be7be9692, $__internal_d37ef75d92334f59eadf2bfdff3ab1c9368273346ed4ec60214bab304bb504cf)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_45f84b67dac7a9f11373e87c97e906ff500b0a4e2bbcea5fba44f4d851c8b2f2 = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_db2e5676680ada2b6116203b31cfd6a1fa6a136d4ba3c750e79ccfb3ad1560da = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_db2e5676680ada2b6116203b31cfd6a1fa6a136d4ba3c750e79ccfb3ad1560da || 0 === strpos($__internal_45f84b67dac7a9f11373e87c97e906ff500b0a4e2bbcea5fba44f4d851c8b2f2, $__internal_db2e5676680ada2b6116203b31cfd6a1fa6a136d4ba3c750e79ccfb3ad1560da)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_3d4feb23d796068839c3baac8fd6472a48be5a4e28c851e4951789e887a1a40c = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_77f18215dae82619d66f359cc390e740a63138860a9f316b6a237cabaac15589 = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_77f18215dae82619d66f359cc390e740a63138860a9f316b6a237cabaac15589 || 0 === strpos($__internal_3d4feb23d796068839c3baac8fd6472a48be5a4e28c851e4951789e887a1a40c, $__internal_77f18215dae82619d66f359cc390e740a63138860a9f316b6a237cabaac15589)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_acf82cd58265cac98a2d4d52b80031566bf18c2c86ca3307aa17079093342893 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2750bd745c963459a28137e0e687627ec111e84454c2ea7dde056f61d1a45ca6 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_2750bd745c963459a28137e0e687627ec111e84454c2ea7dde056f61d1a45ca6 || 0 === strpos($__internal_acf82cd58265cac98a2d4d52b80031566bf18c2c86ca3307aa17079093342893, $__internal_2750bd745c963459a28137e0e687627ec111e84454c2ea7dde056f61d1a45ca6)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_367d7e154079aa40abb202263ae8b11c1a1af581634a48f5e78d722b7ff2d517 = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_953274bd0337904149758e1507a2c363d6d231b1dc123aa70c34a10119f51412 = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_953274bd0337904149758e1507a2c363d6d231b1dc123aa70c34a10119f51412 || 0 === strpos($__internal_367d7e154079aa40abb202263ae8b11c1a1af581634a48f5e78d722b7ff2d517, $__internal_953274bd0337904149758e1507a2c363d6d231b1dc123aa70c34a10119f51412)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_689f1d65d89dfa12f346277d8fd1340ba23e7a0030f31926f84c65e774fba749 = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_2888b1a9604166a7ce192641b25bf0b857d97b43cee1a7e25a689478573e1ff3 = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_2888b1a9604166a7ce192641b25bf0b857d97b43cee1a7e25a689478573e1ff3 || 0 === strpos($__internal_689f1d65d89dfa12f346277d8fd1340ba23e7a0030f31926f84c65e774fba749, $__internal_2888b1a9604166a7ce192641b25bf0b857d97b43cee1a7e25a689478573e1ff3)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_73a079b734d95ccbd4429cc9b2db4737f94d800cfdc84cb5bd423a79dec7767e = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_9c45a923ddb9a58a1936d51750f60c969a588826599cd3c44d02fa15b7229005 = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_9c45a923ddb9a58a1936d51750f60c969a588826599cd3c44d02fa15b7229005 || 0 === strpos($__internal_73a079b734d95ccbd4429cc9b2db4737f94d800cfdc84cb5bd423a79dec7767e, $__internal_9c45a923ddb9a58a1936d51750f60c969a588826599cd3c44d02fa15b7229005)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_21e1fdd815b44d34eb5d3406420219f7f260fd944efc53f8dd8cb7d215f30f64 = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_000b3326d07aeb64e23b5feb03e01b2acb1c3cfa02962add109f95f5682bb31b = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_000b3326d07aeb64e23b5feb03e01b2acb1c3cfa02962add109f95f5682bb31b || 0 === strpos($__internal_21e1fdd815b44d34eb5d3406420219f7f260fd944efc53f8dd8cb7d215f30f64, $__internal_000b3326d07aeb64e23b5feb03e01b2acb1c3cfa02962add109f95f5682bb31b)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_58cb4dd9064ddbcb3c45290908535294eafa49cd257de443cd6c39737399635b = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_16077f4d69f8e0ed1b75d4a19c7ee7997e6f072230db3d4aa01ddf039d201e55 = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_16077f4d69f8e0ed1b75d4a19c7ee7997e6f072230db3d4aa01ddf039d201e55 || 0 === strpos($__internal_58cb4dd9064ddbcb3c45290908535294eafa49cd257de443cd6c39737399635b, $__internal_16077f4d69f8e0ed1b75d4a19c7ee7997e6f072230db3d4aa01ddf039d201e55)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_bb34e880943aadfd839ab7cc2c094f2e1a8eb41630d922fe57dbcc819ed6153e = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_c1a43b84f521cc15d1605d395be6c25ea086cea9471a30c9a935597195272643 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_c1a43b84f521cc15d1605d395be6c25ea086cea9471a30c9a935597195272643 || 0 === strpos($__internal_bb34e880943aadfd839ab7cc2c094f2e1a8eb41630d922fe57dbcc819ed6153e, $__internal_c1a43b84f521cc15d1605d395be6c25ea086cea9471a30c9a935597195272643)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_65180d40e09bc648d62c4ad56ed3d9d4b4a46a09a16d3b7dc8368f94fa39e223 = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_14d6860d6e112fa903e0580b857b98a5407b6b0bbd3cfe01a4c5f57001b22631 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_14d6860d6e112fa903e0580b857b98a5407b6b0bbd3cfe01a4c5f57001b22631 || 0 === strpos($__internal_65180d40e09bc648d62c4ad56ed3d9d4b4a46a09a16d3b7dc8368f94fa39e223, $__internal_14d6860d6e112fa903e0580b857b98a5407b6b0bbd3cfe01a4c5f57001b22631)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_bc24199274746dd6eb4794c3013d4c910fea53d4310b7ba4fc6af9b27d01ce72 = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_5a81b04c3e5c6c878e3c43b2baf9392049bd44463c21c7dd379f1ca2f1995754 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_5a81b04c3e5c6c878e3c43b2baf9392049bd44463c21c7dd379f1ca2f1995754 || 0 === strpos($__internal_bc24199274746dd6eb4794c3013d4c910fea53d4310b7ba4fc6af9b27d01ce72, $__internal_5a81b04c3e5c6c878e3c43b2baf9392049bd44463c21c7dd379f1ca2f1995754)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_c74955ccb6c0711aa049c6175dedbcf2af71158f8b3b7188ae4a3bfe0c4b2417 = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_76891b465eda4ce7f57873f62bc64a1b0603e4b667c1d5188426cf0a4e5d2dde = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_76891b465eda4ce7f57873f62bc64a1b0603e4b667c1d5188426cf0a4e5d2dde || 0 === strpos($__internal_c74955ccb6c0711aa049c6175dedbcf2af71158f8b3b7188ae4a3bfe0c4b2417, $__internal_76891b465eda4ce7f57873f62bc64a1b0603e4b667c1d5188426cf0a4e5d2dde)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_92d803e8dc7b9fe9387c5809882d4d0dd2bfa4b8ed547ec66abb12af58be82ca = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_84954e3f0a3592f17ef7b81c01f371dabc901436fd27a273d3d8ecff3ea25c4b = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_84954e3f0a3592f17ef7b81c01f371dabc901436fd27a273d3d8ecff3ea25c4b || 0 === strpos($__internal_92d803e8dc7b9fe9387c5809882d4d0dd2bfa4b8ed547ec66abb12af58be82ca, $__internal_84954e3f0a3592f17ef7b81c01f371dabc901436fd27a273d3d8ecff3ea25c4b)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_177a0fb30f26960a4aae767b1220ee53b82935f28aaa44d228eb8e9164de936d = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_08af374cc32322e3d7cd59356c66c26954477af713402a3ac5b48586384aa01a = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_08af374cc32322e3d7cd59356c66c26954477af713402a3ac5b48586384aa01a || 0 === strpos($__internal_177a0fb30f26960a4aae767b1220ee53b82935f28aaa44d228eb8e9164de936d, $__internal_08af374cc32322e3d7cd59356c66c26954477af713402a3ac5b48586384aa01a)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_f8cd18b138cdcf6cdf1a52d1a64251872314b1b23ff5a51f40bb2022669ad5ce = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_06919c0e07a3557d89f5a81ad9a16499d25d34ef98d3a7f153a41aa9a62dd6ec = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_06919c0e07a3557d89f5a81ad9a16499d25d34ef98d3a7f153a41aa9a62dd6ec || 0 === strpos($__internal_f8cd18b138cdcf6cdf1a52d1a64251872314b1b23ff5a51f40bb2022669ad5ce, $__internal_06919c0e07a3557d89f5a81ad9a16499d25d34ef98d3a7f153a41aa9a62dd6ec)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_920bd46367cbaed95a4ea7c69e3eb5efe67244143f965e97934dfce7ed564332 = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_6c9a9afe016afc519974b302ba72008fab10cb5f9322c844cd4c3d7660f98726 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_6c9a9afe016afc519974b302ba72008fab10cb5f9322c844cd4c3d7660f98726 || 0 === strpos($__internal_920bd46367cbaed95a4ea7c69e3eb5efe67244143f965e97934dfce7ed564332, $__internal_6c9a9afe016afc519974b302ba72008fab10cb5f9322c844cd4c3d7660f98726)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_93f741b25148dce3d6f3f08f0c85957be49f3030afa4b7835fd202d6ed3f25ee = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_d494f481e3b6cb5be9293df9365189abb31cb9c575e0fee7ab401f74aa3fe7f4 = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_d494f481e3b6cb5be9293df9365189abb31cb9c575e0fee7ab401f74aa3fe7f4 || 0 === strpos($__internal_93f741b25148dce3d6f3f08f0c85957be49f3030afa4b7835fd202d6ed3f25ee, $__internal_d494f481e3b6cb5be9293df9365189abb31cb9c575e0fee7ab401f74aa3fe7f4)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_c5c4d7f5bf9bcf0abb0bb31533686bbb16e3d05233808b2d259fccea3e5ededb = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_64006df105548baacb28ae89d140999898bfa6d1e174927014844bbfa7e601a1 = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_64006df105548baacb28ae89d140999898bfa6d1e174927014844bbfa7e601a1 || 0 === strpos($__internal_c5c4d7f5bf9bcf0abb0bb31533686bbb16e3d05233808b2d259fccea3e5ededb, $__internal_64006df105548baacb28ae89d140999898bfa6d1e174927014844bbfa7e601a1)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_1512505a3d2dac34837ea5e0d3a22624833540c531677ab87ce63e4dd7650834 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_df0bbf45407d52ffbcf2653d35b0cb03b4b7a7d7a91df2aa6c60a571267e876b = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_df0bbf45407d52ffbcf2653d35b0cb03b4b7a7d7a91df2aa6c60a571267e876b || 0 === strpos($__internal_1512505a3d2dac34837ea5e0d3a22624833540c531677ab87ce63e4dd7650834, $__internal_df0bbf45407d52ffbcf2653d35b0cb03b4b7a7d7a91df2aa6c60a571267e876b)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_c8acc62eb03d3a3704c44f7eed84691a6bdb80c0602bfb50796db1d02be2600b = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_55f9feee146b0bb67e1ec11f6eca0d9d4f91316047a1f54ce61e1bc0ed6de265 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_55f9feee146b0bb67e1ec11f6eca0d9d4f91316047a1f54ce61e1bc0ed6de265 || 0 === strpos($__internal_c8acc62eb03d3a3704c44f7eed84691a6bdb80c0602bfb50796db1d02be2600b, $__internal_55f9feee146b0bb67e1ec11f6eca0d9d4f91316047a1f54ce61e1bc0ed6de265)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : null) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : null) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : null) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : null) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : null) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_5328dea2bc292fc8d3b4f2eea5793a1bfaf882aea10a5b2ea563de38442ebc16 = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f72cad44f2aeeba9ccc3040b7cc09e8359ada8dadc3f06edfa2b52006a4b9905 = "http://www.businessbid.ae/node2") && ('' === $__internal_f72cad44f2aeeba9ccc3040b7cc09e8359ada8dadc3f06edfa2b52006a4b9905 || 0 === strpos($__internal_5328dea2bc292fc8d3b4f2eea5793a1bfaf882aea10a5b2ea563de38442ebc16, $__internal_f72cad44f2aeeba9ccc3040b7cc09e8359ada8dadc3f06edfa2b52006a4b9905)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_66d386c3ae3bac4f1f0e07b0222e321e4bf49aa8eec2211fbf2afcc904d83baa = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_21091c9ecf8c91abb06445247c6270ae000dcb6437a80115079fe9bad332ba46 = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_21091c9ecf8c91abb06445247c6270ae000dcb6437a80115079fe9bad332ba46 || 0 === strpos($__internal_66d386c3ae3bac4f1f0e07b0222e321e4bf49aa8eec2211fbf2afcc904d83baa, $__internal_21091c9ecf8c91abb06445247c6270ae000dcb6437a80115079fe9bad332ba46)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_d4c4faa98820da5755226936fd76ccd8b845b5a2d144bbf1a0dbbc49a674a55d = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_8aff4f738c83641b30c119456562e78e4bc65704b7ed876978e549d52e2df994 = "http://www.businessbid.ae/faq") && ('' === $__internal_8aff4f738c83641b30c119456562e78e4bc65704b7ed876978e549d52e2df994 || 0 === strpos($__internal_d4c4faa98820da5755226936fd76ccd8b845b5a2d144bbf1a0dbbc49a674a55d, $__internal_8aff4f738c83641b30c119456562e78e4bc65704b7ed876978e549d52e2df994)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_2e52eba50c13865d2d5e27768deb361b25fd3bad544423926b0a7d89ae5277b3 = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_1bd2e38218d124507b2777212fe4ccb956413d4a34d307cc1f221ab0a5efc517 = "http://www.businessbid.ae/node4") && ('' === $__internal_1bd2e38218d124507b2777212fe4ccb956413d4a34d307cc1f221ab0a5efc517 || 0 === strpos($__internal_2e52eba50c13865d2d5e27768deb361b25fd3bad544423926b0a7d89ae5277b3, $__internal_1bd2e38218d124507b2777212fe4ccb956413d4a34d307cc1f221ab0a5efc517)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_2e186ac9a3ce2d769ae28a2153d6cd53089ceba9b2715abeefa2b701dc261eb3 = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_9273d6eb956692e03bba126ef28ca66ec7766cd744c6fe57e72e3d2abbf2e5a4 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_9273d6eb956692e03bba126ef28ca66ec7766cd744c6fe57e72e3d2abbf2e5a4 || 0 === strpos($__internal_2e186ac9a3ce2d769ae28a2153d6cd53089ceba9b2715abeefa2b701dc261eb3, $__internal_9273d6eb956692e03bba126ef28ca66ec7766cd744c6fe57e72e3d2abbf2e5a4)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_7105cbd05f5b0ca08e6f5ab34b65c98b91b904108780bcbc34afe81617a963d1 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_b589943dc42908208122dc9cabb4f74a94a51a7989c8a691145f793426d0f893 = "http://www.businessbid.ae/node6") && ('' === $__internal_b589943dc42908208122dc9cabb4f74a94a51a7989c8a691145f793426d0f893 || 0 === strpos($__internal_7105cbd05f5b0ca08e6f5ab34b65c98b91b904108780bcbc34afe81617a963d1, $__internal_b589943dc42908208122dc9cabb4f74a94a51a7989c8a691145f793426d0f893)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_8ec45e11805394cec9bddd9974dd90d324da635e1b528b09d3730ccc3257c431 = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_edac7872d1bc1a673ebf50390e813e16991642c6c8fb74e27b0dd049e0a180bf = "http://www.businessbid.ae/feedback") && ('' === $__internal_edac7872d1bc1a673ebf50390e813e16991642c6c8fb74e27b0dd049e0a180bf || 0 === strpos($__internal_8ec45e11805394cec9bddd9974dd90d324da635e1b528b09d3730ccc3257c431, $__internal_edac7872d1bc1a673ebf50390e813e16991642c6c8fb74e27b0dd049e0a180bf)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_55a9ff0008645e8fb6a63c0565451d88f58d6d8e5b313f3a2d704bf0efbc3e77 = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_f3d8b55229dc0a745247e5270e829afbad9fa7f3be9c3419e4aa201fdb86284f = "http://www.businessbid.ae/sitemap") && ('' === $__internal_f3d8b55229dc0a745247e5270e829afbad9fa7f3be9c3419e4aa201fdb86284f || 0 === strpos($__internal_55a9ff0008645e8fb6a63c0565451d88f58d6d8e5b313f3a2d704bf0efbc3e77, $__internal_f3d8b55229dc0a745247e5270e829afbad9fa7f3be9c3419e4aa201fdb86284f)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_2e3c70545779493f343af9d76284abfb5d1047013d9d7c3e057ccccc01bc4867 = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_af2e640e4a97fc7bf3b8e0321477fbb2ee52a6a274635bbdb59afe5b24013556 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_af2e640e4a97fc7bf3b8e0321477fbb2ee52a6a274635bbdb59afe5b24013556 || 0 === strpos($__internal_2e3c70545779493f343af9d76284abfb5d1047013d9d7c3e057ccccc01bc4867, $__internal_af2e640e4a97fc7bf3b8e0321477fbb2ee52a6a274635bbdb59afe5b24013556)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_1536c0bf0bee9f629a72eaf08844b271232962da50b44fa548ef172d5b5a4bd7 = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_86c11896a2b895395421d92694a86a6a58d71fb489b39f216bf63ae24e8cec56 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_86c11896a2b895395421d92694a86a6a58d71fb489b39f216bf63ae24e8cec56 || 0 === strpos($__internal_1536c0bf0bee9f629a72eaf08844b271232962da50b44fa548ef172d5b5a4bd7, $__internal_86c11896a2b895395421d92694a86a6a58d71fb489b39f216bf63ae24e8cec56)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_ce1d8b06bb96f40eca4b10acc70672573c1cc690ab75df087c6aed05299fba48 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_e5ecf4d6ce555c27cdeb09093dc582d37e7ed617bf5698b4d0e0db158893911a = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_e5ecf4d6ce555c27cdeb09093dc582d37e7ed617bf5698b4d0e0db158893911a || 0 === strpos($__internal_ce1d8b06bb96f40eca4b10acc70672573c1cc690ab75df087c6aed05299fba48, $__internal_e5ecf4d6ce555c27cdeb09093dc582d37e7ed617bf5698b4d0e0db158893911a)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_a629af9d2d0ddae727e3fc1a39531dd2ae9ffde7f31dd7b2e3a9d217455b1ada = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : null), "request", array()), "uri", array())) && is_string($__internal_3b6cc66160caef25c88a3c9987bacc4dec7b2f1394212ee81ff4324a18c21724 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_3b6cc66160caef25c88a3c9987bacc4dec7b2f1394212ee81ff4324a18c21724 || 0 === strpos($__internal_a629af9d2d0ddae727e3fc1a39531dd2ae9ffde7f31dd7b2e3a9d217455b1ada, $__internal_3b6cc66160caef25c88a3c9987bacc4dec7b2f1394212ee81ff4324a18c21724)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 161
    public function block_customcss($context, array $blocks = array())
    {
        // line 162
        echo "
";
    }

    // line 165
    public function block_javascripts($context, array $blocks = array())
    {
        // line 166
        echo "<script>
\$(document).ready(function(){
    ";
        // line 168
        if (array_key_exists("keyword", $context)) {
            // line 169
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 173
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 309
    public function block_customjs($context, array $blocks = array())
    {
        // line 310
        echo "
";
    }

    // line 313
    public function block_jquery($context, array $blocks = array())
    {
        // line 314
        echo "
";
    }

    // line 316
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 317
        echo "
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-59054368-1', 'auto');ga('send', 'pageview');
</script>

";
    }

    // line 403
    public function block_maincontent($context, array $blocks = array())
    {
        // line 404
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 411
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 440
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 447
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 455
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 471
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 479
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 491
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 531
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 561
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 562
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 564
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 566
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 572
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 573
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 575
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 577
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 582
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 583
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 585
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 587
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 594
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 595
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 597
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 599
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 605
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 606
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 608
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 610
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 616
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : null))) {
            // line 617
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 619
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 621
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 491
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 492
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 494
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 501
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 502
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 503
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 504
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 505
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 506
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 507
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 509
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 510
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : null), " ");
                // line 511
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : null), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 513
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 515
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 520
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 526
        echo "    </div>
            </div>


    ";
    }

    // line 520
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 521
        echo "    <div class=\"request\">
    <h2>";
        // line 522
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : null), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 637
    public function block_footer($context, array $blocks = array())
    {
        // line 638
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 661
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 668
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 680
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 686
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 688
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 689
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 699
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 704
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 741
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 743
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 753
    public function block_footerScript($context, array $blocks = array())
    {
        // line 754
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 755
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1410 => 755,  1405 => 754,  1402 => 753,  1391 => 743,  1386 => 741,  1346 => 704,  1338 => 699,  1334 => 698,  1330 => 697,  1319 => 689,  1316 => 688,  1312 => 686,  1303 => 680,  1299 => 679,  1295 => 678,  1291 => 677,  1287 => 676,  1276 => 668,  1272 => 667,  1263 => 661,  1259 => 660,  1255 => 659,  1251 => 658,  1247 => 657,  1226 => 638,  1223 => 637,  1215 => 522,  1212 => 521,  1209 => 520,  1201 => 526,  1199 => 520,  1192 => 515,  1185 => 513,  1169 => 511,  1166 => 510,  1163 => 509,  1149 => 507,  1146 => 506,  1143 => 505,  1141 => 504,  1137 => 503,  1134 => 502,  1130 => 501,  1120 => 494,  1116 => 492,  1113 => 491,  1097 => 621,  1091 => 619,  1085 => 617,  1083 => 616,  1075 => 610,  1069 => 608,  1063 => 606,  1061 => 605,  1053 => 599,  1047 => 597,  1041 => 595,  1039 => 594,  1030 => 587,  1024 => 585,  1018 => 583,  1016 => 582,  1009 => 577,  1003 => 575,  997 => 573,  995 => 572,  987 => 566,  981 => 564,  975 => 562,  973 => 561,  941 => 531,  939 => 491,  924 => 479,  913 => 471,  902 => 463,  891 => 455,  880 => 447,  870 => 440,  852 => 425,  842 => 418,  832 => 411,  823 => 404,  820 => 403,  810 => 317,  807 => 316,  802 => 314,  799 => 313,  794 => 310,  791 => 309,  772 => 293,  650 => 173,  644 => 169,  642 => 168,  638 => 166,  635 => 165,  630 => 162,  627 => 161,  623 => 127,  620 => 126,  614 => 109,  610 => 107,  608 => 106,  605 => 105,  603 => 104,  600 => 103,  598 => 102,  595 => 101,  593 => 100,  590 => 99,  588 => 98,  585 => 97,  583 => 96,  580 => 95,  578 => 94,  575 => 93,  573 => 92,  570 => 91,  568 => 90,  565 => 89,  563 => 88,  560 => 87,  558 => 86,  555 => 85,  553 => 84,  550 => 83,  548 => 82,  545 => 81,  543 => 80,  540 => 79,  538 => 78,  535 => 77,  533 => 76,  530 => 75,  528 => 74,  525 => 73,  523 => 72,  520 => 71,  518 => 70,  515 => 69,  513 => 68,  510 => 67,  508 => 66,  505 => 65,  503 => 64,  500 => 63,  498 => 62,  495 => 61,  493 => 60,  490 => 59,  488 => 58,  485 => 57,  483 => 56,  480 => 55,  478 => 54,  475 => 53,  473 => 52,  470 => 51,  468 => 50,  465 => 49,  463 => 48,  460 => 47,  458 => 46,  455 => 45,  453 => 44,  450 => 43,  448 => 42,  445 => 41,  443 => 40,  440 => 39,  438 => 38,  435 => 37,  433 => 36,  430 => 35,  428 => 34,  425 => 33,  423 => 32,  420 => 31,  418 => 30,  415 => 29,  413 => 28,  410 => 27,  408 => 26,  405 => 25,  403 => 24,  400 => 23,  398 => 22,  395 => 21,  393 => 20,  390 => 19,  388 => 18,  385 => 17,  383 => 16,  380 => 15,  378 => 14,  375 => 13,  373 => 12,  370 => 11,  365 => 764,  356 => 757,  354 => 753,  351 => 750,  349 => 637,  344 => 634,  342 => 403,  333 => 396,  319 => 393,  316 => 392,  314 => 391,  308 => 388,  304 => 386,  302 => 385,  294 => 380,  286 => 375,  281 => 373,  275 => 370,  270 => 368,  266 => 367,  257 => 360,  241 => 355,  237 => 354,  233 => 353,  229 => 351,  227 => 350,  221 => 347,  215 => 346,  211 => 344,  209 => 343,  200 => 337,  193 => 332,  191 => 331,  189 => 330,  187 => 329,  185 => 328,  181 => 326,  179 => 325,  176 => 324,  174 => 316,  172 => 313,  169 => 312,  167 => 309,  164 => 308,  162 => 165,  159 => 164,  157 => 161,  152 => 159,  148 => 158,  139 => 152,  134 => 150,  130 => 149,  125 => 147,  121 => 146,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

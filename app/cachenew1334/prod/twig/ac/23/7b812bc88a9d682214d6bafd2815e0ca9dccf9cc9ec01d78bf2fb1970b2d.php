<?php

/* BBidsBBidsHomeBundle:Admin:order_reports.html.twig */
class __TwigTemplate_ac237b812bc88a9d682214d6bafd2815e0ca9dccf9cc9ec01d78bf2fb1970b2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:order_reports.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).ready(function() {

    \$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
    \$.datepicker.regional[\"\"].changeMonth = true;
    \$.datepicker.regional[\"\"].changeYear = true;
    \$.datepicker.setDefaults(\$.datepicker.regional['']);
    \$('#example').dataTable({
        \"aaSorting\": [[ 0, \"desc\" ]],
        \"aaSortingFixed\": [[0,'desc']],
        \"sPaginationType\": \"full_numbers\",
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
        \"sAjaxSource\": \"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/orders_reports.php"), "html", null, true);
        echo "\",
        \"fnServerParams\": function(aoData) {
            if (aoData[30].value != '') {
                if (aoData[30].value     == \"Bronze\") {  aoData[30].value = 25; }
                else if (aoData[30].value == \"Silver\") {  aoData[30].value = 50; }
                else if (aoData[30].value == \"Gold\") { aoData[30].value = 100; }
                else { aoData[30].value = 250; }
            }
        }
    }).columnFilter({
        aoColumns: [{
            type: \"date-range\",
            sSelector: \"#dateFilter\",
            sRangeFormat: \"<label>Date</label> {from} {to}\"
        }
        , {
            sSelector: \"#orderNoFilter\"
        }, {
            sSelector: \"#vendorFilter\"
        }, {
            sSelector: \"#businessFilter\"
        }, {
            sSelector: \"#categoryFilter\"
        }, {
            type: 'select',
            sSelector: '#leadFilter',
            values : [\"Bronze\",\"Silver\",\"Gold\",\"Platinum\"]
        }, {
            sSelector: \"#paymentFilter\"
        }, {
            type: 'number-range',
            sSelector: '#amountFilter',
            sRangeFormat: \"<label>Amount</label> {from} {to}\"
        }]
    });
});
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Vendor Orders</h1></div>

<div>
\t";
        // line 82
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 83
            echo "
\t<div class=\"alert alert-danger\">";
            // line 84
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "
\t";
        // line 88
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 89
            echo "
\t<div class=\"alert alert-success\">";
            // line 90
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "
\t";
        // line 94
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 95
            echo "
\t<div class=\"alert alert-success\">";
            // line 96
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "    <div>";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "</div>
    <div class=\"admin_filter report-filter\">
        <div class=\"side-clear\" id=\"dateFilter\"></div>
        <div class=\"clear-right\" id=\"orderNoFilter\"></div>
        <div class=\"col-md-2 clear-right\" id=\"vendorFilter\"></div>
        <div class=\"col-md-2 clear-right\" id=\"businessFilter\"></div>
        <div class=\"col-md-3 clear-right\" id=\"categoryFilter\"></div>
        <div class=\"col-md-2 side-clear  med-space\" id=\"leadFilter\"></div>
        <div class=\"col-md-2 clear-right  med-space\" id=\"paymentFilter\"></div>
        <div class=\"col-md-3 top-space  med-space\" id=\"amountFilter\"></div>
        <div class=\"col-md-5 clear-right action-links space\">
            <span class=\"export-all\"><a href=\"";
        // line 110
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_exportall_orders");
        echo "\" class=\"form_submit\">Export All</a></span>
            <span class=\"export-selected\"><a href=\"#\" class=\"form_submit\" onclick=\"getSearchInputs();\">Export Filtered Data</a></span>
            <span class=\"reset form_submit\" onclick=\"fnResetFilters();\">Reset</a></span>
        </div>
    </div>
    <div>
        <div class=\"latest-orders\">
           <table id=\"example\"> <thead> <tr> <th>Date Recieved</th> <th>Order Number</th> <th>Vendor Name</th> <th>Business Name</th> <th>Category</th> <th>Lead Package</th> <th>Payment Type</th> <th>Amount</th> </tr></thead> <tbody> <tr> <td colspan=\"8\" class=\"dataTables_empty\">Loading data from server... Please wait.</td></tr></tbody> <tfoot><tr> <th>Date Recieved</th> <th>Order Number</th> <th>Vendor Name</th> <th>Business Name</th> <th>Category</th> <th>Lead Package</th> <th>Payment Type</th> <th>Amount</th> </tr></tfoot> </table>
        </div>
    </div>
</div>
<script type=\"text/javascript\">
    function getSearchInputs () {
        var oTable = \$('#example').dataTable();
        var oSettings = oTable.fnSettings();

        var \$form = document.forms[\"form\"];
        \$form.reset();
        var oForm = document.forms[\"form\"].getElementsByTagName(\"input\");
        var dateRange = '',amountRange = '';
        if((\$('#example_range_from_0').val() != 'undefined') && (\$('#example_range_from_0').val() != '')) {
            dateRange = \$('#example_range_from_0').val() +'~';
            if(\$('#example_range_to_0').val() != 'undefined')
                dateRange += \$('#example_range_to_0').val();
        }
        oForm[0].value = dateRange;
        oForm[1].value = oSettings.aoPreSearchCols[1].sSearch;
        oForm[2].value = oSettings.aoPreSearchCols[2].sSearch;
        oForm[3].value = oSettings.aoPreSearchCols[3].sSearch;
        oForm[4].value = oSettings.aoPreSearchCols[4].sSearch;
        if(oSettings.aoPreSearchCols[5].sSearch != '') {
            if (oSettings.aoPreSearchCols[5].sSearch     == \"Bronze\") {  oForm[5].value  = 25; }
            else if (oSettings.aoPreSearchCols[5].sSearch == \"Silver\") {  oForm[5].value  = 50; }
            else if (oSettings.aoPreSearchCols[5].sSearch == \"Gold\") { oForm[5].value = 100; }
            else { oForm[5].value = 250; }
        }
        oForm[6].value = oSettings.aoPreSearchCols[6].sSearch;

        if((\$('#example_range_from_7').val() != 'undefined') && (\$('#example_range_from_7').val() != '')) {
            amountRange = \$('#example_range_from_7').val() +'~';
            if(\$('#example_range_to_7').val() != 'undefined')
                amountRange += \$('#example_range_to_7').val();
        }
        oForm[7].value = amountRange;

        if((oForm[0].value =='') && (oForm[1].value =='') && (oForm[2].value =='') && (oForm[3].value =='') && (oForm[4].value =='') && (oForm[5].value =='') && (oForm[6].value =='') && (oForm[7].value ==''))
            alert('No filtered results');
        else
           \$form.submit();
    }

    function fnResetFilters() {
        \$(\"input\").val(\"\");
        \$('div#leadFilter').find('select option:eq(0)').prop('selected', true);
        var table = \$('#example').dataTable();
        table.fnFilterClear();
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:order_reports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 110,  178 => 99,  169 => 96,  166 => 95,  162 => 94,  159 => 93,  150 => 90,  147 => 89,  143 => 88,  140 => 87,  131 => 84,  128 => 83,  124 => 82,  79 => 40,  44 => 8,  40 => 7,  36 => 6,  31 => 5,  28 => 3,  11 => 1,);
    }
}

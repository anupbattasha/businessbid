<?php

/* BBidsBBidsHomeBundle:Admin:vendor_reports.html.twig */
class __TwigTemplate_7592a7120e307eba9c296636fa5735a03da476f56f13226e5a61b69b16359d8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:vendor_reports.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 5
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/fnFilterClear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery-ui.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/datatable/jquery.dataTables.columnFilterNew.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script type=\"text/javascript\" charset=\"utf-8\">
\$(document).ready(function() {

    \$.datepicker.regional[\"\"].dateFormat = 'yy-mm-dd';
    \$.datepicker.regional[\"\"].changeMonth = true;
    \$.datepicker.regional[\"\"].changeYear = true;
    \$.datepicker.setDefaults(\$.datepicker.regional['']);
    \$('#example').dataTable({
        \"aaSorting\" : [[0, 'desc']],
        \"sPaginationType\": \"full_numbers\",
        \"aLengthMenu\": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, \"All\"]
        ],
        \"bProcessing\": true,
        \"bServerSide\": true,
        \"oLanguage\": {
            \"sEmptyTable\": '',
            \"sInfoEmpty\": '',
            \"sProcessing\": 'Processing please wait',
            \"sInfoFiltered\": '',
            \"oPaginate\": {
                \"sFirst\": 'First',
                \"sPrevious\": '<<',
                \"sNext\": '>>',
                \"sLast\": 'Last'
            },
            \"sZeroRecords\": 'ZeroRecords',
            \"sSearch\": 'Search',
            \"sLoadingRecords\": 'LoadingRecords',
        },
        \"sEmptyTable\": \"There are no records\",
        \"sAjaxSource\": \"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("reports/vendor_reports.php"), "html", null, true);
        echo "\",
        \"fnServerParams\": function(aoData) {
            if (aoData[30].value != '') {
                aoData[30].value = (aoData[30].value == 'Active') ? 1 : 0;
            }
            if (aoData[18].value != '') {
                aoData[18].value = (aoData[18].value == 'Customer') ? 2 : 3;
            }
        }
    }).columnFilter({
        aoColumns: [{
            type: \"date-range\",
            sSelector: \"#dateFilter\",
            sRangeFormat: \"<label>Date</label> {from} {to}\"
        }, null, {
            sSelector: \"#nameFilter\"
        }, {
            sSelector: \"#emailFilter\"
        }, {
            sSelector: \"#contactFilter\"
        }, {
            type: 'select',
            values: [\"Active\", \"Inactive\"],
            sSelector: '#statusFilter'
        }, null, null]
    });


    \$(\"#btnExport\").click(function() {
        \$(\"#example\").btechco_excelexport({
            containerid: \"example\",
            datatype: \$datatype.Table
        });
    });
});
</script>
<div class=\"page-bg\">
<div class=\"container inner_container admin-dashboard\">
<div class=\"page-title\"><h1>Vendor Reports</h1></div>

<div>
\t";
        // line 83
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 84
            echo "
\t<div class=\"alert alert-danger\">";
            // line 85
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "
\t";
        // line 89
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 90
            echo "
\t<div class=\"alert alert-success\">";
            // line 91
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "
\t";
        // line 95
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 96
            echo "
\t<div class=\"alert alert-success\">";
            // line 97
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "<div>";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "</div>
<div class=\"admin_filter report-filter\">
\t<div class=\"col-md-3 side-clear\" id=\"dateFilter\"></div>
    <div class=\"col-md-2\" id=\"nameFilter\"></div>
    <div class=\"col-md-2\" id=\"emailFilter\"></div>
    <div class=\"col-md-2 clear-right\" id=\"contactFilter\"></div>
\t<div class=\"col-md-2 clear-right\" id=\"statusFilter\"></div>
\t<div class=\"col-md-5 clear-right action-links\">
\t<span class=\"export-all\"><a href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_exportall_vendors");
        echo "\" target=\"_blank\" class=\"form_submit\">Export All</a></span>
\t<span class=\"export-selected\"><a href=\"#\" class=\"form_submit\" onclick=\"getSearchInputs();\">Export Filtered Data</a></span>
\t<span class=\"reset\"><a href=\"#\" class=\"form_submit\" onclick=\"fnResetFilters();\">Reset</a></span>
\t</div>
</div>

<div class=\"latest-orders\">
\t\t<table id=\"example\"><thead><tr><th>Date Created</th><th>Profile</th><th>Name</th><th>Email</th><th>Contact Number</th><th>Status</th><th>Updated Date</th><th>Last Access Date</th></tr></thead><tbody><tr><td colspan=\"8\" class=\"dataTables_empty\">Loading data from server... Please wait</td></tr></tbody><tfoot><tr><th>Date Created</th><th>Profile</th><th>Name</th><th>Email</th><th>Contact Number</th><th>Status</th><th>Updated Date</th><th>Last Access Date</th></tr></tfoot></table>
</div>
</div>

</div>
<script type=\"text/javascript\">
    function getSearchInputs () {
        var oTable = \$('#example').dataTable();
        var oSettings = oTable.fnSettings();
        var \$form = document.forms[\"form\"];
        \$form.reset();
        var oForm = document.forms[\"form\"].getElementsByTagName(\"input\");
        var dateRange = '';
        if(\$('#example_range_from_0').val() != 'undefined') {
            dateRange = \$('#example_range_from_0').val() +'~';
            if(\$('#example_range_to_0').val() != 'undefined')
                dateRange += \$('#example_range_to_0').val();
        }
        oForm[0].value = dateRange;
        oForm[2].value = oSettings.aoPreSearchCols[2].sSearch;
        oForm[3].value = oSettings.aoPreSearchCols[3].sSearch;
        oForm[4].value = oSettings.aoPreSearchCols[4].sSearch;
        if(oSettings.aoPreSearchCols[5].sSearch != '')
            oForm[5].value = (oSettings.aoPreSearchCols[5].sSearch == 'Active') ? 1 : 0;

        if((oForm[0].value =='~') && (oForm[2].value =='') && (oForm[3].value =='') && (oForm[4].value =='') && (oForm[5].value ==''))
            alert('No filtered results');
        else
           \$form.submit();
    }

    function fnResetFilters() {
        var table = \$('#example').dataTable();
        table.fnFilterClear();
        \$('input').val('');
        \$('div#statusFilter').find(\"select option:eq(0)\").prop(\"selected\",true);
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:vendor_reports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 108,  179 => 100,  170 => 97,  167 => 96,  163 => 95,  160 => 94,  151 => 91,  148 => 90,  144 => 89,  141 => 88,  132 => 85,  129 => 84,  125 => 83,  81 => 42,  44 => 8,  40 => 7,  36 => 6,  31 => 5,  28 => 3,  11 => 1,);
    }
}

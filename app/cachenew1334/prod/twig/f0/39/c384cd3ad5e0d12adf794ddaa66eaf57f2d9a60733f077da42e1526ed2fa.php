<?php

/* BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig */
class __TwigTemplate_f039c384cd3ad5e0d12adf794ddaa66eaf57f2d9a60733f077da42e1526ed2fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "
";
    }

    // line 7
    public function block_maincontent($context, array $blocks = array())
    {
        // line 8
        echo "<div id=\"aboutus\" class=\"container about-us\">\t
<div class=\"page-title1\">About BusinessBid</div>
\t\t<div class=\"content-block\">
\t\t<h2>It's all about connecting Customers and Services</h2>
\t\t<p>The concept of BusinessBid was introduced into the UAE after detailed and extensive research which highlighted the need of a comprehensive, efficient and a single source of connecting users to vendors within the UAE market. Simply put we are trying to provide a solution.</p>
\t\t</div>
\t\t<div class=\"content-block\">
\t\t<h2>For Customers</h2>
\t\t<p>We understand how important it is to engage a reliable service provider who provides great service at a competitive price and instead of calling around for quotes and waiting for vendors to get back to you; we do the hard work for you.</p>
\t\t<ul>
\t\t\t<li>Receive competitive quotes from your local professionals</li>
\t\t\t<li>Find qualified vendors and service providers in your area</li>
\t\t\t<li>Save time and money</li>
\t\t</ul>
\t\t</div>
\t\t<div class=\"content-block\">
\t\t<h2>For Businesses</h2>
\t\t<p>We also understand that sustaining a business can be challenging and growing a customer base can be even tougher. Therefore in addition of being customer centric we care about the businesses of our service providers.</p>
\t\t<p>We notify you when a customer requests a service you can provide and ensure you are receiving quality leads, even while you're on-the-go.</p>
\t\t<ul>
\t\t\t<li>Promote your business online and get access to a large audience</li>
\t\t\t<li>Attract new customers who are seeking to engage service professionals</li>
\t\t\t<li>Get access to more jobs</li>
\t\t</ul>
\t\t</div>
</div>

<div id=\"meetteam\" class=\"meet-team\">
\t<div class=\"container\">\t
\t\t<div class=\"page-title1\">Meet the team</div>
\t\t<div class=\"col-sm-4\">
\t\t\t<div class=\"content-block\">
\t\t\t<p>The team at BusinessBid are seasoned professiotnals who have over 50 years of valuable experience in within the service industry in different global market places under different demanding and evolving market trends.</p>
\t\t</div>
\t\t</div>\t\t
\t\t<div class=\"col-sm-4\">
\t\t\t<div class=\"content-block\">
\t\t\t<p>Our philosophy is to keep things simple. Simplicity is what everyone understands and that's how we operate.</p>
\t\t</div>
\t\t</div>\t\t
\t\t<div class=\"col-sm-4\">
\t\t\t<div class=\"content-block\">
\t\t\t<p>Our team consists of professionals who are dedicated to customer service, understand and value the business of our customers and vendors and are dedicated to providing our stakeholders with an amazing experience.</p>
\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"container commit-block\">
\t\t<h2>Our commitment is to ensure value for time and money and our focus is to keep improving on an already high benchmark of client satisfaction. <br>Our team consists of dedicated</h2>
\t</div>\t
</div>
<div class=\"meet-team-bottom\">
\t<div class=\"container\">
\t\t<div class=\"content-block\">
\t\t\t<ul>
\t\t\t\t<li>Account Managers who recognize the need for understanding the value offerings of our vendors and fully assist them in how to best position their service offerings.</li>
\t\t\t\t<li>Customer Service Associates who understand customer service and are client-centric.  No matter how big or small their request is, it becomes a personal responsibility to resolve them.</li>
\t\t\t\t<li>Technology Managers and Technical Teams who are constantly reviewing and improving our technology platforms to help create a better experience for our users to ensure the perfect experience.</li>
\t\t\t\t<li>Marketing Managers who are studying the emerging trends of technology, social media engagement to understand the needs of customers and vendors and how best to provide value to them</li>
\t\t\t\t<li>Operations Managers and Analysts who ensure operational efficiency by working on better processes and working models to ensure the delivery of our services are immaculate.</li>
\t\t\t\t<li>Office and Support Staff who are the unsung heroes who help in optimizing day-to-day activities and transactions so to support our other critical teams.</li>\t\t\t
\t\t\t</ul>
\t\t</div>
\t\t<p>Our teams are supported and managed by our leadership team who have created an amazing work and people culture to enable the best performance. <br>Our  focus on governance and efficient  frameworks and investments into technology help us improve service delivery and serve our clients better.</p>
\t</div>\t
</div>
<div id=\"career\" class=\"career-opnt\">
\t<div class=\"container\">
\t\t<div class=\"page-title1\">Career Opportunities</div>
\t\t<h6>We are always on the look-out for committed and talented and people to join our buzzing team. The people who succeed with us are those who are passionate about their areas, have new ideas and the drive to make things happen!</h6>
\t\t
\t</div>\t
\t<div class=\"container\">
\t\t<div class=\"col-sm-12 career-offer\">
\t\t<h3>What do we offer as an organisation?</h3>
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<span class=\"col-sm-1 count-num\">1</span>
\t\t\t\t\t<span class=\"col-sm-11\">
\t\t\t\t\t\t<h5>We provide a solution</h5>
\t\t\t\t\t\t<p>We strive our best to create a solution to an existing problem - create simplicity</p>
\t\t\t\t\t</span>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<span class=\"col-sm-1 count-num\">4</span>
\t\t\t\t\t<span class=\"col-sm-11\">
\t\t\t\t\t\t<h5>Environment & Culture</h5>
\t\t\t\t\t\t<p>We understand creating an excellent atmosphere is essential for work performance</p>
\t\t\t\t\t</span>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<span class=\"col-sm-1 count-num\">2</span>
\t\t\t\t\t<span class=\"col-sm-11\">
\t\t\t\t\t\t<h5>Growth & Development</h5>
\t\t\t\t\t\t<p>We value our people and if you grow- we grow</p>
\t\t\t\t\t</span>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<span class=\"col-sm-1 count-num\">5</span>
\t\t\t\t\t<span class=\"col-sm-11\">
\t\t\t\t\t\t<h5>Work Hard & Play Hard</h5>
\t\t\t\t\t\t<p>We know it's not all about work and there is a balancing act to accommodate fun and work.</p>
\t\t\t\t\t</span>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<span class=\"col-sm-1 count-num\">3</span>
\t\t\t\t\t<span class=\"col-sm-11\">
\t\t\t\t\t\t<h5>Reward & Recognition</h5>
\t\t\t\t\t\t<p>We know motivation is critical to aim higher</p>
\t\t\t\t\t</span>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<span class=\"col-sm-1 count-num\">6</span>
\t\t\t\t\t<span class=\"col-sm-11\">
\t\t\t\t\t\t<h5>Creativity & Ideas</h5>
\t\t\t\t\t\t<p>We listen. So if you have an idea or a suggestion which will help you and us, let us know.</p>
\t\t\t\t\t</span>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-sm-12 send-mail-link\"><div class=\"send-link\">Send us an email on <a href=\"mailto:careers@businessbid.ae?Subject=careers\" target=\"_top\">careers@businessbid.ae</a></div></div>
\t</div>
\t
<div id=\"valuedpartner\" class=\"valued-partners\">
\t<div class=\"container\">
\t\t<div class=\"page-title1\">Valued Partners</div>
\t\t<h2>If you are interested in teaming up with us and becoming one of our esteemed partners</h2>
\t\t
\t\t<div class=\"tab-content\">
\t\t<h4>Please reach out to us on the details below</h4>
\t\t\t<div class=\"column\">
\t\t\t\t<div class=\"send-link\"><a href=\"mailto:support@businessbid.ae\" target=\"_top\">support@businessbid.ae</a></div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"column\">
\t\t\t\t<div class=\"send-link\"><a href=\"#\">(04) 42 13 777</a></div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:aboutbusinessbid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }
}

<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // ticketing_ticket_ticket_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ticketing_ticket_ticket_homepage')), array (  '_controller' => 'TicketingTicketTicketBundle:Default:index',));
        }

        // b_bids_b_bids_home_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'b_bids_b_bids_home_homepage');
            }

            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::indexAction',  '_route' => 'b_bids_b_bids_home_homepage',);
        }

        if (0 === strpos($pathinfo, '/search')) {
            // b_bids_b_bids_home_search
            if (preg_match('#^/search(?:/(?P<vendorid>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_home_search')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::searchAction',  'vendorid' => 1,));
            }

            // b_bids_b_bids_home_search_resource
            if (0 === strpos($pathinfo, '/search2') && preg_match('#^/search2(?:/(?P<vendorid>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_home_search_resource')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::searchResourceAction',  'vendorid' => 1,));
            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                // b_bids_b_bids_admin_home
                if ($pathinfo === '/admin') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::indexAction',  '_route' => 'b_bids_b_bids_admin_home',);
                }

                // b_bids_b_bids_admin_login
                if (rtrim($pathinfo, '/') === '/admin/login') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_admin_login');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::loginAction',  '_route' => 'b_bids_b_bids_admin_login',);
                }

            }

            // b_bids_b_bids_aboutbusinessbid
            if ($pathinfo === '/aboutbusinessbid') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::aboutbusinessbidAction',  '_route' => 'b_bids_b_bids_aboutbusinessbid',);
            }

            if (0 === strpos($pathinfo, '/admin/categor')) {
                // b_bids_b_bids_admin_categories
                if ($pathinfo === '/admin/categories') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::categoriesAction',  '_route' => 'b_bids_b_bids_admin_categories',);
                }

                // b_bids_b_bids_admin_category_add
                if ($pathinfo === '/admin/category/add') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addcategoryAction',  '_route' => 'b_bids_b_bids_admin_category_add',);
                }

            }

        }

        // b_bids_b_bids_consumer_register
        if ($pathinfo === '/customer/register') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerregisterAction',  '_route' => 'b_bids_b_bids_consumer_register',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            // b_bids_b_bids_user_sms_verify
            if (0 === strpos($pathinfo, '/user/smsverify') && preg_match('#^/user/smsverify/(?P<smscode>[^/]++)/(?P<userid>[^/]++)/(?P<path>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_sms_verify')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::smsverifyAction',));
            }

            // b_bids_b_bids_user_email_verify
            if (0 === strpos($pathinfo, '/user/email/verify') && preg_match('#^/user/email/verify/(?P<userid>[^/]++)/(?P<userhash>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_email_verify')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::emailverifyAction',));
            }

            // b_bids_b_bids_user_sms_verified
            if ($pathinfo === '/user/sms/verified') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::smsverifiedAction',  '_route' => 'b_bids_b_bids_user_sms_verified',);
            }

            // b_bids_b_bids_user_email_verified
            if ($pathinfo === '/user/email/verified') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::emailverifiedAction',  '_route' => 'b_bids_b_bids_user_email_verified',);
            }

        }

        if (0 === strpos($pathinfo, '/post')) {
            // b_bids_b_bids_post_job
            if ($pathinfo === '/post/job') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postjobAction',  '_route' => 'b_bids_b_bids_post_job',);
            }

            // b_bids_b_bids_post_quote
            if ($pathinfo === '/post/quote') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postquoteAction',  '_route' => 'b_bids_b_bids_post_quote',);
            }

        }

        // b_choosesubcategoriesbycategory
        if ($pathinfo === '/chooseSubcategoriesByCategory') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::chooseSubcategoriesByCategoryAction',  '_route' => 'b_choosesubcategoriesbycategory',);
        }

        // b_bids_b_bids_enquiry_processing
        if ($pathinfo === '/enquiry/process') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enquiryprocessAction',  '_route' => 'b_bids_b_bids_enquiry_processing',);
        }

        // b_bids_b_bids_vendor_register
        if ($pathinfo === '/vendor/register') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorregisterAction',  '_route' => 'b_bids_b_bids_vendor_register',);
        }

        if (0 === strpos($pathinfo, '/post/quote/bysearch')) {
            // b_bids_b_bids_post_by_search
            if (preg_match('#^/post/quote/bysearch/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_post_by_search')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postbysearchAction',));
            }

            // b_bids_b_bids_post_by_search_inline
            if (0 === strpos($pathinfo, '/post/quote/bysearch/inline') && preg_match('#^/post/quote/bysearch/inline/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_post_by_search_inline')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::postbysearchinlineAction',));
            }

        }

        // b_bids_b_bids_vendor_register_step_2
        if ($pathinfo === '/vendor/choosecategories') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorcategoriesAction',  '_route' => 'b_bids_b_bids_vendor_register_step_2',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // b_bids_b_bids_admin_master
            if ($pathinfo === '/admin/master') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::loadmasterAction',  '_route' => 'b_bids_b_bids_admin_master',);
            }

            // b_bids_b_bids_account
            if ($pathinfo === '/account') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::accountAction',  '_route' => 'b_bids_b_bids_account',);
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            // b_bids_b_bids_logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::logoutAction',  '_route' => 'b_bids_b_bids_logout',);
            }

            // b_bids_b_bids_login
            if (0 === strpos($pathinfo, '/login') && preg_match('#^/login(?:/(?P<returnurl>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_login')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::loginAction',  'returnurl' => 1,));
            }

        }

        // b_bids_b_bids_vendor_home
        if ($pathinfo === '/vendor/home') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorhomeAction',  '_route' => 'b_bids_b_bids_vendor_home',);
        }

        // b_bids_b_bids_home
        if ($pathinfo === '/home') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::homeAction',  '_route' => 'b_bids_b_bids_home',);
        }

        if (0 === strpos($pathinfo, '/vendor')) {
            // b_bids_b_bids_vendor_buy_leads
            if ($pathinfo === '/vendor/buy/leads') {
                return array (  '_controller' => 'BBidsBBidsHomeBundle:Vendor:buyleads',  '_route' => 'b_bids_b_bids_vendor_buy_leads',);
            }

            // b_bids_b_bids_vendor_purchase_leads
            if ($pathinfo === '/vendor/purchase/leads') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::purchaseLeadsAction',  '_route' => 'b_bids_b_bids_vendor_purchase_leads',);
            }

            // b_bids_b_bids_store_purchase_leads
            if ($pathinfo === '/vendor/store/purchase/leads') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::storePurchaseLeadsAction',  '_route' => 'b_bids_b_bids_store_purchase_leads',);
            }

            // b_bids_b_bids_vendor_lead_plan
            if (0 === strpos($pathinfo, '/vendor/leads/plan') && preg_match('#^/vendor/leads/plan(?:/(?P<purchaseID>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_lead_plan')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsPlanAction',  'purchaseID' => 1,));
            }

            // b_bids_b_bids_store_lead_plan
            if ($pathinfo === '/vendor/store/leads/plan') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::storeLeadsPlanAction',  '_route' => 'b_bids_b_bids_store_lead_plan',);
            }

            // b_bids_b_bids_pymt_form
            if (0 === strpos($pathinfo, '/vendor/leads/payment') && preg_match('#^/vendor/leads/payment/(?P<purchaseID>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_pymt_form')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsPaymentFormAction',));
            }

            // b_bids_b_bids_pymt_success
            if (0 === strpos($pathinfo, '/vendor/payment/confirmation') && preg_match('#^/vendor/payment/confirmation/(?P<ordernumber>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_pymt_success')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsPaymentConfirmationAction',));
            }

        }

        if (0 === strpos($pathinfo, '/leads')) {
            // b_bids_b_bids_leads_history
            if ($pathinfo === '/leads/history') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadhistoryAction',  '_route' => 'b_bids_b_bids_leads_history',);
            }

            if (0 === strpos($pathinfo, '/leads/view')) {
                // b_bids_b_bids_view_leads
                if ($pathinfo === '/leads/view') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsviewAction',  '_route' => 'b_bids_b_bids_view_leads',);
                }

                // b_bids_b_bids_view_leads_history
                if (0 === strpos($pathinfo, '/leads/view/history') && preg_match('#^/leads/view/history/(?P<uid>[^/]++)/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_view_leads_history')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::leadsviewhistoryAction',));
                }

            }

        }

        // b_bids_b_bids_vendor_choose_lead_pack
        if ($pathinfo === '/vendor/choosepack') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::choosepackAction',  '_route' => 'b_bids_b_bids_vendor_choose_lead_pack',);
        }

        // b_bids_b_bids_consumer_home
        if (0 === strpos($pathinfo, '/customer/home') && preg_match('#^/customer/home(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumer_home')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerhomeAction',  'offset' => 1,));
        }

        // b_bids_b_bids_vendor_enquiries
        if (0 === strpos($pathinfo, '/vendor/enquiries') && preg_match('#^/vendor/enquiries(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_enquiries')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorenquiriesAction',  'offset' => 1,));
        }

        // b_bids_b_bids_enquiries_my
        if ($pathinfo === '/enquiries/my') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::myenquiriesAction',  '_route' => 'b_bids_b_bids_enquiries_my',);
        }

        // b_bids_b_bids_leads_my
        if ($pathinfo === '/leads/my') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::buyleadsAction',  '_route' => 'b_bids_b_bids_leads_my',);
        }

        // b_bids_b_bids_accept_enquiry
        if (0 === strpos($pathinfo, '/enquiry/accept') && preg_match('#^/enquiry/accept/(?P<category>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_accept_enquiry')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::acceptenquiryAction',));
        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/user')) {
                if (0 === strpos($pathinfo, '/admin/users')) {
                    // b_bids_b_bids_admin_users
                    if (preg_match('#^/admin/users(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_users')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::usersAction',  'offset' => 1,));
                    }

                    // b_bids_b_bids_admin_users_new
                    if ($pathinfo === '/admin/usersnew') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::usersNewAction',  '_route' => 'b_bids_b_bids_admin_users_new',);
                    }

                    // b_bids_b_bids_admin_users_grid
                    if ($pathinfo === '/admin/usersgrid') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::usersGridAction',  '_route' => 'b_bids_b_bids_admin_users_grid',);
                    }

                }

                // b_bids_b_bids_admin_user_edit
                if (0 === strpos($pathinfo, '/admin/user/edit') && preg_match('#^/admin/user/edit/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_user_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::edituserAction',));
                }

                // b_bids_b_bids_admin_user_delete
                if (0 === strpos($pathinfo, '/admin/user/delete') && preg_match('#^/admin/user/delete/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_user_delete')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::deleteuserAction',));
                }

                // b_bids_b_bids_admin_user_view
                if (0 === strpos($pathinfo, '/admin/user/view') && preg_match('#^/admin/user/view/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_user_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::viewuserAction',));
                }

            }

            // b_bids_b_bids_freetrails
            if (0 === strpos($pathinfo, '/admin/freetrail') && preg_match('#^/admin/freetrail(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_freetrails')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::freetrailAction',  'offset' => 1,));
            }

            if (0 === strpos($pathinfo, '/admin/enquir')) {
                // b_bids_b_bids_admin_enquiries
                if (0 === strpos($pathinfo, '/admin/enquiries') && preg_match('#^/admin/enquiries(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enquiries')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiriesAction',  'offset' => 1,));
                }

                if (0 === strpos($pathinfo, '/admin/enquiry')) {
                    // b_bids_b_bids_admin_enquiry_delete
                    if ($pathinfo === '/admin/enquiry/delete') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquirydeleteAction',  '_route' => 'b_bids_b_bids_admin_enquiry_delete',);
                    }

                    // b_bids_b_bids_admin_enquiry_edit
                    if (0 === strpos($pathinfo, '/admin/enquiry/edit') && preg_match('#^/admin/enquiry/edit/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enquiry_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiryeditAction',));
                    }

                    // b_bids_b_bids_admin_enquiry_view
                    if (0 === strpos($pathinfo, '/admin/enquiry/view') && preg_match('#^/admin/enquiry/view/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enquiry_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiryviewAction',));
                    }

                }

            }

            // b_bids_b_bids_admin_mapped_vendors
            if (0 === strpos($pathinfo, '/admin/mapped/vendors') && preg_match('#^/admin/mapped/vendors/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_mapped_vendors')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::mappedvendorsAction',));
            }

            // b_bids_b_bids_admin_vendor_response
            if (0 === strpos($pathinfo, '/admin/vendor/response') && preg_match('#^/admin/vendor/response/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_vendor_response')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorresponseAction',));
            }

            if (0 === strpos($pathinfo, '/admin/add')) {
                // b_bids_b_bids_admin_add_subcategory
                if (0 === strpos($pathinfo, '/admin/add/subcategory') && preg_match('#^/admin/add/subcategory/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_add_subcategory')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addsubcategoryAction',));
                }

                // b_bids_b_bids_admin_add_keyword
                if (0 === strpos($pathinfo, '/admin/add/keyword') && preg_match('#^/admin/add/keyword/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_add_keyword')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addkeywordAction',));
                }

            }

            // b_bids_b_bids_admin_subcategories
            if (0 === strpos($pathinfo, '/admin/subcategories') && preg_match('#^/admin/subcategories/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_subcategories')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::subcategoriesAction',));
            }

            // b_bids_b_bids_admin_keywords
            if (0 === strpos($pathinfo, '/admin/keywords') && preg_match('#^/admin/keywords/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_keywords')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::keywordsAction',));
            }

            // b_bids_b_bids_admin_add_category
            if ($pathinfo === '/admin/add/category') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::addcategoryAction',  '_route' => 'b_bids_b_bids_admin_add_category',);
            }

        }

        // b_bids_b_bids_forgot_password
        if ($pathinfo === '/forgotpassword') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::forgotpasswordAction',  '_route' => 'b_bids_b_bids_forgot_password',);
        }

        // b_bids_b_bids_dashboard
        if ($pathinfo === '/dashboard') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::dashboardAction',  '_route' => 'b_bids_b_bids_dashboard',);
        }

        if (0 === strpos($pathinfo, '/admin')) {
            // b_bids_b_bids_admin_edit_category
            if (0 === strpos($pathinfo, '/admin/edit/category') && preg_match('#^/admin/edit/category/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit_category')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editcategoryAction',));
            }

            // b_bids_b_bids_admin_delete_category
            if (0 === strpos($pathinfo, '/admin/delete/category') && preg_match('#^/admin/delete/category/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_delete_category')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::deletecategoryAction',));
            }

            if (0 === strpos($pathinfo, '/admin/e')) {
                // b_bids_b_bids_admin_enable_category
                if (0 === strpos($pathinfo, '/admin/enable/category') && preg_match('#^/admin/enable/category/(?P<catid>[^/]++)/(?P<stat>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_enable_category')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enablecategoryAction',));
                }

                if (0 === strpos($pathinfo, '/admin/edit')) {
                    // b_bids_b_bids_admin_edit_subcategory
                    if (0 === strpos($pathinfo, '/admin/edit/subcategory') && preg_match('#^/admin/edit/subcategory/(?P<categoryid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit_subcategory')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editsubcategoryAction',));
                    }

                    // b_bids_b_bids_admin_edit_keyword
                    if (0 === strpos($pathinfo, '/admin/edit/keyword') && preg_match('#^/admin/edit/keyword/(?P<keyid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit_keyword')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editkeywordAction',));
                    }

                }

            }

            // b_bids_b_bids_admin_delete_keyword
            if (0 === strpos($pathinfo, '/admin/delete/keyword') && preg_match('#^/admin/delete/keyword/(?P<keyid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_delete_keyword')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::deletekeywordAction',));
            }

        }

        if (0 === strpos($pathinfo, '/user')) {
            // b_bids_b_bids_user_authenticate
            if (0 === strpos($pathinfo, '/user/authenticate') && preg_match('#^/user/authenticate/(?P<email>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_authenticate')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::authenticateAction',));
            }

            // b_bids_b_bids_user_email_activate
            if (0 === strpos($pathinfo, '/user/email/activate') && preg_match('#^/user/email/activate/(?P<email>[^/]++)/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_email_activate')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::emailactivateAction',));
            }

        }

        // b_bids_b_bids_how_it_works_2
        if ($pathinfo === '/node3') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node3Action',  '_route' => 'b_bids_b_bids_how_it_works_2',);
        }

        // b_bids_b_bids_forgot_password_auto
        if (0 === strpos($pathinfo, '/user/forgotpassword/auto') && preg_match('#^/user/forgotpassword/auto/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_forgot_password_auto')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::forgotpasswordautoAction',));
        }

        // b_bids_b_bids_node4
        if ($pathinfo === '/node4') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node4Action',  '_route' => 'b_bids_b_bids_node4',);
        }

        if (0 === strpos($pathinfo, '/review')) {
            // b_bids_b_bids_reviewform_post
            if (0 === strpos($pathinfo, '/reviews/list') && preg_match('#^/reviews/list/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviewform_post')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewsListByCategoryAction',));
            }

            // b_bids_b_bids_reviews_quote
            if ($pathinfo === '/review/quote/vendor') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::storeReviewVendorsAction',  '_route' => 'b_bids_b_bids_reviews_quote',);
            }

        }

        // b_bids_b_bids_jobform_post
        if ($pathinfo === '/jobformpost') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::jobformpostAction',  '_route' => 'b_bids_b_bids_jobform_post',);
        }

        // b_bids_b_bids_node5
        if ($pathinfo === '/node5') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node5Action',  '_route' => 'b_bids_b_bids_node5',);
        }

        if (0 === strpos($pathinfo, '/vendor')) {
            // b_bids_b_bids_vendor
            if (0 === strpos($pathinfo, '/vendor/profile') && preg_match('#^/vendor/profile/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vendorProfileAction',));
            }

            // b_bids_b_bids_vendor_update
            if (0 === strpos($pathinfo, '/vendor/update') && preg_match('#^/vendor/update/(?P<vendorid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_update')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorupdateAction',));
            }

        }

        // b_bids_b_bids_consumer_update
        if (0 === strpos($pathinfo, '/customer/update') && preg_match('#^/customer/update/(?P<consumerid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumer_update')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerupdateAction',));
        }

        if (0 === strpos($pathinfo, '/vendor')) {
            if (0 === strpos($pathinfo, '/vendor/save')) {
                // b_bids_b_bids_vendor_save_account
                if ($pathinfo === '/vendor/save/account') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::saveaccountAction',  '_route' => 'b_bids_b_bids_vendor_save_account',);
                }

                if (0 === strpos($pathinfo, '/vendor/save/c')) {
                    // b_bids_b_bids_vendor_save_city
                    if ($pathinfo === '/vendor/save/city') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savecityAction',  '_route' => 'b_bids_b_bids_vendor_save_city',);
                    }

                    // b_bids_b_bids_vendor_save_certification
                    if ($pathinfo === '/vendor/save/certification') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savecertificationAction',  '_route' => 'b_bids_b_bids_vendor_save_certification',);
                    }

                }

                // b_bids_b_bids_vendor_save_product
                if ($pathinfo === '/vendor/save/product') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::saveproductAction',  '_route' => 'b_bids_b_bids_vendor_save_product',);
                }

                // b_bids_b_bids_vendor_save_album
                if ($pathinfo === '/vendor/save/album') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savealbumAction',  '_route' => 'b_bids_b_bids_vendor_save_album',);
                }

            }

            // b_bids_b_bids_unlink_album
            if (0 === strpos($pathinfo, '/vendor/unlink/album') && preg_match('#^/vendor/unlink/album/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_album')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removeAlbumAction',));
            }

            if (0 === strpos($pathinfo, '/vendor/save')) {
                // b_bids_b_bids_vendor_save_photo
                if ($pathinfo === '/vendor/save/photo') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::savephotoAction',  '_route' => 'b_bids_b_bids_vendor_save_photo',);
                }

                // b_bids_b_bids_vendor_save_logo
                if ($pathinfo === '/vendor/save/logo') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::savelogoAction',  '_route' => 'b_bids_b_bids_vendor_save_logo',);
                }

            }

        }

        // b_bids_b_bids_album_view
        if (0 === strpos($pathinfo, '/album/view') && preg_match('#^/album/view/(?P<albumid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_album_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::albumviewAction',));
        }

        if (0 === strpos($pathinfo, '/p')) {
            // b_bids_b_bids_add_photo
            if (0 === strpos($pathinfo, '/photo/add') && preg_match('#^/photo/add/(?P<albumid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_add_photo')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::addphotoAction',));
            }

            // b_bids_b_bids_post_review
            if (0 === strpos($pathinfo, '/post/review') && preg_match('#^/post/review/(?P<authorid>[^/]++)/(?P<vendorid>[^/]++)/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_post_review')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::postreviewAction',));
            }

        }

        if (0 === strpos($pathinfo, '/admin/review')) {
            // b_bids_b_bids_reviews
            if (0 === strpos($pathinfo, '/admin/reviews') && preg_match('#^/admin/reviews/(?P<offset>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::reviewsAction',));
            }

            // b_bids_b_bids_review
            if (preg_match('#^/admin/review/(?P<reviewid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_review')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::reviewAction',));
            }

            // b_bids_b_bids_admin_publish_review
            if (0 === strpos($pathinfo, '/admin/review/publish') && preg_match('#^/admin/review/publish/(?P<reviewid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_publish_review')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::publishreviewAction',));
            }

        }

        // b_bids_b_bids_node6
        if ($pathinfo === '/node6') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::node6Action',  '_route' => 'b_bids_b_bids_node6',);
        }

        if (0 === strpos($pathinfo, '/vendor/unlink')) {
            // b_bids_b_bids_unlink_certification
            if (0 === strpos($pathinfo, '/vendor/unlink/certification') && preg_match('#^/vendor/unlink/certification/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_certification')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removecertificationAction',));
            }

            // b_bids_b_bids_unlink_product
            if (0 === strpos($pathinfo, '/vendor/unlink/product') && preg_match('#^/vendor/unlink/product/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_product')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removeproductsAction',));
            }

        }

        if (0 === strpos($pathinfo, '/admin/order')) {
            // b_bids_b_bids_admin_orders
            if (0 === strpos($pathinfo, '/admin/orders') && preg_match('#^/admin/orders/(?P<offset>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_orders')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::ordersAction',));
            }

            // b_bids_b_bids_admin_approve_order
            if (0 === strpos($pathinfo, '/admin/order/approve') && preg_match('#^/admin/order/approve/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_approve_order')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::orderapproveAction',));
            }

            // b_bids_b_bids_admin_order_view
            if (0 === strpos($pathinfo, '/admin/order/view') && preg_match('#^/admin/order/view/(?P<id>[^/]++)/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_order_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vieworderAction',));
            }

        }

        // b_bids_b_bids_change_password
        if ($pathinfo === '/password/change') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::changepasswordAction',  '_route' => 'b_bids_b_bids_change_password',);
        }

        // b_bids_b_bids_view_consumer
        if (0 === strpos($pathinfo, '/customer/view') && preg_match('#^/customer/view/(?P<id>[^/]++)/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_view_consumer')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerviewAction',));
        }

        if (0 === strpos($pathinfo, '/vendorview')) {
            // b_bids_b_bids_vendororders
            if (0 === strpos($pathinfo, '/vendorview/orders') && preg_match('#^/vendorview/orders(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendororders')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorordersAction',  'offset' => 1,));
            }

            // b_bids_b_bids_vendorreviews
            if (0 === strpos($pathinfo, '/vendorview/reviews') && preg_match('#^/vendorview/reviews(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendorreviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorreviewsAction',  'offset' => 1,));
            }

        }

        if (0 === strpos($pathinfo, '/customer')) {
            // b_bids_b_bids_consumerreviews
            if (0 === strpos($pathinfo, '/customerview/reviews') && preg_match('#^/customerview/reviews/(?P<offset>[^/\\:]++)\\:$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumerreviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerreviewsAction',  'offset' => 1,));
            }

            // b_bids_b_bids_customerorder
            if ($pathinfo === '/customerorder:') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerordersAction',  '_route' => 'b_bids_b_bids_customerorder',);
            }

        }

        // b_bids_b_bids_admin_report
        if ($pathinfo === '/admin/report') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::adminReportAction',  '_route' => 'b_bids_b_bids_admin_report',);
        }

        // b_bids_b_bids_try_it_pack
        if (0 === strpos($pathinfo, '/vendor/tryitnoworder') && preg_match('#^/vendor/tryitnoworder/(?P<categoryArray>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_try_it_pack')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::tryitnoworderAction',));
        }

        // b_bids_b_bids_admin_tryleads
        if (0 === strpos($pathinfo, '/admin/tryitnowleads') && preg_match('#^/admin/tryitnowleads(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_tryleads')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::viewtryleadsAction',  'offset' => 1,));
        }

        // b_bids_b_bids_facebook_login
        if (0 === strpos($pathinfo, '/facebook/signin') && preg_match('#^/facebook/signin/(?P<userid>[^/]++)/(?P<email>[^/]++)/(?P<fname>[^/]++)/(?P<pid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_facebook_login')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::facebookloginAction',));
        }

        // b_bids_b_bids_setsessioncategory
        if (0 === strpos($pathinfo, '/setsessioncategory') && preg_match('#^/setsessioncategory/(?P<catid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_setsessioncategory')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::setsessioncategoryAction',));
        }

        // b_bids_b_bids_my_reviews
        if (0 === strpos($pathinfo, '/reviews') && preg_match('#^/reviews(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_my_reviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::myreviewsAction',  'offset' => 1,));
        }

        // b_bids_b_bids_enquiry_view
        if (0 === strpos($pathinfo, '/en/view') && preg_match('#^/en/view/(?P<enquiryid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_enquiry_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enviewAction',));
        }

        // b_bids_b_bids_reviewsmy
        if (0 === strpos($pathinfo, '/my/reviews') && preg_match('#^/my/reviews(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviewsmy')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::conreviewsAction',  'offset' => 1,));
        }

        // b_bids_b_bids_faq
        if ($pathinfo === '/faq') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::faqAction',  '_route' => 'b_bids_b_bids_faq',);
        }

        if (0 === strpos($pathinfo, '/support')) {
            // b_bids_b_bids_support
            if ($pathinfo === '/support') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportAction',  '_route' => 'b_bids_b_bids_support',);
            }

            // b_bids_b_bids_supportview
            if ($pathinfo === '/support/view') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportviewAction',  '_route' => 'b_bids_b_bids_supportview',);
            }

        }

        // b_bids_b_bids_consumer_postenquiries
        if (0 === strpos($pathinfo, '/customer/enquiries') && preg_match('#^/customer/enquiries(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_consumer_postenquiries')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumerenquiriesAction',  'offset' => 1,));
        }

        // b_bids_b_bids_view_vendor
        if (0 === strpos($pathinfo, '/vendor/view') && preg_match('#^/vendor/view/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_view_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorviewAction',));
        }

        if (0 === strpos($pathinfo, '/customer')) {
            // b_bids_b_bids_consumeraccount
            if ($pathinfo === '/customer/account') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumeraccountAction',  '_route' => 'b_bids_b_bids_consumeraccount',);
            }

            if (0 === strpos($pathinfo, '/customer/s')) {
                // b_bids_b_bids_consumer_save_account
                if ($pathinfo === '/customer/save/account') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::consumersaveaccountAction',  '_route' => 'b_bids_b_bids_consumer_save_account',);
                }

                if (0 === strpos($pathinfo, '/customer/support')) {
                    // b_bids_b_bids_support_consumer
                    if ($pathinfo === '/customer/support') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportconsumerAction',  '_route' => 'b_bids_b_bids_support_consumer',);
                    }

                    // b_bids_b_bids_supportview_consumer
                    if ($pathinfo === '/customer/support/view') {
                        return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::supportviewconsumerAction',  '_route' => 'b_bids_b_bids_supportview_consumer',);
                    }

                }

            }

            // b_bids_b_bids_ratings
            if (0 === strpos($pathinfo, '/customer/ratings') && preg_match('#^/customer/ratings(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ratings')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::consumerratingsAction',  'offset' => 1,));
            }

        }

        // b_bids_b_bids_mapped_vendor
        if (0 === strpos($pathinfo, '/mapped/vendor') && preg_match('#^/mapped/vendor/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::mappedvendortocustAction',));
        }

        // b_bids_b_bids_feedback
        if ($pathinfo === '/feedback') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::feedbackAction',  '_route' => 'b_bids_b_bids_feedback',);
        }

        // b_bids_b_bids_vendor_order_view
        if (0 === strpos($pathinfo, '/vendor/order/view') && preg_match('#^/vendor/order/view/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_vendor_order_view')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vieworderAction',));
        }

        // b_bids_b_bids_admin_review_edit
        if (0 === strpos($pathinfo, '/admin/edit/review') && preg_match('#^/admin/edit/review/(?P<reviewid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_review_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editreviewAction',));
        }

        // b_bids_b_bids_reviewlogin
        if (0 === strpos($pathinfo, '/review/login') && preg_match('#^/review/login(?:/(?P<returnurl>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_reviewlogin')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewloginAction',  'returnurl' => 1,));
        }

        // b_bids_b_bids_admin_edit
        if (0 === strpos($pathinfo, '/admin/profile/edit') && preg_match('#^/admin/profile/edit/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_edit')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::editadminAction',));
        }

        if (0 === strpos($pathinfo, '/mapped')) {
            // b_bids_b_bids_mapped_enquiry
            if (0 === strpos($pathinfo, '/mapped/enquiry') && preg_match('#^/mapped/enquiry/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_enquiry')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enquirydetailsAction',));
            }

            // b_bids_b_bids_mapped_reviews
            if (0 === strpos($pathinfo, '/mapped/review') && preg_match('#^/mapped/review/(?P<rid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_reviews')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewdetailsAction',));
            }

            // b_bids_b_bids_mapped_enquiry_vendor
            if (0 === strpos($pathinfo, '/mapped/enquiry/vendor') && preg_match('#^/mapped/enquiry/vendor/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_enquiry_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::enquirydetailsvendorAction',));
            }

            // b_bids_b_bids_mapped_reviews_vendor
            if (0 === strpos($pathinfo, '/mapped/review/vendor') && preg_match('#^/mapped/review/vendor/(?P<rid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_mapped_reviews_vendor')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::reviewdetailsvendorAction',));
            }

        }

        if (0 === strpos($pathinfo, '/vendors')) {
            if (0 === strpos($pathinfo, '/vendors/datatable')) {
                // b_bids_b_bids_ticket_index
                if ($pathinfo === '/vendors/datatable') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::gridAction',  '_route' => 'b_bids_b_bids_ticket_index',);
                }

                // b_bids_b_bids_ticket_datatable
                if ($pathinfo === '/vendors/datatableindex') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::indexAction',  '_route' => 'b_bids_b_bids_ticket_datatable',);
                }

            }

            // b_bids_b_bids_ticket_home
            if ($pathinfo === '/vendors/helpdesk') {
                return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::vendorTicketHomeAction',  '_route' => 'b_bids_b_bids_ticket_home',);
            }

            if (0 === strpos($pathinfo, '/vendors/ticket')) {
                // b_bids_b_bids_ticket_list
                if ($pathinfo === '/vendors/ticketlist') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::vendorTicketListAction',  '_route' => 'b_bids_b_bids_ticket_list',);
                }

                // b_bids_b_bids_ticket_view
                if (0 === strpos($pathinfo, '/vendors/ticketview/no') && preg_match('#^/vendors/ticketview/no\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ticket_view')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\HomeController::vendorViewTicketAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/helpdesk')) {
                // b_bids_b_bids_ticket_adminhome
                if ($pathinfo === '/admin/helpdesk/dashboard') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\AdminticketController::adminTicketHomeAction',  '_route' => 'b_bids_b_bids_ticket_adminhome',);
                }

                // b_bids_b_bids_ticket_adminlist
                if (0 === strpos($pathinfo, '/admin/helpdesk/list') && preg_match('#^/admin/helpdesk/list/(?P<usertype>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ticket_adminlist')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\AdminticketController::adminListTicketsAction',));
                }

            }

            // b_bids_b_bids_ticket_adminview
            if (0 === strpos($pathinfo, '/admin/ticketview/no') && preg_match('#^/admin/ticketview/no\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_ticket_adminview')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\AdminticketController::adminViewTicketAction',));
            }

        }

        // b_bids_b_bids_mymessages
        if ($pathinfo === '/mymessages') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::getemailmessagesAction',  '_route' => 'b_bids_b_bids_mymessages',);
        }

        // b_bids_b_bids_customer_messages
        if ($pathinfo === '/customer/messages') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::getcustomeremailmessagesAction',  '_route' => 'b_bids_b_bids_customer_messages',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            // b_bids_b_bids_customer_ticket_home
            if ($pathinfo === '/user/helpdesk') {
                return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\CustomerticketController::customerTicketHomeAction',  '_route' => 'b_bids_b_bids_customer_ticket_home',);
            }

            if (0 === strpos($pathinfo, '/user/ticket')) {
                // b_bids_b_bids_customer_ticket_list
                if ($pathinfo === '/user/ticketlist') {
                    return array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\CustomerticketController::customerTicketListAction',  '_route' => 'b_bids_b_bids_customer_ticket_list',);
                }

                // b_bids_b_bids_customer_ticket_view
                if (0 === strpos($pathinfo, '/user/ticketview/no') && preg_match('#^/user/ticketview/no\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_customer_ticket_view')), array (  '_controller' => 'Ticketing\\Ticket\\TicketBundle\\Controller\\CustomerticketController::customerViewTicketAction',));
                }

            }

            if (0 === strpos($pathinfo, '/user/custtovendor')) {
                // b_bids_b_bids_user_custtovendorupgration_success
                if ($pathinfo === '/user/custtovendor/success') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::custovendorupgrationAction',  '_route' => 'b_bids_b_bids_user_custtovendorupgration_success',);
                }

                // b_bids_b_bids_user_custtovendorupgration_check
                if (0 === strpos($pathinfo, '/user/custtovendor/check') && preg_match('#^/user/custtovendor/check/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_user_custtovendorupgration_check')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::custovendorupgrationcheckAction',));
                }

            }

        }

        // b_bids_b_bids_sitemap
        if ($pathinfo === '/sitemap') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::sitemapAction',  '_route' => 'b_bids_b_bids_sitemap',);
        }

        // b_bids_b_bids_contactus
        if ($pathinfo === '/contactus') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::contactusAction',  '_route' => 'b_bids_b_bids_contactus',);
        }

        // b_bids_b_bids_vendorlicense
        if ($pathinfo === '/vendors/licenseupdate') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendorLicenseAction',  '_route' => 'b_bids_b_bids_vendorlicense',);
        }

        // b_bids_b_bids_globalsearch
        if ($pathinfo === '/site/search') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::globalSearchAction',  '_route' => 'b_bids_b_bids_globalsearch',);
        }

        // b_bids_b_bids_admin_lead_management
        if (0 === strpos($pathinfo, '/admin/lead/credits') && preg_match('#^/admin/lead/credits(?:/(?P<offset>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_admin_lead_management')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::leadrefundAction',  'offset' => 1,));
        }

        // b_bids_b_bids_unlink_lisence
        if (0 === strpos($pathinfo, '/vendor/unlink/lisence') && preg_match('#^/vendor/unlink/lisence/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_unlink_lisence')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::removelisenceAction',));
        }

        // b_bids_b_bids_admin_dashboard
        if (rtrim($pathinfo, '/') === '/admin/dashboard') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'b_bids_b_bids_admin_dashboard');
            }

            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::homeAction',  '_route' => 'b_bids_b_bids_admin_dashboard',);
        }

        if (0 === strpos($pathinfo, '/customer/con')) {
            // b_bids_b_bids_consumerupdatepasssword
            if (rtrim($pathinfo, '/') === '/customer/conchangepassword') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_consumerupdatepasssword');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::conchangepasswordAction',  '_route' => 'b_bids_b_bids_consumerupdatepasssword',);
            }

            // b_bids_b_bids_consumerupdatemail
            if (rtrim($pathinfo, '/') === '/customer/conupdateemail') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_consumerupdatemail');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::conupdateemailAction',  '_route' => 'b_bids_b_bids_consumerupdatemail',);
            }

            // b_bids_b_bids_consumerdeleteAccount
            if (rtrim($pathinfo, '/') === '/customer/condeleteAccount') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_consumerdeleteAccount');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::condeleteAccountAction',  '_route' => 'b_bids_b_bids_consumerdeleteAccount',);
            }

        }

        if (0 === strpos($pathinfo, '/vendor')) {
            if (0 === strpos($pathinfo, '/vendor/ven')) {
                // b_bids_b_bids_vendorupdatepasssword
                if (rtrim($pathinfo, '/') === '/vendor/venchangepassword') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendorupdatepasssword');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::venchangepasswordAction',  '_route' => 'b_bids_b_bids_vendorupdatepasssword',);
                }

                // b_bids_b_bids_vendorupdatemail
                if (rtrim($pathinfo, '/') === '/vendor/venupdateemail') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendorupdatemail');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::venupdateemailAction',  '_route' => 'b_bids_b_bids_vendorupdatemail',);
                }

                // b_bids_b_bids_vendordeleteAccount
                if (rtrim($pathinfo, '/') === '/vendor/vendeleteAccount') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendordeleteAccount');
                    }

                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::vendeleteAccountAction',  '_route' => 'b_bids_b_bids_vendordeleteAccount',);
                }

            }

            // b_bids_b_bids_vendor_reports
            if (rtrim($pathinfo, '/') === '/vendor/reports') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_vendor_reports');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorReportsAction',  '_route' => 'b_bids_b_bids_vendor_reports',);
            }

        }

        // b_bids_b_bids_customer_reports
        if (rtrim($pathinfo, '/') === '/customer/reports') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'b_bids_b_bids_customer_reports');
            }

            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::customerReportsAction',  '_route' => 'b_bids_b_bids_customer_reports',);
        }

        if (0 === strpos($pathinfo, '/orders')) {
            // b_bids_b_bids_orders_reports
            if (rtrim($pathinfo, '/') === '/orders/reports') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'b_bids_b_bids_orders_reports');
                }

                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::orderReportsAction',  '_route' => 'b_bids_b_bids_orders_reports',);
            }

            // b_bids_b_bids_exportall_orders
            if ($pathinfo === '/orders/export/all') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportOrdersToExcelAction',  '_route' => 'b_bids_b_bids_exportall_orders',);
            }

        }

        // b_bids_b_bids_categories_reports
        if (rtrim($pathinfo, '/') === '/categories/reports') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'b_bids_b_bids_categories_reports');
            }

            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::categoriesReportsAction',  '_route' => 'b_bids_b_bids_categories_reports',);
        }

        // b_bids_b_bids_enquiries_reports
        if (rtrim($pathinfo, '/') === '/enquiries/reports') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'b_bids_b_bids_enquiries_reports');
            }

            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::enquiriesReportsAction',  '_route' => 'b_bids_b_bids_enquiries_reports',);
        }

        // b_bids_b_bids_exportall_customer
        if ($pathinfo === '/customer/export/all') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportCustomersToExcelAction',  '_route' => 'b_bids_b_bids_exportall_customer',);
        }

        // b_bids_b_bids_exportall_vendors
        if ($pathinfo === '/vendor/export/all') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportVendorToExcelAction',  '_route' => 'b_bids_b_bids_exportall_vendors',);
        }

        // b_bids_b_bids_exportall_jobrequest
        if ($pathinfo === '/job/request/export/all') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportJobrequestToExcelAction',  '_route' => 'b_bids_b_bids_exportall_jobrequest',);
        }

        // b_bids_b_bids_exportall_categories
        if ($pathinfo === '/categories/export/all') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportCategoriesToExcelAction',  '_route' => 'b_bids_b_bids_exportall_categories',);
        }

        if (0 === strpos($pathinfo, '/sms')) {
            // b_bids_b_bids_sms_replies
            if ($pathinfo === '/sms/replies/all') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\SmsController::smsRepliesAction',  '_route' => 'b_bids_b_bids_sms_replies',);
            }

            // b_bids_b_bids_sms_job_req
            if ($pathinfo === '/sms/job/request') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\SmsController::sendJobReqSmsToVendorsAction',  '_route' => 'b_bids_b_bids_sms_job_req',);
            }

        }

        // b_bids_b_bids_international_user__verified
        if ($pathinfo === '/international/user/verified') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::internationaluserverifiedAction',  '_route' => 'b_bids_b_bids_international_user__verified',);
        }

        // b_bids_b_bids_pgateway_paypal
        if (0 === strpos($pathinfo, '/paymentgateway/paypal') && preg_match('#^/paymentgateway/paypal/(?P<purchaseID>[^/]++)/(?P<actiontype>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_pgateway_paypal')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::paypalPaymentgatewayAction',));
        }

        // b_bids_b_bids_accouting_auditing_ajaxaction
        if ($pathinfo === '/accouting/auditing/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::accountingAuditingAction',  '_route' => 'b_bids_b_bids_accouting_auditing_ajaxaction',);
        }

        // b_bids_b_bids_catering_services_ajaxaction
        if ($pathinfo === '/catering/service/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::cateringServicesAction',  '_route' => 'b_bids_b_bids_catering_services_ajaxaction',);
        }

        if (0 === strpos($pathinfo, '/i')) {
            // b_bids_b_bids_interior_designers_ajaxaction
            if ($pathinfo === '/interior/designers/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::interiorDesignersFitoutAction',  '_route' => 'b_bids_b_bids_interior_designers_ajaxaction',);
            }

            // b_bids_b_bids_it_support_ajaxaction
            if ($pathinfo === '/it/support/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::itSupportAction',  '_route' => 'b_bids_b_bids_it_support_ajaxaction',);
            }

        }

        // b_bids_b_bids_landscape_gardens_ajaxaction
        if ($pathinfo === '/landscape/gardens/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::landscapeGardensAction',  '_route' => 'b_bids_b_bids_landscape_gardens_ajaxaction',);
        }

        // b_bids_b_bids_signage_signboards_ajaxaction
        if ($pathinfo === '/signage/signboards/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::signageSignboardsAction',  '_route' => 'b_bids_b_bids_signage_signboards_ajaxaction',);
        }

        // b_bids_b_bids_photography_video_ajaxaction
        if ($pathinfo === '/photography/video/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::photographyVideoAction',  '_route' => 'b_bids_b_bids_photography_video_ajaxaction',);
        }

        // b_bids_b_bids_offset_printing_ajaxaction
        if ($pathinfo === '/ofset/printing/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::offsetPrintingAction',  '_route' => 'b_bids_b_bids_offset_printing_ajaxaction',);
        }

        // b_bids_b_bids_commercial_cleaning_ajaxaction
        if ($pathinfo === '/commercial/cleaning/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::commercialCleaningAction',  '_route' => 'b_bids_b_bids_commercial_cleaning_ajaxaction',);
        }

        // biz_bids_florists_ajaxaction
        if ($pathinfo === '/florists/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::floristsAction',  '_route' => 'biz_bids_florists_ajaxaction',);
        }

        // biz_bids_event_managment_ajaxaction
        if ($pathinfo === '/event/managment/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::eventManagmentAction',  '_route' => 'biz_bids_event_managment_ajaxaction',);
        }

        // biz_bids_weeding_planners_ajaxaction
        if ($pathinfo === '/weeding/planners/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::weedingPlannersAction',  '_route' => 'biz_bids_weeding_planners_ajaxaction',);
        }

        // biz_bids_exhibitions_ajaxaction
        if ($pathinfo === '/exhibitions/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::exhibitionsAction',  '_route' => 'biz_bids_exhibitions_ajaxaction',);
        }

        // biz_bids_gifts_promotional_ajaxaction
        if ($pathinfo === '/gifts/promotional/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::giftsPromotionalAction',  '_route' => 'biz_bids_gifts_promotional_ajaxaction',);
        }

        // biz_bids_sound_lighting_ajaxaction
        if ($pathinfo === '/sound/lighting/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::soundLightingAction',  '_route' => 'biz_bids_sound_lighting_ajaxaction',);
        }

        // b_bids_b_bids_maintenance_ajaxaction
        if ($pathinfo === '/maintenance/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::maintenanceAction',  '_route' => 'b_bids_b_bids_maintenance_ajaxaction',);
        }

        // b_bids_b_bids_pest_control_ajaxaction
        if ($pathinfo === '/pest/control/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::pestControlAction',  '_route' => 'b_bids_b_bids_pest_control_ajaxaction',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // biz_bids_air_conditioning_cooling_ajaxaction
            if ($pathinfo === '/air-conditioning/air-cooling/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::airConditioningCoolingAction',  '_route' => 'biz_bids_air_conditioning_cooling_ajaxaction',);
            }

            // biz_bids_attestation_translation_ajaxaction
            if ($pathinfo === '/attestation/translation/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::attestationTranslationAction',  '_route' => 'biz_bids_attestation_translation_ajaxaction',);
            }

        }

        if (0 === strpos($pathinfo, '/b')) {
            // biz_bids_babysitter_nanny_ajaxaction
            if ($pathinfo === '/babysitter/nanny/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::babysitterNannyAction',  '_route' => 'biz_bids_babysitter_nanny_ajaxaction',);
            }

            // biz_bids_beauty_styling_ajaxaction
            if ($pathinfo === '/beauty/styling/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::beautyStylingAction',  '_route' => 'biz_bids_beauty_styling_ajaxaction',);
            }

        }

        if (0 === strpos($pathinfo, '/co')) {
            // biz_bids_computer_phone_repair_ajaxaction
            if ($pathinfo === '/computer/phone/repair/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::computerPhoneRepairAction',  '_route' => 'biz_bids_computer_phone_repair_ajaxaction',);
            }

            // biz_bids_concierge_errands_ajaxaction
            if ($pathinfo === '/concierge/errands/ajaxformaction') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::conciergeErrandsAction',  '_route' => 'biz_bids_concierge_errands_ajaxaction',);
            }

        }

        // biz_bids_education_learning_ajaxaction
        if ($pathinfo === '/education/learning/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::educationLearningAction',  '_route' => 'biz_bids_education_learning_ajaxaction',);
        }

        // biz_bids_moving_storage_ajaxaction
        if ($pathinfo === '/moving/storage/ajaxformaction') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\CategoriesAjaxController::movingStorageAction',  '_route' => 'biz_bids_moving_storage_ajaxaction',);
        }

        // b_bids_b_bids_invoice_reciept_pdf
        if (0 === strpos($pathinfo, '/invoice/reciept/pdf') && preg_match('#^/invoice/reciept/pdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_invoice_reciept_pdf')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::printOrderAction',));
        }

        // b_bids_b_bids_checkvendor_leads_available
        if ($pathinfo === '/vendor/leads/available') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::checkLeadAvailableAction',  '_route' => 'b_bids_b_bids_checkvendor_leads_available',);
        }

        // b_bids_b_bids_form_extra_fields
        if (0 === strpos($pathinfo, '/get/form/extrafields') && preg_match('#^/get/form/extrafields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_form_extra_fields')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::getExtraFormFieldsAction',));
        }

        // b_bids_b_bids_vendor_leads_list
        if ($pathinfo === '/vendor/leads/list') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorLeadsListAction',  '_route' => 'b_bids_b_bids_vendor_leads_list',);
        }

        // b_bids_b_bids_exportall_leadhistory
        if ($pathinfo === '/export/all/leadhistory') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::exportAllLeadsListAction',  '_route' => 'b_bids_b_bids_exportall_leadhistory',);
        }

        if (0 === strpos($pathinfo, '/vendor')) {
            // b_bids_b_bids_vendor_category_mapping
            if ($pathinfo === '/vendor/category/mapping') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorCatgoryMappingAction',  '_route' => 'b_bids_b_bids_vendor_category_mapping',);
            }

            // b_bids_b_bids_deactivate_category_mapping
            if (0 === strpos($pathinfo, '/vendor/toggle/mapping') && preg_match('#^/vendor/toggle/mapping/(?P<vid>[^/]++)/(?P<cid>[^/]++)/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'b_bids_b_bids_deactivate_category_mapping')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::toggleCategoryMappingAction',));
            }

        }

        if (0 === strpos($pathinfo, '/bbids-')) {
            // bizbids_are_you_a_vendor
            if ($pathinfo === '/bbids-leads') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::areyouaVendorAction',  '_route' => 'bizbids_are_you_a_vendor',);
            }

            // bizbids_template2
            if ($pathinfo === '/bbids-work') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::bbidsWorkAction',  '_route' => 'bizbids_template2',);
            }

        }

        // bizbids_template3
        if ($pathinfo === '/get-hired-on-bbids') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::getHiredAction',  '_route' => 'bizbids_template3',);
        }

        // bizbids_template4
        if ($pathinfo === '/why-join-bbids') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::whyJoinBbidsAction',  '_route' => 'bizbids_template4',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // bizbids_template5
            if ($pathinfo === '/are-you-a-vendor') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::template5Action',  '_route' => 'bizbids_template5',);
            }

            // bizbids_addcat
            if ($pathinfo === '/account/categorysuccess') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::categorysuccessAction',  '_route' => 'bizbids_addcat',);
            }

        }

        // bizbids_addcat_port
        if ($pathinfo === '/profile/categorysuccess') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::portcategorysuccessAction',  '_route' => 'bizbids_addcat_port',);
        }

        // bizbids_deletecat
        if (0 === strpos($pathinfo, '/account/categorydelete') && preg_match('#^/account/categorydelete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_deletecat')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::categorydeleteAction',));
        }

        // bizbids_editcat_port
        if ($pathinfo === '/profile/categoryedit') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::portcategoryeditAction',  '_route' => 'bizbids_editcat_port',);
        }

        // bizbids_editcat
        if ($pathinfo === '/account/categoryedit') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::categoryeditAction',  '_route' => 'bizbids_editcat',);
        }

        // bizbids_deletecat_port
        if (0 === strpos($pathinfo, '/profile/categorydelete') && preg_match('#^/profile/categorydelete/(?P<id>[^/]++)/(?P<vid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_deletecat_port')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\UserController::portcategorydeleteAction',));
        }

        if (0 === strpos($pathinfo, '/vendor')) {
            // bizbids_vendor_search
            if ($pathinfo === '/vendor/search/home') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vendorSearchHomeAction',  '_route' => 'bizbids_vendor_search',);
            }

            // bizbids_vendor_direct_enquiry
            if ($pathinfo === '/vendor/direct/enquiry') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::vendorDirectEnqAction',  '_route' => 'bizbids_vendor_direct_enquiry',);
            }

            if (0 === strpos($pathinfo, '/vendor/activation')) {
                // bizbids_vendor_on_off_service
                if ($pathinfo === '/vendor/activation/service') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vedorOnOffServiceAction',  '_route' => 'bizbids_vendor_on_off_service',);
                }

                // bizbids_vendor_on_off_ajax
                if ($pathinfo === '/vendor/activation/request') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendorOnOffAjaxAction',  '_route' => 'bizbids_vendor_on_off_ajax',);
                }

            }

            // bizbids_vendor_test_account_toggle
            if (0 === strpos($pathinfo, '/vendor/test/account/toggle') && preg_match('#^/vendor/test/account/toggle/(?P<vid>[^/]++)/(?P<status>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_vendor_test_account_toggle')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::toggleVendTestAccountAction',));
            }

            if (0 === strpos($pathinfo, '/vendor/reviews')) {
                // bizbids_vendor_reviews_upload
                if ($pathinfo === '/vendor/reviews/upload') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::vendReviewsUploadAction',  '_route' => 'bizbids_vendor_reviews_upload',);
                }

                // bizbids_reviews_ajax_call_upload
                if ($pathinfo === '/vendor/reviews/ajax') {
                    return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::reviewsUploadAjaxAction',  '_route' => 'bizbids_reviews_ajax_call_upload',);
                }

            }

            // bizbids_lead_dedcution_return_upload
            if ($pathinfo === '/vendorlead/deduction/return') {
                return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::manualLeadDeductionAction',  '_route' => 'bizbids_lead_dedcution_return_upload',);
            }

        }

        // bizbids_anonymous_reviews_upload
        if ($pathinfo === '/anonymous/reviews/upload') {
            return array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\AdminController::anonymousReviewsUploadAction',  '_route' => 'bizbids_anonymous_reviews_upload',);
        }

        // bizbids_job_post_sucessful
        if (0 === strpos($pathinfo, '/job/possted/successful') && preg_match('#^/job/possted/successful/(?P<eid>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bizbids_job_post_sucessful')), array (  '_controller' => 'BBids\\BBidsHomeBundle\\Controller\\HomeController::jobPostSucessfulAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

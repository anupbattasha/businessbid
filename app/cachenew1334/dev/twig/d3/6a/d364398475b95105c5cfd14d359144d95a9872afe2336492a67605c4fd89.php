<?php

/* ::base.html.twig */
class __TwigTemplate_d36ad364398475b95105c5cfd14d359144d95a9872afe2336492a67605c4fd89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'customcss' => array($this, 'block_customcss'),
            'javascripts' => array($this, 'block_javascripts'),
            'customjs' => array($this, 'block_customjs'),
            'jquery' => array($this, 'block_jquery'),
            'googleanalatics' => array($this, 'block_googleanalatics'),
            'maincontent' => array($this, 'block_maincontent'),
            'recentactivity' => array($this, 'block_recentactivity'),
            'requestsfeed' => array($this, 'block_requestsfeed'),
            'footer' => array($this, 'block_footer'),
            'footerScript' => array($this, 'block_footerScript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>

<html lang=\"en\">
<head>
";
        // line 5
        $context["aboutus"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#aboutus");
        // line 6
        $context["meetteam"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#meetteam");
        // line 7
        $context["career"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#career");
        // line 8
        $context["valuedpartner"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#valuedpartner");
        // line 9
        $context["vendor"] = ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#vendor");
        // line 10
        echo "
<title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        // line 111
        echo "</title>


<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 118
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/index.php\">
    ";
        }
        // line 120
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/index.php")) {
            // line 121
            echo "    <link rel=\"canonical\" href=\"http://www.businessbid.ae/\">
    ";
        }
        // line 123
        echo "    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 126
        $this->displayBlock('noindexMeta', $context, $blocks);
        // line 128
        echo "    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\">
    <!--[if lt IE 8]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
    <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
     <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/slicknav.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/new-style.css"), "html", null, true);
        echo "\">

    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/responsive.css"), "html", null, true);
        echo "\">
    <link href=\"http://fonts.googleapis.com/css?family=Amaranth:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/owl.carousel.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo ">
    <script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
    <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script>
\$(document).ready(function(){
    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
});
</script>
<script type=\"text/javascript\" src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

";
        // line 161
        $this->displayBlock('customcss', $context, $blocks);
        // line 164
        echo "
";
        // line 165
        $this->displayBlock('javascripts', $context, $blocks);
        // line 308
        echo "
";
        // line 309
        $this->displayBlock('customjs', $context, $blocks);
        // line 312
        echo "
";
        // line 313
        $this->displayBlock('jquery', $context, $blocks);
        // line 316
        $this->displayBlock('googleanalatics', $context, $blocks);
        // line 324
        echo "</head>
";
        // line 325
        ob_start();
        // line 326
        echo "<body>

";
        // line 328
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "uid"), "method");
        // line 329
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "pid"), "method");
        // line 330
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "email"), "method");
        // line 331
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "name"), "method");
        // line 332
        echo "

 <div class=\"main_header_area\">
            <div class=\"inner_headder_area\">
                <div class=\"main_logo_area\">
                    <a href=\"\"><img src=\"";
        // line 337
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"facebook_login_area\">
                    <div class=\"phone_top_area\">
                        <h3>(04) 42 13 777</h3>
                    </div>
                    ";
        // line 343
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 344
            echo "                    <div class=\"fb_login_top_area\">
                        <ul>
                            <li><a href=\"";
            // line 346
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/sign_in_btn.png"), "html", null, true);
            echo "\" alt=\"Sign In\"></a></li>
                            <li><img src=\"";
            // line 347
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/fb_sign_in.png"), "html", null, true);
            echo "\" alt=\"Facebook Sign In\" onclick=\"FBLogin();\"></li>
                        </ul>
                    </div>
                    ";
        } elseif ( !(null ===         // line 350
(isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 351
            echo "                    <div class=\"fb_login_top_area1\">
                        <ul>
                        <li class=\"profilename\">Welcome, <span class=\"profile-name\">";
            // line 353
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
            echo "</span></li>
                        <li><a class='button-login naked' href=\"";
            // line 354
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\"><span>Log out</span></a></li>
                          <li class=\"my-account\"><img src=\"";
            // line 355
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\"><a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
            </li>
                        </ul>
                    </div>
                    ";
        }
        // line 360
        echo "                </div>
            </div>
        </div>
        <div class=\"mainmenu_area\">
            <div class=\"inner_main_menu_area\">
                <div class=\"original_menu\">
                    <ul id=\"nav\">
                        <li><a href=\"";
        // line 367
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 368
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">Find Services Professionals</a>
                            <ul>
                                ";
        // line 370
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 373
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search reviews</a>
                            <ul>
                                ";
        // line 375
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
                            </ul>
                        </li>
                        <li><a href=\"http://www.businessbid.ae/resource-centre/home/\">resource centre</a>
                            <ul>
                                ";
        // line 380
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorResource")));
        echo "
                            </ul>
                        </li>
                    </ul>
                </div>
                ";
        // line 385
        if (twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 386
            echo "                <div class=\"register_menu\">
                    <div class=\"register_menu_btn\">
                        <a href=\"";
            // line 388
            echo $this->env->getExtension('routing')->getPath("bizbids_template5");
            echo "\">Register Your Business</a>
                    </div>
                </div>
                ";
        } elseif ( !(null ===         // line 391
(isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 392
            echo "                <div class=\"register_menu1\">
                  <img src=\"";
            // line 393
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/admin-icon1.png"), "html", null, true);
            echo "\" alt=\"My Account\">  <a href=\"";
            if (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 1)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_admin_dashboard");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 2)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
            } elseif (((isset($context["pid"]) ? $context["pid"] : $this->getContext($context, "pid")) == 3)) {
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            }
            echo "\"><span>My Account</span></a>
                </div>
                ";
        }
        // line 396
        echo "            </div>
        </div>
        <div class=\"main-container-block\">
    <!-- Banner block Ends -->


    <!-- content block Starts -->
    ";
        // line 403
        $this->displayBlock('maincontent', $context, $blocks);
        // line 634
        echo "    <!-- content block Ends -->

    <!-- footer block Starts -->
    ";
        // line 637
        $this->displayBlock('footer', $context, $blocks);
        // line 750
        echo "    <!-- footer block ends -->
</div>";
        // line 753
        $this->displayBlock('footerScript', $context, $blocks);
        // line 757
        echo "

<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 764
        echo "</html>
";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/")) {
            // line 13
            echo "    Hire a Quality Service Professional at a Fair Price
";
        } elseif ((is_string($__internal_4542c247992097a5e48f70cb5765eab1b8fbd19bbeb73e4c10e0d20f5812446c = $this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_5c8d3ccef5fe61ffe5be12205500458107c89ddd74fd4d4f008c246a29293b2d = "http://www.businessbid.ae/contactus") && ('' === $__internal_5c8d3ccef5fe61ffe5be12205500458107c89ddd74fd4d4f008c246a29293b2d || 0 === strpos($__internal_4542c247992097a5e48f70cb5765eab1b8fbd19bbeb73e4c10e0d20f5812446c, $__internal_5c8d3ccef5fe61ffe5be12205500458107c89ddd74fd4d4f008c246a29293b2d)))) {
            // line 15
            echo "    Contact Us Form
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 16
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) === "http://www.businessbid.ae/login")) {
            // line 17
            echo "    Account Login
";
        } elseif ((is_string($__internal_96096254f09d5c407010188ab76aa5ac1312bcc267e7536a5467ee71151efa5e = $this->getAttribute($this->getAttribute(        // line 18
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_1af4f914e316aed58a169210fae48d36ffab8eed56e9ffd65a97c0c3915786b7 = "http://www.businessbid.ae/node3") && ('' === $__internal_1af4f914e316aed58a169210fae48d36ffab8eed56e9ffd65a97c0c3915786b7 || 0 === strpos($__internal_96096254f09d5c407010188ab76aa5ac1312bcc267e7536a5467ee71151efa5e, $__internal_1af4f914e316aed58a169210fae48d36ffab8eed56e9ffd65a97c0c3915786b7)))) {
            // line 19
            echo "    How BusinessBid Works for Customers
";
        } elseif ((is_string($__internal_222ec4644d7e416fc9957835ee62ea7c9a33311e02095e593c38eb480b594624 = $this->getAttribute($this->getAttribute(        // line 20
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_cf84fede5c13f7eada20ae068d6fe4584a9c51490e8075db72a47c96faeabe83 = "http://www.businessbid.ae/review/login") && ('' === $__internal_cf84fede5c13f7eada20ae068d6fe4584a9c51490e8075db72a47c96faeabe83 || 0 === strpos($__internal_222ec4644d7e416fc9957835ee62ea7c9a33311e02095e593c38eb480b594624, $__internal_cf84fede5c13f7eada20ae068d6fe4584a9c51490e8075db72a47c96faeabe83)))) {
            // line 21
            echo "    Write A Review Login
";
        } elseif ((is_string($__internal_67892035e9f1fb2a8c1f02a9305f4075d46fc118d7149f5f3c145eed5a27ab8b = $this->getAttribute($this->getAttribute(        // line 22
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ea89dbb391320c13efd5c293e0ceaf70bbbb8b04a5c4b1b5bdabe993f76a43b6 = "http://www.businessbid.ae/node1") && ('' === $__internal_ea89dbb391320c13efd5c293e0ceaf70bbbb8b04a5c4b1b5bdabe993f76a43b6 || 0 === strpos($__internal_67892035e9f1fb2a8c1f02a9305f4075d46fc118d7149f5f3c145eed5a27ab8b, $__internal_ea89dbb391320c13efd5c293e0ceaf70bbbb8b04a5c4b1b5bdabe993f76a43b6)))) {
            // line 23
            echo "    Are you A Vendor
";
        } elseif ((is_string($__internal_b287597747ac366b35da6502921bb54e13f65321f7fd3957fce30c906b24330e = $this->getAttribute($this->getAttribute(        // line 24
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_3afaedafd699b4222ed80a37c3517c7b32fd313fc03889b9c438ed1dbaee0648 = "http://www.businessbid.ae/post/quote/bysearch/inline/14?city=") && ('' === $__internal_3afaedafd699b4222ed80a37c3517c7b32fd313fc03889b9c438ed1dbaee0648 || 0 === strpos($__internal_b287597747ac366b35da6502921bb54e13f65321f7fd3957fce30c906b24330e, $__internal_3afaedafd699b4222ed80a37c3517c7b32fd313fc03889b9c438ed1dbaee0648)))) {
            // line 25
            echo "    Accounting & Auditing Quote Form
";
        } elseif ((is_string($__internal_da8342cde44d46e68c3e1c835dd48838505b9d426132334e938dee0f5960fa10 = $this->getAttribute($this->getAttribute(        // line 26
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_cbba75524e0725e54406fdcf1ac943bd237b9fb119e24e57b15bbb0704d457f0 = "http://www.businessbid.ae/post/quote/bysearch/inline/924?city=") && ('' === $__internal_cbba75524e0725e54406fdcf1ac943bd237b9fb119e24e57b15bbb0704d457f0 || 0 === strpos($__internal_da8342cde44d46e68c3e1c835dd48838505b9d426132334e938dee0f5960fa10, $__internal_cbba75524e0725e54406fdcf1ac943bd237b9fb119e24e57b15bbb0704d457f0)))) {
            // line 27
            echo "    Get Signage & Signboard Quotes
";
        } elseif ((is_string($__internal_8f5b58224e56cfea659007ee3d801ceba5f41485f847fb5c23f9c80bf1e9a46e = $this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_afa8292e8290bcdd1115f8e021af8553b7813dda00ed52afc37f148bc240d94b = "http://www.businessbid.ae/post/quote/bysearch/inline/89?city=") && ('' === $__internal_afa8292e8290bcdd1115f8e021af8553b7813dda00ed52afc37f148bc240d94b || 0 === strpos($__internal_8f5b58224e56cfea659007ee3d801ceba5f41485f847fb5c23f9c80bf1e9a46e, $__internal_afa8292e8290bcdd1115f8e021af8553b7813dda00ed52afc37f148bc240d94b)))) {
            // line 29
            echo "    Obtain IT Support Quotes
";
        } elseif ((is_string($__internal_862c2685eb65cc54753c5d4ef849940181a33f7ac401862be38ce43efd7a2ec3 = $this->getAttribute($this->getAttribute(        // line 30
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ba04b0ac553bb7560ce91440f34820f5c5eac07403bf20516ab676ca41f2b0e2 = "http://www.businessbid.ae/post/quote/bysearch/inline/114?city=") && ('' === $__internal_ba04b0ac553bb7560ce91440f34820f5c5eac07403bf20516ab676ca41f2b0e2 || 0 === strpos($__internal_862c2685eb65cc54753c5d4ef849940181a33f7ac401862be38ce43efd7a2ec3, $__internal_ba04b0ac553bb7560ce91440f34820f5c5eac07403bf20516ab676ca41f2b0e2)))) {
            // line 31
            echo "    Easy Way to get Photography & Videos Quotes
";
        } elseif ((is_string($__internal_f944876ea38a2c92c0bc4783d18d489c0278fcf6e7b8d82549b7c53f0fbeec16 = $this->getAttribute($this->getAttribute(        // line 32
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_d8e4e8551a05cb22a075fc7c927bd6a98f1167938642e7dcd16c7436854ddbba = "http://www.businessbid.ae/post/quote/bysearch/inline/996?city=") && ('' === $__internal_d8e4e8551a05cb22a075fc7c927bd6a98f1167938642e7dcd16c7436854ddbba || 0 === strpos($__internal_f944876ea38a2c92c0bc4783d18d489c0278fcf6e7b8d82549b7c53f0fbeec16, $__internal_d8e4e8551a05cb22a075fc7c927bd6a98f1167938642e7dcd16c7436854ddbba)))) {
            // line 33
            echo "    Procure Residential Cleaning Quotes Right Here
";
        } elseif ((is_string($__internal_b2e0562c2eef23826937bcb286ac7d5ba34ecddde7ae3dfce9aa1622ac5c2856 = $this->getAttribute($this->getAttribute(        // line 34
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_0c5bf3e46a4dd5fa24b91e04cacbacebb244058d4acb874f4934606ce43b168f = "http://www.businessbid.ae/post/quote/bysearch/inline/994?city=") && ('' === $__internal_0c5bf3e46a4dd5fa24b91e04cacbacebb244058d4acb874f4934606ce43b168f || 0 === strpos($__internal_b2e0562c2eef23826937bcb286ac7d5ba34ecddde7ae3dfce9aa1622ac5c2856, $__internal_0c5bf3e46a4dd5fa24b91e04cacbacebb244058d4acb874f4934606ce43b168f)))) {
            // line 35
            echo "    Receive Maintenance Quotes for Free
";
        } elseif ((is_string($__internal_edf0cd2a20053c3d45aa1176810aa74ba087030f35ac702ea9b5b8b40da80a49 = $this->getAttribute($this->getAttribute(        // line 36
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_37c3cd9f7e144cf186410628cd3969179b249939eb3e5f25f6c8be1efab70a92 = "http://www.businessbid.ae/post/quote/bysearch/inline/87?city=") && ('' === $__internal_37c3cd9f7e144cf186410628cd3969179b249939eb3e5f25f6c8be1efab70a92 || 0 === strpos($__internal_edf0cd2a20053c3d45aa1176810aa74ba087030f35ac702ea9b5b8b40da80a49, $__internal_37c3cd9f7e144cf186410628cd3969179b249939eb3e5f25f6c8be1efab70a92)))) {
            // line 37
            echo "    Acquire Interior Design & FitOut Quotes
";
        } elseif ((is_string($__internal_64815a5b64827e43a2f7c776f7905ca8252ed3d48e32bae0164414311ee9628d = $this->getAttribute($this->getAttribute(        // line 38
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_a302b97194250d41b362794fedba4def008d69460ce54b73314207fa0bcee91a = "http://www.businessbid.ae/post/quote/bysearch/inline/45?city=") && ('' === $__internal_a302b97194250d41b362794fedba4def008d69460ce54b73314207fa0bcee91a || 0 === strpos($__internal_64815a5b64827e43a2f7c776f7905ca8252ed3d48e32bae0164414311ee9628d, $__internal_a302b97194250d41b362794fedba4def008d69460ce54b73314207fa0bcee91a)))) {
            // line 39
            echo "    Get Hold of Free Catering Quotes
";
        } elseif ((is_string($__internal_5ae55beb8f8a09d578b556542826448c577cf66851232812ce13a1749c688099 = $this->getAttribute($this->getAttribute(        // line 40
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_61e1594d4b0bb61346a4e121fc75b7ad82f67f539e98f474bc6266ee612c483f = "http://www.businessbid.ae/post/quote/bysearch/inline/93?city=") && ('' === $__internal_61e1594d4b0bb61346a4e121fc75b7ad82f67f539e98f474bc6266ee612c483f || 0 === strpos($__internal_5ae55beb8f8a09d578b556542826448c577cf66851232812ce13a1749c688099, $__internal_61e1594d4b0bb61346a4e121fc75b7ad82f67f539e98f474bc6266ee612c483f)))) {
            // line 41
            echo "    Find Free Landscaping & Gardens Quotes
";
        } elseif ((is_string($__internal_d2a54539a6630ef4af70bdde4df676ec2fab766f1ee004a96202f9a111dbc04b = $this->getAttribute($this->getAttribute(        // line 42
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_132992cb1897c655bfa09b632cdbb3a0e4808669270fa1a95e1bce7a0a697dc3 = "http://www.businessbid.ae/post/quote/bysearch/inline/79?city=") && ('' === $__internal_132992cb1897c655bfa09b632cdbb3a0e4808669270fa1a95e1bce7a0a697dc3 || 0 === strpos($__internal_d2a54539a6630ef4af70bdde4df676ec2fab766f1ee004a96202f9a111dbc04b, $__internal_132992cb1897c655bfa09b632cdbb3a0e4808669270fa1a95e1bce7a0a697dc3)))) {
            // line 43
            echo "    Attain Free Offset Printing Quotes from Companies
";
        } elseif ((is_string($__internal_545ffd1031ebed934b04d39805eb121edb80b450f18c5634c143d97d916bb1d1 = $this->getAttribute($this->getAttribute(        // line 44
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_65c0f834a06368423d63948e1035d01e3e61a714458ad783776634657a3ee574 = "http://www.businessbid.ae/post/quote/bysearch/inline/47?city=") && ('' === $__internal_65c0f834a06368423d63948e1035d01e3e61a714458ad783776634657a3ee574 || 0 === strpos($__internal_545ffd1031ebed934b04d39805eb121edb80b450f18c5634c143d97d916bb1d1, $__internal_65c0f834a06368423d63948e1035d01e3e61a714458ad783776634657a3ee574)))) {
            // line 45
            echo "    Get Free Commercial Cleaning Quotes Now
";
        } elseif ((is_string($__internal_1b17976da4529c24e6346988cf8d25714fb15cc3af0571c8afefdb892803f296 = $this->getAttribute($this->getAttribute(        // line 46
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_1ca90b99e2ff7b240ff3f7c5c810ad4266c34a7553f2e79eb18a5f44a1efbfcd = "http://www.businessbid.ae/post/quote/bysearch/inline/997?city=") && ('' === $__internal_1ca90b99e2ff7b240ff3f7c5c810ad4266c34a7553f2e79eb18a5f44a1efbfcd || 0 === strpos($__internal_1b17976da4529c24e6346988cf8d25714fb15cc3af0571c8afefdb892803f296, $__internal_1ca90b99e2ff7b240ff3f7c5c810ad4266c34a7553f2e79eb18a5f44a1efbfcd)))) {
            // line 47
            echo "    Procure Free Pest Control Quotes Easily
";
        } elseif (($this->getAttribute($this->getAttribute(        // line 48
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) == "http://www.businessbid.ae/web/app.php/node4")) {
            // line 49
            echo "    Vendor Reviews Directory Home Page
";
        } elseif ((is_string($__internal_3ad6b5657d31c921e28f3e4cdefb81517f3f1c5b2d3f70008dd874000c11589e = $this->getAttribute($this->getAttribute(        // line 50
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_b52b1009cece420c36064b0a71e0d2121873950b3ca07201458989ce8dbac9c4 = "http://www.businessbid.ae/node4?category=Accounting%20%2526%20Auditing") && ('' === $__internal_b52b1009cece420c36064b0a71e0d2121873950b3ca07201458989ce8dbac9c4 || 0 === strpos($__internal_3ad6b5657d31c921e28f3e4cdefb81517f3f1c5b2d3f70008dd874000c11589e, $__internal_b52b1009cece420c36064b0a71e0d2121873950b3ca07201458989ce8dbac9c4)))) {
            // line 51
            echo "    Accounting & Auditing Reviews Directory Page
";
        } elseif ((is_string($__internal_95ca24ca860d1825ed07f1c9b89fda6aee1196a43512a46ce5f4d270bbcc2a72 = $this->getAttribute($this->getAttribute(        // line 52
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_4f420732c1e264cf18a4f089a7703488e6e5246c28a6b0d8a42e31b9b3c743e3 = "http://www.businessbid.ae/node4?category=Signage%20%2526%20Signboards") && ('' === $__internal_4f420732c1e264cf18a4f089a7703488e6e5246c28a6b0d8a42e31b9b3c743e3 || 0 === strpos($__internal_95ca24ca860d1825ed07f1c9b89fda6aee1196a43512a46ce5f4d270bbcc2a72, $__internal_4f420732c1e264cf18a4f089a7703488e6e5246c28a6b0d8a42e31b9b3c743e3)))) {
            // line 53
            echo "    Find Signage & Signboard Reviews
";
        } elseif ((is_string($__internal_1a5c8c0ac06ce5d93030b5ee4ebc1db3d5226f64f1ff20bfef8df65ea4033dbb = $this->getAttribute($this->getAttribute(        // line 54
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_150a74843505e139ad734b53a4b9e304fd2e3ab15bb28b03df8578f4441276e9 = "http://www.businessbid.ae/node4?category=IT%20Support") && ('' === $__internal_150a74843505e139ad734b53a4b9e304fd2e3ab15bb28b03df8578f4441276e9 || 0 === strpos($__internal_1a5c8c0ac06ce5d93030b5ee4ebc1db3d5226f64f1ff20bfef8df65ea4033dbb, $__internal_150a74843505e139ad734b53a4b9e304fd2e3ab15bb28b03df8578f4441276e9)))) {
            // line 55
            echo "    Have a Look at IT Support Reviews Directory
";
        } elseif ((is_string($__internal_5feead5857c31333ae5ab9220a3c9e9d8b7e425651993298d02d7d5b036a483a = $this->getAttribute($this->getAttribute(        // line 56
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_8383fb67c35d8e9ad2209e3378f98c86e2f3b226ea3f773be32a24af927b899a = "http://www.businessbid.ae/node4?category=Photography%20and%20Videography") && ('' === $__internal_8383fb67c35d8e9ad2209e3378f98c86e2f3b226ea3f773be32a24af927b899a || 0 === strpos($__internal_5feead5857c31333ae5ab9220a3c9e9d8b7e425651993298d02d7d5b036a483a, $__internal_8383fb67c35d8e9ad2209e3378f98c86e2f3b226ea3f773be32a24af927b899a)))) {
            // line 57
            echo "    Check Out Photography & Videos Reviews Here
";
        } elseif ((is_string($__internal_a3f88351ab34663157871aac71d8b7538010c034984924bdc9240c3f673c5469 = $this->getAttribute($this->getAttribute(        // line 58
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_5bb29cea62c143b5aef0b6e548e442eb1f78436158f741dc93706ee8e58c2a15 = "http://www.businessbid.ae/node4?category=Residential%20Cleaning") && ('' === $__internal_5bb29cea62c143b5aef0b6e548e442eb1f78436158f741dc93706ee8e58c2a15 || 0 === strpos($__internal_a3f88351ab34663157871aac71d8b7538010c034984924bdc9240c3f673c5469, $__internal_5bb29cea62c143b5aef0b6e548e442eb1f78436158f741dc93706ee8e58c2a15)))) {
            // line 59
            echo "    View Residential Cleaning Reviews From Here
";
        } elseif ((is_string($__internal_78ef2dda43a6c6644e83f5192dabebdc4b5f2a6324df20b4b5742571d0056726 = $this->getAttribute($this->getAttribute(        // line 60
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_10893236d10b16c8206e50ca84fb2bbac69d0701f3b11c7eae455fee1162b7d2 = "http://www.businessbid.ae/node4?category=Maintenance") && ('' === $__internal_10893236d10b16c8206e50ca84fb2bbac69d0701f3b11c7eae455fee1162b7d2 || 0 === strpos($__internal_78ef2dda43a6c6644e83f5192dabebdc4b5f2a6324df20b4b5742571d0056726, $__internal_10893236d10b16c8206e50ca84fb2bbac69d0701f3b11c7eae455fee1162b7d2)))) {
            // line 61
            echo "    Find Genuine Maintenance Reviews
";
        } elseif ((is_string($__internal_e02bb6c638a056792150423e69fead8544c93e03cb58491197d45ff6379baab0 = $this->getAttribute($this->getAttribute(        // line 62
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_f00036656961b08973e70bf7152865f0e9b1e20794c6e6ded2d415686496a77e = "http://www.businessbid.ae/node4?category=Interior%20Design%20%2526%20Fit%20Out") && ('' === $__internal_f00036656961b08973e70bf7152865f0e9b1e20794c6e6ded2d415686496a77e || 0 === strpos($__internal_e02bb6c638a056792150423e69fead8544c93e03cb58491197d45ff6379baab0, $__internal_f00036656961b08973e70bf7152865f0e9b1e20794c6e6ded2d415686496a77e)))) {
            // line 63
            echo "    Locate Interior Design & FitOut Reviews
";
        } elseif ((is_string($__internal_83d1e765dc6a8b02528e0eead222806e74920be43f6feb1e6f3af210e9224cde = $this->getAttribute($this->getAttribute(        // line 64
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_cb2bf86867740ea177ae686f36d8a4e6ceb7c8319e83b94d8893615e6da72a52 = "http://www.businessbid.ae/node4?category=Catering") && ('' === $__internal_cb2bf86867740ea177ae686f36d8a4e6ceb7c8319e83b94d8893615e6da72a52 || 0 === strpos($__internal_83d1e765dc6a8b02528e0eead222806e74920be43f6feb1e6f3af210e9224cde, $__internal_cb2bf86867740ea177ae686f36d8a4e6ceb7c8319e83b94d8893615e6da72a52)))) {
            // line 65
            echo "    See All Catering Reviews Logged
";
        } elseif ((is_string($__internal_dc3c9024401e9d2e8edd217309d6fcb605c052b348535fd3ecf7358536aace60 = $this->getAttribute($this->getAttribute(        // line 66
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2fcde76d4de0b5c11296b00f3ff9c3b5266eaea1679d4ac7fef242ec86a8b93d = "http://www.businessbid.ae/node4?category=Landscaping%20and%20Gardening") && ('' === $__internal_2fcde76d4de0b5c11296b00f3ff9c3b5266eaea1679d4ac7fef242ec86a8b93d || 0 === strpos($__internal_dc3c9024401e9d2e8edd217309d6fcb605c052b348535fd3ecf7358536aace60, $__internal_2fcde76d4de0b5c11296b00f3ff9c3b5266eaea1679d4ac7fef242ec86a8b93d)))) {
            // line 67
            echo "    Listings of all Landscaping & Gardens Reviews
";
        } elseif ((is_string($__internal_a2488cefde1f78b0a97dbe480b96b4942ae9cc0638bcac8d47bf65e02a95ab71 = $this->getAttribute($this->getAttribute(        // line 68
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_6b30ef2ed85c77cc7414e3dbb8395b294d319ab3776162df023be7167d02004c = "http://www.businessbid.ae/node4?category=Offset%20Printing") && ('' === $__internal_6b30ef2ed85c77cc7414e3dbb8395b294d319ab3776162df023be7167d02004c || 0 === strpos($__internal_a2488cefde1f78b0a97dbe480b96b4942ae9cc0638bcac8d47bf65e02a95ab71, $__internal_6b30ef2ed85c77cc7414e3dbb8395b294d319ab3776162df023be7167d02004c)))) {
            // line 69
            echo "    Get Access to Offset Printing Reviews
";
        } elseif ((is_string($__internal_394ffed76e9840ce70e97afff007ad108f463965a57fe503d08db2ac9b5def18 = $this->getAttribute($this->getAttribute(        // line 70
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2716bb4d416dba9d95b73de5d007c626632289be05febbe6a2e7ee43d71627e6 = "http://www.businessbid.ae/node4?category=Commercial%20Cleaning") && ('' === $__internal_2716bb4d416dba9d95b73de5d007c626632289be05febbe6a2e7ee43d71627e6 || 0 === strpos($__internal_394ffed76e9840ce70e97afff007ad108f463965a57fe503d08db2ac9b5def18, $__internal_2716bb4d416dba9d95b73de5d007c626632289be05febbe6a2e7ee43d71627e6)))) {
            // line 71
            echo "    Have a Look at various Commercial Cleaning Reviews
";
        } elseif ((is_string($__internal_5e42f57c9ecd0ce080501d94e32d3dd86d0b4ec668cb71b0220f0a1361532948 = $this->getAttribute($this->getAttribute(        // line 72
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_d1ef8e60833841cbe0b6af1412ddfea71cd35becf74dc9a780ac36174cb723b5 = "http://www.businessbid.ae/node4?category=Pest%20Control%20and%20Inspection") && ('' === $__internal_d1ef8e60833841cbe0b6af1412ddfea71cd35becf74dc9a780ac36174cb723b5 || 0 === strpos($__internal_5e42f57c9ecd0ce080501d94e32d3dd86d0b4ec668cb71b0220f0a1361532948, $__internal_d1ef8e60833841cbe0b6af1412ddfea71cd35becf74dc9a780ac36174cb723b5)))) {
            // line 73
            echo "    Read the Pest Control Reviews Listed Here
";
        } elseif ((        // line 74
(isset($context["aboutus"]) ? $context["aboutus"] : $this->getContext($context, "aboutus")) == "http://www.businessbid.ae/aboutbusinessbid#aboutus")) {
            // line 75
            echo "    Find information About BusinessBid
";
        } elseif ((        // line 76
(isset($context["meetteam"]) ? $context["meetteam"] : $this->getContext($context, "meetteam")) == "http://www.businessbid.ae/aboutbusinessbid#meetteam")) {
            // line 77
            echo "    Let us Introduce the BusinessBid Team
";
        } elseif ((        // line 78
(isset($context["career"]) ? $context["career"] : $this->getContext($context, "career")) == "http://www.businessbid.ae/aboutbusinessbid#career")) {
            // line 79
            echo "    Have a Look at BusinessBid Career Opportunities
";
        } elseif ((        // line 80
(isset($context["valuedpartner"]) ? $context["valuedpartner"] : $this->getContext($context, "valuedpartner")) == "http://www.businessbid.ae/aboutbusinessbid#valuedpartner")) {
            // line 81
            echo "    Want to become BusinessBid Valued Partners
";
        } elseif ((        // line 82
(isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")) == "http://www.businessbid.ae/login#vendor")) {
            // line 83
            echo "    Vendor Account Login Access Page
";
        } elseif ((is_string($__internal_4c38783ca50eea0945c4b00001cabfdf993789c66dc9eade00b87946a74c131a = $this->getAttribute($this->getAttribute(        // line 84
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_187694a3c4af488c7dc82bf4f95136b02cf4a2044ca7a6615a3019496dd2f20b = "http://www.businessbid.ae/node2") && ('' === $__internal_187694a3c4af488c7dc82bf4f95136b02cf4a2044ca7a6615a3019496dd2f20b || 0 === strpos($__internal_4c38783ca50eea0945c4b00001cabfdf993789c66dc9eade00b87946a74c131a, $__internal_187694a3c4af488c7dc82bf4f95136b02cf4a2044ca7a6615a3019496dd2f20b)))) {
            // line 85
            echo "    How BusinessBid Works for Vendors
";
        } elseif ((is_string($__internal_64cd56dabfc605957735087a4e71d15fbe2fd9988d57eebb0c580203f5de0402 = $this->getAttribute($this->getAttribute(        // line 86
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ab440dd566c4710d9f08f18b84eaede76d5383b2b53d03484c81088c46fd075d = "http://www.businessbid.ae/vendor/register") && ('' === $__internal_ab440dd566c4710d9f08f18b84eaede76d5383b2b53d03484c81088c46fd075d || 0 === strpos($__internal_64cd56dabfc605957735087a4e71d15fbe2fd9988d57eebb0c580203f5de0402, $__internal_ab440dd566c4710d9f08f18b84eaede76d5383b2b53d03484c81088c46fd075d)))) {
            // line 87
            echo "    BusinessBid Vendor Registration Page
";
        } elseif ((is_string($__internal_114432f06f59730a70d7d0c014c9b53cced976eaf6bd15ade63222788a7b5719 = $this->getAttribute($this->getAttribute(        // line 88
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ff94190ca3914fd87ccd32049b5564ea2c7cbf0a297ef54c63182c6ddae7b1c4 = "http://www.businessbid.ae/faq") && ('' === $__internal_ff94190ca3914fd87ccd32049b5564ea2c7cbf0a297ef54c63182c6ddae7b1c4 || 0 === strpos($__internal_114432f06f59730a70d7d0c014c9b53cced976eaf6bd15ade63222788a7b5719, $__internal_ff94190ca3914fd87ccd32049b5564ea2c7cbf0a297ef54c63182c6ddae7b1c4)))) {
            // line 89
            echo "    Find answers to common Vendor FAQ’s
";
        } elseif ((is_string($__internal_32c9fa6edd699c73f18ebcb34496207ff35c0073ec3da127728103b4bb15ac08 = $this->getAttribute($this->getAttribute(        // line 90
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_8827acfb338aca7a4b704fa66d7b95dc0b03641584c1219273205d08a450088e = "http://www.businessbid.ae/node4") && ('' === $__internal_8827acfb338aca7a4b704fa66d7b95dc0b03641584c1219273205d08a450088e || 0 === strpos($__internal_32c9fa6edd699c73f18ebcb34496207ff35c0073ec3da127728103b4bb15ac08, $__internal_8827acfb338aca7a4b704fa66d7b95dc0b03641584c1219273205d08a450088e)))) {
            // line 91
            echo "    BusinessBid Vendor Reviews Directory Home
";
        } elseif ((is_string($__internal_2189a2d2510117f9c47ac8b7673f8b38a529aa4bc8bfee1f1b4abfed6748fd0e = ($this->getAttribute($this->getAttribute(        // line 92
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array()) . "#consumer")) && is_string($__internal_1736fc8c1f7c82c9b2c7b9c59a7542556f977a309ed70150cd7884a787fe1e62 = "http://www.businessbid.ae/login#consumer") && ('' === $__internal_1736fc8c1f7c82c9b2c7b9c59a7542556f977a309ed70150cd7884a787fe1e62 || 0 === strpos($__internal_2189a2d2510117f9c47ac8b7673f8b38a529aa4bc8bfee1f1b4abfed6748fd0e, $__internal_1736fc8c1f7c82c9b2c7b9c59a7542556f977a309ed70150cd7884a787fe1e62)))) {
            // line 93
            echo "    Customer Account Login and Dashboard Access
";
        } elseif ((is_string($__internal_d10623c23c50d0c21084fb870ab8dcd1f060ca827d2f464529737f3d0ca39b79 = $this->getAttribute($this->getAttribute(        // line 94
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_12e0865bfbd15b4a7ec3f43a986a5a03f08237b88e642ab6a66937dbeaf92994 = "http://www.businessbid.ae/node6") && ('' === $__internal_12e0865bfbd15b4a7ec3f43a986a5a03f08237b88e642ab6a66937dbeaf92994 || 0 === strpos($__internal_d10623c23c50d0c21084fb870ab8dcd1f060ca827d2f464529737f3d0ca39b79, $__internal_12e0865bfbd15b4a7ec3f43a986a5a03f08237b88e642ab6a66937dbeaf92994)))) {
            // line 95
            echo "    Find helpful answers to common Customer FAQ’s
";
        } elseif ((is_string($__internal_f4b7f87333cb2325f5f8e629e8ab5db8b121e4ad1e06c3be3ff5622d8dafe5f3 = $this->getAttribute($this->getAttribute(        // line 96
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_c820440950daac1893797756eb90ec2f9bb91b4e1fb19f130783ea6682567730 = "http://www.businessbid.ae/feedback") && ('' === $__internal_c820440950daac1893797756eb90ec2f9bb91b4e1fb19f130783ea6682567730 || 0 === strpos($__internal_f4b7f87333cb2325f5f8e629e8ab5db8b121e4ad1e06c3be3ff5622d8dafe5f3, $__internal_c820440950daac1893797756eb90ec2f9bb91b4e1fb19f130783ea6682567730)))) {
            // line 97
            echo "    Give us Your Feedback
";
        } elseif ((is_string($__internal_7f2a55ba7fde19858cd8c88a83f73a3c41725b74d00598111d4ac85a6f0156ab = $this->getAttribute($this->getAttribute(        // line 98
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_0ce1648e41cf618505a39d9d60beb7ac490e39d6542fd503f8c8c7cb589d3e4c = "http://www.businessbid.ae/sitemap") && ('' === $__internal_0ce1648e41cf618505a39d9d60beb7ac490e39d6542fd503f8c8c7cb589d3e4c || 0 === strpos($__internal_7f2a55ba7fde19858cd8c88a83f73a3c41725b74d00598111d4ac85a6f0156ab, $__internal_0ce1648e41cf618505a39d9d60beb7ac490e39d6542fd503f8c8c7cb589d3e4c)))) {
            // line 99
            echo "    Site Map Page
";
        } elseif ((is_string($__internal_e9b5e55ff047628ba3d600e40b438793494721dbe81ea367bac37e73977a4f45 = $this->getAttribute($this->getAttribute(        // line 100
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_e90374e6a46211c9d3f56d2349be574479010ff1a00aba27e38baef29b86bf16 = "http://www.businessbid.ae/forgotpassword") && ('' === $__internal_e90374e6a46211c9d3f56d2349be574479010ff1a00aba27e38baef29b86bf16 || 0 === strpos($__internal_e9b5e55ff047628ba3d600e40b438793494721dbe81ea367bac37e73977a4f45, $__internal_e90374e6a46211c9d3f56d2349be574479010ff1a00aba27e38baef29b86bf16)))) {
            // line 101
            echo "    Did you Forget Forgot Your Password
";
        } elseif ((is_string($__internal_8dba37c67f0419f717579accf89759f867963fcec4f7f5db4522312dd5611a5f = $this->getAttribute($this->getAttribute(        // line 102
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_2b0288bef81d800c62df7611209147e18d2bc70a248b6884bf402176079b8e15 = "http://www.businessbid.ae/user/email/verify/") && ('' === $__internal_2b0288bef81d800c62df7611209147e18d2bc70a248b6884bf402176079b8e15 || 0 === strpos($__internal_8dba37c67f0419f717579accf89759f867963fcec4f7f5db4522312dd5611a5f, $__internal_2b0288bef81d800c62df7611209147e18d2bc70a248b6884bf402176079b8e15)))) {
            // line 103
            echo "    Do You Wish to Reset Your Password
";
        } elseif ((is_string($__internal_50417a3c06573b030d45af8771e3aeefedd6bc05e7496abfcd8d3106fc3bbbf7 = $this->getAttribute($this->getAttribute(        // line 104
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_40b03d6bd5298ec2044ff699b6ade1abbad95a35e7a9912387779be530fc67ab = "http://www.businessbid.ae/aboutbusinessbid#contact") && ('' === $__internal_40b03d6bd5298ec2044ff699b6ade1abbad95a35e7a9912387779be530fc67ab || 0 === strpos($__internal_50417a3c06573b030d45af8771e3aeefedd6bc05e7496abfcd8d3106fc3bbbf7, $__internal_40b03d6bd5298ec2044ff699b6ade1abbad95a35e7a9912387779be530fc67ab)))) {
            // line 105
            echo "    All you wanted to Know About Us
";
        } elseif ((is_string($__internal_20ecad3c2a4ee07a9c5db4c39222f0160555c961b2a0127b8d27632de37afa24 = $this->getAttribute($this->getAttribute(        // line 106
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "uri", array())) && is_string($__internal_ab02a2ed3d338697f40de67eda4a1ab2ba6d62a4c3ef427c375c5c0f3e128b41 = "http://www.businessbid.ae/are_you_a_vendor") && ('' === $__internal_ab02a2ed3d338697f40de67eda4a1ab2ba6d62a4c3ef427c375c5c0f3e128b41 || 0 === strpos($__internal_20ecad3c2a4ee07a9c5db4c39222f0160555c961b2a0127b8d27632de37afa24, $__internal_ab02a2ed3d338697f40de67eda4a1ab2ba6d62a4c3ef427c375c5c0f3e128b41)))) {
            // line 107
            echo "    Information for All Prospective Vendors
";
        } else {
            // line 109
            echo "    Business BID
";
        }
    }

    // line 126
    public function block_noindexMeta($context, array $blocks = array())
    {
        // line 127
        echo "    ";
    }

    // line 161
    public function block_customcss($context, array $blocks = array())
    {
        // line 162
        echo "
";
    }

    // line 165
    public function block_javascripts($context, array $blocks = array())
    {
        // line 166
        echo "<script>
\$(document).ready(function(){
    ";
        // line 168
        if (array_key_exists("keyword", $context)) {
            // line 169
            echo "        window.onload = function(){
              document.getElementById(\"submitButton\").click();
            }
    ";
        }
        // line 173
        echo "    var realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

    \$('#category').autocomplete({
        source: function( request, response ) {
        \$.ajax({
            url : realpath+\"ajax-info.php\",
            dataType: \"json\",
            data: {
               name_startsWith: request.term,
               type: 'category'
            },
            success: function( data ) {
                response( \$.map( data, function( item ) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            },
            select: function(event, ui) {
                alert( \$(event.target).val() );
            }
        });
        },autoFocus: true,minLength: 2
    });

    \$(\"#form_email\").on('input',function (){
        var ax =    \$(\"#form_email\").val();
        if(ax==''){
            \$(\"#emailexists\").text('');
        }
        else {

            var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
            var \$th = \$(this);
            if (filter.test(ax)) {
                \$th.css('border', '3px solid green');
            }
            else {
                \$th.css('border', '3px solid red');
                e.preventDefault();
            }
            if(ax.indexOf('@') > -1 ) {
                \$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){
                    if(result == \"exists\") {
                        \$(\"#emailexists\").text('Email address already exists!');
                    } else {
                        \$(\"#emailexists\").text('');
                    }
                }
            });
            }

        }
    });
    \$(\"#form_vemail\").on('input',function (){
        var ax =    \$(\"#form_vemail\").val();
        var filter = /^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/;
        var \$th = \$(this);
        if (filter.test(ax))
        {
            \$th.css('border', '3px solid green');
        }
        else {
            \$th.css('border', '3px solid red');
            e.preventDefault();
            }
        if(ax==''){
        \$(\"#emailexists\").text('');
        }
        if(ax.indexOf('@') > -1 ) {
            \$.ajax({url:realpath+\"checkEmailv.php?emailid=\"+ax,success:function(result){

            if(result == \"exists\") {
                \$(\"#emailexists\").text('Email address already exists!');
            } else {
                \$(\"#emailexists\").text('');
            }
            }});
        }
    });
    \$('#form_description').attr(\"maxlength\",\"240\");
    \$('#form_location').attr(\"maxlength\",\"30\");
});
</script>
<script type=\"text/javascript\">
window.fbAsyncInit = function() {
    FB.init({
    appId      : '754756437905943', // replace your app id here
    status     : true,
    cookie     : true,
    xfbml      : true
    });
};
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = \"//connect.facebook.net/en_US/all.js\";
    ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
    FB.login(function(response){
        if(response.authResponse){
            window.location.href = \"//www.businessbid.ae/devfbtestlogin/actions.php?action=fblogin\";
        }
    }, {scope: 'email,user_likes'});
}
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){

  \$(\"#subscribe\").submit(function(e){
  var email=\$(\"#email\").val();

    \$.ajax({
        type : \"POST\",
        data : {\"email\":email},
        url:\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("newsletter.php"), "html", null, true);
        echo "\",
        success:function(result){
            \$(\"#succ\").css(\"display\",\"block\");
        },
        error: function (error) {
            \$(\"#err\").css(\"display\",\"block\");
        }
    });

  });
});
</script>


";
    }

    // line 309
    public function block_customjs($context, array $blocks = array())
    {
        // line 310
        echo "
";
    }

    // line 313
    public function block_jquery($context, array $blocks = array())
    {
        // line 314
        echo "
";
    }

    // line 316
    public function block_googleanalatics($context, array $blocks = array())
    {
        // line 317
        echo "
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-59054368-1', 'auto');ga('send', 'pageview');
</script>

";
    }

    // line 403
    public function block_maincontent($context, array $blocks = array())
    {
        // line 404
        echo "    <div class=\"main_content\">
    <div class=\"container content-top-one\">
    <h1>How does it work? </h1>
    <h3>Find the perfect vendor for your project in just three simple steps</h3>
    <div class=\"content-top-one-block\">
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 411
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/tell-us.jpg"), "html", null, true);
        echo " \" />
    <h4>TELL US ONCE</h4>
    <h5>Simply fill out a short form or give us a call</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/vendor-contacts.jpg"), "html", null, true);
        echo " \" />
    <h4>VENDORS CONTACT YOU</h4>
        <h5>Receive three quotes from screened vendors in minutes</h5>
    </div>
    </div>
    <div class=\"col-sm-4 float-shadow\">
    <div class=\"how-work\">
    <img src=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/choose-best-vendor.jpg"), "html", null, true);
        echo " \" />
    <h4>CHOOSE THE BEST VENDOR</h4>
    <h5>Use quotes and reviews to select the perfect vendors</h5>
    </div>
    </div>
    </div>
    </div>

<div class=\"content-top-two\">
    <div class=\"container\">
    <h2>Why choose Business Bid?</h2>
    <h3>The most effective way to find vendors for your work</h3>
    <div class=\"content-top-two-block\">
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 440
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/convenience.jpg"), "html", null, true);
        echo " \" />
    <h4>Convenience</h4>
    </div>
    <h5>Multiple quotations with a single, simple form</h5>
    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 447
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/competitive.jpg"), "html", null, true);
        echo " \" />
    <h4>Competitive Pricing</h4>
    </div>
    <h5>Compare quotes before picking the most suitable</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 455
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/speed.jpg"), "html", null, true);
        echo " \" />
    <h4>Speed</h4>
    </div>
    <h5>Quick responses to all your job requests</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/reputation.jpg"), "html", null, true);
        echo " \" />
    <h4>Reputation</h4>
    </div>
    <h5>Check reviews and ratings for the inside scoop</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 471
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/freetouse.jpg"), "html", null, true);
        echo " \" />
    <h4>Free To Use</h4>
    </div>
    <h5>No usage fees. Ever!</h5>

    </div>
    <div class=\"col-sm-4 wobble-vertical\">
    <div class=\"choose-vendor\">
    <img src=\"";
        // line 479
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/support.jpg"), "html", null, true);
        echo " \" />
    <h4>Amazing Support</h4>
    </div>
    <h5>Local Office. Phone. Live Chat. Facebook. Twitter</h5>

    </div>
    </div>
    </div>

</div>
        <div class=\"container content-top-three\">
            <div class=\"row\">
    ";
        // line 491
        $this->displayBlock('recentactivity', $context, $blocks);
        // line 531
        echo "            </div>

         </div>
        </div>

    </div>

     <div class=\"certified_block\">
        <div class=\"container\">
            <div class=\"row\">
            <h2 >We do work for you!</h2>
            <div class=\"content-block\">
            <p>Each vendor undergoes a carefully designed selection and screening process by our team to ensure the highest quality and reliability.</p>
            <p>Your satisfication matters to us!</p>
            <p>Get the job done fast and hassle free by using Business Bid's certified vendors.</p>
            <p>Other customers just like you, leave reviews and ratings of vendors. This helps you choose the perfect vendor and ensures an excellent customer
        service experience.</p>
            </div>
            </div>
        </div>
    </div>
     <div class=\"popular_category_block\">
        <div class=\"container\">
    <div class=\"row\">
    <h2>Popular Categories</h2>
    <h3>Check out some of the hottest services in the UAE</h3>
    <div class=\"content-block\">
    <div class=\"col-sm-12\">
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 561
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 562
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        } else {
            // line 564
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 14));
            echo "\">Accounting and <br>Auditing Services</a></li>
    ";
        }
        // line 566
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>

    ";
        // line 572
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 573
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        } else {
            // line 575
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 87));
            echo "\">Interior Designers and <br> Fit Out</a> </li>
    ";
        }
        // line 577
        echo "
    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 582
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 583
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        } else {
            // line 585
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 924));
            echo "\">Signage and <br>Signboards</a></li>
    ";
        }
        // line 587
        echo "


    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 594
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 595
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 45));
            echo "\">Catering Services</a></li>
    ";
        } else {
            // line 597
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 45));
            echo "\">Catering Services</a> </li>
    ";
        }
        // line 599
        echo "
    </ul>
    </div>

    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 605
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 606
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        } else {
            // line 608
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 93));
            echo "\">Landscaping and Gardens</a></li>
    ";
        }
        // line 610
        echo "

    </ul>
    </div>
    <div class=\"col-sm-2\">
    <ul>
    ";
        // line 616
        if ( !twig_test_empty((isset($context["uid"]) ? $context["uid"] : $this->getContext($context, "uid")))) {
            // line 617
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search", array("categoryid" => 89));
            echo "\">IT Support</a></li>
    ";
        } else {
            // line 619
            echo "    <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_by_search_inline", array("categoryid" => 89));
            echo "\">IT Support</a> </li>
    ";
        }
        // line 621
        echo "

    </ul>
    </div>
    </div>
    </div>
    <div class=\"col-sm-12 see-all-cat-block\"><a  href=\"/resource-centre/home\" class=\"btn btn-success see-all-cat\" >See All Categories</a></div>
    </div>
    </div>
       </div>


    ";
    }

    // line 491
    public function block_recentactivity($context, array $blocks = array())
    {
        // line 492
        echo "    <h2>We operate all across the UAE</h2>
            <div class=\"col-md-6 grow\">
    <img src=\"";
        // line 494
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location-map.jpg"), "html", null, true);
        echo " \" />
            </div>

    <div class=\"col-md-6\">
                <h2>Recent Jobs</h2>
                <div id=\"demo2\" class=\"scroll-text\">
                    <ul class=\"recent_block\">
                        ";
        // line 501
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : $this->getContext($context, "activities")));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 502
            echo "                        <li class=\"activities-block\">
                        <div class=\"act-date\">";
            // line 503
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["activity"], "created", array()), "d-m-Y G:i"), "html", null, true);
            echo "</div>
                        ";
            // line 504
            if (($this->getAttribute($context["activity"], "type", array()) == 1)) {
                // line 505
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 506
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : $this->getContext($context, "fullname")), " ");
                // line 507
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : $this->getContext($context, "firstName")), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", posted a new job against <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            } else {
                // line 509
                echo "                            ";
                $context["fullname"] = $this->getAttribute($context["activity"], "contactname", array());
                // line 510
                echo "                            ";
                $context["firstName"] = twig_split_filter($this->env, (isset($context["fullname"]) ? $context["fullname"] : $this->getContext($context, "fullname")), " ");
                // line 511
                echo "                        <span><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["firstName"]) ? $context["firstName"] : $this->getContext($context, "firstName")), 0, array(), "array"), "html", null, true);
                echo "</strong> from ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "city", array()), "html", null, true);
                echo ", recommended ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "message", array()), "html", null, true);
                echo " for <a href=\"";
                echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
                echo "search?category=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["activity"], "category", array()), "html", null, true);
                echo "</a></span>
                        ";
            }
            // line 513
            echo "                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 515
        echo "                    </ul>
                </div>


    <div class=\"col-md-12 side-clear\">
    ";
        // line 520
        $this->displayBlock('requestsfeed', $context, $blocks);
        // line 526
        echo "    </div>
            </div>


    ";
    }

    // line 520
    public function block_requestsfeed($context, array $blocks = array())
    {
        // line 521
        echo "    <div class=\"request\">
    <h2>";
        // line 522
        echo twig_escape_filter($this->env, (isset($context["enquiries"]) ? $context["enquiries"] : $this->getContext($context, "enquiries")), "html", null, true);
        echo "</h2>
    <span>NUMBER OF NEW REQUESTS UPDATED TODAY</span>
    </div>
    ";
    }

    // line 637
    public function block_footer($context, array $blocks = array())
    {
        // line 638
        echo "</div>
        <div class=\"main_footer_area\" style=\"width:100%;\">
            <div class=\"inner_footer_area\">
                <div class=\"customar_footer_area\">
                   <div class=\"ziggag_area\"></div>
                    <h2>Customer Benefits</h2>
                    <ul>
                        <li class=\"convenience\"><p>Convenience<span>Multiple quotations with a single, simple form</span></p></li>
                        <li class=\"competitive\"><p>Competitive Pricing<span>Compare quotes before choosing the most suitable.</span></p></li>
                        <li class=\"speed\"><p>Speed<span>Quick responses to all your job requests.</span></p></li>
                        <li class=\"reputation\"><p>Reputation<span>Check reviews and ratings for the inside scoop</span></p></li>
                        <li class=\"freetouse\"><p>Free to Use<span>No usage fees. Ever!</span></p></li>
                        <li class=\"amazing\"><p>Amazing Support<span>Phone. Live Chat. Facebook. Twitter.</span></p></li>
                    </ul>
                </div>
                <div class=\"about_footer_area\">
                    <div class=\"inner_abour_area_footer\">
                        <h2>About</h2>
                        <ul>
                            <li><a href=\"";
        // line 657
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#aboutus\">About Us</a></li>
                            <li><a href=\"";
        // line 658
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#meetteam\">Meet the Team</a></li>
                            <li><a href=\"";
        // line 659
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#career\">Career Opportunities</a></li>
                            <li><a href=\"";
        // line 660
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_aboutbusinessbid");
        echo "#valuedpartner\">Valued Partners</a></li>
                            <li><a href=\"";
        // line 661
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_area_footer\">
                        <h2>Client Services</h2>
                        <ul>
                            <li><a href=\"";
        // line 667
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\">Search Reviews</a></li>
                            <li><a href=\"";
        // line 668
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_reviewlogin");
        echo "\">Write a Review</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"vendor_footer_area\">
                    <div class=\"inner_vendor_area_footer\">
                        <h2>Vendor Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 676
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#vendor\">Login</a></li>
                            <li><a href=\"";
        // line 677
        echo $this->env->getExtension('routing')->getPath("bizbids_template5");
        echo "\">Grow Your Business</a></li>
                            <li><a href=\"";
        // line 678
        echo $this->env->getExtension('routing')->getPath("bizbids_template2");
        echo "\">How it Works</a></li>
                            <li><a href=\"";
        // line 679
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_register");
        echo "\">Become a Vendor</a></li>
                            <li><a href=\"";
        // line 680
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_faq");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                    <div class=\"inner_client_resposce_footer\">
                        <h2>Client Resources</h2>
                        <ul>
                            <li><a href=\"";
        // line 686
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_login");
        echo "#consumer\">Login</a></li>
                            ";
        // line 688
        echo "                            <li><a href=\"http://www.businessbid.ae/resource-centre/home-2/\">Resource Center</a></li>
                            <li><a href=\"";
        // line 689
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node6");
        echo "\">FAQ's & Support</a></li>
                        </ul>
                    </div>
                </div>
                <div class=\"saty_footer_area\">
                    <div class=\"footer_social_area\">
                       <h2>Stay Connected</h2>
                        <ul>
                            <li><a href=\"https://www.facebook.com/businessbid?ref=hl\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 697
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_face_icon.PNG"), "html", null, true);
        echo "\" alt=\"Facebook Icon\" ></a></li>
                            <li><a href=\"https://twitter.com/BusinessBid\" rel=\"nofollow\" target=\"_blank\"><img src=\"";
        // line 698
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_twitter_icon.PNG"), "html", null, true);
        echo "\" alt=\"Twitter Icon\"></a></li>
                            <li><a href=\"https://plus.google.com/102994148999127318614\" rel=\"nofollow\"  target=\"_blank\"><img src=\"";
        // line 699
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/f_google_icon.PNG"), "html", null, true);
        echo "\" alt=\"Google Icon\"></a></li>
                        </ul>
                    </div>
                    <div class=\"footer_card_area\">
                       <h2>Accepted Payment</h2>
                        <img src=\"";
        // line 704
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/card_icon.PNG"), "html", null, true);
        echo "\" alt=\"card_area\">
                    </div>
                </div>
                <div class=\"contact_footer_area\">
                    <div class=\"inner_contact_footer_area\">
                        <h2>Contact Us</h2>
                        <ul>
                            <li class=\"place\">
                                <p>Suite 503, Level 5, Indigo Icon<br/>
                            Tower, Cluster F, Jumeirah Lakes<br/>
                            Tower - Dubai, UAE</p>
                            <p>Sunday-Thursday<br/>
                            9 am - 6 pm</p>
                            </li>
                            <li class=\"phone\"><p>(04) 42 13 777</p></li>
                            <li class=\"business\"><p>BusinessBid DMCC,<br/>
                            <p>PO Box- 393507, Dubai, UAE</p></li>
                        </ul>
                    </div>
                    <div class=\"inner_newslatter_footer_area\">
                        <h2>Subscribe to Newsletter</h2>
                        <p>Sign up with your e-mail to get tips, resources and amazing deals</p>
                             <div class=\"mail-box\">
                                <form name=\"news\" method=\"post\"  action=\"http://115.124.120.36/resource-centre/wp-content/plugins/newsletter/do/subscribe.php\" onsubmit=\"return newsletter_check(this)\" >
                                <input type=\"hidden\" name=\"nr\" value=\"widget\">
                                <input type=\"email\" name=\"ne\" id=\"email\" value=\"\" placeholder=\"Enter your email\">
                                <input class=\"submit\" type=\"submit\" value=\"Subscribe\" />
                                </form>
                            </div>
                        <div class=\"inner_newslatter_footer_area_zig\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"botom_footer_menu\">
            <div class=\"inner_bottomfooter_menu\">
                <ul>
                    <li><a href=\"";
        // line 741
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_feedback");
        echo "\">Feedback</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#privacy-policy\">Privacy policy</a></li>
                    <li><a href=\"";
        // line 743
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_sitemap");
        echo "\">Site Map</a></li>
                    <li><a data-toggle=\"modal\" data-target=\"#terms-conditions\">Terms & Conditions</a></li>
                </ul>
            </div>

        </div>
    ";
    }

    // line 753
    public function block_footerScript($context, array $blocks = array())
    {
        // line 754
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/plugins.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 755
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/new/main.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1410 => 755,  1405 => 754,  1402 => 753,  1391 => 743,  1386 => 741,  1346 => 704,  1338 => 699,  1334 => 698,  1330 => 697,  1319 => 689,  1316 => 688,  1312 => 686,  1303 => 680,  1299 => 679,  1295 => 678,  1291 => 677,  1287 => 676,  1276 => 668,  1272 => 667,  1263 => 661,  1259 => 660,  1255 => 659,  1251 => 658,  1247 => 657,  1226 => 638,  1223 => 637,  1215 => 522,  1212 => 521,  1209 => 520,  1201 => 526,  1199 => 520,  1192 => 515,  1185 => 513,  1169 => 511,  1166 => 510,  1163 => 509,  1149 => 507,  1146 => 506,  1143 => 505,  1141 => 504,  1137 => 503,  1134 => 502,  1130 => 501,  1120 => 494,  1116 => 492,  1113 => 491,  1097 => 621,  1091 => 619,  1085 => 617,  1083 => 616,  1075 => 610,  1069 => 608,  1063 => 606,  1061 => 605,  1053 => 599,  1047 => 597,  1041 => 595,  1039 => 594,  1030 => 587,  1024 => 585,  1018 => 583,  1016 => 582,  1009 => 577,  1003 => 575,  997 => 573,  995 => 572,  987 => 566,  981 => 564,  975 => 562,  973 => 561,  941 => 531,  939 => 491,  924 => 479,  913 => 471,  902 => 463,  891 => 455,  880 => 447,  870 => 440,  852 => 425,  842 => 418,  832 => 411,  823 => 404,  820 => 403,  810 => 317,  807 => 316,  802 => 314,  799 => 313,  794 => 310,  791 => 309,  772 => 293,  650 => 173,  644 => 169,  642 => 168,  638 => 166,  635 => 165,  630 => 162,  627 => 161,  623 => 127,  620 => 126,  614 => 109,  610 => 107,  608 => 106,  605 => 105,  603 => 104,  600 => 103,  598 => 102,  595 => 101,  593 => 100,  590 => 99,  588 => 98,  585 => 97,  583 => 96,  580 => 95,  578 => 94,  575 => 93,  573 => 92,  570 => 91,  568 => 90,  565 => 89,  563 => 88,  560 => 87,  558 => 86,  555 => 85,  553 => 84,  550 => 83,  548 => 82,  545 => 81,  543 => 80,  540 => 79,  538 => 78,  535 => 77,  533 => 76,  530 => 75,  528 => 74,  525 => 73,  523 => 72,  520 => 71,  518 => 70,  515 => 69,  513 => 68,  510 => 67,  508 => 66,  505 => 65,  503 => 64,  500 => 63,  498 => 62,  495 => 61,  493 => 60,  490 => 59,  488 => 58,  485 => 57,  483 => 56,  480 => 55,  478 => 54,  475 => 53,  473 => 52,  470 => 51,  468 => 50,  465 => 49,  463 => 48,  460 => 47,  458 => 46,  455 => 45,  453 => 44,  450 => 43,  448 => 42,  445 => 41,  443 => 40,  440 => 39,  438 => 38,  435 => 37,  433 => 36,  430 => 35,  428 => 34,  425 => 33,  423 => 32,  420 => 31,  418 => 30,  415 => 29,  413 => 28,  410 => 27,  408 => 26,  405 => 25,  403 => 24,  400 => 23,  398 => 22,  395 => 21,  393 => 20,  390 => 19,  388 => 18,  385 => 17,  383 => 16,  380 => 15,  378 => 14,  375 => 13,  373 => 12,  370 => 11,  365 => 764,  356 => 757,  354 => 753,  351 => 750,  349 => 637,  344 => 634,  342 => 403,  333 => 396,  319 => 393,  316 => 392,  314 => 391,  308 => 388,  304 => 386,  302 => 385,  294 => 380,  286 => 375,  281 => 373,  275 => 370,  270 => 368,  266 => 367,  257 => 360,  241 => 355,  237 => 354,  233 => 353,  229 => 351,  227 => 350,  221 => 347,  215 => 346,  211 => 344,  209 => 343,  200 => 337,  193 => 332,  191 => 331,  189 => 330,  187 => 329,  185 => 328,  181 => 326,  179 => 325,  176 => 324,  174 => 316,  172 => 313,  169 => 312,  167 => 309,  164 => 308,  162 => 165,  159 => 164,  157 => 161,  152 => 159,  148 => 158,  139 => 152,  134 => 150,  130 => 149,  125 => 147,  121 => 146,  116 => 144,  112 => 143,  108 => 142,  104 => 141,  100 => 140,  96 => 139,  85 => 131,  80 => 128,  78 => 126,  73 => 123,  69 => 121,  66 => 120,  62 => 118,  60 => 117,  52 => 111,  50 => 11,  47 => 10,  45 => 9,  43 => 8,  41 => 7,  39 => 6,  37 => 5,  31 => 1,);
    }
}

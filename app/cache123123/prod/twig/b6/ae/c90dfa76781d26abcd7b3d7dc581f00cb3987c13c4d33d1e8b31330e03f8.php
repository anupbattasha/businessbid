<?php

/* BBidsBBidsHomeBundle:Admin:viewmappedvendors.html.twig */
class __TwigTemplate_b6aec90dfa76781d26abcd7b3d7dc581f00cb3987c13c4d33d1e8b31330e03f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base_admin.html.twig", "BBidsBBidsHomeBundle:Admin:viewmappedvendors.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container\">
<div class=\"page-title\"><h1>Mapped Vendors -Job request number-";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["enquiryid"]) ? $context["enquiryid"] : null), "html", null, true);
        echo "</h1>
\t";
        // line 7
        if (((isset($context["expStatus"]) ? $context["expStatus"] : null) == 2)) {
            echo "<span class=\"cleared-status\">Job is Cleared </span>
\t";
        } elseif ((        // line 8
(isset($context["enquiryacceptcount"]) ? $context["enquiryacceptcount"] : null) >= 3)) {
            echo "<span class=\"open-status\">Job Assigned </span>
\t";
        } else {
            // line 9
            echo "<span class=\"close-status\">Job is open </span>";
        }
        echo "</div>
<div>
\t";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 12
            echo "
\t<div class=\"alert alert-danger\">";
            // line 13
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
\t";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 18
            echo "
\t<div class=\"alert alert-success\">";
            // line 19
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "<table class=\"vendor-enquiries\">
<thead><tr><td>Contact Name</td><td>Business/Company Name</td><td>Contact Number</td><td>Email</td><td>Lead Status</td></tr></thead>
\t";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mapping"]) ? $context["mapping"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["mapped"]) {
            // line 25
            echo "\t<tr>
\t\t<td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["mapped"], "contactname", array()), "html", null, true);
            echo "</td>
\t\t<td ";
            // line 27
            if (( !(null === $this->getAttribute($context["mapped"], "softstatus", array())) && ($this->getAttribute($context["mapped"], "softstatus", array()) == 1))) {
                echo " style=\"background-color: rgb(255, 222, 0);\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["mapped"], "bizname", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["mapped"], "smsphone", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["mapped"], "email", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 30
            if (($this->getAttribute($context["mapped"], "acceptstatus", array()) == 0)) {
                echo " Waiting For Response ";
            } elseif (($this->getAttribute($context["mapped"], "acceptstatus", array()) == 1)) {
                echo " Accepted  ";
            }
            echo "</td>

\t</tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mapped'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "</table>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Admin:viewmappedvendors.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 34,  120 => 30,  116 => 29,  112 => 28,  104 => 27,  100 => 26,  97 => 25,  93 => 24,  89 => 22,  80 => 19,  77 => 18,  73 => 17,  70 => 16,  61 => 13,  58 => 12,  54 => 11,  48 => 9,  43 => 8,  39 => 7,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Vendor:activationemailvendor.html.twig */
class __TwigTemplate_7169350ea1af563931b19fb7ab777e5721c83dc666d1f71c3c6cea76b50757aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table width=\"100%\" bgcolor=\"#939598\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
  <tr>
    <td colspan=\"2\" align=\"center\" valign=\"middle\" bgcolor=\"#939598\">
    <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td height=\"23\" bgcolor=\"#f36e23\">&nbsp;</td>
  </tr>
  <tr>
    <!--<td height=\"92\" align=\"center\" bgcolor=\"#FFFFFF\"><img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/img/logo.png"), "html", null, true);
        echo "\" ></td>-->
    <td height=\"92\" align=\"center\" bgcolor=\"#FFFFFF\"><img src=\"http://115.124.120.36/web/img/logo.png\" ></td>
  </tr>
  <tr>
    <td height=\"34\" bgcolor=\"#FFFFFF\">&nbsp;</td>
  </tr>
  <tr>
    <td align=\"center\" bgcolor=\"#FFFFFF\"><table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>


        <td align=\"center\" style=\" font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; color:#262626;font-family:Proxinova,Calibri,Arial;\">
        <p style=\"font-size:24px; margin-bottom:10px; font-weight:600; color:#1e7dc0; font-family:Proxinova,Calibri,Arial;\">Welcome to the BusinessBid Network.</p></td>
      </tr>
       
       <tr>
        <td align=\"left\" style=\"font-size:14px;line-height: 22px;color:#262626;font-family:Proxinova,Calibri,Arial;\">
\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">We have only one more step to be completed and then your account with BusinessBid will be processed.</p>
          <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">With your own BusinessBid account, you can be assured that you have access to qualified customer leads who are looking for service professionals like yourselves to assist them with their projects.</p>
          <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;\">And of course, if you have any questions or need help call us on 04 42 13 777.</p>
        </td>
      </tr>
      <tr>
        <td align=\"left\"  style=\"font-size:14px;line-height: 22px;color:#262626;font-family:Proxinova,Calibri,Arial;\">Please click on the link below to set up your password and credentials.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align=\"center\"></td>
      </tr>
      <tr>
        <td align=\"center\"><table width=\"280\" bgcolor=\"#d25e00\" align=\"center\" cellspacing=\"2\" cellpadding=\"0\" border=\"0\">
<tbody>
<tr>
<td bgcolor=\"#ff7201\" align=\"center\">
<a href=\"http://www.businessbid.ae";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_user_email_verify", array("userid" => (isset($context["userid"]) ? $context["userid"] : null), "userhash" => (isset($context["userhash"]) ? $context["userhash"] : null))), "html", null, true);
        echo "\" target=\"_blank\" style=\"color:#ffffff;font-family:Proxinova,Calibri,Arial; font-size:20px;text-decoration:none;display:block;padding-top:12px;padding-bottom:12px\"  >
<b>CLICK HERE </b>

</a>
</td>
</tr>
</tbody>
</table></td>
      </tr>
      <tr>
        <td>
\t\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt;text-align:left\">Regards<br>
                BusinessBid Team
\t\t\t</p>
\t\t</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table></td>
  </tr>
  <tr>
    <td bgcolor=\"#eeeeee\">&nbsp;</td>
  </tr>
 
  <tr>
    <td bgcolor=\"#eeeeee\">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor=\"#939598\">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor=\"#939598\">&nbsp;</td>
  </tr>
    </table>

    </td>
  </tr>
  </table>


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Vendor:activationemailvendor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 50,  34 => 14,  19 => 1,);
    }
}

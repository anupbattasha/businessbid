<?php

/* BBidsBBidsHomeBundle:User:consumeraccount.html.twig */
class __TwigTemplate_5630fa29430f25b14775ea22c01a9dab40c091d3f4fe9f9168503a5d2177ddde extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "BBidsBBidsHomeBundle:User:consumeraccount.html.twig", 1);
        $this->blocks = array(
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_enquiries($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">
<div class=\"page-title\"><h1>My Customer Account</h1></div>
";
        // line 7
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 8
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 9
        echo "
<div class=\"row\">
<div class=\"panel-title\"><h2>My Profile</h2></div>
\t<div class=\"vendor-user user-information dashboard-container\">
\t";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) ? $context["user"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 14
            echo "\t
\t";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["account"]) ? $context["account"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["accounts"]) {
                // line 16
                echo "\t<div class=\"field-item\"><label>Contact Name</label><div class=\"field-value\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "contactname", array()), "html", null, true);
                echo "</div></div>
\t";
                // line 17
                if ( !twig_test_empty($this->getAttribute($context["accounts"], "bizname", array()))) {
                    // line 18
                    echo "\t<div class=\"field-item\"><label>Business/Company Name</label><div class=\"field-value\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "bizname", array()), "html", null, true);
                    echo "</div></div>
\t";
                }
                // line 20
                echo "\t<div class=\"field-item\"><label>Mobile Number</label><div class=\"field-value\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "smsphone", array()), "html", null, true);
                echo "</div></div>
\t";
                // line 21
                if ( !twig_test_empty($this->getAttribute($context["accounts"], "homephone", array()))) {
                    // line 22
                    echo "\t<div class=\"field-item\"><label>Home Phone</label><div class=\"field-value\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "homephone", array()), "html", null, true);
                    echo "</div></div>
\t";
                }
                // line 24
                echo "\t";
                if ( !twig_test_empty($this->getAttribute($context["accounts"], "fax", array()))) {
                    // line 25
                    echo "\t<div class=\"field-item\"><label>Fax Number</label><div class=\"field-value\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "fax", array()), "html", null, true);
                    echo "</div></div>
\t";
                }
                // line 27
                echo "\t";
                if ( !twig_test_empty($this->getAttribute($context["accounts"], "address", array()))) {
                    // line 28
                    echo "\t<div class=\"field-item\"><label>Address</label><div class=\"field-value\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["accounts"], "address", array()), "html", null, true);
                    echo "</div></div>
\t";
                }
                // line 30
                echo "\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['accounts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "\t<div class=\"field-item\"><label>Email Address</label><div class=\"field-value\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["users"], "email", array()), "html", null, true);
            echo "</div></div>
\t<div class=\"field-item\"><label>Member Since</label><div class=\"field-value\">";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["users"], "created", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</div></div>
\t<div class=\"field-item\"><label>Updated Date</label><div class=\"field-value\">";
            // line 33
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["users"], "updated", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</div></div>
\t<div class=\"field-item\"><label>Last Access Date</label><div class=\"field-value\">";
            // line 34
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["users"], "lastaccess", array()), "Y-m-d h:i:s"), "html", null, true);
            echo "</div></div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t</div>
\t 

";
        // line 41
        echo "
\t
</div>
";
        // line 44
        if (((isset($context["pid"]) ? $context["pid"] : null) != 3)) {
            // line 45
            echo "<div class=\"row text-right update-account\">\t

\t\t<a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_update", array("consumerid" => (isset($context["uid"]) ? $context["uid"] : null))), "html", null, true);
            echo "\" class=\"btn btn-success\"> Update My Profile </a>

</div>
\t";
        }
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:consumeraccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 47,  140 => 45,  138 => 44,  133 => 41,  128 => 37,  119 => 34,  115 => 33,  111 => 32,  106 => 31,  100 => 30,  94 => 28,  91 => 27,  85 => 25,  82 => 24,  76 => 22,  74 => 21,  69 => 20,  63 => 18,  61 => 17,  56 => 16,  52 => 15,  49 => 14,  45 => 13,  39 => 9,  37 => 8,  35 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}

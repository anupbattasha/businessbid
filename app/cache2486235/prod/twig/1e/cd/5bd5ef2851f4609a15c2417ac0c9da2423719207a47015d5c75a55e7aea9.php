<?php

/* BBidsBBidsHomeBundle:User:leads_paymentconfirmation.html.twig */
class __TwigTemplate_1ecd5bd5ef2851f4609a15c2417ac0c9da2423719207a47015d5c75a55e7aea9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::vendor_dashboard.html.twig", "BBidsBBidsHomeBundle:User:leads_paymentconfirmation.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::vendor_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container payment-confirmation\">
<br>
<div class=\"page-title\"><h1>Order Confirmation</h1></div><br>
<h5>Thank you for your payment, your order number is ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["ordernumber"]) ? $context["ordernumber"] : null), "html", null, true);
        echo "</h5><br>
<p>Your order will be activated within 48 hours of the payment being processed.<br>
Businessbid has access to customers who are looking to engage vendors all over the UAE for their project needs.</p><br>
<p>With your own BusinessBid Account you can be assured that you have access to qualified customers leads<br>
thus generating a reliable and easy source of new business generation.</p>
<p>Please ensure you set up your dashboard and profile to reflect your preferences and business needs.</p>
<br>
<div><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendororders");
        echo "\" class=\"btn btn-success see-all-cat\">CONTINUE</a></div>

<div class=\"col-sm-10 next-step\">
<div class=\"page-title\"><h1>WHAT ARE THE NEXT STEPS?</h1></div>
<ul>
 <li>Once your payment has been processed you will begin to receive customer enquiries and leads on the registered mobile contact number for the services that you have subscribed for. This Usually takes up to 48 hours, defending upon the mode of payment.</li>
 <li>You will receive an update notification from the BusinessBid team and/or contact from your account manager. </li>
</ul>
<p>If you have questions or need assistance contact us on 04 42 13 777 or email us on <a href=\"mailto:support@businessbid.ae\" target=\"_top\">support@businessbid.ae</a></p>
</div>
<div>
    ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "
    <div class=\"alert alert-danger\">";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
    ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 32
            echo "
    <div class=\"alert alert-success\">";
            // line 33
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "
    ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 38
            echo "
    <div class=\"alert alert-success\">";
            // line 39
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:leads_paymentconfirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 42,  105 => 39,  102 => 38,  98 => 37,  95 => 36,  86 => 33,  83 => 32,  79 => 31,  76 => 30,  67 => 27,  64 => 26,  60 => 25,  46 => 14,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}

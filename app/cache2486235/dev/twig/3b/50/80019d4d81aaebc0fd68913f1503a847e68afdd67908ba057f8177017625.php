<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig */
class __TwigTemplate_3b5080019d4d81aaebc0fd68913f1503a847e68afdd67908ba057f8177017625 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <body>
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">Dear ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo ",</p>
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">Congratulations!</p>
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
\t\t\tYou have accepted- Job ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["enquiryid"]) ? $context["enquiryid"] : $this->getContext($context, "enquiryid")), "html", null, true);
        echo " for ";
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo " in ";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo " and 1 Lead have been deducted from your account.
\t\t</p>
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
\t\t\tPlease Contact ";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["authname"]) ? $context["authname"] : $this->getContext($context, "authname")), "html", null, true);
        echo " on ";
        echo twig_escape_filter($this->env, (isset($context["authmobile"]) ? $context["authmobile"] : $this->getContext($context, "authmobile")), "html", null, true);
        echo ".
\t\t</P>
          
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
            Good luck with the project!
\t\t</p>
\t\t<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; width:100%; \">
\t\t\tRegards<br>BusinessBid
\t\t</p>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Vendor:enquiryacceptancestatusemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  29 => 6,  23 => 3,  19 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Home:enview.html.twig */
class __TwigTemplate_7be0377a10cff4c942cb2dbada78aedf2e86edf7bac53d6f7a7ba542e5f4b298 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::consumer_dashboard.html.twig", "BBidsBBidsHomeBundle:Home:enview.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::consumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageblock($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-9 dashboard-rightpanel\">

<div class=\"page-title\"><h1>View Job Requests</h1></div>

<div class=\"row\">

<table class=\"vendor-enquiries\">
<table>
<tr>
<th>Job Requested Date</th>
<th>Job Requested Number</th>
<th>Subject</th>
<th>Category</th>
<th>Description</th>
<th>Job Request Accepted Date</th>
<th>City</th>
<th>Location</th>
<th>Contact Name</th>
</tr>
";
        // line 23
        if ( !twig_test_empty((isset($context["enquiry"]) ? $context["enquiry"] : null))) {
            // line 24
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiry"]) ? $context["enquiry"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 25
                echo "<tr><td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["e"], "created", array()), "Y-m-d"), "html", null, true);
                echo "</td>
<td>";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
                echo "</td>
<td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "subj", array()), "html", null, true);
                echo "</td>
<td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "category", array()), "html", null, true);
                echo "</td>
<td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "description", array()), "html", null, true);
                echo "</td>
<td>";
                // line 30
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["e"], "updated", array()), "Y-m-d"), "html", null, true);
                echo "</td>
<td>";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "city", array()), "html", null, true);
                echo "</td>
<td>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "location", array()), "html", null, true);
                echo "</td>

<td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "contactname", array()), "html", null, true);
                echo "</td></tr>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "
";
        } else {
            // line 38
            echo "
<tr><td>No data Available</td></tr>

";
        }
        // line 42
        echo "</table>


</table>

</div>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Home:enview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 42,  104 => 38,  100 => 36,  92 => 34,  87 => 32,  83 => 31,  79 => 30,  75 => 29,  71 => 28,  67 => 27,  63 => 26,  58 => 25,  54 => 24,  52 => 23,  31 => 4,  28 => 3,  11 => 1,);
    }
}

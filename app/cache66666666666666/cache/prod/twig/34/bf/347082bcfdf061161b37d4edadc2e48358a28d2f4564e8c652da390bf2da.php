<?php

/* TicketingTicketTicketBundle:Customer:index.html.twig */
class __TwigTemplate_34bf347082bcfdf061161b37d4edadc2e48358a28d2f4564e8c652da390bf2da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::newconsumer_dashboard.html.twig", "TicketingTicketTicketBundle:Customer:index.html.twig", 1);
        $this->blocks = array(
            'pageblock' => array($this, 'block_pageblock'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'enquiries' => array($this, 'block_enquiries'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::newconsumer_dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_pageblock($context, array $blocks = array())
    {
        // line 3
        echo "\t\t
\t\t<div class=\"container\">
\t\t<div class=\"dashboard-panel\">
\t\t
\t\t";
        // line 7
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 20
        echo "
\t\t";
        // line 21
        $this->displayBlock('enquiries', $context, $blocks);
        // line 60
        echo "\t\t</div>
\t\t</div>


\t
";
    }

    // line 7
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 8
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
        echo "\">Customer Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"lead-purchases\" href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_home");
        echo "\">Submit Support Ticket</a></li>
                            <li><a class=\"account\" href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_list");
        echo "\">View All Tickets</a></li>
                            <li><a class=\"livechat\" href=\"#\">Live Chat</a></li>
                            <li><a class=\"contact\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
        echo "\">Contact Us</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t";
    }

    // line 21
    public function block_enquiries($context, array $blocks = array())
    {
        // line 22
        echo "\t\t\t\t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t\t<div>
\t\t\t\t\t";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 25
            echo "\t\t\t\t
\t\t\t\t\t\t<div class=\"alert alert-danger\">";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "\t\t\t\t
\t\t\t\t\t\t<div class=\"alert alert-success\">";
            // line 32
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 37
            echo "\t\t\t\t
\t\t\t\t\t\t<div class=\"alert alert-success\">";
            // line 38
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "\t\t\t\t </div>
\t\t\t\t \t<div class=\"page-title\"><h1>Create Ticket</h1>\t</div>
\t\t\t\t\t<div class=\"Create-ticket-form\">
\t\t\t\t\t\t<div class=\"create-ticket col-sm-7\">
\t\t\t\t\t\t\t";
        // line 44
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["ticketform"]) ? $context["ticketform"] : null), 'form');
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"add-certificate col-sm-5\">
\t\t\t\t\t\t\t\t<div class=\"add-certificate-block\">
\t\t\t\t\t\t\t\t<h4>We believe you need help with something and you've come to the right place. However, you may be able to find an answer to your questions by accessing the following sections:</h4>
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li>Browse our FAQ's for a comprehensive list of everyday queries.</li>
\t\t\t\t\t\t\t\t<li>?Live Chat with our Support Personnel who might be able to help you immediately.</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t<h5>Sometimes it's easier to try one of the options above for a quicker resolution.</h5>
\t\t\t\t\t\t\t\t<h5><strong>We are here to serve you better!</strong></h5>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>\t 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t";
    }

    public function getTemplateName()
    {
        return "TicketingTicketTicketBundle:Customer:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 44,  145 => 40,  137 => 38,  134 => 37,  130 => 36,  127 => 35,  118 => 32,  115 => 31,  111 => 30,  108 => 29,  99 => 26,  96 => 25,  92 => 24,  88 => 22,  85 => 21,  76 => 15,  71 => 13,  67 => 12,  63 => 11,  58 => 8,  55 => 7,  46 => 60,  44 => 21,  41 => 20,  39 => 7,  33 => 3,  30 => 2,  11 => 1,);
    }
}

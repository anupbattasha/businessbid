<?php

/* BBidsBBidsHomeBundle:User:showleadscredit.html.twig */
class __TwigTemplate_13754ce4109687d193bdb8a911feeea082b169c6cebf956cbfb98029a0bc4d45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (twig_test_empty((isset($context["credits"]) ? $context["credits"] : null))) {
            // line 2
            echo "
No leads credited\t

";
        } else {
            // line 6
            echo "

<div class=\"top-space-block\">
<table class=\"col-md-6 vendor-enquiries\">
 <thead>
 <tr>
     <th>Category Name </th>
\t <th>Leads Credited </th>
\t <th>New Lead Count </th>
\t <th>Lead Credited Date </th>
</tr>
</thead>
<tbody>
";
            // line 19
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["credits"]) ? $context["credits"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 20
                echo " <tr>
     <td class=\"center-align\">";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "category", array()), "html", null, true);
                echo " </td>
\t <td class=\"center-align\"> ";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "leadcredited", array()), "html", null, true);
                echo "</td>
\t <td class=\"center-align\">";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "newleadcount", array()), "html", null, true);
                echo " </td>
\t <td class=\"center-align\"> ";
                // line 24
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["c"], "created", array()), "Y-m-d"), "html", null, true);
                echo "</td>\t 
</tr>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "</tbody>
</table>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:showleadscredit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 27,  61 => 24,  57 => 23,  53 => 22,  49 => 21,  46 => 20,  42 => 19,  27 => 6,  21 => 2,  19 => 1,);
    }
}

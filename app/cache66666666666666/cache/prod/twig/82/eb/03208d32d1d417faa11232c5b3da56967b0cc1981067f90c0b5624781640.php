<?php

/* BBidsBBidsHomeBundle:User:form_extra_fields.html.twig */
class __TwigTemplate_82eb03208d32d1d417faa11232c5b3da56967b0cc1981067f90c0b5624781640 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style type=\"text/css\">
.tg{border-collapse:collapse;border-spacing:0;border-color:#ccc}.tg td{font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff}.tg th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0}.tg .tg-4eph{background-color:#f9f9f9}
</style>
<table class=\"tg\">
  <tr>
    <th class=\"tg-031e\">Tag</th>
    <th class=\"tg-031e\">Value</th>
  </tr>
  ";
        // line 9
        $context["rowColor"] = "";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["formExtraFields"]) ? $context["formExtraFields"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 11
            echo "  ";
            if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                echo " ";
                $context["rowColor"] = "tg-4eph";
                echo " ";
            } else {
                echo " ";
                $context["rowColor"] = "tg-031e";
                echo " ";
            }
            // line 12
            echo "  <tr>
    <td class=\"";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["rowColor"]) ? $context["rowColor"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fields"], "keyName", array()), "html", null, true);
            echo "</td>
    <td class=\"";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["rowColor"]) ? $context["rowColor"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fields"], "keyValue", array()), "html", null, true);
            echo "</td>
  </tr>
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "</table>";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:User:form_extra_fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 17,  68 => 14,  62 => 13,  59 => 12,  48 => 11,  31 => 10,  29 => 9,  19 => 1,);
    }
}

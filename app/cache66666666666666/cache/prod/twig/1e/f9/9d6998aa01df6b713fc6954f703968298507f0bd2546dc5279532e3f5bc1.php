<?php

/* BBidsBBidsHomeBundle:Categoriesform:sports_swimming_form.html.twig */
class __TwigTemplate_1ef99d6998aa01df6b713fc6954f703968298507f0bd2546dc5279532e3f5bc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BBidsBBidsHomeBundle:Categoriesform:sports_swimming_form.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'noindexMeta' => array($this, 'block_noindexMeta'),
            'banner' => array($this, 'block_banner'),
            'maincontent' => array($this, 'block_maincontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Easy Way To Sports & Swimming Lessons Quotes Here";
    }

    // line 3
    public function block_noindexMeta($context, array $blocks = array())
    {
        echo "<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("b_bids_b_bids_post_by_search_inline", array("categoryid" => (isset($context["catid"]) ? $context["catid"] : null))), "html", null, true);
        echo "\" />";
    }

    // line 4
    public function block_banner($context, array $blocks = array())
    {
        // line 5
        echo "
";
    }

    // line 8
    public function block_maincontent($context, array $blocks = array())
    {
        // line 9
        echo "
<style>
    .pac-container:after{
        content:none !important;
    }
</style>
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/nprogress.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/validationEngine.jquery.css"), "html", null, true);
        echo "\" type=\"text/css\"/>
<script type=\"text/javascript\">
  function checkForOption (field, rules, i, options) {
    if(field.prev(':checkbox').is(\":checked\"))
    {
      console.log('called')
      rules.push('required');
      return \"Please fill in the other field\";
    }
  }
</script>
<div class=\"container category-search\">
    <div class=\"category-wrapper\">
        ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "
        <div class=\"alert alert-danger\">";
            // line 32
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
        ";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 37
            echo "
        <div class=\"alert alert-success\">";
            // line 38
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 42
            echo "
        <div class=\"alert alert-success\">";
            // line 43
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "    </div>
<div class=\"cat-form\">
  <div class=\"photo_form_one_area modal1\">
    <h1>Looking for Sports & Swimming Lessons in the UAE</h1>
    <p>Have a Sports & Swimming lessons requirement ? Access competitive quotes from experienced service professionals in Dubai now by completing the short form below.
      <br>OR<br><br><span class=\"call-us\">CALL (04) 4213777</span></p>
  </div>

        ";
        // line 54
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "category_form")));
        echo "
        <div id=\"step1\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 1 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Education-1.png"), "html", null, true);
        echo "\" alt=\"education\">
                  </div>
                  ";
        // line 93
        $context["oddoreven"] = "even";
        // line 94
        echo "                  ";
        $context["subcateriesLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategories", array())) - 1);
        // line 95
        echo "                  ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategories", array())) % 2 == 1)) {
            // line 96
            echo "                    ";
            $context["oddoreven"] = "odd";
            // line 97
            echo "                  ";
        }
        // line 98
        echo "                  <div class=\"inner_photo_one_form_table\">
                      <h3>What sport doe you require lessons in?</h3>
                      <div class=\"child_pro_tabil_main kindofService\">
                          ";
        // line 101
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategories", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 102
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 103
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"child_pho_table_left_inner_text\" >
                                    ";
                // line 105
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                    ";
                // line 106
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "subcategory_other", array()), 'widget', array("attr" => array("class" => "subcategory_other validate[condRequired[categories_form_subcategories_7]]")));
                echo "
                                    <h5>(max 30 characters)</h5>
                                </div>
                              </div>
                            ";
            } else {
                // line 111
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"";
                // line 112
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["subcateriesLength"]) ? $context["subcateriesLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                    ";
                // line 113
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                                </div>
                              </div>
                            ";
            }
            // line 117
            echo "                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "                          ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 119
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                           </div>
                          ";
        }
        // line 123
        echo "                          <div class=\"error\" id=\"kind_service\"></div>
                      </div>
                  <div class=\"photo_contunue_btn proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue1\">CONTINUE</a>
                  </div>
                  </div>
              </div>
          </div>
        </div>


        ";
        // line 135
        echo "        <div id=\"step2\" style=\"display:none\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 2 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 168
        $context["oddoreven"] = "even";
        // line 169
        echo "          ";
        $context["coachingLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_for", array())) - 1);
        // line 170
        echo "          ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_for", array())) % 2 == 1)) {
            // line 171
            echo "            ";
            $context["oddoreven"] = "odd";
            // line 172
            echo "          ";
        }
        // line 173
        echo "          <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Sports-2.png"), "html", null, true);
        echo "\" alt=\"Sports\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                    <h3>Is the coaching for?</h3>
                    <div class=\"child_pro_tabil_main\">
                      ";
        // line 181
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_for", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 182
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 183
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"child_pho_table_left_inner_text\" >
                                    ";
                // line 185
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                    ";
                // line 186
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_for_other", array()), 'widget', array("attr" => array("class" => "acedemic_other validate[condRequired[categories_form_coaching_for_1]]", "placeholder" => "Multiple")));
                echo "
                                    <h5>(max 30 characters)</h5>
                                </div>
                              </div>
                            ";
            } else {
                // line 191
                echo "                              <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                                <div class=\"";
                // line 192
                if ((((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "even") && ((isset($context["coachingLength"]) ? $context["coachingLength"] : null) == $this->getAttribute($context["loop"], "index", array())))) {
                    echo "child_pho_table_left_inner_text";
                } else {
                    echo "child_pho_table_left_inner";
                }
                echo "\" >
                                    ";
                // line 193
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                                </div>
                              </div>
                            ";
            }
            // line 197
            echo "                          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 198
        echo "                          ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 199
            echo "                            <div class=\"child_pho_table_right\">
                               <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                           </div>
                          ";
        }
        // line 203
        echo "                  </div>
                </div>
          </div>
        </div>

        ";
        // line 208
        $context["oddoreven"] = "even";
        // line 209
        echo "        ";
        $context["coachingTypeLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_type", array())) - 1);
        // line 210
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_type", array())) % 2 == 1)) {
            // line 211
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 212
            echo "        ";
        }
        // line 213
        echo "        <div class=\"photo_one_form\">
          <div class=\"inner_photo_one_form subjects\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Sports-3.png"), "html", null, true);
        echo "\" alt=\"Sports\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>Is the coaching for?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 221
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_type", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 222
            echo "                      ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 223
                echo "                        <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                          <div class=\"child_pho_table_left_inner_text\" >
                              ";
                // line 225
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                              ";
                // line 226
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coaching_type_other", array()), 'widget', array("attr" => array("class" => "subject_other validate[condRequired[categories_form_subjects_3]]")));
                echo "
                              <h5>(max 30 characters)</h5>
                          </div>
                        </div>
                      ";
            } else {
                // line 231
                echo "                        <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                          <div class=\"child_pho_table_left_inner\" >
                              ";
                // line 233
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                          </div>
                        </div>
                      ";
            }
            // line 237
            echo "                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 238
        echo "                    ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 239
            echo "                      <div class=\"child_pho_table_right\">
                         <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                     </div>
                    ";
        }
        // line 243
        echo "                </div>
              </div>
          </div>
        </div>

        ";
        // line 248
        $context["oddoreven"] = "even";
        // line 249
        echo "        ";
        $context["traineeAgeLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "trainee_age", array())) - 1);
        // line 250
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "trainee_age", array())) % 2 == 1)) {
            // line 251
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 252
            echo "        ";
        }
        // line 253
        echo "        <div class=\"photo_one_form\">
          <div class=\"inner_photo_one_form musicType\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Sports-4.png"), "html", null, true);
        echo "\" alt=\"Sports\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>What is the age range of the person(s)?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 261
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "trainee_age", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 262
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 263
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 265
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 266
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "music_type_other", array()), 'widget', array("attr" => array("class" => "music_other validate[condRequired[categories_form_music_type_4]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 271
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner\" >
                                  ";
                // line 273
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 277
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 278
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 279
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 283
        echo "                </div>
              </div>
          </div>
        </div>

        ";
        // line 288
        $context["oddoreven"] = "even";
        // line 289
        echo "        ";
        $context["frequencyLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "frequency", array())) - 1);
        // line 290
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "frequency", array())) % 2 == 1)) {
            // line 291
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 292
            echo "        ";
        }
        // line 293
        echo "        <div class=\"photo_one_form\">
          <div class=\"inner_photo_one_form\">
              <div class=\"inner_photo_one_form_img\">
                  <img src=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/often.png"), "html", null, true);
        echo "\" alt=\"often icon\">
              </div>
              <div class=\"inner_photo_one_form_table\">
                  <h3>How often do you require coaching?</h3>
                  <div class=\"child_pro_tabil_main\">
                    ";
        // line 301
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "frequency", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 302
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 303
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 305
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 306
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "frequency_other", array()), 'widget', array("attr" => array("class" => "validate[condRequired[categories_form_dan_sing_type_3]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 311
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner\" >
                                  ";
                // line 313
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 317
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 318
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 319
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 323
        echo "                    <div class=\"error\" id=\"danSingType\"></div>
                </div>
              </div>
          </div>
        </div>

        ";
        // line 329
        $context["oddoreven"] = "even";
        // line 330
        echo "        ";
        $context["travelLength"] = (twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "travel", array())) - 1);
        // line 331
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "travel", array())) % 2 == 1)) {
            // line 332
            echo "          ";
            $context["oddoreven"] = "odd";
            // line 333
            echo "        ";
        }
        // line 334
        echo "        <div class=\"photo_one_form\">
            <div class=\"inner_photo_one_form\">
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 337
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/Sports-5.png"), "html", null, true);
        echo "\" alt=\"travel\">
                </div>
                <div class=\"inner_photo_one_form_table\">
                  <h3>Are you willing to travel to other areas for the coaching?</h3>
                    ";
        // line 341
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "travel", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 342
            echo "                          ";
            if (($this->getAttribute($this->getAttribute($context["child"], "vars", array()), "label", array()) == "Other")) {
                // line 343
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner_text\" >
                                  ";
                // line 345
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo "
                                  ";
                // line 346
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "travel_other", array()), 'widget', array("attr" => array("class" => "validate[condRequired[categories_form_travel_3]]")));
                echo "
                                  <h5>(max 30 characters)</h5>
                              </div>
                            </div>
                          ";
            } else {
                // line 351
                echo "                            <div class=\"child_pho_table_";
                echo twig_escape_filter($this->env, twig_cycle(array(0 => "right", 1 => "left"), $this->getAttribute($context["loop"], "index", array())), "html", null, true);
                echo "\">
                              <div class=\"child_pho_table_left_inner\" >
                                  ";
                // line 353
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => "validate[minCheckbox[1]]")));
                echo " ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
                echo "
                              </div>
                            </div>
                          ";
            }
            // line 357
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 358
        echo "                        ";
        if (((isset($context["oddoreven"]) ? $context["oddoreven"] : null) == "odd")) {
            // line 359
            echo "                          <div class=\"child_pho_table_right\">
                             <div class=\"child_pho_table_left_inner_text innertext2\"></div>
                         </div>
                        ";
        }
        // line 363
        echo "                  <div class=\"inner_photo_one_form_table\">
                    <div class=\"proceed\">
                        <a href=\"javascript:void(0)\" id=\"continue2\" class=\"button3\"><button type=\"button\">CONTINUE</button></a>
                    </div>
                    <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback1\">BACK</a></div>
                  </div>
                </div>
            </div>
        </div>
      </div>
      ";
        // line 374
        echo "      ";
        // line 375
        echo "         <div id=\"step3\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                  <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <p>Page 3 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          ";
        // line 408
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array(), "any", true, true)) {
            // line 409
            echo "          <div class=\"photo_form_main\">
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form name\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 413
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/name.png"), "html", null, true);
            echo "\" alt=\"Name\">
                  </div>
                  <div class=\"inner_photo_one_form_table\" >
                      <h3>";
            // line 416
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'label');
            echo "</h3>
                          ";
            // line 417
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "contactname", array()), 'widget', array("attr" => array("class" => "form-input name validate[required,custom[onlyLetterSp]]")));
            echo "
                      <div class=\"error\" id=\"name\" ></div>
                  </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form  email\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 425
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/email.png"), "html", null, true);
            echo "\" alt=\"Email\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 428
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label');
            echo " </h3>
                     ";
            // line 429
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-input email_id validate[required,custom[email]]")));
            echo "
                     <div class=\"error\" id=\"email\" ></div>
                  </div>
              </div>
          </div>
          ";
        }
        // line 435
        echo "          <div class=\"photo_one_form\">
              <div class=\"inner_photo_one_form location\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
        // line 438
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location.png"), "html", null, true);
        echo "\" alt=\"Location\">
                  </div>
                  <div class=\"inner_photo_one_form_table res2\" >
                      <h3>Select your Location*</h3>
                      <div class=\"child_pro_tabil_main res21\">
                          <div class=\"child_accounting_three_left\">
                              ";
        // line 444
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'widget', array("attr" => array("class" => "land_location validate[minCheckbox[1]]")));
        echo "
                                  <p>";
        // line 445
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 0, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"child_accounting_three_right\">
                             ";
        // line 448
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'widget', array("attr" => array("class" => "land_location validate[minCheckbox[1]]")));
        echo "
                                  <p>";
        // line 449
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locationtype", array()), "children", array()), 1, array(), "array"), 'label', array("label_attr" => array("class" => "location-type")));
        echo "</p>
                          </div>
                          <div class=\"error\" id=\"location\"></div>
                      </div>
                  </div>
              </div>
          </div>


              ";
        // line 458
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array(), "any", true, true)) {
            echo " <div class=\"photo_one_form for-uae \">
              <div class=\"inner_photo_one_form mask domcity_cont\" id=\"domCity\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
            // line 462
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                    <h3>";
            // line 465
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label');
            echo "</h3>
                    <div class=\"child_pro_tabil_main city_cont\">
                      ";
            // line 467
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "validate[condRequired[categories_form_locationtype_0]]")));
            echo "
                    </div>
                  </div>
                </div>
                <div class=\"error\" id=\"acc_city\"></div>
              </div>
              ";
        }
        // line 474
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array(), "any", true, true)) {
            // line 475
            echo "              <div class=\"inner_photo_one_form mask\" id=\"domArea\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 477
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/location icon.png"), "html", null, true);
            echo "\" alt=\"Location Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 480
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'label');
            echo "</h3>
                     ";
            // line 481
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "location", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_0]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_area\"></div>
              </div>
              ";
        }
        // line 486
        echo "
              ";
        // line 487
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array(), "any", true, true)) {
            // line 488
            echo "              <div class=\"mask\" id=\"domContact\" >
                <div class=\"inner_photo_one_form\" style=\"margin-top: 40px;\" id=\"uae_mobileno\">
                    <div class=\"inner_photo_one_form_img\">
                        <img src=\"";
            // line 491
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                    </div>
                    <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                        <h3>";
            // line 494
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'label');
            echo "</h3>


                        <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                        ";
            // line 498
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecode", array()), 'widget', array("attr" => array("class" => "validate[condRequired[categories_form_locationtype_0]]")));
            echo "
                        ";
            // line 499
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_0],custom[number],minSize[6]]")));
            echo "

                    </div>
                    <div class=\"error\" id=\"acc_mobile\"></div>
                </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top: 40px;\" >
                        <div class=\"inner_photo_one_form_img\">
                            <img src=\"";
            // line 506
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                        </div>
                        <div id=\"int-country\" class=\"inner_photo_one_form_table\">
                            <h3>";
            // line 509
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'label');
            echo "</h3>


                             <label style=\"margin-top:-4px;\"><b>Area Code</b></label>
                            ";
            // line 513
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecode", array()), 'widget', array("attr" => array("class" => "validate[optional]")));
            echo "
                            ";
            // line 514
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephone", array()), 'widget', array("attr" => array("class" => "form-input validate[optional,custom[number]]")));
            echo "

                        </div>
                    </div>
              </div></div>
                    ";
        }
        // line 520
        echo "

            <div class=\"photo_one_form for-foreign\">
              ";
        // line 523
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array(), "any", true, true)) {
            // line 524
            echo "              <div class=\"inner_photo_one_form mask intCountry_cont\" id=\"intCountry\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 526
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/international icon.png"), "html", null, true);
            echo "\" alt=\"international Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 529
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'label');
            echo "</h3>
                     ";
            // line 530
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_1]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcountry\"></div>
              </div>
              <div class=\"inner_photo_one_form mask\" id=\"intCity\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 536
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/city icon.png"), "html", null, true);
            echo "\" alt=\"City Icon\">
                  </div>
                  <div class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 539
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'label');
            echo "</h3>
                      ";
            // line 540
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cityint", array()), 'widget', array("attr" => array("class" => "form-input validate[condRequired[categories_form_locationtype_1],custom[onlyLetterSp]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intcity\"></div>
              </div>
              ";
        }
        // line 545
        echo "              ";
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array(), "any", true, true)) {
            // line 546
            echo "              <div class=\"mask\" id=\"intContact\">
                <div class=\"inner_photo_one_form\" style=\"margin-top:40px;\" id=\"int_mobileno\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 549
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/mobile icon.png"), "html", null, true);
            echo "\" alt=\"Mobile Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 552
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 554
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobilecodeint", array()), 'widget', array("attr" => array("class" => "form-control validate[condRequired[categories_form_locationtype_1],custom[number]]")));
            echo "
                      ";
            // line 555
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobileint", array()), 'widget', array("attr" => array("class" => "form-input1 validate[condRequired[categories_form_locationtype_1],custom[number],minSize[6]]")));
            echo "
                  </div>
                  <div class=\"error\" id=\"acc_intmobile\"></div>
              </div>
              <div class=\"inner_photo_one_form ph_cont\" style=\"margin-top:40px;\">
                  <div class=\"inner_photo_one_form_img\">
                      <img src=\"";
            // line 561
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/phone icon.png"), "html", null, true);
            echo "\" alt=\"Phone Icon\">
                  </div>
                  <div id=\"int-foreign\" class=\"inner_photo_one_form_table\">
                      <h3>";
            // line 564
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'label');
            echo "</h3>
                     <label style=\"margin-top:-4px;\"><b>Area Code &nbsp;&nbsp;&nbsp;+</b></label>
                      ";
            // line 566
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homecodeint", array()), 'widget', array("attr" => array("class" => "form-control validate[optional,custom[number]]")));
            echo "
                      ";
            // line 567
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "homephoneint", array()), 'widget', array("attr" => array("class" => "form-input1 validate[optional,custom[number]]")));
            echo "
                  </div>
              </div>
             </div>
              ";
        }
        // line 572
        echo "            </div>

            <div class=\"photo_one_form\" id=\"hireBlock\">
              <div class=\"inner_photo_one_form hire_cont\">
                <div class=\"inner_photo_one_form\" style=\"margin-bottom: 40px;\" >
                  <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 578
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/looking to hire.png"), "html", null, true);
        echo "\" alt=\"Looking To Hire\">
                  </div>
                  <div class=\"inner_photo_one_form_table resp1\">
                    <h3>When are you looking to hire?*</h3>
                    <div class=\"child_pro_tabil_main  \">
                      ";
        // line 583
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hire", array()), 'widget');
        echo "
                    </div>
                  </div>
                </div>
                <div class=\"inner_photo_one_form_img\">
                    <img src=\"";
        // line 588
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/information.png"), "html", null, true);
        echo "\" alt=\"Information\">
                </div>
                <div class=\"inner_photo_one_form_table resp1\">
                    <h3>Is there any other additional information you would like to provide?</h3>
                    ";
        // line 592
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget', array("attr" => array("class" => "validate[optional,custom[noSpecialCaracters]]")));
        echo "
                     <br> <h5 class=\"char\">(max 120 characters)</h5>
                </div>

                <div class=\"inner_photo_one_form_table\">
                  <div class=\"proceed\">
                      <a href=\"javascript:void(0);\" id=\"continue3\" class=\"button3\">";
        // line 598
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postJob", array()), 'widget', array("label" => "CONTINUE", "attr" => array("class" => "")));
        echo "</a>
                  </div>
                  <div class=\"photo_contunue_btn_back1 goback\"><a href=\"javascript:void(0);\"id=\"goback2\">BACK</a></div>
                </div>
              </div>
          </div>
        </div>
        </div>
      ";
        // line 607
        echo "      ";
        // line 608
        echo "        <div id=\"step4\" style=\"display:none;\">
          <div class=\"infograghy_photo_one\">
              <div class=\"inner_infograghy_photo_one \">
              <div class=\"inner_child_info_photo_area_hr\"></div>
                 <div class=\"inner_child_info_photo_area\">
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>1. What sort of services do you require?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>2. Tell us a bit more about your requirement</p>
                      </div>
                      <div class=\"child_infograghy_photo_one\">
                          <div class=\"info_circel\"></div>
                          <p>3. How would you like to be contacted?</p>
                      </div>
                      <div class=\"child_infograghy_photo_one_active\">
                          <div class=\"info_circel_active modal3\"></div>
                          <p>4. Done</p>
                      </div>
                 </div>
                 <div class=\"inner_child_info_photo_area mobile\">
                    <div class=\"child_mobile_ingography\">
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_inactive\"></div>
                        <div class=\"info_circel_actived\"></div>
                        <p>Page 4 of 4</p>
                    </div>
               </div>
              </div>
          </div>
          <div class=\"photo_one_form\">
            <h3 class=\"thankyou-heading\"><b>Thank you, you’re done!</b></h3>
              <p class=\"thankyou-content\" id=\"enquiryResult\">Your job request has been logged successfully and your request number is processing. The most suitable professionals will now contact you for a quote, compare them and hire the best pro!</p>
              <p class=\"thankyou-content\">If you thought BusinessBid was useful to you, why not share the love? We’d greatly appreciate it. Like us on
              Facebook or follow us on Twitter to to keep updated on all the news and best deals.</p>
              <div class=\"social-icon\">
                <a href=\"https://twitter.com/\"><img src=\"";
        // line 647
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/twitter.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Twitter Bird\"/></a>
                <a href=\"https://www.facebook.com/\"><img src=\"";
        // line 648
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" rel=\"nofollow\" alt=\"Facebook Sign\" /></a>
              </div>
              <div id=\"thankyou-btn\"  class=\"inner_photo_one_form\">
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 652
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\">LOG ANOTHER REQUEST</a>
              </div>
              <div class=\"photo_contunue_btn11\">
                  <a href=\"";
        // line 655
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" >CONTINUE TO HOMEPAGE</a>
              </div>
              </div>
          </div>
        </div>
        ";
        // line 661
        echo "           ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
</div>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&dummy=.js\"></script>
<script>
    var input = document.getElementById('categories_form_location');
    var options = {componentRestrictions: {country: 'AE'}};
    new google.maps.places.Autocomplete(input, options);
</script>
<script type=\"text/javascript\" src=\"";
        // line 669
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/nprogress.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 670
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.validationEngine-en.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
<script src=\"";
        // line 671
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.validationEngine.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
<script type=\"text/javascript\" src=\"";
        // line 672
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/reviewsformsvalidation/reviewsform.general.js"), "html", null, true);
        echo "\" ></script>
<script>
function scrollBox(tag) {
    \$('html,body').animate({
        scrollTop: tag.position().top - 10
    }, 'slow');
}
\$(document).ready(function() {
  var validationFlag = true;
  // binds form submission and fields to the validation engine
  \$(\"#category_form\").validationEngine({
      ignore: \":hidden\"
  });
  \$('div.proceed').click(function() {
      var clickedStep = \$(this).children(\"a:first\").attr('id'),
          \$step = clickedStep.match(/\\d+/);

      if (\$step) {
          if (\$step == \"1\") {
            valid = \$(\"#category_form\").validationEngine('validate');
            if (valid == false) {
                \$(\"#category_form\").validationEngine({
                    ignore: \":hidden\"
                });
                return false;
            }
          } else if (\$step == \"2\") {

              var valid = \$(\"#category_form\").validationEngine('validate');
              if (valid == false) {
                  \$(\"#category_form\").validationEngine({
                      ignore: \":hidden\"
                  });
                  return false;
              }
          } else if (\$step == \"3\") {
            validationFlag = false;
              valid = \$(\"#category_form\").validationEngine('validate');
              if (valid == false) {
                  \$(\"#category_form\").validationEngine({
                      ignore: \":hidden\"
                  });
                  return false;
              }
              validationFlag = true;
              //\$(\"#category_form\").trigger( \"submit\" );
          }

          \$(\"div#step\" + \$step).hide();
          \$step++;
          \$(\"#category_form\").validationEngine('attach');
          \$(\"div#step\" + \$step).show();
          scrollBox(\$(\"div#step\" + \$step));
          return true;
      }
  });
  \$(\"div.goback\").click(function(){
      var gobackStep = \$(this).children(\"a:first\").attr('id'), \$step = gobackStep.match(/\\d+/);
      \$(\"div#step\"+\$step).show();
      \$step++;
      \$(\"div#step\"+\$step).hide();
  });
  ";
        // line 735
        echo "  var forms = [
    '[ name=\"";
        // line 736
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "full_name", array()), "html", null, true);
        echo "\"]'
  ];

  \$(forms.join(',')).submit(function(e) {
      e.preventDefault();
      console.log(\"tried submitting\")
      if (validationFlag) {
          postForm(\$(this), function(response) {
              if (response.hasOwnProperty('error')) {
                  alert(response.error);
                  var i = 1;
                  for (i = 1; i < 5; i++) {
                      \$(\"div#step\" + i).hide();
                  };
                  \$(\"div#step\" + response.step).show();
              } else {
                var form=document.createElement(\"form\");form.setAttribute(\"method\",\"post\"),form.setAttribute(\"action\",'";
        // line 752
        echo $this->env->getExtension('routing')->getPath("bizbids_job_post_sucessful");
        echo "');var hiddenField=document.createElement(\"input\");hiddenField.setAttribute(\"name\",\"jobno\"),hiddenField.setAttribute(\"value\",response.jobNo),hiddenField.setAttribute(\"type\",\"hidden\"),form.appendChild(hiddenField),document.body.appendChild(form),form.submit();
                \$(\"#category_form\")[0].reset();
              }
          });
          ga('send', 'event', 'Button', 'Click', 'Form Sent');
      }
      return false;
  });

  function postForm(\$form, callback) {
      NProgress.start();
      \$.ajax({
          type: \$form.attr('method'),
          beforeSend: function() {
              NProgress.inc()
          },
          url: \$form.attr('action'),
          data: \$form.serialize(),
          success: function(data) {
              callback(data);
              NProgress.done();
          },
          error: function(xhr) {
              alert(\"Error occured.please try again\");
              NProgress.done();
          }
      });
  }

});
</script>

";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Categoriesform:sports_swimming_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1481 => 752,  1462 => 736,  1459 => 735,  1394 => 672,  1390 => 671,  1386 => 670,  1382 => 669,  1370 => 661,  1362 => 655,  1356 => 652,  1349 => 648,  1345 => 647,  1304 => 608,  1302 => 607,  1291 => 598,  1282 => 592,  1275 => 588,  1267 => 583,  1259 => 578,  1251 => 572,  1243 => 567,  1239 => 566,  1234 => 564,  1228 => 561,  1219 => 555,  1215 => 554,  1210 => 552,  1204 => 549,  1199 => 546,  1196 => 545,  1188 => 540,  1184 => 539,  1178 => 536,  1169 => 530,  1165 => 529,  1159 => 526,  1155 => 524,  1153 => 523,  1148 => 520,  1139 => 514,  1135 => 513,  1128 => 509,  1122 => 506,  1112 => 499,  1108 => 498,  1101 => 494,  1095 => 491,  1090 => 488,  1088 => 487,  1085 => 486,  1077 => 481,  1073 => 480,  1067 => 477,  1063 => 475,  1060 => 474,  1050 => 467,  1045 => 465,  1039 => 462,  1032 => 458,  1020 => 449,  1016 => 448,  1010 => 445,  1006 => 444,  997 => 438,  992 => 435,  983 => 429,  979 => 428,  973 => 425,  962 => 417,  958 => 416,  952 => 413,  946 => 409,  944 => 408,  909 => 375,  907 => 374,  895 => 363,  889 => 359,  886 => 358,  872 => 357,  863 => 353,  857 => 351,  849 => 346,  845 => 345,  839 => 343,  836 => 342,  819 => 341,  812 => 337,  807 => 334,  804 => 333,  801 => 332,  798 => 331,  795 => 330,  793 => 329,  785 => 323,  779 => 319,  776 => 318,  762 => 317,  753 => 313,  747 => 311,  739 => 306,  735 => 305,  729 => 303,  726 => 302,  709 => 301,  701 => 296,  696 => 293,  693 => 292,  690 => 291,  687 => 290,  684 => 289,  682 => 288,  675 => 283,  669 => 279,  666 => 278,  652 => 277,  643 => 273,  637 => 271,  629 => 266,  625 => 265,  619 => 263,  616 => 262,  599 => 261,  591 => 256,  586 => 253,  583 => 252,  580 => 251,  577 => 250,  574 => 249,  572 => 248,  565 => 243,  559 => 239,  556 => 238,  542 => 237,  533 => 233,  527 => 231,  519 => 226,  515 => 225,  509 => 223,  506 => 222,  489 => 221,  481 => 216,  476 => 213,  473 => 212,  470 => 211,  467 => 210,  464 => 209,  462 => 208,  455 => 203,  449 => 199,  446 => 198,  432 => 197,  423 => 193,  415 => 192,  410 => 191,  402 => 186,  398 => 185,  392 => 183,  389 => 182,  372 => 181,  364 => 176,  359 => 173,  356 => 172,  353 => 171,  350 => 170,  347 => 169,  345 => 168,  310 => 135,  297 => 123,  291 => 119,  288 => 118,  274 => 117,  265 => 113,  257 => 112,  252 => 111,  244 => 106,  240 => 105,  234 => 103,  231 => 102,  214 => 101,  209 => 98,  206 => 97,  203 => 96,  200 => 95,  197 => 94,  195 => 93,  190 => 91,  150 => 54,  140 => 46,  131 => 43,  128 => 42,  123 => 41,  114 => 38,  111 => 37,  107 => 36,  104 => 35,  95 => 32,  92 => 31,  88 => 30,  72 => 17,  68 => 16,  64 => 15,  56 => 9,  53 => 8,  48 => 5,  45 => 4,  37 => 3,  31 => 2,  11 => 1,);
    }
}

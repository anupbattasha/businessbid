<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Vendor:paymentsuccessemail.html.twig */
class __TwigTemplate_68bb9cb7104a8aa677dbb13a2c20d2c71197fe241e6157820f6522c2e2e56a40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">Dear ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ",</p>
   
            <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; margin-top:0px; width:100%; \">Thank you for your purchase. Your job request number is  ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["OrderNumber"]) ? $context["OrderNumber"] : null), "html", null, true);
        echo " .</p>

      
            <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; margin-top:0px; width:100%; \">
                Our administrative teams will activate your purchase within 48 hours and you will start receiving new customer leads very soon to keep you busy!
            </P>
           
            <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">
                Please be assured that we will always strive to serve you better.
            </P>
            
            <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">
                If you have any questions or need help call us on 04 42 13 777.
            </P>
        
            <p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">Regards
                <br>
            BusinessBid</p>
     
</p>
";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Vendor:paymentsuccessemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 3,  19 => 1,);
    }
}

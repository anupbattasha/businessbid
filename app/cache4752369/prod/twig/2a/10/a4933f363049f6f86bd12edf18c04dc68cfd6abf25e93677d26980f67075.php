<?php

/* ::newconsumer_dashboard.html.twig */
class __TwigTemplate_2a10a4933f363049f6f86bd12edf18c04dc68cfd6abf25e93677d26980f67075 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'jquery' => array($this, 'block_jquery'),
            'customcss' => array($this, 'block_customcss'),
            'header' => array($this, 'block_header'),
            'menu' => array($this, 'block_menu'),
            'topmenu' => array($this, 'block_topmenu'),
            'pageblocknew' => array($this, 'block_pageblocknew'),
            'megamenu' => array($this, 'block_megamenu'),
            'leftmenutab' => array($this, 'block_leftmenutab'),
            'enquiries' => array($this, 'block_enquiries'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<head>
\t<title>Business BID</title>
\t<meta name=\"google-site-verification\" content=\"QAYmcKY4C7lp2pgNXTkXONzSKDfusK1LWOP1Iqwq-lk\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">
\t<script src=\"http://code.jquery.com/jquery-1.10.2.min.js\"></script>
\t<!--<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>-->
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover-min.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/hover.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
\t<link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/eticket.css"), "html", null, true);
        echo "\">
\t<!--<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>-->
\t<!--[if lt IE 9]>
  \t<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/html5shiv.min.js"), "html", null, true);
        echo "\"></script>
  \t<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.min.js"), "html", null, true);
        echo "\"></script>
\t<![endif]-->
\t\t<!--[if lt IE 9]>
\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/respond.src.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>

\t<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js\"></script>
\t<script>
\t\$(document).ready(function(){

\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$('#category').autocomplete({
\t      \tsource: function( request, response ) {
\t      \t\$.ajax({
\t      \turl : realpath+\"ajax-info.php\",
\t      \tdataType: \"json\",
\tdata: {
\t   name_startsWith: request.term,
\t   type: 'category'
\t},
\t success: function( data ) {
\t response( \$.map( data, function( item ) {

\treturn {
\tlabel: item,
\tvalue: item

\t}

\t}));
\t},
\tselect: function(event, ui) {
                               alert( \$(event.target).val() );
                            }
\t      \t});
\t      \t},
\t      \tautoFocus: true,
\t      \tminLength: 2

\t      });

});
\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_subcategory\").html('<option value=\"0\">Select</option>');
\t\$(\"#form_category\").on('change',function (){
\tvar ax = \t\$(\"#form_category\").val();

\t\$.ajax({url:realpath+\"pullSubCategoryByCategory.php?categoryid=\"+ax,success:function(result){

\t\$(\"#form_subcategory\").html(result);
\t}});
\t});

});

\t\$(document).ready(function(){
\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\t\$(\"#form_email\").on('input',function (){
\tvar ax = \t\$(\"#form_email\").val();
\tif(ax==''){
\t\$(\"#emailexists\").text('');
\t}
\tif(ax.indexOf('@') > -1 ) {

\t\$.ajax({url:realpath+\"checkEmail.php?emailid=\"+ax,success:function(result){

\tif(result == \"exists\") {
\t\$(\"#emailexists\").text('Email address already exists!');
\t} else {
\t\$(\"#emailexists\").text('');
\t}
\t}});
\t}
\t});

});

\t\$(document).ready(function(){
\t\$('#form_description').attr(\"maxlength\",\"300\");
\t\$('#form_location').attr(\"maxlength\",\"30\");

\t});

</script>
\t<script>
\t\t\$(document).ready(function() {
\t\t\tsetTimeout(function() {
\t\t\t\t// Slide

\t\t\t\t\$('div.example_menu > div > a.expanded + ul').slideToggle('medium');
\t\t\t\t\$('div.example_menu > div > a').click(function() {
\t\t\t\t\t\$('div.example_menu > div > a.expanded').not(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
\t\t\t\t\t\$(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
\t\t\t\t});
\t\t\t}, 250);
\t\t});
\t</script>


\t<script type='text/javascript' >

\tfunction jsfunction(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";

\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//~ alert(xmlhttp.responseText);
\tdocument.getElementById(\"searchval\").innerHTML=xmlhttp.responseText;
\tmegamenublogAjax();
\tmegamenubycatAjax();
\tmegamenubycatVendorSearchAjax();
\tmegamenubycatVendorReviewAjax();
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"ajax-info.php\",true);
\txmlhttp.send();



\t}
\tfunction megamenublogAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t\t\t\t\t\t\t\t\t//document.getElementById(\"megamenu1\").innerHTML=xmlhttp.responseText;
\t\t//~ var mvsa = document.getElementById(\"megamenu1Anch\");
\t\t//~ for (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t//~ if (mvsa.childNodes[i].className == \"dropdown-menu Resourcesegment-drop\") {
\t\t\t\t//~ mvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  //~ break;
\t\t\t//~ }
\t\t//~ }
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menublog.php\",true);
\t//xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);

\txmlhttp.send();



\t}
\tfunction megamenubycatAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\tdocument.getElementById(\"megamenubycatUl\").innerHTML=xmlhttp.responseText;
\t}
\t  }
\txmlhttp.open(\"GET\",realpath+\"mega-menubycat.php\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorSearchAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();
\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t//alert(xmlhttp.responseText);
\t//console.log(xmlhttp.responseText);
\t\tvar mvsa = document.getElementById(\"megamenuVenSearchAnch\");
\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Searchsegment-drop\") {
\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t  break;
\t\t\t}
\t\t}
\t//document.getElementById(\"megamenuVenSearchAnch\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorSearch\",true);
\txmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorSearch\",true);
\txmlhttp.send();



\t}
\tfunction megamenubycatVendorReviewAjax(){


\tvar realpath = window.location.protocol + \"//\" + window.location.host + \"/\";
\tvar xmlhttp;
\tif (window.XMLHttpRequest)
\t  {// code for IE7+, Firefox, Chrome, Opera, Safari
\t  xmlhttp=new XMLHttpRequest();

\t  }
\telse
\t  {// code for IE6, IE5
\t  xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
\t  }
\txmlhttp.onreadystatechange=function()
\t  {
\t  if (xmlhttp.readyState==4 && xmlhttp.status==200)
\t{
\t\t//alert(xmlhttp.responseText);
\t\t\tvar mvsa = document.getElementById(\"megamenuVenReviewAnch\");
\t\t\tfor (var i = 0; i < mvsa.childNodes.length; i++) {
\t\t\t\t\tif (mvsa.childNodes[i].className == \"dropdown-menu Reviewsegment-drop\") {
\t\t\t\t\t\tmvsa.childNodes[i].innerHTML = xmlhttp.responseText;
\t\t\t\t\t  break;
\t\t\t\t\t}
\t\t\t}

\t\t//document.getElementById(\"megamenuVenReviewUl\").innerHTML = xmlhttp.responseText;
\t}
\t  }
\t  xmlhttp.open(\"GET\",realpath+\"vendorReviewDropdown.php?type=vendorReview\",true);
\t//xmlhttp.open(\"GET\",realpath+\"mega-menubycat.php?type=vendorReview\",true);
\txmlhttp.send();
\t}


</script>

<script type=\"text/javascript\">
function show_submenu(b, menuid)
{


\tdocument.getElementById(b).style.display=\"block\";
\tif(menuid!=\"megaanchorbycat\"){
\tdocument.getElementById(menuid).className = \"setbg\";
\t}
}

function hide_submenu(b, menuid)
{


\tsetTimeout(dispminus(b,menuid), 200);



\t//document.getElementById(b).onmouseover = function() { document.getElementById(b).style.display=\"block\"; }
\t//document.getElementById(b).onmouseout = function() { document.getElementById(b).style.display=\"none\"; }
\t//document.getElementById(b).style.display=\"none\";
}

function dispminus(subid,menuid){
\t//~ if(menuid!=\"megaanchorbycat\"){
\t//~ document.getElementById(menuid).className = \"unsetbg\";
\t//~ }
\t//~ document.getElementById(subid).style.display=\"none\";


\t}


\$(document).ready(function(){
  \$(\"#flip\").click(function(){
    \$(\"#panel\").slideToggle(\"slow\");
  });

});

 \$(document).ready(function(){
      \$(\".Searchsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Searchsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenSearchAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });
 \$(document).ready(function(){
      \$(\".Reviewsegment-drop\").mouseover(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#0e508e\");
      });

\t   \$(\".Reviewsegment-drop\").mouseout(function(){
        \$(\"#megamenuVenReviewAnch\").css(\"background-color\",\"#1e7cc8\");
      });
    });


</script>
";
        // line 356
        $this->displayBlock('jquery', $context, $blocks);
        // line 359
        echo "
";
        // line 360
        $this->displayBlock('customcss', $context, $blocks);
        // line 363
        echo "

</head>
<body>

";
        // line 368
        $context["uid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "uid"), "method");
        // line 369
        $context["pid"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "pid"), "method");
        // line 370
        $context["email"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "email"), "method");
        // line 371
        $context["name"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "name"), "method");
        // line 372
        echo "
";
        // line 373
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 374
            echo "<div class=\"top-nav-bar\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-6 contact-info\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-earphone fa-phone\"></span>
\t\t\t\t\t<span>(04) 42 13 777</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3 left-clear\">
\t\t\t\t\t<span class=\"contact-no\"><a href=\"";
            // line 383
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_contactus");
            echo "\">Contact Us</a></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-6 clear-right dash-right-top-links\">
\t\t\t\t<ul>
\t\t\t\t\t<li>Welcome, <span class=\"profile-name\">";
            // line 388
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "</span></li>
\t\t\t\t\t<li><a href=\"";
            // line 389
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_logout");
            echo "\">Logout</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

";
        }
        // line 397
        echo "
<div class=\"main_container\">
\t";
        // line 399
        $this->displayBlock('header', $context, $blocks);
        // line 408
        echo "\t";
        $this->displayBlock('menu', $context, $blocks);
        // line 469
        echo "

\t";
        // line 471
        $this->displayBlock('pageblocknew', $context, $blocks);
        // line 537
        echo "


\t\t";
        // line 540
        $this->displayBlock('enquiries', $context, $blocks);
        // line 565
        echo "</div>
    \t</div>
</div></div>
\t";
        // line 568
        $this->displayBlock('footer', $context, $blocks);
        // line 577
        echo "<!-- Pure Chat Snippet -->
<script type=\"text/javascript\" data-cfasync=\"false\">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '07f34a99-9568-46e7-95c9-afc14a002834', f: true });done = true;}};})();</script>
<!-- End Pure Chat Snippet -->
</body>
<script type=\"text/javascript\">
//window.onload = jsfunction();
</script>
</html>
";
    }

    // line 356
    public function block_jquery($context, array $blocks = array())
    {
        // line 357
        echo "
";
    }

    // line 360
    public function block_customcss($context, array $blocks = array())
    {
        // line 361
        echo "
";
    }

    // line 399
    public function block_header($context, array $blocks = array())
    {
        // line 400
        echo "\t<div class=\"header container\">
    \t<div class=\"col-md-6 logo\">
        \t<a href=\"";
        // line 402
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\" > <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"> </a>
        </div>
        <div class=\"col-md-6\">
        </div>
    \t</div>
\t";
    }

    // line 408
    public function block_menu($context, array $blocks = array())
    {
        // line 409
        echo "\t<div class=\"menu_block\">
\t\t\t<div class=\"container\">
\t\t\t\t";
        // line 411
        $this->displayBlock('topmenu', $context, $blocks);
        // line 466
        echo "        \t</div>
  \t\t</div>
\t";
    }

    // line 411
    public function block_topmenu($context, array $blocks = array())
    {
        // line 412
        echo "\t\t\t\t\t<div class=\"container\">
\t<div class=\"row\">

        <nav class=\"navbar navbar-default\" role=\"navigation\">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\">
        <li class=\"active\"><a href=\"";
        // line 431
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_homepage");
        echo "\">Home</a></li>

        <li class=\"dropdown\">
          <a href=\"";
        // line 434
        echo $this->env->getExtension('routing')->getPath("bizbids_vendor_search");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Find Services Professionals<b class=\"caret\"></b></a>
          <ul class=\"dropdown-menu\">
            ";
        // line 436
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vendorSearch")));
        echo "
          </ul>
        </li>
         <li class=\"dropdown\">
          <a href=\"";
        // line 440
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_node4");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Vendor Review <b class=\"caret\"></b></a>
          <ul class=\"dropdown-menu\">
            ";
        // line 442
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BBidsBBidsHomeBundle:Menu:fetchMenu", array("typeOfMenu" => "vedorReview")));
        echo "
          </ul>
        </li>
      </ul>

      <ul class=\"nav navbar-nav navbar-right  col-md-5\">
         <form class=\"navbar-form navbar-left\" action=\"";
        // line 448
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_home_search");
        echo "\">
        <div class=\"form-group\">
          <input id='category' type=\"text\" class=\"form-control serice-required\" placeholder=\"Type the serive you require\"><datalist id=\"searchval\"></datalist>
        </div>
        <button type=\"submit\" class=\"btn btn-default serice-required-btn\">Search</button>
      </form>
      </ul>
    </div><!-- /.navbar-collapse -->

</nav>

\t</div>
</div>



\t\t\t\t\t</div>
\t\t\t\t";
    }

    // line 471
    public function block_pageblocknew($context, array $blocks = array())
    {
        // line 472
        echo "\t\t<div class=\"page-bg\">
    \t<div class=\"container inner_container\">
\t\t<div class=\"row \">
\t\t\t<div class=\"dashboard-panel\">


    <!-- mega menu div block starts -->
\t\t";
        // line 479
        $this->displayBlock('megamenu', $context, $blocks);
        // line 496
        echo "
\t";
        // line 497
        $this->displayBlock('leftmenutab', $context, $blocks);
        // line 534
        echo "

\t";
    }

    // line 479
    public function block_megamenu($context, array $blocks = array())
    {
        // line 480
        echo "\t\t<div id=\"megamenuVenSearch\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenSearch','megamenuVenSearchAnch');\" ><div><ul id=\"megamenuVenSearchUl\"></ul></div></div>
\t\t<div id=\"megamenuVenReview\" class=\"submenu menu-mega megamenubg-1 megamenubg\" onMouseOver=\"javascript:show_submenu('megamenuVenReview','megamenuVenReviewAnch');\" onMouseOut=\"javascript:hide_submenu('megamenuVenReview','megamenuVenReviewAnch');\"><div class=\"review-menu-left\"><ul id=\"megamenuVenReviewUl\"></ul></div><div  class=\"review-menu-right\"><button class=\"btn btn-success write-button\" type=\"button\" >Write a Review</button></div></div>
\t\t<ul id=\"megamenu1\" class=\"submenu menu-mega megamenubg\" onMouseOver=\"javascript:show_submenu('megamenu1','megamenu1Anch');\" onMouseOut=\"javascript:hide_submenu12('megamenu1','megamenu1Anch');\" ></ul>
\t\t<div id=\"megamenubycat\" class=\"submenu menu-megabycat megamenubg\" onMouseOver=\"javascript:show_submenu('megamenubycat','megaanchorbycat');\" onMouseOut=\"javascript:hide_submenu('megamenubycat','megaanchorbycat');\"><div><ul id=\"megamenubycatUl\"></ul></div><div></div></div>

\t\t";
        // line 485
        if ( !(null === (isset($context["uid"]) ? $context["uid"] : null))) {
            // line 486
            echo "
\t\t<script type=\"text/javascript\">

\t\tdocument.getElementById(\"megamenuVenSearch\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\t\tdocument.getElementById(\"megamenuVenReview\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\t\tdocument.getElementById(\"megamenu1\").setAttribute(\"class\", \"submenu menu-mega-login megamenubg\");
\t\tdocument.getElementById(\"megamenubycat\").setAttribute(\"class\", \"submenu menu-megabycat-login megamenubg\");
\t\t</script>
\t";
        }
        // line 495
        echo "\t\t";
    }

    // line 497
    public function block_leftmenutab($context, array $blocks = array())
    {
        // line 498
        echo "\t\t\t\t<div class=\"left-panel\">
\t\t\t\t\t<div class=\"left-tab-menu\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a class=\"user-dashboard\" href=\"";
        // line 501
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_home");
        echo "\">Customer Dashboard</a></li>
\t\t\t\t\t\t\t<li><a class=\"enquiries reviews\" href=\"";
        // line 502
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumer_postenquiries");
        echo "\">My Job Requests</a></li>
\t\t\t\t\t\t\t<li><a class=\"enquiry\" href=\"";
        // line 503
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_ratings");
        echo "\">Rating & Reviews</a></li>
\t\t\t\t\t\t\t<li><a class=\"account\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">My Account <span class=\"caret\"></span></a>
\t\t\t\t\t\t\t\t<ul id=\"collapseOne\" class=\"panel-collapse collapse menu-col\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 506
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumeraccount");
        echo "\">My Profile</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 507
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumerupdatepasssword");
        echo "\">Change Password</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 508
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumerupdatemail");
        echo "\">Update Email</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 509
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_consumerdeleteAccount");
        echo "\">Delete Account</a>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        // line 512
        if ((array_key_exists("emailCount", $context) && ((isset($context["emailCount"]) ? $context["emailCount"] : null) != 0))) {
            // line 513
            echo "                            <li>
                            \t<a class=\"messages1\" href=\"";
            // line 514
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_messages");
            echo "\">Messages<span class=\"mess-read\">NEW</span></a>
                            </li>
                            ";
        } else {
            // line 517
            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a class=\"messages\" href=\"";
            // line 518
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_messages");
            echo "\">Messages</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        }
        // line 521
        echo "\t\t\t\t\t\t\t<li><a class=\"support\" href=\"";
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_customer_ticket_home");
        echo "\">Support</a>

\t\t\t\t\t\t\t\t<!--<ul class=\"ticket-links\">
\t\t\t\t\t\t\t\t<li><a class=\"creat-ticket\" href=\"";
        // line 524
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_support_consumer");
        echo "\">Creat Ticket</a></li>
\t\t\t\t\t\t\t\t<li><a class=\"view-ticket\" href=\"";
        // line 525
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_supportview_consumer");
        echo "\">View Ticket</a></li>
\t\t\t\t\t\t\t\t</ul>-->
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"get-quote-link\"><a href=\"";
        // line 528
        echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_post_quote");
        echo "\">GET QUOTES NOW</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t";
    }

    // line 540
    public function block_enquiries($context, array $blocks = array())
    {
        // line 541
        echo "        \t<div class=\"col-md-9 dashboard-rightpanel\">
\t\t\t\t<div class=\"overview_block\">
\t\t\t\t\t";
        // line 543
        if (((isset($context["pid"]) ? $context["pid"] : null) == 3)) {
            // line 544
            echo "\t\t\t\t<div class=\"page-title\">
\t\t\t\t\t<h1>My Customer Account</h1>

\t\t\t\t\t\t\t<a class=\"switch-user-vendor\" href=\"";
            // line 547
            echo $this->env->getExtension('routing')->getPath("b_bids_b_bids_vendor_home");
            echo "\">Switch to My Vendor Profile</a>

\t\t\t\t</div>
\t\t\t\t";
        }
        // line 551
        echo "\t\t\t\t<div>
        \t<div class=\"dashboardmain_block\">
\t\t\t\t<h3>Welcome ";
        // line 553
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ", to your personalised BusinessBid Dashboard where you can:</h3>
\t\t\t\t<ul>
\t\t\t\t\t<li>View all the job requests you have logged</li>
\t\t\t\t\t<li>View all your posted vendor reviews</strong></li>
\t\t\t\t\t<li>Update your personal profile</li>
\t\t\t\t\t<li>View all communications and messages</li>
\t\t\t\t\t<li>Log any support issues</li>
\t\t\t\t\t<li>Log new job requests</li>
\t\t\t\t</ul>
        \t</div>
        \t</div>
\t\t";
    }

    // line 568
    public function block_footer($context, array $blocks = array())
    {
        // line 569
        echo "    \t<div class=\"footer-dashboard text-center\">
\t\t<div class=\"container\">
\t\t<div class=\"col-sm-6 footer-messages\">Copyright 2013 - 2014 BBusinessBid</div>
    \t<div class=\"col-sm-6 clear-right\"></div>

    \t</div>
\t\t</div>
    \t";
    }

    public function getTemplateName()
    {
        return "::newconsumer_dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  835 => 569,  832 => 568,  816 => 553,  812 => 551,  805 => 547,  800 => 544,  798 => 543,  794 => 541,  791 => 540,  781 => 528,  775 => 525,  771 => 524,  764 => 521,  758 => 518,  755 => 517,  749 => 514,  746 => 513,  744 => 512,  738 => 509,  734 => 508,  730 => 507,  726 => 506,  720 => 503,  716 => 502,  712 => 501,  707 => 498,  704 => 497,  700 => 495,  689 => 486,  687 => 485,  680 => 480,  677 => 479,  671 => 534,  669 => 497,  666 => 496,  664 => 479,  655 => 472,  652 => 471,  630 => 448,  621 => 442,  616 => 440,  609 => 436,  604 => 434,  598 => 431,  577 => 412,  574 => 411,  568 => 466,  566 => 411,  562 => 409,  559 => 408,  547 => 402,  543 => 400,  540 => 399,  535 => 361,  532 => 360,  527 => 357,  524 => 356,  512 => 577,  510 => 568,  505 => 565,  503 => 540,  498 => 537,  496 => 471,  492 => 469,  489 => 408,  487 => 399,  483 => 397,  472 => 389,  468 => 388,  460 => 383,  449 => 374,  447 => 373,  444 => 372,  442 => 371,  440 => 370,  438 => 369,  436 => 368,  429 => 363,  427 => 360,  424 => 359,  422 => 356,  89 => 26,  85 => 25,  77 => 20,  73 => 19,  68 => 17,  64 => 16,  60 => 15,  56 => 14,  52 => 13,  45 => 9,  41 => 8,  36 => 6,  29 => 1,);
    }
}

<?php

/* BBidsBBidsHomeBundle:Emailtemplates/Vendor:cheque_clearance.html.twig */
class __TwigTemplate_e1f9e487aca968d00321d24d2823aad361b3435c9ccbf37e69a161c8f26b5352 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; float:left; margin-top:0px; width:100%; \">Dear ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo ",</p>      
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">Thank you for your purchase. Your job request number is  ";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["OrderNumber"]) ? $context["OrderNumber"] : null), "html", null, true);
        echo " .</p>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">
\tOur administrative teams has activated your purchase.
</P>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">
\tPlease be assured that we will always strive to serve you better.
</P>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">
\tIf you have any questions or need help call us on 04 42 13 777.
</P>
<p style=\"font-family:Proxinova,Calibri,Arial; font-size:11pt; line-height:15pt; margin-bottom:10px; margin-top:0px; float:left; width:100%; \">
\tRegards<br>BusinessBid
</p>
  


";
    }

    public function getTemplateName()
    {
        return "BBidsBBidsHomeBundle:Emailtemplates/Vendor:cheque_clearance.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 2,  19 => 1,);
    }
}
